/*
	LAW to ISI Postgres
	
	Tables requested by ISI (Lee) for GL conversion 
	
	
	History:
	04/24/2018	FZ	Created
	
	Notes:
	Changes required  (Oracle=Postgres)
		BYTE = space, 
		NOT NULL ENABLE=NOT NULL, 
		VARCHAR2=character varying
		NUMBER=NUMERIC
		remove commas ( " " )
		remove asterix (*)
		CLOB=character varying(2000)
	
*/

-- Claim
CREATE TABLE public.claim
(
    claimId     character varying(20  ) NOT NULL,
    claimNumber character varying(20  ),
    claimSeq    NUMERIC(3,0),
    coverageDate DATE,
    claimMadeDate DATE,
    diaryDate DATE,
    diaryAction character varying(30  ),
    inceptionDate DATE,
	perClaimLimit               NUMERIC(11,2),
    aggregateLimit              NUMERIC(11,2),
    perClaimDeductible          NUMERIC(11,2),
    isTail                      character varying(5  ),
    totalLawyerCount            NUMERIC(22,0),
    estimateALAE                NUMERIC(11,2),
    reserveALAE                 NUMERIC(11,2) default 0,
    paymentALAE                 NUMERIC(11,2) default 0,
    deductibleALAE              NUMERIC(11,2),
    incurredDeductibleALAE      NUMERIC(11,2),
    netIncurredALAE             NUMERIC(11,2),
    grossIncurredALAE           NUMERIC(11,2),
    estimateIndemnity           NUMERIC(11,2),
    reserveIndemnity            NUMERIC(11,2) default 0,
    paymentIndemnity            NUMERIC(11,2) default 0,
    deductibleIndemnity         NUMERIC(11,2),
    incurredDeductibleIndemnity NUMERIC(11,2),
    netIncurredIndemnity        NUMERIC(11,2),
    grossIncurredIndemnity      NUMERIC(11,2),
    paymentNonPolicyALAE        NUMERIC(11,2),
    deductibleAdvance           NUMERIC(11,2),
    deductibleReimburse         NUMERIC(11,2),
    deductibleWriteOff          NUMERIC(11,2),
    deductibleBalance           NUMERIC(11,2),
    deductibleDirect            NUMERIC(11,2),
    treatyYear                  NUMERIC(22,0),
    claimType                   character varying(6  ),
    pleadingCaption             character varying(200  ),
    court                       character varying(100  ),
    courtCaseNum                character varying(25  ),
    insuredServeDate DATE,
    insurerServeDate DATE,
    firmServeDate DATE,
    isFeeDispute       character varying(5  ),
    trialDateRange     character varying(25  ),
    isJudgmentClaimant character varying(5  ),
    lawDispositionId   character varying(20  ),
    lastReserveDate DATE,
    fileStatus          character varying(5  ),
    respAtty            character varying(5  ),
    yrsOfPrac           character varying(5  ),
    claimantZip         character varying(10  ),
    insuredZip          character varying(10  ),
    isSuit              character varying(5  ),
    isCapRpt            character varying(5  ),
    isBench             character varying(5  ),
    isReinsuranceNotice character varying(5  ),
    isNoBill            character varying(5  ),
    isReinsurerReport   character varying(5  ),
    reinsurerReportDate DATE,
    otherPayment   NUMERIC(18,2),
    otherReimburse NUMERIC(18,2),
    benchDate DATE,
    capRptDate DATE,
    suitDate DATE,
    reinsurerReimburse       NUMERIC(21,2),
    isClaimRepair            character varying(5  ),
    isConflictInterest       character varying(5  ),
    isCoverageIssue          character varying(5  ),
    isEthicalProblem         character varying(5  ),
    isSalvageSubrogation     character varying(5  ),
    committeePersonId        character varying(20  ),
    treatyId                 character varying(20  ) NOT NULL,
    deductiblePrepay         NUMERIC(11,2),
    deductiblePrepayAllocate NUMERIC(11,2),
	subrogationIndemnity     NUMERIC(11,2),
	subrogationALAE      	   NUMERIC(11,2),
    CONSTRAINT Claim_PK PRIMARY KEY (claimId)
); 

-- ClaimCoverage
 CREATE TABLE public.claimcoverage
  (
    claimId                     character varying(20 ) NOT NULL,
    coverageId                  character varying(20 ) NOT NULL,
    estimateALAE                NUMERIC(11,2),
    reserveALAE                 NUMERIC(11,2),
    paymentALAE                 NUMERIC(11,2),
    deductibleALAE              NUMERIC(11,2),
    incurredDeductibleALAE      NUMERIC(11,2),
    netIncurredALAE             NUMERIC(11,2),
    grossIncurredALAE           NUMERIC(11,2),
    estimateIndemnity           NUMERIC(11,2),
    reserveIndemnity            NUMERIC(11,2),
    paymentIndemnity            NUMERIC(11,2),
    deductibleIndemnity         NUMERIC(11,2),
    incurredDeductibleIndemnity NUMERIC(11,2),
    netIncurredIndemnity        NUMERIC(11,2),
    grossIncurredIndemnity      NUMERIC(11,2),
    deductibleAdvance           NUMERIC(11,2),
    deductibleReimburse         NUMERIC(11,2),
    deductibleWriteOff          NUMERIC(11,2),
    deductibleBalance           NUMERIC(11,2),
    deductibleDirect            NUMERIC(11,2),
    perClaimLimit               NUMERIC(11,2),
    perClaimDeductible          NUMERIC(11,2),
    CONSTRAINT claimcoverage_PK PRIMARY KEY (claimId, coverageId)
);

--ClaimPayment
CREATE TABLE public.claimpayment
  (
    claimPaymentId           character varying(20 ) NOT NULL,
    claimId                  character varying(20 ) NOT NULL,
    eventId                  character varying(20 ) NOT NULL,
    disbursementId           character varying(20 ),
    receiptAllocationId      character varying(20 ),
    claimPaymentSeq          NUMERIC(3,0),
    paymentALAE              NUMERIC(11,2),
    paymentIndemnity         NUMERIC(11,2),
    paymentNonPolicyALAE     NUMERIC(11,2),
    deductibleALAE           NUMERIC(11,2),
    deductibleIndemnity      NUMERIC(11,2),
    deductibleAdvance        NUMERIC(11,2),
    deductibleDirect         NUMERIC(11,2),
    deductibleOverpay        NUMERIC(11,2),
    subrogationALAE          NUMERIC(11,2),
    subrogationIndemnity     NUMERIC(11,2),
    tranCode                 character varying(5 ),
    descCode                 character varying(5 ),
    otherReimburse           NUMERIC(11,2),
    otherPayment             NUMERIC(11,2),
    recoverableALAE          NUMERIC(11,2),
    recoverableIndemnity     NUMERIC(11,2),
    deductiblePrepayAllocate NUMERIC(11,2),
    CONSTRAINT claimpayment_PK PRIMARY KEY (claimPaymentId)
);

--ClaimReserve
CREATE TABLE public.claimreserve
  (
    claimReserveId           character varying(20 ) NOT NULL,
    claimId                  character varying(20 ) NOT NULL,
    eventId                  character varying(20 ) NOT NULL,
    claimReserveSeq          NUMERIC(3,0),
    estimateALAE             NUMERIC(20,2),
    estimateIndemnity        NUMERIC(20,2),
    sumEstimateALAE          NUMERIC(20,2),
    sumEstimateIndemnity     NUMERIC(20,2),
    estimateNonPolicyALAE    NUMERIC(20,2),
    sumEstimateNonPolicyALAE NUMERIC(20,2),
    CONSTRAINT claimreserve_PK PRIMARY KEY (claimReserveId)
);

--Coverage
CREATE TABLE public.coverage
  (
    coverageId         character varying(20 ) NOT NULL,
    applicationId      character varying(20 ),
    applicationLimitId character varying(20 ),
    coverageTypeId     character varying(20 ) NOT NULL,
    eventId            character varying(20 ) NOT NULL,
    firmId             character varying(20 ) NOT NULL,
    historyId          character varying(20 ),
    policyId           character varying(20 ),
    quoteId            character varying(20 ),
    quoteBookId        character varying(20 ),
    quoteCoverageId    character varying(20 ),
    coverageSeq        NUMERIC(3,0),
    coverageLocator    character varying(20 ),
    firmName           character varying(100 ),
    issueDate DATE,
    retroactiveDate DATE,
    perClaimLimit               NUMERIC(9,0),
    aggregateLimit              NUMERIC(9,0),
    perClaimDeductible          NUMERIC(9,0),
    aggregateDeductible         NUMERIC(9,0),
    estimateALAE                NUMERIC(11,2),
    reserveALAE                 NUMERIC(11,2),
    paymentALAE                 NUMERIC(11,2),
    deductibleALAE              NUMERIC(11,2),
    incurredDeductibleALAE      NUMERIC(11,2),
    netIncurredALAE             NUMERIC(11,2),
    grossIncurredALAE           NUMERIC(11,2),
    estimateIndemnity           NUMERIC(11,2),
    reserveIndemnity            NUMERIC(11,2),
    paymentIndemnity            NUMERIC(11,2),
    deductibleIndemnity         NUMERIC(11,2),
    incurredDeductibleIndemnity NUMERIC(11,2),
    netIncurredIndemnity        NUMERIC(11,2),
    grossIncurredIndemnity      NUMERIC(11,2),
    deductibleAdvance           NUMERIC(11,2),
    deductibleReimburse         NUMERIC(11,2),
    deductibleWriteOff          NUMERIC(11,2),
    deductibleBalance           NUMERIC(11,2),
    deductibleDirect            NUMERIC(11,2),
    factorPremium               NUMERIC(9,2),
    schedulePremium             NUMERIC(9,2),
    excessCoveragePremium       NUMERIC(9,2),
    premium                     NUMERIC(9,2),
    premiumAdjust               NUMERIC(9,2),
    financeCharge               NUMERIC(9,2),
    financeChargeDue            NUMERIC(9,2),
    surplus                     NUMERIC(9,2),
    transfer                    NUMERIC(9,2),
    writeOff                    NUMERIC(9,2),
    dueAmount                   NUMERIC(9,2),
    receiveAmount               NUMERIC(9,2),
    returnAmount                NUMERIC(9,2),
    excessPremium               NUMERIC(9,2),
    semiFacPremium              NUMERIC(9,2),
    trueFacPremium              NUMERIC(9,2),
    cancellationCoverageId      character varying(20 ),
    premiumReceipt              NUMERIC(9,2),
    premiumWriteOff             NUMERIC(9,2),
    financeChargeReceipt        NUMERIC(9,2),
    financeChargeWriteOff       NUMERIC(9,2),
    deductiblePrepay            NUMERIC(11,2),
    deductiblePrepayAllocate    NUMERIC(11,2),
    CONSTRAINT coverage_PK PRIMARY KEY (coverageId)
);

--DeductibleReimburse
CREATE TABLE public.deductiblereimburse
  (
    deductibleReimburseId  character varying(20 ) NOT NULL,
    eventId                character varying(20 ) NOT NULL,
    claimId                character varying(20 ) NOT NULL,
    disbursementId         character varying(20 ),
    receiptAllocationId    character varying(20 ),
    deductibleReimburseSeq NUMERIC(3,0),
    deductibleReimburse    NUMERIC(11,2),
    deductibleWriteOff     NUMERIC(11,2),
    deductiblePrepay       NUMERIC(11,2),
    CONSTRAINT deductiblereimburse_PK PRIMARY KEY (deductibleReimburseId) 
);
 

--Disbursement
CREATE TABLE public.disbursement
  (
    disbursementId      character varying(20 ) NOT NULL,
    eventId             character varying(20 ) NOT NULL,
    vendorId            character varying(20 ),
    receiptId           character varying(20 ),
    disbursementSeq     NUMERIC(8,0),
    checkNumber         character varying(20 ),
    referenceNumber     character varying(35 ),
    payee               character varying(100 ),
    payeeLine2          character varying(100 ),
    overpayAmount       NUMERIC(11,2),
    receiptAllocationId character varying(20 ),
    CONSTRAINT disbursement_PK PRIMARY KEY (disbursementId)
); 

--Event
CREATE TABLE public.event
  (
    eventId         character varying(20 ) NOT NULL,
    eventTypeId     character varying(20 ) NOT NULL,
    eventTypeRuleId character varying(20 ),
    folderId        character varying(20 ),
    parentEventId   character varying(20 ),
    eventLocator    character varying(20 ),
    eventSeq        NUMERIC(3,0),
    eventDate DATE,
    eventName character varying(80 ),
    eventDesc character varying(80 ),
    --eventNote character varying(2000),
    eventAmount NUMERIC(11,2),
    createDate DATE NOT NULL,
    createUserId character varying(20 ),
    modifyDate DATE,
    modifyUserId character varying(20 ),
    notifyDate DATE,
    notifyUserId character varying(20 ),
    dueDate DATE,
    dueUserId character varying(20 ),
    completeDate DATE,
    completeUserId character varying(20 ),
    voidDate DATE,
    voidUserId  character varying(20 ),
    eventStatus character varying(10 ),
    eventURN    character varying(255 ),
    CONSTRAINT event_PK PRIMARY KEY (eventId)
); 

--EventType
 CREATE TABLE public.eventtype
  (
    eventTypeId      character varying(20 ) NOT NULL,
    sourceTypeId     character varying(20 ),
    eventTypeLocator character varying(20 ),
    eventTypeName    character varying(40 ),
    --eventTypeDesc    character varying(255 ),
    documentFileName character varying(128 ),
    --eventTypeNote character varying(2000 ),
    owner      character varying(20 ),
    version    character varying(5 ),
    folderPath character varying(255 ),
	letterheadFileName 	character varying(128),
	inactiveDate DATE,
    CONSTRAINT eventtype_PK PRIMARY KEY (eventTypeId)
);

--GLAccount
 CREATE TABLE public.glaccount
  (
    glAccountId character varying(20 ) NOT NULL,
    accountName character varying(40 ),
    accountDesc character varying(160 ),
    accountNote character varying(2000 ),
    access       NUMERIC(22,0),
    accountType  character varying(1 ),
    accountGroup character varying(8 ),
    isObsolete   character varying(5 ),
    CONSTRAINT glaccount_PK PRIMARY KEY (glAccountId)
);

--GLEntry
CREATE TABLE public.glentry
  (
    glEntryId  character varying(20 ) NOT NULL,
    eventId    character varying(20 ),
    glBatchId  character varying(20 ),
    glEntrySeq NUMERIC(3,0),
    entryDate DATE NOT NULL,
    entryName     character varying(40 ),
    sourceLedger  character varying(20 ) NOT NULL,
    sourceType    character varying(20 ),
    fiscalYear    NUMERIC(4,0) NOT NULL,
    fiscalPeriod  NUMERIC(2,0) NOT NULL,
    debitAmount   NUMERIC(11,2) NOT NULL,
    creditAmount  NUMERIC(11,2) NOT NULL,
    balanceAmount NUMERIC(11,2) NOT NULL,
    groupEventId  character varying(20 ),
    CONSTRAINT glentry_PK PRIMARY KEY (glEntryId)
);  

--GLTransaction
CREATE TABLE public.gltransaction
  (
    glTransactionId      character varying(20 ) NOT NULL,
    glAccountId          character varying(20 ) NOT NULL,
    glEntryId            character varying(20 ) NOT NULL,
    glTransactionSeq     NUMERIC(6,0),
    transactionName      character varying(40 ),
    transactionReference character varying(20 ),
    transactionAmount    NUMERIC(11,2),
    CONSTRAINT gltransaction_PK PRIMARY KEY (glTransactionId)
); 

--Instance
CREATE TABLE public.instance
  (
    instanceId           character varying(20 ) NOT NULL,
    coverageId           character varying(20 ),
    eventId              character varying(20 ),
    firmId               character varying(20 ),
    historyId            character varying(20 ),
    cwpTypeId            character varying(20 ),
    lawErrorTypeId       character varying(20 ),
    lawDispositionId     character varying(20 ),
    otherInstanceId      character varying(20 ),
    policyId             character varying(20 ),
    treatyId             character varying(20 ),
    mainLawyerId         character varying(20 ),
    instanceSeq          NUMERIC(3,0),
    firmName             character varying(100 ),
    claimant             character varying(200 ),
    claimantRelationship character varying(20 ),
    legalCapacity        character varying(20 ),
    adjuster             character varying(40 ),
    reportDate DATE,
    reporterTypeId character varying(20 ),
    coverageDate DATE,
    occurDateRange character varying(60 ),
    occurDate DATE,
    knowDateRange character varying(60 ),
    knowDate DATE,
    openDate DATE,
    closeDate DATE,
    reOpenDate DATE,
    boxNo                NUMERIC(4,0),
    archiveNo            NUMERIC(4,0),
    mainLawActivityId    character varying(20 ),
    mainLawAreaId        character varying(20 ),
    mainLawErrorId       character varying(20 ),
    lastStatusEventId    character varying(20 ),
    watchListCode        character varying(20 ),
    estimateGeneralLegal NUMERIC(11,2),
    reserveGeneralLegal  NUMERIC(11,2),
    paymentGeneralLegal  NUMERIC(11,2),
    incurredGeneralLegal NUMERIC(11,2),
    isFeeDispute         character varying(5 ),
    isMedicare           character varying(5 ),
    CONSTRAINT instance_PK PRIMARY KEY (instanceId)
);

--Invoice
CREATE TABLE public.invoice
  (
    invoiceId       character varying(20 ) NOT NULL,
    invoiceGroupId  character varying(20 ) NOT NULL,
    eventId         character varying(20 ) NOT NULL,
    entityId        character varying(20 ),
    invoiceSeq      NUMERIC(6,0),
    invoiceNumber   character varying(20 ),
    invoiceAmount   NUMERIC(11,2),
    invoiceWriteOff NUMERIC(11,2),
    invoiceReceipt  NUMERIC(11,2),
    CONSTRAINT invoice_PK PRIMARY KEY (invoiceId)
);

-- InvoiceGroup
CREATE TABLE public.invoicegroup
  (
    invoiceGroupId     character varying(20 ) NOT NULL,
    invoiceGroupTypeId character varying(20 ) NOT NULL,
    invoiceGroupNumber character varying(20 ),
    claimId            character varying(20 ),
    coverageId         character varying(20 ),
    policyId           character varying(20 ),
    brokerId           character varying(20 ),
    claimPaymentId     character varying(20 ),
    CONSTRAINT invoicegroup_PK PRIMARY KEY (invoiceGroupId)
);

-- InvoiceGroupType
CREATE TABLE public.invoicegrouptype
  (
    invoiceGroupTypeId   character varying(20 ) NOT NULL,
    invoiceGroupTypeName character varying(40 ),
    invoiceGroupTypeDesc character varying(160 ),
    invoiceGroupTypeNote character varying(2000 ),
    categoryType character varying(20 ),
    interestPercent numeric(5,5),
    CONSTRAINT invoicegrouptype_PK PRIMARY KEY (invoiceGroupTypeId)
);

-- InvoiceItemType (no convert required)
CREATE TABLE public.invoiceitemtype
  (
    invoiceId       character varying(20 ) NOT NULL,
    itemTypeId      character varying(20 ) NOT NULL,
    invoiceAmount   NUMERIC(11,2),
    invoiceWriteOff NUMERIC(11,2),
    invoiceReceipt  NUMERIC(11,2),
    CONSTRAINT invoiceitemtype_PK PRIMARY KEY (invoiceId, itemTypeId)
);

-- InvoiceReceipt(no convert required) [NO RECORDS]
CREATE TABLE public.invoicereceipt
  (
    invoiceId           character varying(20 ) NOT NULL,
    itemTypeId          character varying(20 ) NOT NULL,
    receiptAllocationId character varying(20 ) NOT NULL,
    invoiceWriteOff     NUMERIC(11,2),
    invoiceReceipt      NUMERIC(11,2),
    accountingEventId   character varying(20 ),
    CONSTRAINT invoicereceipt_PK PRIMARY KEY (invoiceId, itemTypeId, receiptAllocationId)
);
 
--Policy
CREATE TABLE public.policy
  (
    policyId       character varying(20 ) NOT NULL,
    eventId        character varying(20 ) NOT NULL,
    mainCoverageId character varying(20 ),
    firmId         character varying(20 ) NOT NULL,
    coverageTypeId character varying(20 ),
    policySeq      NUMERIC(3,0),
    policyNumber   character varying(20 ),
    policySuffix   character varying(10 ),
    firmName       character varying(100 ),
    issueDate DATE,
    effectiveDate DATE NOT NULL,
    expirationDate DATE NOT NULL,
    policyRetroactiveDate DATE,
    cancelEffectiveDate DATE,
    cancelIssueDate DATE,
    isPolicyTailCoverage character varying(5 ),
    perClaimLimit        NUMERIC(9,0),
    aggregateLimit       NUMERIC(9,0),
    perClaimDeductible   NUMERIC(9,0),
    aggregateDeductible  NUMERIC(9,0),
    initalPaidDate DATE,
    paidToDate DATE,
    newReissue                  character varying(1 ),
    totalLawyerCount            NUMERIC(11,0),
    treatyId                    character varying(20 ) NOT NULL,
    factorPremium               NUMERIC(9,2),
    schedulePremium             NUMERIC(9,2),
    excessCoveragePremium       NUMERIC(9,2),
    premium                     NUMERIC(9,2),
    premiumAdjust               NUMERIC(9,2),
    financeCharge               NUMERIC(9,2),
    financeChargeDue            NUMERIC(9,2),
    surplus                     NUMERIC(9,2),
    transfer                    NUMERIC(9,2),
    writeOff                    NUMERIC(9,2),
    dueAmount                   NUMERIC(9,2),
    receiveAmount               NUMERIC(9,2),
    returnAmount                NUMERIC(9,2),
    excessPremium               NUMERIC(9,2),
    semiFacPremium              NUMERIC(9,2),
    trueFacPremium              NUMERIC(9,2),
    estimateALAE                NUMERIC(11,2),
    reserveALAE                 NUMERIC(11,2),
    paymentALAE                 NUMERIC(11,2),
    deductibleALAE              NUMERIC(9,2),
    incurredDeductibleALAE      NUMERIC(9,2),
    netIncurredALAE             NUMERIC(11,2),
    grossIncurredALAE           NUMERIC(11,2),
    estimateIndemnity           NUMERIC(11,2),
    reserveIndemnity            NUMERIC(11,2),
    paymentIndemnity            NUMERIC(11,2),
    deductibleIndemnity         NUMERIC(9,2),
    incurredDeductibleIndemnity NUMERIC(9,2),
    netIncurredIndemnity        NUMERIC(11,2),
    grossIncurredIndemnity      NUMERIC(11,2),
    paymentNonPolicyALAE        NUMERIC(11,2),
    deductibleAdvance           NUMERIC(11,2),
    deductibleReimburse         NUMERIC(11,2),
    deductibleBalance           NUMERIC(11,2),
    deductibleDirect            NUMERIC(11,2),
    polItem                     NUMERIC(11,2),
    treatyYear                  NUMERIC(9,0),
    endorsementCount            NUMERIC(6,0),
    deductibleWriteOff          NUMERIC(9,2),
    paymentMethod               character varying(20 ),
    facultative                 NUMERIC(9,2),
    cleDisc                     NUMERIC(9,2),
    premDisc                    NUMERIC(9,2),
    isJudge                     character varying(5 ),
    isTail                      character varying(5 ),
    policyTerm                  NUMERIC(5,0),
    surplusAmount               NUMERIC(9,2),
    policyState                 character varying(5 ),
    checkamt                    NUMERIC(9,2),
    priorpay                    NUMERIC(9,2),
    referralService             character varying(20 ),
    expenseAllowance            NUMERIC(9,0),
    policyStatus                character varying(5 ),
    legFacultative              NUMERIC(3,0),
    legCleDisc                  NUMERIC(3,0),
    legPremDisc                 NUMERIC(3,0),
    legFirmRating               NUMERIC(20,2),
    legClaimDisc                NUMERIC(3,0),
    legClaimCost                NUMERIC(20,2),
    legDefDis                   NUMERIC(3,0),
    legRefServ                  NUMERIC(3,0),
    legPtBase112                character varying(5 ),
    legPtBase1324               character varying(5 ),
    legPtPa112                  NUMERIC(3,0),
    legPtPa1324                 NUMERIC(3,0),
    legPtPa25                   NUMERIC(3,0),
    legContractBa               character varying(5 ),
    legConPaCon                 NUMERIC(3,0),
    legConPaConSo               NUMERIC(3,0),
    legClaimsSur                NUMERIC(20,2),
    legTailPrem                 NUMERIC(20,2),
    legTailDate DATE,
    paralegalCount           NUMERIC(9,2),
    staffCount               NUMERIC(8,2),
    contractNumber           character varying(20 ),
    cedeCommissionPercent    NUMERIC(7,4),
    premiumReceipt           NUMERIC(9,2),
    premiumWriteOff          NUMERIC(9,2),
    financeChargeReceipt     NUMERIC(9,2),
    financeChargeWriteOff    NUMERIC(9,2),
    deductiblePrepay         NUMERIC(11,2),
    deductiblePrepayAllocate NUMERIC(11,2),
    CONSTRAINT policy_PK PRIMARY KEY (policyId)
);
 
--PolicyAccounting
CREATE TABLE public.policyaccounting
  (
    policyAccountingId    character varying(20 ) NOT NULL,
    eventId               character varying(20 ) NOT NULL,
    disbursementId        character varying(20 ),
    receiptAllocationId   character varying(20 ),
    coverageId            character varying(20 ),
    policyId              character varying(20 ),
    policyAccountingSeq   NUMERIC(3,0),
    factorPremium         NUMERIC(9,2),
    schedulePremium       NUMERIC(9,2),
    excessCoveragePremium NUMERIC(9,2),
    premium               NUMERIC(9,2),
    premiumAdjust         NUMERIC(9,2),
    financeCharge         NUMERIC(9,2),
    financeChargeDue      NUMERIC(9,2),
    surplus               NUMERIC(9,2),
    transfer              NUMERIC(9,2),
    writeOff              NUMERIC(9,2),
    dueAmount             NUMERIC(9,2),
    receiveAmount         NUMERIC(9,2),
    returnAmount          NUMERIC(9,2),
    premiumReceipt        NUMERIC(9,2),
    premiumWriteOff       NUMERIC(9,2),
    financeChargeReceipt  NUMERIC(9,2),
    financeChargeWriteOff NUMERIC(9,2),
    excessPremium         NUMERIC(11,2),
    semiFacPremium        NUMERIC(11,2),
    trueFacPremium        NUMERIC(11,2),
    CONSTRAINT policyaccounting_PK PRIMARY KEY (policyAccountingId)
);
 
--Receipt [NO RECORDS]
CREATE TABLE public.receipt
  (
    receiptId     character varying(20 ) NOT NULL,
    entityId      character varying(20 ) NOT NULL,
    eventId       character varying(20 ) NOT NULL,
    receiptSeq    NUMERIC(3,0),
    receiptNumber character varying(20 ),
    depositDate DATE,
    checkNumber             character varying(20 ),
    referenceNumber         character varying(20 ),
    payor                   character varying(100 ),
    receiptAmount           NUMERIC(11,2),
    claimPaymentReceipt     NUMERIC(11,2),
    deductibleReceipt       NUMERIC(11,2),
    instancePaymentReceipt  NUMERIC(11,2),
    policyAccountingReceipt NUMERIC(11,2),
    overpayAmount           NUMERIC(11,2),
    suspenseAmount          NUMERIC(11,2),
    allocationAmount        NUMERIC(11,2),
    CONSTRAINT receipt_PK PRIMARY KEY (receiptId)
); 

--ReceiptAllocation [NO RECORDS]
CREATE TABLE public.receiptallocation
  (
    receiptAllocationId     character varying(20 ) NOT NULL,
    eventId                 character varying(20 ) NOT NULL,
    receiptId               character varying(20 ) NOT NULL,
    receiptAmount           NUMERIC(11,2),
    claimPaymentReceipt     NUMERIC(11,2),
    deductibleReceipt       NUMERIC(11,2),
    instancePaymentReceipt  NUMERIC(11,2),
    policyAccountingReceipt NUMERIC(11,2),
    suspenseAmount          NUMERIC(11,2),
    allocationAmount        NUMERIC(11,2),
    overpayAmount           NUMERIC(11,2),
    CONSTRAINT receiptallocation_PK PRIMARY KEY (receiptAllocationId)
); 

--ReinsurerAccounting [NO RECORDS]
CREATE TABLE public.reinsureraccounting
  (
    reinsurerAccountingId  character varying(20 ) NOT NULL,
    claimId                character varying(20 ) NOT NULL,
    reinsurerId            character varying(20 ),
    treatyLayerId          character varying(20 ),
    eventId                character varying(20 ),
    reinsurerAccountingSeq NUMERIC(3,0),
    paymentDate DATE,
    checkNumber          character varying(20 ),
    referenceNumber      character varying(20 ),
    recoverableALAE      NUMERIC(11,2),
    recoverableIndemnity NUMERIC(11,2),
    recoverableAmount    NUMERIC(11,2),
    recoveryALAE         NUMERIC(11,2),
    recoveryIndemnity    NUMERIC(11,2),
    recoveryAmount       NUMERIC(11,2),
    recoverableBalance   NUMERIC(18,2),
    brokerId             character varying(20 ),
    receiptAllocationId  character varying(20 ),
    CONSTRAINT reinsureraccounting_PK PRIMARY KEY (reinsurerAccountingId) 
);

-- 05/21/2018
--Endorsement
create table public.endorsement 
 (
  endorsementId 		character varying(20) not null,
  coverageId 			character varying(20) not null, 
  policyId 				character varying(20) not null,
  endorsementSeq 		integer,
  endorsementNumber 	character varying(20),
  endorsementDesc 		character varying(160),
  effectiveDate 		date,
  expirationDate 		date,
  premium 				decimal(22,0),
  yearsTail 			decimal(4,0), 
  constraint endorsement_PK primary key (endorsementId)
);

--05/30/2018
--policylawyercoverage
CREATE TABLE  public.policylawyercoverage  
   (	 policyId  character varying(20 ) NOT NULL, 
	 lawyerId  character varying(20 ) NOT NULL, 
	 coverageId  character varying(20 ) NOT NULL, 
	 effectiveDate  DATE, 
	 expirationDate  DATE, 
	 retroactiveDate  DATE, 
	 eventId  character varying(20 ) NOT NULL, 
	 excludeAfterDate  DATE, 
	 cancelEffectiveDate  DATE, 
	 excludeBeforeDate  DATE, 
	 cancellationCoverageId  character varying(20 ), 
	 policyStatus  character varying(5 ),
	CONSTRAINT  policylawyercoverage_PK  PRIMARY KEY ( policyId ,  lawyerId ,  coverageId )
);

--lawyer
CREATE TABLE  public.lawyer  
   (	 lawyerId  character varying(20 ) NOT NULL, 
	 lawyerSeq  NUMERIC(8,0), 
	 positionCode  character varying(20 ), 
	 inPracticeDuration  NUMERIC(22,0), 
	 inPracticeDate  DATE, 
	 isUninsurable  character varying(5 ), 
	 lawSchool  character varying(5 ), 
	 barDistrict  character varying(5 ), 
	 county  character varying(5 ), 
	 occupationCode  character varying(5 ), 
	 lawyerCode  character varying(2 ), 
	 mainStateBarId  character varying(20 ), 
	 latestApplicationId  character varying(20 ), 
	 latestCoverageId  character varying(20 ), 
	 latestFirmId  character varying(20 ), 
	 latestPolicyId  character varying(20 ), 
	 latestPolicyStatus  character varying(20 ), 
	 effectiveDate  DATE, 
	 expirationDate  DATE, 
	 retroactiveDate  DATE, 
	 cleCount  NUMERIC(22,0), 
	 lawyerDesignationId  character varying(20 ), 
	 CONSTRAINT  lawyer_PK  PRIMARY KEY ( lawyerId )
);

--lawyerstatebar
CREATE TABLE  public.lawyerstatebar  
   (	 lawyerId  character varying(20 ) NOT NULL, 
	 stateBarId  character varying(20 ) NOT NULL, 
	 stateBarNumber  character varying(20 ), 
	 barAdmitDate  DATE, 
	 barYear  NUMERIC(4,0), 
	 isInactive  character varying(5 ), 
	 activityDate  DATE, 
	 CONSTRAINT  lawyerstatebar_PK  PRIMARY KEY ( lawyerId ,  stateBarId )
);

--quote
CREATE TABLE  public.quote  
   (	 quoteId  character varying(20 ) NOT NULL, 
	 applicationId  character varying(20 ) NOT NULL, 
	 eventId  character varying(20 ) NOT NULL, 
	 invoiceGroupId  character varying(20 ), 
	 receiptId  character varying(20 ), 
	 mainQuoteBookId  character varying(20 ), 
	 mainQuoteCoverageId  character varying(20 ), 
	 quoteSeq  NUMERIC(8,0), 
	 quoteName  character varying(40 ), 
	 issueDate  DATE, 
	 expirationDate  DATE, 
	 interestRate  NUMERIC(8,5) 
	 CONSTRAINT  quote_PK  PRIMARY KEY ( quoteId )
);

--entity
CREATE TABLE  public.entity  
   (	 entityId  character varying(20 ) NOT NULL, 
	 eventId  character varying(20 ) NOT NULL, 
	 entitySeq  NUMERIC(8,0), 
	 entityLocator  character varying(20 ), 
	 entityNumber  character varying(20 ), 
	 entityName  character varying(100 ) NOT NULL, 
	 namespace  character varying(100 ), 
	 namespaceTypeId  character varying(20 ), 
	 entityURL  character varying(66 ), 
	 sourceId  character varying(20 ), 
	 latestHistoryId  character varying(20 ), 
	 mainLocationId  character varying(20 ), 
	 NAMEID  character varying(5 ), 
	 EFFECTIVEDATE  DATE, 
	 CONSTRAINT  entity_PK  PRIMARY KEY ( entityId )
);

--location
CREATE TABLE  public.location  
   ( locationId  character varying(20 ) NOT NULL, 
	 eventId  character varying(20 ) NOT NULL, 
	 locationTypeId  character varying(20 ) NOT NULL, 
	 mainEntityId  character varying(20 ), 
	 locationSeq  NUMERIC(8,0), 
	 addressLine1  character varying(45 ), 
	 addressLine2  character varying(40 ), 
	 addressLine3  character varying(40 ), 
	 city  character varying(28 ), 
	 county  character varying(25 ), 
	 state  CHAR(2 ), 
	 zip  character varying(10 ), 
	 country  character varying(25 ), 
	 telephone  character varying(14 ), 
	 fax  character varying(14 ), 
	 email  character varying(66 ), 
	 effectiveDate  DATE, 
	 expirationDate  DATE, 
	 CONSTRAINT  location_PK  PRIMARY KEY ( locationId )
);

--taillawyer2
CREATE TABLE  public.taillawyer2  
   (	 policyId  character varying(20 ), 
	 lawyerId  character varying(20 ), 
	 coverageId  character varying(20 ), 
	 effectiveDate  DATE, 
	 expirationDate  DATE, 
	 retroactiveDate  DATE, 
	 excludeBeforeDate  DATE, 
	 excludeAfterDate  DATE, 
	 cancelEffectiveDate  DATE, 
	 eventId  character varying(20 ), 
	 firmId  character varying(20 ), 
	 firmName  character varying(100 ), 
	 policyExpirationDate  DATE, 
	 perClaimLimit  NUMERIC(9,0), 
	 aggregateLimit  NUMERIC(9,0), 
	 perClaimDeductible  NUMERIC(9,0), 
	 aggregateDeductible  NUMERIC(9,0), 
	 eventTypeId  character varying(20 ), 
	 eventTypeName  character varying(40 ), 
	 policyNumber  character varying(20 ), 
	 applicationId  character varying(20 ), 
	 firmLocator  character varying(20 ), 
	 applicationRatingId  character varying(20 ), 
	 applicationLimitId  character varying(20 ), 
	 firmPremium  NUMERIC, 
	 premiumPerLawyer1M  NUMERIC, 
	 premiumPerLawyerXS5M  NUMERIC, 
	 premiumPerLawyerXS10M  NUMERIC, 
	 DeductibleFactor  NUMERIC, 
	 LimitFactor  NUMERIC, 
	 LawyerFactor  NUMERIC, 
	 LawyerYIP  NUMERIC, 
	 LawyerRDI  NUMERIC, 
	 LawyerActivity  NUMERIC, 
	 SchdAdjPctCalc  NUMERIC, 
	 lawyerName  character varying(100 ), 
	 lawyerActivityId  CHAR(2 ), 
	 premiumPerLawyer5MNoDeductible  NUMERIC, 
	 LawyerFactorUsed  character varying(14 ), 
	 tailTerm  character varying(40 ), 
	 quoteId  character varying(20 ), 
	 quoteCoverageId  character varying(20 ), 
	 tailType  character varying(6 ), 
	 oldTailPremium  NUMERIC, 
	 newTailPremium  NUMERIC
);

--firm
CREATE TABLE  public.firm  
   (	 firmId  character varying(20 ) NOT NULL, 
	 firmNumber  character varying(20 ), 
	 firmSeq  NUMERIC(8,0), 
	 stateBarNumber  character varying(20 ), 
	 nextEffectiveDate  DATE, 
	 latestApplicationId  character varying(20 ), 
	 latestInsurerId  character varying(20 ), 
	 initialPolicyId  character varying(20 ), 
	 latestPolicyId  character varying(20 ), 
	 latestPolicyStatus  character varying(20 ), 
	 underwritingStatusId  character varying(20 ), 
	 insureDuration  NUMERIC(5,2), 
	 insureStatusId  character varying(20 ), 
	 isDefensePanel  character varying(5 ), 
	 isUninsurable  character varying(5 ), 
	 visitDate  DATE, 
	 totalClaimCount  NUMERIC(22,0), 
	 totalLawyerCount  NUMERIC(22,0), 
	 effectiveDate  DATE, 
	 expirationDate  DATE, 
	 firmRetroactiveDate  DATE, 
	 perClaimLimit  NUMERIC(22,0), 
	 aggregateLimit  NUMERIC(22,0), 
	 perClaimDeductible  NUMERIC(22,0), 
	 premium  NUMERIC(22,0), 
	 totalPremium  NUMERIC(22,0), 
	 estimateALAE  NUMERIC(11,2), 
	 reserveALAE  NUMERIC(11,2), 
	 paymentALAE  NUMERIC(11,2), 
	 deductibleALAE  NUMERIC(11,2), 
	 incurredDeductibleALAE  NUMERIC(11,2), 
	 netIncurredALAE  NUMERIC(11,2), 
	 grossIncurredALAE  NUMERIC(11,2), 
	 estimateIndemnity  NUMERIC(11,2), 
	 reserveIndemnity  NUMERIC(11,2), 
	 paymentIndemnity  NUMERIC(11,2), 
	 deductibleIndemnity  NUMERIC(11,2), 
	 incurredDeductibleIndemnity  NUMERIC(11,2), 
	 netIncurredIndemnity  NUMERIC(11,2), 
	 grossIncurredIndemnity  NUMERIC(11,2), 
	 paymentNonPolicyALAE  NUMERIC(11,2), 
	 deductibleAdvance  NUMERIC(11,2), 
	 deductibleReimburse  NUMERIC(11,2), 
	 deductibleBalance  NUMERIC(11,2), 
	 deductibleDirect  NUMERIC(11,2), 
	 deductibleWriteOff  NUMERIC(11,2), 
	 officeCount  NUMERIC(4,0), 
	 maxLimit  NUMERIC(9,0), 
	 maxDeductible  NUMERIC(9,0), 
	 minDeductible  NUMERIC(9,0), 
	 claimSurcharge  NUMERIC(6,0), 
	 claimDiscount  NUMERIC(6,0), 
	 deductiblePrepay  NUMERIC(11,2), 
	 deductiblePrepayAllocate  NUMERIC(11,2), 
	 CONSTRAINT  firm_PK  PRIMARY KEY ( firmId )
);

--ApplicationLimit
CREATE TABLE public.applicationlimit  
   ( applicationLimitId  character varying(20  ) NOT NULL, 
	 applicationId  character varying(20  ) NOT NULL, 
	 coverageTypeId  character varying(20  ), 
	 applicationLimitSeq  NUMERIC(8,0), 
	 coverageTerm  NUMERIC(5,0), 
	 perClaimLimit  NUMERIC(9,0), 
	 aggregateLimit  NUMERIC(9,0), 
	 perClaimDeductible  NUMERIC(9,0), 
	 aggregateDeductible  NUMERIC(9,0), 
	 paCon  NUMERIC(4,0), 
	 paConS  NUMERIC(4,0), 
	 quoteInstruction  character varying(20  ), 
	 CONSTRAINT  applicationlimit_PK  PRIMARY KEY ( applicationLimitId )
);

--QuoteCoverage
CREATE TABLE  public.quotecoverage  
   ( quoteCoverageId  character varying(20  ) NOT NULL, 
	 coverageTypeId  character varying(20  ) NOT NULL, 
	 eventId  character varying(20  ) NOT NULL, 
	 quoteId  character varying(20  ) NOT NULL, 
	 quoteBookId  character varying(20  ), 
	 lawyerSet  character varying(20  ), 
	 applicationLimitId  character varying(20  ), 
	 parentQuoteCoverageId  character varying(20  ), 
	 quoteCoverageSeq  NUMERIC(8,0), 
	 retroactiveDate  DATE, 
	 coverageTerm  FLOAT(126), 
	 perClaimLimit  NUMERIC(9,0), 
	 aggregateLimit  NUMERIC(9,0), 
	 perClaimDeductible  NUMERIC(9,0), 
	 aggregateDeductible  NUMERIC(9,0), 
	 premium  NUMERIC(9,2), 
	 surplus  NUMERIC(9,2), 
	 initialPayment  NUMERIC(9,2), 
	 subsequentPayment  NUMERIC(9,2), 
	 factorPremium  NUMERIC(11,2), 
	 schedulePremium  NUMERIC(11,2), 
	 excessPremium  NUMERIC(11,2), 
	 semiFacPremium  NUMERIC(11,2), 
	 trueFacPremium  NUMERIC(11,2), 
	 policyAccountingId  character varying(20  ), 
	 CONSTRAINT  QuoteCoverage_PK  PRIMARY KEY ( quoteCoverageId )
);

--RatingLimitCrosstab
create table public.ratinglimitcrosstab
(
quoteBookId  character varying(20  ) NOT NULL 
,  quoteCoverageId  character varying(20  ) NOT NULL 
,  ContLegalEdCalc  NUMERIC 
,  ContinuityAmountCalc  NUMERIC 
,  Deductible  NUMERIC 
,  DeductibleFactor  NUMERIC 
,  FactorPremCalc  NUMERIC 
,  Limit  NUMERIC 
,  LimitFactor  NUMERIC 
,  LimitAgg  NUMERIC 
, PRM2M NUMERIC 
, PRM2MADJ NUMERIC 
, PRM3M NUMERIC 
, PRM3MADJ NUMERIC 
,  Prem5mCalc  NUMERIC 
,  PremCalc  NUMERIC 
,  PremXS5mMinCalc  NUMERIC 
,  PremXS10mCalc  NUMERIC 
);

--RatingCrosstab
create table public.ratingcrosstab
(
   quoteBookId  character varying(20  ) NOT NULL 
,  applicationId  character varying(20  ) NOT NULL 
,  firmId  character varying(20  ) NOT NULL 
,  effectiveDate  DATE 
,  AbstTitleInsAgent  NUMERIC 
,  Base  NUMERIC 
,  BodilyInjuryDefense  NUMERIC 
,  BodilyInjuryPlaintiff  NUMERIC 
,  ClaimsSurcharge  NUMERIC 
,  CorpBusOrg  NUMERIC 
,  DefReimbCoverage  NUMERIC 
,  DocketControl  NUMERIC 
,  DocketControlAmount  NUMERIC 
,  DomesticRelations  NUMERIC 
,  EstateProbateTrust  NUMERIC 
,  ExcessOfLossPremium  NUMERIC 
,  ExclVicariousLiab  NUMERIC 
,  ExclWordFormer  NUMERIC 
,  ExcludeCapacities  NUMERIC 
,  ExcludeOtherEntities  NUMERIC 
,  Firm  NUMERIC 
,  FirmRDI  NUMERIC 
,  FlatPremCalc  NUMERIC 
,  IntellProp  NUMERIC 
,  Manuscript  NUMERIC 
,  OtherAdjustment  NUMERIC 
,  RealEstate  NUMERIC 
,  RegulatoryExclusion  NUMERIC 
,  RestrictCvgExcess5M  NUMERIC 
,  SECCoverage  NUMERIC 
,  SchdAdjPctCalc  NUMERIC 
,  SeverityIndex  NUMERIC 
,  SeverityIndexZero  NUMERIC 
,  Term  NUMERIC 
,  WorkersCompDefense  NUMERIC 
,  WorkersCompPlaintiff  NUMERIC 
,  YearsInsured  NUMERIC 
);

--RatingLawyerCrosstab
CREATE TABLE  public.ratinglawyercrosstab  
   (	 quoteBookId  character varying(20  ) NOT NULL, 
	 lawyerId  character varying(20  ), 
	 ContLegalEdCredit  NUMERIC, 
	 LawyerActivity  NUMERIC, 
	 LawyerFactor  NUMERIC, 
	 LawyerRDI  NUMERIC, 
	 LawyerYIP  NUMERIC, 
	 DefReimbCoverage  NUMERIC, 
	 LawyerActivityAdjusted  NUMERIC
);

--Application
CREATE TABLE  public.application  
   (	 applicationId  character varying(20 ) NOT NULL, 
	 eventId  character varying(20 ) NOT NULL, 
	 firmId  character varying(20 ) NOT NULL, 
	 locationId  character varying(20 ), 
	 policyId  character varying(20 ), 
	 coverageId  character varying(20 ), 
	 applicationSeq  NUMERIC(8,0), 
	 firmName  character varying(100 ), 
	 receiveDate  DATE, 
	 signDate  DATE, 
	 effectiveDate  DATE, 
	 policyNumber  character varying(20 ), 
	 policyTerm  NUMERIC(18,0), 
	 applicationStatus  character varying(5 ), 
	 lawyerPrincipalCount  NUMERIC(18,0), 
	 lawyerOfCounselCount  NUMERIC(18,0), 
	 lawyerOtherCount  NUMERIC(18,0), 
	 staffCount  NUMERIC(18,0), 
	 staffPartTimeCount  NUMERIC(18,0), 
	 lawClerkCount  NUMERIC(18,0), 
	 abstracterCount  NUMERIC(18,0), 
	 accountantCount  NUMERIC(18,0), 
	 paralegalCount  NUMERIC(9,0), 
	 claimAgainstCount  NUMERIC(18,0), 
	 organizationType  character varying(6 ), 
	 secIncome  NUMERIC(28,2), 
	 suitCount  NUMERIC(18,0), 
	 abstracterTitleRevenue  NUMERIC(28,2), 
	 retroactiveDate  DATE, 
	 isBackUpLawyer  character varying(5 ), 
	 isDisbarred  character varying(5 ), 
	 isDocketControl  character varying(5 ), 
	 isIncidentKnowledge  character varying(5 ), 
	 isOfficeShare  character varying(5 ), 
	 isRefuseCoverage  character varying(5 ), 
	 isSuccessor  character varying(5 ), 
	 insureDuration  NUMERIC(18,0), 
	 totalLawyerCount  NUMERIC(18,0), 
	 priorLawyerCount  NUMERIC(18,0), 
	 cleDiscount  NUMERIC(18,0), 
	 surplus  NUMERIC(9,2), 
	 basicCat  NUMERIC(8,0), 
	 isJudge  character varying(5 ), 
	 isRenewal  character varying(5 ), 
	 referralService  character varying(255 ), 
	 applicationTypeId  character varying(20 ), 
	 CONSTRAINT  application_PK  PRIMARY KEY ( applicationId )
);