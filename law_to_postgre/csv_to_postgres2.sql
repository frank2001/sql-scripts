


-- claim
truncate table public.claim cascade;
copy public.claim from '/import/claim.csv' delimiter '|' csv;

-- claimcoverage
truncate table public.claimcoverage cascade;
copy public.claimcoverage from '/import/claimcoverage.csv' delimiter '|' csv;

-- claimpayment
truncate table public.claimpayment cascade;
copy public.claimpayment from '/import/claimpayment.csv' delimiter '|' csv;

-- claimreserve
truncate table public.claimreserve cascade;
copy public.claimreserve from '/import/claimreserve.csv' delimiter '|' csv;

-- coverage
truncate table public.coverage cascade;
copy public.coverage from '/import/coverage.csv' delimiter '|' csv;

-- DeductibleReimburse
truncate table public.deductiblereimburse cascade;
copy public.deductiblereimburse from '/import/deductiblereimburse.csv' delimiter '|' csv;

--Disbursement
truncate table public.disbursement cascade;
copy public.disbursement from '/import/disbursement.csv' delimiter '|' csv;

-- event
truncate table public.event cascade;
copy public.event from '/import/event.csv' delimiter '|' csv;

-- eventtype
truncate table public.eventtype cascade;
copy public.eventtype from '/import/eventtype.csv' delimiter '|' csv;

-- glaccount
truncate table public.glaccount cascade;
copy public.glaccount from '/import/.csv' delimiter '|' csv;

-- glentry
truncate table public. cascade;
copy public.glentry from '/import/glaccount.csv' delimiter '|' csv;

-- gltransaction
truncate table public.gltransaction cascade;
copy public.gltransaction from  '/import/gltransaction.csv' delimiter '|' csv;

-- instance
truncate table public.instance cascade;
copy public.instance from  '/import/instance.csv' delimiter '|' csv;

-- invoice
truncate table public.invoice cascade;
copy public.invoice from  '/import/invoice.csv' delimiter '|' csv;

-- invoicegroup
truncate table public.invoicegroup cascade;
copy public.invoicegroup from  '/import/invoicegroup.csv' delimiter '|' csv;

-- invoicegrouptype
truncate table public.invoicegrouptype cascade;
copy public.invoicegrouptype from  '/import/invoicegrouptype.csv' delimiter '|' csv;

-- invoiceitemtype
truncate table public.invoiceitemtype cascade;
copy public.invoiceitemtype from  '/import/invoiceitemtype.csv' delimiter '|' csv;

-- invoicereceipt
truncate table public.invoicereceipt cascade;
copy public.invoicereceipt from  '/import/invoicereceipt.csv' delimiter '|' csv;

-- policy
truncate table public.policy cascade;
copy public.policy from  '/import/policy.csv' delimiter '|' csv;

-- policyaccounting
truncate table public.policyaccounting cascade;
copy public.policyaccounting from  '/import/policyaccounting.csv' delimiter '|' csv;

-- receipt  [NO RECORDS]
truncate table public.receipt cascade;
copy public.receipt from  '/import/receipt.csv' delimiter '|' csv;

-- receiptallocation  [NO RECORDS]
truncate table public.receiptallocation cascade;
copy public.receiptallocation from  '/import/receiptallocation.csv' delimiter '|' csv;

-- reinsureraccounting  [NO RECORDS]
truncate table public.reinsureraccounting cascade;
copy public.reinsureraccounting from  '/import/reinsureraccounting.csv' delimiter '|' csv;

-- endorsement
truncate table public.endorsement cascade;
copy public.endorsement from '/import/endorsement.csv' delimiter '|' csv;



-- policylawyercoverage
truncate table public.policylawyercoverage cascade;
copy public.policylawyercoverage from '/import/policylawyercoverage.csv' delimiter '|' csv;

-- lawyer
truncate table public.lawyer cascade;
copy public.lawyer from '/import/lawyer.csv' delimiter '|' csv;

-- lawyerstatebar
truncate table public.lawyerstatebar cascade;
copy public.lawyerstatebar from '/import/lawyerstatebar.csv' delimiter '|' csv;

-- entity
truncate table public.entity cascade;
copy public.entity from '\import\entity.csv' delimiter '|' csv;

-- location
truncate table public.location cascade;
copy public.location from '\import\location.csv' delimiter '|' csv;

-- taillawyer2
truncate table public.taillawyer2 cascade;
copy public.taillawyer2 from '\import\taillawyer2.csv' delimiter '|' csv;

-- firm
truncate table public.firm cascade;
copy public.firm from '\import\firm.csv' delimiter '|' csv;

-- quote
truncate table public.quote cascade;
copy public.quote from '/import/quote.csv' delimiter '|' csv;



-- ratinglawyercrosstab
truncate table public.ratinglawyercrosstab cascade;
copy public.ratinglawyercrosstab from '/import/ratinglawyercrosstab.csv' delimiter '|' csv;
	
-- ratingcrosstab
truncate table public.ratingcrosstab cascade;
copy public.ratingcrosstab from '/import/ratingcrosstab.csv' delimiter '|' csv;

--ratinglimitcrosstab
truncate table public.ratinglimitcrosstab cascade;
copy public.ratinglimitcrosstab from '/import/ratinglimitcrosstab.csv' delimiter '|' csv; 

-- quotecoverage
truncate table public.quotecoverage cascade;
copy public.quotecoverage from '/import/quotecoverage.csv' delimiter '|' csv;

-- applicationlimit
truncate table public.applicationlimit cascade;
copy public.applicationlimit from '/import/applicationlimit.csv' delimiter '|' csv;

-- application
truncate table public.application cascade;
copy public.application from '/import/application.csv' delimiter '|' csv;