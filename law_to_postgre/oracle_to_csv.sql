
/*
*/

--------------------------------------------------------------------------------
-- CSV and spooling setup
-- sets up csv file properly 

set sqlformat

set colsep '|'
set sqlprompt ''
set pagesize 0
set trimspool on
set headsep off
set pages 0
set feed off
set long 2000
set longchunksize 2000
--------------------------------------------------------------------------------

-- claim
spool 'z:\claim.csv'
select "claimId"||'|'||"claimNumber"||'|'||"claimSeq"||'|'||"coverageDate"||'|'||"claimMadeDate"||'|'||"diaryDate"||'|'||"diaryAction"||'|'||"inceptionDate"||'|'||"perClaimLimit"||'|'||"aggregateLimit"||'|'||"perClaimDeductible"||'|'||"isTail"||'|'||"totalLawyerCount"||'|'||"estimateALAE"||'|'||"reserveALAE"||'|'||"paymentALAE"||'|'||"deductibleALAE"||'|'||"incurredDeductibleALAE"||'|'||"netIncurredALAE"||'|'||"grossIncurredALAE"||'|'||"estimateIndemnity"||'|'||"reserveIndemnity"||'|'||"paymentIndemnity"||'|'||"deductibleIndemnity"||'|'||"incurredDeductibleIndemnity"||'|'||"netIncurredIndemnity"||'|'||"grossIncurredIndemnity"||'|'||"paymentNonPolicyALAE"||'|'||"deductibleAdvance"||'|'||"deductibleReimburse"||'|'||"deductibleWriteOff"||'|'||"deductibleBalance"||'|'||"deductibleDirect"||'|'||"treatyYear"||'|'||"claimType"||'|'||"pleadingCaption"||'|'||"court"||'|'||"courtCaseNum"||'|'||"insuredServeDate"||'|'||"insurerServeDate"||'|'||"firmServeDate"||'|'||"isFeeDispute"||'|'||"trialDateRange"||'|'||"isJudgmentClaimant"||'|'||"lawDispositionId"||'|'||"lastReserveDate"||'|'||"fileStatus"||'|'||"respAtty"||'|'||"yrsOfPrac"||'|'||"claimantZip"||'|'||"insuredZip"||'|'||"isSuit"||'|'||"isCapRpt"||'|'||"isBench"||'|'||"isReinsuranceNotice"||'|'||"isNoBill"||'|'||"isReinsurerReport"||'|'||"reinsurerReportDate"||'|'||"otherPayment"||'|'||"otherReimburse"||'|'||"benchDate"||'|'||"capRptDate"||'|'||"suitDate"||'|'||"reinsurerReimburse"||'|'||"isClaimRepair"||'|'||"isConflictInterest"||'|'||"isCoverageIssue"||'|'||"isEthicalProblem"||'|'||"isSalvageSubrogation"||'|'||"committeePersonId"||'|'||"treatyId"||'|'||"deductiblePrepay"||'|'||"deductiblePrepayAllocate"||'|'||"subrogationIndemnity"||'|'||"subrogationALAE" from "Claim";
spool off

-- claimcoverage
spool 'z:\claimcoverage.csv'
select "claimId"||'|'||"coverageId"||'|'||"estimateALAE"||'|'||"reserveALAE"||'|'||"paymentALAE"||'|'||"deductibleALAE"||'|'||"incurredDeductibleALAE"||'|'||"netIncurredALAE"||'|'||"grossIncurredALAE"||'|'||"estimateIndemnity"||'|'||"reserveIndemnity"||'|'||"paymentIndemnity"||'|'||"deductibleIndemnity"||'|'||"incurredDeductibleIndemnity"||'|'||"netIncurredIndemnity"||'|'||"grossIncurredIndemnity"||'|'||"deductibleAdvance"||'|'||"deductibleReimburse"||'|'||"deductibleWriteOff"||'|'||"deductibleBalance"||'|'||"deductibleDirect"||'|'||"perClaimLimit"||'|'||"perClaimDeductible" from "ClaimCoverage";
spool off

--claimpayment
spool 'z:\claimpayment.csv'
select "claimPaymentId"||'|'||"claimId"||'|'||"eventId"||'|'||"disbursementId"||'|'||"receiptAllocationId"||'|'||"claimPaymentSeq"||'|'||"paymentALAE"||'|'||"paymentIndemnity"||'|'||"paymentNonPolicyALAE"||'|'||"deductibleALAE"||'|'||"deductibleIndemnity"||'|'||"deductibleAdvance"||'|'||"deductibleDirect"||'|'||"deductibleOverpay"||'|'||"subrogationALAE"||'|'||"subrogationIndemnity"||'|'||"tranCode"||'|'||"descCode"||'|'||"otherReimburse"||'|'||"otherPayment"||'|'||"recoverableALAE"||'|'||"recoverableIndemnity"||'|'||"deductiblePrepayAllocate"  from "ClaimPayment";
spool off

--claimreserve
spool 'z:\claimreserve.csv'
select "claimReserveId"||'|'||"claimId"||'|'||"eventId"||'|'||"claimReserveSeq"||'|'||"estimateALAE"||'|'||"estimateIndemnity"||'|'||"sumEstimateALAE"||'|'||"sumEstimateIndemnity"||'|'||"estimateNonPolicyALAE"||'|'||"sumEstimateNonPolicyALAE" from "ClaimReserve";
spool off

--coverage
spool 'z:\coverage.csv'
select "coverageId"||'|'||"applicationId"||'|'||"applicationLimitId"||'|'||"coverageTypeId"||'|'||"eventId"||'|'||"firmId"||'|'||"historyId"||'|'||"policyId"||'|'||"quoteId"||'|'||"quoteBookId"||'|'||"quoteCoverageId"||'|'||"coverageSeq"||'|'||"coverageLocator"||'|'||"firmName"||'|'||"issueDate"||'|'||"retroactiveDate"||'|'||"perClaimLimit"||'|'||"aggregateLimit"||'|'||"perClaimDeductible"||'|'||"aggregateDeductible"||'|'||"estimateALAE"||'|'||"reserveALAE"||'|'||"paymentALAE"||'|'||"deductibleALAE"||'|'||"incurredDeductibleALAE"||'|'||"netIncurredALAE"||'|'||"grossIncurredALAE"||'|'||"estimateIndemnity"||'|'||"reserveIndemnity"||'|'||"paymentIndemnity"||'|'||"deductibleIndemnity"||'|'||"incurredDeductibleIndemnity"||'|'||"netIncurredIndemnity"||'|'||"grossIncurredIndemnity"||'|'||"deductibleAdvance"||'|'||"deductibleReimburse"||'|'||"deductibleWriteOff"||'|'||"deductibleBalance"||'|'||"deductibleDirect"||'|'||"factorPremium"||'|'||"schedulePremium"||'|'||"excessCoveragePremium"||'|'||"premium"||'|'||"premiumAdjust"||'|'||"financeCharge"||'|'||"financeChargeDue"||'|'||"surplus"||'|'||"transfer"||'|'||"writeOff"||'|'||"dueAmount"||'|'||"receiveAmount"||'|'||"returnAmount"||'|'||"excessPremium"||'|'||"semiFacPremium"||'|'||"trueFacPremium"||'|'||"cancellationCoverageId"||'|'||"premiumReceipt"||'|'||"premiumWriteOff"||'|'||"financeChargeReceipt"||'|'||"financeChargeWriteOff"||'|'||"deductiblePrepay"||'|'||"deductiblePrepayAllocate" from "Coverage";
spool off

--deductiblereimburse
spool 'z:\deductiblereimburse.csv'
select "deductibleReimburseId"||'|'||"eventId"||'|'||"claimId"||'|'||"disbursementId"||'|'||"receiptAllocationId"||'|'||"deductibleReimburseSeq"||'|'||"deductibleReimburse"||'|'||"deductibleWriteOff"||'|'||"deductiblePrepay" from "DeductibleReimburse";
spool off

--disbursement
spool 'z:\disbursement.csv'
select "disbursementId"||'|'||"eventId"||'|'||"vendorId"||'|'||"receiptId"||'|'||"disbursementSeq"||'|'||"checkNumber"||'|'||"referenceNumber"||'|'||"payee"||'|'||"payeeLine2"||'|'||"overpayAmount"||'|'||"receiptAllocationId" from "Disbursement";
spool off

--event
spool 'z:\event.csv'
select "eventId"||'|'||"eventTypeId"||'|'||"eventTypeRuleId"||'|'||"folderId"||'|'||"parentEventId"||'|'||"eventLocator"||'|'||"eventSeq"||'|'||"eventDate"||'|'||"eventName"||'|'||"eventDesc"||'|'||"eventAmount"||'|'||"createDate"||'|'||"createUserId"||'|'||"modifyDate"||'|'||"modifyUserId"||'|'||"notifyDate"||'|'||"notifyUserId"||'|'||"dueDate"||'|'||"dueUserId"||'|'||"completeDate"||'|'||"completeUserId"||'|'||"voidDate"||'|'||"voidUserId"||'|'||"eventStatus"||'|'||"eventURN" from "Event" where "eventDate" > '31-DEC-1999';
spool off

--eventtype
spool 'z:\eventtype.csv'
select "eventTypeId"||'|'||"sourceTypeId"||'|'||"eventTypeLocator"||'|'||"eventTypeName"||'|'||"documentFileName"||'|'||"owner"||'|'||"version"||'|'||"folderPath"||'|'||"letterheadFileName"||'|'||"inactiveDate" from "EventType";
spool off

--glaccount
spool 'z:\glaccount.csv'
select "glAccountId"||'|'||"accountName"||'|'||"accountDesc"||'|'||"accountNote"||'|'||"access"||'|'||"accountType"||'|'||"accountGroup"||'|'||"isObsolete" from "GLAccount";
spool off

--glentry
spool 'z:\glentry.csv'
select "glEntryId"||'|'||"eventId"||'|'||"glBatchId"||'|'||"glEntrySeq"||'|'||"entryDate"||'|'||"entryName"||'|'||"sourceLedger"||'|'||"sourceType"||'|'||"fiscalYear"||'|'||"fiscalPeriod"||'|'||"debitAmount"||'|'||"creditAmount"||'|'||"balanceAmount"||'|'||"groupEventId" from "GLEntry";
spool off

--gltransaction
spool 'z:\gltransaction.csv'
select "glTransactionId"||'|'||"glAccountId"||'|'||"glEntryId"||'|'||"glTransactionSeq"||'|'||"transactionName"||'|'||"transactionReference"||'|'||"transactionAmount" from "GLTransaction";
spool off

--instance
spool 'z:\instance.csv'
select "instanceId"||'|'||"coverageId"||'|'||"eventId"||'|'||"firmId"||'|'||"historyId"||'|'||"cwpTypeId"||'|'||"lawErrorTypeId"||'|'||"lawDispositionId"||'|'||"otherInstanceId"||'|'||"policyId"||'|'||"treatyId"||'|'||"mainLawyerId"||'|'||"instanceSeq"||'|'||"firmName"||'|'||"claimant"||'|'||"claimantRelationship"||'|'||"legalCapacity"||'|'||"adjuster"||'|'||"reportDate"||'|'||"reporterTypeId"||'|'||"coverageDate"||'|'||"occurDateRange"||'|'||"occurDate"||'|'||"knowDateRange"||'|'||"knowDate"||'|'||"openDate"||'|'||"closeDate"||'|'||"reOpenDate"||'|'||"boxNo"||'|'||"archiveNo"||'|'||"mainLawActivityId"||'|'||"mainLawAreaId"||'|'||"mainLawErrorId"||'|'||"lastStatusEventId"||'|'||"watchListCode"||'|'||"estimateGeneralLegal"||'|'||"reserveGeneralLegal"||'|'||"paymentGeneralLegal"||'|'||"incurredGeneralLegal"||'|'||"isFeeDispute"||'|'||"isMedicare" from "Instance";
spool off

--invoice
spool 'z:\invoice.csv'
select "invoiceId"||'|'||"invoiceGroupId"||'|'||"eventId"||'|'||"entityId"||'|'||"invoiceSeq"||'|'||"invoiceNumber"||'|'||"invoiceAmount"||'|'||"invoiceWriteOff"||'|'||"invoiceReceipt" from "Invoice";
spool off

--invoicegroup
spool 'z:\invoicegroup.csv'
select "invoiceGroupId"||'|'||"invoiceGroupTypeId"||'|'||"invoiceGroupNumber"||'|'||"claimId"||'|'||"coverageId"||'|'||"policyId"||'|'||"brokerId"||'|'||"claimPaymentId" from "InvoiceGroup";
spool off

--invoicegrouptype
spool 'z:\invoicegrouptype.csv'
select "invoiceGroupTypeId"||'|'||"invoiceGroupTypeName"||'|'||"invoiceGroupTypeDesc"||'|'||"invoiceGroupTypeNote"||'|'||"categoryType"||'|'||"interestPercent" from "InvoiceGroupType";
spool off

--invoiceitemtype
spool 'z:\invoiceitemtype.csv'
select "invoiceId"||'|'||"itemTypeId"||'|'||"invoiceAmount"||'|'||"invoiceWriteOff"||'|'||"invoiceReceipt" from "InvoiceItemType";
spool off

--invoicereceipt
spool 'z:\invoicereceipt.csv'
select "invoiceId"||'|'||"itemTypeId"||'|'||"receiptAllocationId"||'|'||"invoiceWriteOff"||'|'||"invoiceReceipt"||'|'||"accountingEventId" from "InvoiceReceipt";
spool off

--policy
spool 'z:\policy.csv'
select "policyId"||'|'||"eventId"||'|'||"mainCoverageId"||'|'||"firmId"||'|'||"coverageTypeId"||'|'||"policySeq"||'|'||"policyNumber"||'|'||"policySuffix"||'|'||"firmName"||'|'||"issueDate"||'|'||"effectiveDate"||'|'||"expirationDate"||'|'||"policyRetroactiveDate"||'|'||"cancelEffectiveDate"||'|'||"cancelIssueDate"||'|'||"isPolicyTailCoverage"||'|'||"perClaimLimit"||'|'||"aggregateLimit"||'|'||"perClaimDeductible"||'|'||"aggregateDeductible"||'|'||"initalPaidDate"||'|'||"paidToDate"||'|'||"newReissue"||'|'||"totalLawyerCount"||'|'||"treatyId"||'|'||"factorPremium"||'|'||"schedulePremium"||'|'||"excessCoveragePremium"||'|'||"premium"||'|'||"premiumAdjust"||'|'||"financeCharge"||'|'||"financeChargeDue"||'|'||"surplus"||'|'||"transfer"||'|'||"writeOff"||'|'||"dueAmount"||'|'||"receiveAmount"||'|'||"returnAmount"||'|'||"excessPremium"||'|'||"semiFacPremium"||'|'||"trueFacPremium"||'|'||"estimateALAE"||'|'||"reserveALAE"||'|'||"paymentALAE"||'|'||"deductibleALAE"||'|'||"incurredDeductibleALAE"||'|'||"netIncurredALAE"||'|'||"grossIncurredALAE"||'|'||"estimateIndemnity"||'|'||"reserveIndemnity"||'|'||"paymentIndemnity"||'|'||"deductibleIndemnity"||'|'||"incurredDeductibleIndemnity"||'|'||"netIncurredIndemnity"||'|'||"grossIncurredIndemnity"||'|'||"paymentNonPolicyALAE"||'|'||"deductibleAdvance"||'|'||"deductibleReimburse"||'|'||"deductibleBalance"||'|'||"deductibleDirect"||'|'||"polItem"||'|'||"treatyYear"||'|'||"endorsementCount"||'|'||"deductibleWriteOff"||'|'||"paymentMethod"||'|'||"facultative"||'|'||"cleDisc"||'|'||"premDisc"||'|'||"isJudge"||'|'||"isTail"||'|'||"policyTerm"||'|'||"surplusAmount"||'|'||"policyState"||'|'||"checkamt"||'|'||"priorpay"||'|'||"referralService"||'|'||"expenseAllowance"||'|'||"policyStatus"||'|'||"legFacultative"||'|'||"legCleDisc"||'|'||"legPremDisc"||'|'||"legFirmRating"||'|'||"legClaimDisc"||'|'||"legClaimCost"||'|'||"legDefDis"||'|'||"legRefServ"||'|'||"legPtBase112"||'|'||"legPtBase1324"||'|'||"legPtPa112"||'|'||"legPtPa1324"||'|'||"legPtPa25"||'|'||"legContractBa"||'|'||"legConPaCon"||'|'||"legConPaConSo"||'|'||"legClaimsSur"||'|'||"legTailPrem"||'|'||"legTailDate"||'|'||"paralegalCount"||'|'||"staffCount"||'|'||"contractNumber"||'|'||"cedeCommissionPercent"||'|'||"premiumReceipt"||'|'||"premiumWriteOff"||'|'||"financeChargeReceipt"||'|'||"financeChargeWriteOff"||'|'||"deductiblePrepay"||'|'||"deductiblePrepayAllocate" from "Policy";
spool off

--policyaccounting
spool 'z:\policyaccounting.csv'
select "policyAccountingId"||'|'||"eventId"||'|'||"disbursementId"||'|'||"receiptAllocationId"||'|'||"coverageId"||'|'||"policyId"||'|'||"policyAccountingSeq"||'|'||"factorPremium"||'|'||"schedulePremium"||'|'||"excessCoveragePremium"||'|'||"premium"||'|'||"premiumAdjust"||'|'||"financeCharge"||'|'||"financeChargeDue"||'|'||"surplus"||'|'||"transfer"||'|'||"writeOff"||'|'||"dueAmount"||'|'||"receiveAmount"||'|'||"returnAmount"||'|'||"premiumReceipt"||'|'||"premiumWriteOff"||'|'||"financeChargeReceipt"||'|'||"financeChargeWriteOff"||'|'||"excessPremium"||'|'||"semiFacPremium"||'|'||"trueFacPremium" from "PolicyAccounting";
spool off

--endorsement
spool 'z:\endorsement.csv'
select "endorsementId"||'|'||"coverageId"||'|'||"policyId"||'|'||"endorsementSeq"||'|'||"endorsementNumber"||'|'||"endorsementDesc"||'|'||"effectiveDate"||'|'||"expirationDate"||'|'||"premium"||'|'||"yearsTail" from "Endorsement";
spool off


--05/30/2018

--lawyer
--lawyerstatebar
--quote
--entity
--location
--taillawyer2
--firm
--policylawyercoverage
--endorsement

--policylawyercoverage
spool 'z:\policylawyercoverage.csv'
select "policyId"||'|'||"lawyerId"||'|'||"coverageId"||'|'||"effectiveDate"||'|'||"expirationDate"||'|'||"retroactiveDate"||'|'||"eventId"||'|'||"excludeAfterDate"||'|'||"cancelEffectiveDate"||'|'||"excludeBeforeDate"||'|'||"cancellationCoverageId"||'|'||"policyStatus" from "PolicyLawyerCoverage";
spool off

--lawyer
spool 'z:\lawyer.csv'
select "lawyerId"||'|'||"lawyerSeq"||'|'||"positionCode"||'|'||"inPracticeDuration"||'|'||"inPracticeDate"||'|'||"isUninsurable"||'|'||"lawSchool"||'|'||"barDistrict"||'|'||"county"||'|'||"occupationCode"||'|'||"lawyerCode"||'|'||"mainStateBarId"||'|'||"latestApplicationId"||'|'||"latestCoverageId"||'|'||"latestFirmId"||'|'||"latestPolicyId"||'|'||"latestPolicyStatus"||'|'||"effectiveDate"||'|'||"expirationDate"||'|'||"retroactiveDate"||'|'||"cleCount"||'|'||"lawyerDesignationId" from "Lawyer";
spool off


--lawyerstatebar
spool 'z:\lawyerstatebar.csv'
select "lawyerId"||'|'||"stateBarId"||'|'||"stateBarNumber"||'|'||"barAdmitDate"||'|'||"barYear"||'|'||"isInactive"||'|'||"activityDate" from "LawyerStateBar";
spool off

--quote
spool from 'z:\quote.csv'
select "quoteId"||'|'||"applicationId"||'|'||"eventId"||'|'||"invoiceGroupId"||'|'||"receiptId"||'|'||"mainQuoteBookId"||'|'||"mainQuoteCoverageId"||'|'||"quoteSeq"||'|'||"quoteName"||'|'||"issueDate"||'|'||"expirationDate"||'|'||"interestRate" from "Quote" ;
spool off

--entity
spool 'z:\entity.csv'
select "entityId"||'|'||"eventId"||'|'||"entitySeq"||'|'||"entityLocator"||'|'||"entityNumber"||'|'||"entityName"||'|'||"namespace"||'|'||"namespaceTypeId"||'|'||"entityURL"||'|'||"sourceId"||'|'||"latestHistoryId"||'|'||"mainLocationId"||'|'||"NAMEID"||'|'||"EFFECTIVEDATE" from "Entity";
spool off

--location
spool 'z:\location.csv'
select "locationId"||'|'||"eventId"||'|'||"locationTypeId"||'|'||"mainEntityId"||'|'||"locationSeq"||'|'||"addressLine1"||'|'||"addressLine2"||'|'||"addressLine3"||'|'||"city"||'|'||"county"||'|'||"state"||'|'||"zip"||'|'||"country"||'|'||"telephone"||'|'||"fax"||'|'||"email"||'|'||"effectiveDate"||'|'||"expirationDate" from "Location";
spool off

--taillawyer2
spool 'z:\taillawyer2.csv'
select "policyId"||'|'||"lawyerId"||'|'||"coverageId"||'|'||"effectiveDate"||'|'||"expirationDate"||'|'||"retroactiveDate"||'|'||"excludeBeforeDate"||'|'||"excludeAfterDate"||'|'||"cancelEffectiveDate"||'|'||"eventId"||'|'||"firmId"||'|'||"firmName"||'|'||"policyExpirationDate"||'|'||"perClaimLimit"||'|'||"aggregateLimit"||'|'||"perClaimDeductible"||'|'||"aggregateDeductible"||'|'||"eventTypeId"||'|'||"eventTypeName"||'|'||"policyNumber"||'|'||"applicationId"||'|'||"firmLocator"||'|'||"applicationRatingId"||'|'||"applicationLimitId"||'|'||"firmPremium"||'|'||"premiumPerLawyer1M"||'|'||"premiumPerLawyerXS5M"||'|'||"premiumPerLawyerXS10M"||'|'||"DeductibleFactor"||'|'||"LimitFactor"||'|'||"LawyerFactor"||'|'||"LawyerYIP"||'|'||"LawyerRDI"||'|'||"LawyerActivity"||'|'||"SchdAdjPctCalc"||'|'||"lawyerName"||'|'||"lawyerActivityId"||'|'||"premiumPerLawyer5MNoDeductible"||'|'||"LawyerFactorUsed"||'|'||"tailTerm"||'|'||"quoteId"||'|'||"quoteCoverageId"||'|'||"tailType"||'|'||"oldTailPremium"||'|'||"newTailPremium" from "TailLawyer2" ;
spool off

--firm
spool 'z:\firm.csv'
select "firmId"||'|'||"firmNumber"||'|'||"firmSeq"||'|'||"stateBarNumber"||'|'||"nextEffectiveDate"||'|'||"latestApplicationId"||'|'||"latestInsurerId"||'|'||"initialPolicyId"||'|'||"latestPolicyId"||'|'||"latestPolicyStatus"||'|'||"underwritingStatusId"||'|'||"insureDuration"||'|'||"insureStatusId"||'|'||"isDefensePanel"||'|'||"isUninsurable"||'|'||"visitDate"||'|'||"totalClaimCount"||'|'||"totalLawyerCount"||'|'||"effectiveDate"||'|'||"expirationDate"||'|'||"firmRetroactiveDate"||'|'||"perClaimLimit"||'|'||"aggregateLimit"||'|'||"perClaimDeductible"||'|'||"premium"||'|'||"totalPremium"||'|'||"estimateALAE"||'|'||"reserveALAE"||'|'||"paymentALAE"||'|'||"deductibleALAE"||'|'||"incurredDeductibleALAE"||'|'||"netIncurredALAE"||'|'||"grossIncurredALAE"||'|'||"estimateIndemnity"||'|'||"reserveIndemnity"||'|'||"paymentIndemnity"||'|'||"deductibleIndemnity"||'|'||"incurredDeductibleIndemnity"||'|'||"netIncurredIndemnity"||'|'||"grossIncurredIndemnity"||'|'||"paymentNonPolicyALAE"||'|'||"deductibleAdvance"||'|'||"deductibleReimburse"||'|'||"deductibleBalance"||'|'||"deductibleDirect"||'|'||"deductibleWriteOff"||'|'||"officeCount"||'|'||"maxLimit"||'|'||"maxDeductible"||'|'||"minDeductible"||'|'||"claimSurcharge"||'|'||"claimDiscount"||'|'||"deductiblePrepay"||'|'||"deductiblePrepayAllocate" from "Firm";
spool off




--
-- ratinglawyercrosstab
spool 'z:\ratinglawyercrosstab'
select "quoteBookId"||'|'||"lawyerId"||'|'||"ContLegalEdCredit"||'|'||"LawyerActivity"||'|'||"LawyerFactor"||'|'||"LawyerRDI"||'|'||"LawyerYIP"||'|'||"DefReimbCoverage"||'|'||"LawyerActivityAdjusted" from "RatingLawyerCrosstab";
spool off

-- ratingcrosstab
spool 'z:\ratingcrosstab.csv'
select "quoteBookId"||'|'||"applicationId"||'|'||"firmId"||'|'||"effectiveDate""AbstTitleInsAgent"||'|'||"Base"||'|'||"BodilyInjuryDefense"||'|'||"BodilyInjuryPlaintiff"||'|'||"ClaimsSurcharge"||'|'||"CorpBusOrg"||'|'||"DefReimbCoverage"||'|'||"DocketControl"||'|'||"DocketControlAmount"||'|'||"DomesticRelations"||'|'||"EstateProbateTrust"||'|'||"ExcessOfLossPremium"||'|'||"ExclVicariousLiab"||'|'||"ExclWordFormer"||'|'||"ExcludeCapacities"||'|'||"ExcludeOtherEntities"||'|'||"Firm"||'|'||"FirmRDI"||'|'||"FlatPremCalc"||'|'||"IntellProp"||'|'||"Manuscript"||'|'||"OtherAdjustment"||'|'||"RealEstate"||'|'||"RegulatoryExclusion"||'|'||"RestrictCvgExcess5M"||'|'||"SECCoverage"||'|'||"SchdAdjPctCalc"||'|'||"SeverityIndex"||'|'||"SeverityIndexZero"||'|'||"Term"||'|'||"WorkersCompDefense"||'|'||"WorkersCompPlaintiff"||'|'||"YearsInsured" from "RatingCrosstab";
spool off

-- ratinglimitcrosstab
spool 'z:\ratinglimitcrosstab.csv'
select "quoteBookId"||'|'||"quoteCoverageId"||'|'||"ContLegalEdCalc"||'|'||"ContinuityAmountCalc"||'|'||"Deductible"||'|'||"DeductibleFactor"||'|'||"FactorPremCalc"||'|'||"Limit"||'|'||"LimitFactor"||'|'||"LimitAgg"||'|'||"PRM2M"||'|'||"PRM2MADJ"||'|'||"PRM3M"||'|'||"PRM3MADJ"||'|'||"Prem5mCalc"||'|'||"PremCalc"||'|'||"PremXS5mMinCalc"||'|'||"PremXS10mCalc" from "RatingLimitCrosstab";
spool off

-- quotecoverage
spool 'z:\quotecoverage.csv'
select "quoteCoverageId"||'|'||"coverageTypeId"||'|'||"eventId"||'|'||"quoteId"||'|'||"quoteBookId"||'|'||"lawyerSet"||'|'||"applicationLimitId"||'|'||"parentQuoteCoverageId"||'|'||"quoteCoverageSeq"||'|'||"retroactiveDate"||'|'||"coverageTerm"||'|'||"perClaimLimit"||'|'||"aggregateLimit"||'|'||"perClaimDeductible"||'|'||"aggregateDeductible"||'|'||"premium"||'|'||"surplus"||'|'||"initialPayment"||'|'||"subsequentPayment"||'|'||"factorPremium"||'|'||"schedulePremium"||'|'||"excessPremium"||'|'||"semiFacPremium"||'|'||"trueFacPremium"||'|'||"policyAccountingId" from "QuoteCoverage"
spool off
	 
-- applicationlimit
spool 'z:\applicationlimit.csv'
select "applicationLimitId"||'|'||"applicationId"||'|'||"coverageTypeId"||'|'||"applicationLimitSeq"||'|'||"coverageTerm"||'|'||"perClaimLimit"||'|'||"aggregateLimit"||'|'||"perClaimDeductible"||'|'||"aggregateDeductible"||'|'||"paCon"||'|'||"paConS"||'|'||"quoteInstruction" from "ApplicationLimit";
spool off

-- application
spool 'z:\application.csv'
select "applicationId"||'|'||"eventId"||'|'||"firmId"||'|'||"locationId"||'|'||"policyId"||'|'||"coverageId"||'|'||"applicationSeq"||'|'||"firmName"||'|'||"receiveDate"||'|'||"signDate"||'|'||"effectiveDate"||'|'||"policyNumber"||'|'||"policyTerm"||'|'||"applicationStatus"||'|'||"lawyerPrincipalCount"||'|'||"lawyerOfCounselCount"||'|'||"lawyerOtherCount"||'|'||"staffCount"||'|'||"staffPartTimeCount"||'|'||"lawClerkCount"||'|'||"abstracterCount"||'|'||"accountantCount"||'|'||"paralegalCount"||'|'||"claimAgainstCount"||'|'||"organizationType"||'|'||"secIncome"||'|'||"suitCount"||'|'||"abstracterTitleRevenue"||'|'||"retroactiveDate"||'|'||"isBackUpLawyer"||'|'||"isDisbarred"||'|'||"isDocketControl"||'|'||"isIncidentKnowledge"||'|'||"isOfficeShare"||'|'||"isRefuseCoverage"||'|'||"isSuccessor"||'|'||"insureDuration"||'|'||"totalLawyerCount"||'|'||"priorLawyerCount"||'|'||"cleDiscount"||'|'||"surplus"||'|'||"basicCat"||'|'||"isJudge"||'|'||"isRenewal"||'|'||"referralService"||'|'||"applicationTypeId" from "Application";
spool off


