
/*
*/

--------------------------------------------------------------------------------
-- CSV and spooling setup
-- sets up csv file properly 

set sqlformat

set colsep '|'
set sqlprompt ''
set pagesize 0
set trimspool on
set headsep off
set pages 0
set feed off
set long 2000
set longchunksize 2000
--------------------------------------------------------------------------------

/*
-- claim
spool 'z:\claim.csv'
select "claimId"||'|'||"claimNumber"||'|'||"claimSeq"||'|'||"coverageDate"||'|'||"claimMadeDate"||'|'||"diaryDate"||'|'||"diaryAction"||'|'||"inceptionDate"||'|'||"perClaimLimit"||'|'||"aggregateLimit"||'|'||"perClaimDeductible"||'|'||"isTail"||'|'||"totalLawyerCount"||'|'||"estimateALAE"||'|'||"reserveALAE"||'|'||"paymentALAE"||'|'||"deductibleALAE"||'|'||"incurredDeductibleALAE"||'|'||"netIncurredALAE"||'|'||"grossIncurredALAE"||'|'||"estimateIndemnity"||'|'||"reserveIndemnity"||'|'||"paymentIndemnity"||'|'||"deductibleIndemnity"||'|'||"incurredDeductibleIndemnity"||'|'||"netIncurredIndemnity"||'|'||"grossIncurredIndemnity"||'|'||"paymentNonPolicyALAE"||'|'||"deductibleAdvance"||'|'||"deductibleReimburse"||'|'||"deductibleWriteOff"||'|'||"deductibleBalance"||'|'||"deductibleDirect"||'|'||"treatyYear"||'|'||"claimType"||'|'||"pleadingCaption"||'|'||"court"||'|'||"courtCaseNum"||'|'||"insuredServeDate"||'|'||"insurerServeDate"||'|'||"firmServeDate"||'|'||"isFeeDispute"||'|'||"trialDateRange"||'|'||"isJudgmentClaimant"||'|'||"lawDispositionId"||'|'||"lastReserveDate"||'|'||"fileStatus"||'|'||"respAtty"||'|'||"yrsOfPrac"||'|'||"claimantZip"||'|'||"insuredZip"||'|'||"isSuit"||'|'||"isCapRpt"||'|'||"isBench"||'|'||"isReinsuranceNotice"||'|'||"isNoBill"||'|'||"isReinsurerReport"||'|'||"reinsurerReportDate"||'|'||"otherPayment"||'|'||"otherReimburse"||'|'||"benchDate"||'|'||"capRptDate"||'|'||"suitDate"||'|'||"reinsurerReimburse"||'|'||"isClaimRepair"||'|'||"isConflictInterest"||'|'||"isCoverageIssue"||'|'||"isEthicalProblem"||'|'||"isSalvageSubrogation"||'|'||"committeePersonId"||'|'||"treatyId"||'|'||"deductiblePrepay"||'|'||"deductiblePrepayAllocate"||'|'||"subrogationIndemnity"||'|'||"subrogationALAE" from "Claim";
spool off

-- claimcoverage
spool 'z:\claimcoverage.csv'
select "claimId"||'|'||"coverageId"||'|'||"estimateALAE"||'|'||"reserveALAE"||'|'||"paymentALAE"||'|'||"deductibleALAE"||'|'||"incurredDeductibleALAE"||'|'||"netIncurredALAE"||'|'||"grossIncurredALAE"||'|'||"estimateIndemnity"||'|'||"reserveIndemnity"||'|'||"paymentIndemnity"||'|'||"deductibleIndemnity"||'|'||"incurredDeductibleIndemnity"||'|'||"netIncurredIndemnity"||'|'||"grossIncurredIndemnity"||'|'||"deductibleAdvance"||'|'||"deductibleReimburse"||'|'||"deductibleWriteOff"||'|'||"deductibleBalance"||'|'||"deductibleDirect"||'|'||"perClaimLimit"||'|'||"perClaimDeductible" from "ClaimCoverage";
spool off

--claimpayment
spool 'z:\claimpayment.csv'
select "claimPaymentId"||'|'||"claimId"||'|'||"eventId"||'|'||"disbursementId"||'|'||"receiptAllocationId"||'|'||"claimPaymentSeq"||'|'||"paymentALAE"||'|'||"paymentIndemnity"||'|'||"paymentNonPolicyALAE"||'|'||"deductibleALAE"||'|'||"deductibleIndemnity"||'|'||"deductibleAdvance"||'|'||"deductibleDirect"||'|'||"deductibleOverpay"||'|'||"subrogationALAE"||'|'||"subrogationIndemnity"||'|'||"tranCode"||'|'||"descCode"||'|'||"otherReimburse"||'|'||"otherPayment"||'|'||"recoverableALAE"||'|'||"recoverableIndemnity"||'|'||"deductiblePrepayAllocate"  from "ClaimPayment";
spool off

--claimreserve
spool 'z:\claimreserve.csv'
select "claimReserveId"||'|'||"claimId"||'|'||"eventId"||'|'||"claimReserveSeq"||'|'||"estimateALAE"||'|'||"estimateIndemnity"||'|'||"sumEstimateALAE"||'|'||"sumEstimateIndemnity"||'|'||"estimateNonPolicyALAE"||'|'||"sumEstimateNonPolicyALAE" from "ClaimReserve";
spool off

--coverage
spool 'z:\coverage.csv'
select "coverageId"||'|'||"applicationId"||'|'||"applicationLimitId"||'|'||"coverageTypeId"||'|'||"eventId"||'|'||"firmId"||'|'||"historyId"||'|'||"policyId"||'|'||"quoteId"||'|'||"quoteBookId"||'|'||"quoteCoverageId"||'|'||"coverageSeq"||'|'||"coverageLocator"||'|'||"firmName"||'|'||"issueDate"||'|'||"retroactiveDate"||'|'||"perClaimLimit"||'|'||"aggregateLimit"||'|'||"perClaimDeductible"||'|'||"aggregateDeductible"||'|'||"estimateALAE"||'|'||"reserveALAE"||'|'||"paymentALAE"||'|'||"deductibleALAE"||'|'||"incurredDeductibleALAE"||'|'||"netIncurredALAE"||'|'||"grossIncurredALAE"||'|'||"estimateIndemnity"||'|'||"reserveIndemnity"||'|'||"paymentIndemnity"||'|'||"deductibleIndemnity"||'|'||"incurredDeductibleIndemnity"||'|'||"netIncurredIndemnity"||'|'||"grossIncurredIndemnity"||'|'||"deductibleAdvance"||'|'||"deductibleReimburse"||'|'||"deductibleWriteOff"||'|'||"deductibleBalance"||'|'||"deductibleDirect"||'|'||"factorPremium"||'|'||"schedulePremium"||'|'||"excessCoveragePremium"||'|'||"premium"||'|'||"premiumAdjust"||'|'||"financeCharge"||'|'||"financeChargeDue"||'|'||"surplus"||'|'||"transfer"||'|'||"writeOff"||'|'||"dueAmount"||'|'||"receiveAmount"||'|'||"returnAmount"||'|'||"excessPremium"||'|'||"semiFacPremium"||'|'||"trueFacPremium"||'|'||"cancellationCoverageId"||'|'||"premiumReceipt"||'|'||"premiumWriteOff"||'|'||"financeChargeReceipt"||'|'||"financeChargeWriteOff"||'|'||"deductiblePrepay"||'|'||"deductiblePrepayAllocate" from "Coverage";
spool off

--deductiblereimburse
spool 'z:\deductiblereimburse.csv'
select "deductibleReimburseId"||'|'||"eventId"||'|'||"claimId"||'|'||"disbursementId"||'|'||"receiptAllocationId"||'|'||"deductibleReimburseSeq"||'|'||"deductibleReimburse"||'|'||"deductibleWriteOff"||'|'||"deductiblePrepay" from "DeductibleReimburse";
spool off

--disbursement
spool 'z:\disbursement.csv'
select "disbursementId"||'|'||"eventId"||'|'||"vendorId"||'|'||"receiptId"||'|'||"disbursementSeq"||'|'||"checkNumber"||'|'||"referenceNumber"||'|'||"payee"||'|'||"payeeLine2"||'|'||"overpayAmount"||'|'||"receiptAllocationId" from "Disbursement";
spool off
*/
--event
spool 'z:\event.csv'
select "eventId"||'|'||"eventTypeId"||'|'||"eventTypeRuleId"||'|'||"folderId"||'|'||"parentEventId"||'|'||"eventLocator"||'|'||"eventSeq"||'|'||"eventDate"||'|'||"eventName"||'|'||"eventDesc"||'|'||"eventNote"||'|'||"eventAmount"||'|'||"createDate"||'|'||"createUserId"||'|'||"modifyDate"||'|'||"modifyUserId"||'|'||"notifyDate"||'|'||"notifyUserId"||'|'||"dueDate"||'|'||"dueUserId"||'|'||"completeDate"||'|'||"completeUserId"||'|'||"voidDate"||'|'||"voidUserId"||'|'||"eventStatus"||'|'||"eventURN" from "Event";
spool off
