/*  
--  ACTIVITY & COMMUNICATIONS
*/
--------------------------------------------------------------------------------
/*  UTILITIES
--  These are support views used to assemble activity data from LAW.
*/


-----------------------
--Used for document file name extraction
--process >> log into legaolas, cd /usr/local/tomcat/webapps/out,  LS -1 > /home/fzillner/dirlisting.txt, import txt file to windows via filezilla, via sqldeveloper load drilisting.txt into DocumentListing
-- 
-- select * from "EventType" where "documentFileName" is not NULL order by "eventTypeId";
drop table "DocumentListing";
create table "DocumentListing" (
    "filename" varchar2(40) NOT NULL,
    constraint "PK_DocumentListing" unique ("filename")
);

create or replace view "DocumentListingView" as 
    select 
        D."filename" as "filename",
        SUBSTR(D."filename",-3, 3) as "filetype",
        CASE
            WHEN SUBSTR(D."filename", 1,2) = 'ET' THEN SUBSTR(D."filename", 1, (INSTR(D."filename", '.') -1 )) --SUBSTR(D."filename", 1,
            ELSE NULL
        END as "eventId"
    from "DocumentListing" D
;

select * from "DocumentListingView";
------------------------

--------------------------------------------------------------------------------
/*
    ISI_ActivityView
    a_activity
    
    Dependancies: 
    
    NOTES:
     
    History
    03/02/2018  FZ  Created
*/
create or replace view "ISI_ActivityView" as
    -- claim diary
    select
        I."instanceId" as "INSTANCEID",
        I."firmId" as "FIRMID",
        P."policyNumber" as "POLICYNUMBER",
        E."eventId" as "activitykey",
        0 as "vnumber",
        TO_CHAR(E."createDate", 'YYYYMMDD') as "ventrydate",
        CASE
            WHEN E."modifyDate" is not NULL THEN TO_CHAR(E."modifyDate", 'YYYYMMDD') 
            ELSE TO_CHAR(E."createDate", 'YYYYMMDD')
        END as "vmodifieddate",
        CASE
            WHEN E."modifyDate" is not NULL THEN TO_CHAR(E."modifyDate", 'HHMMSS')
            ELSE TO_CHAR(E."createDate", 'HHMMSS') 
        END as "vmodifiedtime",
        CASE
            WHEN E."createUserId" = 'Joyce' THEN 'jneumaier'
            WHEN E."createUserId" = 'Matt' THEN 'mbeier'
            WHEN E."createUserId" = 'Katja' THEN 'kkunzke'
            WHEN E."createUserId" = 'Brian' THEN 'banderson'
            WHEN E."createUserId" = 'Sally' THEN 'sanderson'
            ELSE '>>unknown<<'||COALESCE(E."createUserId", 'NULL') --QFC, NDZ, WHD
        END as "vusermappingkey",
        'ClaimsDiary' as "activitytypekey",
        CASE
            WHEN E."eventStatus" = 'Pending' THEN 1 --'InProgress'
            WHEN E."eventStatus" = 'Void' THEN 2 --'Suspended'  --???
            WHEN E."eventStatus" = 'Complete' THEN 1 --'Complete" per LP 07/06/2018
            ELSE 0 --'Complete'
        END as "status",
        --E."eventName" as "subject",
        'Claims Diary' as "subject",
        '4112' as "sbuid",
        E."eventDesc" as "detaildescription",
        CASE
            WHEN E."modifyDate" is NOT NULL THEN TO_CHAR(E."modifyDate", 'YYYYMMDD')
            ELSE TO_CHAR(E."createDate", 'YYYYMMDD')
        END as "receiveddate",
        TO_CHAR(E."completeDate", 'YYYYMMDD') as "resolveddate",
        E."completeUserId" as "resolvedbymappingkey",
        CASE
            WHEN E."dueUserId" = 'Joyce' THEN 'jneumaier'
            WHEN E."dueUserId" = 'Matt' THEN 'mbeier'
            WHEN E."dueUserId" = 'Katja' THEN 'kkunzke'
            WHEN E."dueUserId" = 'Brian' THEN 'banderson'
            WHEN E."dueUserId" = 'Sally' THEN 'sanderson'
            ELSE '>>unknown<<'||COALESCE(E."dueUserId", 'NULL') --QFC, NDZ, WHD
        END as "assignedtokey",
        1 as "priority", --1-normal, 0-low, 2-high, 3-urgent
        NULL as "percentcomplete",
        NULL as "activequeuekey",
        CASE
            WHEN E."dueUserId" = 'Joyce' THEN 'jneumaier'
            WHEN E."dueUserId" = 'Matt' THEN 'mbeier'
            WHEN E."dueUserId" = 'Katja' THEN 'kkunzke'
            WHEN E."dueUserId" = 'Brian' THEN 'banderson'
            WHEN E."dueUserId" = 'Sally' THEN 'sanderson'
            ELSE '>>unknown<<'||COALESCE(E."dueUserId", 'NULL') --QFC, NDZ, WHD
        END as "activeownerkey",
        NULL as "projectedstartdate",
        NULL as "projectedstarttime",
        NULL as "projectedenddate",
        NULL as "projectedendtime",
        COALESCE(TO_CHAR(E."dueDate", 'YYYYMMDD'), TO_CHAR(SYSDATE, 'YYYMMDD')) as "duedate",
        NULL as "taglist",
        1 as "activitycommtype", --1=task, 2=email, 4=Note
        CASE
            WHEN E."eventStatus" = 'Pending' THEN 1
            ELSE 1
        END as "vstatus",  --changed to try and fix diary issues, default to 1=pending
        NULL as "reminderdate",
        NULL as "remindertime",
        NULL as "reminderkey",
        NULL as "notetypeid",
        0 as "privateind",
        NULL as "conversionserver",
        NULL as "sequencenumber"
    from (select * from "Event" where "eventTypeId" = 'ClaimDiary' and "eventTypeRuleId" = 'ClaimEvent' and "eventStatus" not in ('Void')) E
        left join "Claim" C on C."claimId" = SUBSTR(E."eventName", 1, 7)
        left join "Instance" I on I."instanceId" = SUBSTR(E."eventName", 1, 7)
            left join "Policy" P on P."policyId" = I."policyId"
    -- SCOPE
    where P."effectiveDate" > '31-DEC-2005'
      and I."coverageDate" > '31-DEC-2006'
    ---where C."claimId" like '15-0%' or C."claimId" like '14-0%' or C."claimId" like '13-0%'
 --   order by SUBSTR(E."eventName", 1, 7)
--;
/*
  union all
    -- incident diary
    select
        I."instanceId" as "INSTANCEID",
        I."firmId" as "FIRMID",
        P."policyNumber" as "POLICYNUMBER",
        E."eventId" as "activitykey",
        0 as "vnumber",
        TO_CHAR(E."createDate", 'YYYYMMDD') as "ventrydate",
        CASE
            WHEN E."modifyDate" is not NULL THEN TO_CHAR(E."modifyDate", 'YYYYMMDD') 
            ELSE TO_CHAR(E."createDate", 'YYYYMMDD')
        END as "vmodifieddate",
        CASE
            WHEN E."modifyDate" is not NULL THEN TO_CHAR(E."modifyDate", 'HHMMSS')
            ELSE TO_CHAR(E."createDate", 'HHMMSS') 
        END as "vmodifiedtime",
        CASE
            WHEN E."createUserId" = 'Joyce' THEN 'jneumaier'
            WHEN E."createUserId" = 'Matt' THEN 'mbeier'
            WHEN E."createUserId" = 'Katja' THEN 'kkunzke'
            WHEN E."createUserId" = 'Brian' THEN 'banderson'
            WHEN E."createUserId" = 'Sally' THEN 'sanderson'
            ELSE '>>unknown<<'||COALESCE(E."createUserId", 'NULL') --QFC, NDZ, WHD
        END as "vusermappingkey",
        'ClaimsDiary' as "activitytypekey",
        CASE
            WHEN E."eventStatus" = 'Pending' THEN 1 --'InProgress'
            WHEN E."eventStatus" = 'Void' THEN 2 --'Suspended'  --???
            WHEN E."eventStatus" = 'Complete' THEN 1 --'Complete" per LP 07/06/2018
            ELSE 0 --'Complete'
        END as "status",
        --E."eventName" as "subject",
        'Claims Diary' as "subject",
        '4112' as "sbuid",
        E."eventDesc" as "detaildescription",
        CASE
            WHEN E."modifyDate" is NOT NULL THEN TO_CHAR(E."modifyDate", 'YYYYMMDD')
            ELSE TO_CHAR(E."createDate", 'YYYYMMDD')
        END as "receiveddate",
        TO_CHAR(E."completeDate", 'YYYYMMDD') as "resolveddate",
        E."completeUserId" as "resolvedbymappingkey",
        CASE
            WHEN E."dueUserId" = 'Joyce' THEN 'jneumaier'
            WHEN E."dueUserId" = 'Matt' THEN 'mbeier'
            WHEN E."dueUserId" = 'Katja' THEN 'kkunzke'
            WHEN E."dueUserId" = 'Brian' THEN 'banderson'
            WHEN E."dueUserId" = 'Sally' THEN 'sanderson'
            ELSE '>>unknown<<'||COALESCE(E."dueUserId", 'NULL') --QFC, NDZ, WHD
        END as "assignedtokey",
        1 as "priority", --1-normal, 0-low, 2-high, 3-urgent
        NULL as "percentcomplete",
        NULL as "activequeuekey",
        CASE
            WHEN E."dueUserId" = 'Joyce' THEN 'jneumaier'
            WHEN E."dueUserId" = 'Matt' THEN 'mbeier'
            WHEN E."dueUserId" = 'Katja' THEN 'kkunzke'
            WHEN E."dueUserId" = 'Brian' THEN 'banderson'
            WHEN E."dueUserId" = 'Sally' THEN 'sanderson'
            ELSE '>>unknown<<'||COALESCE(E."dueUserId", 'NULL') --QFC, NDZ, WHD
        END as "activeownerkey",
        NULL as "projectedstartdate",
        NULL as "projectedstarttime",
        NULL as "projectedenddate",
        NULL as "projectedendtime",
        COALESCE(TO_CHAR(E."dueDate", 'YYYYMMDD'), TO_CHAR(SYSDATE, 'YYYMMDD')) as "duedate",
        NULL as "taglist",
        1 as "activitycommtype", --1=task, 2=email, 4=Note
        CASE
            WHEN E."eventStatus" = 'Pending' THEN 1
            ELSE 0
        END as "vstatus",
        NULL as "reminderdate",
        NULL as "remindertime",
        NULL as "reminderkey",
        NULL as "notetypeid",
        0 as "privateind",
        NULL as "conversionserver",
        NULL as "sequencenumber"
    from (select * from "Event" where "eventTypeId" = 'ClaimDiary' and "eventTypeRuleId" = 'ClaimEvent' and "eventStatus" not in ('Void')) E
        left join "Incident" C on C."incidentId" = SUBSTR(E."eventName", 1, 7)
        left join "Instance" I on I."instanceId" = SUBSTR(E."eventName", 1, 7)
            left join "Policy" P on P."policyId" = I."policyId"
    -- SCOPE
    where P."effectiveDate" > '31-DEC-2005'
      and I."coverageDate" > '31-DEC-2006'
  */
  union all
--create or replace view "ISI_ActivityDocumentView" as
    select
        NULL as "INSTANCEID",
        P."firmId" as "FIRMID",
        P."policyNumber" as "POLICYNUMBER",
        E."eventId" as "activitykey",
        0 as "vnumber",
        TO_CHAR(E."createDate", 'YYYYMMDD') as "ventrydate",
        CASE
            WHEN E."modifyDate" is not NULL THEN TO_CHAR(E."modifyDate", 'YYYYMMDD') 
            ELSE TO_CHAR(E."createDate", 'YYYYMMDD')
        END as "vmodifieddate",
        CASE
            WHEN E."modifyDate" is not NULL THEN TO_CHAR(E."modifyDate", 'HHMMSS')
            ELSE TO_CHAR(E."createDate", 'HHMMSS') 
        END as "vmodifiedtime",
        CASE
            WHEN E."createUserId" = 'Joyce' THEN 'jneumaier'
            WHEN E."createUserId" = 'Matt' THEN 'mbeier'
            WHEN E."createUserId" = 'Katja' THEN 'kkunzke'
            WHEN E."createUserId" = 'Brian' THEN 'banderson'
            WHEN E."createUserId" = 'Sally' THEN 'sanderson'
            WHEN E."createUserId" = 'Jane' THEN 'jharder'
            WHEN E."createUserId" = 'Joe' THEN 'jmccarthy'
            WHEN E."createUserId" = 'Stephanie' THEN 'swilliams'
            WHEN E."createUserId" = 'Linda' THEN 'lbudziszewski'
            WHEN E."createUserId" = 'Megan' THEN 'mobrien'
            WHEN E."createUserId" = 'Tom' THEN 'twatson'
            ELSE '>>unknown<<'||COALESCE(E."createUserId", 'NULL') --QFC, NDZ, WHD
        END as "vusermappingkey",
        'PolicyDocument' as "activitytypekey",
        CASE
            WHEN E."eventStatus" = 'Pending' THEN 1 --'InProgress'
            WHEN E."eventStatus" = 'Void' THEN 2 --'Suspended'  --???
            ELSE 0 --'Complete'
        END as "status",
        E."eventTypeId" as "subject",
        '4112' as "sbuid",
        E."eventDesc" as "detaildescription",
        CASE
            WHEN E."modifyDate" is NOT NULL THEN TO_CHAR(E."modifyDate", 'YYYYMMDD')
            ELSE TO_CHAR(E."createDate", 'YYYYMMDD')
        END as "receiveddate",
        TO_CHAR(E."completeDate", 'YYYYMMDD') as "resolveddate",
        E."completeUserId" as "resolvedbymappingkey",
        CASE
            WHEN E."createUserId" = 'Joyce' THEN 'jneumaier'
            WHEN E."createUserId" = 'Matt' THEN 'mbeier'
            WHEN E."createUserId" = 'Katja' THEN 'kkunzke'
            WHEN E."createUserId" = 'Brian' THEN 'banderson'
            WHEN E."createUserId" = 'Sally' THEN 'sanderson'
            WHEN E."createUserId" = 'Jane' THEN 'jharder'
            WHEN E."createUserId" = 'Joe' THEN 'jmccarthy'
            WHEN E."createUserId" = 'Stephanie' THEN 'swilliams'
            WHEN E."createUserId" = 'Linda' THEN 'lbudziszewski'
            WHEN E."createUserId" = 'Megan' THEN 'mobrien'
            WHEN E."createUserId" = 'Tom' THEN 'twatson'
            ELSE '>>unknown<<'||COALESCE(E."createUserId", 'NULL') --QFC, NDZ, WHD
        END as "assignedtokey",
        1 as "priority", --1-normal, 0-low, 2-high, 3-urgent
        NULL as "percentcomplete",
        NULL as "activequeuekey",
        CASE
            WHEN E."createUserId" = 'Joyce' THEN 'jneumaier'
            WHEN E."createUserId" = 'Matt' THEN 'mbeier'
            WHEN E."createUserId" = 'Katja' THEN 'kkunzke'
            WHEN E."createUserId" = 'Brian' THEN 'banderson'
            WHEN E."createUserId" = 'Sally' THEN 'sanderson'
            WHEN E."createUserId" = 'Jane' THEN 'jharder'
            WHEN E."createUserId" = 'Joe' THEN 'jmccarthy'
            WHEN E."createUserId" = 'Stephanie' THEN 'swilliams'
            WHEN E."createUserId" = 'Linda' THEN 'lbudziszewski'
            WHEN E."createUserId" = 'Megan' THEN 'mobrien'
            WHEN E."createUserId" = 'Tom' THEN 'twatson'
            ELSE '>>unknown<<'||COALESCE(E."createUserId", 'NULL') --QFC, NDZ, WHD
        END as "activeownerkey",
        NULL as "projectedstartdate",
        NULL as "projectedstarttime",
        NULL as "projectedenddate",
        NULL as "projectedendtime",
        COALESCE(TO_CHAR(E."dueDate", 'YYYYMMDD'), TO_CHAR(SYSDATE, 'YYYMMDD')) as "duedate",
        NULL as "taglist",
        4 as "activitycommtype", --1=task, 2=email, 4=Note
        CASE
            WHEN E."eventStatus" = 'Pending' THEN 1
            ELSE 0
        END as "vstatus",
        NULL as "reminderdate",
        NULL as "remindertime",
        NULL as "reminderkey",
        10 as "notetypeid",
        0 as "privateind",
        NULL as "conversionserver",
        NULL as "sequencenumber"
    from (select * from "Event" where "eventTypeId" in ('DeclarationPage', 'MP-POL', 'RF-POL', 'AR-POL', 'CertificateInsurance', 'WLMEND02(01/14)', 'WLMEND03(01/14)', 'WLMEND28(01/14)') ) E
        left join (select * from "EventReferenceView" where "referenceEventTypeId" = 'Firm') ERV1 on ERV1."eventId" = E."eventId"
            left join "Entity" EN on EN."eventId" = ERV1."referenceEventId"
        left join (select * from "EventReferenceView" where "referenceEventTypeId" = 'PolicyBinding') ERV2 on ERV2."eventId" = E."eventId"
            left join "Policy" P on P."eventId" = ERV2."referenceEventId"
    --- DOUMENTS SCOPE ---
    where P."policyNumber" = '1710135'
    --where P."firmId" = 'FRANWI'
;

--- incident diaries
create or replace view "ISI_ActivityINCView" as
select 
        I."instanceId" as "INSTANCEID",
        I."firmId" as "FIRMID",
        P."policyNumber" as "POLICYNUMBER",
        E."eventId" as "activitykey",
        0 as "vnumber",
        TO_CHAR(E."createDate", 'YYYYMMDD') as "ventrydate",
        CASE
            WHEN E."modifyDate" is not NULL THEN TO_CHAR(E."modifyDate", 'YYYYMMDD') 
            ELSE TO_CHAR(E."createDate", 'YYYYMMDD')
        END as "vmodifieddate",
        CASE
            WHEN E."modifyDate" is not NULL THEN TO_CHAR(E."modifyDate", 'HHMMSS')
            ELSE TO_CHAR(E."createDate", 'HHMMSS') 
        END as "vmodifiedtime",
        CASE
            WHEN E."createUserId" = 'Joyce' THEN 'jneumaier'
            WHEN E."createUserId" = 'Matt' THEN 'mbeier'
            WHEN E."createUserId" = 'Katja' THEN 'kkunzke'
            WHEN E."createUserId" = 'Brian' THEN 'banderson'
            WHEN E."createUserId" = 'Sally' THEN 'sanderson'
            ELSE '>>unknown<<'||COALESCE(E."createUserId", 'NULL') --QFC, NDZ, WHD
        END as "vusermappingkey",
        'ClaimsDiary' as "activitytypekey",
        CASE
            WHEN E."eventStatus" = 'Pending' THEN 1 --'InProgress'
            WHEN E."eventStatus" = 'Void' THEN 2 --'Suspended'  --???
            WHEN E."eventStatus" = 'Complete' THEN 1 --'Complete" per LP 07/06/2018
            ELSE 0 --'Complete'
        END as "status",
        'Claims Diary' as "subject",
        '4112' as "sbuid",
        E."eventDesc" as "detaildescription",
        CASE
            WHEN E."modifyDate" is NOT NULL THEN TO_CHAR(E."modifyDate", 'YYYYMMDD')
            ELSE TO_CHAR(E."createDate", 'YYYYMMDD')
        END as "receiveddate",
        TO_CHAR(E."completeDate", 'YYYYMMDD') as "resolveddate",
        E."completeUserId" as "resolvedbymappingkey",
        CASE
            WHEN E."dueUserId" = 'Joyce' THEN 'jneumaier'
            WHEN E."dueUserId" = 'Matt' THEN 'mbeier'
            WHEN E."dueUserId" = 'Katja' THEN 'kkunzke'
            WHEN E."dueUserId" = 'Brian' THEN 'banderson'
            WHEN E."dueUserId" = 'Sally' THEN 'sanderson'
            ELSE '>>unknown<<'||COALESCE(E."dueUserId", 'NULL') --QFC, NDZ, WHD
        END as "assignedtokey",
        1 as "priority", --1-normal, 0-low, 2-high, 3-urgent
        NULL as "percentcomplete",
        NULL as "activequeuekey",
        CASE
            WHEN E."dueUserId" = 'Joyce' THEN 'jneumaier'
            WHEN E."dueUserId" = 'Matt' THEN 'mbeier'
            WHEN E."dueUserId" = 'Katja' THEN 'kkunzke'
            WHEN E."dueUserId" = 'Brian' THEN 'banderson'
            WHEN E."dueUserId" = 'Sally' THEN 'sanderson'
            ELSE '>>unknown<<'||COALESCE(E."dueUserId", 'NULL') --QFC, NDZ, WHD
        END as "activeownerkey",
        NULL as "projectedstartdate",
        NULL as "projectedstarttime",
        NULL as "projectedenddate",
        NULL as "projectedendtime",
        COALESCE(TO_CHAR(E."dueDate", 'YYYYMMDD'), TO_CHAR(SYSDATE, 'YYYMMDD')) as "duedate",
        NULL as "taglist",
        1 as "activitycommtype", --1=task, 2=email, 4=Note
        /*
        CASE
            WHEN E."eventStatus" = 'Pending' THEN 1
            ELSE 0
        END as "vstatus",
        */
        1 as "vstatus",
        NULL as "reminderdate",
        NULL as "remindertime",
        NULL as "reminderkey",
        NULL as "notetypeid",
        0 as "privateind",
        NULL as "conversionserver",
        NULL as "sequencenumber"
from "EventReferenceView" ER
    inner join "Instance" I on I."eventId" = ER."referenceEventId"
        left join "Policy" P on P."policyId" = I."policyId"
        left join "Event" E on E."eventId" = ER."eventId"
where ER."eventTypeRuleId" = 'IncidentEvent' 
  and ER."eventTypeId" = 'ClaimDiary'
;







select * from "ISI_ActivityView" where "activitycommtype" = 4;

/*--- Testing ---*/
select * from "Event" where "eventId" = 'ET001147488';
select * from "ISI_ActivityView"; where "subject" like '%|%';
select * from "Event" where "eventId" = 'ET000427579';
update "Event" set "eventDesc" = 'Claim Diary- claim insd lost ; close 9/13' where "eventId" = 'ET000427579' and "eventDesc" = 'Claim Diary- claim insd "lost ; close 9/13';

select unique("eventTypeId") from "Event" where "eventTypeId" like '%Diary%';


/*
    ISI_ActivityLinkView
    l_linkdocument
    
    Dependancies: 
    
    NOTES:
     
    History
    04/12/2018  FZ  Created
*/
create or replace view "ISI_ActivityLinkView" as 
    --client link
    select
        AV."activitykey" as "activitykey",
        0 as "activityvnumber",
        AV."FIRMID" as "linkedentitykey",
        0 as "linkedentityvnumber",
        1 as "linktype",
        AV."sbuid" as "sbuid",
        NULL as "polconversionref",
        NULL as "poltransactiontype",
        NULL as "poltransactioneffectivedate",
        NULL as "poltransactionseqno",
        NULL as "conversionserver",
        NULL as "sequencenumber"
    from "ISI_ActivityView" AV
 union all
    --claim link
    select
        AV."activitykey" as "activitykey",
        0 as "activityvnumber",
        AV."INSTANCEID" as "linkedentitykey",
        0 as "linkedentityvnumber",
        200 as "linktype",
        AV."sbuid" as "sbuid",
        NULL as "polconversionref",
        NULL as "poltransactiontype",
        NULL as "poltransactioneffectivedate",
        NULL as "poltransactionseqno",
        NULL as "conversionserver",
        NULL as "sequencenumber"
    from (select * from "ISI_ActivityView" where "activitycommtype" = 1) AV
 union all
    --policy related to claims
    select
        AV."activitykey" as "activitykey",
        0 as "activityvnumber",
        NULL as "linkedentitykey",
        NULL as "linkedentityvnumber",
        100 as "linktype",
        AV."sbuid" as "sbuid",
        CV."polconversionref" as "polconversionref",
        CV."poltranstype" as "poltransactiontype",
        CV."poltranseffdate" as "poltransactioneffectivedate",
        CV."poltransseqno" as "poltransactionseqno",
        NULL as "conversionserver",
        NULL as "sequencenumber"
    from (select * from "ISI_ActivityView" where "activitycommtype" = 1) AV
        left join "Policy" P on P."policyId" = AV."POLICYNUMBER"
        inner join "ISIClaimToPolicyView" CTP on CTP."instanceId" = AV."INSTANCEID"
        right join "ISI_ClaimView" CV on CV."claimmappingkey" = AV."INSTANCEID"
 union all 
     --policy or firm related to documents
    select
        AV."activitykey" as "activitykey",
        0 as "activityvnumber",
        CASE
            WHEN PP."transactioneffectivedate" is NULL THEN AV."FIRMID"
            --ELSE AV."POLICYNUMBER"
            ELSE NULL
        END as "linkentitykey",
        NULL as "linkedentityvnumber",
        CASE
            WHEN PP."transactioneffectivedate" is NULL THEN 1
            ELSE 100 
        END as "linktype",
        AV."sbuid" as "sbuid",
        PP."conversionreference" as "polconversionref",
        PP."transactiontype" as "poltransactiontype",
        PP."transactioneffectivedate" as "poltransactioneffectivedate",
        PP."transactionseqno" as "poltransactionseqno",
        NULL as "conversionserver",
        NULL as "sequencenumber"
    from (select * from "ISI_ActivityView" where "activitycommtype" = 4) AV
        left join (select * from "ISI_Policy" where "EVENTTYPEID" = 'PolicyIssue' and "transactioneffectivedate" = "policyeffectivedate" and "transactiontype" in ('NEW', 'REN', 'RWR', 'RWN')) PP on PP."policykey" = AV."POLICYNUMBER"
--order by "activitykey", "linkedentitykey"   
;

select * from "ISI_ActivityView" where /*"activitycommtype" = 1 and*/ "FIRMID" = 'MOHRAN';
select * from "ISI_ActivityLinkView" where "polconversionref" = 'MOHRAN_1992_2013';
--------------------------------------------------------------------------------
/*
    ISI_ActivityDocumentsView
    l_activitydocuments
    
    Dependancies: 
    
    NOTES:
     
    History
    05/31/2018  FZ  Created
*/
create or replace view "ISI_ActivityDocumentsView" as 
    -- Policy Documents --
    select  
        AV."activitykey" as "activitykey",
        AV."sbuid" as "sbuid",
        'D:\DocumentsToConvert' as "fullpath",  --default 
        DLV."filename" as "filename",
        ---CASE  --"sysPolicyDec", "sysPolicyDocument", "sysPolicyLetter", "sysPolicyMisc", "sysQuoteDocument"
        CASE
            WHEN AV."subject" in ('QL-POL') THEN 'sysQuoteDocument'
            WHEN AV."subject" like '%-POL' THEN 'sysPolicyLetter'
            WHEN AV."subject" in ('CertificateInsurance', 'DeclarationPage') THEN 'sysPolicyDec'
            ELSE 'sysPolicyDocument'
        END as "categoryid",  --NEED TO REFINE BASED ON eventTypeId
        AV."vusermappingkey" as "vusermappingkey",
        AV."POLICYNUMBER"||'_'||AV."subject" as "description",
        AV."ventrydate" as "ventrydate",
        '000000' as "ventrytime",
        NULL as "conversionserver",
        NULL as "sequencenumber",
        0 as "activityvnumber"
    from (select * from "ISI_ActivityView" where "activitycommtype" = 4) AV
        left join "DocumentListingView" DLV on DLV."eventId" = AV."activitykey"
        
    --- SCOPE ---
    where AV."POLICYNUMBER" = '1710135'
;


