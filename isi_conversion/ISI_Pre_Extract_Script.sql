/*
    ISI_Pre_Extract_Script.sql
    
    Prepares data for LAW to ISI conversion extract
    
    History
    04/04/2018  FZ  Assembled from prior scripts
*/


---*** LAW Data Correrction for ISI Data Conversion***---
--Endorsement table effectiveDate (HH24:MI:SS) were not 12:00:00 so multiple transactioneffectivedate records were appearing in ISI_PolicyView causing duplicates, so set HH24:MI:SS to 00:00:00.

select to_char("effectiveDate", 'DD-MON-YYYY HH:MI:SS AM') as effDate, P.* from "Policy" P where to_char("effectiveDate", 'HH:MI:SS') <> '12:00:00';
update "Policy"
set "effectiveDate" = to_date(to_char("effectiveDate", 'DD-MON-YYYY')||' 00:00:00', 'DD-MON-YYYY HH24:MI:SS')
where to_char("effectiveDate", 'HH24:MI:SS') <> '00:00:00';
select to_char("expirationDate", 'DD-MON-YYYY HH:MI:SS AM') as expDate, P.* from "Policy" P where to_char("expirationDate", 'HH:MI:SS') <> '12:00:00';
update "Policy"
set "expirationDate" = to_date(to_char("expirationDate", 'DD-MON-YYYY')||' 00:00:00', 'DD-MON-YYYY HH24:MI:SS')
where to_char("expirationDate", 'HH24:MI:SS') <> '00:00:00';

select to_char(FI."effectiveDate", 'DD-MON-YYYY HH:MI:SS AM') as effDate, FI.* from "FirmInsurer" FI where to_char(FI."effectiveDate", 'HH:MI:SS') <> '12:00:00';
update "FirmInsurer"
set "effectiveDate" = to_date(to_char("effectiveDate", 'DD-MON-YYYY')||' 00:00:00', 'DD-MON-YYYY HH24:MI:SS')
where to_char("effectiveDate", 'HH24:MI:SS') <> '00:00:00';
update "FirmInsurer"
set "expirationDate" = to_date(to_char("expirationDate", 'DD-MON-YYYY')||' 00:00:00', 'DD-MON-YYYY HH24:MI:SS')
where to_char("expirationDate", 'HH24:MI:SS') <> '00:00:00';

select to_char("effectiveDate", 'DD-MON-YYYY HH:MI:SS AM') as effDate, E.* from "Endorsement" E where to_char("effectiveDate", 'HH:MI:SS') <> '12:00:00';
update "Endorsement"
set "effectiveDate" = to_date(to_char("effectiveDate", 'DD-MON-YYYY')||' 00:00:00', 'DD-MON-YYYY HH24:MI:SS')
where to_char("effectiveDate", 'HH24:MI:SS') <> '00:00:00';


-- tail lawyer view -> table  This is done for performance purposes
drop table "TailLawyer2";
create table "TailLawyer2" as 
    (select * from "TailLawyerView");