/*******************************************************************************
	ISI Conversion Scripts
	Scripts used to extract LAW data into ISI interim conversion tables
	
	Naming convention = 'ISI_'+ ISI tablename minus leading alpha underscore + 'View'
	
	Created: FZ	07/07/2017
	Revised: FZ 07/18/2017 - Added ISI_ContactRelationshipsView
*******************************************************************************/

/******************
**  COMMON
******************/

-- E_Contact
-- ISI_ContactView
create or replace view "ISI_ContactView" as 
  select
    E."entityId" as "entitykey",
    0 as "entitykeyvnumber",
    NULL as "sbuid",  --deprecated per documentation
    CASE 
      WHEN O."organizationId" is NULL and P."lastName" is not NULL THEN P."firstName"
      ELSE NULL
    END as "firstname",
    CASE 
      WHEN O."organizationId" is NULL THEN P."middleName"
      ELSE NULL
    END as "middlename",
    CASE 
      WHEN O."organizationId" is NULL and P."firstName" is not NULL THEN P."lastName"
      ELSE NULL
    END as "lastname",
    CASE 
      --WHEN O."organizationId" is NULL and P."title" = 'Atty.' THEN 'Atty'
      WHEN O."organizationId" is NULL and (P."firstName" is not NULL or P."lastName" is not NULL) THEN REPLACE(P."title", '.', '')
      ELSE NULL
    END as "nameprefix",
    CASE 
      WHEN O."organizationId" is NULL THEN REPLACE(P."suffix", '.', '')
      ELSE NULL
    END as "namesuffix",
    CASE 
      WHEN O."organizationId" is NULL THEN P."jobTitle"
      ELSE NULL
    END as "jobtitle",
    CASE 
      WHEN O."organizationId" is NULL and (P."firstName" is not NULL and P."lastName" is not NULL) THEN E."entityName"
      ELSE NULL
    END as "fullname",  --maybe concatenate from Person
    CASE 
      WHEN O."organizationId" is not NULL then E."entityName"
      WHEN O."organizationId" is NULL and (P."firstName" is NULL or P."lastName" is NULL) THEN E."entityName"
      ELSE NULL
    END as "companyname",
    1 as "preferredhtc",
    CASE 
      WHEN O."organizationId" is NULL and (P."firstName" is not NULL and P."lastName" is not NULL) THEN 1  --there are person recs for companies????
      ELSE 0
    END as "isindividual",
    CASE
      WHEN (O."organizationId" is not NULL) or (O."organizationId" is NULL and (P."firstName" is NULL or P."lastName" is NULL)) THEN
        CASE 
          WHEN O."organizationLegalForm" = 'P' THEN 'PR'
          WHEN O."organizationLegalForm" = 'LLP' THEN 'LLP' 
          WHEN O."organizationLegalForm" = 'PSC' THEN 'PC'
          WHEN O."organizationLegalForm" = 'I' THEN 'SP'
          WHEN O."organizationLegalForm" = 'LLC' THEN 'LLC'
          WHEN O."organizationLegalForm" = 'GP' THEN 'GP'
          WHEN O."organizationLegalForm" is NULL and substr(E."entityName", -3, 3) = 'LLP' THEN 'LLP'
          WHEN O."organizationLegalForm" is NULL and substr(E."entityName", -3, 3) = 'LLC' THEN 'LLC'
          ELSE 'CP'
        END 
    END as "contacttype", 
    NULL as "birthdate",
    NULL as "gender",
    NULL as "maritalstatus",
    NULL as "comments",
    NULL as "conversionserver",
    NULL as "sequencenumber",
    NULL as "linkedentitykey",
    /* --09/19/2017 removed since WILMIC does use specific "legal name"
    CASE 
      WHEN O."organizationId" is not NULL THEN E."entityLocator"
      ELSE NULL
    END as "linkedentitykey", --entityLocator
    */
    0 as "linkedentitykeyvnumber",
    NULL as "ssn",
    NULL as "hicn",
    NULL as "fein",
    NULL as "tin",
    'EN' as "preferredlanguage"
  from "Entity" E
    left join "Organization" O on O."organizationId" = E."entityId"
    left join "Person" P on P."personId" = E."entityId"
  where E."entityId" <> 'LFUNKNOWN'
;

select * from "ISI_ContactView";


/******************************************************************************/
-- e_address
-- ISI_AddressVew
-- 10/27/2017-added Unknown values for ISI required fields (ie: street)
create or replace view "ISI_AddressView" as 
 select
    EL."entityId"||'_'||L."locationId" as "addresskey",
    EL."entityId" as "entitykey",
    0 as "entitykeyvnumber",
    CASE
      WHEN EL."mainLocationId" is NULL THEN 'C'
      WHEN EL."mainLocationId" is not NULL and L."locationTypeId" = 'Firm' THEN 'P'
      ELSE 'CL' --locationTypeId = Alternate, Branch or Other
    END as "entitytype",  --default to C=Contact...P=Policy, CL=Claim
    NULL as "sbuid",  --deprecated per documentation
    'F' as "addresskind", -- default to F=Full
    CASE 
      WHEN C."contactTypeId" = 'Firm' THEN 1
      ELSE 0
    END as "mailingaddressind",
    CASE
        WHEN L."locationTypeId" = 'Firm' THEN 1
        WHEN L."locationTypeId" in ('Alternate', 'Branch') THEN 4
        ELSE 99
    END as "addresstype",
    /*  -- removed 07/05/2018
    CASE
        WHEN F."firmId" is not NULL THEN C."contactSalutation" 
        ELSE NULL
    END as "incareof",  -- for firms s/b firm contact
    */
    NULL as "incareof",
    NULL as "addressname",
    CASE
      WHEN substr("addressLine3", 1, 2) in ('PO', 'P.', 'P ', 'Po') THEN NULL
      ELSE "addressLine3"
    END as "buildingname",  --ADD: suite, apt buildingname (ie: Suite 200, MI Building)
    CASE
      WHEN substr("addressLine3", 1, 2) in ('PO', 'P.', 'P ', 'Po') THEN LTRIM(REGEXP_SUBSTR(L."addressLine3", '[^x]+$')) 
      ELSE NULL
    END as "pobox",
    NULL as "streetnumber",
    CASE
        WHEN L."addressLine1" is NULL and L."addressLine2" is NULL THEN 'Unknown Street'
        ELSE L."addressLine1" 
    END as "street",
    NULL as "streetdirection",
    CASE
        WHEN LOWER(SUBSTR(L."addressLine2", 1, 5)) = 'suite' THEN 'Suite'
        WHEN LOWER(SUBSTR(L."addressLine2", 1, 3)) = 'apt' THEN 'Apt'
        ELSE NULL
    END as "unitdesignator",
    CASE
        WHEN LOWER(SUBSTR(L."addressLine2", 1, 5)) = 'suite' THEN LTRIM(REGEXP_REPLACE(SUBSTR(L."addressLine2", 6, 30), '[.#]', ''))
        WHEN LOWER(SUBSTR(L."addressLine2", 1, 3)) = 'ste' THEN LTRIM(REGEXP_REPLACE(SUBSTR(L."addressLine2", 4, 30), '[.#]', ''))
        WHEN LOWER(SUBSTR(L."addressLine2", 1, 3)) = 'apt' THEN LTRIM(REGEXP_REPLACE(SUBSTR(L."addressLine2", 4, 30), '[.#]', ''))
        ELSE NULL
    END as "unitnumber",
    CASE
        WHEN LOWER(SUBSTR(L."addressLine2", 1, 5)) = 'suite' OR LOWER(SUBSTR(L."addressLine2", 1, 3)) = 'apt' THEN NULL
        ELSE "addressLine2"
    END as "addressline2",
    NULL as "lot",
    NULL as "concession",
    NULL as "lotpart",
    NULL as "site",
    NULL as "compartment",
    NULL as "quadrant",
    NULL as "section",
    NULL as "township",
    NULL as "range",
    NULL as "meridian",
    NULL as "routetype",
    NULL as "routenumber",
    NULL as "stationtype",
    NULL as "stationnumber",
    NULL as "countycode",
    L."county" as "countyname",
    CASE
        WHEN L."city" is NULL THEN 'Unknown City'
        ELSE L."city" 
    END as "city",
    CASE
      WHEN L."state" is NULL and L."country" is NULL THEN 'WI'
      WHEN L."country" is NULL THEN UPPER(L."state")
      ELSE NULL
    END as "regioncode",
    CASE
        WHEN S."stateName" is NOT NULL THEN S."stateName" 
        ELSE 'Wisconsin'
    END as "regionname",
    CASE
        WHEN L."zip" is NULL THEN '00000'
        ELSE L."zip" 
    END as "zippostal",
    CASE
      WHEN L."country" is NULL THEN 'US'
      ELSE L."country"
    END as "countrycode",
    CASE 
        WHEN L."country" = 'GB' THEN 'United Kingdom'
        ELSE 'United States' 
    END as "countryname",
    NULL as "customformattedaddress",  --not sure what this is 
    CASE 
      WHEN E."modifyDate" is NOT NULL THEN TO_CHAR(E."modifyDate", 'YYYYMMDD')
      WHEN E."createDate" is NOT NULL THEN TO_CHAR(E."createDate", 'YYYYMMDD')
      ELSE '20180901'
    END as "modifieddate",
    NULL as "wweobjectid",
    NULL as "conversionserver",
    NULL as "sequencenumber",
    NULL as "addrnumsuffix",
    NULL as "streettype",
    NULL as "traveldirection",
    NULL as "milemarker",
    NULL as "exitnumber",
    NULL as "streettype2",
    NULL as "traveldirection2",
    NULL as "interstreetname",
    NULL as "latitudedegree",
    NULL as "latitudeminute",
    NULL as "latitudesecond",
    NULL as "longitudedegree",
    NULL as "longitudeminute",
    NULL as "longitudesecond",
    NULL as "adjacentstreetind",
    NULL as "adjacentstreetnumber",
    NULL as "adjacentstreet",
    NULL as "adjacentstreetdirection"
  from (select * from "Entity" where "entityId" <> 'LFUNKNOWN') EL
    left join "Firm" F on F."firmId" = EL."entityId"
    left join "Location" L on L."locationId" = EL."mainLocationId"
        left join (select * from "Contact" where "contactTypeId" = 'Firm') C on C."locationId" = L."locationId"
        left join "State" S on S."stateId" = L."state"
        left join "Event" E on E."eventId" = L."eventId"
  order by EL."entityId"
;

select * from "Entity" where "entityId" in ('AAGARO', 'AAGAROBW');
select * from "Firm" where "firmId"  in ('AAGARO', 'AAGAROBW');
select * from "Lawyer" where "lawyerId" in ('AAGARO', 'AAGAROBW');
select * from "EntityLocation" where "entityId" in ('AAGARO', 'AAGAROBW');
 

--testing
select * from "ISI_AddressView";
select "entitykey", "entitykeyvnumber", "entitytype", "mailingaddressind",  count(*) from "ISI_AddressView" group by "entitykey", "entitykeyvnumber", "entitytype", "mailingaddressind" having count(*) > 1;
select * from "ISI_AddressView" where "entitykey" like 'MCDO%';

select * from "ISI_AddressView" order by "entitykey";
select * from "ISI_AddressView" where "unitdesignator" is not NULL order by "entitykey";
select count(*) from "ISI_AddressView";
select count(*) from "Location";
select * from "Contact" order by "entityId";
select * from "ISI_AddressView" where "countrycode" <> 'US';
--problem resolution
select * from "ISI_AddressView" where "entitykey" in ('GRIMKIM_','SOLHBI', 'EY000202874');
select * from "Entity" where "entityId" in ('GRIMKIM_', 'SOLHBI', 'EY000202874');
select * from "EntityLocation" where "entityId" = 'GRIMKIM_';
select * from "Location" where "locationId" in ('LFSOLHBI', 'LN000576487');
select * from "Entity" where "entityId" in ('ABTSNICR', 'BRAZPO', 'HILLGL', 'LUMMCH', 'ABTSNI');
select * from "EntityLocation" where "entityId" in ('ABTSNICR', 'BRAZPO', 'HILLGL', 'LUMMCH', 'ABTSNI');
select * from "Location" where "locationId" in ('LN13963135', 'LFLUMMCH', 'LFBRAZPO', 'LFHILLGL');

ABTSNICR	LN13963135
ABTSNICR	LFLUMMCH
ABTSNICR	LFBRAZPO
ABTSNICR	LFHILLGL

/******************************************************************************/
-- e_phonenumbers
-- ISI_PhoneNumbersView
--  Location
select * from "Location";
create or replace view "ISI_PhoneView" as 
    select 
        L."locationId"||L."telephone"||'.1' as "contactmethodkey",
        EL."entityId" as "entitykey",
        0 as "entitykeyvnumber",
        NULL as "entitytype",  --deprecated per documentation
        NULL as "sbuid",  --deprecated per documentation
        CASE
            WHEN L."locationTypeId" <> 'Firm' THEN 0
            ELSE 1
        END as "primaryind",
        1 as "htctype",
        SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(L."telephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),1,3) as "areacode",
        SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(L."telephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),4,3)||'-'||
            SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(L."telephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),7,4) as "phonenumber",
        NULL as "extension",
        SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(L."telephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),1,3)||'-'||
          SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(L."telephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),4,3)||'-'||
          SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(L."telephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),7,4) as "formattedhtc",
        NULL as "conversionserver",
        NULL as "sequencenumber"
    from "EntityLocation" EL
        left join "Location" L on L."locationId" = EL."locationId"
    where L."telephone" is not NULL 
 union all
  select 
    L."locationId"||L."fax" as "contactmethodkey",
    L."mainEntityId" as "entitykey",
    0 as "entitykeyvnumber",
    NULL as "entitytype",  --deprecated per documentation
    NULL as "sbuid",  --deprecated per documentation
    0 as "primaryind",
    2 as "htctype",
    SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(L."fax", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),1,3) as "areacode",
    SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(L."fax", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),4,3)||'-'||
      SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(L."fax", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),7,4) as "phonenumber",
    NULL as "extension",
    SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(L."fax", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),1,3)||'-'||
      SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(L."fax", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),4,3)||'-'||
      SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(L."fax", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),7,4) as "formattedhtc",
    NULL as "conversionserver",
    NULL as "sequencenumber"
  from "Location" L
  where L."fax" is not NULL
 union all
  select
    P."personId"||P."homeTelephone"||'.5' as "contactmethodkey",
    P."personId" as "entitykey",
    0 as "entitykeyvnumber",
    NULL as "entitytype",  --deprecated per documentation
    NULL as "sbuid",  --deprecated per documentation
    0 as "primaryind",
    5 as "htctype",
    SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(P."homeTelephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),1,3) as "areacode",
    SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(P."homeTelephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),4,3)||'-'||
      SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(P."homeTelephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),7,4) as "phonenumber",
    NULL as "extension",
    SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(P."homeTelephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),1,3)||'-'||
      SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(P."homeTelephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),4,3)||'-'||
      SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(P."homeTelephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),7,4) as "formattedhtc",
    NULL as "conversionserver",
    NULL as "sequencenumber"
  from "Person" P 
  where P."homeTelephone" is not NULL
 union all
  select
    P."personId"||P."workTelephone" as "contactmethodkey",
    P."personId" as "entitykey",
    0 as "entitykeyvnumber",
    NULL as "entitytype",  --deprecated per documentation
    NULL as "sbuid",  --deprecated per documentation
    0 as "primaryind",
    1 as "htctype",
    SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(P."workTelephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),1,3) as "areacode",
    SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(P."workTelephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),4,3)||'-'||
      SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(P."workTelephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),7,4) as "phonenumber",
    NULL as "extension",
    SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(P."workTelephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),1,3)||'-'||
      SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(P."workTelephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),4,3)||'-'||
      SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(P."workTelephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),7,4) as "formattedhtc",
    NULL as "conversionserver",
    NULL as "sequencenumber"
  from "Person" P 
  where P."workTelephone" is not NULL
 union all
  select
    P."personId"||P."mobileTelephone" as "contactmethodkey",
    P."personId" as "entitykey",
    0 as "entitykeyvnumber",
    NULL as "entitytype",  --deprecated per documentation
    NULL as "sbuid",  --deprecated per documentation
    0 as "primaryind",
    8 as "htctype",
    SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(P."mobileTelephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),1,3) as "areacode",
    SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(P."mobileTelephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),4,3)||'-'||
      SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(P."mobileTelephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),7,4) as "phonenumber",
    NULL as "extension",
    SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(P."mobileTelephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),1,3)||'-'||
      SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(P."mobileTelephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),4,3)||'-'||
      SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(P."mobileTelephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),7,4) as "formattedhtc",
    NULL as "conversionserver",
    NULL as "sequencenumber"
  from "Person" P 
  where P."mobileTelephone" is not NULL
;
select * from "ISI_PhoneView";
select * from "Firm" where "firmId" = 'PETEJO';

select * from "Entity" where "entityId" in ('PETEJO', 'LCWOLVMA');
select * from "Location" where "locationId" in ('LFPETEJO', 'LCPETEJO');
select * from "EntityLocation" where "entityId" = 'PETEJO';
select * from "EntityLocation" where "locationId" = 'LFPETEJO';
select * from "Location" where "locationTypeId" = 'Branch';
select * from "ISI_PhoneView" where "entitykey" = 'PETEJO';
select unique("locationTypeId") from "Location";
/******************************************************************************/
-- e_emailandwebsites
-- ISI_EmailWebsitesView
--  Entity, Location, Person
create or replace view "ISI_EmailWebsiteView" as 
  select
    E."eventId"||'.10' as "contactmethodkey",
    E."entityId" as "entitykey",
    0 as "entitykeyvnumber",
    NULL as "entitytype",  --deprecated per documentation
    NULL as "sbuid",  --deprecated per documentation
    0 as "primaryind",
    10 as "htctype", 
    CASE
      WHEN ((SUBSTR(E."entityURL",1,3) <> 'www') and (SUBSTR(E."entityURL",1,4) <> 'http')) THEN 'www.'||REPLACE(E."entityURL", ',', '.')
      ELSE REPLACE(E."entityURL", ',', '.')
    END as "emailandweb", 
    NULL as "conversionserver",
    NULL as "sequencenumber"
  from "Entity" E
  where E."entityURL" is not NULL
 --union all
 
 union all
  select 
    P."personId"||'.3' as "contactmethodkey",
    P."personId" as "entitykey",
    0 as "entitykeyvnumber",
    NULL as "entitytype",  --deprecated per documentation
    NULL as "sbuid",  --deprecated per documentation
    0 as "primaryind",
    3 as "htctype", 
    CASE
        WHEN P."email" LIKE '%@%' THEN  REPLACE(P."email", ',', '.') 
        ELSE NULL
    END as "emailandweb",  -- NEED: to remove non-email addresses - No email, unsubscribe, unlisted
    NULL as "conversionserver",
    NULL as "sequencenumber"
  from "Person" P
  where P."email" like '%@%' -- is not NULL
 union all
  select 
    P."personId"||'.7' as "contactmethodkey",
    P."personId" as "entitykey",
    0 as "entitykeyvnumber",
    NULL as "entitytype",  --deprecated per documentation
    NULL as "sbuid",  --deprecated per documentation
    0 as "primaryind",
    7 as "htctype", 
    REPLACE(P."homeEmail", ',', '.') as "emailandweb", 
    NULL as "conversionserver",
    NULL as "sequencenumber"
  from "Person" P
  where P."homeEmail" is not NULL
 union all
  select 
    L."eventId"||'.3' as "contactmethodkey",
    L."mainEntityId" as "entitykey",
    0 as "entitykeyvnumber",
    NULL as "entitytype",  --deprecated per documentation
    NULL as "sbuid",  --deprecated per documentation
    0 as "primaryind",
    3 as "htctype", 
    CASE
        WHEN L."email" like '%@%' THEN REPLACE(L."email", ',', '.') 
        ELSE NULL
    END as "emailandweb",  -- NEED: to remove non-email addresses - No email, unsubscribe, unlisted
    NULL as "conversionserver",
    NULL as "sequencenumber"
  from "Location" L
  where L."email" like '%@%' --is not NULL
;

select * from "ISI_EmailWebsiteView";
select * from "ISI_EmailWebsiteView"  where "htctype" = 10 and "emailandweb" like '%@%';
select unique("emailandweb") from "ISI_EmailWebsiteView"  where "htctype" = 3 and "emailandweb" not like '%@%';
select * from "ISI_EmailWebsiteView"  where "htctype" = 3 and "emailandweb" not like '%@%' and "emailandweb" like '%.com';


/******************************************************************************/
/*
-- e_contactaccounts
-- ISI_ContactAccountsView
-- EFT*

    NOTE:  Account data is bogus until prior to going live.
*/
create or replace view "ISI_ContactAccountsView" as 
    select
        EI."eftIndividualId" as "entitykey",
        0 as "entitykeyvnumber",
        NULL as "entitytype",
        NULL as "sbuid",
        1 as "primaryind",
        '001' as "accounttype",
        '614' as "institutionnumber",  --NEEDS REAL DATA ?????
        --'**********'||SUBSTR(EI."accountNumber", -4,4) as "accountnumber",  --NEEDS REAL DATA
        EI."accountNumber" as "accountnumber",
        NULL as  "billingdaypreferred",
        'S' as "withdrawaltypecd",
        'USD' as "withdrawalcurrencycd",
        EI."individualName" as "accountholdername",
        EI."eftDepositFinancialInstId" as "transitnumber",
        NULL as "accountexpirymonth",
        NULL as "accountexpiryyear",
        '614' as "banknumber",  ---?????
        NULL as "secondaccountholdername",
        NULL as "conversionserver",
        NULL as "sequencenumber",
        NULL as "description",
        'B' as "creditcardflag",
        CASE
            WHEN EI."eftTransactionCodeId" = '37' THEN 'PA'
            ELSE 'CA'
        END as "accountusage",
        NULL as "branchcontactentitykey",
        NULL as "branchcontactentitykeyvnumber"
    from "EFTIndividual" EI
;
-------------------- DO THIS LATER SINCE SENSITIVE DATA -----------------------

--******************************************************************************
-- e_license
-- ISI_LicenseView
--  LawyerStateBar
select * from "LawyerStateBar";
create or replace view "ISI_LicenseView" as 
-- lawyer -> firm 
  select
    LSB."lawyerId" as "licensekey",
    LSB."lawyerId" as "entitykey",
    0 as "entitykeyvnumber",
    'BN' as "licensetype",
    'US' as "licensecountry",
    'WI' as "licensejurisdiction",
    '-' as "licenseclass",
    LSB."stateBarNumber" as "licensenumber",
    CASE
        WHEN LSB."barAdmitDate" is not NULL THEN TO_CHAR(LSB."barAdmitDate", 'YYYYMMDD')
        ELSE '20200101'
    END as "licensedate",  
    1 as "currentind",  -- controlled by state bar
    'A' as "licensestatus", -- controlled by state bar
    NULL as "licensestatus_reason",
    NULL as "licensestatus_startdate",
    NULL as "licensestatus_enddate",
    NULL as "specialtrainingind",
    NULL as "specialtrainingdate"
  from "LawyerStateBar" LSB
;
select * from "ISI_LicenseView" where "licensekey" = 'EY000205194';
select * from "ISI_ContactView" where "entitykey" = 'EY000205194';
/******************************************************************************/
-- e_contactrelationships
-- ISI_ContactRelationshipView
--
-- History:

create or replace view "ISI_ContactRelationshipView" as
  select
    L."lawyerId" as "contactentitykey",
    0 as "contactentitykeyvnumber",
    L."latestFirmId" as "relatedentitykey",
    0 as "relatedentitykeyvnumber",
    'Firm' as "relatedcontacttype",  --client, vendor, broker (aon)
    'Lawyer-Firm' as "relationshiptype",
    NULL as "description",
    TO_CHAR(EV."eventDate",'YYYYMMDD') as "startdate",
    NULL as "enddate", --default to NULL
    NULL as "conversionserver",
    NULL as "sequencenumber"
  from "Lawyer" L
    left join "Entity" E on E."entityId" = L."lawyerId"
      left join "Event" EV on EV."eventId" = E."eventId"
;
select * from "ISI_ContactRelationshipView";

/******************************************************************************/
-- ct_lawyercontactdtls_1      [11/07/2017]
-- ISI_LawyerContactDetailsView
--
create or replace view "ISI_LawyerContactDetailsView" as
    select
        L."lawyerId" as "entitykey122",
        0 as "vnumber122",
        4112 as "sbuid",
        CASE
            WHEN L."inPracticeDate" is not NULL THEN TO_CHAR(L."inPracticeDate", 'YYYYMMDD')
            WHEN LSB."barAdmitDate" is not NULL THEN TO_CHAR(LSB."barAdmitDate", 'YYYYMMDD')
            ELSE '20200101'  -- DEFAULT... both L.inPracticeDate and LSB.barAdmitDate are null
        END as "inpracticedate"
        --L."inPracticeDuration" as "inpracticeyears"  
    from "Lawyer" L
        left join "LawyerStateBar" LSB on LSB."lawyerId" = L."lawyerId"
    order by L."lawyerId"
; 
select * from "ISI_LawyerContactDetailsView";

select LSB."barAdmitDate", L.* from "Lawyer" L left join  "LawyerStateBar" LSB on LSB."lawyerId" = L."lawyerId" order by L."lawyerId";

/******************************************************************************/
select * from "ISI_LawyerContactDetailsView";
select * from "Contact";
select * from "Entity";
select * from "Person" where "homeTelephone" is not NULL;
select * from "Person" where "workTelephone" is not NULL;
select * from "Person" where "mobileTelephone" is not NULL;
select * from "Person" where "pager" is not NULL;
select * from "Person" where "homeEmail" is not NULL;
select * from "Person" where "email" is not NULL;
select * from "Location" where "email" is not NULL;
select * from "Lawyer";
select * from "Firm";
/******************************************************************************/


