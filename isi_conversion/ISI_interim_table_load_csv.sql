/*
-- ISI_interim_table_load_csv.sql
--
-- loads Oracle LAW data using ISI extract views which are then converted to csv files.
-- NOTE: 
--      CSV files assumed to have field names list as record 1 (header)
--  	This script is run via 
--
-- created:	FZ	08/15/2017
-- Notes:
--	10/30/2017	FZ	e_address interim documentation and postgres database don't match.  Database contains deprecated fields-sbuid, 
--	11/07/2017	FZ	added ct_lawyercontatdtls_1
--	02/04/2018	FZ	added claim file extracts
--	02/28/2018	FZ	added Activity file extract
--	04/06/2018	FZ	accommodated for performance view splitting
--
*/

--
-- COMMON
--
-- e_contact
truncate table public.e_contact cascade;
--copy public.e_contact("entitykey", "entitykeyvnumber", "firstname", "middlename", "lastname", "nameprefix", "namesuffix", "jobtitle", "fullname", "companyname", "preferredhtc", "isindividual", "contacttype", "birthdate", "gender", "maritalstatus", "comments", "conversionserver", "sequencenumber", "linkedentitykey", "linkedentitykeyvnumber", "ssn", "hicn", "fein", "tin") from '/import/e_contact.csv' delimiter '|' csv;
copy public.e_contact from '/import/e_contact.csv' delimiter '|' csv;

-- e_address
truncate table public.e_address cascade;
-- CAN NOT USE--need to add all columns>>>>>>>>copy public.e_address from '/import/e_address.csv' delimiter '|' CSV;  
--copy public.e_address("addresskey", "entitykey", "entitykeyvnumber", "entitytype", "sbuid", "addresskind", "mailingaddressind", "addresstype", "incareof", "addressname", "buildingname", "pobox", "street", "addressline2", "countyname", "city", "regioncode", "regionname", "zippostal", "countrycode", "countryname", "modifieddate") from '/import/e_address.csv' delimiter '|' csv;
copy public.e_address("addresskey", "entitykey", "entitykeyvnumber", "entitytype", "sbuid", "addresskind", "mailingaddressind", "addresstype", "incareof", "addressname", "buildingname", "pobox", "street", "addressline2", "unitdesignator", "unitnumber", "countyname", "city", "regioncode", "regionname", "zippostal", "countrycode", "countryname", "modifieddate") from '/import/e_address.csv' delimiter '|' csv;

-- e_phonenumbers
truncate table public.e_phonenumbers cascade;
--copy public.e_phonenumbers("contactmethodkey", "entitykey", "entitykeyvnumber", "primaryind", "htctype", "areacode", "phonenumber", "extension", "formattedhtc", "conversionserver", "sequencenumber") from '/import/e_phonenumbers.csv' delimiter '|' csv;
copy public.e_phonenumbers from '/import/e_phonenumbers.csv' delimiter '|' csv;

-- e_emailandwebsites
truncate table public.e_emailandwebsites cascade;
--copy public.e_emailandwebsites("contactmethodkey", "entitykey", "entitykeyvnumber", "primaryind", "htctype", "emailandweb", "conversionserver", "sequencenumber") from '/import/e_emailandwebsites.csv' delimiter '|' csv;
copy public.e_emailandwebsites from '/import/e_emailandwebsites.csv' delimiter '|' csv;

-- e_licence
truncate table public.e_licence cascade;
--copy public.e_licence("licencekey", "entitykey", "entitykeyvnumber", "licencetype", "licencecountry", "licencejurisdiction", "licenceclass", "licencenumber", "licencedate", "currentind", "licencestatus", "licencestatus_reason", "licencestatus_startdate", "licencestatus_enddate", "specialtrainingind", "specialtrainingdate") from '/import/e_license.csv' delimiter '|' csv;
copy public.e_licence from '/import/e_license.csv' delimiter '|' csv;

-- e_contactaccounts
truncate table public.e_contactaccounts cascade;
copy public.e_contactaccounts from '/import/e_contactaccounts.csv' delimiter '|' csv;

-- ct_lawyercontactdtls_1
truncate table public.ct_lawyercontactdtls_1 cascade;
copy public.ct_lawyercontactdtls_1 from '/import/ct_lawyercontactdtls_1.csv' delimiter '|' csv;

-- e_vendor
truncate table public.e_vendor cascade;
--copy public.e_vendor("entitykey", "entitykeyvnumber", "previousvendorkey", "businessnumber", "status", "inceptiondate", "profilecreditlimit", "profilecreditterms_days", "sbuid", "description", "contactentitykey", "contactentitykeyvnumber", "defaultaddresskey", "defaultcontactmethodkey", "conversionserver", "sequencenumber", "vendortype", "taxid", "taxexemptind", "corporationind", "foreignentityind", "screenkey", "settlementmethod", "deliverymethod", "deliveryaddresskey", "deliverymethodkey", "separatechequeind") from '/import/e_vendor.csv' delimiter '|' csv;
copy public.e_vendor from '/import/e_vendor.csv' delimiter '|' csv;

-- e_client
truncate table public.e_client cascade;
--copy public.e_client("entitykey", "entitykeyvnumber", "previousentitykey", "clientstatus", "inceptiondate", "continuoussince", "clientcustomformattedname", "altkey", "vipind", "sbuid", "comments", "contactentitykey1", "contactentitykeyvnumber1", "contactentitykey2", "contactentitykeyvnumber2", "contactentitykey3", "contactentitykeyvnumber3", "contactentitykey4", "contactentitykeyvnumber4", "contactentitykey5", "contactentitykeyvnumber5", "defaultaddresskey", "defaultcontactmethodkey", "deliverymethod", "deliveryaddresskey", "deliverymethodkey", "clienttype", "settlementmethod", "conversionserver", "sequencenumber", "screenkey") from '/import/e_client.csv' delimiter '|' csv;
copy public.e_client from '/import/e_client.csv' delimiter '|' CSV;

-- e_clientstaff
truncate table public.e_clientstaff cascade;
copy public.e_clientstaff from '/import/e_clientstaff.csv' delimiter '|' csv;

-- cl_clientdetails_1
truncate table public.cl_clientdetails_1 cascade;
copy public.cl_clientdetails_1 from '/import/cl_clientdetails_1.csv' delimiter '|' csv;

--
-- POLICY DATA
--
-- p_policy
truncate table public.p_policy cascade;
copy public.p_policy from '/import/p_policy.csv' delimiter '|' csv;
-- ON HOLD 03/14/2018 per ISI >>>>>> copy public.p_policy from '/import/p_policy_quote.csv' delimiter '|' csv;

-- p_billingdetails
truncate table public.p_billingdetail cascade;
copy public.p_billingdetail from '/import/p_billingdetail.csv' delimiter '|' csv;

-- p_paymentschedule
truncate table public.p_paymentschedule cascade;
copy public.p_paymentschedule from '/import/p_paymentschedule_PP.csv' delimiter '|' csv;
copy public.p_paymentschedule from '/import/p_paymentschedule_FP.csv' delimiter '|' csv;

-- p_insureds
truncate table public.p_insureds cascade;
copy public.p_insureds from '/import/p_insuredsFIRM.csv' delimiter '|' csv; 
copy public.p_insureds from '/import/p_insuredsLAWYER.csv' delimiter '|' csv; 
--07/18/2018 not available-- copy public.p_insureds from '/import/p_insuredsLAWYERERP.csv' delimiter '|' csv; 

-- p_items
truncate table public.p_items cascade;
copy public.p_items from '/import/p_itemsFIRM.csv' delimiter '|' csv;
copy public.p_items from '/import/p_itemsLAWYER.csv' delimiter '|' csv;
copy public.p_items from '/import/p_itemsLAWYER_cvgpa.csv' delimiter '|' csv;
copy public.p_items from '/import/p_itemsLAWYER_erp.csv' delimiter '|' csv;

-- i_lawfirm_1
truncate table public.i_lawfirm_1 cascade;
copy public.i_lawfirm_1("conversionreference","transactioneffectivedate","transactionseqno","transactiontype","sbuid","packageid","riskid","abstracterentity","abstracterind","abstracterinscompagentind","abstracterownfirmentitylabel","abstracterrevenue","affiliationind","aopfactorschedadj","attorneydisbarredind","attorneydisbarredinfo","attorneyerrorind","attorneygrievanceind","calcpremiumxs5m","claimsratioactloss10_1","claimsratioactloss1_1","claimsratioactloss3_1","claimsratioactloss5_1","claimsratioactlossall_1","claimsratioactratio10_1","claimsratioactratio1_1","claimsratioactratio3_1","claimsratioactratio5_1","claimsratioactratioall_1","claimsratioincloss10_1","claimsratioincloss1_1","claimsratioincloss3_1","claimsratioincloss5_1","claimsratioinclossall_1","claimsratioincratio10_1","claimsratioincratio1_1","claimsratioincratio3_1","claimsratioincratio5_1","claimsratioincratioall_1","claimsratioprem10_1","claimsratioprem1_1","claimsratioprem3_1","claimsratioprem5_1","claimsratiopremall_1","claimsurcharge","claimsurchargecreditmax","claimsurchargecreditmin","claimsurchargecreditrange","claimsurchargedebitmax","claimsurchargedebitmin","claimsurchargedebitrange","conditionalmandatoryfields","contactwhenabsent","contingencyfeeind","continuityfactor","covind_wlmend02","covind_wlmend03","covind_wlmend22","covind_wlmend23","covind_wlmend24","covind_wlmend25","covind_wlmend27", "covind_wlmendmn","facpremiumxs10m","facpremiumxs5m","financialinterestclientind","firmname","firmsize","firmsizefactor","firmsizeforrating","foreignclientcountries","foreignclientind","insurepastworkind","licensingagreementind","licensingagreementpct","localaffiliateind","nonattorneystaff","officerclientind","officesharingind","oneclient10pctind","organizationstructure","otherdebit","otherdebitcreditmax","otherdebitcreditmin","otherdebitcreditrange","otherdebitdebitmax","otherdebitdebitmin","otherdebitdebitrange","otherdebitexplain","outofstateind","outsoursepaymentind","outsoursesearchind","percentipcatcopyright","percentipcatdomestic","percentipcatforeign","percentipcatinfringement","percentipcatintproperty","percentipcatother","percentipcatotherexplain","percentipcattrademark","percentiptypebiotechnology","percentiptypebusiness","percentiptypechemical","percentiptypecomputer","percentiptypeelectrical","percentiptypemechanical","percentiptypeother","percentiptypeotherexplain","predecessorfirmsnotapplicab_1","prevlawfirmind","priordeclinedind","priordeclinedinfo","priortailind","priortailinfo","reinsurancepremium1m","reinsurancepremium1mfactor","restrictprioractsind","restrictprioractsinfo","scheduledadjustmentstotal","severityindex","severityindexcreditmax","severityindexcreditmin","severityindexcreditrange","severityindexdebitmax","severityindexdebitmin","severityindexdebitrange","severityindexzero","severityindexzerocreditmax","severityindexzerocreditmin","severityindexzerocreditrange","severityindexzerodebitmax","severityindexzerodebitmin","severityindexzerodebitrange","tailfactorunlimited","tailfactoryear_1","tailfactoryear_2","tailfactoryear_3","tailfactoryear_6","tailpremiumunlimited","tailpremiumyear_1","tailpremiumyear_2","tailpremiumyear_3","tailpremiumyear_6","thirdpartydocketind","thirdpartydocketsystem","wlmendmn_body","wlmendmn_code","wlmendmn_desc","yearsestablished","yearsinsured","conversionserver","sequencenumber") from '/import/i_lawfirm_1.csv' delimiter '|' csv;
-- copy public.i_lawfirm_1 from '/import/i_lawfirm_1.csv' delimiter '|' csv;

-- i_attorney_1
truncate table public.i_attorney_1 cascade;
copy public.i_attorney_1("conversionreference","transactioneffectivedate","transactionseqno","transactiontype","sbuid","packageid","riskid","attorneyaddedthisterm","attorneydesignation","attorneyemail","attorneyfactor","attorneyname","attorneynameid_ek","attorneynameid_vn","attorneynumber","clecredits","clecreditsfactor","currentleavestartdate","currentlyonleaveind","endleavethistransind","hoursworkedpermonth","inpracticedate","militaryleavetext","militaryleavetotaldays","newattorneyprioracts","otherlicenses","parttimecomment","parttimefactor","parttimefactoroverride","parttimefactorsys","parttimeoverridecomment","primarylicense","retrodatecomment","retrodatefactor","retrodateoverride","startleavethistransind","tailattorneyfactor","taildeductiblefactor","tailfactor","tailfactorunlimited","tailfactoryear_1","tailfactoryear_2","tailfactoryear_3","tailfactoryear_6","tailpremiumbefattorneyfactor","tailpremiumfactoredup","tailpremiumstart","tailpremiumtotal","tailpremiumunlimited","tailpremiumwodeductible","tailpremiumxs10m","tailpremiumxs5m","tailpremiumyear_1","tailpremiumyear_2","tailpremiumyear_3","tailpremiumyear_6","tailschedadjustfactor","yearsinpracticecommentor","yearsinpracticecommentsys","yearsinpracticefactor","yearsinpracticefactoror","yearsinpracticefactorsys","yearsinpracticeor","yearsinpracticesys") from '/import/i_attorney_1.csv' delimiter '|' csv;
-- copy public.i_attorney_1 from '/import/i_i_attorney_1.csv' delimiter '|' csv;

-- p_coverage
truncate table public.p_coverages cascade;
copy public.p_coverages("conversionreference","transactiontype","transactioneffectivedate","transactionseqno","pcmmappingkey","sbuid","packageid","riskid","linenumber","coverageid","basisofsettlement","coinsurance","rategroup","drivingrecord","rateper","baseratefactor","overrideratefactor","limit1","limit2","limitcurrencycd","deductible1","deductible2","premiumcurrencycd","ratedbasepremium","ratedgrosspremium","ratednetpremium","annualpremium","transactionpremium","nonpremiumsurchargeamount","premiumsurchargeamount","transactionsurcharge","nonpremiumdiscountamount","premiumdiscountamount","transactiondiscount","commissionamount","numberof1","valueof1","typeof1","numberof2","valueof2","typeof2","deletedrow","conversionserver","sequencenumber","veffectivedate","vexpirydate","claimsmadeind","retrodate","postedwweobjectid","limittierinfo","ratedftbasepremium","ratedftgrosspremium","ratedftpremium","ratedannualpremium","ratedtransactionpremium","ratedcommissionpercent","ratedcommissionamount","ftpremium","offsetftpremium","commissiontype","commissionpercent","transactionpremiumfactor","transactionpremiumdlyrate","overriderule","overrideftpremium","overrideannualpremium","overridetransactionpremium","overridecommissionpercent","overridecommissionamount","taxamount","gldiv","gldept","gllob","siretention1","siretention2","mappingcode","submappingcode","submappingcode2","netrate","grossrate","rateexposure","adjbilltypeind","waivedtransactionpremium","classcode","iso_statecode","iso_zipcode","iso_ratingterritory","iso_typeofpolicycode","iso_aslobstatcode","iso_sublinecode","iso_classificationcode","iso_coveragestatcode","iso_ratingidentificationcode","iso_constructioncode","iso_fireprotectioncode","iso_terrorismcoveragecode","iso_windhaildedcode","iso_bcegclass","iso_ratingbasiscode","iso_liabilityformcode","iso_molddamagecode","iso_liabilityexposureindcode","iso_exposurestatamount","iso_ratingmodificationfactor","iso_stateexceptionindcode","iso_bussincomeexpensecode","iso_liabcovindcode","iso_pctowneroccupied","iso_classcodedesc","rateeffectivedate","deductibletype","iso_formcode","iso_losscostmultiplier","iso_losscostdate","iso_yearofconstruction","ouid") from '/import/p_coverage_lpl_firm.csv' delimiter '|' csv;
copy public.p_coverages("conversionreference","transactiontype","transactioneffectivedate","transactionseqno","pcmmappingkey","sbuid","packageid","riskid","linenumber","coverageid","basisofsettlement","coinsurance","rategroup","drivingrecord","rateper","baseratefactor","overrideratefactor","limit1","limit2","limitcurrencycd","deductible1","deductible2","premiumcurrencycd","ratedbasepremium","ratedgrosspremium","ratednetpremium","annualpremium","transactionpremium","nonpremiumsurchargeamount","premiumsurchargeamount","transactionsurcharge","nonpremiumdiscountamount","premiumdiscountamount","transactiondiscount","commissionamount","numberof1","valueof1","typeof1","numberof2","valueof2","typeof2","deletedrow","conversionserver","sequencenumber","veffectivedate","vexpirydate","claimsmadeind","retrodate","postedwweobjectid","limittierinfo","ratedftbasepremium","ratedftgrosspremium","ratedftpremium","ratedannualpremium","ratedtransactionpremium","ratedcommissionpercent","ratedcommissionamount","ftpremium","offsetftpremium","commissiontype","commissionpercent","transactionpremiumfactor","transactionpremiumdlyrate","overriderule","overrideftpremium","overrideannualpremium","overridetransactionpremium","overridecommissionpercent","overridecommissionamount","taxamount","gldiv","gldept","gllob","siretention1","siretention2","mappingcode","submappingcode","submappingcode2","netrate","grossrate","rateexposure","adjbilltypeind","waivedtransactionpremium","classcode","iso_statecode","iso_zipcode","iso_ratingterritory","iso_typeofpolicycode","iso_aslobstatcode","iso_sublinecode","iso_classificationcode","iso_coveragestatcode","iso_ratingidentificationcode","iso_constructioncode","iso_fireprotectioncode","iso_terrorismcoveragecode","iso_windhaildedcode","iso_bcegclass","iso_ratingbasiscode","iso_liabilityformcode","iso_molddamagecode","iso_liabilityexposureindcode","iso_exposurestatamount","iso_ratingmodificationfactor","iso_stateexceptionindcode","iso_bussincomeexpensecode","iso_liabcovindcode","iso_pctowneroccupied","iso_classcodedesc","rateeffectivedate","deductibletype","iso_formcode","iso_losscostmultiplier","iso_losscostdate","iso_yearofconstruction","ouid") from '/import/p_coverage_lpl_lawyer.csv' delimiter '|' csv;
copy public.p_coverages("conversionreference","transactiontype","transactioneffectivedate","transactionseqno","pcmmappingkey","sbuid","packageid","riskid","linenumber","coverageid","basisofsettlement","coinsurance","rategroup","drivingrecord","rateper","baseratefactor","overrideratefactor","limit1","limit2","limitcurrencycd","deductible1","deductible2","premiumcurrencycd","ratedbasepremium","ratedgrosspremium","ratednetpremium","annualpremium","transactionpremium","nonpremiumsurchargeamount","premiumsurchargeamount","transactionsurcharge","nonpremiumdiscountamount","premiumdiscountamount","transactiondiscount","commissionamount","numberof1","valueof1","typeof1","numberof2","valueof2","typeof2","deletedrow","conversionserver","sequencenumber","veffectivedate","vexpirydate","claimsmadeind","retrodate","postedwweobjectid","limittierinfo","ratedftbasepremium","ratedftgrosspremium","ratedftpremium","ratedannualpremium","ratedtransactionpremium","ratedcommissionpercent","ratedcommissionamount","ftpremium","offsetftpremium","commissiontype","commissionpercent","transactionpremiumfactor","transactionpremiumdlyrate","overriderule","overrideftpremium","overrideannualpremium","overridetransactionpremium","overridecommissionpercent","overridecommissionamount","taxamount","gldiv","gldept","gllob","siretention1","siretention2","mappingcode","submappingcode","submappingcode2","netrate","grossrate","rateexposure","adjbilltypeind","waivedtransactionpremium","classcode","iso_statecode","iso_zipcode","iso_ratingterritory","iso_typeofpolicycode","iso_aslobstatcode","iso_sublinecode","iso_classificationcode","iso_coveragestatcode","iso_ratingidentificationcode","iso_constructioncode","iso_fireprotectioncode","iso_terrorismcoveragecode","iso_windhaildedcode","iso_bcegclass","iso_ratingbasiscode","iso_liabilityformcode","iso_molddamagecode","iso_liabilityexposureindcode","iso_exposurestatamount","iso_ratingmodificationfactor","iso_stateexceptionindcode","iso_bussincomeexpensecode","iso_liabcovindcode","iso_pctowneroccupied","iso_classcodedesc","rateeffectivedate","deductibletype","iso_formcode","iso_losscostmultiplier","iso_losscostdate","iso_yearofconstruction","ouid") from '/import/p_coverage_erp_lawyer.csv' delimiter '|' csv;
copy public.p_coverages("conversionreference","transactiontype","transactioneffectivedate","transactionseqno","pcmmappingkey","sbuid","packageid","riskid","linenumber","coverageid","basisofsettlement","coinsurance","rategroup","drivingrecord","rateper","baseratefactor","overrideratefactor","limit1","limit2","limitcurrencycd","deductible1","deductible2","premiumcurrencycd","ratedbasepremium","ratedgrosspremium","ratednetpremium","annualpremium","transactionpremium","nonpremiumsurchargeamount","premiumsurchargeamount","transactionsurcharge","nonpremiumdiscountamount","premiumdiscountamount","transactiondiscount","commissionamount","numberof1","valueof1","typeof1","numberof2","valueof2","typeof2","deletedrow","conversionserver","sequencenumber","veffectivedate","vexpirydate","claimsmadeind","retrodate","postedwweobjectid","limittierinfo","ratedftbasepremium","ratedftgrosspremium","ratedftpremium","ratedannualpremium","ratedtransactionpremium","ratedcommissionpercent","ratedcommissionamount","ftpremium","offsetftpremium","commissiontype","commissionpercent","transactionpremiumfactor","transactionpremiumdlyrate","overriderule","overrideftpremium","overrideannualpremium","overridetransactionpremium","overridecommissionpercent","overridecommissionamount","taxamount","gldiv","gldept","gllob","siretention1","siretention2","mappingcode","submappingcode","submappingcode2","netrate","grossrate","rateexposure","adjbilltypeind","waivedtransactionpremium","classcode","iso_statecode","iso_zipcode","iso_ratingterritory","iso_typeofpolicycode","iso_aslobstatcode","iso_sublinecode","iso_classificationcode","iso_coveragestatcode","iso_ratingidentificationcode","iso_constructioncode","iso_fireprotectioncode","iso_terrorismcoveragecode","iso_windhaildedcode","iso_bcegclass","iso_ratingbasiscode","iso_liabilityformcode","iso_molddamagecode","iso_liabilityexposureindcode","iso_exposurestatamount","iso_ratingmodificationfactor","iso_stateexceptionindcode","iso_bussincomeexpensecode","iso_liabcovindcode","iso_pctowneroccupied","iso_classcodedesc","rateeffectivedate","deductibletype","iso_formcode","iso_losscostmultiplier","iso_losscostdate","iso_yearofconstruction","ouid") from '/import/p_coverage_cvgpa_lawyer.csv' delimiter '|' csv;

/*
copy public.p_coverages("conversionreference","transactiontype","transactioneffectivedate","transactionseqno","pcmmappingkey","sbuid","packageid","riskid","linenumber","coverageid","basisofsettlement","coinsurance","rategroup","drivingrecord","rateper","baseratefactor","overrideratefactor","limit1","limit2","limitcurrencycd","deductible1","deductible2","premiumcurrencycd","ratedbasepremium","ratedgrosspremium","ratednetpremium","annualpremium","transactionpremium","nonpremiumsurchargeamount","premiumsurchargeamount","transactionsurcharge","nonpremiumdiscountamount","premiumdiscountamount","transactiondiscount","commissionamount","numberof1","valueof1","typeof1","numberof2","valueof2","typeof2","deletedrow","conversionserver","sequencenumber","veffectivedate","vexpirydate","claimsmadeind","retrodate","postedwweobjectid","limittierinfo","ratedftbasepremium","ratedftgrosspremium","ratedftpremium","ratedannualpremium","ratedtransactionpremium","ratedcommissionpercent","ratedcommissionamount","ftpremium","offsetftpremium","commissiontype","commissionpercent","transactionpremiumfactor","transactionpremiumdlyrate","overriderule","overrideftpremium","overrideannualpremium","overridetransactionpremium","overridecommissionpercent","overridecommissionamount","taxamount","gldiv","gldept","gllob","siretention1","siretention2","mappingcode","submappingcode","submappingcode2","netrate","grossrate","rateexposure","adjbilltypeind","waivedtransactionpremium","classcode","iso_statecode","iso_zipcode","iso_ratingterritory","iso_typeofpolicycode","iso_aslobstatcode","iso_sublinecode","iso_classificationcode","iso_coveragestatcode","iso_ratingidentificationcode","iso_constructioncode","iso_fireprotectioncode","iso_terrorismcoveragecode","iso_windhaildedcode","iso_bcegclass","iso_ratingbasiscode","iso_liabilityformcode","iso_molddamagecode","iso_liabilityexposureindcode","iso_exposurestatamount","iso_ratingmodificationfactor","iso_stateexceptionindcode","iso_bussincomeexpensecode","iso_liabcovindcode","iso_pctowneroccupied","iso_classcodedesc","rateeffectivedate","deductibletype","iso_formcode","iso_losscostmultiplier","iso_losscostdate","iso_yearofconstruction","ouid") from '/import/p_coverage_gc_firm.csv' delimiter '|' csv;
copy public.p_coverages("conversionreference","transactiontype","transactioneffectivedate","transactionseqno","pcmmappingkey","sbuid","packageid","riskid","linenumber","coverageid","basisofsettlement","coinsurance","rategroup","drivingrecord","rateper","baseratefactor","overrideratefactor","limit1","limit2","limitcurrencycd","deductible1","deductible2","premiumcurrencycd","ratedbasepremium","ratedgrosspremium","ratednetpremium","annualpremium","transactionpremium","nonpremiumsurchargeamount","premiumsurchargeamount","transactionsurcharge","nonpremiumdiscountamount","premiumdiscountamount","transactiondiscount","commissionamount","numberof1","valueof1","typeof1","numberof2","valueof2","typeof2","deletedrow","conversionserver","sequencenumber","veffectivedate","vexpirydate","claimsmadeind","retrodate","postedwweobjectid","limittierinfo","ratedftbasepremium","ratedftgrosspremium","ratedftpremium","ratedannualpremium","ratedtransactionpremium","ratedcommissionpercent","ratedcommissionamount","ftpremium","offsetftpremium","commissiontype","commissionpercent","transactionpremiumfactor","transactionpremiumdlyrate","overriderule","overrideftpremium","overrideannualpremium","overridetransactionpremium","overridecommissionpercent","overridecommissionamount","taxamount","gldiv","gldept","gllob","siretention1","siretention2","mappingcode","submappingcode","submappingcode2","netrate","grossrate","rateexposure","adjbilltypeind","waivedtransactionpremium","classcode","iso_statecode","iso_zipcode","iso_ratingterritory","iso_typeofpolicycode","iso_aslobstatcode","iso_sublinecode","iso_classificationcode","iso_coveragestatcode","iso_ratingidentificationcode","iso_constructioncode","iso_fireprotectioncode","iso_terrorismcoveragecode","iso_windhaildedcode","iso_bcegclass","iso_ratingbasiscode","iso_liabilityformcode","iso_molddamagecode","iso_liabilityexposureindcode","iso_exposurestatamount","iso_ratingmodificationfactor","iso_stateexceptionindcode","iso_bussincomeexpensecode","iso_liabcovindcode","iso_pctowneroccupied","iso_classcodedesc","rateeffectivedate","deductibletype","iso_formcode","iso_losscostmultiplier","iso_losscostdate","iso_yearofconstruction","ouid") from '/import/p_coverage_gc_lawyer.csv' delimiter '|' csv;

copy public.p_coverages("conversionreference","transactiontype","transactioneffectivedate","transactionseqno","pcmmappingkey","sbuid","packageid","riskid","linenumber","coverageid","basisofsettlement","coinsurance","rategroup","drivingrecord","rateper","baseratefactor","overrideratefactor","limit1","limit2","limitcurrencycd","deductible1","deductible2","premiumcurrencycd","ratedbasepremium","ratedgrosspremium","ratednetpremium","annualpremium","transactionpremium","nonpremiumsurchargeamount","premiumsurchargeamount","transactionsurcharge","nonpremiumdiscountamount","premiumdiscountamount","transactiondiscount","commissionamount","numberof1","valueof1","typeof1","numberof2","valueof2","typeof2","deletedrow","conversionserver","sequencenumber","veffectivedate","vexpirydate","claimsmadeind","retrodate","postedwweobjectid","limittierinfo","ratedftbasepremium","ratedftgrosspremium","ratedftpremium","ratedannualpremium","ratedtransactionpremium","ratedcommissionpercent","ratedcommissionamount","ftpremium","offsetftpremium","commissiontype","commissionpercent","transactionpremiumfactor","transactionpremiumdlyrate","overriderule","overrideftpremium","overrideannualpremium","overridetransactionpremium","overridecommissionpercent","overridecommissionamount","taxamount","gldiv","gldept","gllob","siretention1","siretention2","mappingcode","submappingcode","submappingcode2","netrate","grossrate","rateexposure","adjbilltypeind","waivedtransactionpremium","classcode","iso_statecode","iso_zipcode","iso_ratingterritory","iso_typeofpolicycode","iso_aslobstatcode","iso_sublinecode","iso_classificationcode","iso_coveragestatcode","iso_ratingidentificationcode","iso_constructioncode","iso_fireprotectioncode","iso_terrorismcoveragecode","iso_windhaildedcode","iso_bcegclass","iso_ratingbasiscode","iso_liabilityformcode","iso_molddamagecode","iso_liabilityexposureindcode","iso_exposurestatamount","iso_ratingmodificationfactor","iso_stateexceptionindcode","iso_bussincomeexpensecode","iso_liabcovindcode","iso_pctowneroccupied","iso_classcodedesc","rateeffectivedate","deductibletype","iso_formcode","iso_losscostmultiplier","iso_losscostdate","iso_yearofconstruction","ouid") from '/import/p_coverage_dr_firm.csv' delimiter '|' csv;
copy public.p_coverages("conversionreference","transactiontype","transactioneffectivedate","transactionseqno","pcmmappingkey","sbuid","packageid","riskid","linenumber","coverageid","basisofsettlement","coinsurance","rategroup","drivingrecord","rateper","baseratefactor","overrideratefactor","limit1","limit2","limitcurrencycd","deductible1","deductible2","premiumcurrencycd","ratedbasepremium","ratedgrosspremium","ratednetpremium","annualpremium","transactionpremium","nonpremiumsurchargeamount","premiumsurchargeamount","transactionsurcharge","nonpremiumdiscountamount","premiumdiscountamount","transactiondiscount","commissionamount","numberof1","valueof1","typeof1","numberof2","valueof2","typeof2","deletedrow","conversionserver","sequencenumber","veffectivedate","vexpirydate","claimsmadeind","retrodate","postedwweobjectid","limittierinfo","ratedftbasepremium","ratedftgrosspremium","ratedftpremium","ratedannualpremium","ratedtransactionpremium","ratedcommissionpercent","ratedcommissionamount","ftpremium","offsetftpremium","commissiontype","commissionpercent","transactionpremiumfactor","transactionpremiumdlyrate","overriderule","overrideftpremium","overrideannualpremium","overridetransactionpremium","overridecommissionpercent","overridecommissionamount","taxamount","gldiv","gldept","gllob","siretention1","siretention2","mappingcode","submappingcode","submappingcode2","netrate","grossrate","rateexposure","adjbilltypeind","waivedtransactionpremium","classcode","iso_statecode","iso_zipcode","iso_ratingterritory","iso_typeofpolicycode","iso_aslobstatcode","iso_sublinecode","iso_classificationcode","iso_coveragestatcode","iso_ratingidentificationcode","iso_constructioncode","iso_fireprotectioncode","iso_terrorismcoveragecode","iso_windhaildedcode","iso_bcegclass","iso_ratingbasiscode","iso_liabilityformcode","iso_molddamagecode","iso_liabilityexposureindcode","iso_exposurestatamount","iso_ratingmodificationfactor","iso_stateexceptionindcode","iso_bussincomeexpensecode","iso_liabcovindcode","iso_pctowneroccupied","iso_classcodedesc","rateeffectivedate","deductibletype","iso_formcode","iso_losscostmultiplier","iso_losscostdate","iso_yearofconstruction","ouid") from '/import/p_coverage_dr_lawyer.csv' delimiter '|' csv;

--copy public.p_coverages from '/import/p_coverage.csv' delimter '|' csv;
*/

-- p_inlineschedules
truncate table public.p_inlineschedules cascade;
copy public.p_inlineschedules("conversionreference","transactiontype","transactioneffectivedate","transactionseqno","sbuid","packageid","riskid","inlinecategory","linenumber","description","itemvalue","deletedrow","conversionserver","sequencenumber","veffectivedate","vexpirydate","postedwweobjectid") from '/import/p_inlineschedulesFirmAOP.csv' delimiter '|' csv;
copy public.p_inlineschedules("conversionreference","transactiontype","transactioneffectivedate","transactionseqno","sbuid","packageid","riskid","inlinecategory","linenumber","description","itemvalue","deletedrow","conversionserver","sequencenumber","veffectivedate","vexpirydate","postedwweobjectid") from '/import/p_inlineschedulesFirmLim.csv' delimiter '|' csv;
copy public.p_inlineschedules("conversionreference","transactiontype","transactioneffectivedate","transactionseqno","sbuid","packageid","riskid","inlinecategory","linenumber","description","itemvalue","deletedrow","conversionserver","sequencenumber","veffectivedate","vexpirydate","postedwweobjectid") from '/import/p_inlineschedulesLawyer.csv' delimiter '|' csv;
--copy public.p_inlineschedules from '/import/p_inlineschedules.csv' delimiter '|' csv;

-- is_areaofpractice_1
truncate table public.is_areaofpractice_1 cascade;
--copy public.is_areaofpractice_1("conversionreference","transactioneffectivedate","transactionseqno","transactiontype","sbuid","packageid","riskid","inlinecategory","linenumber","aopcode","aopfactor","aopfactorhigh","aopfactorlow","aopotherexplain","aoppercent","aopweightedfactor","conditionalmandatoryfields") from '/import/is_areaofpractice_1.csv' delimiter '|' csv;
copy public.is_areaofpractice_1 from '/import/is_areaofpractice_1.csv' delimiter '|' csv;

-- is_quotelimits_1
truncate table public.is_quotelimits_1 cascade;
--copy public.is_quotelimits_1("conversionreference","transactioneffectivedate","transactionseqno","transactiontype","sbuid","packageid","riskid","inlinecategory","linenumber","additionallimitfactor","attorneyfactor","attorneyfactorsubtotal","attorneyfactortotal","baserate","clecreditsfactor","clecreditsfactorsubtotal","clecreditsfactortotal","conditionalmandatoryfields","continuityfactor","continuityfactorsubtotal","continuityfactortotal","deductiblefactor","deductiblefactorsubtotal","deductiblefactortotal","facpremiumxs10m","facpremiumxs10mreadonly","facpremiumxs5m","facpremiumxs5mreadonly","facpremiumxs5msysind","firmlinenumber","firmpremium","firmsizefactor","firmsizefactorsubtotal","firmsizefactortotal","liabilitydeductible","liabilitydeductibletext","liabilitylimit","liabilitylimit1","liabilitylimit2","liabilitylimittext","liabilitypremiumtext","limitfactor","limitfactorsubtotal","limitfactortotal","policylimitind","quoteproposalind","scheduledadjustmentfactor","scheduledadjustmentfactorsu_1","scheduledadjustmentfactortotal","xs10premiumfactor","xs10premiumperattorney","xs10premiumsubtotal","xs10premiumtotal","xs5factor","xs5minimum","xs5premiumfactor","xs5premiumperattorney","xs5premiumsubtotal","xs5premiumtotal","conversionserver","sequencenumber") from '/import/is_quotelimits_1.csv' delimiter '|' csv;
copy public.is_quotelimits_1 from '/import/is_quotelimits_1.csv' delimiter '|' csv;

-- is_quotelimitsattorney_1
truncate table public.is_quotelimitsattorney_1 cascade;
copy public.is_quotelimitsattorney_1("conversionreference","transactioneffectivedate","transactionseqno","transactiontype","sbuid","packageid","riskid","inlinecategory", "linenumber", "additionallimitfactor","attorneyfactor","attorneyfactorcomment","attorneyfactorsubtotal","attorneynumber", "attorneypremium","attorneypremium1m", "attorneypremium5m", "baserate","clecreditsfactor","clecreditsfactorcomment","clecreditsfactorsubtotal","continuityfactor","continuityfactorcomment","continuityfactorsubtotal","deductiblefactor","deductiblefactorcomment","deductiblefactorsubtotal","firmsizefactor","firmsizefactorcomment","firmsizefactorsubtotal","liabilitydeductibletext","liabilitylimit1","liabilitylimit2","liabilitylimittext","limitfactor","limitfactorcomment","limitfactorsubtotal","militaryleaveind","policylimitind","scheduledadjustmentfactor","scheduledadjustmentfactorco_1","scheduledadjustmentfactorsu_1","xs10premium","xs10premiumcomment","xs10premiumperattorney","xs10premiumsubtotal","xs5factor","xs5minimum","xs5premium","xs5premiumcomment","xs5premiumperattorney","xs5premiumsubtotal","conversionserver","sequencenumber") from '/import/is_quotelimitsattorney_1.csv' delimiter '|' csv;
--copy public.is_quotelimitsattorney_1 from '/import/is_quotelimitsattorney_1.csv' delimiter '|' csv;



--
--	CLAIMS DATA
--
--c_claim
truncate table public.c_claim cascade;
copy public.c_claim from '/import/c_claim.csv' delimiter '|' csv;

--c_contactclaimant  (requirement--claimant records exist in contact)
copy public.e_contact from '/import/e_contactclaimant.csv' delimiter '|' csv;

--c_involvedparty
truncate table public.c_involvedparty cascade;
copy public.c_involvedparty from '/import/c_involvedparty.csv' delimiter '|' csv;

--c_involvedrelation
truncate table public.c_involvedrelation cascade;
copy public.c_involvedrelation from '/import/c_involvedrelation.csv' delimiter '|' csv;

--c_claimantdetails
truncate table public.c_claimantdetails cascade;
copy public.c_claimantdetails from '/import/c_claimantdetails.csv' delimiter '|' csv;

--c_claimantsubfile
truncate table public.c_claimantsubfile cascade;
copy public.c_claimantsubfile from '/import/c_claimantsubfile.csv' delimiter '|' csv;

--c_claimitem
truncate table public.c_claimitem cascade;
copy public.c_claimitem from '/import/c_claimitem_LPL.csv' delimiter '|' csv;

--c_claimantcoverage
truncate table public.c_claimantcoverage cascade;
copy public.c_claimantcoverage from '/import/c_claimantcoverage_LPL.csv' delimiter '|' csv;
copy public.c_claimantcoverage from '/import/c_claimantcoverage_GC.csv' delimiter '|' csv;
copy public.c_claimantcoverage from '/import/c_claimantcoverage_DF.csv' delimiter '|' csv;
copy public.c_claimantcoverage from '/import/c_claimantcoverage_ERP.csv' delimiter '|' csv;
copy public.c_claimantcoverage from '/import/c_claimantcoverage_ULAE.csv' delimiter '|' csv;

--c_claimexaminer
truncate table public.c_claimexaminer cascade;
copy public.c_claimexaminer from '/import/c_claimexaminer.csv' delimiter '|' csv;

--c_claimexaminersubfile
truncate table public.c_claimexaminersubfile cascade;
copy public.c_claimexaminersubfile from '/import/c_claimexaminersubfile.csv' delimiter '|' csv;

--c_transaction
truncate table public.c_transaction cascade;
copy public.c_transaction from '/import/c_transaction.csv' delimiter '|' csv;
copy public.c_transaction from '/import/c_transactionded.csv' delimiter '|' csv;

--c_transactioncoverage
truncate table public.c_transactioncoverage cascade;
copy public.c_transactioncoverage from '/import/c_transcov_res_alae.csv' delimiter '|' csv;
copy public.c_transactioncoverage from '/import/c_transcov_res_ind.csv' delimiter '|' csv;
copy public.c_transactioncoverage from '/import/c_transcov_pay_alae.csv' delimiter '|' csv;
copy public.c_transactioncoverage from '/import/c_transcov_pay_ind.csv' delimiter '|' csv;
copy public.c_transactioncoverage from '/import/c_transcov_pay_ded.csv' delimiter '|' csv;
copy public.c_transactioncoverage from '/import/c_transcov_pay_ulae.csv' delimiter '|' csv;

--c_transactiocategory
truncate table public.c_transactioncategory cascade;
copy public.c_transactioncategory from '/import/c_transcat_res_alae.csv' delimiter '|' csv;
copy public.c_transactioncategory from '/import/c_transcat_res_ind.csv' delimiter '|' csv;
copy public.c_transactioncategory from '/import/c_transcat_pay_alae.csv' delimiter '|' csv;
copy public.c_transactioncategory from '/import/c_transcat_pay_ind.csv' delimiter '|' csv;
copy public.c_transactioncategory from '/import/c_transcat_ded.csv' delimiter '|' csv;
copy public.c_transactioncategory from '/import/c_transcat_ULAE.csv' delimiter '|' csv;

--c_cl_clmlossdetails_1
truncate table public.c_cl_clmlossdetails_1 cascade;
copy public.c_cl_clmlossdetails_1 from '/import/c_cl_clmlossdetail_1.csv' delimiter '|' csv;

--c_claimantworkarea
truncate table public.c_claimantworkarea cascade;
copy public.c_claimantworkarea from '/import/c_claimantworkarea.csv' delimiter '|' csv;
copy public.c_claimantworkarea from '/import/c_claimantworkareaULAE.csv' delimiter '|' csv;

--c_cl_clmtlitigation_1
truncate table public.c_cl_clmtlitigation_1 cascade;
copy public.c_cl_clmtlitigation_1 from '/import/c_cl_clmtlitigation_1.csv' delimiter '|' csv;


--
--	INCIDENT DATA
--
--c_claim
copy public.c_claim from '/import/c_INCIDENT.csv' delimiter '|' csv;

--c_contactclaimant
copy public.e_contact from '/import/e_contactclaimantINCIDENT.csv' delimiter '|' csv;

--c_involvedparty
copy public.c_involvedparty from '/import/c_involvedpartyINCIDENT.csv' delimiter '|' csv;

--c_involvedrelation
copy public.c_involvedrelation from '/import/c_involvedrelationINCIDENT.csv' delimiter '|' csv;

--c_claimantdetails
copy public.c_claimantdetails from '/import/c_claimantdetailsINCIDENT.csv' delimiter '|' csv;

--c_claimantsubfile
copy public.c_claimantsubfile from '/import/c_claimantsubfileINCIDENT.csv' delimiter '|' csv;

--c_claimexaminer
copy public.c_claimexaminer from '/import/c_claimexaminerINCIDENT.csv' delimiter '|' csv;

--c_claimexaminersubfile
copy public.c_claimexaminersubfile from '/import/c_claimexaminersubfileINCIDENT.csv' delimiter '|' csv;

--c_cl_clmlossdetails_1
copy public.c_cl_clmlossdetails_1 from '/import/c_cl_clmlossdetail_1INC.csv' delimiter '|' csv;


--
--	ACTIVITY DATA
--
--a_activity
truncate table public.a_activity cascade;
copy public.a_activity from '/import/a_activity.csv' delimiter '|' csv;

--l_linkdocument
truncate table public.l_linkdocument cascade;
copy public.l_linkdocument from '/import/l_linkdocument.csv' delimiter '|' csv;

--l_activitydocuments
truncate table public.l_activitydocuments cascade;
copy public.l_activitydocuments from '/import/l_activitydocuments.csv' delimiter '|' csv;
