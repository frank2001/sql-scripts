/*
    ISI_Post_Extract_Script.sql
    
    Fixes extracted ISI Interim data prior to moving to ISI Iteration environment
    
    History
    04/14/2018  FZ  Created
*/

/* #129  ISI doesn't allow RCA, RWR, RWN so change to END */
--policy
select * from p_policy where transactiontype in ('RCA', 'RWN', 'RWR') order by conversionreference;
select * from p_insureds where transactiontype in ('RCA', 'RWN', 'RWR') order by conversionreference;
select * from p_items where transactiontype in ('RCA', 'RWN', 'RWR') order by conversionreference;
select * from i_lawfirm_1 where transactiontype in ('RCA', 'RWN', 'RWR') order by conversionreference;
select * from i_lawfirm_1 where transactiontype in ('RCA', 'RWN', 'RWR') order by conversionreference;
select * from i_attorney_1 where transactiontype in ('RCA', 'RWN', 'RWR') order by conversionreference;

select * from p_coverages where transactiontype in ('RCA', 'RWN', 'RWR') order by conversionreference;
--activity
select * from l_linkdocument where poltransactiontype in ('RCA', 'RWN', 'RWR') order by polconversionref; --1
--claim
select * from c_claim where poltranstype in ('RCA', 'RWN', 'RWR') order by polconversionref; --1
select * from c_claimitem where poltranstype in ('RCA', 'RWN', 'RWR') order by polconversionref; --1
select * from c_claimantcoverage where poltranstype in ('RCA', 'RWN', 'RWR') order by polconversionref; --0
select * from c_transactioncoverage where poltranstype in ('RCA', 'RWN', 'RWR') order by polconversionref; --0
select * from c_claimantworkarea where poltranstype in ('RCA', 'RWN', 'RWR') order by polconversionref; --0
select * from c_cl_clmtlitigation_1 where poltranstype in ('RCA', 'RWN', 'RWR') order by polconversionref; --0


-- drop constraints
ALTER TABLE p_policy DROP CONSTRAINT f88_p_policy;
ALTER TABLE p_policy DROP CONSTRAINT f89_p_policy;
 
ALTER TABLE p_insureds DROP CONSTRAINT f01_p_insureds;
 
ALTER TABLE p_items DROP CONSTRAINT f01_p_items;
 
ALTER TABLE p_coverages DROP CONSTRAINT f01_p_coverages;
 
ALTER TABLE p_inlineschedules DROP CONSTRAINT f01_p_inlineschedules;
 
ALTER TABLE i_attorney_1 DROP CONSTRAINT f01_i_attorney_1;
ALTER TABLE i_lawfirm_1 DROP CONSTRAINT f01_i_lawfirm_1;
ALTER TABLE is_areaofpractice_1 DROP CONSTRAINT f01_is_areaofpractice_1;
ALTER TABLE is_quotelimits_1 DROP CONSTRAINT f01_is_quotelimits_1;
ALTER TABLE is_quotelimitsattorney_1 DROP CONSTRAINT f01_is_quotelimitsattorney_1;
 
ALTER TABLE c_claim DROP CONSTRAINT f01_c_claim;
ALTER TABLE c_claim DROP CONSTRAINT f04_c_claim;
 
ALTER TABLE c_claimitem DROP CONSTRAINT f02_c_claimitem;
ALTER TABLE c_claimitem DROP CONSTRAINT f03_c_claimitem;
 
ALTER TABLE c_claimantcoverage DROP CONSTRAINT f02_c_claimantcoverage;
ALTER TABLE c_claimantcoverage DROP CONSTRAINT f05_c_claimantcoverage;
 
ALTER TABLE c_transactioncoverage DROP CONSTRAINT f03_c_transactioncoverage;
ALTER TABLE c_transactioncoverage DROP CONSTRAINT f04_c_transactioncoverage;
 
ALTER TABLE c_claimantworkarea DROP CONSTRAINT f01_c_claimantworkarea;
ALTER TABLE c_claimantworkarea DROP CONSTRAINT f03_c_claimantworkarea;

ALTER TABLE l_linkdocument DROP CONSTRAINT c02_l_linkdocument;
 
-- change transactiontype
update p_coverages
	set transactiontype = 'END'
	where transactiontype in ('RCA', 'RWN', 'RWR')
;
update p_items
    set transactiontype = 'END'
	where transactiontype in ('RCA', 'RWN', 'RWR')
;
update p_policy
	set transactiontype = 'END',
	    relatedrevrcatranstype = NULL,
		relatedrevrcatranseffdate = NULL,
		relatedrevrcatransseqno = NULL
	where transactiontype = 'RCA'
	  and relatedrevrcatranstype is not NULL
	  and relatedrevrcatranseffdate is not NULL
	  and relatedrevrcatransseqno is not NULL
;
update p_policy
	set transactiontype = 'END'
	where transactiontype in ('RWN', 'RWR')
;
--enable constraints
ALTER TABLE p_policy ADD CONSTRAINT f88_p_policy FOREIGN KEY (conversionreference, relatedtranseffdate, relatedtransseqno, relatedtranstype, sbuid) REFERENCES p_policy (conversionreference, transactioneffectivedate, transactionseqno, transactiontype, sbuid);
ALTER TABLE p_policy ADD CONSTRAINT f89_p_policy FOREIGN KEY (conversionreference, relatedrevrcatranseffdate, relatedrevrcatransseqno, relatedrevrcatranstype, sbuid) REFERENCES p_policy (conversionreference, transactioneffectivedate, transactionseqno, transactiontype, sbuid);
 
ALTER TABLE p_insureds ADD CONSTRAINT f01_p_insureds FOREIGN KEY (conversionreference, transactioneffectivedate, transactionseqno, transactiontype, sbuid) REFERENCES p_policy (conversionreference, transactioneffectivedate, transactionseqno, transactiontype, sbuid);
 
ALTER TABLE p_items ADD CONSTRAINT f01_p_items FOREIGN KEY (conversionreference, transactioneffectivedate, transactionseqno, transactiontype, sbuid) REFERENCES p_policy (conversionreference, transactioneffectivedate, transactionseqno, transactiontype, sbuid);
 
ALTER TABLE p_coverages ADD CONSTRAINT f01_p_coverages FOREIGN KEY (conversionreference, transactioneffectivedate, transactionseqno, transactiontype, sbuid) REFERENCES p_policy (conversionreference, transactioneffectivedate, transactionseqno, transactiontype, sbuid);
 
ALTER TABLE p_inlineschedules ADD CONSTRAINT f01_p_inlineschedules FOREIGN KEY (conversionreference, transactioneffectivedate, transactionseqno, transactiontype, sbuid) REFERENCES p_policy (conversionreference, transactioneffectivedate, transactionseqno, transactiontype, sbuid);
 
ALTER TABLE i_attorney_1 ADD CONSTRAINT f01_i_attorney_1 FOREIGN KEY (transactiontype, transactionseqno, packageid, sbuid, conversionreference, transactioneffectivedate, riskid) REFERENCES p_items (transactiontype, transactionseqno, packageid, sbuid, conversionreference, transactioneffectivedate, riskid);
ALTER TABLE i_lawfirm_1 ADD CONSTRAINT f01_i_lawfirm_1 FOREIGN KEY (transactiontype, transactionseqno, packageid, sbuid, conversionreference, transactioneffectivedate, riskid) REFERENCES p_items (transactiontype, transactionseqno, packageid, sbuid, conversionreference, transactioneffectivedate, riskid);
ALTER TABLE is_areaofpractice_1 ADD CONSTRAINT f01_is_areaofpractice_1 FOREIGN KEY (transactiontype, transactioneffectivedate, inlinecategory, linenumber, riskid, packageid, transactionseqno, sbuid, conversionreference) REFERENCES p_inlineschedules (transactiontype, transactioneffectivedate, inlinecategory, linenumber, riskid, packageid, transactionseqno, sbuid, conversionreference);
ALTER TABLE is_quotelimits_1 ADD CONSTRAINT f01_is_quotelimits_1 FOREIGN KEY (transactiontype, transactioneffectivedate, inlinecategory, linenumber, riskid, packageid, transactionseqno, sbuid, conversionreference) REFERENCES p_inlineschedules (transactiontype, transactioneffectivedate, inlinecategory, linenumber, riskid, packageid, transactionseqno, sbuid, conversionreference);
ALTER TABLE is_quotelimitsattorney_1 ADD CONSTRAINT f01_is_quotelimitsattorney_1 FOREIGN KEY (transactiontype, transactioneffectivedate, inlinecategory, linenumber, riskid, packageid, transactionseqno, sbuid, conversionreference) REFERENCES p_inlineschedules (transactiontype, transactioneffectivedate, inlinecategory, linenumber, riskid, packageid, transactionseqno, sbuid, conversionreference);
 
ALTER TABLE c_claim ADD CONSTRAINT f01_c_claim FOREIGN KEY (polconversionref, poltranseffdate, poltransseqno, poltranstype, sbuid) REFERENCES p_policy (conversionreference, transactioneffectivedate, transactionseqno, transactiontype, sbuid);
ALTER TABLE c_claim ADD CONSTRAINT f04_c_claim FOREIGN KEY (polconversionref, poltranseffdate, poltransseqno, poltranstype, sbuid, packageid) REFERENCES p_packages (conversionreference, transactioneffectivedate, transactionseqno, transactiontype, sbuid, packageid);
 
ALTER TABLE c_claimitem ADD CONSTRAINT f02_c_claimitem FOREIGN KEY (polconversionref, poltranseffdate, poltransseqno, poltranstype, sbuid, packageid, riskid) REFERENCES p_items (conversionreference, transactioneffectivedate, transactionseqno, transactiontype, sbuid, packageid, riskid);
ALTER TABLE c_claimitem ADD CONSTRAINT f03_c_claimitem FOREIGN KEY (claimmappingkey, claimvnumber, polconversionref, poltranseffdate, poltransseqno, poltranstype, sbuid) REFERENCES c_claim (claimmappingkey, claimvnumber, polconversionref, poltranseffdate, poltransseqno, poltranstype, sbuid);
 
ALTER TABLE c_claimantcoverage ADD CONSTRAINT f02_c_claimantcoverage FOREIGN KEY (polconversionref, poltranseffdate, poltransseqno, poltranstype, sbuid, packageid, riskid, linenumber, coverageid) REFERENCES p_coverages (conversionreference, transactioneffectivedate, transactionseqno, transactiontype, sbuid, packageid, riskid, linenumber, coverageid);
ALTER TABLE c_claimantcoverage ADD CONSTRAINT f05_c_claimantcoverage FOREIGN KEY (claimmappingkey, claimvnumber, polconversionref, poltranseffdate, poltransseqno, poltranstype, sbuid, packageid, riskid, linenumber) REFERENCES c_claimitem (claimmappingkey, claimvnumber, polconversionref, poltranseffdate, poltransseqno, poltranstype, sbuid, packageid, riskid, linenumber);
 
ALTER TABLE c_transactioncoverage ADD CONSTRAINT f03_c_transactioncoverage FOREIGN KEY (polconversionref, poltranseffdate, poltransseqno, poltranstype, sbuid, packageid, riskid, linenumber, coverageid) REFERENCES p_coverages (conversionreference, transactioneffectivedate, transactionseqno, transactiontype, sbuid, packageid, riskid, linenumber, coverageid);
ALTER TABLE c_transactioncoverage ADD CONSTRAINT f04_c_transactioncoverage FOREIGN KEY (claimmappingkey, claimvnumber, polconversionref, poltranseffdate, poltransseqno, poltranstype, sbuid, packageid, riskid, linenumber, coverageid, invno, subfilekey, claimtypekey, kindofloss, classofloss, basisofloss, losscontrolevent) REFERENCES c_claimantcoverage (claimmappingkey, claimvnumber, polconversionref, poltranseffdate, poltransseqno, poltranstype, sbuid, packageid, riskid, linenumber, coverageid, invno, subfilekey, claimtypekey, kindofloss, classofloss, basisofloss, losscontrolevent);
 
ALTER TABLE c_claimantworkarea ADD CONSTRAINT f01_c_claimantworkarea FOREIGN KEY (claimmappingkey, claimvnumber, polconversionref, poltranseffdate, poltransseqno, poltranstype, sbuid, packageid, riskid, linenumber) REFERENCES c_claimitem (claimmappingkey, claimvnumber, polconversionref, poltranseffdate, poltransseqno, poltranstype, sbuid, packageid, riskid, linenumber);
ALTER TABLE c_claimantworkarea ADD CONSTRAINT f03_c_claimantworkarea FOREIGN KEY (polconversionref, poltranseffdate, poltransseqno, poltranstype, sbuid, packageid, riskid, linenumber, coverageid) REFERENCES p_coverages (conversionreference, transactioneffectivedate, transactionseqno, transactiontype, sbuid, packageid, riskid, linenumber, coverageid);

ALTER TABLE l_lnkdocument ADD CONSTRAINT c02_l_linkdocument FOREIGN KEY 

---------------------------BELOW NOT CURRENTLY USED-----------------------------
-- Fix flat cancel/rewrite transaction, namely assure intial transaction exists
INSERT INTO p_policy
	(
	SELECT
		P.conversionreference,
		'NEW' as transactiontype,
		P.transactioneffectivedate,
		P.transactionseqno,
		P.pcmmappingkey,
		P.sbuid,
		P.inceptiondate,
		P.policyeffectivedate,
		P.policyexpirydate,
		P.veffectivedate,
		P.vexpirydate,
		P.veffectivetime,
		P.vexpirytime,
		P.policykey,
		P.previouspolicykey,
		P.alternatepolicykey,
		P.agentbrokerpolicykey,
		P.binderkey,
		P.policynextunderwritedate,
		P.cliententitymappingkey,
		P.renewaltype,
		P.termlengthind,
		P.termlengthindunit,
		P.billingtype,
		P.papind,
		P.paymentplan,
		P.planeffdate,
		P.planexpdate,
		P.subscriptionind,
		P.facultativeind,
		P.premiumcurrencycd,
		P.limitcurrencycd,
		P.sbuofficekey,
		P.underwriterentitymappingkey,
		P.priorperiodind,
		P.cancellationtype,
		P.canreturnpremium,
		P.canearnedpremium,
		P.reinstateexpirydate,
		P.conversionserver,
		P.sequencenumber,
		NULL as relatedrevrcatranstype,
		NULL as relatedrevrcatranseffdate,
		NULL as relatedrevrcatransseqno,
		P.claimsmadeind,
		P.retrodate,
		P.extendreporteffdate,
		P.extendreportexpdate,
		200 as vreasoncode,  ---or 500
		P.ventrydatetime,
		P.vusermappingkey,
		P.postedyearmth,
		P.policysourceind,
		P.assumedcompanymappingkey,
		P.writingcompanymappingkey,
		P.freetradezoneriskcode,
		P.languagecd,
		P.overridereasoncd,
		P.overridereasonnote,
		P.postedwweobjectid,
		P.postedwwevnumber,
		P.transactionstatus,
		P.nextfollowupdate,
		P.finaladjustmentind,
		P.rateeditionname,
		P.rateeditionversion,
		P.wipstatus,
		P.quoterelatedtranstype,
		P.relatedtranstype,
		P.relatedtranseffdate,
		P.relatedtransseqno,
		P.quotekey,
		P.quotedate,
		P.quotedaystoaccept,
		P.quoteexpirydate,
		P.quotedefinedname,
		P.quoteseriescode,
		P.quotestatus,
		P.quotedocumentsprinted,
		P.brkwritingcompanymappingkey,
		P.brokeragepolicycommission,
		P.quickquoteind,
		P.referralstatus,
		P.documentcategory,
		P.frontedcompanymappingkey,
		P.assumedcommissionamount,
		P.assumedcommissiontype,
		P.policycompositiontype,
		P.predominantjurisdiction,
		P.predominantpolicyregion,
		P.totalwaivedpremium,
		P.accountingdate,
		P.quotedelcinedby,
		P.quotecompetitor,
		P.quotecompetitorpremium,
		P.policycontactentitykey,
		P.policycontactentitykeyvnumber,
		P.comments,
		P.renewalstatus,
		P.insurancestatus,
		P.liabilitylimit,
		P.overridemailingaddrkey,
		P.auditreportingind,
		P.convertedtopolicytrneffdate,
		P.convertedtopolicytrnseqno,
		P.quotecompletedreasoncode,
		P.quotecompletedreasonnotes,
		P.quotelastmodifieddatetime,
		P.extendreporttermlength,
		P.extendreportlengthunit,
		P.useextendreportexpoverrideind
	FROM p_policy P
	WHERE transactiontype = 'RCA'
	)
;

-- Fix flat cancel/rewrite transaction, namely assure intial transaction exists
INSERT INTO p_policy
	(
	SELECT
		CAST(P.conversionreference as TEXT) as conversionreference,
		CAST('REN' as TEXT) as transactiontype,
		CAST(P.transactioneffectivedate as NUMERIC) as transactioneffectivedate,
		CAST(P.transactionseqno as NUMERIC) as transactionseqno,
		CAST(P.pcmmappingkey as TEXT) as pcmmappingkey,
		CAST(P.sbuid as NUMERIC) as sbuid,
		CAST(P.inceptiondate as NUMERIC) as inceptiondate,
		CAST(P.policyeffectivedate as NUMERIC) as policyeffectivedate,
		CAST(P.policyexpirydate as NUMERIC) as policyexpirydate,
		CAST(P.veffectivedate as NUMERIC) as veffectivedate,
		CAST(P.vexpirydate as NUMERIC) as vexpirydate,
		CAST(P.veffectivetime as TEXT) as veffectivetime,
		CAST(P.vexpirytime as TEXT) as vexpirytime,
		CAST(P.policykey as TEXT) as policykey,
		CAST(P.previouspolicykey as TEXT) as previouspolicykey,
		CAST(P.alternatepolicykey as TEXT) as alternatepolicykey,
		CAST(P.agentbrokerpolicykey as TEXT) as agentbrokerpolicykey,
		CAST(P.binderkey as TEXT) as binderkey,
		CAST(P.policynextunderwritedate as NUMERIC) as policynextunderwritedate,
		CAST(P.cliententitymappingkey as TEXT) as cliententitymappingkey,
		CAST(P.renewaltype as TEXT) as renewaltype,
		CAST(P.termlengthind as NUMERIC) as termlengthind,
		CAST(P.termlengthindunit as TEXT) as termlengthindunit,
		CAST(P.billingtype as TEXT) as billingtype,
		CAST(P.papind as NUMERIC) as papind,
		CAST(P.paymentplan as TEXT) as paymentplan,
		CAST(P.planeffdate as NUMERIC) as planeffdate,
		CAST(P.planexpdate as NUMERIC) as planexpdate,
		CAST(P.subscriptionind as TEXT) as subscriptionind,
		CAST(P.facultativeind as NUMERIC) as facultativeind,
		CAST(P.premiumcurrencycd as TEXT) as premiumcurrencycd,
		CAST(P.limitcurrencycd as TEXT) as limitcurrencycd,
		CAST(P.sbuofficekey as TEXT) as sbuofficekey,
		CAST(P.underwriterentitymappingkey as TEXT) as underwriterentitymappingkey,
		CAST(P.priorperiodind as NUMERIC) as priorperiodind,
		CAST(P.cancellationtype as NUMERIC) as cancellationtype,
		CAST(P.canreturnpremium as NUMERIC) as canreturnpremium,
		CAST(P.canearnedpremium as NUMERIC) as canearnedpremium,
		CAST(P.reinstateexpirydate as NUMERIC) as reinstateexpirydate,
		CAST(NULL as TEXT) as conversionserver,
		CAST(NULL as NUMERIC) as sequencenumber,
		CAST(NULL as NUMERIC) as relatedrevrcatranstype,
		CAST(NULL as NUMERIC) as relatedrevrcatranseffdate,
		CAST(NULL as NUMERIC) as relatedrevrcatransseqno,
		CAST(P.claimsmadeind as NUMERIC) as claimsmadeind,
		CAST(P.retrodate as NUMERIC) as retrodate,
		CAST(P.extendreporteffdate as NUMERIC) as extendreporteffdate,
		CAST(P.extendreportexpdate as NUMERIC) as extendreportexpdate,
		CAST(500 as NUMERIC) as vreasoncode,
		CAST(P.ventrydatetime as NUMERIC) as ventrydatetime,
		CAST(P.vusermappingkey as TEXT) as vusermappingkey,
		CAST(P.postedyearmth as NUMERIC) as postedyearmth,
		CAST(P.policysourceind as TEXT) as policysourceind,
		CAST(P.assumedcompanymappingkey as TEXT) as assumedcompanymappingkey,
		CAST(P.writingcompanymappingkey as NUMERIC) as writingcompanymappingkey,
		CAST(P.freetradezoneriskcode as TEXT) as freetradezoneriskcode,
		CAST(P.languagecd as TEXT) as languagecd,
		CAST(P.overridereasoncd as TEXT) as overridereasoncd,
		CAST(P.overridereasonnote as TEXT) as overridereasonnote,
		CAST(P.postedwweobjectid as NUMERIC) as postedwweobjectid,
		CAST(P.postedwwevnumber as NUMERIC) as postedwwevnumber,
		CAST(P.transactionstatus as TEXT) as transactionstatus,
		CAST(P.nextfollowupdate as TEXT) as nextfollowupdate,
		CAST(P.finaladjustmentind as NUMERIC) as finaladjustmentind,
		CAST(P.rateeditionname as TEXT) as rateeditionname,
		CAST(P.rateeditionversion as NUMERIC) as rateeditionversion,
		CAST(P.wipstatus as NUMERIC) as wipstatus,
		CAST(P.quoterelatedtranstype as TEXT) as quoterelatedtranstype,
		CAST(P.relatedtranstype as TEXT) as relatedtranstype,
		CAST(P.relatedtranseffdate as NUMERIC) as relatedtranseffdate,
		CAST(P.relatedtransseqno as NUMERIC) as relatedtransseqno,
		CAST(P.quotekey as TEXT) as quotekey,
		CAST(P.quotedate as NUMERIC) as quotedate,
		CAST(P.quotedaystoaccept as TEXT) as quotedaystoaccept,
		CAST(P.quoteexpirydate as NUMERIC) as quoteexpirydate,
		CAST(P.quotedefinedname as TEXT) as quotedefinedname,
		CAST(P.quoteseriescode as TEXT) as quoteseriescode,
		CAST(P.quotestatus as TEXT) as quotestatus,
		CAST(P.quotedocumentsprinted as TEXT) as quotedocumentsprinted,
		CAST(P.brkwritingcompanymappingkey as TEXT) as brkwritingcompanymappingkey,
		CAST(P.brokeragepolicycommission as NUMERIC) as brokeragepolicycommission,
		CAST(P.quickquoteind as TEXT) as quickquoteind,
		CAST(P.referralstatus as TEXT) as referralstatus,
		CAST(P.documentcategory as TEXT) as documentcategory,
		CAST(P.frontedcompanymappingkey as TEXT) as frontedcompanymappingkey,
		CAST(P.assumedcommissionamount as NUMERIC) as assumedcommissionamount,
		CAST(P.assumedcommissiontype as NUMERIC) as assumedcommissiontype,
		CAST(P.policycompositiontype as TEXT) as policycompositiontype,
		CAST(P.predominantjurisdiction as TEXT) as predominantjurisdiction,
		CAST(P.predominantpolicyregion as TEXT) as predominantpolicyregion,
		CAST(P.totalwaivedpremium as NUMERIC) as totalwaivedpremium,
		CAST(P.accountingdate as NUMERIC) as accountingdate,
		CAST(P.quotedelcinedby as TEXT) as quotedelcinedby,
		CAST(P.quotecompetitor as TEXT) as quotecompetitor,
		CAST(P.quotecompetitorpremium as NUMERIC) as quotecompetitorpremium,
		CAST(P.policycontactentitykey as TEXT) as policycontactentitykey, 
		CAST(P.policycontactentitykeyvnumber as NUMERIC) as policycontactentitykeyvnumber,
		CAST('inserted' as TEXT) as comments,
		CAST(P.renewalstatus as TEXT) as renewalstatus,
		CAST(P.insurancestatus as NUMERIC) as insurancestatus,
		CAST(P.liabilitylimit as NUMERIC) as liabilitylimit,
		CAST(P.overridemailingaddrkey as TEXT) as overridemailingaddrkey,
		CAST(P.auditreportingind as NUMERIC) as auditreportingind	,
		CAST(P.convertedtopolicytrneffdate as NUMERIC) as convertedtopolicytrneffdate,
		CAST(P.convertedtopolicytrnseqno as NUMERIC) as convertedtopolicytrnseqno,
		CAST(P.quotecompletedreasoncode as TEXT) as quotecompletedreasoncode,
		CAST(P.quotecompletedreasonnotes as TEXT) as quotecompletedreasonnotes,
		CAST(P.quotelastmodifieddatetime as NUMERIC) as quotelastmodifieddatetime,
		CAST(P.extendreporttermlength as TEXT) as extendreporttermlength,
		CAST(P.extendreportlengthunit as TEXT) as extendreportlengthunit,
		CAST(P.useextendreportexpoverrideind as NUMERIC) as useextendreportexpoverrideind
	FROM p_policy P
	WHERE transactiontype = 'RCA'
	  and conversionreference = 'KOENME_1988_2018'
	)
;