/******************************************************************************/
/*                      Conversion Problem Data                               */
/******************************************************************************/


-- Claims without policy coverage
select * from "Instance";
select 
    I."coverageDate",
    P."expirationDate",
    C."isTail",
    I.* 
from "Instance" I
    inner join "Claim" C on C."claimId" = I."instanceId"
    left join "Policy" P on P."policyId" = I."policyId"
where I."coverageDate" > P."expirationDate"
  and P."effectiveDate" > '31-DEC-2005'
  and C."isTail" is NULL
;