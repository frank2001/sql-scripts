-- ISI_interim_enable_constraints.sql

/*
-- Contact Model 
*/
-- e_contact
ALTER TABLE public.e_contact
    ADD CONSTRAINT pk_e_contact PRIMARY KEY (entitykey, entitykeyvnumber);
ALTER TABLE public.e_contact
    ADD CONSTRAINT c00_e_contact CHECK (isindividual = ANY (ARRAY[0::numeric, 1::numeric]));
ALTER TABLE public.e_contact
    ADD CONSTRAINT c01_e_contact CHECK ((gender = ANY (ARRAY[0::numeric, 1::numeric])) OR gender IS NULL);
ALTER TABLE public.e_contact
    ADD CONSTRAINT c02_e_contact UNIQUE (entitykey);
ALTER TABLE public.e_contact
    ADD CONSTRAINT c03_e_contact CHECK (
CASE
    WHEN isindividual = 0::numeric THEN
    CASE
        WHEN companyname IS NULL OR btrim(companyname::text) = ''::text THEN false
        ELSE true
    END
    WHEN isindividual = 1::numeric THEN
    CASE
        WHEN firstname IS NULL OR btrim(firstname::text) = ''::text OR lastname IS NULL OR btrim(lastname::text) = ''::text THEN false
        ELSE true
    END
    ELSE NULL::boolean
END);
ALTER TABLE public.e_contact
    ADD CONSTRAINT c04_e_contact CHECK ((maritalstatus = ANY (ARRAY[0::numeric, 1::numeric, 2::numeric, 3::numeric, 4::numeric, 5::numeric])) OR maritalstatus IS NULL);
ALTER TABLE public.e_contact
    ADD CONSTRAINT c05_e_contact CHECK (
CASE
    WHEN isindividual = 0::numeric THEN
    CASE
        WHEN firstname IS NOT NULL OR btrim(firstname::text) <> ''::text OR middlename IS NOT NULL OR btrim(middlename::text) <> ''::text OR lastname IS NOT NULL OR btrim(lastname::text) <> ''::text THEN false
        ELSE true
    END
    WHEN isindividual = 1::numeric THEN
    CASE
        WHEN companyname IS NOT NULL OR btrim(companyname::text) <> ''::text THEN false
        ELSE true
    END
    ELSE NULL::boolean
END);
ALTER TABLE public.e_contact
    ADD CONSTRAINT c06_e_contact CHECK ((preferredhtc = ANY (ARRAY[1::numeric, 2::numeric, 3::numeric, 5::numeric, 6::numeric, 7::numeric, 8::numeric, 99::numeric])) OR preferredhtc IS NULL);
	
ALTER TABLE public.e_contact DISABLE TRIGGER ALL;

---------------------------------

