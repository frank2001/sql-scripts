/*
-- ISI_interim_delete_recs.sql
--
-- script clears ISI interim table records prior to data refresh
--
-- created:	FZ	08/15/2017
--
*/


-- e_contact
delete from public.e_contact;
