/*
--	ISI Data Conversion Data Checks
*/

/*
-- e_contact
*/
select * from "ISI_ClientView";
select "entitykey"
from "ISI_ClientView"
group by "entitykey"
having count(*) > 1
order by "entitykey";


/*
--	p_policy
*/
--pk
select "conversionreference", "transactioneffectivedate", "transactionseqno", "transactiontype", "sbuid"
from "ISI_PolicyView"
group by  "conversionreference", "transactioneffectivedate", "transactionseqno", "transactiontype", "sbuid"
having count(*) > 1
order by  "conversionreference", "transactioneffectivedate", "transactionseqno", "transactiontype", "sbuid";

--c11
select * from "ISI_PolicyView" where "transactiontype" = 'RCA' and "relatedrevrcatranstype" is NULL;
--c14
select * from "ISI_Policy" where "veffectivedate" < "policyeffectivedate" or "veffectivedate" > "policyexpirydate";



--f89


/*
-- p_billingdetail
*/
--pk_p_billingdetail
select "POLICYNUMBER", "conversionreference", "sbuid", "policyeffectivedate", "policyexpirydate"
from "ISI_BillingDetailView"
group by "POLICYNUMBER", "conversionreference", "sbuid", "policyeffectivedate", "policyexpirydate"
having count(*) > 1
order by "conversionreference"
;

select * from "ISI_BillingDetailView" where "POLICYNUMBER" = '0702005';
select * from "ISI_BillingDetailView" where "conversionreference" = 'STERSA_1991_2019';

/*
-- p_paymentschedule
*/
--pk_p_paymentschedule
select "POLICYNUMBER", "conversionreference", "sbuid", "policyeffectivedate", "policyexpirydate", "scheduledate", "transactioneffectivedate", "transactionseqno"
from "ISI_PaymentScheduleView"
group by "POLICYNUMBER", "conversionreference", "sbuid", "policyeffectivedate", "policyexpirydate", "scheduledate", "transactioneffectivedate", "transactionseqno"
having count(*) > 1
order by "conversionreference"
;
select * from "ISI_PaymentScheduleView" where "conversionreference" = 'DEHODA_2005_2019' and "policyeffectivedate" = '20100331' order by "policyeffectivedate", "scheduledate";  --DEHODA_2005_2019, 4112, 20100331, 20110331, 20101130, 20100331, 1

select "conversionreference", "sbuid", "policyeffectivedate", "policyexpirydate", "scheduledate", "transactioneffectivedate", "transactionseqno"
from "ISI_PaymentScheduleFPView"
group by "conversionreference", "sbuid", "policyeffectivedate", "policyexpirydate", "scheduledate", "transactioneffectivedate", "transactionseqno"
having count(*) > 1
order by "conversionreference"
;

--f10_p_paymentschedule   (conversionreference, policyeffectivedate, policyexpirydate, sbuid)=(RUCKCL_2007_2007, 20070201, 20080201, 4112) is not present in table "p_billingdetail".
select PS."POLICYNUMBER", PS."conversionreference", PS."policyeffectivedate", PS."policyexpirydate", PS."sbuid"
from "ISI_PaymentScheduleView" PS where not exists
    (select "conversionreference", "policyeffectivedate", "policyexpirydate", "sbuid" from "ISI_BillingDetailView"
        where PS."POLICYNUMBER" = "POLICYNUMBER"
          and PS."conversionreference" = "conversionreference"
          and PS."policyeffectivedate" = "policyeffectivedate"
          and PS."policyexpirydate" = "policyexpirydate"
          and PS."sbuid" = "sbuid");

select * from "ISIConversionReferenceView" where "conversionreference" = 'BUCKMA_2010_2010';  --BUCKMA_2010_2010	20100222	20110222	4112
select * from "ISI_PolicyView" where "conversionreference" = 'BUCKMA_2010_2010';  --policeffectivedate=20100222	policyexpirtydate=20110222	veffectivedate=20100916	vexpirydate=20100916
select * from "ISI_BillingDetailView" where "conversionreference" = 'BUCKMA_2010_2010'; --policyeffectivedate=20100222	policyexpirydate=20100916
select * from "ISIConversionReferenceView" where "conversionreference" = 'ABSOTE_2007_2013'; --ABSOTE_2007_2013	20101213	20110625	4112
select * from "ISI_PolicyView" where "conversionreference" = 'ABSOTE_2007_2013';
select * from "Policy" where "policyNumber" = '1012044';
select * from  "ISIFirmPolicyView" where "policyNumber" = '1012044'; 
select * from "Endorsement" where "policyId" = 'AP000257759';
select DISTINCT "policyId", "effectiveDate", min("endorsementNumber") as "endorsementNumber" from "Endorsement" where "policyId" = 'AP000257759' group by "policyId", "effectiveDate" order by "effectiveDate";
select * from "ISI_BillingDetailView" where "conversionreference" = 'ABSOTE_2007_2013';


select PSFP."POLICYNUMBER", PSFP."conversionreference", PSFP."policyeffectivedate", PSFP."policyexpirydate", PSFP."sbuid"
from "ISI_PaymentScheduleFPView" PSFP where not exists
    (select "POLICYNUMBER", "conversionreference", "policyeffectivedate", "policyexpirydate", "sbuid" from "ISI_BillingDetailView"
        where PSFP."POLICYNUMBER" = "POLICYNUMBER"
          and PSFP."conversionreference" = "conversionreference"
          and PSFP."policyeffectivedate" = "policyeffectivedate"
          and PSFP."policyexpirydate" = "policyexpirydate"
          and PSFP."sbuid" = "sbuid");


/*
--	p_insureds
*/
--pk_p_insureds
select "conversionreference", "transactioneffectivedate", "transactionseqno", "transactiontype", "sbuid", "insuredid"
from "ISI_InsuredsFirmView"
--where "insuredrole" = 7
group by "conversionreference", "transactioneffectivedate", "transactionseqno", "transactiontype", "sbuid", "insuredid"
having count(*) > 1
order by "conversionreference", "transactioneffectivedate"
;

-- idx1_p_insureds
select "conversionreference", "POLICYNUMBER", "transactioneffectivedate", "transactionseqno", "transactiontype", "sbuid", "contactentitykey"
from "ISI_InsuredsView"
where "insuredrole" = 7
group by "conversionreference", "POLICYNUMBER", "transactioneffectivedate", "transactionseqno", "transactiontype", "sbuid", "contactentitykey"
having count(*) > 1
order by "conversionreference", "transactioneffectivedate"
;

--null checks
select * from "ISI_InsuredsView" where "insuredid" is NULL;

/*
-- p_items
*/
--pk_p_items
select "conversionreference", "transactioneffectivedate", "transactionseqno", "transactiontype", "sbuid", "packageid", "riskid"
from "ISI_ItemView"
--where "insuredrole" = 7
group by "conversionreference", "transactioneffectivedate", "transactionseqno", "transactiontype", "sbuid", "packageid", "riskid"
having count(*) > 1
order by "conversionreference", "transactioneffectivedate"
;

--idx6_p_items

--c06
select * from "ISI_ItemView" where ("transactiontype" = 'CAN' and "veffectivedate" > "vexpirydate") or ("transactiontype" = 'RCA' and "veffectivedate" <> "vexpirydate");


/*
-- i_lawfirm_1
*/
--f01_i_lawfirm_1
select LFV."conversionreference", LFV."transactioneffectivedate", LFV."transactionseqno", LFV."transactiontype", LFV."sbuid", LFV."packageid", LFV."riskid"
from "ISI_LawFirm1View" LFV where not exists
    (select "conversionreference", "transactioneffectivedate", "transactionseqno", "transactiontype", "sbuid", "packageid", "riskid" from "ISI_ItemView" 
        where LFV."conversionreference" = "conversionreference"
          and LFV."transactioneffectivedate" = "transactioneffectivedate"
          and LFV."transactionseqno" = "transactionseqno"
          and LFV."transactiontype" = "transactiontype"
          and LFV."sbuid" = "sbuid"
          and LFV."packageid" = "packageid"
          and LFV."riskid" = "riskid");


		  
		  
		  
/*
-- p_coverages
*/
select "conversionreference", "transactioneffectivedate", "transactionseqno", "transactiontype", "sbuid", "packageid", "riskid", "linenumber", "coverageid"
from "ISI_CoverageLPLView"
group by "conversionreference", "transactioneffectivedate", "transactionseqno", "transactiontype", "sbuid", "packageid", "riskid", "linenumber", "coverageid"
having count(*) > 1
order by "conversionreference", "transactioneffectivedate", "transactionseqno", "transactiontype", "sbuid", "packageid", "riskid", "linenumber", "coverageid"
;


select * from "ISI_CoverageLPLLawyerView";



/*
-- p_inlineschedules
*/
--pk_p_inlineschedules
select "conversionreference","transactiontype","transactioneffectivedate","transactionseqno","sbuid","packageid","riskid","inlinecategory","linenumber"
from "ISI_InlineSchedulesView"
group by "conversionreference","transactiontype","transactioneffectivedate","transactionseqno","sbuid","packageid","riskid","inlinecategory","linenumber"
having count(*) > 1
order by "conversionreference","transactiontype","transactioneffectivedate","transactionseqno","sbuid","packageid","riskid","inlinecategory","linenumber"
;


/*
-- is_areaofpractice_1
*/
select "conversionreference", "transactioneffectivedate", "transactionseqno", "transactiontype", "sbuid", "packageid", "riskid", "inlinecategory", "linenumber"
from "ISI_AreaOfPractice1View"
group by "conversionreference", "transactioneffectivedate", "transactionseqno", "transactiontype", "sbuid", "packageid", "riskid", "inlinecategory", "linenumber"
having count(*) > 1
order by "conversionreference", "transactioneffectivedate", "transactionseqno", "transactiontype", "sbuid", "packageid", "riskid", "inlinecategory", "linenumber"
;


/*
-- is_quotelimits_1
*/
-- pk_is_quotelimits_1
select "conversionreference", "transactioneffectivedate", "transactionseqno", "transactiontype", "sbuid", "packageid", "riskid", "inlinecategory", "linenumber"
from "ISI_QuoteLimits1View"
group by "conversionreference", "transactioneffectivedate", "transactionseqno", "transactiontype", "sbuid", "packageid", "riskid", "inlinecategory", "linenumber"
having count(*) > 1
order by "conversionreference", "transactioneffectivedate", "transactionseqno", "transactiontype", "sbuid", "packageid", "riskid", "inlinecategory", "linenumber"
;

/*
-- is_quotelimitsattorney_1
*/
-- pk_is_quotelimitsattorney_1
select "conversionreference", "transactioneffectivedate", "transactionseqno", "transactiontype", "sbuid", "packageid", "riskid", "inlinecategory", "linenumber"
from "ISI_QuoteLimitsAttorneyView"
group by "conversionreference", "transactioneffectivedate", "transactionseqno", "transactiontype", "sbuid", "packageid", "riskid", "inlinecategory", "linenumber"
having count(*) > 1
order by "conversionreference", "transactioneffectivedate", "transactionseqno", "transactiontype", "sbuid", "packageid", "riskid", "inlinecategory", "linenumber"
;

/*
-
--
--- C L A I M S
--
-
*/ 

/*
-- c_claim
*/
-- date check
select * from "ISI_ClaimView" where "occurrencedate" > "reporteddate";




/*
-- c_items
*/
-- claims with missing item records
select * from "ISI_ClaimView" C where C."claimmappingkey" not in (select "claimmappingkey" from "ISI_ClaimItemLPLView");

/*
-- c_involvedparty
*/
select * from "ISI_InvolvedPartyView" IP where IP."claimmappingkey" not in (select "claimmappingkey" from "ISI_ClaimView");

/*
-- c_transaction
*/
-- pk_c_claimtr
select "claimmappingkey", "claimvnumber", "transmappingkey"
from "ISI_ClaimTransactionView"
group by "claimmappingkey", "claimvnumber", "transmappingkey"
having count(*) > 1
order by "claimmappingkey", "claimvnumber", "transmappingkey"
;

-- claims with missing transaction records 
select * from "ISI_ClaimView" C where C."claimmappingkey" not in (select "claimmappingkey" from "ISI_ClaimTransactionView");
/*
--c_cl_clmtlitigation_1
*/
-- f01_c_cl_clmtlitigation_1
select CL."claimmappingkey", CL."claimvnumber", CL."polconversionref", CL."poltranseffdate", CL."poltransseqno", CL."poltranstype", CL."sbuid", CL."packageid", CL."riskid", CL."linenumber", CL."coverageid", CL."invno", CL."subfilekey", CL."claimtypekey", CL."workareakey"
from "ISI_ClaimantLitigationView" CL 
    where not exists
    (select "claimmappingkey", "claimvnumber", "polconversionref", "poltranseffdate", "poltransseqno", "poltranstype", "sbuid", "packageid", "riskid", "linenumber", "coverageid", "invno", "subfilekey", "claimtypekey", "workareakey"
        from "ISI_ClaimantWorkAreaView"
        where CL."claimmappingkey" = "claimmappingkey"
           and CL."claimvnumber" = "claimvnumber" 
           and CL."polconversionref" = "polconversionref"
           and CL."poltranseffdate" = "poltranseffdate" 
           and CL."poltransseqno" = "poltransseqno" 
           and CL."poltranstype" = "poltranstype" 
           and CL."sbuid" = "sbuid" 
           and CL."packageid" = "packageid"
           and CL."riskid" = "riskid" 
           and CL."linenumber" = "linenumber" 
           and CL."coverageid" = "coverageid" 
           and CL."invno" = "invno" 
           and CL."subfilekey" = "subfilekey" 
           and CL."claimtypekey" = "claimtypekey" 
           and CL."workareakey" = "workareakey"
    );
