select DISTINCT
		iv."conversionreference" as "conversionreference",
        iv."transactiontype" as "transactiontype",
        iv."transactioneffectivedate" as "transactioneffectivedate",
        iv."transactionseqno" as "transactionseqno",
        p."policyNumber" as "POLICYNUMBER",
        iv."EVENTTYPEID" as "EVENTTYPEID",
        /* LP Use Carry Forward Join */
        CASE
            WHEN iv."transactiontype" = 'END' AND carry_forward_cov."EVENTTYPEID" = 'Cvg-DF' THEN 'LPL__ATTORNEY__WLMEND20_WI_19000101_20180301_4112'  --DF
            WHEN iv."transactiontype" = 'END' AND carry_forward_cov."EVENTTYPEID" = 'Cvg-GC' THEN 'LPL__ATTORNEY__WLMEND28_WI_19000101_20180301_4112'  --GC
            --WHEN iv."transactiontype" = 'END' AND carry_forward_cov."EVENTTYPEID" = 'Cvg-SC' THEN 'LPL__ATTORNEY__WLMEND01_WI_19000101_20991231_4112'  --SEC
            WHEN iv."transactiontype" = 'END' AND carry_forward_cov."EVENTTYPEID" = 'Cvg-EL' THEN 'LPL__ATTORNEY__WLMEND03_WI_19000101_20991231_4112'  --exclude other entities
            WHEN iv."transactiontype" = 'END' AND carry_forward_cov."EVENTTYPEID" = 'Cvg-AT' THEN 'LPL__ATTORNEY__WLMEND24_WI_19000101_20991231_4112'  --abstracter
            WHEN iv."transactiontype" = 'END' AND carry_forward_cov."EVENTTYPEID" = 'Cvg-EV' THEN 'LPL__ATTORNEY__WLMEND25_WI_19000101_20991231_4112'  --exclude vicarious liability
            WHEN iv."transactiontype" = 'END' AND carry_forward_cov."EVENTTYPEID" = 'Cvg-MN' THEN 'LPL__ATTORNEY__WLMENDMN_WI_19000101_20991231_4112'  --manuascript
            WHEN iv."transactiontype" = 'END' AND carry_forward_cov."EVENTTYPEID" = 'Cvg-AB' THEN 'LPL__ATTORNEY__WLMEND30_WI_20180101_20991231_4112'  --additional benefits
            ELSE 'LPL__ATTORNEY__PL_0_WI_19000101_20991231_4112' --attorney
        END AS "pcmmappingkey",
        iv."sbuid" as "sbuid",
        0 as "packageid",
        iv."riskid" as "riskid",
        0 as "linenumber",
        /* LP Use Carry Forward Join */
        CASE
            WHEN iv."transactiontype" = 'END' AND carry_forward_cov."EVENTTYPEID" = 'Cvg-DF' THEN 'WLMEND20'  --DF
            WHEN iv."transactiontype" = 'END' AND carry_forward_cov."EVENTTYPEID" = 'Cvg-GC' THEN 'WLMEND28'  --GC
            --WHEN iv."transactiontype" = 'END' AND carry_forward_cov."EVENTTYPEID" = 'Cvg-SC' THEN 'WLMEND01'  --SEC
            WHEN iv."transactiontype" = 'END' AND carry_forward_cov."EVENTTYPEID" = 'Cvg-EL' THEN 'WLMEND03'  --exclude other entities
            WHEN iv."transactiontype" = 'END' AND carry_forward_cov."EVENTTYPEID" = 'Cvg-AT' THEN 'WLMEND24'  --abstracter
            WHEN iv."transactiontype" = 'END' AND carry_forward_cov."EVENTTYPEID" = 'Cvg-EV' THEN 'WLMEND25'  --exclude vicarious liability
            WHEN iv."transactiontype" = 'END' AND carry_forward_cov."EVENTTYPEID" = 'Cvg-MN' THEN 'WLMENDMN'  --manuascript
            WHEN iv."transactiontype" = 'END' AND carry_forward_cov."EVENTTYPEID" = 'Cvg-AB' THEN 'WLMEND30'  --additional benefits
            ELSE 'PL_0' 
        END as "coverageid",
		CAST(NULL as VARCHAR(4)) as "basisofsettlement",
        CAST(NULL as VARCHAR(8)) as "coinsurance",
        CAST(NULL as VARCHAR(20)) as "rategroup",
        CAST(NULL as VARCHAR(20)) as "drivingrecord",
        CAST(NULL as INT) as "rateper",
        CAST(NULL as INT) as "baseratefactor",
        CAST(NULL as INT) as "overrideratefactor",
        CASE
            WHEN carry_forward_cov."EVENTTYPEID" IN ('PolicyIssue','Cvg-AA','Cvg-DA') THEN p."perClaimLimit"
            WHEN carry_forward_cov."EVENTTYPEID" = 'Cvg-GC' THEN 5000
            ELSE 0 
        END as "limit1",
        CASE
            WHEN carry_forward_cov."EVENTTYPEID" IN ('PolicyIssue','Cvg-AA','Cvg-DA') THEN p."aggregateLimit"
            ELSE 0 
        END as "limit2",
        'USD' as "limitcurrencycd",
        CASE
            WHEN carry_forward_cov."EVENTTYPEID" IN ('PolicyIssue','Cvg-AA','Cvg-DA') THEN p."perClaimDeductible"
            ELSE 0 
        END as "deductible1", 
        0 as "deductible2",
        'USD' as "premiumcurrencycd",
        CAST(NULL as INT) as "ratedbasepremium",
        CAST(NULL as INT) as "ratedgrosspremium",
        CAST(NULL as INT) as "ratednetpremium", 
        ROUND(CASE 
			WHEN (((COALESCE(pplv."premiumPerLawyer1M",0) + COALESCE(pplv."premiumPerLawyerXS5M",0) + COALESCE(pplv."premiumPerLawyerXS10M",0) - COALESCE(pplv."DefReimbCoverage",0)) * 12) <> 0) AND ((COALESCE(iv."POLICYTERM",-12) * -1) <> 0)
            THEN ( ((COALESCE(pplv."premiumPerLawyer1M",0) + COALESCE(pplv."premiumPerLawyerXS5M",0) + COALESCE(pplv."premiumPerLawyerXS10M",0) - COALESCE(pplv."DefReimbCoverage",0)) * 12) / (COALESCE(iv."POLICYTERM",-12) * -1) )
          ELSE 0
        END,2) as "annualpremium",
        CASE
            WHEN iv."transactiontype" in ('NEW', 'REN', 'RWN', 'RWR') THEN COALESCE(pplv."premiumPerLawyer1M",0) + COALESCE(pplv."premiumPerLawyerXS5M",0) + COALESCE(pplv."premiumPerLawyerXS10M",0) - COALESCE(pplv."DefReimbCoverage",0)
            WHEN iv."transactiontype" = 'END' and iv."EVENTTYPEID" = 'Cvg-DF' THEN COALESCE(pplv."DefReimbCoverage",0)
            --WHEN iv."transactiontype" = 'END' and iv."EVENTTYPEID" = 'Cvg-SC' THEN COALESCE(pplv."SECCoverage", 0)
            --ELSE COALESCE(pa."premiumAdjust", 0)
            ELSE 0
        END as "transactionpremium",
        0 as "nonpremiumsurchargeamount",
        0 as "premiumsurchargeamount",
        0 as "transactionsurcharge",
        0 as "nonpremiumdiscountamount",
        0 as "premiumdiscountamount",
        0 as "transactiondiscount",
        0 as "commissionamount",
        CAST(NULL as VARCHAR(4)) as "numberof1",
        CAST(NULL as VARCHAR(4)) as "valueof1",
        CAST(NULL as VARCHAR(4)) as "typeof1",
        CAST(NULL as VARCHAR(4)) as "numberof2",
        CAST(NULL as VARCHAR(4)) as "valueof2",
        CAST(NULL as VARCHAR(4)) as "typeof2", 
        iv."deletedrow",
		CAST(NULL as VARCHAR(4)) as "conversionserver",
        CAST(NULL as VARCHAR(4)) as "sequencenumber", 
        iv."veffectivedate" as "veffectivedate",
        iv."vexpirydate" as "vexpirydate",
        iv."claimsmadeind" as "claimsmadeind",
        COALESCE(iv."retrodate", TO_CHAR(p."effectiveDate", 'YYYYMMDD')) as "retrodate",
        CAST(NULL as INT) as "postedwweobjectid",
        CAST(NULL as VARCHAR(4)) as "limittierinfo",
        -- only added to transactionseqno=1
        COALESCE(pplvc."premiumPerLawyer1M",0) + COALESCE(pplvc."premiumPerLawyerXS5M",0) + COALESCE(pplvc."premiumPerLawyerXS10M",0) - COALESCE(pplvc."DefReimbCoverage", 0) as "ratedftbasepremium",
        COALESCE(pplvc."premiumPerLawyer1M",0) + COALESCE(pplvc."premiumPerLawyerXS5M",0) + COALESCE(pplvc."premiumPerLawyerXS10M",0) - COALESCE(pplvc."DefReimbCoverage", 0) as "ratedftgrosspremium",
        COALESCE(pplvc."premiumPerLawyer1M",0) + COALESCE(pplvc."premiumPerLawyerXS5M",0) + COALESCE(pplvc."premiumPerLawyerXS10M",0) - COALESCE(pplvc."DefReimbCoverage", 0) as "ratedftpremium",
        COALESCE(pplvc."premiumPerLawyer1M",0) + COALESCE(pplvc."premiumPerLawyerXS5M",0) + COALESCE(pplvc."premiumPerLawyerXS10M",0) - COALESCE(pplvc."DefReimbCoverage", 0) as "ratedannualpremium",
        CASE
            WHEN iv."transactiontype" in ('NEW', 'REN', 'RWN', 'RWR') THEN COALESCE(pplv."premiumPerLawyer1M",0) + COALESCE(pplv."premiumPerLawyerXS5M",0) + COALESCE(pplv."premiumPerLawyerXS10M",0) 
            WHEN iv."transactiontype" = 'END' and iv."EVENTTYPEID" = 'Cvg-DF' THEN COALESCE(pplv."DefReimbCoverage",0)
            --WHEN iv."transactiontype" = 'END' and iv."EVENTTYPEID" = 'Cvg-SC' THEN COALESCE(pplv."SECCoverage", 0)
            --ELSE COALESCE(pa."premiumAdjust", 0)
            ELSE 0
        END as "ratedtransactionpremium",
        0 as "ratedcommissionpercent",
        0 as "ratedcommissionamount",
        /* CASE
            WHEN iv.transactiontype in ('NEW', 'REN', 'RWN', 'RWR') THEN COALESCE(pplvc.premiumPerLawyer1M,0) + COALESCE(pplvc.premiumPerLawyerXS5M,0) + COALESCE(pplvc.premiumPerLawyerXS10M,0) - COALESCE(pplvc.DefReimbCoverage, 0) 
            WHEN iv.transactiontype = 'END' and carry_forward_cov.EVENTTYPEID = 'Cvg-DF' THEN COALESCE(pplvc.DefReimbCoverage,0)
            --WHEN iv.transactiontype = 'END' and carry_forward_cov.EVENTTYPEID = 'Cvg-SC' THEN COALESCE(pplvc.SECCoverage, 0)
            ELSE COALESCE(pa.premiumAdjust, 0)
        END */ 
        COALESCE(pplvc."premiumPerLawyer1M",0) + COALESCE(pplvc."premiumPerLawyerXS5M",0) + COALESCE(pplvc."premiumPerLawyerXS10M",0) - COALESCE(pplvc."DefReimbCoverage", 0) as "ftpremium",
        0 as "offsetftpremium",
        1 as "commissiontype",
        0 as "commissionpercent",
        1.0000000 as "transactionpremiumfactor",
        0 as "transactionpremiumdlyrate",
        CAST(NULL as INT) as "overriderule",
        CAST(NULL as INT) as "overrideftpremium",
        CAST(NULL as INT) as "overrideannualpremium",
        CAST(NULL as INT) as "overridetransactionpremium",
        CAST(NULL as INT) as "overridecommissionpercent",
        CAST(NULL as INT) as "overridecommissionamount",
        0 as "taxamount",
        CAST(NULL as VARCHAR(4)) as "gldiv",
        CAST(NULL as VARCHAR(4)) as "gldept",
        CAST(NULL as VARCHAR(4)) as "gllob",
        0 as "siretention1",
        0 as "siretention2",
        CAST(NULL as VARCHAR(4)) as "mappingcode",
        CAST(NULL as VARCHAR(4)) as "submappingcode",
        CAST(NULL as VARCHAR(4)) as "submappingcode2",
        CAST(NULL as INT) as "netrate",
        CAST(NULL as INT) as "grossrate",
        CAST(NULL as VARCHAR(4)) as "rateexposure",
        0 as "adjbilltypeind",
        CAST(NULL as INT) as "waivedtransactionpremium",
        CAST(NULL as VARCHAR(4)) as "classcode",
        CAST(NULL as VARCHAR(4)) as "iso_statecode",
        CAST(NULL as VARCHAR(4)) as "iso_zipcode",
        CAST(NULL as VARCHAR(4)) as "iso_ratingterritory",
        CAST(NULL as VARCHAR(4)) as "iso_typeofpolicycode",
        CAST(NULL as VARCHAR(4)) as "iso_aslobstatcode",
        CAST(NULL as VARCHAR(4)) as "iso_sublinecode",
        CAST(NULL as VARCHAR(4)) as "iso_classificationcode",
        CAST(NULL as VARCHAR(4)) as "iso_coveragestatcode",
        CAST(NULL as VARCHAR(4)) as "iso_ratingidentificationcode",
        CAST(NULL as VARCHAR(4)) as "iso_constructioncode",
        CAST(NULL as VARCHAR(4)) as "iso_fireprotectioncode",
        CAST(NULL as VARCHAR(4)) as "iso_terrorismcoveragecode",
        CAST(NULL as VARCHAR(4)) as "iso_windhaildedcode",
        CAST(NULL as VARCHAR(4)) as "iso_bcegclass",
        CAST(NULL as VARCHAR(4)) as "iso_ratingbasiscode",
        CAST(NULL as VARCHAR(4)) as "iso_liabilityformcode",
        CAST(NULL as VARCHAR(4)) as "iso_molddamagecode",
        CAST(NULL as VARCHAR(4)) as "iso_liabilityexposureindcode",
        CAST(NULL as INT) as "iso_exposurestatamount",
        CAST(NULL as VARCHAR(4)) as "iso_ratingmodificationfactor",
        CAST(NULL as VARCHAR(4)) as "iso_stateexceptionindcode",
        CAST(NULL as VARCHAR(4)) as "iso_bussincomeexpensecode",
        CAST(NULL as VARCHAR(4)) as "iso_liabcovindcode",
        CAST(NULL as VARCHAR(4)) as "iso_pctowneroccupied",
        CAST(NULL as VARCHAR(4)) as "iso_classcodedesc", 
        TO_CHAR(p."effectiveDate", 'YYYYMMDD') as "rateeffectivedate",
        'FL' as "deductibletype",
        CAST(NULL as VARCHAR(4)) as "iso_formcode",
        CAST(NULL as VARCHAR(4)) as "iso_losscostmultiplier",
        CAST(NULL as VARCHAR(4)) as "iso_losscostdate", 
        CAST(NULL as VARCHAR(4)) as "iso_yearofconstruction"
    from (select * from "ISI_ItemLawyer" where "transactiontype" <> 'ERP') iv
        left join "Policy" p on p."policyNumber" = iv."POLICYNUMBER"
            left join "PremiumPerLawyer" pplv on pplv."policyId" = p."policyId" and pplv."effectiveDate" = p."effectiveDate" and pplv."lawyerId" = iv."LAWYERID" and pplv."aggregateLimit" = p."aggregateLimit" and pplv."Limit" = p."perClaimLimit" and pplv."Deductible" = p."perClaimDeductible"
        left join "PolicyAccounting" pa on pa."coverageId" = iv."COVERAGEID" and iv."transactiontype" in ('END', 'CAN', 'RCA')  
        LEFT JOIN (select * from "ISI_ItemLawyer" where "transactiontype" <> 'ERP') carry_forward_cov
                       ON iv."POLICYNUMBER" = carry_forward_cov."POLICYNUMBER"
                          AND iv."LAWYERID" = carry_forward_cov."LAWYERID"
                          AND (iv."transactioneffectivedate" * 100) + iv."transactionseqno" >= (carry_forward_cov."transactioneffectivedate" * 100) + carry_forward_cov."transactionseqno"
            left join "PremiumPerLawyer" pplvc on pplvc."policyId" = p."policyId" and pplvc."effectiveDate" = p."effectiveDate" and pplvc."lawyerId" = carry_forward_cov."LAWYERID" and pplvc."aggregateLimit" = p."aggregateLimit" and pplvc."Limit" = p."perClaimLimit" and pplvc."Deductible" = p."perClaimDeductible" 
WHERE iv."POLICYNUMBER" = '0607190' 
    order by "POLICYNUMBER", "transactioneffectivedate", "transactionseqno", "riskid", "coverageid";

 

SELECT * FROM Policy p WHERE p.policyNumber  = '0607190';

 

SELECT * FROM PolicyAccounting WHERE coverageId IN (SELECT COVERAGEID FROM ISI_ItemLawyerView WHERE POLICYINT = '0607190');

 

SELECT * FROM PremiumPerLawyerView WHERE policyId IN (SELECT policyid FROM Policy WHERE policyNumber = '0607190');