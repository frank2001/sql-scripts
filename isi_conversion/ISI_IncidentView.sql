/*
    ISI_IncidentView
    c_claim
    
    Dependancies: 
    
    NOTES:
     
    History
    05/14/2018	FZ	Created via ISI_ClaimView
*/
create or replace view "ISI_IncidentView" as
    select
        C."claimId" as "claimmappingkey",
        0 as "claimvnumber",
        '4112' as "sbuid",
        C."claimId" as "claimkey",
        'LAW_PL_19000101_21121231_4112' as "claimtypekey",
        I."firmId" as "cliententitymappingkey",
        0 as "cliententitykeyvnumber",
        TO_CHAR(I."occurDate", 'YYYYMMDD') as "occurrencedate",
        '000000' as "occurrencetime",
        TO_CHAR(C."claimMadeDate", 'YYYYMMDD') as "reporteddate",  --IMPORTANT 
        '000000' as "reportedtime",
        CASE
            WHEN I."reOpenDate" is not NULL and COALESCE(I."closeDate",I."openDate") < I."reOpenDate" THEN 'R'
            WHEN (I."reOpenDate" is not NULL and I."closeDate" >= I."reOpenDate") or (I."closeDate" is not NULL) THEN 'C'
            ELSE 'O'
         END as "claimstatus",
        TO_CHAR(E."eventDate", 'YYYYMMDDHHMMSS') as "ventrydatetime",
         CASE
            WHEN E."createUserId" = 'Joyce' THEN 'jneumaier'
            WHEN E."createUserId" = 'Matt' THEN 'mbeier'
            WHEN E."createUserId" = 'Katja' THEN 'kkunzke'
            WHEN E."createUserId" = 'Brian' THEN 'banderson'
 --REPLACE once sanderson available           WHEN E."createUserId" = 'Sally' THEN 'sanderson'
            WHEN E."createUserId" = 'Sally' THEN 'banderson'
            ELSE '>>unknown<<'||COALESCE(I."adjuster", 'NULL') --QFC, NDZ, WHD
        END as "vuserid",
        CTP."conversionreference" as "polconversionref",
        CTP."transactiontype" as "poltranstype",
        CTP."transactioneffectivedate" as "poltranseffdate",
        CTP."transactionseqno" as "poltransseqno",
        NULL as "prevclaimno",
        'CST' as "losstimezone",
        'CST' as "reportedtimezone",
        CASE
            WHEN (I."reOpenDate" is not NULL and I."closeDate" >= I."reOpenDate") or (I."closeDate" is not NULL) THEN TO_CHAR(I."closeDate", 'YYYYMMDD')
            ELSE NULL
        END as "closedate",
        CASE
            WHEN I."reOpenDate" is not NULL and I."closeDate" < I."reOpenDate" THEN TO_CHAR(I."reOpenDate", 'YYYYMMDD')
            ELSE NULL
        END as "reopendate",
        1 as "chargeableind",
        NULL as "eventid",
        NULL as "occurrenceid",
        0 as "policecalledind",
        NULL as "policefileno",
        NULL as "lossdescription",  --NEED loss description
        NULL as "lossnotes",
        CASE
            WHEN I."reOpenDate" is not NULL and I."closeDate" < I."reOpenDate" THEN 10
            ELSE NULL
        END as "reopenreason",
        1 as "claimsmadeind",
        10 as "reportedmethod", --10-other, 2-fax, 3-phone, 4-email, 5-mail
        30 as "reportingtype", --10=claimant, 20-claimant attorney, 30-insured, 99-other
        NULL as "branch",
        NULL as "refid",  ---?????
        0 as "totallossind",
        NULL as "conversionserver",
        NULL as "sequencenumber",
        NULL as "reopenreasonother",
        TO_CHAR(I."closeDate", 'HHMMSS') as "closetime",
        CASE
            WHEN C."court" is not NULL THEN 1
            ELSE 0
        END as "litigationind", 
        0 as "subrogationchk",
        NULL as "subrogationtext",
        NULL as "packageid",
        NULL as "spoilreasonother",
        NULL as "spoilreason",
        REPLACE(C."claimId", '-', '') as "ouid",
        NULL as "reportedother",
        NULL as "reportingtypeother",
        NULL as "closereason"
    from "Claim" C
        left join "Instance" I on I."instanceId" = C."claimId"
            left join "Policy" P on P."policyId" = I."policyId"
            left join "Event" E on E."eventId" = I."eventId"
        inner join "ISIClaimToPolicyView" CTP on CTP."instanceId" = C."claimId"
 --- SCOPE 
    where P."effectiveDate" > '31-DEC-2005'  
      and I."coverageDate" > '31-DEC-2006'
    -- REQUIRED FIXES
      and C."claimId" not in ('08-0031', '11-0001', '17-0011')

order by C."claimId"