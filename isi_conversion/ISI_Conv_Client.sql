/*******************************************************************************
	ISI_Conv_Client.sql
    
    ISI Conversion Scripts
	Scripts used to extract LAW data into ISI interim conversion tables
	
	Naming convention = 'ISI_'+ ISI tablename minus leading alpha underscore + 'View'
	
	Created: FZ	09/29/2017
	Revised: 
*******************************************************************************/
--work area
-- FirmPolicyGapView
CREATE OR REPLACE FORCE VIEW "LAWUA"."FirmPolicyGapView" ("firmId", "effectiveDate", "startYear", "endYear") AS 
  select l."firmId", l."effectiveDate", extract(YEAR from l."effectiveDate") as "startYear",
        (
            select min(extract(YEAR from a."effectiveDate")) as "startYear"
            from "Policy" a
                left outer join "Policy" b on a."firmId" = b."firmId" and  extract(YEAR from a."effectiveDate") = (extract(YEAR from b."effectiveDate") - 1)
            where extract(YEAR from b."effectiveDate") is NULL 
              and extract(YEAR from a."effectiveDate") >= extract(YEAR from l."effectiveDate")
              and a."firmId" = l."firmId"
        ) as "endYear"
    from "Policy" l
        left outer join "Policy" r on r."firmId" = l."firmId" and extract(YEAR from r."effectiveDate") = (extract(YEAR from l."effectiveDate") - 1)
    where extract(YEAR from r."effectiveDate") is NULL
    order by l."firmId";
grant SELECT on "FirmPolicyGapView" to WILMICRD;
select * from "FirmPolicyGapView";

-- LawyerPolicyGapView
create or replace view "LawyerPolicyGapView" as 
    select l."lawyerId", l."effectiveDate", extract(YEAR from l."effectiveDate") as "startYear",
    (
        select min(extract(YEAR from a."effectiveDate")) as "startYear"
        from "PolicyLawyerCoverage" a
            left outer join "PolicyLawyerCoverage" b on a."lawyerId" = b."lawyerId" and extract(YEAR from a."effectiveDate") = (extract(YEAR from b."effectiveDate") - 1)
        where extract(YEAR from b."effectiveDate") is NULL
          and extract(YEAR from a."effectiveDate") >= extract(YEAR from l."effectiveDate")
          and a."lawyerId" = l."lawyerId"
    ) as "endYear"
    from "PolicyLawyerCoverage" l
        left outer join "PolicyLawyerCoverage" r on r."lawyerId" = l."lawyerId" and extract(YEAR from r."effectiveDate") = (extract(YEAR from l."effectiveDate") - 1)
    where extract(YEAR from r."effectiveDate") is NULL
    order by l."lawyerId";


/******************
**  CLIENT
******************/
-- ISI_ClientView
-- e_client
--
-- 
CREATE OR REPLACE FORCE VIEW "LAWUA"."ISI_ClientView" as
    select
        NULL as "mappingid",  --deprecated per documentation
        NULL as "mappingvnumber",  --deprecated per documentation
		F."firmId" as "entitykey",
		0 as "entitykeyvnumber",
		NULL as "previousentitykey",
        CASE
            WHEN F."insureStatusId" in ('CI', 'TI', 'CT') THEN 'A'
            WHEN F."insureStatusId" = 'PI' THEN 'R'
            ELSE 'P'
		END as "clientstatus",
		TO_CHAR(EV."eventDate", 'YYYYMMDD') as "inceptiondate",
        CASE
            WHEN FPGV."maxStartYear" IS NOT NULL THEN FPGV."maxStartYear"||'01'||'01' 
            ELSE TO_CHAR(EV."eventDate", 'YYYYMMDD')
        END as "continuoussince",
		NULL as "clientcustomformattedname",
        NULL as "ar_glaccountmappingid",  --deprecated per documentation
		E."entityId" as "altkey",  --entityId  NOTE: need to connect with State Bar file firmId (if possible)
        NULL as "preferredhtc",  --deprecated per documentation
		0 as "vipind",
        NULL as "employeeind",  --deprecated per documentation
        NULL as "donotacceptchequesind",  --deprecated per documentation
		4112 as "sbuid",
		REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(RTRIM(SUBSTR(EV."eventNote", 1, 1000)), '"', ''),CHR(10), ' '), CHR(13), ''),'$', ''), CHR(171), ''), CHR(189), ''),CHR(194), ''),CHR(195), ''),CHR(249), ''),CHR(185), ''),CHR(188), '') as "comments",
		E."entityId" as "contactentitykey1",
		0 as "contactentitykeyvnumber1",
		NULL as "contactentitykey2",
		NULL as "contactentitykeyvnumber2",
		NULL as "contactentitykey3",
		NULL as "contactentitykeyvnumber3",
		NULL as "contactentitykey4",
		NULL as "contactentitykeyvnumber4",
		NULL as "contactentitykey5",
		NULL as "contactentitykeyvnumber5",
        CASE
            WHEN E."mainLocationId" is not NULL THEN E."entityId"||'_'||E."mainLocationId" 
            ELSE NULL
        END as "defaultaddresskey",
        CASE
            WHEN E."mainLocationId" is not NULL AND L."telephone" is not NULL THEN E."mainLocationId"||L."telephone"||.1
            ELSE NULL
        END as "defaultcontactmethodkey",
		'M' as "deliverymethod",
        CASE
            WHEN E."mainLocationId" is not NULL THEN E."entityId"||'_'||E."mainLocationId" 
            ELSE NULL
        END as "deliveryaddresskey",
		NULL as "deliverymethodkey",
		'1' as "clienttype",
		'CHK' as "settlementmethod",
		NULL as "conversionserver",
		NULL as "sequencenumber",
		'clientdetails_1' as "screenkey",
        C."contactEntityId" as "defaultcontactentitykey",
        0 as "defaultcontactentityvnumber"
    from "Firm" F    
        left join "Entity" E on F."firmId" = E."entityId"
            left join "Event" EV on EV."eventId" = E."eventId"
            left join "Location" L on L."locationId" = E."mainLocationId"
            left join (select * from "Contact" where "contactTypeId" = 'Firm') C on C."entityId" = E."entityId"
        left join (select "firmId", max("startYear") as "maxStartYear" from "FirmPolicyGapView" group by "firmId") FPGV on F."firmId" = FPGV."firmId"
;

/*
-- TESTS
*/

select * from "ISI_ClientView";
select * from "ISI_ClientView" where "entitykey" = 'PRICBRAR';
select * from "Entity" where "entityId" = 'PRICBRAR';
select * from "EntityLocation" where "entityId" = 'PRICBRAR';
select * from "Lawyer" where "lawyerId" = 'PRICBRAR';
select * from "Location" where "locationId" = 'LN12329390';
select * from "ISI_PhoneView" where "contactmethodkey" = 'LN12329390.1';
select * from "ISI_AddressView" where "addresskey" like 'UNK%';

select * from "ISI_ClientView" where "entitykey" = 'GONZJON_';
select * from "Lawyer" where "lawyerId" = 'GONZJON_';
select * from "Person" where "personId" in ('EY000202676', 'GONZJON_');
select * from "EntityLocation" where "entityId" = 'GONZJON_';
select * from "Entity" where "entityId" in ('GONZJON_', 'EY000202676');
select * from "ISI_ContactView" where "entitykey" = 'EY000202676';

select 
    E."mainLocationId",
    L.*
from "Lawyer" L
    left join "Entity" E on E."entityId" = L."lawyerId"
where E."mainLocationId" is NULL;

select REPLACE(REPLACE(RTRIM(SUBSTR(EV."eventNote", 1, 2000)), '"', ''), CHR(10), ' ') from "Event" EV where EV."eventNote" is not NULL;
--------------------------------------------------------------------------------
-- ISI_ClientStaffView
--
create or replace view "ISI_ClientStaffView" as
    select
        F."firmId" as "cliententitykey",
        0 as "cliententitykeyvnumber",
        TO_CHAR(L2."rank", 'FM000') as "clientstaffkey",
        0 as "clientstaffkeyvnumber",
        4112 as "sbuid",
        NULL as "comments",
        L."lawyerId" as "contactentitykey",
        0 as "contactentitykeyvnumber",
        CASE
            WHEN L."latestPolicyStatus" in ('CI', 'TI', 'CT') THEN 'A'
            ELSE 'I'
        END as "status"
    from "Lawyer" L
        left join "Firm" F on F."firmId" = L."latestFirmId"
        left join (select "lawyerId", "latestFirmId", row_number() over (partition by "latestFirmId" order by "lawyerId") as "rank" from "Lawyer") L2 on L2."lawyerId" = L."lawyerId"
    where L."latestFirmId" is not NULL
    
;
-- TESTS
select * from "ISI_ClientStaffView" CSV where not exists (select CV."entitykey" from "ISI_ClientView" CV where CV."entitykey" = CSV."cliententitykey");
select * from "ISI_ClientView" where "entitykey" = 'AAABA';

select * from "ISI_ClientStaffView" where "cliententitykey" = 'BRUCLA';


--------------------------------------------------------------------------------
/*
    ISI_ClientDetails1View
    cl_clientdetails_1

    History
    05/14/2018  FZ  05/14/2018
    
*/
create or replace view "ISI_ClientDetails1View" as 
    select
        F."firmId" as "entitykey",
        0 as "entitykeyvnumber",
        '4112' as "sbuid",
        COALESCE(AQ."answerNote", NULL) as "contactwhenabsent",
        A."staffCount" as "nonattorneystaff",
        CASE 
          WHEN O."organizationLegalForm" = 'P' THEN 'PR'
          WHEN O."organizationLegalForm" = 'LLP' THEN 'LLP' 
          WHEN O."organizationLegalForm" = 'PSC' THEN 'PC'
          WHEN O."organizationLegalForm" = 'I' THEN 'SP'
          WHEN O."organizationLegalForm" = 'LLC' THEN 'LLC'
          WHEN O."organizationLegalForm" = 'GP' THEN 'GP'
          WHEN O."organizationLegalForm" is NULL and substr(E."entityName", -3, 3) = 'LLP' THEN 'LLP'
          WHEN O."organizationLegalForm" is NULL and substr(E."entityName", -3, 3) = 'LLC' THEN 'LLC'
          ELSE 'CP'
        END as "organizationstructure",
        EXTRACT(YEAR FROM O."establishDate") as "yearsestablished"
    from "Firm" F    
        left join "Entity" E on F."firmId" = E."entityId"
        left join "Application" A on A."applicationId" = F."latestApplicationId"
            left join (select * from "ApplicationQuestion" where "questionId" = 'SBL') AQ on AQ."applicationId" = A."applicationId"
        left join "Organization" O on O."organizationId" = F."firmId"
;

--------------------------------------------------------------------------------
/*
    ISI_1View
    
    History
    05/14/2018  FZ  05/14/2018
    
*/