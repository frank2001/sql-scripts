/******************
**  POLICY
******************/
/*
    ISI_PolicyView
    p_policy

    History:
    03/07/2018  FZ  fix endorsements on policies with cancelEffectiveDate
    03/23/2018  FZ  added Firm tail information
    04/04/2018  FZ  added CAST for NULL values
    
*/
create or replace view "ISI_PolicyView" as
    select
        /* -- DEBUG AREA
        P."policyNumber" as "POLICYNUMBER",
        P."cancelEffectiveDate" as "CANCEL_DATE",
        */
        P."policyId" as "POLICYID",
        ICRV."conversionreference",
        -- 01/05/2018 transaction redone to allow for cancellations
        CASE
            WHEN TLV."policyId" is not NULL and TLV."tailType" = 'Firm' THEN 'ERP'
            WHEN E."endorsementNumber" = 1 THEN
                CASE
                    WHEN P."cancelEffectiveDate" IS NOT NULL and P."cancelEffectiveDate" = P."effectiveDate" THEN  --flat cancelled policy
                        CASE
                            WHEN P."cancelEffectiveDate" = lead(P."effectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") and P."firmId" = lead(P."firmId") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") THEN 'RCA'  --cancel for purposes of rewrite
                            ELSE 'CAN'  --just cancelled flat
                        END
                     WHEN P."newReissue" = 'R' and P."effectiveDate" = lag(P."cancelEffectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber",E."endorsementNumber") THEN 'RWR' -- cancel/rewrite  
                     WHEN P."newReissue" = 'N' and P."effectiveDate" = lag(P."cancelEffectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber",E."endorsementNumber") THEN 'RWN' -- cancel/rewrite
                     WHEN P."newReissue" = 'N' THEN 'NEW'
                     WHEN P."newReissue" = 'R' and (FX."ISIFirmEarliestDate" = FPV."effectiveDate") THEN 'NEW'  --ADDED: 01/30/2018 to fix #4
                     ELSE 'REN'
                END
            WHEN E."endorsementNumber" > 1 THEN
                CASE 
                    WHEN P."cancelEffectiveDate" IS NOT NULL THEN
                        CASE
                            WHEN P."cancelEffectiveDate" = lead(P."effectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") and P."firmId" = lead(P."firmId") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") THEN 'RCA'  --cancel for purposes of rewrite
                            WHEN P."cancelEffectiveDate" > E."effectiveDate" THEN 'END' --END with cancelEffectiveDate 03/07/2018 to fix #49
                            ELSE 'CAN'  --just cancelled flat
                        END
                    ELSE 'END'
                END
        END as "transactiontype",
        TO_CHAR(E."effectiveDate", 'YYYYMMDD') as "transactioneffectivedate",
        E."endorsementNumber" as "transactionseqno",
        -- REVISE once s_predompolregion_mapping is updated with historical rate levels
        CASE
            WHEN P."effectiveDate" < '01-JAN-2010' THEN 'LPL_19000101_20991231_4112'  --'LPL_19000101_20100101_4112'
            WHEN P."effectiveDate" < '01-JAN-2016' THEN 'LPL_19000101_20991231_4112'  --'LPL_20100101_20160101_4112'
            WHEN P."effectiveDate" < '01-OCT-2017' THEN 'LPL_19000101_20991231_4112'  --'LPL_20160101_20171001_4112'
            WHEN P."effectiveDate" < '01-JAN-2018' THEN 'LPL_19000101_20991231_4112'  --'LPL_20171001_20180101_4112'
            ELSE 'LPL_19000101_20991231_4112'  --'LPL_20180101_99991231_4112'
        END as "pcmmappingkey", -- LPL_<rate effective date>_<rate expiration date>_4112
        4112 as "sbuid",
        TO_CHAR(FX."ISIFirmEarliestDate", 'YYYYMMDD') as "inceptiondate",
        TO_CHAR(FPV."effectiveDate", 'YYYYMMDD') as "policyeffectivedate",
        TO_CHAR(FPV."expirationDate", 'YYYYMMDD') as "policyexpirydate",
        
        CASE
            WHEN E."effectiveDate" < P."effectiveDate" or E."effectiveDate" > P."expirationDate" THEN TO_CHAR(P."effectiveDate", 'YYYYMMDD') -- fixes endorsements effective > policy expiration date (ie: 0703097)
            ELSE TO_CHAR(E."effectiveDate", 'YYYYMMDD') 
        END as "veffectivedate",
        CASE
            WHEN E."effectiveDate" < P."effectiveDate" or E."effectiveDate" > P."expirationDate" THEN TO_CHAR(P."effectiveDate", 'YYYYMMDD')  -- fixes endorsements effective > policy expiration date (ie: 0703097)
            --WHEN E."endorsementNumber" = 1 and P."cancelEffectiveDate" IS NOT NULL and P."cancelEffectiveDate" = P."effectiveDate" and P."cancelEffectiveDate" = lead(P."effectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") THEN TO_CHAR(E."effectiveDate", 'YYYYMMDD') --RCA
            --WHEN E."endorsementNumber" > 1 and E."effectiveDate" <> P."effectiveDate" and SUBSTR(E2."endorsementDesc",1,8) = 'WLMEND14' THEN TO_CHAR(E."effectiveDate", 'YYYYMMDD')  --CAN
            --WHEN E."endorsementNumber" > 1 and SUBSTR(E2."endorsementDesc",1,8) = 'WLMEND14' and P."cancelEffectiveDate" <> lead(P."effectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") THEN TO_CHAR(E."effectiveDate", 'YYYYMMDD')  --CAN
           -- WHEN E."endorsementNumber" > 1 and P."cancelEffectiveDate" is not NULL THEN  TO_CHAR(E."effectiveDate", 'YYYYMMDD')  --CAN/RCA
            WHEN P."cancelEffectiveDate" is not NULL THEN TO_CHAR(E."effectiveDate", 'YYYYMMDD')
            ELSE TO_CHAR(LEAD(E."effectiveDate", 1, P."expirationDate") over (partition by E."policyId" order by E."policyId", E."effectiveDate"), 'YYYYMMDD') 
        END as "vexpirydate", 
        
        '000100' as "veffectivetime",
        '000100' as "vexpirytime",
        FPV."policyNumber" as "policykey",
        CAST(NULL as VARCHAR2(40)) as "previouspolicykey",
        E."endorsementNumber" as "alternatepolicykey",
        CAST(NULL as VARCHAR2(40)) as "agentbrokerpolicykey",
        CAST(NULL as VARCHAR2(40)) as "binderkey",
        TO_CHAR(P."expirationDate", 'YYYYMMDD') as "policynextunderwritedate",
        P."firmId" as "cliententitymappingkey",  
        'M' as "renewaltype",
        A."policyTerm" as "termlengthind",
        'M' as "termlengthindunit",
        'D' as "billingtype",
        0 as "papind",
        CASE
            WHEN P."paymentMethod" = 'QP' THEN '4 Pay 25 Down'
            WHEN P."paymentMethod" = 'MP' THEN '10 Pay'
            ELSE 'Annual'
        END as "paymentplan",
        19000101 as "planeffdate",
        20991231 as "planexpdate",
        'N' as "subscriptionind",
        CAST(NULL as NUMBER) as "facultativeind",
        'USD' as "premiumcurrencycd",
        'USD' as "limitcurrencycd",
        CAST(NULL as VARCHAR2(3)) as "sbuofficekey",
        'jmccarthy' as "underwriterentitymappingkey",
        0 as "priorperiodind",
        CAST(NULL as NUMBER) as "cancellationtype",
        CAST(NULL as NUMBER) as "canreturnpremium",
        CAST(NULL as NUMBER) as "canearnedpremium",
        CAST(NULL as NUMBER) as "reinstateexpirydate",
        CAST(NULL as VARCHAR2(40)) as "conversionserver",
        CAST(NULL as NUMBER) as "sequencenumber",
        -- only required if transactiontype=RCA
        CASE
            WHEN E."endorsementNumber" = 1 and P."cancelEffectiveDate" is not NULL and P."cancelEffectiveDate" = P."effectiveDate" and P."cancelEffectiveDate" = lead(P."effectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") and lead("newReissue") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") = 'N' THEN 'RWN' 
            WHEN E."endorsementNumber" = 1 and P."cancelEffectiveDate" is not NULL and P."cancelEffectiveDate" = P."effectiveDate" and P."cancelEffectiveDate" = lead(P."effectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") THEN 'RWR'
            WHEN E."endorsementNumber" > 1 and P."newReissue" = 'N' and P."cancelEffectiveDate" is not NULL and P."cancelEffectiveDate" = lead(P."effectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") and P."firmId" = lead(P."firmId") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") THEN 'RWR'   
            WHEN E."endorsementNumber" > 1 and P."newReissue" = 'R' and P."cancelEffectiveDate" is not NULL and P."cancelEffectiveDate" = lead(P."effectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") and P."firmId" = lead(P."firmId") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") THEN 'RWR'
            ELSE CAST(NULL as VARCHAR2(3))
        END as "relatedrevrcatranstype",
        -- only required if transactiontype=RCA
        CASE
            WHEN E."endorsementNumber" = 1 and P."cancelEffectiveDate" is not NULL and P."cancelEffectiveDate" = P."effectiveDate" and P."cancelEffectiveDate" = lead(P."effectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") and P."firmId" = lead(P."firmId") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") THEN TO_CHAR(LEAD(E."effectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber"), 'YYYYMMDD') 
            WHEN E."endorsementNumber" > 1 and P."cancelEffectiveDate" is not NULL and P."cancelEffectiveDate" = lead(P."effectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") and P."firmId" = lead(P."firmId") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") THEN TO_CHAR(LEAD(E."effectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber"), 'YYYYMMDD') --midterm cancel/rewrite
            ELSE CAST(NULL as VARCHAR2(8))
        END as "relatedrevrcatranseffdate",
        -- only required if transactiontype=RCA
        CASE
           WHEN E."endorsementNumber" = 1 and P."cancelEffectiveDate" is not NULL and P."cancelEffectiveDate" = P."effectiveDate" and P."cancelEffectiveDate" = lead(P."effectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") and P."firmId" = lead(P."firmId") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") THEN 1 
           WHEN E."endorsementNumber" > 1 and P."cancelEffectiveDate" is not NULL and P."cancelEffectiveDate" = lead(P."effectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") and P."firmId" = lead(P."firmId") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") THEN 1
           ELSE CAST(NULL as NUMBER) 
        END as "relatedrevrcatranseqno", 
        1 as "claimsmadeind",
        TO_CHAR(X."ISIPolicyRetroDate", 'YYYYMMDD') as "retrodate",
        TO_CHAR(TLV."effectiveDate", 'YYYYMMDD') as "extendreporteffdate",  --firm tail effective date
        TO_CHAR(TLV."expirationDate", 'YYYYMMDD') as "extendreportexpdate",  --firm tail expiration date, unlimited 29991231      
        CASE
            WHEN TLV."policyId" is not NULL and TLV."tailType" = 'Firm' THEN 702  --ERP non-practicing
            WHEN E."endorsementNumber" = 1 THEN
                CASE
                    WHEN P."cancelEffectiveDate" IS NOT NULL and P."cancelEffectiveDate" = P."effectiveDate" THEN  --flat cancelled policy
                        CASE
                            WHEN P."cancelEffectiveDate" = lead(P."effectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") and P."firmId" = lead(P."firmId") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") THEN 999  --RCA cancel for purposes of rewrite
                            ELSE 699  --just cancelled flat
                        END
                     WHEN P."newReissue" = 'R' and P."effectiveDate" = lag(P."cancelEffectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber",E."endorsementNumber") THEN 1099 --RWR cancel/rewrite  
                     WHEN P."newReissue" = 'N' and P."effectiveDate" = lag(P."cancelEffectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber",E."endorsementNumber") THEN 1099 --RWN cancel/rewrite
                     WHEN P."newReissue" = 'N' THEN 200  --new business
                     WHEN P."newReissue" = 'R' and (FX."ISIFirmEarliestDate" = FPV."effectiveDate") THEN 200  --ADDED: 01/30/2018 to fix #4
                     ELSE 500 --renewal
                END
            WHEN E."endorsementNumber" > 1 THEN
                CASE 
                    WHEN P."cancelEffectiveDate" IS NOT NULL THEN
                        CASE
                            WHEN P."cancelEffectiveDate" = lead(P."effectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") and P."firmId" = lead(P."firmId") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") THEN 999  --RCA cancel for purposes of rewrite
                            WHEN P."cancelEffectiveDate" > E."effectiveDate" THEN 300 --END with cancelEffectiveDate 03/07/2018
                            ELSE 699  --CAN just cancelled flat
                        END
                    ELSE 300 --END endorsement
                END
        END as "vreasoncode",
        TO_CHAR(EV."eventDate", 'YYYYMMDDHH24MISS') as "ventrydatetime",
        EV."createUserId" as "vusermappingkey",
        TO_CHAR(EV."eventDate", 'YYYYMM') as "postedyearmth",  --ignoring since conversion is not posting transaction
        'D' as "policysourceind",
        CAST(NULL as VARCHAR2(40)) as "assumedcompanymappingkey",
        CAST(NULL as NUMBER) as "writingcompanymappingkey",
        CAST(NULL as VARCHAR2(40)) as "freetradezoneriskcode",
        'EN' as "languagecd",
        CAST(NULL as VARCHAR2(40)) as "overridereasoncd",
        CAST(NULL as VARCHAR2(40)) as "overridereasonnote",
        CAST(NULL as NUMBER) as "postedwweobjectid",
        CAST(NULL as NUMBER) as "postedwwevnumber",
        CAST(NULL as VARCHAR2(40)) as "transactionstatus",
        CAST(NULL as NUMBER) as "nextfollowupdate",
        0 as "finaladjustmentind",
        'LPL_Ratebook' as "rateeditionname",
        1 as "rateeditionversion",
        CAST(NULL as NUMBER) as "wipstatus", -- probably use event folder
        CAST(NULL as VARCHAR2(40)) as "quoterelatedtranstype",
        CAST(NULL as VARCHAR2(3)) as "relatedtranstype",
        CAST(NULL as VARCHAR2(8)) as "relatedtranseffdate",
        CAST(NULL as NUMBER) as "relatedtransseqno",
        CAST(NULL as VARCHAR2(40)) as "quotekey",
        CAST(NULL as VARCHAR2(8)) as "quotedate",
        CAST(NULL as NUMBER) as "quotedaystoaccept",
        CAST(NULL as VARCHAR2(8)) as "quoteexpirydate",
        CAST(NULL as VARCHAR2(40)) as "quotedefinedname",
        CAST(NULL as VARCHAR2(40)) as "quoteseriescode",
        CAST(NULL as VARCHAR2(1)) as "quotestatus",
        1 as "quotedocumentsprinted",
        CAST(NULL as VARCHAR2(40)) as "brkwritingcompanymappingkey",
        CAST(NULL as NUMBER) as "brokeragepolicycommission",
        CAST(NULL as VARCHAR2(1)) as "quickquoteind",
        CAST(NULL as VARCHAR2(1)) as "referralstatus",
        CAST(NULL as VARCHAR2(40)) as "documentcategory",
        CAST(NULL as VARCHAR2(40)) as "frontedcompanymappingkey",
        0 as "assumedcommissionamount",
        1 as "assumedcommissiontype",
        CAST(NULL as VARCHAR2(40)) as "policycompositiontype",
        'WI' as "predominantjurisdiction",
        'WI' as "predominantpolicyregion",
        0 as "totalwaivedpremium",
        TO_CHAR(EV."eventDate", 'YYYYMMDD') as "accountingdate", --Event.eventDate
        CAST(NULL as VARCHAR2(3)) as "quotedeclinedby",
        CAST(NULL as VARCHAR2(40)) as "quotecompetitor",
        0 as "quotecompetitorpremium",
        CASE
            WHEN CT."personId" IS NOT NULL THEN CT."personId"
            ELSE CT."contactEntityId"
        END as "policycontactentitykey",
        0 as "policycontactentitykeyvnumber",
        CAST(NULL as VARCHAR2(40)) as "comments",  --helpful but not required
        CASE
            WHEN P."newReissue" = 'N' THEN 'N'
            WHEN P."newReissue" = 'R' THEN 'R'
        END as "renewalstatus",
        CASE
            WHEN P."cancelEffectiveDate" is NULL THEN 0
            ELSE 7
       END as "insurancestatus",
       P."perClaimLimit" as "liabilitylimit",
       CAST(NULL as VARCHAR2(40)) as "overridemailingaddrkey",
       0 as "auditreportingind",
       CAST(NULL as VARCHAR2(8)) as "convertedropolicytrneffdate",
       CAST(NULL as NUMBER) as "convertedtopolicytrnseqno",
       CAST(NULL as VARCHAR2(20)) as "quotecompletedreasoncode",
       CAST(NULL as VARCHAR2(40)) as "quotecompletedreasonnotes",
       CAST(NULL as VARCHAR2(40)) as "quotelastmodifieddatetime",
       CASE
            WHEN TLV."tailTerm" is not NULL and TLV."tailTerm" <> 'Unlimited' THEN TLV."tailTerm"
            ELSE CAST(NULL as VARCHAR2(20))
        END as "extendreporttermlength",  --v132  if U then NULL
        CASE
            WHEN TLV."tailTerm" is not NULL and TLV."tailTerm" = 'Unlimited' THEN 'U'
            WHEN TLV."tailTerm" is not NULL and TLV."tailTerm" <> 'Unlimited' THEN 'Y'
            ELSE CAST(NULL as VARCHAR2(1))
        END as "extendreportlengthunit",  --v132  Y=years, M=months, U=unlimited
        CAST(NULL as VARCHAR2(1)) as "useextendreportexpoverrideind"  --v132  default= NULL
from (select DISTINCT "policyId", "effectiveDate", min("endorsementNumber") as "endorsementNumber" from "Endorsement" group by "policyId", "effectiveDate" order by "effectiveDate") E  
    left join "ISIFirmPolicyView" FPV on FPV."policyId" = E."policyId"
        left join "Policy" P on P."policyId" = FPV."policyId"
            left join "Application" A on A."applicationId" = P."policyId"
            left join (select * from "Contact" where "contactTypeId" = 'Firm') CT on CT."organizationId" = P."firmId"  -- may produce duplicates when there are multiple contactTypeId=Firm
            left join "ISIConversionReferenceView" ICRV on ICRV."firmId" = P."firmId" and (P."effectiveDate" BETWEEN ICRV."startDate" and ICRV."endDate" and COALESCE(P."cancelEffectiveDate", P."expirationDate") BETWEEN ICRV."startDate" and ICRV."endDate")
        left join "Quote" Q2 on Q2."applicationId" = P."policyId"
        left join "ISIPolicyRetroDateView" X on X."policyId" = P."policyId"
        left join "ISIFirmDatesView" FX on FX."firmId" = P."firmId"
    left join "Endorsement" E2 on E2."policyId" = E."policyId" and E2."endorsementNumber" = E."endorsementNumber"
        left join "Coverage" C on C."coverageId" = E2."endorsementId"
            left join "Event" EV on EV."eventId" = C."eventId"  
    left join (select unique "policyId", "effectiveDate", "expirationDate", "tailType", "tailTerm" from "TailLawyerAllView" where "tailType" = 'Firm') TLV on TLV."policyId" = E."policyId" and TLV."effectiveDate" = E."effectiveDate"
where P."effectiveDate" > '31-DEC-2005'
-- BELOW ITEMS NEED FIXING
  and P."firmId" not in ('SKOGLA', 'CASEL1')
  and P."policyNumber" NOT in ('0607050', '0703097', '0807068', '0811071', '1003092', '1006065', '1007005', '1111122', '1112041', '1206065', '1308050', '1408002', '1505056', '1602014',  '1601090')
order by FPV."firmId", FPV."policyNumber", TO_CHAR(FPV."effectiveDate", 'YYYYMMDD'), E."endorsementNumber"
;


/*--- Create Policy Table ---*/
drop table "ISI_Policy";
create table "ISI_Policy" as
    (select * from "ISI_PolicyView");
select * from "ISI_Policy";   
select count(*) from "ISI_Policy";  

/*---- Insert Policy Table Records ---*/
	-- Fix flat cancel/rewrite transaction, namely assure intial transaction exists
-- Fix flat cancel/rewrite transaction, namely assure intial transaction exists
INSERT INTO "ISI_Policy"
	(
	SELECT
        P."POLICYID",
		P."conversionreference",
		'NEW' as "transactiontype",
		P."transactioneffectivedate",
		P."transactionseqno",
		P."pcmmappingkey",
		P."sbuid",
		P."inceptiondate",
		P."policyeffectivedate",
		P."policyexpirydate",
		P."veffectivedate",
		P."vexpirationdate",
		P."veffectivetime",
		P."vexpirytime",
		P."policykey",
		P."previouspolicykey",
		P."alternatepolicykey",
		P."agentbrokerpolicykey",
		P."binderkey",
		P."policynextunderwritedate",
		P."cliententitymappingkey",
		P."renewaltype",
		P."termlengthind",
		P."termlengthindunit",
		P."billingtype",
		P."papind",
		P."paymentplan",
		P."planeffdate",
		P."planexpdate",
		P."subscriptionind",
		P."facultativeind",
		P."premiumcurrencycd",
		P."limitcurrencycd",
		P."sbuofficekey",
		P."underwriterentitymappingkey",
		P."priorperiodind",
		P."cancellationtype",
		P."canreturnpremium",
		P."canearnedpremium",
		P."reinstateexpirydate",
		P."conversionserver",
		P."sequencenumber",
		NULL as "relatedrevrcatranstype",
		NULL as "relatedrevrcatranseffdate",
		NULL as "relatedrevrcatransseqno",
		P."claimsmadeind",
		P."retrodate",
		P."extendreporteffdate",
		P."extendreportexpdate",
		200 as "vreasoncode",  ---or 500
		P."ventrydatetime",
		P."vusermappingkey",
		P."postedyearmth",
		P."policysourceind",
		P."assumedcompanymappingkey",
		P."writingcompanymappingkey",
		P."freetradezoneriskcode",
		P."languagecd",
		P."overridereasoncd",
		P."overridereasonnote",
		P."postedwweobjectid",
		P."postedwwevnumber",
		P."transactionstatus",
		P."nextfollowupdate",
		P."finaladjustmentind",
		P."rateeditionname",
		P."rateeditionversion",
		P."wipstatus",
		P."quoterelatedtranstype",
		P."relatedtranstype",
		P."relatedtranseffdate",
		P."relatedtransseqno",
		P."quotekey",
		P."quotedate",
		P."quotedaystoaccept",
		P."quoteexpirydate",
		P."quotedefinedname",
		P."quoteseriescode",
		P."quotestatus",
		P."quotedocumentsprinted",
		P."brkwritingcompanymappingkey",
		P."brokeragepolicycommission",
		P."quickquoteind",
		P."referralstatus",
		P."documentcategory",
		P."frontcompanymappingkey",
		P."assumedcommissionamount",
		P."assumedcommissiontype",
		P."policycompositetype",
		P."predominantjusrisdiction",
		P."predominantpolicyreion",
		P."totalwaivedpremium",
		P."accountingdate",
        P."quotedeclinedby",
		P."quotecompetitor",
		P."quotecompetitorpremium",
		P."policycontactentitykey",
		P."policycontactentitykeyvnumber",
		'Inserted from Python' as "comments",
		P."renewalstatus",
		P."insurancestatus",
		P."liabilitylimit",
		P."overridemailingaddrkey",
		P."auditreportingind",
        P."convertedropolicytrneffdate",
		P."convertedtopolicytrneqno",
		P."quotecompletedreasoncode",
		P."quotecompletedreasonnotes",
		P."quotelastmodifieddatetime",
		P."extendreporttermlength",
		P."extendreportlengthunit",
		P."useextendreportexpoverrideind"
	FROM "ISI_Policy" P
	WHERE P."transactiontype" = 'RCA'
      and P."conversionreference" = 'KOENME_1988_2018'
	)
;