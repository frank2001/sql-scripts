/*
	ISI Conversion Tests
*/

/*
-- e_contact
*/
select * from "ISI_ContactView";
select "entitykey", count(*) from "ISI_ContactView" group by "entitykey" having count(*) > 1;


/*
-- e_address
*/
select "entitykey", "entitykeyvnumber", "entitytype", "mailingaddressind",  count(*) from "ISI_AddressView" group by "entitykey", "entitykeyvnumber", "entitytype", "mailingaddressind" having count(*) > 1;
select CV.* from "ISI_ContactView" CV where not exists (select AV.* from "ISI_AddressView" AV where CV."entitykey" = AV."entitykey");  --contact's without addresses

/*
-- e_emailandweb
*/
select * from "ISI_EmailWebsiteView" where "emailandweb" is NULL;


/*
-- e_vendor
*/
select count(*) from "ISI_VendorView";
select * from "ISI_VendorView";
select "entitykey", count(*) from "ISI_VendorView" group by "entitykey" having count(*) > 1;
select VV.* from "ISI_VendorView" VV where not exists (select AV.* from "ISI_AddressView" AV where AV."addresskey" = VV."deliveryaddresskey");
