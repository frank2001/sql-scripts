/*
--  INCIDENTS
    Same as CLAIM extract but without the coverage & transaction detail
    
    NOTE:
    IMPORTANT   Only incidents that have a matching policy version are currently converted
                (ie: incident's outside of policy or retro dates are NOT converted)
*/
--------------------------------------------------------------------------------

/*  UTILITIES  */

-- ISIIncidentToPolicyView
-- Allows matching of incident to policy transaction based on incident's report date 
-- and policy transactioneffectivedate.  The goal is to get one match per claim.
--  Dependancies:   ISIConversionReferenceView
--  Used By:        ISI_IncidentItemView
create or replace view "ISIIncidentToPolicyView" as 
with first_cut as (
select 
    I."instanceId" as "instanceId",
    PV."conversionreference" as "conversionreference",
    PV."POLICYID" as "policyId",
    PV."policykey" as "policyNumber",
    P."effectiveDate" as "effectiveDate",
    TO_CHAR(I."reportDate", 'YYYYMMDD') as "reportdate",
    PV."transactioneffectivedate" as "transactioneffectivedate"
from "Instance" I
    inner join "Incident" INC on INC."incidentId" = I."instanceId"
    inner join (select "conversionreference", "transactioneffectivedate", "POLICYID", "policykey", "transactiontype", "transactionseqno" from "ISI_Policy") PV on PV."POLICYID" = I."policyId" and PV."transactioneffectivedate" <= TO_CHAR(I."reportDate", 'YYYYMMDD')   -- inner to account for excluded policies         
        left join "Policy" P on P."policyId" = PV."POLICYID"
order by I."instanceId"
)
, second_cut as (
    select 
        "instanceId",
        "conversionreference",
        "policyId",
        "policyNumber",
        "effectiveDate",
        MAX("transactioneffectivedate") as "transactioneffectivedate",
        "reportdate"
    from first_cut
    group by "instanceId", "conversionreference", "policyId", "policyNumber","effectiveDate", "reportdate"
),
    third_cut as (
    select 
            SC.*,
            P2."transactiontype",
            P2."transactionseqno",
            row_number() over (partition by SC."instanceId" order by P2."transactionseqno") as "tranSeq"
        from second_cut SC
            left join "ISI_Policy" P2 on P2."policykey" = SC."policyNumber" and P2."transactioneffectivedate" = SC."transactioneffectivedate" 
),
    forth_cut as ( 
    select 
        "instanceId",
        "conversionreference",
        "policyId",
        "policyNumber",
        "effectiveDate",
        "transactioneffectivedate",
         MAX("transactionseqno") as "transactionseqno",
        "reportdate"       
    from third_cut
    group by "instanceId", "conversionreference", "policyId", "policyNumber", "effectiveDate", "transactioneffectivedate", "reportdate"
)
    select 
        FC.*,
        P3."transactiontype"
    from forth_cut FC
        left join "ISI_Policy" P3 on P3."policykey" = FC."policyNumber" and P3."transactioneffectivedate" = FC."transactioneffectivedate" and P3."transactionseqno" = FC."transactionseqno"
    order by FC."instanceId"    
    
;
select count(*) from "ISIIncidentToPolicyView" ;
select * from "ISIIncidentToPolicyView" where "instanceId" = 'Zerbkl8';



--------------------------------------------------------------------------------
/*
    ISI_IncidentView
    c_claim
    
    Dependancies: 
    
    NOTES:
     
    History
    05/14/2018	FZ	Created via ISI_ClaimView
*/
create or replace view "ISI_IncidentView" as
    select
        INC."incidentId" as "claimmappingkey",
        0 as "claimvnumber",
        '4112' as "sbuid",
        INC."incidentId" as "claimkey",
        'LAW_PL_19000101_21121231_4112' as "claimtypekey",
        I."firmId" as "cliententitymappingkey",
        0 as "cliententitykeyvnumber",
        TO_CHAR(COALESCE(I."occurDate", I."reportDate"), 'YYYYMMDD') as "occurrencedate",
        '000000' as "occurrencetime",
        TO_CHAR(I."reportDate", 'YYYYMMDD') as "reporteddate",  --IMPORTANT 
        '000000' as "reportedtime",
        'I' as "claimstatus",
        TO_CHAR(E."eventDate", 'YYYYMMDDHHMMSS') as "ventrydatetime",
         CASE
            WHEN E."createUserId" = 'Joyce' THEN 'jneumaier'
            WHEN E."createUserId" = 'Matt' OR I."adjuster" = 'Matt' THEN 'mbeier'
            WHEN E."createUserId" = 'Katja' OR I."adjuster" = 'Katja' THEN 'kkunzke'
            WHEN E."createUserId" = 'Brian' OR I."adjuster" = 'Brian' THEN 'banderson'
            WHEN E."createUserId" = 'Sally' OR I."adjuster" = 'Sally' THEN 'sanderson'
            ELSE '>>unknown<<'||COALESCE(I."adjuster", 'NULL') --QFC, NDZ, WHD
        END as "vuserid",
        CTP."conversionreference" as "polconversionref",
        CTP."transactiontype" as "poltranstype",
        CTP."transactioneffectivedate" as "poltranseffdate",
        CTP."transactionseqno" as "poltransseqno",
        CAST(NULL as NUMBER) as "prevclaimno",
        'CST' as "losstimezone",
        'CST' as "reportedtimezone",
        /*
        CASE
            WHEN (I."reOpenDate" is not NULL and I."closeDate" >= I."reOpenDate") or (I."closeDate" is not NULL) THEN TO_CHAR(I."closeDate", 'YYYYMMDD')
            ELSE NULL
        END as "closedate",
        */
        CAST(NULL as VARCHAR2(10)) as "closedate",
        /*
        CASE
            WHEN I."reOpenDate" is not NULL and I."closeDate" < I."reOpenDate" THEN TO_CHAR(I."reOpenDate", 'YYYYMMDD')
            ELSE NULL
        END as "reopendate",
        */
        CAST(NULL as VARCHAR2(10)) as "reopendate",
        1 as "chargeableind",
        CAST(NULL as VARCHAR2(10)) as "eventid",
        CAST(NULL as VARCHAR2(10)) as "occurrenceid",
        0 as "policecalledind",
        CAST(NULL as VARCHAR2(10)) as "policefileno",
        CAST(NULL as VARCHAR2(10)) as "lossdescription",  --NEED loss description
        CAST(NULL as VARCHAR2(10)) as "lossnotes",
        CASE
            WHEN I."reOpenDate" is not NULL and I."closeDate" < I."reOpenDate" THEN 10
            ELSE CAST(NULL as NUMBER)
        END as "reopenreason",
        1 as "claimsmadeind",
        10 as "reportedmethod", --10-other, 2-fax, 3-phone, 4-email, 5-mail
        30 as "reportingtype", --10=claimant, 20-claimant attorney, 30-insured, 99-other
        CAST(NULL as VARCHAR2(10)) as "branch",
        CAST(NULL as VARCHAR2(10)) as "refid",  ---?????
        0 as "totallossind",
        CAST(NULL as VARCHAR2(10)) as "conversionserver",
        CAST(NULL as VARCHAR2(10)) as "sequencenumber",
        CAST(NULL as VARCHAR2(10)) as "reopenreasonother",
        --TO_CHAR(I."closeDate", 'HHMMSS') as "closetime",
        CAST(NULL as VARCHAR2(10)) as "closetime",
        CAST(NULL as VARCHAR2(10)) as "litigationind", 
        0 as "subrogationchk",
        CAST(NULL as VARCHAR2(10)) as "subrogationtext",
        CAST(NULL as VARCHAR2(10)) as "packageid",
        CAST(NULL as VARCHAR2(10)) as "spoilreasonother",
        CAST(NULL as VARCHAR2(10)) as "spoilreason",
        TO_NUMBER(ORA_HASH(INC."incidentId")) as "ouid",
        CAST(NULL as VARCHAR2(10)) as "reportedother",
        CAST(NULL as VARCHAR2(10)) as "reportingtypeother",
        CAST(NULL as VARCHAR2(10)) as "closereason",
        CASE
            WHEN I."closeDate" IS NOT NULL THEN '3' --Records Only
            ELSE '1' --Monitoring
        END as "claimstatustype"
    from "Incident" INC
        left join "Instance" I on I."instanceId" = INC."incidentId"
            left join "Policy" P on P."policyId" = I."policyId"
            left join "Event" E on E."eventId" = I."eventId"
        inner join "ISIIncidentToPolicyView" CTP on CTP."instanceId" = INC."incidentId"
 --- SCOPE 
    where P."effectiveDate" > '31-DEC-2005'  
      and I."reportDate" > '31-DEC-2006'
    -- REQUIRED FIXES
union all
    select
    -- no policy inforce when reported
        INC."incidentId" as "claimmappingkey",
        0 as "claimvnumber",
        '4112' as "sbuid",
        INC."incidentId" as "claimkey",
        'LAW_PL_19000101_21121231_4112' as "claimtypekey",
        I."firmId" as "cliententitymappingkey",
        0 as "cliententitykeyvnumber",
        TO_CHAR(COALESCE(I."occurDate", I."reportDate"), 'YYYYMMDD') as "occurrencedate",
        '000000' as "occurrencetime",
        TO_CHAR(I."reportDate", 'YYYYMMDD') as "reporteddate",  --IMPORTANT 
        '000000' as "reportedtime",
        'I' as "claimstatus",
        TO_CHAR(E."eventDate", 'YYYYMMDDHHMMSS') as "ventrydatetime",
         CASE
            WHEN E."createUserId" = 'Joyce' THEN 'jneumaier'
            WHEN E."createUserId" = 'Matt' OR I."adjuster" = 'Matt' THEN 'mbeier'
            WHEN E."createUserId" = 'Katja' OR I."adjuster" = 'Katja' THEN 'kkunzke'
            WHEN E."createUserId" = 'Brian' OR I."adjuster" = 'Brian' THEN 'banderson'
            WHEN E."createUserId" = 'Sally' OR I."adjuster" = 'Sally' THEN 'sanderson'
            ELSE '>>unknown<<'||COALESCE(I."adjuster", 'NULL') --QFC, NDZ, WHD
        END as "vuserid",
        CAST(NULL as VARCHAR2(10)) as "polconversionref",
        CAST(NULL as VARCHAR2(10)) as "poltranstype",
        CAST(NULL as VARCHAR2(10)) as "poltranseffdate",
        CAST(NULL as NUMBER) as "poltransseqno",
        CAST(NULL as NUMBER) as "prevclaimno",
        'CST' as "losstimezone",
        'CST' as "reportedtimezone",
        CAST(NULL as VARCHAR2(10)) as "closedate",
        CAST(NULL as VARCHAR2(10)) as "reopendate",
        1 as "chargeableind",
        CAST(NULL as VARCHAR2(10)) as "eventid",
        CAST(NULL as VARCHAR2(10)) as "occurrenceid",
        0 as "policecalledind",
        CAST(NULL as VARCHAR2(10)) as "policefileno",
        CAST(NULL as VARCHAR2(10)) as "lossdescription",  --NEED loss description
        CAST(NULL as VARCHAR2(10)) as "lossnotes",
        CAST(NULL as NUMBER) as "reopenreason",
        1 as "claimsmadeind",
        10 as "reportedmethod", --10-other, 2-fax, 3-phone, 4-email, 5-mail
        30 as "reportingtype", --10=claimant, 20-claimant attorney, 30-insured, 99-other
        CAST(NULL as VARCHAR2(10)) as "branch",
        CAST(NULL as VARCHAR2(10)) as "refid",  ---?????
        0 as "totallossind",
        CAST(NULL as VARCHAR2(10)) as "conversionserver",
        CAST(NULL as VARCHAR2(10)) as "sequencenumber",
        CAST(NULL as VARCHAR2(10)) as "reopenreasonother",
        CAST(NULL as VARCHAR2(10)) as "closetime",
        CAST(NULL as VARCHAR2(10)) as "litigationind", 
        0 as "subrogationchk",
        CAST(NULL as VARCHAR2(10)) as "subrogationtext",
        CAST(NULL as VARCHAR2(10)) as "packageid",
        CAST(NULL as VARCHAR2(10)) as "spoilreasonother",
        CAST(NULL as VARCHAR2(10)) as "spoilreason",
        TO_NUMBER(ORA_HASH(INC."incidentId")) as "ouid",
        CAST(NULL as VARCHAR2(10)) as "reportedother",
        CAST(NULL as VARCHAR2(10)) as "reportingtypeother",
        CAST(NULL as VARCHAR2(10)) as "closereason",
        CASE
            WHEN I."closeDate" IS NOT NULL THEN '3' --Records Only
            ELSE '1' --Monitoring
        END as "claimstatustype"
    from "Incident" INC
        left join "Instance" I on I."instanceId" = INC."incidentId"
            left join "Policy" P on P."policyId" = I."policyId"
            left join "Event" E on E."eventId" = I."eventId"
        left join "ISIIncidentToPolicyView" CTP on CTP."instanceId" = INC."incidentId"
 --- SCOPE 
    where CTP."instanceId" is NULL
      and P."effectiveDate" > '31-DEC-2005'  
      and I."reportDate" > '31-DEC-2006'
    -- REQUIRED FIXES
    
order by "claimmappingkey"
;


/*-------------------------------------*/
/*  Performance Improvement            */
/*-------------------------------------*/
drop table "ISI_Incident";
create table "ISI_Incident" as 
    (select * from "ISI_IncidentView");


/*---- Testing ----*/
select * from "ISIIncidentToPolicyView" where "instanceId" = 'Zerbkl8'; -- DEVOBA_2004_2019	AP001055049	1706064	14-JUN-17	20170614	2	20180606	END
select * from "ISI_IncidentView" where "claimmappingkey" = 'Zerbkl8';  --DEVOBA_2004_2019	END	20170614	2
select * from "ISI_Policy" where "conversionreference" = 'DEVOBA_2004_2019' order by "transactioneffectivedate", "transactionseqno";
--------------------------------------------------------------------------------
/*
    ISI_InvolvedPartyView
    c_involvedparty
    
    Dependancies: 
    
    NOTES:
        ASCII character exist: http://roubaixinteractive.com/PlayGround/Binary_Conversion/The_Characters.asp
        invno = 1 is Firm
        invno = 2 is Claimant
        invno = >2 is everything else
     
    History
    01/23/2018  FZ  Created
    02/12/2017  FZ  corrected NULL personId for contactentitykey
    02/27/2018  FZ  correct claimant's contactentitykey to be <claimId>||'_Claimant'
    03/02/2018  FZ  default all iscaller=NULL
*/


create or replace view "ISI_ContactClaimantINCView" as
    select
        INC."incidentId"||'_Claimant' as "entitykey",
        0 as "entitykeyvnumber",
        NULL as "sbuid",  --deprecated per documentation
        NULL as "firstname",
        NULL as "middlename",
        NULL as "lastname",
        NULL as "nameprefix",
        NULL as "namesuffix",
        NULL as "jobtitle",
        NULL as "fullname",
        I."claimant" as "companyname",
        1 as "preferredhtc",
        0 as "isindividual",
        'CP' as "contacttype",
        NULL as "birthdate",
        NULL as "gender",
        NULL as "maritalstatus",
        NULL as "comments",
        NULL as "conversionserver",
        NULL as "sequencenumber",
        NULL as "linkedentitykey",
        0 as "linkedentitykeyvnumber",
        NULL as "ssn",
        NULL as "hicn",
        NULL as "fein",
        NULL as "tin",
        'EN' as "preferredlanguage"
    from "Incident" INC
        left join "Instance" I on I."instanceId" = INC."incidentId"
            left join "Policy" P on P."policyId" = I."policyId"
-- SCOPE
    where P."effectiveDate" > '31-DEC-2005'
      and I."reportDate" > '31-DEC-2006'
     -- REQUIRED FIXES
    order by INC."incidentId"
;
select * from "ISI_ContactClaimantINCView";


create or replace view "ISI_InvolvedPartyINCView" as
    -- firm
    select
        INC."claimmappingkey" as "claimmappingkey",
        0 as "claimvnumber",
        1 as "invno",
        I."firmId" as "contactentitykey", --firmId 
        0 as "contactentitykeyvnumber",
        NULL as "description",
        '4112' as "sbuid",
        'Firm' as "invdescription",
        NULL as "relationnum",
        NULL as "iscaller",
        NULL as "htctime",
        NULL as "repcomments",
        0 as "isclaimant",
        NULL as "adjustermappingkey",
        NULL as "adjustermappingvnumber",
        NULL as "adjusterfileno",
        NULL as "conversionserver",
        NULL as "sequencenumber",
        NULL as "claimantcontacttype",
        1 as "validcontact",
        NULL as "reportingstatus"
    from "ISI_Incident" INC  
        left join "Instance" I on I."instanceId" = INC."claimmappingkey"
            left join "Policy" P on P."policyId" = I."policyId"
-- SCOPE
    where P."effectiveDate" > '31-DEC-2005'
      and I."reportDate" > '31-DEC-2006'
    -- REQUIRED FIXES   
union all
    -- claimant
    select
        INC."claimmappingkey" as "claimmappingkey",
        0 as "claimvnumber",
        2 as "invno",
        INC."claimmappingkey"||'_Claimant' as "contactentitykey",
        0 as "contactentitykeyvnumber",
        I."claimant" as "description",
        '4112' as "sbuid",
        'Claimant' as "invdescription",
        NULL as "relationnum",
        NULL as "iscaller",
        NULL as "htctime",
        NULL as "repcomments",
        1 as "isclaimant",
        NULL as "adjustermappingkey",
        NULL as "adjustermappingvnumber",
        NULL as "adjusterfileno",
        NULL as "conversionserver",
        NULL as "sequencenumber",
        'T1' as "claimantcontacttype",
        1 as "validcontact",
        NULL as "reportingstatus"
    from "ISI_Incident" INC
        left join "Instance" I on I."instanceId" = INC."claimmappingkey"
            left join "Policy" P on P."policyId" = I."policyId"
-- SCOPE
    where P."effectiveDate" > '31-DEC-2005'
      and I."reportDate" > '31-DEC-2006'
     -- REQUIRED FIXES
      
union all
    -- person 
    select
        INC."claimmappingkey" as "claimmappingkey",
        0 as "claimvnumber",
        CV."contactSeq" + 2 as "invno",
        CV."personId" as "contactentitykey",
        /*
        CASE
            WHEN CV."personId" IS NOT NULL THEN CV."personId" --personId vs organizationId
            WHEN L."mainEntityId" is not NULL THEN L."mainEntityId" --fixes #91
            ELSE 'UNKNOWN'
        END  as "contactentitykey",
        */
        0 as "contactentitykeyvnumber",
        NULL as "description",
        '4112' as "sbuid",
        CV."contactTypeId" as "invdescription",
        NULL as "relationnum",
        NULL as "iscaller",
        NULL as "htctime",
        NULL as "repcomments",
        0 as "isclaimant",
        NULL as "adjustermappingkey",
        NULL as "adjustermappingvnumber",
        NULL as "adjusterfileno",
        NULL as "conversionserver",
        NULL as "sequencenumber",
        CASE
            WHEN CV."contactTypeId" in ('Claimant', 'ClaimantProSe') THEN 'T1'
            ELSE NULL
        END as "claimantcontacttype",
        1 as "validcontact",
        NULL as "reportingstatus"
    from "ISI_Incident" INC
        inner join (select * from "ContactView" where "personId" is not NULL) CV on CV."instanceId" = INC."claimmappingkey"  --inner join accounts for no contact records (ie: 14-0171 Test claim)
            left join "Instance" I on I."instanceId" = CV."instanceId"
            left join "Location" L on L."locationId" = CV."locationId"
                left join "Policy" P on P."policyId" = I."policyId"
-- SCOPE
    where P."effectiveDate" > '31-DEC-2005'
      and I."reportDate" > '31-DEC-2006'
      -- REQUIRED FIXES
      
union all
    -- organization
    select
        INC."claimmappingkey" as "claimmappingkey",
        0 as "claimvnumber",
        CV."contactSeq" + 42 as "invno",
        CV."organizationId" as "contactentitykey",
        0 as "contactentitykeyvnumber",
        NULL as "description",
        '4112' as "sbuid",
        CV."contactTypeId" as "invdescription",
        NULL as "relationnum",
        NULL as "iscaller",
        NULL as "htctime",
        NULL as "repcomments",
        0 as "isclaimant",
        NULL as "adjustermappingkey",
        NULL as "adjustermappingvnumber",
        NULL as "adjusterfileno",
        NULL as "conversionserver",
        NULL as "sequencenumber",
        CASE
            WHEN CV."contactTypeId" in ('Claimant', 'ClaimantProSe') THEN 'T1'
            ELSE NULL
        END as "claimantcontacttype",
        1 as "validcontact",
        NULL as "reportingstatus"
    from "ISI_Incident" INC
        inner join (select * from "ContactView" where "organizationId" is not NULL) CV on CV."instanceId" = INC."claimmappingkey"  --inner join accounts for no contact records (ie: 14-0171 Test claim)
            left join "Instance" I on I."instanceId" = CV."instanceId"
            left join "Location" L on L."locationId" = CV."locationId"
                left join "Policy" P on P."policyId" = I."policyId"
-- SCOPE
    where P."effectiveDate" > '31-DEC-2005'
      and I."reportDate" > '31-DEC-2006'
     -- REQUIRED FIXES
union all
    select
    -- tortfeasors with no contactview records
        INC."incidentId" as "claimmappingkey",
        0 as "claimvnumber",
        81 as "invno",
        I."mainLawyerId" as "contactentitykey",
        0 as "contactentitykeyvnumber",
        NULL as "description",
        '4112' as "sbuid",
        'Tortfeasor' as "invdescription",
        NULL as "relationnum",
        NULL as "iscaller",
        NULL as "htctime",
        NULL as "repcomments",
        0 as "isclaimant",
        NULL as "adjustermappingkey",
        NULL as "adjustermappingvnumber",
        NULL as "adjusterfileno",
        NULL as "conversionserver",
        NULL as "sequencenumber",
        NULL as "claimantcontacttype",
        1 as "validcontact",
        NULL as "reportingstatus"
    from "Incident" INC
        left join (select * from "Instance" where "mainLawyerId" is not NULL) I on I."instanceId" = INC."incidentId"
            left join "Policy" P on P."policyId" = I."policyId"
    where INC."incidentId" not in (select "instanceId" from "ContactView" where "contactTypeId" = 'Tortfeasor' and "instanceId" = INC."incidentId")
      and P."effectiveDate" > '31-DEC-2005'
      and I."reportDate" > '31-DEC-2006'
    -- REQUIRED FIXES       
      
order by "claimmappingkey", "invno"
;
/*--- Testing ---*/
select * from "ISI_InvolvedPartyINCView"; where "claimmappingkey" in ();
select * from "ContactView" where "instanceId" in ('14-0171', '14-0123', '06-0076');

--------------------------------------------------------------------------------
/*
    ISI_InvolvedRelationView
    c_involvedrelation
    
    Dependancies: 
    
    NOTES:
     
    History
    01/23/2018  FZ  Created
    03/02/2018  FZ  ClaimantAttorney = PAT
*/
create or replace view "ISI_InvolvedRelationINCView" as
    select
        CV."claimmappingkey" as "claimmappingkey",
        CV."claimvnumber" as "claimvnumber",
        CV."invno",
        CASE 
            ---select * from s_list_values_mapping where listname = 'ClaimPartyType' order by listname;
            --03/21/2018 notes insured=claim contact, other  letertoother=other
            WHEN CV."invdescription" in ('DefenseAttorney', 'OutOfStateDefense') and CV."invno" < 43 THEN 'DAT'  --defense lawyer
            WHEN CV."invdescription" in ('DefenseAttorney', 'OutOfStateDefense') and CV."invno" > 43 THEN 'DFM'  --defense firm
            WHEN CV."invdescription" in ('ClaimantAttorney') and CV."invno" < 43 THEN 'PAT' --plaintiff attorney
            WHEN CV."invdescription" in ('ClaimantAttorney') and CV."invno" > 43 THEN 'PFM' --plaintiff firm
            WHEN CV."invdescription" in ('Expert') THEN 'EX' --Expert
            WHEN CV."invdescription" in ('Mediator') THEN 'MED'  --mediator
            WHEN CV."invdescription" in ('Claimant', 'ClaimantProSe') THEN 'CL' --third party claimant
            WHEN CV."invdescription" in ('CoverageCounsel') THEN 'COVC'  --coverage counsel
            WHEN CV."invdescription" in ('AccountingConsultant', 'BankruptcyConsultant', 'Consultant', 'CriminalConsultant', 'FamilyConsultant', 'InsuranceConsultant', 'LitigationConsultant', 'PIDefConsultant', 'PIPlaintConsultant', 'ProbateConsultant') THEN 'CONS' --consultant
            WHEN CV."invdescription" in ('AON') THEN 'BRO' --broker
            WHEN CV."invdescription" in ('Firm', 'Tortfeasor') THEN 'FPO' --insured/first party involved 
            WHEN CV."invdescription" in ('InsuredAttorney') THEN 'IATT'  --insured attorney
            WHEN CV."invdescription" in ('DeductibleReimburse') THEN 'DPA' --deductible payor
            WHEN CV."invdescription" in ('GrievanceCounsel') THEN 'GRVC'  --grievance counsel
            WHEN CV."invdescription" in ('RepairCounsel') THEN 'RCOU' --repair counsel
            WHEN CV."invdescription" in ('BillingAddress') THEN 'PY' --payee
            WHEN CV."invdescription" in ('Insured') THEN '1' --claim contact
            ELSE 'OT'               
        END as "relationtype",      
        NULL as "conversionserver",
        NULL as "sequencenumber"
    from "ISI_InvolvedPartyINCView" CV
;

/*--- Testing ---*/
select * from "ISI_InvolvedRelationINCView"; where "claimmappingkey" in ('14-0171', '14-0123', '06-0076');

--------------------------------------------------------------------------------
/*
    ISI_ClaimantDetailsView
    c_claimantdetails
    
    Dependancies: 
    
    NOTES:
     
    History
    01/24/2018  FZ  Created
    02/12/2018  FZ  Added lawsuitind logic
    03/02/2018  FZ  change clmttype default to 10
*/
create or replace view "ISI_ClaimantDetailsINCView" as
    select 
        IPV."claimmappingkey" as "claimmappingkey",
        IPV."claimvnumber" as "claimvnumber",
        IPV."invno" as "invno",
        '99' as "clmttype", --10=client, 20=non-client, 99-other   --not sure if this is law firm client or ISI client (ie: insured)
        0 as "involvedtype", --directly involved....not sure how to handle GC claims when claimant is insured
        NULL as "percentatfault",
        NULL as "losstransfer",
        NULL as "insuredbyid",
        NULL as "insuredbyvnumber",
        0 as "potentialrecovery",
        NULL as "lawsuitind", 
        NULL as "conversionserver",
        NULL as "sequencenumber"
    from (select * from "ISI_InvolvedPartyINCView" where "invno" = 2) IPV
;

/*--- Testing ---*/
select * from "ISI_ClaimantDetailsINCView";
--------------------------------------------------------------------------------
/*
    ISI_ClaimantSubFileView
    c_claimantsubfile
    
    Dependancies:
    
    NOTES:
    
    History:
    02/21/2018  FZ  Created
*/
create or replace view "ISI_ClaimantSubFileINCView" as
    select
        CD."claimmappingkey" as "claimmappingkey",
        CD."claimvnumber" as "claimvnumber",
        CD."invno" as "invno",
        'LAW_PL_19000101_21121231_4112' as "claimtypekey",
        'LAW_PL_19000101_21121231_4112_IND' as "subfilekey",
        '4112' as "sbuid",
        NULL as "conversionserver",
        NULL as "sequencenumber"
    from "ISI_ClaimantDetailsINCView" CD
;
    
/*--- Testing ---*/

--------------------------------------------------------------------------------
/*
    ISI_ClaimExaminerView
    c_claimexaminer
    
    Dependancies: 
    
    NOTES:
     
    History
    01/25/2018  FZ  Created
*/
create or replace view "ISI_ClaimExaminerINCView" as
    select
        INC."claimmappingkey" as "claimmappingkey",
        0 as "claimvnumber",
        CASE
            WHEN I."adjuster" = 'Matt' THEN 'mbeier'
            WHEN I."adjuster" = 'Katja' THEN 'kkunzke'
            WHEN I."adjuster" = 'Brian' THEN 'banderson'
            WHEN I."adjuster" = 'Sally' THEN 'sanderson'
            ELSE '>>unknown<<'||COALESCE(I."adjuster", 'NULL') --QFC, NDZ, WHD
        END as "userentitymappingkey",
        '4112' as "sbuid",
        NULL as "conversionserver",
        NULL as "sequencenumber"
    from "ISI_Incident" INC
        left join "Instance" I on I."instanceId" = INC."claimmappingkey"
            left join "Policy" P on P."policyId" = I."policyId"
    --- SCOPE
    where P."effectiveDate" > '31-DEC-2005'
      and I."reportDate" > '31-DEC-2006'
      -- REQUIRED FIXES
    order by INC."claimmappingkey"
;    
    
/*--- Testing ---*/
select * from "ISI_ClaimExaminerINCView";
select unique("adjuster") from "Instance";
select * from "ISI_ClaimExaminerINCView";
--------------------------------------------------------------------------------
/*
    ISI_ClaimExaminerSubfileView
    c_claimexaminersubfile
    
    Dependencies:
        ISI_ClaimExaminerView
        
    History
    05/02/2018  FZ  Created
*/

create or replace view "ISI_ClaimExamSubfileINCView" as 
    select
        CE."claimmappingkey" as "claimmappingkey",
        CE."claimvnumber" as "claimvnumber",
        CE."userentitymappingkey" as "userentitymappingkey",
        CE."sbuid" as "sbuid",
        'LAW_PL_19000101_21121231_4112_IND' as "subfilekey",
        'LAW_PL_19000101_21121231_4112' as "claimtypekey",
        NULL as "conversionserver",
        NULL as "sequencenumber"
    from "ISI_ClaimExaminerINCView" CE
    ;
select * from  "ISI_ClaimExamSubfileINCView" ;  


--------------------------------------------------------------------------------
/*
    ISI_ClaimLossDetailsINCView
    c_cl_clmlossdetails_1
    
    Dependancies:
    
    History
    06/28/2018  FZ  Created
*/
--incidents
create or replace view "ISI_ClaimLossDetailsINCView" as  
    select
        C."incidentId" as "claimmappingkey",
        0 as "claimvnumber",
        NULL as "additionalbenefitstype",
        NULL as "allegederror",
        --NVL(SUBSTR(LEA."lawErrorIdISIList", 0, INSTR(LEA."lawErrorIdISIList", '~')-1), LEA."lawErrorIdISIList") as "allegederrorvalues",
        LEA."lawErrorIdISIList" as "allegederrorvalues",
        --SUBSTR(LAA."lawAreaIdISIList",1,2) as "aopactivity",
        SUBSTR(LAA."lawAreaIdISIList", 1, 2) as "areaofpractice",  --06/27/2018
        NULL as "associatedsublaw",
        --LEA."lawErrorIdISIList" as "associatedsublawvalues",
        LAA."lawAreaIdISIList" as "associatedsublawvalues",
        NULL as "claimremarkdate",
        NULL as "claimremarkind",
        NULL as "claimrepairind",
        NULL as "claimrepairindvo",
        'N' as "conflictofinterestind",
        NULL as "conflictofinteresttype",
        NULL as "cyberdatabreachind",
        NULL as "descofresolution",
        NULL as "dhccomplaintind",
        NULL as "ethicsissueind",
        NULL as "factmalpracticeallegations",
        NULL as "factualcoverageissue",
        NULL as "factualdamages",
        NULL as "factualevaluation",
        NULL as "factualplan",
        NULL as "factualprimarycase",
        NULL as "familyfailure",
        NULL as "feedispluteind",
        '10' as "groundsforror",
        '10' as "groundsforrorvalues",
        NULL as "insurermistakeind",
        NULL as "possibleexcessclaim",
        NULL as "priorsolfile",
        NULL as "reinsreportingrequiredind",
        NULL as "searcherrormadeby",
        NULL as "stateofaccident",
        NULL as "subpoenarequests",
        NULL as "subpeonarequestsvalues",
        NULL as "subpeonatoinsured",
        NULL as "successclaimrepairind",
        NULL as "successclaimrepairindvo",
        NULL as "taxrelatedissueind",
        NULL as "testatorcompetenceind",
        NULL as "timingofresolution",
        NULL as "titleinscompany",
        NULL as "titleinscompanyind",
        NULL as "titleinscompanyvalues",
        NULL as "tpbeneficiary2ind",
        NULL as "tpbeneficiaryind",
        NULL as "trustaccountissueind",
        NULL as "conversionserver",
        NULL as "sequencenumber"
    from "Incident" C
        left join "ISILawErrorAggListView" LEA on LEA."instanceId" = C."incidentId"
        left join "ISILawAreaAggListView" LAA on LAA."instanceId" = C."incidentId"
        left join "Instance" I on I."instanceId" = C."incidentId"
            left join "Policy" P on P."policyId" = I."policyId"
    --SCOPE
    where P."effectiveDate" > '31-DEC-2005'
      and I."reportDate" > '31-DEC-2006'
      
       -- REQUIRED FIXES     
order by "claimmappingkey"
;

/**** Testing ****/
select * from "ISILawAreaAggListView" where "instanceId" = 'Croolo5';
select * from "ISI_ClaimLossDetailsINCView";
select * from "Incident" C left join "Instance" I on I."instanceId" = C."incidentId"
            left join "Policy" P on P."policyId" = I."policyId" 
            ;
