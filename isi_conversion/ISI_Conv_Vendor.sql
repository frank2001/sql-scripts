/*******************************************************************************
	ISI_Conv_Vendor.sql
    
    ISI Conversion Scripts
	Scripts used to extract LAW data into ISI interim conversion tables
	
	Naming convention = 'ISI_'+ ISI tablename minus leading alpha underscore + 'View'
	
	Created: FZ	09/25/2017
	Revised: FZ 11/28/2017  Modified to select company/firm instead of individual lawyer
                            unless lawyer had no association with company
*******************************************************************************/
--work area
select 
    EV."eventDate",
    O.* 
from "Organization" O
    left join "Entity" E on E."entityId" = O."organizationId"
        left join "Event" EV on EV."eventId" = E."eventId"
;
select * from "Organization" where "organizationId" in ('WHERRA');
select * from "Entity" where "entityId" in ('WHERRA');
select * from "Entity";
select * from "Contact" where "contactId" = 'CT12365733'; --ROTTKENF

select unique(C."contactEntityId") from "Contact" C where C."contactTypeId" NOT IN ('Firm', 'Tortfeasor', 'Insured', 'Claimant', 'ClaimantProSe', 'BillingAddress', 'DeductibleReimburse') order by C."contactEntityId";
select * from "Contact" where "organizationId" is NULL and "contactTypeId" NOT IN ('Firm', 'Tortfeasor', 'Insured', 'Claimant', 'ClaimantProSe', 'BillingAddress', 'DeductibleReimburse') and "contactEntityId" = 'DOYLL2';
/******************
**  VENDOR
******************/

-- ISI_VendorView
-- need to identify claim vendors, not all law firms
create or replace view "ISI_VendorView" as
-- individuals not associated with company/firm
    select
        NULL as "mappingid",  --deprecated per documentation
        NULL as "mappingvnumber",  --deprecated per documentation
        E."entityId" as "entitykey",
        0 as "entitykeyvnumber",
        NULL as "previousvendorkey",  --possible SAGE vendor Id
        NULL as "businessnumber", --VAT #??
        'A' as "status",
        CASE
            WHEN O."establishDate" IS NOT NULL THEN TO_CHAR(O."establishDate", 'YYYYMMDD')
            WHEN EV."eventDate" IS NOT NULL THEN TO_CHAR(EV."eventDate", 'YYYYMMDD')
            ELSE NULL
        END AS "inceptiondate",
        NULL as "arap_glaccountmappingid",  --deprecated per documentation
        NULL as "profilecreditlimit",
        NULL as "profilecreditterms_days",
        '4112' as "sbuid",
        E."entityName" as "description",
        C."contactEntityId" as "contactentitykey",
        0 as "contactentitykeyvnumber",
        E."entityId"||'_'||E."mainLocationId" as "defaultaddresskey",  -- locationid
        NULL as "defaultcontactmethodkey",
        NULL as "conversionserver",
        NULL as "sequencenumber",
        CASE
            WHEN F."firmId" IS NOT NULL THEN 'LF'
            ELSE 'O'
        END as "vendortype",
        NULL as "taxid",  -- information in SAGE
        '0' as "taxexemptind",  -- information in SAGE
        '0' as "corporationind",  -- information in SAGE
        '0' as "foreignentityind",
        NULL as "screenkey",  --????
        'CHK' as "settlementmethod",
        'M' as "deliverymethod",  --default=M (mail) ... E (Email) or F (Fax)
        E."entityId"||'_'||E."mainLocationId" as "deliveryaddresskey", --**entityId_locationId
        NULL as "deliverymethodkey", --if deliverymethod=E or F, approp key
        0 as "separatechequeind",
        '219000' as "glaccountid",  --219000 Trade Payable + Acccrual
        0 as "labelprintind",
        NULL as "boxcode1099"
    from (select unique("contactEntityId"), "entityId" from "Contact" where "contactTypeId" NOT IN ('Firm', 'Tortfeasor', 'Insured', 'Claimant', 'ClaimantProSe', 'BillingAddress', 'DeductibleReimburse')) C
        left join "Entity" E on E."entityId" = C."contactEntityId"
            left join "Organization" O on O."organizationId" = E."entityId"
                left join "Event" EV on EV."eventId" = E."eventId"
        left join "Firm" F on F."firmId" = C."contactEntityId"
    where C."entityId" is NULL 
       or C."entityId" = 'LFUNKNOWN'
union all
-- firm or company names
    select
        NULL as "mappingid",  --deprecated per documentation
        NULL as "mappingvnumber",  --deprecated per documentation     
        E."entityId" as "entitykey",
        0 as "entitykeyvnumber",
        NULL as "previousvendorkey",  --possible SAGE vendor Id
        NULL as "businessnumber", --VAT #??
        'A' as "status",
        CASE
            WHEN O."establishDate" IS NOT NULL THEN TO_CHAR(O."establishDate", 'YYYYMMDD')
            WHEN EV."eventDate" IS NOT NULL THEN TO_CHAR(EV."eventDate", 'YYYYMMDD')
            ELSE NULL
        END AS "inceptiondate",
        NULL as "arap_glaccountmappingid",  --deprecated per documentation
        NULL as "profilecreditlimit",
        NULL as "profilecreditterms_days",
        '4112' as "sbuid",
        E."entityName" as "description",
        C."entityId" as "contactentitykey",
        0 as "contactentitykeyvnumber",
        E."entityId"||'_'||E."mainLocationId" as "defaultaddresskey",  -- locationid
        NULL as "defaultcontactmethodkey",
        NULL as "conversionserver",
        NULL as "sequencenumber",
        CASE
            WHEN F."firmId" IS NOT NULL THEN 'LF'
            ELSE 'O'
        END as "vendortype",
        NULL as "taxid",  -- information in SAGE
        '0' as "taxexemptind",  -- information in SAGE
        '0' as "corporationind",  -- information in SAGE
        '0' as "foreignentityind",
        NULL as "screenkey",  --????
        'CHK' as "settlementmethod",
        'M' as "deliverymethod",  --default=M (mail) ... E (Email) or F (Fax)
        E."entityId"||'_'||E."mainLocationId" as "deliveryaddresskey", --**entityId_locationId
        NULL as "deliverymethodkey", --if deliverymethod=E or F, approp key
        0 as "separatechequeind",
        '219000' as "glaccountid",  --219000 Trade Payable + Acccrual
        0 as "labelprintind",
        NULL as "boxcode1099"
    from (select unique("entityId") from "Contact" where "contactTypeId" NOT IN ('Firm', 'Tortfeasor', 'Insured', 'Claimant', 'ClaimantProSe', 'BillingAddress', 'DeductibleReimburse')) C
        left join "Entity" E on E."entityId" = C."entityId"  --changed to use company (entityId)
            left join "Organization" O on O."organizationId" = E."entityId"
                left join "Event" EV on EV."eventId" = E."eventId"
        left join "Firm" F on F."firmId" = C."entityId"
    where C."entityId" is not NULL
;

select * from "ISI_VendorView";
select count(*) from "ISI_VendorView";
select "entitykey", count(*) from "ISI_VendorView" group by "entitykey" having count(*) > 1;

select * from "ISI_VendorView" where "entitykey" in ('EY000202294', 'LN13789109') order by "entitykey";
select * from "Contact" where "contactEntityId" in ('EY000202294', 'LN13789109');
select * from "Contact" where "contactEntityId" in ('SCHMT__W', 'LN13789109');
select * from "Entity" where "entityId" in ('SCHMT__W');

select * from "Contact" where "contactEntityId" is NULL;
select * from "ISI_VendorView" where "entitykey" is NULL;
select * from "Location" where "locationId" = 'LFLIEBCO';
select * from "ISI_VendorView" where "entitykey" = 'SCHMT__W';
select * from "ISI_AddressView" where "addresskey" = 'SCHMT__W_LFLIEBCO'; --SCHMT__W_LFLIEBCO
select * from "ISI_AddressView" where "addresskey" = 'CROOMICP_LCPETEJO';
select count(*) from "ISI_AddressView";

select 
    C.*,
 --   O.*
    E.*
from (select unique("contactEntityId") from "Contact" where "contactTypeId" NOT IN ('Firm', 'Tortfeasor', 'Insured', 'Claimant', 'ClaimantProSe', 'BillingAddress', 'DeductibleReimburse')) C
 --   left join "Organization" O on O."organizationId" = C."contactEntityId"
        left join "Entity" E on E."entityId" = C."contactEntityId"
;