/*  
--  QUOTES
*/
--------------------------------------------------------------------------------
/*  UTILITIES
--  These are support views used to assemble activity data from LAW.
*/
select 
    A.* 
from "Application" A
where A."effectiveDate" > SYSDATE - 30;

select e.*, A.* from "Application" A 
    left join "Event" E on E."eventId" = A."eventId"
where A."effectiveDate" > SYSDATE - 30;
select * from "ApplicationLimit";
select * from "Quote";


--------------------------------------------------------------------------------
/*
    ISI_QuoteView
    p_policy
    
    Dependancies: 
    
    NOTES:
        - active quote is any quote with an effective date > SYSDATE - 30
        
    History
    03/11/2018  FZ  Created
*/
--create or replace view "ISI_QuoteView" as 
    -- recent quotes/applications
    select
        A."applicationId"||'_'||AL."applicationLimitId" as "conversionreference",
        'QUO' as "transactiontype",
        TO_CHAR(A."effectiveDate", 'YYYYMMDD') as "transactioneffectivedate",
        1 as "transactionseqno",
        'LPL_19000101_20991231_4112' as "pcmmappingkey",
        '4112' as "sbuid",
        NULL as "inceptiondate",
        TO_CHAR(A."effectiveDate", 'YYYYMMDD') as "policyeffectivedate",
        TO_CHAR(ADD_MONTHS(A."effectiveDate", A."policyTerm"), 'YYYYMMDD') as "policyexpirydate",
        TO_CHAR(A."effectiveDate", 'YYYYMMDD') as "veffectivedate",
        TO_CHAR(ADD_MONTHS(A."effectiveDate", A."policyTerm"), 'YYYYMMDD') as "vexpirydate",
        '000000' as "veffectivetime",
        '000000' as "vexpirytime",
        NULL as "policykey",
        NULL as "previouspolicykey",
        NULL as "alternatepolicykey",
        NULL as "agentbrokerpolicykey",
        NULL as "binderkey",
        NULL as "policynextunderwritedate",
        A."firmId" as "cliententitymappingkey",
        'M' as "renewaltype",
        A."policyTerm" as "termlengthind",
        'M' as "termlengthindunit",
        'D' as "billingtype",
        NULL as "papind",
        NULL as "paymentplan",
        NULL as "planeffdate",
        NULL as "planexpdate",
        'N' as "subscriptionind",
        NULL as "facultativeind",
        'USD' as "premiumcurrencycd",
        'USD' as "limitcurrencycd",
        NULL as "sbuofficekey",
        NULL as "underwriterentitymappingkey",
        NULL as "priorperiodind",
        0 as "cancellationtype",
        NULL as "canreturnpremium",
        NULL as "canearnedpremium",
        NULL as "reinstateexpirydate",
        NULL as "conversionserver",
        NULL as "sequencenumber",
        NULL as "relatedrevrcatranstype",
        NULL as "relatedrevrcatranseffdate",
        NULL as "relatedrevrcatransseqno",
        1 as "claimsmadeind",
        NULL as "retrodate",
        NULL as "extendreporteffdate",
        NULL as "extendreportexpdate",
        CASE
            WHEN E2."eventTypeId" = 'ReissueApp' THEN 101
            ELSE 100
        END as "vreasoncode",
        TO_CHAR(A."receiveDate", 'YYYYMMDDHH24MISS') as "ventrydatetime",
        'jharder' as "vusermappingkey",
        NULL as "postedyearmth",
        'D' as "policysourceind",
        NULL as "assumedcompanymappingkey",
        NULL as "writingcompanymappingkey",
        NULL as "freetradezoneriskcode",
        'EN' as "languagecd",
        NULL as "overridereasoncd",
        NULL as "overridereasonnote",
        NULL as "postedwweobjectid",
        NULL as "postedwwevnumber",
        'OPEN' as "transactionstatus", --OPEN, CLOSED
        NULL as "transactionstatus",
        NULL as "finaladjustmentind",
        'LPL_Ratebook' as "rateeditionname",
        1 as "rateeditionversion",
        NULL as "wipstatus", -- probably use event folder
        CASE
            WHEN E2."eventTypeId" = 'ReissueApp' THEN 'REN'
            ELSE 'NEW'
        END as "quoterelatedtranstype",
        NULL as "relatedtranstype",
        NULL as "relatedtranseffdate",
        NULL as "relatedtransseqno",
        A."applicationId"||'_'||Q."quoteId"||'_'||Q."mainQuoteBookId" as "quotekey",
        TO_CHAR(E."eventDate", 'YYYYMMDD') as "quotedate",
        120 as "quotedaystoaccept",
        TO_CHAR(A."effectiveDate" + 30, 'YYYYMMDD') as "quoteexpirydate",  --DEFAULT VALUE--Verify
        Q."quoteName" as "quotedefinedname",
        NULL as "quoteseriescode",
        9 as "quotestatus",
        1 as "quotedocumentsprinted", 
        NULL as "brkwritingcompanymappingkey",
        NULL as "brokeragepolicycommission",
        0 as "quickquoteind",
        NULL as "referralstatus",
        NULL as "documentcategory",
        NULL as "frontedcompanymappingkey",
        NULL as "assumedcommissionamount",
        NULL as "assumedcommissiontype",
        NULL as "policycompositiontype",
        NULL as "predominantjurisdiction",
        NULL as "predominantpolicyregion",
        NULL as "totalwaivedpremium",
        NULL as "accountingdate",
        NULL as "quotedelcinedby",
        NULL as "quotecompetitor",
        NULL as "quotecompetitorpremium",
        NULL as "policycontactentitykey",  --S/B firm contact
        NULL as "policycontactentitykeyvnumber",
        NULL as "comments",
        NULL as "renewalstatus",
        NULL as "insurancestatus",
        AL."perClaimLimit" as "liabilitylimit",
        NULL as "overridemailingaddrkey",
        NULL as "auditreportingind",
        NULL as "convertedtopolicytrneffdate",
        NULL as "convertedtopolicytrnseqno",
        NULL as "quotecompletedreasoncode",
        NULL as "quotecompletedreasonnotes",
        NULL as "quotelastmodifieddatetime"
    from "Quote" Q
        left join "Application" A on A."applicationId" = Q."applicationId"
            left join "ApplicationLimit" AL on AL."applicationId" = A."applicationId"
            left join "Event" E2 on E2."eventId" = A."eventId"
        left join "Event" E on E."eventId" = Q."eventId"
    where A."effectiveDate" > SYSDATE - 30
      and A."policyId" is NULL
;