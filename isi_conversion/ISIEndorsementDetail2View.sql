create or replace view "ISIEndorsementDetail2View" as 
with "EndorsementDetail1" as (
select 
    P."firmId" as "firmId",
    P."policyId" as "policyId",
    P."policyNumber" as "policyNumber",
    P."effectiveDate" as "effectiveDate",
    P."expirationDate" as "expirationDate",
    P."cancelEffectiveDate" as "cancelEffectiveDate",
    P."newReissue" as "newReissue",
    COALESCE(PA."premium",0) as "coveragePremium",
    COALESCE(PA."premiumAdjust",0) as "coveragePremiumAdjust",
    COALESCE(PA."financeCharge",0) as "coverageFinanceCharge",
    COALESCE(PA."transfer",0) as "coverageTransfer",
    COALESCE(PA."writeOff",0) as "coverageWriteOff",
    COALESCE(PA."dueAmount",0) as "coverageDueAmount",
    COALESCE(PA."receiveAmount",0) as "coverageReceiveAmount",
    COALESCE(PA."returnAmount",0) as "coverageReturnAmount",
    E."eventTypeId" as "eventTypeId",
    CASE
        WHEN EN."endorsementDesc" IS NULL THEN 'Policy Issue'
        ELSE EN."endorsementDesc"
    END as "endorsementDesc",
    C."coverageId" as "coverageId",
    CASE
        WHEN EN."effectiveDate" is NULL THEN P."effectiveDate"
        ELSE EN."effectiveDate" 
    END as "endorsementEffectiveDate",
    TO_NUMBER(COALESCE(EN."endorsementNumber", '0')) + 1 as "endorsementNumber"
from "Coverage" C
    left join "Event" E on E."eventId" = C."eventId"
    left join "Policy" P on P."policyId" = C."policyId"
    left join "Endorsement" EN on EN."endorsementId" = C."coverageId"
    left join (select "coverageId", sum("premium") as "premium", sum("premiumAdjust") as "premiumAdjust", sum("financeCharge") as "financeCharge", sum("transfer") as "transfer", sum("writeOff") as "writeOff", sum("dueAmount") as "dueAmount", sum("receiveAmount") as "receiveAmount", sum("returnAmount") as "returnAmount" from "PolicyAccounting" group by "coverageId") PA on PA."coverageId" = C."coverageId"
where C."coverageId" not in ('CG001148214', 'CG13630118') --04/27/2018--bogus fixes

)
--   ,"EndorsementDetail2" as (
    select
        E1.*,
        row_number() over (partition by E1."firmId", E1."endorsementEffectiveDate" order by E1."policyNumber", E1."endorsementNumber") as "transactionseqno",
         CASE
            WHEN E1."eventTypeId" in ('Cvg-CP', 'Cvg-EC', 'Cvg-ED', 'Cvg-EU', 'Cvg-NP', 'Cvg-ND', 'Cvg-NU') THEN 'ERP'  -- tail endorsement   
            WHEN E1."eventTypeId" = 'Cvg-CN' THEN --cancellation
                CASE
                    WHEN E1."cancelEffectiveDate" IS NOT NULL THEN --and P."cancelEffectiveDate" = P."effectiveDate" THEN  --flat cancelled policy
                        CASE
                            WHEN E1."cancelEffectiveDate" = lead(E1."effectiveDate") over (order by E1."firmId", E1."effectiveDate", E1."policyNumber", E1."endorsementNumber") and E1."firmId" = lead(E1."firmId") over (order by E1."firmId", E1."effectiveDate", E1."policyNumber", E1."endorsementNumber") THEN 'RCA'  --cancel for purposes of rewrite
                            ELSE 'CAN'  --just cancelled flat without a reissue -- at least not effective the previous term's effective date
                        END
                    ELSE 'CAN'
                END  
            WHEN E1."eventTypeId" = 'PolicyIssue' and E1."newReissue" = 'R' and E1."effectiveDate" = lag(E1."cancelEffectiveDate") over (order by E1."firmId", E1."effectiveDate", E1."policyNumber", E1."endorsementNumber") THEN 'RWR' -- cancel/rewrite  
            WHEN E1."eventTypeId" = 'PolicyIssue' and E1."newReissue" = 'N' and E1."effectiveDate" = lag(E1."cancelEffectiveDate") over (order by E1."firmId", E1."effectiveDate", E1."policyNumber", E1."endorsementNumber") THEN 'RWN' -- cancel/rewrite
            WHEN E1."eventTypeId" = 'PolicyIssue' and E1."newReissue" = 'N' THEN 'NEW'
            WHEN E1."eventTypeId" = 'PolicyIssue' and E1."newReissue" = 'R' and (FX."ISIFirmEarliestDate" = FPV."effectiveDate") THEN 'NEW'  --ADDED: 01/30/2018 to fix #4
            WHEN E1."eventTypeId" = 'PolicyIssue' and E1."newReissue" = 'R' THEN 'REN'
            ELSE 'END'
        END as "transactiontype"   
    --from "EndorsementDetail1" E1
    from (select * from "EndorsementDetail1" where "eventTypeId" not in ('Cvg-PA', 'Cvg-DA','Cvg-NC', 'Cvg-EL', 'Cvg-CC', 'Cvg-RC', 'Cvg-AT', 'Cvg-EV', 'Cvg-MN', 'Cvg-RL', 'Cvg-PE')) E1 --
        left join "ISIFirmPolicyView" FPV on FPV."policyId" = E1."policyId" 
        left join "ISIFirmDatesView" FX on FX."firmId" = E1."firmId"
/*   
    
        
)
  ,"EndorsementDetail3" as (
    select
        E2.*,
        --MAX("endorsementNumber") over (partition by E2."firmId", E2."endorsementEffectiveDate" order by E2."policyId", E2."endorsementEffectiveDate") as "transactionseqno" 
        MAX("transactionseqno") over (partition by E2."firmId", E2."endorsementEffectiveDate" order by E2."endorsementEffectiveDate") as "maxtransactionseqno" 
    from "EndorsementDetail2" E2   
)
    select
        E3.*,
        EX."transactiontype" as "maxtransactiontype" 
    from "EndorsementDetail3" E3
        left join "EndorsementDetail3" EX on EX."firmId" = E3."firmId" and EX."endorsementEffectiveDate" = E3."endorsementEffectiveDate" and EX."transactionseqno" = E3."maxtransactionseqno"
*/

    order by E1."policyNumber", E1."endorsementEffectiveDate", E1."endorsementNumber"
;

select * from "ISIEndorsementDetail2View" ;
















    select
        E2.*,
        MAX("endorsementNumber") over (partition by E2."firmId", E2."endorsementEffectiveDate" order by E2."policyId", E2."endorsementEffectiveDate") as "transactionseqno" 
    from "EndorsementDetail2" E2              
    
    order by E2."policyNumber", E2."endorsementEffectiveDate", E2."endorsementNumber"
;

select * from "ISIEndorsementDetailView";