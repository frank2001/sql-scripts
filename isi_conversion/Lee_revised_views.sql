/* ISIEndorsementDetail2View.sql */
CREATE FUNCTION months_between (t_start timestamp, t_end timestamp)
    RETURNS integer
    AS $$
        SELECT
            (
                12 * extract('years' from a.i) + extract('months' from a.i)
            )::integer
        from (
            values (justify_interval($2 - $1))
        ) as a (i)
    $$
    LANGUAGE SQL
    IMMUTABLE
    RETURNS NULL ON NULL INPUT;
  
  
  
CREATE OR REPLACE VIEW ISIFirmPolicyView (firmId, firmEntityLocator, policyId, policyNumber, policySuffix, policyFirmId, firmEntityName, policyFirmName, effectiveDate, expirationDate, cancelEffectiveDate, policyFirmLocator, ISITransaction, relatedrevrcatranseffdate) 
AS 
  SELECT
 f1.firmId,
 e.entityLocator as firmEntityLocator,      -- V1 used Firm.firmLocator
 p.policyId, 
 p.policyNumber,
 p.policySuffix,
 p.firmId AS policyFirmId,
 e.entityName AS firmEntityName,        -- V1 used Firm.firmName
-- e.entityName AS policyFirmName,
 p.firmName as policyFirmName,
 p.effectiveDate,
 p.expirationDate,
 p.cancelEffectiveDate,
 e.entityLocator AS policyFirmLocator,      -- V1 used Firm.firmLocator
 CASE
    WHEN p.newReissue = 'R' THEN
        CASE
            WHEN p.effectiveDate = lag(p.cancelEffectiveDate) over (order by p.firmId, p.effectiveDate, p.policyNumber) THEN 'RWR' --rewrite after cancel of reissue
            ELSE 'REN'
        END
    WHEN p.newReissue = 'N' THEN
        CASE
            WHEN p.effectiveDate = lag(p.cancelEffectiveDate) over (order by p.firmId, p.effectiveDate, p.policyNumber) THEN 'RWN' --rewrite after cancel of new business
            ELSE 'NEW'
        END
 END as ISITransaction,
 CASE
    WHEN p.firmId = lead(p.firmId) over (order by p.firmId, p.effectiveDate, p.policyNumber) and p.cancelEffectiveDate is not NULL THEN lead(p.effectiveDate) over (order by p.firmId, p.effectiveDate, p.policyNumber)   --used when ISITransaction = RWR or RWN
    ELSE NULL
 END as relatedrevrcatranseffdate   
FROM Firm f1
    left join Policy p on p.firmId = f1.firmId
    left join Entity e on e.entityId = f1.firmId
   
--where p.firmId in ('GAUTLA')
;


--------------------------------------------------------------------------------
create or replace view ISIFirmDatesView as
select 
    p.firmId,
    MIN(p.effectiveDate) as ISIFirmEarliestDate,
    MAX(p.expirationDate) as ISIFirmLatestDate
from Policy P
group by p.firmId
order by p.firmId
;


--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
/*
    ISIEndorsementDetail2View

    History:
    06/01/2018  LP INCLUDE 'Cvg-DA' Endorsements
    
*/

create or replace view ISIEndorsementDetail2View as 
with EndorsementDetail1 as (
select 
    p."firmId" as "firmId",
    p."policyId" as "policyId",
    p."policyNumber" as "policyNumber",
    p."effectiveDate" as "effectiveDate",
    p."expirationDate" as "expirationDate",
    p."cancelEffectiveDate" as "cancelEffectiveDate",
    p."newReissue" as "newReissue",
    COALESCE(pa."premium",0) as "coveragePremium",
    COALESCE(pa."premiumAdjust",0) as "coveragePremiumAdjust",
    COALESCE(pa."financeCharge",0) as "coverageFinanceCharge",
    COALESCE(pa."transfer",0) as "coverageTransfer",
    COALESCE(pa."writeOff",0) as "coverageWriteOff",
    COALESCE(pa."dueAmount",0) as "coverageDueAmount",
    COALESCE(pa."receiveAmount",0) as "coverageReceiveAmount",
    COALESCE(pa."returnAmount",0) as "coverageReturnAmount",
    e."eventTypeId" as "eventTypeId",
    CASE
        WHEN en."endorsementDesc" IS NULL THEN 'Policy Issue'
        ELSE en."endorsementDesc"
    END as "endorsementDesc",
    c."coverageId" as "coverageId",
    CASE
        WHEN en."effectiveDate" is NULL THEN p."effectiveDate"
        ELSE en."effectiveDate" 
    END as "endorsementEffectiveDate",
    (COALESCE(en."endorsementNumber", '0')) + 1 as "endorsementNumber"
from "Coverage" c
    left join "Event" e on e."eventId" = c."eventId"
    left join "Policy" p on p."policyId" = c."policyId"
    left join "Endorsement" en on en."endorsementId" = c."coverageId"
    left join (select "coverageId", sum("premium") as "premium", sum("premiumAdjust") as "premiumAdjust", sum("financeCharge") as "financeCharge", sum("transfer") as "transfer", sum("writeOff") as "writeOff", sum("dueAmount") as "dueAmount", sum("receiveAmount") as "receiveAmount", sum("returnAmount") as "returnAmount" from "PolicyAccounting" group by "coverageId") pa on pa."coverageId" = c."coverageId"
where c."coverageId" not in ('CG001148214', 'CG13630118') --04/27/2018--bogus fixes

)
--   ,EndorsementDetail2 as (
    select
        e1.*,
        row_number() over (partition by e1."firmId", e1."endorsementEffectiveDate" order by e1."policyNumber", e1."endorsementNumber") as "transactionseqno",
         CASE
            WHEN e1."eventTypeId" in ('Cvg-CP', 'Cvg-EC', 'Cvg-ED', 'Cvg-EU', 'Cvg-NP', 'Cvg-ND', 'Cvg-NU') THEN 'ERP'  -- tail endorsement   
            WHEN e1."eventTypeId" = 'Cvg-CN' THEN --cancellation
                CASE
                    WHEN e1."cancelEffectiveDate" IS NOT NULL THEN --and p."cancelEffectiveDate" = p."effectiveDate" THEN  --flat cancelled policy
                        CASE
                            WHEN e1."cancelEffectiveDate" = lead(e1."effectiveDate") over (order by e1."firmId", e1."effectiveDate", e1."policyNumber", e1."endorsementNumber") and e1."firmId" = lead(e1."firmId") over (order by e1."firmId", e1."effectiveDate", e1."policyNumber", e1."endorsementNumber") THEN 'RCA'  --cancel for purposes of rewrite
                            ELSE 'CAN'  --just cancelled flat without a reissue -- at least not effective the previous term's effective date
                        END
                    ELSE 'CAN'
                END  
            WHEN e1."eventTypeId" = 'PolicyIssue' and e1."newReissue" = 'R' and e1."effectiveDate" = lag(e1."cancelEffectiveDate") over (order by e1."firmId", e1."effectiveDate", e1."policyNumber", e1."endorsementNumber") THEN 'RWR' -- cancel/rewrite  
            WHEN e1."eventTypeId" = 'PolicyIssue' and e1."newReissue" = 'N' and e1."effectiveDate" = lag(e1."cancelEffectiveDate") over (order by e1."firmId", e1."effectiveDate", e1."policyNumber", e1."endorsementNumber") THEN 'RWN' -- cancel/rewrite
            WHEN e1."eventTypeId" = 'PolicyIssue' and e1."newReissue" = 'N' THEN 'NEW'
            WHEN e1."eventTypeId" = 'PolicyIssue' and e1."newReissue" = 'R' and (fx."ISIFirmEarliestDate" = fpv."effectiveDate") THEN 'NEW'  --ADDED: 01/30/2018 to fix #4
            WHEN e1."eventTypeId" = 'PolicyIssue' and e1."newReissue" = 'R' THEN 'REN'
            ELSE 'END'
        END as "transactiontype"   
    --from EndorsementDetail1 e1
    /* LP INCLUDE 'Cvg-DA' Endorsements */
    -- from (select * from EndorsementDetail1 where eventTypeId not in ('Cvg-PA', 'Cvg-DA','Cvg-NC', 'Cvg-EL', 'Cvg-CC', 'Cvg-RC', 'Cvg-AT', 'Cvg-EV', 'Cvg-MN', 'Cvg-RL', 'Cvg-PE')) E1 --
    from (select * from "EndorsementDetail1" where "eventTypeId" not in ('Cvg-PA', 'Cvg-NC', 'Cvg-EL', 'Cvg-CC', 'Cvg-RC', 'Cvg-AT', 'Cvg-EV', 'Cvg-MN', 'Cvg-RL', 'Cvg-PE')) E1 --
        left join "ISIFirmPolicyView" fpv on fpv."policyId" = e1."policyId" 
        left join "ISIFirmDatesView" fx on fx."firmId" = e1."firmId"
/*   
    
        
)
  ,EndorsementDetail3 as (
    select
        e2.*,
        --MAX(endorsementNumber) over (partition by e2.firmId, e2.endorsementEffectiveDate order by e2.policyId, e2.endorsementEffectiveDate) as transactionseqno 
        MAX(transactionseqno) over (partition by e2.firmId, e2.endorsementEffectiveDate order by e2.endorsementEffectiveDate) as maxtransactionseqno 
    from EndorsementDetail2 e2   
)
    select
        e3.*,
        ex.transactiontype as maxtransactiontype 
    from EndorsementDetail3 e3
        left join EndorsementDetail3 ex on ex.firmId = e3.firmId and ex.endorsementEffectiveDate = e3.endorsementEffectiveDate and ex.transactionseqno = e3.maxtransactionseqno
*/

    order by e1.policyNumber, e1.endorsementEffectiveDate, e1.endorsementNumber
;

/*
select * from ISIEndorsementDetail2View ;



    select
        e2.*,
        MAX(endorsementNumber) over (partition by e2.firmId, e2.endorsementEffectiveDate order by e2.policyId, e2.endorsementEffectiveDate) as transactionseqno 
    from EndorsementDetail2 e2              
    
    order by e2.policyNumber, e2.endorsementEffectiveDate, e2.endorsementNumber
;

select * from ISIEndorsementDetailView;
*/

















/*******************************************************************************
  ISI_Conv_Policy.sql
    
    ISI Conversion Scripts
  Scripts used to extract LAW data into ISI interim conversion tables
  
  Naming convention = 'ISI_'+ ISI tablename minus leading alpha underscore + 'View'
    
    Scope:
    Scope can be limited by restricting input (ie: firms, effective dates, etc..) 
    in ISI_PolicyView, ISI_BillingDetailView, ISI_InsuredsView
    
    10/01/2017  Firm = KIMLAV
  
    
    History:
  Created: FZ 09/29/2017
  Revised: 
*******************************************************************************/

/* UTILITIES */
------------------------------------------
CREATE OR REPLACE VIEW ISIFirmPolicyView (firmId, firmEntityLocator, policyId, policyNumber, policySuffix, policyFirmId, firmEntityName, policyFirmName, effectiveDate, expirationDate, cancelEffectiveDate, policyFirmLocator, ISITransaction, relatedrevrcatranseffdate) AS 
SELECT
 f1.firmId,
 e.entityLocator as firmEntityLocator,      -- V1 used Firm.firmLocator
 p.policyId, 
 p.policyNumber,
 p.policySuffix,
 p.firmId AS policyFirmId,
 e.entityName AS firmEntityName,        -- V1 used Firm.firmName
-- e.entityName AS policyFirmName,
 p.firmName as policyFirmName,
 p.effectiveDate,
 p.expirationDate,
 p.cancelEffectiveDate,
 e.entityLocator AS policyFirmLocator,      -- V1 used Firm.firmLocator
 CASE
    WHEN p.newReissue = 'R' THEN
        CASE
            WHEN p.effectiveDate = lag(p.cancelEffectiveDate) over (order by p.firmId, p.effectiveDate, p.policyNumber) THEN 'RWR' --rewrite after cancel of reissue
            ELSE 'REN'
        END
    WHEN p.newReissue = 'N' THEN
        CASE
            WHEN p.effectiveDate = lag(p.cancelEffectiveDate) over (order by p.firmId, p.effectiveDate, p.policyNumber) THEN 'RWN' --rewrite after cancel of new business
            ELSE 'NEW'
        END
 END as ISITransaction,
 CASE
    WHEN p.firmId = lead(p.firmId) over (order by p.firmId, p.effectiveDate, p.policyNumber) and p.cancelEffectiveDate is not NULL THEN lead(p.effectiveDate) over (order by p.firmId, p.effectiveDate, p.policyNumber)   --used when ISITransaction = RWR or RWN
    ELSE NULL
 END as relatedrevrcatranseffdate   
FROM Firm f1
    left join Policy p on p.firmId = f1.firmId
    left join Entity e on e.entityId = f1.firmId
   
--where p.firmId in ('GAUTLA')
;




--------------------------------------------------------------------------------
-- PolicyLawyerCoverageAllView
-- This view is basically same as PolicyLawyerCoverage but each lawyer only has
-- one record per policy.  Normally deleted lawyers have two records 
-- LP Add init coverage and endorsement coverage
create or replace view PolicyLawyerCoverageAllView as 
  with plc_start as (  
    select 
        p.firmId as firmId,
        p.policyNumber as policyNumber,
        plc.policyId as policyId,
        plc.lawyerId as lawyerId,
        row_number() over (partition by plc.policyId, plc.lawyerId order by plc.eventId) as PLC_seq,
        plc.coverageId,
        plc.eventId,
        MIN(plc.effectiveDate) over (partition by plc.policyId, plc.lawyerId) as effectiveDate,
        CASE 
            WHEN SUBSTR(e.endorsementDesc, 1, 8) = 'WLMEND12' THEN e.effectiveDate
            ELSE MIN(p.expirationDate) over (partition by plc.policyId, plc.lawyerId) 
        END as expirationDate,
        
        plc.retroactiveDate as retroactiveDate,
        CASE
            WHEN SUBSTR(e.endorsementDesc, 1, 8) = 'WLMEND29' THEN e.expirationDate ---MAX(plc.expirationDate) over (partition by plc.policyId, plc.lawyerId)
            ELSE MAX(plc.excludeAfterDate) over (partition by plc.policyId, plc.lawyerId)
        END as excludeAfterDate,
        MAX(plc.excludeBeforeDate) over (partition by plc.policyId, plc.lawyerId) as excludeBeforeDate,
        MIN(TO_CHAR(e2.endorsementEffectiveDate,'YYYYMMDD')::NUMERIC) over (partition by plc.policyId, plc.lawyerId) as transactioneffectivedate,
        MIN(e2.transactionseqno) over (partition by plc.policyId, plc.lawyerId) as transactionseqno
    from PolicyLawyerCoverage plc
        left join Policy p on p.policyId = plc.policyId
        left join Endorsement e on e.endorsementId = plc.coverageId
        left join isiendorsementdetail2view e2 ON e2.policyId = p.policyId AND e2.coverageId = plc.coverageId
       /* where p.policynumber = '0607190' */
--    where plc.policyId = 'AP11696463'
    --group by plc.policyId, plc.lawyerId
)
, plc_mid as (
    select 
        firmId,
        policyNumber,
        policyId,
        lawyerId,
        MIN(effectiveDate) as effectiveDate,
        MIN(expirationDate) as expirationDate,
        MAX(retroactiveDate) as retroactiveDate,
        MAX(excludeAfterDate) as excludeAfterDate,
        MIN(excludeBeforeDate) as excludeBeforeDate,
        MIN(coverageId) as init_coverageId,
        MAX(coverageId) as end_coverageId,
        MIN(transactioneffectivedate) AS transactioneffectivedate,
        MIN(transactionseqno) AS transactionseqno
      from plc_start 
      group by firmId, policyNumber, policyId, lawyerId
)
    select
        firmId,
        policyNumber,
        policyId,
        lawyerId,
        effectiveDate,
        expirationDate,
        retroactiveDate,
        excludeAfterDate,
        excludeBeforeDate,
        init_coverageId,
        end_coverageId,
        transactioneffectivedate,
        transactionseqno,
        row_number() over (partition by firmId, policyNumber order by lawyerId) + 1 as lawyerPolicySequence
    from plc_mid
    
    order by firmId, policyNumber, policyId, lawyerId
;



------------------------------------------ 
create or replace view ISILawyerRetroDateView as 
    select 
        p.policyNumber,
        plc.policyId,
        plc.lawyerId,
        CASE
            WHEN plc.retroactiveDate is NOT NULL and plc.retroactiveDate > plc.effectiveDate THEN plc.effectiveDate   -- possible lawyer on multiple polices at same time, LAW plc does not handle properly
            WHEN plc.retroactiveDate IS NOT NULL and plc.retroactiveDate <= plc.effectiveDate THEN plc.retroactiveDate  --01/25/2018 previously plc.effectiveDate
            ELSE LEAST(lsb.barAdmitDate, l.inPracticeDate)
        END as ISILawyerRetroDate
    from PolicyLawyerCoverageAllView plc   --01/14/2018 previously PolicyLawyerCoverage - issue with lawyers on multiple policies at same time causing multiple records
        left join Policy p on p.policyId = plc.policyId
        left join Lawyer l on l.lawyerId = plc.lawyerId 
            left join LawyerStateBar lsb on lsb.lawyerId = l.lawyerId   
    order by plc.policyId, plc.lawyerId

;


--------------------------------------------------------------------------------
create or replace view ISIPolicyRetroDateView as 
select 
    x.policyId,
    x.policyNumber,
    MIN(ISILawyerRetroDate) as ISIPolicyRetroDate
from ISILawyerRetroDateView x
group by x.policyId, x.policyNumber
;


--------------------------------------------------------------------------------
create or replace view ISIFirmDatesView as
select 
    p.firmId,
    MIN(p.effectiveDate) as ISIFirmEarliestDate,
    MAX(p.expirationDate) as ISIFirmLatestDate
from Policy p
group by p.firmId
order by p.firmId
;



--------------------------------------------------------------------------------
-- ISIConversionReferenceView
-- Determines ISI conversion reference based on policy eff/exp dates
---https://stewashton.wordpress.com/2014/03/16/merging-contiguous-date-ranges/
--select * from ISIFirmPolicyView where firmId = 'KIMLAV';
create or replace view ISIConversionReferenceView as
with grp_starts as (
    select 
        firmId,
        effectiveDate,
        CASE
            WHEN cancelEffectiveDate is not NULL and cancelEffectiveDate <> effectiveDate THEN cancelEffectiveDate
            ELSE expirationDate
        END AS expirationDate,
        case
            when effectiveDate = lag(COALESCE(cancelEffectiveDate, expirationDate)) over (order by firmId, effectiveDate, COALESCE(cancelEffectiveDate, expirationDate)) then 0
  --01/05/2018          when effectiveDate = lag(expirationDate) over (order by firmId, effectiveDate, expirationDate) then 0
            else 1
        end as grp_start
    from ISIFirmPolicyView
)
, grps as (
    select
        firmId,
        effectiveDate,
        expirationDate,
        sum(grp_start) over (order by firmId, effectiveDate, expirationDate) grp
    from grp_starts
)
--create or replace view ISIConversionReferenceView as 
select
    firmId,
    min(effectiveDate) as startDate,
    max(expirationDate) as endDate,
    firmId||'_'||to_char(min(effectiveDate), 'YYYY')||'_'||to_char(max(expirationDate),'YYYY') as conversionreference
from grps
group by firmId, grp
order by firmId, 1, 2
;



-----------------------------------------------------------------------------------------------------------------------------------------
/* NOT DONE YET. Waiting for Quote table */
CREATE OR REPLACE VIEW PremiumPerLawyerView (applicationId, effectiveDate, firmName, firmId, firmLocator, policyId, lawyerPrincipalCount, lawyerOfCounselCount, applicationRatingId, applicationLimitId, Deductible, "Limit", aggregateLimit, firmPremium, premiumPerLawyer1M, premiumPerLawyer1MAdjusted, premiumPerLawyerXS5M, premiumPerLawyerXS10M, DeductibleFactor, LimitFactor, LimitAgg, ContinuityAmountCalc, PremXS5mMinCalc, PremXS10mCalc, ContLegalEdCredit, DefReimbCoverage, LawyerFactor, LawyerFactorAdjusted, LawyerYIP, LawyerRDI, LawyerActivity, LawyerActivityAdjusted, SECCoverage, Base, Firm, SchdAdjPctCalc, YearsInsured, retroactiveDate, lawyerId, inPracticeDuration, lawyerLocator, lawyerName) AS 
  SELECT 
 a.applicationId,
    a.effectiveDate,
    a.firmName,
    a.firmId,
    e2.entityLocator AS firmEntityLocator,
    c.policyId,
    a.lawyerPrincipalCount,
    a.lawyerOfCounselCount,
    q.mainQuoteBookId,
    r.quoteCoverageId,
    r.Deductible,
    r."Limit",
    qc.aggregateLimit,
    r.PremCalc AS firmPremium,
    CASE
      WHEN ABS(r3.SchdAdjPctCalc) < 0.5
--      THEN ROUND((r3.Base * COALESCE(r.DeductibleFactor, 1) * COALESCE(r.LimitFactor, 1) * COALESCE(r2.LawyerFactor, 1) * COALESCE(r3.Firm, 1) * (1 + r3.SchdAdjPctCalc) * (1 + COALESCE(r3.YearsInsured,0)) * (1 + COALESCE(r2.ContLegalEdCredit,0))) + (COALESCE(r3.SECCoverage,0) + COALESCE(r2.DefReimbCoverage,0)),2)
--       ELSE ROUND((r3.Base * COALESCE(r.DeductibleFactor, 1) * COALESCE(r.LimitFactor, 1) * COALESCE(r2.LawyerFactor, 1) * COALESCE(r3.Firm, 1) * COALESCE(r3.SchdAdjPctCalc, 1) * (1 + COALESCE(r3.YearsInsured,0)) * (1 + COALESCE(r2.ContLegalEdCredit,0))) + (COALESCE(r3.SECCoverage,0) + COALESCE(r2.DefReimbCoverage,0)),2)
      THEN ROUND((r3.Base * COALESCE(r.DeductibleFactor, 1) * (COALESCE(r.LimitFactor, 1) + COALESCE(r.LimitAgg, 0)) * LEAST(COALESCE(r2.LawyerActivity, 1), COALESCE(r2.LawyerRDI, 1), COALESCE(r2.LawyerYIP, 1)) * COALESCE(r3.Firm, 1) * (1 + r3.SchdAdjPctCalc) * (1 + COALESCE(r3.YearsInsured,0)) * (1 + COALESCE(r2.ContLegalEdCredit,0))) + (COALESCE(r3.SECCoverage,0) + COALESCE(r2.DefReimbCoverage,0)),2)
      ELSE ROUND((r3.Base * COALESCE(r.DeductibleFactor, 1) * (COALESCE(r.LimitFactor, 1) + COALESCE(r.LimitAgg, 0)) * LEAST(COALESCE(r2.LawyerActivity, 1), COALESCE(r2.LawyerRDI, 1), COALESCE(r2.LawyerYIP, 1)) * COALESCE(r3.Firm, 1) * COALESCE(r3.SchdAdjPctCalc, 1) * (1 + COALESCE(r3.YearsInsured,0)) * (1 + COALESCE(r2.ContLegalEdCredit,0))) + (COALESCE(r3.SECCoverage,0) + COALESCE(r2.DefReimbCoverage,0)),2)
    END AS premiumPerLawyer1M,
 CASE
      WHEN ABS(r3.SchdAdjPctCalc) < 0.5 THEN ROUND((r3.Base * COALESCE(r.DeductibleFactor, 1) * (COALESCE(r.LimitFactor, 1) + COALESCE(r.LimitAgg, 0)) * LEAST(COALESCE(r2.LawyerActivityAdjusted, 1), COALESCE(r2.LawyerRDI, 1), COALESCE(r2.LawyerYIP, 1)) * COALESCE(r3.Firm, 1) * (1 + r3.SchdAdjPctCalc) * (1 + COALESCE(r3.YearsInsured,0)) * (1 + COALESCE(r2.ContLegalEdCredit,0))) + (COALESCE(r3.SECCoverage,0) + COALESCE(r2.DefReimbCoverage,0)),2)
      ELSE ROUND((r3.Base * COALESCE(r.DeductibleFactor, 1) * (COALESCE(r.LimitFactor, 1) + COALESCE(r.LimitAgg, 0)) * LEAST(COALESCE(r2.LawyerActivityAdjusted, 1), COALESCE(r2.LawyerRDI, 1), COALESCE(r2.LawyerYIP, 1)) * COALESCE(r3.Firm, 1) * COALESCE(r3.SchdAdjPctCalc, 1) * (1 + COALESCE(r3.YearsInsured,0)) * (1 + COALESCE(r2.ContLegalEdCredit,0))) + (COALESCE(r3.SECCoverage,0) + COALESCE(r2.DefReimbCoverage,0)),2)
    END AS premiumPerLawyer1MAdjusted, 
    CASE
      WHEN a.lawyerPrincipalCount = 0
      THEN ROUND(COALESCE(r.PremXS5mMinCalc,0)/1, 2)
      WHEN a.lawyerPrincipalCount IS NULL
      THEN ROUND(COALESCE(r.PremXS5mMinCalc, 0)/1, 2)
      ELSE ROUND(COALESCE(r.PremXS5mMinCalc,0) /(a.lawyerPrincipalCount + a.lawyerOfCounselCount), 2)
    END AS premiumPerLawyerXS5M,
    CASE
      WHEN a.lawyerPrincipalCount = 0
      THEN ROUND(COALESCE(r.PremXS10mCalc,0)/1, 2)
      WHEN a.lawyerPrincipalCount IS NULL
      THEN ROUND(COALESCE(r.PremXS10mCalc, 0)/1, 2)
      ELSE ROUND(COALESCE(r.PremXS10mCalc,0) /(a.lawyerPrincipalCount+ a.lawyerOfCounselCount), 2)
    END AS premiumPerLawyerXS10M,
    -- r.PremXS5mMinCalc/a.lawyerPrincipalCount AS premiumPerLawyerXS5M,
    -- r.PremXS10mCalc/a.lawyerPrincipalCount AS premiumPerLawyerXS10M,
    r.DeductibleFactor,
    r.LimitFactor,
    r.LimitAgg,
    r.ContinuityAmountCalc,
    r.PremXS5mMinCalc,
    r.PremXS10mCalc,
    r2.ContLegalEdCredit,
    r2.DefReimbCoverage,
    LEAST(COALESCE(r2.LawyerActivity, 1), COALESCE(r2.LawyerRDI, 1), COALESCE(r2.LawyerYIP, 1)) as LawyerFactor,
 LEAST(COALESCE(r2.LawyerActivityAdjusted, 1), COALESCE(r2.LawyerRDI, 1), COALESCE(r2.LawyerYIP, 1)) as LawyerFactorAdjusted,
 r2.LawyerYIP,
    r2.LawyerRDI,
    r2.LawyerActivity,
 r2.LawyerActivityAdjusted,
    r3.SECCoverage,
    r3.Base,
    r3.Firm,
    r3.SchdAdjPctCalc,
    r3.YearsInsured,
    l.retroactiveDate,
    l.lawyerId,
    l.inPracticeDuration,
    e.entityLocator AS lawyerEntityLocator,
    e.entityName    AS lawyerEntityName
  FROM Quote q
 INNER JOIN Application a ON a.applicationId = q.applicationId
  LEFT JOIN Firm f ON f.firmId = a.firmId
   LEFT JOIN Entity e2 ON e2.entityId = f.firmId
 INNER JOIN RatingLimitCrosstab r ON r.quoteBookId = q.mainQuoteBookId
--   LEFT JOIN Coverage c ON c.quoteBookId = r.quoteBookId  
  LEFT JOIN (select * from Coverage where coverageTypeId in ('Liability', 'Policy')) c ON c.quoteBookId = r.quoteBookId
           AND c.perClaimDeductible = r.Deductible
           AND c.perClaimLimit      = r."Limit"
--140402  LEFT JOIN ApplicationLimit a3 ON a3.applicationLimitId = r.quoteCoverageId
   left join QuoteCoverage qc on qc.quoteCoverageId = r.quoteCoverageId
 INNER JOIN RatingLawyerCrosstab r2 ON r2.quoteBookId = q.mainQuoteBookId
  INNER JOIN Lawyer l ON l.lawyerId = r2.lawyerId
   INNER JOIN Entity e ON e.entityId = l.lawyerId
 INNER JOIN RatingCrosstab r3 ON r3.quoteBookId = q.mainQuoteBookId
  ORDER BY a.effectiveDate, a.applicationId, r."Limit", r.Deductible;


--------------------------------------------------------------------------------------------------------------
/* NOT DONE YET. Waiting for Quote table */

CREATE OR REPLACE VIEW TailLawyerAllView (policyId, lawyerId, coverageId, effectiveDate, expirationDate, retroactiveDate, excludeBeforeDate, excludeAfterDate, cancelEffectiveDate, eventId, firmId, firmName, policyExpirationDate, perClaimLimit, aggregateLimit, perClaimDeductible, aggregateDeductible, eventTypeId, eventTypeName, policyNumber, applicationId, firmLocator, applicationRatingId, applicationLimitId, firmPremium, premiumPerLawyer1M, premiumPerLawyerXS5M, premiumPerLawyerXS10M, DeductibleFactor, LimitFactor, LawyerFactor, LawyerYIP, LawyerRDI, LawyerActivity, SchdAdjPctCalc, lawyerName, lawyerActivityId, premiumPerLawyer5MNoDeductible, LawyerFactorUsed, tailTerm, quoteId, quoteCoverageId, tailType, oldTailPremium, newTailPremium) AS 
  SELECT 
 p.policyId, 
 p.lawyerId, 
 p.coverageId, 
 p.effectiveDate, 
 COALESCE(p.expirationDate, TO_DATE('31-DEC-9999', 'DD-MON-YYYY')) as expirationDate,
 p.retroactiveDate, 
 p.excludeBeforeDate, 
 p.excludeAfterDate, 
 p.cancelEffectiveDate, 
 p.eventId, 
 p1.firmId, 
 p1.firmName, 
 p1.expirationDate AS policyExpirationDate, 
 p1.perClaimLimit, 
 p1.aggregateLimit, 
 p1.perClaimDeductible, 
 p1.aggregateDeductible, 
 e.eventTypeId, 
 et.eventTypeName, 
 p1.policyNumber, 
 pplv.applicationId,
 pplv.firmLocator,
 pplv.applicationRatingId,
 pplv.applicationLimitId,
 pplv.firmPremium,
 pplv.premiumPerLawyer1M,
 pplv.premiumPerLawyerXS5M,
 pplv.premiumPerLawyerXS10M,
 pplv.DeductibleFactor,
 pplv.LimitFactor,
 pplv.LawyerFactor,
 COALESCE(pplv.LawyerYIP, 1) as LawyerYIP,
 COALESCE(pplv.LawyerRDI, 1) as LawyerRDI,
 COALESCE(pplv.LawyerActivity, 1) as LawyerActivity,
 pplv.SchdAdjPctCalc,
 pplv.lawyerName,
 CASE 
   WHEN COALESCE(pplv.LawyerActivity, 1) <> 1 THEN 'PT'
   ELSE 'FT'
 END AS lawyerActivityId,
 ROUND((pplv.premiumPerLawyer1M/pplv.DeductibleFactor),2) as premiumPerLawyer5MNoDeductible,
 CASE
    WHEN COALESCE(pplv.LawyerActivity,1) < COALESCE(pplv.LawyerYIP,1) and COALESCE(pplv.LawyerActivity, 1) < COALESCE(pplv.LawyerRDI, 1) THEN 'LawyerActivity'
    WHEN COALESCE(pplv.LawyerYIP, 1) < COALESCE(pplv.LawyerRDI, 1) THEN 'LawyerYIP'
    ELSE 'LawyerRDI'
  END AS LawyerFactorUsed,
 CASE
   WHEN p.expirationDate is NULL THEN 'Unlimited'
   --WHEN e.eventTypeId = 'PLC-NU' THEN 'Unlimited'
   ELSE CAST((trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) AS VARCHAR)
 END AS tailTerm,
 q.quoteId,
 qc.quoteCoverageId,
 CASE
   WHEN e2.endorsementDesc is not NULL THEN 'Firm'
   ELSE 'Lawyer'
 END AS tailType,
 CASE
    -- Firm Tail
    WHEN e2.endorsementDesc is not NULL THEN 
        CASE 
            WHEN (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 1 THEN ROUND((((pplv.premiumPerLawyer1M/pplv.DeductibleFactor) * 1.03) * 0.90), 2)
            WHEN (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 2 THEN ROUND((((pplv.premiumPerLawyer1M/pplv.DeductibleFactor) * 1.03) * 1.00), 2)
            WHEN (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 3 THEN ROUND((((pplv.premiumPerLawyer1M/pplv.DeductibleFactor) * 1.03) * 1.85), 2)
            WHEN (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 6 THEN ROUND((((pplv.premiumPerLawyer1M/pplv.DeductibleFactor) * 1.03) * 2.00), 2)
            ELSE  ROUND((((pplv.premiumPerLawyer1M/pplv.DeductibleFactor) * 1.03) * 2.25), 2)
         END
    ELSE 
    -- Lawyer Tail
        CASE
            -- continuing to practice tails
            WHEN e.eventTypeId = 'PLC-CP' THEN
                CASE
                    WHEN pplv.LawyerActivity < 0.26 and (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 3 THEN ROUND((((pplv.premiumPerLawyer1M/pplv.DeductibleFactor) * 1.03) * 1.85), 2)
                    WHEN pplv.LawyerActivity < 0.26 and (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 6 THEN ROUND((((pplv.premiumPerLawyer1M/pplv.DeductibleFactor) * 1.03) * 2.00), 2) 
                    WHEN pplv.LawyerActivity < 0.51 and (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 3 THEN ROUND((((pplv.premiumPerLawyer1M/pplv.DeductibleFactor) * 1.03) * 1.65), 2)
                    WHEN pplv.LawyerActivity < 0.51 and (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 6 THEN ROUND((((pplv.premiumPerLawyer1M/pplv.DeductibleFactor) * 1.03) * 1.80), 2)
                    WHEN (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 3 THEN ROUND((((pplv.premiumPerLawyer1M/pplv.DeductibleFactor) * 1.03) * 1.45), 2)
                    WHEN (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 6 THEN ROUND((((pplv.premiumPerLawyer1M/pplv.DeductibleFactor) * 1.03) * 1.65), 2)
                END
            WHEN e.eventTypeId in ('PLC-NU', 'PLC-NP') THEN
                CASE
                    WHEN pplv.LawyerActivity < 0.26 THEN
                        CASE
                            WHEN (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 1 THEN ROUND((((pplv.premiumPerLawyer1M/pplv.DeductibleFactor) * 1.03) * 0.90), 2)
                            WHEN (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 2 THEN ROUND((((pplv.premiumPerLawyer1M/pplv.DeductibleFactor) * 1.03) * 1.00), 2)
                            WHEN (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 3 THEN ROUND((((pplv.premiumPerLawyer1M/pplv.DeductibleFactor) * 1.03) * 1.85), 2)
                            WHEN (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 6 THEN ROUND((((pplv.premiumPerLawyer1M/pplv.DeductibleFactor) * 1.03) * 2.00), 2)
                            WHEN p.expirationDate is NULL THEN ROUND((((pplv.premiumPerLawyer1M/pplv.DeductibleFactor) * 1.03) * 2.25), 2)
                        END
                    WHEN pplv.LawyerActivity < 0.51 THEN
                        CASE
                            WHEN (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 1 THEN ROUND((((pplv.premiumPerLawyer1M/pplv.DeductibleFactor) * 1.03) * 0.80), 2)
                            WHEN (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 2 THEN ROUND((((pplv.premiumPerLawyer1M/pplv.DeductibleFactor) * 1.03) * 1.05), 2)
                            WHEN (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 3 THEN ROUND((((pplv.premiumPerLawyer1M/pplv.DeductibleFactor) * 1.03) * 1.65), 2)
                            WHEN (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 6 THEN ROUND((((pplv.premiumPerLawyer1M/pplv.DeductibleFactor) * 1.03) * 1.80), 2)
                            WHEN p.expirationDate is NULL THEN ROUND((((pplv.premiumPerLawyer1M/pplv.DeductibleFactor) * 1.03) * 2.05), 2)
                        END
                    --WHEN pplv.LawyerActivity >= 0.51 THEN
                    ELSE
                        CASE
                            WHEN (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 1 THEN ROUND((((pplv.premiumPerLawyer1M/pplv.DeductibleFactor) * 1.03) * 0.70), 2)
                            WHEN (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 2 THEN ROUND((((pplv.premiumPerLawyer1M/pplv.DeductibleFactor) * 1.03) * 1.15), 2)
                            WHEN (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 3 THEN ROUND((((pplv.premiumPerLawyer1M/pplv.DeductibleFactor) * 1.03) * 1.45), 2)
                            WHEN (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 6 THEN ROUND((((pplv.premiumPerLawyer1M/pplv.DeductibleFactor) * 1.03) * 1.65), 2)
                            WHEN p.expirationDate is NULL THEN ROUND((((pplv.premiumPerLawyer1M/pplv.DeductibleFactor) * 1.03) * 1.85), 2)
                        END   
                END
        END
 END as oldTailPremium,
 CASE
    -- Firm Tail
    WHEN e2.endorsementDesc is not NULL THEN 
        CASE 
            WHEN pplv.LawyerActivity < 0.50 and pplv.LawyerFactor = pplv.LawyerActivity THEN
                CASE
                    WHEN (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 1 THEN ROUND(((((pplv.premiumPerLawyer1M/pplv.LawyerFactor) * LEAST(.5, COALESCE(pplv.LawyerYIP,1), COALESCE(pplv.LawyerRDI, 1)))/pplv.DeductibleFactor)) * 0.90, 2)
                    WHEN (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 2 THEN ROUND(((((pplv.premiumPerLawyer1M/pplv.LawyerFactor) * LEAST(.5, COALESCE(pplv.LawyerYIP,1), COALESCE(pplv.LawyerRDI, 1)))/pplv.DeductibleFactor)) * 1.00, 2)
                    WHEN (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 3 THEN ROUND(((((pplv.premiumPerLawyer1M/pplv.LawyerFactor) * LEAST(.5, COALESCE(pplv.LawyerYIP,1), COALESCE(pplv.LawyerRDI, 1)))/pplv.DeductibleFactor)) * 1.85, 2)
                    WHEN (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 6 THEN ROUND(((((pplv.premiumPerLawyer1M/pplv.LawyerFactor) * LEAST(.5, COALESCE(pplv.LawyerYIP,1), COALESCE(pplv.LawyerRDI, 1)))/pplv.DeductibleFactor)) * 2.00, 2)
                    ELSE  ROUND(((((pplv.premiumPerLawyer1M/pplv.LawyerFactor) * LEAST(.5, COALESCE(pplv.LawyerYIP,1), COALESCE(pplv.LawyerRDI, 1)))/pplv.DeductibleFactor)) * 2.25, 2)
                END
        ELSE
                CASE
                    WHEN (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 1 THEN ROUND(((pplv.premiumPerLawyer1M/pplv.DeductibleFactor) * 0.90), 2)
                    WHEN (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 2 THEN ROUND(((pplv.premiumPerLawyer1M/pplv.DeductibleFactor) * 1.00), 2)
                    WHEN (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 3 THEN ROUND(((pplv.premiumPerLawyer1M/pplv.DeductibleFactor) * 1.85), 2)
                    WHEN (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 1 THEN ROUND(((pplv.premiumPerLawyer1M/pplv.DeductibleFactor) * 2.00), 2)
                    ELSE ROUND(((pplv.premiumPerLawyer1M/pplv.DeductibleFactor) * 2.25), 2)
                END
        END
    ELSE
        --Lawyer Tail
        CASE 
            WHEN pplv.LawyerActivity < 0.50  and pplv.LawyerFactor = pplv.LawyerActivity THEN
                CASE
                    -- Lawyer continuing to practice tail
                    WHEN e.eventTypeId = 'PLC-CP' and (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 3 THEN ROUND(((((pplv.premiumPerLawyer1M/pplv.LawyerFactor) * LEAST(.5, COALESCE(pplv.LawyerYIP,1), COALESCE(pplv.LawyerRDI, 1)))/pplv.DeductibleFactor)) * 1.85, 2)
                    WHEN e.eventTypeId = 'PLC-CP' and (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 6 THEN ROUND(((((pplv.premiumPerLawyer1M/pplv.LawyerFactor) * LEAST(.5, COALESCE(pplv.LawyerYIP,1), COALESCE(pplv.LawyerRDI, 1)))/pplv.DeductibleFactor)) * 2.00, 2)
                    -- Lawyer non-practicing tail
                    WHEN e.eventTypeId in ('PLC-NP', 'PLC-NU') and (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 1 THEN ROUND(((((pplv.premiumPerLawyer1M/pplv.LawyerFactor) * LEAST(.5, COALESCE(pplv.LawyerYIP,1), COALESCE(pplv.LawyerRDI, 1)))/pplv.DeductibleFactor)) * 0.90, 2)
                    WHEN e.eventTypeId in ('PLC-NP', 'PLC-NU') and (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 2 THEN ROUND(((((pplv.premiumPerLawyer1M/pplv.LawyerFactor) * LEAST(.5, COALESCE(pplv.LawyerYIP,1), COALESCE(pplv.LawyerRDI, 1)))/pplv.DeductibleFactor)) * 1.00, 2)
                    WHEN e.eventTypeId in ('PLC-NP', 'PLC-NU') and (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 3 THEN ROUND(((((pplv.premiumPerLawyer1M/pplv.LawyerFactor) * LEAST(.5, COALESCE(pplv.LawyerYIP,1), COALESCE(pplv.LawyerRDI, 1)))/pplv.DeductibleFactor)) * 1.85, 2)
                    WHEN e.eventTypeId in ('PLC-NP', 'PLC-NU') and (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 6 THEN ROUND(((((pplv.premiumPerLawyer1M/pplv.LawyerFactor) * LEAST(.5, COALESCE(pplv.LawyerYIP,1), COALESCE(pplv.LawyerRDI, 1)))/pplv.DeductibleFactor)) * 2.00, 2)
                    WHEN e.eventTypeId in ('PLC-NP', 'PLC-NU') and p.expirationDate is NULL THEN ROUND(((((pplv.premiumPerLawyer1M/pplv.LawyerFactor) * LEAST(.5, COALESCE(pplv.LawyerYIP,1), COALESCE(pplv.LawyerRDI, 1)))/pplv.DeductibleFactor)) * 2.25, 2)
                END
            ELSE
                CASE
                    -- Lawyer continuing to practice tail
                    WHEN e.eventTypeId = 'PLC-CP' and (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 3 THEN ROUND(((pplv.premiumPerLawyer1M/pplv.DeductibleFactor) * 1.85), 2)
                    WHEN e.eventTypeId = 'PLC-CP' and (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 6 THEN ROUND(((pplv.premiumPerLawyer1M/pplv.DeductibleFactor) * 2.00), 2)
                    -- Lawyer non-practicing tail
                    WHEN e.eventTypeId in ('PLC-NP', 'PLC-NU') and (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 1 THEN ROUND(((pplv.premiumPerLawyer1M/pplv.DeductibleFactor) * 0.90), 2)
                    WHEN e.eventTypeId in ('PLC-NP', 'PLC-NU') and (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 2 THEN ROUND(((pplv.premiumPerLawyer1M/pplv.DeductibleFactor) * 1.00), 2)
                    WHEN e.eventTypeId in ('PLC-NP', 'PLC-NU') and (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 3 THEN ROUND(((pplv.premiumPerLawyer1M/pplv.DeductibleFactor) * 1.85), 2)
                    WHEN e.eventTypeId in ('PLC-NP', 'PLC-NU') and (trunc(months_between(TO_DATE(CAST (p.expirationDate AS VARCHAR) , 'YYYYMMDD'), TO_DATE(CAST (p.effectiveDate AS VARCHAR) , 'YYYYMMDD'))/12)) = 6 THEN ROUND(((pplv.premiumPerLawyer1M/pplv.DeductibleFactor) * 2.00), 2)
                    WHEN e.eventTypeId in ('PLC-NP', 'PLC-NU') and p.expirationDate is NULL THEN ROUND(((pplv.premiumPerLawyer1M/pplv.DeductibleFactor) * 2.25), 2)
                END
        END  
 END as newTailPremium
FROM PolicyLawyerCoverage p
  LEFT JOIN Policy p1 ON p1.policyId = p.policyId
    LEFT JOIN (select policyId, endorsementDesc from Endorsement where SUBSTR(endorsementDesc, 7, 2) = '16') e2 on e2.policyId = p.policyId
  LEFT JOIN Lawyer l ON l.lawyerId = p.lawyerId
  LEFT JOIN Event e ON e.eventId = p.eventId
    LEFT JOIN EventType et ON et.eventTypeId = e.eventTypeId
    LEFT JOIN Quote q on q.applicationId = p.policyId
    LEFT JOIN QuoteCoverage qc on qc.quoteId = q.quoteId and qc.quoteBookId = q.mainQuoteBookId and qc.perClaimLimit = p1.perClaimLimit and qc.aggregateLimit = p1.aggregateLimit and qc.perClaimDeductible = p1.perClaimDeductible
      LEFT JOIN PremiumPerLawyerView pplv on pplv.lawyerId = p.lawyerId and pplv.applicationLimitId = qc.quoteCoverageId  
where e.eventTypeId in ('PLC-NU', 'PLC-NP', 'PLC-CP')
  and p.effectiveDate > '31-DEC-2005'
order by p.lawyerId;
 



--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
/******************
**  POLICY
******************/
/*
    ISI_PolicyView
    p_policy

    History:
    03/07/2018  FZ  fix endorsements on policies with cancelEffectiveDate
    03/23/2018  FZ  added Firm tail information
    04/04/2018  FZ  added CAST for NULL values
    
*/
create or replace view ISI_PolicyView as
    select
        /* -- DEBUG AREA
        p.policyNumber as POLICYINT,
        p.cancelEffectiveDate as CANCEL_DATE,
        */
        p.policyId as POLICYID,
        e.eventTypeId as EVENTTYPEID,
        icrv.conversionreference,
        -- 01/05/2018 transaction redone to allow for cancellations
        CASE
            WHEN e.eventTypeId in ('Cvg-CP', 'Cvg-EC', 'Cvg-ED', 'Cvg-EU', 'Cvg-NP', 'Cvg-ND', 'Cvg-NU') THEN 'ERP'  -- tail endorsement   
            WHEN e.eventTypeId = 'Cvg-CN' THEN --cancellation
                CASE
                    WHEN p.cancelEffectiveDate IS NOT NULL and p.cancelEffectiveDate = p.effectiveDate THEN  --flat cancelled policy --05/30/2018
                        CASE
                            WHEN p.cancelEffectiveDate = lead(p.effectiveDate) over (order by p.firmId, p.effectiveDate, p.policyNumber, e.endorsementNumber) and p.firmId = lead(p.firmId) over (order by p.firmId, p.effectiveDate, p.policyNumber, e.endorsementNumber) THEN 'RCA'  --cancel for purposes of rewrite
                            --WHEN p.cancelEffectiveDate = lead(p.effectiveDate) over (order by p.firmId, p.effectiveDate, p.policyNumber, e.transactionseqno) and p.firmId = lead(p.firmId) over (order by p.firmId, p.effectiveDate, p.policyNumber, e.transactionseqno) THEN 'RCA'  --cancel for purposes of rewrite
                            ELSE 'CAN'  --just cancelled flat without a reissue -- at least not effective the previous term's effective date
                        END
                    ELSE 'CAN'
                END 
            WHEN e.eventTypeId = 'PolicyIssue' and p.effectiveDate = lag(p.cancelEffectiveDate) over (order by p.firmId, p.effectiveDate, p.policyNumber,e.endorsementNumber) and lag(p.cancelEffectiveDate) over (order by p.firmId, p.effectiveDate, p.policyNumber,e.endorsementNumber) <> lag(p.effectiveDate) over (order by p.firmId, p.effectiveDate, p.policyNumber,e.endorsementNumber)THEN 'REI' -- cancel/rewrite - no gap
            WHEN e.eventTypeId = 'PolicyIssue' and p.newReissue = 'R' and p.effectiveDate = lag(p.cancelEffectiveDate) over (order by p.firmId, p.effectiveDate, p.policyNumber,e.endorsementNumber) THEN 'RWR' -- flat cancel/rewrite  
            WHEN e.eventTypeId = 'PolicyIssue' and p.newReissue = 'N' and p.effectiveDate = lag(p.cancelEffectiveDate) over (order by p.firmId, p.effectiveDate, p.policyNumber,e.endorsementNumber) THEN 'RWN' -- flat cancel/rewrite
            --WHEN e.eventTypeId = 'PolicyIssue' and p.newReissue = 'R' and p.effectiveDate = lag(p.cancelEffectiveDate) over (order by p.firmId, p.effectiveDate, p.policyNumber,e.transactionseqno) THEN 'RWR' -- cancel/rewrite  
            --WHEN e.eventTypeId = 'PolicyIssue' and p.newReissue = 'N' and p.effectiveDate = lag(p.cancelEffectiveDate) over (order by p.firmId, p.effectiveDate, p.policyNumber,e.transactionseqno) THEN 'RWN' -- cancel/rewrite
            WHEN e.eventTypeId = 'PolicyIssue' and p.newReissue = 'N' THEN 'NEW'
            WHEN e.eventTypeId = 'PolicyIssue' and p.newReissue = 'R' and (fx.ISIFirmEarliestDate = fpv.effectiveDate) THEN 'NEW'  --ADDED: 01/30/2018 to fix #4
            WHEN e.eventTypeId = 'PolicyIssue' and p.newReissue = 'R' THEN 'REN'
            ELSE 'END'
        END as transactiontype,
        TO_CHAR(e.endorsementEffectiveDate, 'YYYYMMDD') as transactioneffectivedate,
        --TO_CHAR(p.effectiveDate, 'YYYYMMDD') as transactioneffectivedate,
        --0425 version....row_number() over (partition by p.firmId, e.endorsementEffectiveDate order by p.policyId, e.endorsementEffectiveDate, e.endorsementNumber) as transactionseqno,
        e.transactionseqno as transactionseqno,
        -- REVISE once s_predompolregion_mapping is updated with historical rate levels
        CASE
            WHEN p.effectiveDate < '01-JAN-2010' THEN 'LPL_19000101_20991231_4112'  --'LPL_19000101_20100101_4112'
            WHEN p.effectiveDate < '01-JAN-2016' THEN 'LPL_19000101_20991231_4112'  --'LPL_20100101_20160101_4112'
            WHEN p.effectiveDate < '01-OCT-2017' THEN 'LPL_19000101_20991231_4112'  --'LPL_20160101_20171001_4112'
            WHEN p.effectiveDate < '01-JAN-2018' THEN 'LPL_19000101_20991231_4112'  --'LPL_20171001_20180101_4112'
            ELSE 'LPL_19000101_20991231_4112'  --'LPL_20180101_99991231_4112'
        END as pcmmappingkey, -- LPL_<rate effective date>_<rate expiration date>_4112
        4112 as sbuid,
        TO_CHAR(fx.ISIFirmEarliestDate, 'YYYYMMDD') as inceptiondate,
        TO_CHAR(fpv.effectiveDate, 'YYYYMMDD') as policyeffectivedate,
        TO_CHAR(fpv.expirationDate, 'YYYYMMDD') as policyexpirydate,
        
        CASE
            WHEN e.endorsementEffectiveDate < p.effectiveDate or e.endorsementEffectiveDate > p.expirationDate THEN TO_CHAR(p.effectiveDate, 'YYYYMMDD') -- fixes endorsements effective > policy expiration date (ie: 0703097)
            ELSE TO_CHAR(e.endorsementEffectiveDate, 'YYYYMMDD') 
        END as veffectivedate,
        CASE
            WHEN e.endorsementEffectiveDate < p.effectiveDate or e.endorsementEffectiveDate > p.expirationDate THEN TO_CHAR(p.effectiveDate, 'YYYYMMDD')  -- fixes endorsements effective > policy expiration date (ie: 0703097)
            WHEN p.cancelEffectiveDate is not NULL THEN TO_CHAR(e.endorsementEffectiveDate, 'YYYYMMDD')
            WHEN e.eventTypeId = 'Cvg-CN' and p.cancelEffectiveDate is NULL THEN TO_CHAR(e.endorsementEffectiveDate, 'YYYYMMDD')  --fixes c17 issue with LAW cancels w/out cancelEffectiveDate
            ELSE TO_CHAR(p.expirationDate, 'YYYYMMDD')
            ---ELSE TO_CHAR(LEAD(e.endorsementEffectiveDate, 1, p.expirationDate) over (partition by e.policyId order by e.policyId, e.endorsementEffectiveDate), 'YYYYMMDD') --gets next endorsement effective date
        END as vexpirydate, 
        '000100' as veffectivetime,
        '000100' as vexpirytime,
        e.policyNumber as policykey,  --was fpv.policyNumber
        CAST(NULL as VARCHAR(40)) as previouspolicykey,
        e.coverageId as alternatepolicykey,
        CAST(NULL as VARCHAR(40)) as agentbrokerpolicykey,
        CAST(NULL as VARCHAR(40)) as binderkey,
        TO_CHAR(p.expirationDate, 'YYYYMMDD') as policynextunderwritedate,
        p.firmId as cliententitymappingkey,  
        'A' as renewaltype,
        months_between(p.expirationDate, p.effectiveDate) as termlengthind,
        'M' as termlengthindunit,
        'D' as billingtype,
        0 as papind,  --should be 1 if payamentMethod=MP ???
        CASE
            WHEN p.paymentMethod = 'QP' THEN '4 Pay 25 Down'
            WHEN p.paymentMethod = 'MP' THEN '10 Pay'
            ELSE 'Annual'
        END as paymentplan,
        19000101 as planeffdate,
        20991231 as planexpdate,
        'N' as subscriptionind,
        CAST(NULL as INT) as facultativeind,
        'USD' as premiumcurrencycd,
        'USD' as limitcurrencycd,
        CAST(NULL as VARCHAR(3)) as sbuofficekey,
        'jmccarthy' as underwriterentitymappingkey,
        0 as priorperiodind,
        CASE
            WHEN e.eventTypeId = 'Cvg-CN' THEN 0
            ELSE NULL
        END as cancellationtype,
        CAST(NULL as INT) as canreturnpremium,
        CAST(NULL as INT) as canearnedpremium,
        CAST(NULL as INT) as reinstateexpirydate,
        CAST(NULL as VARCHAR(40)) as conversionserver,
        CAST(NULL as INT) as sequencenumber,
        -- only required if transactiontype=RCA
        /*
        CASE
            WHEN e.eventTypeId = 'Cvg-CN' and p.cancelEffectiveDate = p.effectiveDate THEN  --flat cancelled policy  --05/30/2018
                CASE 
                    WHEN p.cancelEffectiveDate = lead(p.effectiveDate) over (order by p.firmId, p.effectiveDate, p.policyNumber, e.endorsementNumber) and p.firmId = lead(p.firmId) over (order by p.firmId, p.effectiveDate, p.policyNumber, e.endorsementNumber) and lead(newReissue) over (order by p.firmId, p.effectiveDate, p.policyNumber, e.endorsementNumber) = 'N' THEN 'RWN' 
                    WHEN p.cancelEffectiveDate = lead(p.effectiveDate) over (order by p.firmId, p.effectiveDate, p.policyNumber, e.endorsementNumber) and p.firmId = lead(p.firmId) over (order by p.firmId, p.effectiveDate, p.policyNumber, e.endorsementNumber) and lead(newReissue) over (order by p.firmId, p.effectiveDate, p.policyNumber, e.endorsementNumber) = 'R' THEN 'RWR'
                    ELSE NULL
                END
        END as relatedrevrcatranstype,
        
        CASE
            WHEN e.eventTypeId = 'Cvg-CN' and p.cancelEffectiveDate = p.effectiveDate and p.cancelEffectiveDate = lead(p.effectiveDate) over (order by p.firmId, p.effectiveDate, p.policyNumber, e.endorsementNumber) and p.firmId = lead(p.firmId) over (order by p.firmId, p.effectiveDate, p.policyNumber, e.endorsementNumber) THEN TO_CHAR(LEAD(e.endorsementEffectiveDate) over (order by p.firmId, p.effectiveDate, p.policyNumber, e.endorsementNumber), 'YYYYMMDD') --flat cancelled policy
            --05/30/2018 WHEN e.eventTypeId = 'Cvg-CN' and p.cancelEffectiveDate is not NULL and p.cancelEffectiveDate = lead(p.effectiveDate) over (order by p.firmId, p.effectiveDate, p.policyNumber, e.endorsementNumber) and p.firmId = lead(p.firmId) over (order by p.firmId, p.effectiveDate, p.policyNumber, e.endorsementNumber) THEN TO_CHAR(LEAD(e.endorsementEffectiveDate) over (order by p.firmId, p.effectiveDate, p.policyNumber, e.endorsementNumber), 'YYYYMMDD') --midterm cancel/rewrite
            ELSE NULL
        END as relatedrevrcatranseffdate,
        */
        /*
        CASE
            WHEN e.eventTypeId = 'Cvg-CN' and p.cancelEffectiveDate is not NULL and p.cancelEffectiveDate = p.effectiveDate and p.cancelEffectiveDate = lead(p.effectiveDate) over (order by p.firmId, p.effectiveDate, p.policyNumber, e.endorsementNumber) and p.firmId = lead(p.firmId) over (order by p.firmId, p.effectiveDate, p.policyNumber, e.endorsementNumber) THEN row_number() over (partition by p.firmId, e.effectiveDate order by p.policyId, e.endorsementNumber)+1
            WHEN e.eventTypeId = 'Cvg-CN' and p.cancelEffectiveDate is not NULL and p.cancelEffectiveDate = lead(p.effectiveDate) over (order by p.firmId, p.effectiveDate, p.policyNumber, e.endorsementNumber) and p.firmId = lead(p.firmId) over (order by p.firmId, p.effectiveDate, p.policyNumber, e.endorsementNumber) THEN row_number() over (partition by p.firmId, e.effectiveDate order by p.policyId, e.endorsementNumber)+1
            ELSE CAST(NULL as INT) 
        END as relatedrevrcatranseqno, 
        */
        CASE
            WHEN e.eventTypeId = 'Cvg-CN' and p.cancelEffectiveDate = p.effectiveDate THEN  --flat cancelled policy --05/30/2018
                CASE 
                    --WHEN p.cancelEffectiveDate = lead(p.effectiveDate) over (order by p.firmId, p.effectiveDate, p.policyNumber, e.endorsementNumber) and p.firmId = lead(p.firmId) over (order by p.firmId, p.effectiveDate, p.policyNumber, e.endorsementNumber) THEN row_number() over (partition by p.firmId, e.endorsementEffectiveDate order by p.policyId, e.endorsementEffectiveDate, e.endorsementNumber) + 1 
                    WHEN p.cancelEffectiveDate = lead(p.effectiveDate) over (order by p.firmId, p.effectiveDate, p.policyNumber, e.endorsementNumber) and p.firmId = lead(p.firmId) over (order by p.firmId, p.effectiveDate, p.policyNumber, e.endorsementNumber) THEN lead(e.transactionseqno) over (order by p.firmId, p.effectiveDate, p.policyNumber, e.endorsementNumber) 
                    --WHEN p.cancelEffectiveDate = lead(p.effectiveDate) over (order by p.firmId, p.effectiveDate, p.policyNumber, e.endorsementNumber) and p.firmId = lead(p.firmId) over (order by p.firmId, p.effectiveDate, p.policyNumber, e.endorsementNumber) and lead(newReissue) over (order by p.firmId, p.effectiveDate, p.policyNumber, e.endorsementNumber) = 'R' THEN row_number() over (partition by p.firmId, e.endorsementEffectiveDate order by p.policyId, e.endorsementEffectiveDate, e.endorsementNumber) + 1
                    ELSE NULL
                END
        END as relatedrevrcatranseqno,
        1 as claimsmadeind,
        TO_CHAR(x.ISIPolicyRetroDate, 'YYYYMMDD') as retrodate,
        TO_CHAR(tlv.effectiveDate, 'YYYYMMDD') as extendreporteffdate,  --firm tail effective date
        TO_CHAR(tlv.expirationDate, 'YYYYMMDD') as extendreportexpdate,  --firm tail expiration date, unlimited 29991231      
        CASE
            WHEN e.eventTypeId in ('Cvg-CP', 'Cvg-EC', 'Cvg-ED', 'Cvg-EU', 'Cvg-NP', 'Cvg-ND', 'Cvg-NU') THEN 702  -- ERP  tail endorsement   
            WHEN e.eventTypeId = 'Cvg-CN' THEN --cancellation
                CASE
                    WHEN p.cancelEffectiveDate IS NOT NULL and p.cancelEffectiveDate = p.effectiveDate THEN  --flat cancelled policy--05/30/2018
                        CASE
                            WHEN p.cancelEffectiveDate = lead(p.effectiveDate) over (order by p.firmId, p.effectiveDate, p.policyNumber, e.endorsementNumber) and p.firmId = lead(p.firmId) over (order by p.firmId, p.effectiveDate, p.policyNumber, e.endorsementNumber) THEN 999 --'RCA'  --cancel for purposes of rewrite
                            ELSE 699 --'CAN' just cancelled flat without a reissue -- at least not effective the previous term's effective date
                        END
                    ELSE 699 --CAN
                END  
            WHEN e.eventTypeId = 'PolicyIssue' and p.effectiveDate = lag(p.cancelEffectiveDate) over (order by p.firmId, p.effectiveDate, p.policyNumber,e.endorsementNumber) and lag(p.cancelEffectiveDate) over (order by p.firmId, p.effectiveDate, p.policyNumber,e.endorsementNumber) <> lag(p.effectiveDate) over (order by p.firmId, p.effectiveDate, p.policyNumber,e.endorsementNumber) THEN 1501 --REI -- cancel/rewrite - no gap    
            WHEN e.eventTypeId = 'PolicyIssue' and p.newReissue = 'R' and p.effectiveDate = lag(p.cancelEffectiveDate) over (order by p.firmId, p.effectiveDate, p.policyNumber,e.endorsementNumber) THEN 1099 --'RWR'  cancel/rewrite  
            WHEN e.eventTypeId = 'PolicyIssue' and p.newReissue = 'N' and p.effectiveDate = lag(p.cancelEffectiveDate) over (order by p.firmId, p.effectiveDate, p.policyNumber,e.endorsementNumber) THEN 1099 --'RWN'  cancel/rewrite
            WHEN e.eventTypeId = 'PolicyIssue' and p.newReissue = 'N' THEN 200 --'NEW' new business
            WHEN e.eventTypeId = 'PolicyIssue' and p.newReissue = 'R' and (fx.ISIFirmEarliestDate = fpv.effectiveDate) THEN 200 --'NEW' new business --ADDED: 01/30/2018 to fix #4
            WHEN e.eventTypeId = 'PolicyIssue' and p.newReissue = 'R' THEN 500 --'REN' reissue
            ELSE 300 --'END' endorsement (non tail)
        END as vreasoncode,
        TO_CHAR(ev.eventDate, 'YYYYMMDDHH24MISS') as ventrydatetime,
        CASE
            WHEN ev.createUserId in ('Jane', 'JaneH') THEN 'jharder'
            ELSE ev.createUserId 
        END as vusermappingkey,
        TO_CHAR(COALESCE(ev.dueDate,ev.eventDate), 'YYYYMM') as postedyearmth,  --This does not always match accounting's fiscalYear, fiscalPeriod due to multiple trans mapping to coverage spanning multiple periods (ie: BRUCLA, CG11878593)
        'D' as policysourceind,
        CAST(NULL as VARCHAR(40)) as assumedcompanymappingkey,
        CAST(NULL as INT) as writingcompanymappingkey,
        CAST(NULL as VARCHAR(40)) as freetradezoneriskcode,
        'EN' as languagecd,
        CAST(NULL as VARCHAR(40)) as overridereasoncd,
        CAST(NULL as VARCHAR(40)) as overridereasonnote,
        CAST(NULL as INT) as postedwweobjectid,
        CAST(NULL as INT) as postedwwevnumber,
        CAST(NULL as VARCHAR(40)) as transactionstatus,
        CAST(NULL as INT) as nextfollowupdate,
        0 as finaladjustmentind,
        'LPL_Ratebook' as rateeditionname,
        1 as rateeditionversion,
        CAST(NULL as INT) as wipstatus, -- probably use event folder
        CAST(NULL as VARCHAR(40)) as quoterelatedtranstype,
        CAST(NULL as VARCHAR(3)) as relatedtranstype,
        CAST(NULL as VARCHAR(8)) as relatedtranseffdate,
        CAST(NULL as INT) as relatedtransseqno,
        CAST(NULL as VARCHAR(40)) as quotekey,
        CAST(NULL as VARCHAR(8)) as quotedate,
        CAST(NULL as INT) as quotedaystoaccept,
        CAST(NULL as VARCHAR(8)) as quoteexpirydate,
        CAST(NULL as VARCHAR(40)) as quotedefinedname,
        CAST(NULL as VARCHAR(40)) as quoteseriescode,
        CAST(NULL as VARCHAR(1)) as quotestatus,
        1 as quotedocumentsprinted,
        CAST(NULL as VARCHAR(40)) as brkwritingcompanymappingkey,
        CAST(NULL as INT) as brokeragepolicycommission,
        CAST(NULL as VARCHAR(1)) as quickquoteind,
        CAST(NULL as VARCHAR(1)) as referralstatus,
        CAST(NULL as VARCHAR(40)) as documentcategory,
        CAST(NULL as VARCHAR(40)) as frontedcompanymappingkey,
        0 as assumedcommissionamount,
        1 as assumedcommissiontype,
        CAST(NULL as VARCHAR(40)) as policycompositiontype,
        'WI' as predominantjurisdiction,
        'WI' as predominantpolicyregion,
        0 as totalwaivedpremium,
        --TO_CHAR(ev.eventDate, 'YYYYMMDD') as accountingdate, --Event.eventDate
        TO_CHAR(COALESCE(ev.dueDate, ev.completeDate), 'YYYYMMDD') as accountingdate,  --make this match postedyearmth????
        CAST(NULL as VARCHAR(3)) as quotedeclinedby,
        CAST(NULL as VARCHAR(40)) as quotecompetitor,
        0 as quotecompetitorpremium,
        /*
        CASE
            WHEN ct.personId IS NOT NULL THEN ct.personId
            ELSE ct.contactEntityId
        END as policycontactentitykey,
        0 as policycontactentitykeyvnumber,
        */
        CAST(NULL as VARCHAR(40)) as comments,  --helpful but not required
        CASE
            WHEN p.newReissue = 'N' THEN 'N'
            WHEN p.newReissue = 'R' THEN 'R'
        END as renewalstatus,
        CASE
            WHEN p.cancelEffectiveDate is NULL THEN 0
            ELSE 7
       END as insurancestatus,
       p.perClaimLimit as liabilitylimit,
       CAST(NULL as VARCHAR(40)) as overridemailingaddrkey,
       0 as auditreportingind,
       CAST(NULL as VARCHAR(8)) as convertedropolicytrneffdate,
       CAST(NULL as INT) as convertedtopolicytrnseqno,
       CAST(NULL as VARCHAR(20)) as quotecompletedreasoncode,
       CAST(NULL as VARCHAR(40)) as quotecompletedreasonnotes,
       CAST(NULL as VARCHAR(40)) as quotelastmodifieddatetime,
       CASE
            WHEN tlv.tailTerm is not NULL and tlv.tailTerm <> 'Unlimited' THEN tlv.tailTerm
            ELSE CAST(NULL as VARCHAR(20))
        END as extendreporttermlength,  --v132  if U then NULL
        CASE
            WHEN tlv.tailTerm is not NULL and tlv.tailTerm = 'Unlimited' THEN 'U'
            WHEN tlv.tailTerm is not NULL and tlv.tailTerm <> 'Unlimited' THEN 'Y'
            ELSE CAST(NULL as VARCHAR(1))
        END as extendreportlengthunit,  --v132  Y=years, M=months, U=unlimited
        CAST(NULL as VARCHAR(1)) as useextendreportexpoverrideind  --v132  default= NULL
from (select * from ISIEndorsementDetail2View) e  --where eventTypeId not in ('Cvg-NC', 'Cvg-EL', 'Cvg-CC', 'Cvg-RC', 'Cvg-AT', 'Cvg-EV', 'Cvg-MN', 'Cvg-RL')) e  
    left join ISIFirmPolicyView fpv on fpv.policyId = e.policyId
        left join Policy p on p.policyId = fpv.policyId
            --left join Application a on a.applicationId = p.policyId
            --left join (select * from Contact where contactTypeId = 'Firm') ct on ct.organizationId = p.firmId  -- may produce duplicates when there are multiple contactTypeId=Firm
            left join ISIConversionReferenceView icrv on icrv.firmId = p.firmId and (p.effectiveDate BETWEEN icrv.startDate and icrv.endDate and COALESCE(p.cancelEffectiveDate, p.expirationDate) BETWEEN icrv.startDate and icrv.endDate)
        left join Quote q2 on q2.applicationId = p.policyId
        left join ISIPolicyRetroDateView x on x.policyId = p.policyId
        left join ISIFirmDatesView fx on fx.firmId = p.firmId
    left join Coverage c on c.coverageId = e.coverageId
            left join Event ev on ev.eventId = c.eventId  
    left join (SELECT DISTINCT coverageId, effectiveDate, expirationDate, tailType, tailTerm from TailLawyerAllView) tlv on tlv.coverageId = e.coverageId
where p.effectiveDate > '31-DEC-2005'
-- BELOW ITEMS NEED FIXING
  and p.firmId not in ('SKOGLA', 'CASEL1')
  and p.policyNumber NOT in ('0607050', '0703097', '0807068', '0811071', '1003092', '1006065', '1007005', '1111122', '1112041', '1206065', '1308050', '1408002', '1505056', '1602014',  '1601090')
order by fpv.firmId, fpv.policyNumber, TO_CHAR(fpv.effectiveDate, 'YYYYMMDD'), e.transactionseqno
;



/*--- Table created for performance---*/
drop table IF EXISTS ISI_Policy CASCADE;
--delete * from ISI_Policy;
create table ISI_Policy as
    (select * from ISI_PolicyView);
select * from ISI_Policy;   
select count(*) from ISI_Policy;  


/*----- FIXES MUST BE RUN UNTIL FIXED -----*/
--select * from ISI_Policy where conversionreference = 'MARELT_2011_2018' ;
delete from ISI_Policy where conversionreference = 'MARELT_2011_2018' and transactiontype = 'REN' and alternatepolicykey = 'CG11483286';
--select * from ISI_Policy where conversionreference = 'PREVGO_1988_2018' and transactioneffectivedate = '20100601' ;
--update ISI_Policy set relatedrevrcatranseqno = 1 where conversionreference = 'PREVGO_1988_2018' and transactiontype = 'RCA' and transactioneffectivedate = '20100601' and relatedrevrcatranseqno = 6;
--select * from ISI_Policy where conversionreference = 'SANDKR_2010_2011' and transactioneffectivedate = '20101115' ;
--update ISI_Policy set relatedrevrcatranseqno = 1 where conversionreference = 'SANDKR_2010_2011' and transactiontype = 'RCA' and transactioneffectivedate = '20101115' and relatedrevrcatranseqno = 4;




-- ISIPolicyEndorsementView
-- transposes the LAW Endorsement table for select endorsements to accommadate
-- listing endorsements by policy
/* REWRITE THE Oracle DECODE to PostGreSQL */
create or replace view ISIPolicyEndorsementView as 
    select
        p.policyNumber as policyNumber,
    --    p.policyId as policyId,
    --    e.endorsementId as endorsementId,
        MAX(CASE WHEN SUBSTR(e.endorsementDesc,1,8) = 'WLMEND02' THEN 1 ELSE 0 END) as WLMEND02,
        MAX(CASE WHEN SUBSTR(e.endorsementDesc,1,8) = 'WLMEND03' THEN 1 ELSE 0 END) as WLMEND03,
        MAX(CASE WHEN SUBSTR(e.endorsementDesc,1,8) = 'WLMEND24' THEN 1 ELSE 0 END) as WLMEND24,
        MAX(CASE WHEN SUBSTR(e.endorsementDesc,1,8) = 'WLMEND25' THEN 1 ELSE 0 END) as WLMEND25,
        MAX(CASE WHEN SUBSTR(e.endorsementDesc,1,10) = 'Manuscript' THEN 1 ELSE 0 END) as Manuscript
    from Endorsement e 
        left join Policy p on p.policyId = e.policyId
    group by p.policyNumber 
    order by p.policyNumber
;



    
----
-- ISIEndorsementDetailView
--  created to provide endorsement detail since as of 04/23/2018 not necessary to combine
--  all endorsements on same effective date into 1 record since this causes a problem
--  
-- drop view ISIEndorsementDetailView;
create or replace view ISIEndorsementDetailView as 
select
    p.firmId,
    p.policyId,
    p.policyNumber,
    p.effectiveDate,
    p.expirationDate,
    p.cancelEffectiveDate,
    COALESCE(pa.premium,0) as coveragePremium,
    COALESCE(pa.premiumAdjust,0) as coveragePremiumAdjust,
    COALESCE(pa.financeCharge,0) as coverageFinanceCharge,
    COALESCE(pa.transfer,0) as coverageTransfer,
    COALESCE(pa.writeOff,0) as coverageWriteOff,
    COALESCE(pa.dueAmount,0) as coverageDueAmount,
    COALESCE(pa.receiveAmount,0) as coverageReceiveAmount,
    COALESCE(pa.returnAmount,0) as coverageReturnAmount,
    e.eventTypeId,
    CASE
        WHEN en.endorsementDesc IS NULL THEN 'Policy Issue'
        ELSE en.endorsementDesc
    END as endorsementDesc,
    c.coverageId,
    CASE
        WHEN en.effectiveDate is NULL THEN p.effectiveDate
        ELSE en.effectiveDate 
    END as endorsementEffectiveDate,
    (COALESCE(en.endorsementNumber, '0'))::INT + 1 as endorsementNumber   
from Coverage c
    left join Event e on e.eventId = c.eventId
    left join Policy p on p.policyId = c.policyId
    left join Endorsement en on en.endorsementId = c.coverageId
    left join (select coverageId, sum(premium) as premium, sum(premiumAdjust) as premiumAdjust, sum(financeCharge) as financeCharge, sum(transfer) as transfer, sum(writeOff) as writeOff, sum(dueAmount) as dueAmount, sum(receiveAmount) as receiveAmount, sum(returnAmount) as returnAmount from PolicyAccounting group by coverageId) pa on pa.coverageId = c.coverageId
where c.coverageId not in ('CG001148214') --04/27/2018--bogus fixes
  
--where p.policyId in ( 'AP11865481', 'AP11936260')
order by p.policyId, (COALESCE(en.endorsementNumber, '0'))::INT
;




--------------------------------------------------------------------------------
/*
    ISI_InsuredsView
    ISI table = p_insureds
    
    Dependancies: ISI_PolicyView

    History
    12/01/2017  FZ  Created
    03/31/2018  FZ  NOTE:: lengthy compile time so split into 2 views
    06/05/2018  LP  
*/
create or replace view ISI_InsuredsFirmView as
--  firm level required for each transaction sequence
    select
        p.policykey as POLICYINT,
        p.alternatepolicykey as COVERAGEID,
        p.policyeffectivedate as POLICYEFFECTIVEDATE,
        p.policyexpirydate as POLICYEXPIRYDATE,
        /* LP Firms items will always be from the start to the end of the term since the effective date doesn't change from the start of the term */
        p.policyeffectivedate as VEFFECTIVEDATE,
        p.policyexpirydate as VEXPIRYDATE,
        p.cliententitymappingkey as FIRMID,
        p.termlengthind as POLICYTERM,
        p.EVENTTYPEID as EVENTTYPEID,
        p.conversionreference as conversionreference,
        p.transactiontype as transactiontype,
        p.transactioneffectivedate as transactioneffectivedate,
        p.transactionseqno as transactionseqno,
        4112 as sbuid,
        1 as insuredid,
        2 as insuredrole,
        p.cliententitymappingkey as contactentitykey,
        0 as contactentityvnumber,
        e.entityId||'_'||L.locationId as defaultaddresskey,
        NULL as defaultcontactmethodkey,
        NULL as conversionserver,
        NULL as sequencenumber,
        NULL as postedwweobjectid /*,
        p.policykey||TO_INT(ORA_HASH(p.cliententitymappingkey)) as ouid */
    from ISI_Policy p    --previously ISI_PolicyView
        left join Entity e on e.entityId = p.cliententitymappingkey
            left join Location l on l.locationId = e.mainLocationId
    --where p.transactioneffectivedate > '20051231' ---
;
select * from ISI_Policy where EVENTTYPEID = 'Cvg-AA';






/* UPDATED BY LEE June 4, 2018 This view is for p_insureds only */
create or replace view ISI_InsuredsLawyerView as 
--  lawyer (insured) level
  WITH all_attorney_trans AS (
    SELECT DISTINCT
        p.policykey as POLICYINT,
        p.alternatepolicykey as COVERAGEID,
        p.policyeffectivedate as POLICYEFFECTIVEDATE,
        p.policyexpirydate as POLICYEXPIRYDATE,
        /* LP We need to cover periods of the attorneys */
        TO_CHAR(plc.effectiveDate,'YYYYMMDD')::NUMERIC AS VEFFECTIVEDATE,
        CASE WHEN p.transactioneffectivedate::NUMERIC < TO_CHAR(plc.expirationDate,'YYYYMMDD')::NUMERIC THEN p.policyexpirydate::NUMERIC ELSE TO_CHAR(plc.expirationDate,'YYYYMMDD')::NUMERIC END AS VEXPIRYDATE, 
        p.cliententitymappingkey as FIRMID,
        p.termlengthind as POLICYTERM,
        p.EVENTTYPEID as EVENTTYPEID,
        p.conversionreference as conversionreference,
        p.transactiontype as transactiontype,
        p.transactioneffectivedate as transactioneffectivedate,
        p.transactionseqno as transactionseqno,
        4112 as sbuid,
        plc.lawyerPolicySequence as insuredid,
        7 as insuredrole,
        plc.lawyerId as contactentitykey,
        0 as contactentityvnumber,
        NULL as defaultaddresskey,
        NULL as defaultcontactmethodkey,
        NULL as conversionserver,
        NULL as sequencenumber,
        NULL as postedwweobjectid /* ,
        p.policykey||(ORA_HASH(plc.lawyerId))::INT as ouid */
    --from ISI_Policy p
    --from (select * from ISI_Policy where EVENTTYPEID not in ('Cvg-SC', 'Cvg-EC', 'Cvg-ED', 'Cvg-EU', 'Cvg-CP', 'Cvg-ND', 'Cvg-NP', 'Cvg-NU', 'Cvg-DA', 'Cvg-AA', 'Cvg-PA')) p
    /* LP We want to select Cvg-DA and Cvg-AA endorsements */
    FROM (select * from ISI_Policy where EVENTTYPEID not in ('Cvg-SC', 'Cvg-EC', 'Cvg-ED', 'Cvg-EU', 'Cvg-CP', 'Cvg-ND', 'Cvg-NP', 'Cvg-NU', 'Cvg-PA') /* AND policykey = '0607190' */) p
       JOIN PolicyLawyerCoverageAllView plc on plc.policyId = p.POLICYID 
          AND (plc.transactioneffectivedate * 100) + plc.transactionseqno <= (p.transactioneffectivedate::BIGINT * 100) + p.transactionseqno
    WHERE p.transactioneffectivedate::BIGINT > 20051231    
   ), deleted_attorneys AS (
      SELECT DISTINCT
        p.policykey as POLICYINT,   
        p.conversionreference as conversionreference,
        p.transactiontype as transactiontype,
        p.transactioneffectivedate as transactioneffectivedate,
        p.transactionseqno as transactionseqno,
        4112 as sbuid,
        plc.lawyerPolicySequence as insuredid,
        plc.lawyerId as contactentitykey  
      from (select * from ISI_Policy where EVENTTYPEID IN ('Cvg-DA') /* AND policykey = '0607190' */) p
      JOIN PolicyLawyerCoverageAllView plc on plc.policyId = p.POLICYID 
        AND plc.end_coverageId = p.alternatepolicykey   
      WHERE p.transactioneffectivedate::BIGINT > 20051231
   )
   SELECT all_attorney_trans.*
   FROM all_attorney_trans
   WHERE NOT EXISTS (
      SELECT 1
      FROM deleted_attorneys 
      WHERE all_attorney_trans.POLICYINT = deleted_attorneys.POLICYINT
        AND all_attorney_trans.contactentitykey = deleted_attorneys.contactentitykey
        AND (all_attorney_trans.transactioneffectivedate::BIGINT * 100) + all_attorney_trans.transactionseqno >= (deleted_attorneys.transactioneffectivedate::BIGINT * 100) + deleted_attorneys.transactionseqno
   )
   ORDER BY POLICYINT, transactioneffectivedate, transactionseqno, insuredrole, contactentitykey
;


/* UPDATED BY LEE June 4, 2018 This view is for p_items only */
create or replace view ISI_ItemLawyerWithDeleteIndView as 
--  lawyer (item) level
  WITH all_attorney_trans AS (
    SELECT DISTINCT
        p.policykey as POLICYINT,
        p.alternatepolicykey as COVERAGEID,
        p.policyeffectivedate as POLICYEFFECTIVEDATE,
        p.policyexpirydate as POLICYEXPIRYDATE,
        /* LP We need to cover periods of the attorneys */
        TO_CHAR(plc.effectiveDate,'YYYYMMDD')::NUMERIC AS VEFFECTIVEDATE,
        CASE WHEN p.transactioneffectivedate::NUMERIC < TO_CHAR(plc.expirationDate,'YYYYMMDD')::NUMERIC THEN p.policyexpirydate::NUMERIC ELSE TO_CHAR(plc.expirationDate,'YYYYMMDD')::NUMERIC END AS VEXPIRYDATE, 
        p.cliententitymappingkey as FIRMID,
        p.termlengthind as POLICYTERM,
        p.EVENTTYPEID as EVENTTYPEID,
        p.conversionreference as conversionreference,
        p.transactiontype as transactiontype,
        p.transactioneffectivedate as transactioneffectivedate,
        p.transactionseqno as transactionseqno,
        4112 as sbuid,
        plc.lawyerPolicySequence as insuredid,        
        plc.lawyerId as contactentitykey /* ,
        p.policykey||(ORA_HASH(plc.lawyerId))::INT as ouid */
    FROM (select * from ISI_Policy where EVENTTYPEID not in ('Cvg-SC', 'Cvg-EC', 'Cvg-ED', 'Cvg-EU', 'Cvg-CP', 'Cvg-ND', 'Cvg-NP', 'Cvg-NU', 'Cvg-PA') /* AND policykey = '0607190' */) p
       JOIN PolicyLawyerCoverageAllView plc on plc.policyId = p.POLICYID 
          AND (plc.transactioneffectivedate * 100) + plc.transactionseqno <= (p.transactioneffectivedate::BIGINT * 100) + p.transactionseqno
    WHERE p.transactioneffectivedate::BIGINT > 20051231    
   ), deleted_attorneys AS (
      SELECT DISTINCT
        p.policykey as POLICYINT,   
        p.conversionreference as conversionreference,
        p.transactiontype as transactiontype,
        p.transactioneffectivedate as transactioneffectivedate,
        p.transactionseqno as transactionseqno,
        4112 as sbuid,
        plc.lawyerId as contactentitykey  
      from (select * from ISI_Policy where EVENTTYPEID IN ('Cvg-DA') /* AND policykey = '0607190' */) p
      JOIN PolicyLawyerCoverageAllView plc on plc.policyId = p.POLICYID 
        AND plc.end_coverageId = p.alternatepolicykey   
      WHERE p.transactioneffectivedate::BIGINT > 20051231
   )
   SELECT all_attorney_trans.*, CASE WHEN deleted_attorneys.contactentitykey IS NOT NULL THEN 1 ELSE 0 END AS deletedrow
   FROM all_attorney_trans
   LEFT JOIN deleted_attorneys ON all_attorney_trans.POLICYINT = deleted_attorneys.POLICYINT
      AND all_attorney_trans.contactentitykey = deleted_attorneys.contactentitykey
      AND (all_attorney_trans.transactioneffectivedate::BIGINT * 100) + all_attorney_trans.transactionseqno >= (deleted_attorneys.transactioneffectivedate::BIGINT * 100) + deleted_attorneys.transactionseqno
   ORDER BY POLICYINT, transactioneffectivedate, transactionseqno, contactentitykey
;

/*
SELECT * FROM ISI_InsuredsFirmView WHERE POLICYINT = '0607190';
SELECT * FROM ISI_InsuredsLawyerView WHERE POLICYINT = '0607190';
SELECT * FROM ISI_ItemLawyerWithDeleteIndView WHERE POLICYINT = '0607190';
*/





/*   
-- union all
--create or replace view ISI_InsuredsLawyerERPView as 
--  lawyer tail level (04/30/2018)
*/
    /* select
        p.policykey as POLICYINT,
        p.alternatepolicykey as COVERAGEID,
        p.policyeffectivedate as POLICYEFFECTIVEDATE,
        p.policyexpirydate as POLICYEXPIRYDATE,
        p.veffectivedate as VEFFECTIVEDATE,
        p.vexpirydate as VEXPIRYDATE, 
        p.cliententitymappingkey as FIRMID,
        p.termlengthind as POLICYTERM,
        p.EVENTTYPEID as EVENTTYPEID,
        p.conversionreference as conversionreference,
        p.transactiontype as transactiontype,
        p.transactioneffectivedate as transactioneffectivedate,
        p.transactionseqno as transactionseqno,
        4112 as sbuid,
        plc.lawyerPolicySequence as insuredid,
        7 as insuredrole,
        plc.lawyerId as contactentitykey,
        0 as contactentityvnumber,
        NULL as defaultaddresskey,
        NULL as defaultcontactmethodkey,
        NULL as conversionserver,
        NULL as sequencenumber,
        NULL as postedwweobjectid,
        p.policykey||TO_INT(ORA_HASH(plc.lawyerId)) as ouid
    from (select * from ISI_Policy where transactiontype = 'ERP') p  --previously ISI_PolicyView
        inner join TailLawyer2 TL on TL.coverageId = p.alternatepolicykey  
            left join PolicyLawyerCoverageAllView plc on plc.policyId = TL.policyId and plc.lawyerId = TL.lawyerId */
    /*
        left join PolicyLawyerCoverageAllView plc on policyId = p.POLICYID
        ---- date logic to exclude deleted lawyers since deleted lawyers appear as 2 records in LAW (add/delete)
            and (p.transactioneffectivedate BETWEEN TO_CHAR(plc.effectiveDate, 'YYYYMMDD') and TO_CHAR(plc.expirationDate, 'YYYYMMDD')
            and p.transactioneffectivedate <= TO_CHAR(COALESCE(plc.excludeAfterDate, plc.expirationDate), 'YYYYMMDD'))  -- previously compare was <
    */
    /* where plc.effectiveDate > '31-DEC-2005' */
/*
--order by POLICYINT, transactioneffectivedate, insuredrole, contactentitykey
--;
--union all
--create or replace view ISI_InsuredsLawyerENDView as 
--  lawyer endorsement level (04/30/2018)
*/
/*    select
        p.policykey as POLICYINT,
        p.alternatepolicykey as COVERAGEID,
        p.policyeffectivedate as POLICYEFFECTIVEDATE,
        p.policyexpirydate as POLICYEXPIRYDATE,
        p.veffectivedate as VEFFECTIVEDATE,
        p.vexpirydate as VEXPIRYDATE, 
        p.cliententitymappingkey as FIRMID,
        p.termlengthind as POLICYTERM,
        p.EVENTTYPEID as EVENTTYPEID,
        p.conversionreference as conversionreference,
        p.transactiontype as transactiontype,
        p.transactioneffectivedate as transactioneffectivedate,
        p.transactionseqno as transactionseqno,
        4112 as sbuid,
        plcv.lawyerPolicySequence as insuredid,
        7 as insuredrole,
        plcv.lawyerId as contactentitykey,
        0 as contactentityvnumber,
        NULL as defaultaddresskey,
        NULL as defaultcontactmethodkey,
        NULL as conversionserver,
        NULL as sequencenumber,
        NULL as postedwweobjectid,
        p.policykey||TO_INT(ORA_HASH(plc.lawyerId)) as ouid */
    /* from (select * from ISI_Policy where EVENTTYPEID in ('Cvg-DA', 'Cvg-AA', 'Cvg-PA')) p  --previously ISI_PolicyView
        --inner join TailLawyer2 TL on TL.coverageId = p.alternatepolicykey  
        inner join PolicyLawyerCoverage plc on plc.coverageId = p.alternatepolicykey 
            left join PolicyLawyerCoverageAllView plcv on plcv.policyId = plc.policyId and plcv.lawyerId = plc.lawyerId
    where plc.effectiveDate > '31-DEC-2005' */
/*    
--ORDER BY POLICYINT, transactioneffectivedate, transactionseqno, insuredrole, contactentitykey
--;
*/





































/* OLD VERSION PRE 01/12/2018
create or replace view PolicyLawyerCoverageAllView as 
    select
        p.firmId,
        p.policyNumber,
        plc.policyId,
        plc.lawyerId,
        row_number() over (partition by p.firmId, plc.policyId order by plc.lawyerId) as insuredid,
        plc.coverageId,
        plc.eventId,
        plc.effectiveDate,
        plc.expirationDate,
        plc.retroactiveDate,
            plc.cancelEffectiveDate,
        plc.excludeBeforeDate,
        plc.cancellationCoverageId,
        plc.policyStatus,
        PLC2.coverageId as deleteCoverageId,
        PLC2.eventId as deleteEventId,
        PLC2.excludeAfterDate as excludeAfterDate
    from PolicyLawyerCoverage PLC
        left join (select * from PolicyLawyerCoverage where excludeAfterDate is not NULL) PLC2 on PLC2.policyId = plc.policyId and PLC2.lawyerId = plc.lawyerId   
        left join Policy p on p.policyId = plc.policyId
    where plc.excludeAfterDate is NULL
--        and p.policyNumber = '0601090'
--      and p.firmId in ('KIMLAV')
--      and p.policyNumber = '1606112'
    order by p.firmId, p.policyNumber, plc.effectiveDate, plc.lawyerId
;
*/
select * from PolicyLawyerCoverageAllView where policyNumber = '1606112' order by firmId, policyId, effectiveDate, lawyerId;
select * from PolicyLawyerCoverageAllView where policyNumber = '1004063' order by firmId, policyId, effectiveDate, lawyerId;


-- PolicyAccountingSumView
--
/*
CREATE OR REPLACE FORCE VIEW PolicyAccountingSumView (firmId, policyId, paymentDate, paymentAmount, premiumAdjust, transfer, writeOff, dueAmount, premium, financeCharge) AS 
  select 
        firmId, 
        policyId, 
        TO_DATE(TO_CHAR(paymentDate, 'DD-MON-YYYY')) as paymentDate,
        sum(paymentAmount) as paymentAmount, 
        sum(premiumAdjust) as premiumAdjust,
        sum(transfer) as transfer,
        sum(writeOff) as writeOff,
        sum(dueAmount) as dueAmount,
        sum(premium) as premium, 
        sum(financeCharge) as financeCharge 
    from PolicyAccountingView 
    group by firmId, policyId, TO_DATE(TO_CHAR(paymentDate, 'DD-MON-YYYY'));
*/



/*-- Testing --*/
select * from ISI_PolicyView order by conversionreference, policykey, transactioneffectivedate, transactionseqno;
select * from ISI_PolicyView where conversionreference = 'MARELT_2011_2018';
select * from ISI_PolicyView where conversionreference = 'PREVGO_1988_2018' and transactiontype = 'RCA' and transactioneffectivedate = '20100601' and relatedrevrcatranseqno = 6;
select * from Endorsement where policyId in ('AP000703551', 'AP000720161') order by policyId, endorsementNumber;
select DISTINCT policyId, effectiveDate, min(endorsementNumber) as endorsementNumber
from Endorsement E
    left join ISIFirmPolicyView fpv on fpv.policyId = e.policyId
        left join Policy p on p.policyId = fpv.policyId
            left join ISIConversionReferenceView icrv on icrv.firmId = p.firmId and (p.effectiveDate BETWEEN icrv.startDate and icrv.endDate and p.expirationDate BETWEEN icrv.startDate and icrv.endDate)
--            left join ISIConversionReferenceView icrv on icrv.firmId = p.firmId and (p.effectiveDate BETWEEN icrv.startDate and icrv.endDate and (p.expirationDate BETWEEN icrv.startDate and icrv.endDate or p.cancelEffectiveDate BETWEEN icrv.startDate and icrv.endDate))
where policyId in ('AP000703551', 'AP000720161') group by policyId, effectiveDate order by effectiveDate;
select * from ISIConversionReferenceView where firmId in ('KIMLAV', 'KIM_LA', 'LAVOLA', 'GAUTLA', 'ALEMAS') order by firmId, startDate ;
select * from ISIConversionReferenceView where firmId in ('GAUTLA', 'ALEMAS') order by firmId, startDate ;
-- tail testing
select * from ISI_PolicyView where policykey in ('1710130', '1608097', '1510133');
--------------------------------------------------------------------------------   
/*
    ISI_BillingDetailView
    p_billingdetail
    
    HISTORY:
    04/20/2018  FZ  changed policyexpirydate to use originl policy expiration date

*/

create or replace view ISI_BillingDetailView as 
    select
    -- DEBUG AREA
        p.policyNumber as POLICYINT,
        PV.conversionreference as conversionreference,
        PV.sbuid as sbuid,
        CASE
            WHEN p.paymentMethod = 'QP' THEN '4 Pay 25 Down'
            WHEN p.paymentMethod = 'MP' THEN '10 Pay'
            ELSE 'Annual'
        END as paymentplan,
        19000101 as planeffdate,
        20991231 as planexpdate,
        --TO_CHAR(p.effectiveDate, 'YYYYMMDD') as policyeffectivedate,
        PV.policyeffectivedate as policyeffectivedate ,--04/21/2018
        --TO_CHAR(COALESCE(p.cancelEffectiveDate, p.expirationDate), 'YYYYMMDD') as policyexpirydate,
        -------TO_CHAR(p.expirationDate, 'YYYYMMDD') as policyexpirydate,  --04/20/2018 change to use original expiration date instead of 
        PV.policyexpirydate as policyexpirydate,  --04/21/2018
        2 as payplantype,  --2=invoice
        NULL as paymentmethodentitykey,
        p.financeCharge as fees,
        p.premium + p.premiumAdjust as totalamount,  ---????? what about surplus, transfer and writeoff
        p.premium + p.premiumAdjust - p.financeCharge as grossamount,
        TO_CHAR(p.effectiveDate, 'DD') as preferredbillday,
        CASE
            WHEN p.paymentMethod = 'QP' THEN 4
            WHEN p.paymentMethod = 'MP' THEN 10
            ELSE 1
        END as maximumterms,
        CASE
            WHEN p.paymentMethod = 'QP' THEN 3
            WHEN p.paymentMethod = 'MP' THEN 1
            ELSE 5
        END as unit,
        0 as feesondownpaymentind,
        NULL as downpaymenttype,  --??
        0 as downpaymentvalue,
        'USD' as downpaymentcurrency,
        NULL as financetype,
        0 as financevalue,
        'USD' as financecurrency,
        NULL as lagdays,
        NULL as roundondownpaymentind,
        CASE
            WHEN p.paymentMethod = 'QP' THEN 4
            WHEN p.paymentMethod = 'MP' THEN 10
            ELSE 1
        END as numpayments,
         -- REVISE: need to determine number of payments remaining
        CASE
            WHEN p.paymentMethod = 'QP' THEN 0
            WHEN p.paymentMethod = 'MP' THEN 0
            ELSE 0
        END as numpaymentsleft,
        ((p.premium + p.premiumAdjust + p.financeCharge) - p.receiveAmount) as outstandingamount,
        (p.premium + p.premiumAdjust + p.financeCharge) - (p.receiveAmount - p.financeChargeReceipt) as outstandinggrossamount,
        1 as activeind,
        NULL as authorizationdate,
        NULL as downpaymentreceivedind,
        NULL as invoicenumber,
        TO_CHAR(p.effectiveDate, 'YYYYMMDD') as invoicedate,  --?????
        NULL as invoicecontactentitykey,
        NULL as invoiceaddresskey,
        NULL as conversionsever,
        NULL as sequencenumber,
        NULL as postedwweobjectid,
        NULL as invoicecontactentityvnumber,
        NULL as premfinancecontactentitykey,
        NULL as premfinancecontactentkeyvnum,
        NULL as premfinanceamount,
        NULL as premfinancecontractdate,
        0 as overridefeesind,
        NULL as nocissuedate,
        2 as directbillmailto,
        NULL as taxchargeref
    from (select * from Policy where (premium + premiumAdjust) <> 0) p
        inner join (select * from ISI_Policy where EVENTTYPEID = 'PolicyIssue') PV on PV.policykey = p.policyNumber
        
        --left join ISIConversionReferenceView icrv on icrv.firmId = p.firmId and (p.effectiveDate BETWEEN icrv.startDate and icrv.endDate and COALESCE(p.cancelEffectiveDate, p.expirationDate) BETWEEN icrv.startDate and icrv.endDate)
    where p.effectiveDate > '31-DEC-2005'
    -- BELOW ITEMS NEED FIXING
    --  and p.firmId not in ('SKOGLA', 'CASEL1')
    --  and p.policyNumber NOT in ('0607050', '0703097', '0807068', '0811071', '1003092', '1006065', '1007005', '1111122', '1112041', '1206065', '1308050', '1408002', '1505056', '1602014',  '1601090')
;

/*-- Testing --*/
select * from ISI_BillingDetailView;
select * from ISI_Policy where policykey = '1111122';
select * from ISI_BillingDetailView where POLICYINT = '1111122';
select * from ISI_Policy where policykey = '0702005';
select * from PolicyAccounting where policyId in ('AP000823716', 'AP001054520', 'AP11057746');
select * from PolicyAccounting where financeChargeDue <> 0;
select * from Policy where policyId in ('AP000823716', 'AP001054520');
select * from Policy where firmId in ('KIMLAV') order by effectiveDate;
select * from ISI_BillingDetailView where conversionreference = 'PREVGO_1988_2018' order by conversionreference, policyeffectivedate;--PREVGO_1988_2018 4112  20100601  20110601
--------------------------------------------------------------------------------
/*
    ISI_PaymentScheduleView
    p_paymentschedule
    
    Dependancies: ISI_BillingDetailView
    
    NOTES:  04/23/2018---ISI TO CREATE PAYMENTSCHEDULE INTERIM TABLE AS PART OF THE GL CONVERSION
    
    History:
    03/08/2018  FZ Created
*/
create or replace view ISI_PaymentScheduleView as
    --Quarterly and Monthly pay plan
    select
--        iv.eventTypeId as EVENTTYPEID,
        PV.policykey as POLICYINT,
        PV.conversionreference as conversionreference,
        PV.transactiontype as transactiontype,
        PV.transactioneffectivedate as transactioneffectivedate,
        PV.transactionseqno as transactionseqno,
        PV.sbuid assbuid,
        PV.policyeffectivedate as policyeffectivedate,
        PV.policyexpirydate as policyexpirydate,
        --PV.vexpirydate as policyexpirydate,
        TO_CHAR(iv.dueDate, 'YYYYMMDD') as scheduledate,
        iv.premium as amount,
        COALESCE(iv.financeCharge, 0) + COALESCE(iv.lateCharge, 0) as fees,
        iv.invoiceAmount as totalamount,
        iv.premium as grossamount,
        CASE
            WHEN iv.paidDate is NULL THEN 0
            ELSE 1
        END as fixedind,
        NULL as transactionid,
        CASE 
            WHEN iv.paidDate is NULL THEN 0
            ELSE 1
        END as paidind,
        CASE
            WHEN iv.eventTypeId = 'InitialPayInvoice' THEN 1
            ELSE 0
        END as downpaymentind,  --0=initial payment, 1=regular payment
        NULL as conversionserver,
        NULL as sequencenumber,
        NULL as postedwweobjectid,
        TO_CHAR(iv.eventDate, 'YYYYMMDD') as processeddate,
        iv.premium as premium,
        NULL as comm,
        COALESCE(iv.financeCharge, 0) + COALESCE(iv.lateCharge, 0) as  tfs,
        TO_CHAR(iv.dueDate, 'YYYYMMDD') as duedate,
        CASE
            WHEN p.paymentMethod = 'MP' THEN 'FAKE_ACCOUNT'
            ELSE NULL
        END as paymentmethodkey
    from (select * from InvoiceView where invoiceAmount <> 0) iv
        inner join Policy p on p.policyId = iv.policyId
            inner join (select * from ISI_Policy where transactionseqno = 1 and transactiontype <> 'RCA') PV on PV.policykey = p.policyNumber
--order by p.policyNumber, iv.dueDate
/*
    from (select * from ISI_PolicyView where transactionseqno = 1) PV
        left join Policy p on p.policyNumber = PV.policykey
            left join InvoiceView iv on iv.policyId = p.policyId
*/
;
--union all   
create or replace view ISI_PaymentScheduleFPView as 
    -- Full pay policies
    select
        PV.policykey as POLICYINT,
        PV.conversionreference as conversionreference,
        PV.transactiontype as transactiontype,
        PV.transactioneffectivedate as transactioneffectivedate,
        PV.transactionseqno as transactionseqno,
        PV.sbuid assbuid,
        PV.policyeffectivedate as policyeffectivedate,
        PV.policyexpirydate as policyexpirydate,  --04/21/2018 removed to use vexpirydate to account for cancel/rewrites
        pa.paymentDate as scheduledate,
        pa.paymentAmount as amount,
        COALESCE(pa.financeCharge, 0) as fees,
        pa.paymentAmount as totalamount,
        pa.premium as grossamount,
        1 as fixedind,
        NULL as transactionid,
        CASE 
            WHEN pa.paymentDate is NULL THEN 0
            ELSE 1
        END as paidind,
        1 as downpaymentind,  --0=initial payment, 1=regular payment
        NULL as conversionserver,
        NULL as sequencenumber,
        NULL as postedwweobjectid,
        TO_CHAR(pa.paymentDate, 'YYYYMMDD') as processeddate,
        pa.premium as premium,
        NULL as comm,
        COALESCE(pa.financeCharge, 0) as  tfs,
        TO_CHAR(pa.paymentDate, 'YYYYMMDD') as duedate,
        CASE
            WHEN p.paymentMethod = 'MP' THEN 'FAKE_ACCOUNT'
            ELSE NULL
        END as paymentmethodkey
    from (select * from Policy where paymentMethod = 'PD') p
        inner join PolicyAccountingSumView pa on pa.policyId = p.policyId
        inner join (select * from ISI_Policy where transactionseqno = 1) PV on PV.policykey = p.policyNumber
;




/*-- Testing --*/
/*
select * from ISI_InsuredsFirmView; where POLICYINT = '1003092';
SELECT * FROM ISI_ItemLawyerWithDeleteIndView; where transactiontype is NULL;
select * from ISI_InsuredsLawyerView; where transactiontype is NULL;
select count(*) from ISI_InsuredsLawyerView;
select * from ISI_PolicyView where policykey = '1003092';
select * from ISI_PolicyView where policykey = '0601090';
select * from ISI_InsuredsLawyerView where POLICYINT = '0601090';
select * from ISI_InsuredsLawyerView where contactentitykey = 'SKOGLA'; 
select * from ISI_PolicyView;
select * from PolicyLawyerCoverageAllView where policyId = 'AP11057746';
select * from PolicyLawyerCoverageAllView where policyId = 'AP11696463';
select * from PolicyLawyerCoverage where policyId = 'AP11057746';
select * from PolicyLawyer where policyId = 'AP11057746';

select * from ISI_AddressView where entitykey in ('KIMLAV', 'KIM_JULI');
select * from ISI_ClientView where entitykey = 'KIMLAV';
select * from ISI_ContactView where entitykey = 'KIM_JULI';
select * from ISI_PolicyView; --1606112  KIM_JULI
select * from PolicyLawyerCoverage where policyId in ('AP000823716', 'AP001054520') order by policyId, effectiveDate;
-- tail testing
select * from ISI_PolicyView where policykey in ('1710130', '1608097', '1510133');
*/

--------------------------------------------------------------------------------    
/*
    ISI_ItemView
    ISI table = p_items
    
    Dependancies: ISI_InsuredsView
    
    History
    12/01/2017  FZ  Created
    03/31/2018  FZ  split into 2 views
    06/05/2018  LP  Tweaked tailTerm
    
*/    
select count(*) from ISI_ItemFirmView;
create or replace view ISI_ItemFirmView as 
    -- firm record
    select
        iv.conversionreference as conversionreference,
        iv.transactiontype as transactiontype,
        iv.POLICYINT as POLICYINT, 
        iv.COVERAGEID as COVERAGEID,
        NULL as LAWYERID,
        iv.POLICYTERM as POLICYTERM,
        iv.EVENTTYPEID as EVENTTYPEID,
        iv.transactioneffectivedate as transactioneffectivedate,
        iv.transactionseqno as transactionseqno,
        'LPL__FIRM__WI_19000101_20991231_4112' as pcmitemmappingkey,
        NULL as pcmfloatermappingkey,
        iv.sbuid as sbuid,
        0 as packageid,
        iv.insuredid as riskid,  
        NULL as locationnumber,
        1 as riskgroup, -- firm
        NULL as commissionamt,
        NULL as commissioncalcamt,
        NULL as commissiontype,    
        0 as deletedrow,
        NULL as conversionserver,
        NULL as sequencenumber,
        iv.VEFFECTIVEDATE as veffectivedate,
        iv.VEXPIRYDATE as vexpirydate,
        NULL as formcode,
        1 as claimsmadeind,
        TO_CHAR(z.ISIPolicyRetroDate, 'YYYYMMDD') as retrodate,
        NULL as postedwweobjectid,
        NULL as overridereasoncd,
        NULL as overridereasonnote,
        NULL as billbyitemind,
        NULL as taxamount,
        NULL as parentriskid,
        NULL as buildingnumber,
        NULL as occupancynumber,
        'LPL_Ratebook' as rateeditionname,
        2 as rateeditionversion,
        NULL as vehicleuse,
        NULL as accumulationcode,
        iv.ouid as ouid,
        CASE 
            WHEN iv.transactiontype = 'ERP' THEN TO_CHAR(tla.effectiveDate, 'YYYYMMDD') 
            ELSE NULL
        END as extendreporteffdate,
        CASE
            WHEN iv.transactiontype = 'ERP' AND TO_CHAR(tla.expirationDate, 'DD-MON-YY') = '31-DEC-99' THEN '99991231'
            WHEN iv.transactiontype = 'ERP' AND tla.tailTerm is not NULL THEN TO_CHAR(tla.expirationDate, 'YYYYMMDD')
            ELSE NULL
        END as extendreportexpdate,
        CASE
            WHEN iv.transactiontype = 'ERP' AND tla.tailTerm = 'Unlimited' THEN NULL
            WHEN iv.transactiontype = 'ERP' AND tla.tailTerm in ('1','2','3','4','5','6') THEN tla.tailTerm::NUMERIC
            ELSE NULL
        END as extendreporttermlength,
        CASE
            WHEN iv.transactiontype = 'ERP' AND tla.tailTerm = 'Unlimited' THEN 'U'
            WHEN iv.transactiontype = 'ERP' AND tla.tailTerm is not NULL THEN 'Y'
            ELSE NULL
        END as extendreportlengthunit,
        iv.FIRMID as FIRMID
    from ISI_InsuredsFirmView iv 
        left join ISIPolicyRetroDateView z on z.policyNumber = iv.POLICYINT
    --    left join (SELECT DISTINCT coverageId, effectiveDate, expirationDate, tailTerm, sum(oldTailPremium) as tailPremiumTotal from TailLawyerAllView where tailType = 'Firm' group by policyNumber, effectiveDate, expirationDate, tailTerm) tla on tla.policyNumber = iv.POLICYINT
        left join (SELECT DISTINCT coverageId, effectiveDate, expirationDate, tailTerm from TailLawyerAllView) tla on tla.coverageId = iv.COVERAGEID
    where iv.insuredrole = 2
  ORDER BY POLICYINT, transactioneffectivedate, transactionseqno, riskid
;
select * from ISI_ItemFirmView; where conversionreference = 'KLOSFL_1987_2007';
select * from ISI_InsuredsFirmView where conversionreference = 'KLOSFL_1987_2007';
select * from TailLawyerAllView where policyNumber = '0607050';



/* LP June 4, 2018 Update view to select from ISI_ItemLawyerWithDeleteIndView */
select count(*) from ISI_ItemLawyerView;
create or replace view ISI_ItemLawyerView as 
-- lawyer record
    select
        iv.conversionreference as conversionreference,
        iv.transactiontype as transactiontype,
        iv.POLICYINT as POLICYINT,  
        iv.COVERAGEID as COVERAGEID,
        iv.contactentitykey as LAWYERID,
        iv.POLICYTERM as POLICYTERM,
        iv.EVENTTYPEID as EVENTTYPEID,
        iv.transactioneffectivedate as transactioneffectivedate,
        iv.transactionseqno as transactionseqno,
        'LPL__ATTORNEY__WI_19000101_20991231_4112' as pcmitemmappingkey,
        NULL as pcmfloatermappingkey,
        4112 as sbuid,
        0 as packageid,
        --SUBSTR(TO_CHAR(TO_INT(ORA_HASH(y.lawyerId))),-3) as riskid,  --unique number hashed using lawyerId
        iv.insuredid as riskid,
        NULL as locationnumber,
        1 as riskgroup, -- firm
        NULL as commissionamt,
        NULL as commissioncalcamt,
        NULL as commissiontype, 
        iv.deletedrow,
        NULL as conversionserver,
        NULL as sequencenumber,
        iv.VEFFECTIVEDATE as veffectivedate, --01/14/2018 previously TO_CHAR(p.effectiveDate, 'YYYYMMDD') as veffectivedate,
        iv.VEXPIRYDATE as vexpirydate,
        NULL as formcode,
        1 as claimsmadeind,
        CASE 
            WHEN x.ISILawyerRetroDate is not NULL THEN TO_CHAR(x.ISILawyerRetroDate, 'YYYYMMDD')::NUMERIC
            ELSE iv.VEFFECTIVEDATE
        END as retrodate,
        NULL as postedwweobjectid,
        NULL as overridereasoncd,
        NULL as overridereasonnote,
        NULL as billbyitemind,
        NULL as taxamount,
        1 as parentriskid,  --fix #
        NULL as buildingnumber,
        NULL as occupancynumber,
        'LPL_Ratebook' as rateeditionname,
        2 as rateeditionversion,
        NULL as vehicleuse,
        NULL as accumulationcode,
        --TO_CHAR(TO_INT(ORA_HASH(iv.POLICYINT)))||SUBSTR(TO_CHAR(TO_INT(ORA_HASH(iv.insuredid))),-3) as ouid,  
        --
        CASE 
            WHEN iv.transactiontype = 'ERP' THEN TO_CHAR(tla.effectiveDate, 'YYYYMMDD') 
            ELSE NULL
        END as extendreporteffdate,
        CASE
            WHEN iv.transactiontype = 'ERP' AND TO_CHAR(tla.expirationDate, 'DD-MON-YY') = '31-DEC-99' THEN '99991231'
            WHEN iv.transactiontype = 'ERP' AND tla.tailTerm is not NULL THEN TO_CHAR(tla.expirationDate, 'YYYYMMDD')
            ELSE NULL
        END as extendreportexpdate,
        CASE
            WHEN iv.transactiontype = 'ERP' AND tla.tailTerm = 'Unlimited' THEN NULL
            WHEN iv.transactiontype = 'ERP' AND tla.tailTerm in ('1','2','3','4','5','6') THEN tla.tailTerm::NUMERIC
            ELSE NULL
        END as extendreporttermlength,
        CASE
            WHEN iv.transactiontype = 'ERP' AND tla.tailTerm = 'Unlimited' THEN 'U'
            WHEN iv.transactiontype = 'ERP' AND tla.tailTerm is not NULL THEN 'Y'
            ELSE NULL
        END as extendreportlengthunit,
        iv.FIRMID as FIRMID      
        /* LP June 4, 2018 Changed the source view */
    from (select * from ISI_ItemLawyerWithDeleteIndView where COVERAGEID <> 'CG001148214') iv 
        --left join PolicyLawyerCoverageAllView y on y.policyNumber = iv.POLICYINT and y.lawyerId = iv.contactentitykey 
            -- and (TO_CHAR(y.effectiveDate, 'YYYYMMDD') <= iv.transactioneffectivedate)
        left join ISILawyerRetroDateView x on x.lawyerId = iv.contactentitykey and x.policyNumber = iv.POLICYINT 
        left join (SELECT DISTINCT * from TailLawyerAllView) tla on tla.coverageId = iv.COVERAGEID and tla.lawyerId = iv.contactentitykey
        --left join (select * from TailLawyerView) tla on tla.coverageId = iv.COVERAGEID and tla.lawyerId = iv.contactentitykey    
    /* LP Only lawyers in ISI_ItemLawyerWithDeleteIndView so no need for this condition */
    /* where iv.insuredrole = 7 */
  order by POLICYINT, transactioneffectivedate, transactionseqno, riskid
;
    
 
/*--- Testing  ---*/
/*
SELECT * FROM ISI_ItemLawyerView WHERE POLICYINT = '0607190';

select * from PolicyLawyerCoverageAllView where policyNumber = '1202024';
select * from ISILawyerRetroDateView where lawyerId = 'EY000200317' and policyNumber = '1202024';
select * from ISI_ItemView;
select * from ISI_ItemLawyerView;
select * from ISI_ItemLawyerView where POLICYINT = '0709073';
select * from ISI_ItemLawyerView where POLICYINT = '0601090' order by FIRMID, POLICYINT, transactioneffectivedate, transactionseqno;
select FIRMID, iv.* from ISI_ItemLawyerView iv order by iv.FIRMID, iv.POLICYINT, iv.transactioneffectivedate, iv.transactionseqno;
select * from ISILawyerRetroDateView; 
select * from ISILawyerRetroDateView where lawyerId = 'LSB1031169';
select * from ISIPolicyRetroDateView order by policyId;
select * from ISI_InsuredsView where POLICYINT = '1202024';
select p.firmId, p.policyNumber,plc.* from PolicyLawyerCoverage plc left join Policy p on p.policyId = plc.policyId where p.firmId in ('KIMLAV') order by p.policyNumber, lawyerId;
select * from ISI_PolicyView;
select * from PolicyLawyerCoverageAllView where lawyerId = 'LSB1031169';
*/

--------------------------------------------------------------------------------
/*
    ISI_LawFirm1View
    i_lawfirm_1
    
    Dependancies:
    
    History
    12/01/2017  FZ  Created
*/
create or replace view ISI_LawFirm1View as
    select
        PV.conversionreference as conversionreference,
        PV.transactioneffectivedate as transactioneffectivedate,
        PV.transactionseqno as transactionseqno,
        PV.transactiontype as transactiontype,
        p.policyNumber as POLICYINT,
        PV.sbuid as sbuid,
        0 as packageid,
        1 as riskid,
        NULL as abstracterentity,
        CASE
            WHEN AQAT.questionId IS NOT NULL THEN 1
            ELSE 0
        END as abstracterind,
        NULL as abstracterinscompagentind,
        NULL as abstracterownfirmentitylabel,
        NULL as abstracterrevenue, 
        NULL as affiliationind,
        COALESCE(Rc.SchdAdjPctCalc,1.00) - COALESCE(Rc.SeverityIndexZero,0) as aopfactorschedadj,
        NULL as attorneydisbarredind,
        NULL as attorneydisbarredinfo,
        NULL as attorneyerrorind,
        NULL as attorneygrievanceind,
        0 as calcpremiumxs5m,
        0 as claimsratioactloss10_1,
        0 as claimsratioactloss1_1,
        0 as claimsratioactloss3_1,
        0 as claimsratioactloss5_1,
        0 as claimsratioactlossall_1,
        0 as claimsratioactratio10_1,
        0 as claimsratioactratio1_1,
        0 as claimsratioactratio3_1,
        0 as claimsratioactratio5_1,
        0 as claimsratioactratioall_1,
        0 as claimsratioincloss10_1,
        0 as claimsratioincloss1_1,
        0 as claimsratioincloss3_1,
        0 as claimsratioincloss5_1,
        0 as claimsratioinclossall_1,
        0 as claimsratioincratio10_1,
        0 as claimsratioincratio1_1,
        0 as claimsratioincratio3_1,
        0 as claimsratioincratio5_1,
        0 as claimsratioincratioall_1,
        0 as claimsratioprem10_1,
        0 as claimsratioprem1_1,
        0 as claimsratioprem3_1,
        0 as claimsratioprem5_1,
        0 as claimsratiopremall_1,
        NULL as claimsurcharge,
        NULL as claimsurchargecreditmax,
        NULL as claimsurchargecreditmin,
        'N/A' as claimsurchargecreditrange,
        '150' as claimsurchargedebitmax,
        '5' as claimsurchargedebitmin,
        '5% to 150%' as claimsurchargedebitrange,
        NULL as conditionalmandatoryfields,
        NULL as contactwhenabsent,
        NULL as contingencyfeeind,
        1 + COALESCE(Rc.YearsInsured, 0) as continuityfactor,
        0 as conversionpremiumrounding,
        COALESCE(Pe.WLMEND02, NULL) as covind_wlmend02,
        COALESCE(Pe.WLMEND03, NULL) as covind_wlmend03,
        NULL as covind_wlmend22,
        NULL as covind_wlmend23,
        COALESCE(Pe.WLMEND24, NULL) as covind_wlmend24,
        COALESCE(Pe.WLMEND25, NULL) as covind_wlmend25,
        NULL as covind_wlmend27,
        COALESCE(Pe.Manuscript, NULL) as covind_wlmendmn,
        rlc.PremXS10mCalc as facpremiumxs10m,
        rlc.PremXS5mMinCalc as facpremiumxs5m,     
        NULL as financialinterestclientind,
        a.firmName as firmname,
        a.totalLawyerCount as firmsize, --lawyerPrincipalCount
        COALESCE(Rc.Firm, 1.00) as firmsizefactor,  
        a.totalLawyerCount as firmsizeforrating,  
        --en.entityURL as firmwebsite,
        NULL as foreignclientcountries,
        NULL as foreignclientind,
        0 as insurepastworkind, --R2
        NULL as licensingagreementind,
        NULL as licensingagreementpct,
        NULL as localaffiliateind,
        a.staffCount as nonattorneystaff, 
        0 as officerclientind,
        0 as officesharingind,
        0 as oneclient10pctind,
        O.organizationLegalForm as organizationstructure,
        --start new release 2
        NULL as otherdebit,
        NULL as otherdebitcreditmax,
        NULL as otherdebitcreditmin,
        NULL as otherdebitcreditrange,
        NULL as otherdebitdebitmax,
        NULL as otherdebitdebitmin,
        NULL as otherdebitdebitrange,
        NULL as otherdebitexplain,
        0 as outofstatefutureind,
        --end new release 2
        0 as outofstateind,
        NULL as outsoursepaymentind,
        NULL as outsoursesearchind,
        NULL as percentipcatcopyright,
        NULL as percentipcatdomestic,
        NULL as percentipcatforeign,
        NULL as percentipcatinfringement,
        NULL as percentipcatintproperty,
        NULL as percentipcatother,
        NULL as percentipcatotherexplain,
        NULL as percentipcattrademark,
        NULL as percentiptypebiotechnology,
        NULL as percentiptypebusiness,
        NULL as percentiptypechemical,
        NULL as percentiptypecomputer,
        NULL as percentiptypeelectrical,
        NULL as percentiptypemechanical,
        NULL as percentiptypeother,
        NULL as percentiptypeotherexplain,
        NULL as predecessorfirmsnotapplicab_1,
        0 as prevlawfirmind,
        0 as priordeclinedind,
        NULL as priordeclinedinfo,
        NULL as priortailind,
        NULL as priortailinfo,
        0 as reinsurancepremium1m,  --R2
        0 as reinsurancepremium1mfactor, --R2
        0 as reinsurancepremiumxs1, --R2
        NULL as restrictprioractsind,
        NULL as restrictprioractsinfo,
        Rc.SchdAdjPctCalc * 100 as scheduledadjustmentstotal,
        0 as severityindex,
        NULL as severityindexcreditmax,
        NULL as severityindexcreditmin,
        'N/A' as severityindexcreditrange,
        '20' as severityindexdebitmax,
        '1' as severityindexdebitmin,
        '1% to 20%' as severityindexdebitrange,
         1 + COALESCE(Rc.SeverityIndexZero, 0) as severityindexzero,
        NULL as severityindexzerocreditmax,
        NULL as severityindexzerocreditmin,
        'N/A' as severityindexzerocreditrange,
        NULL as severityindexzerodebitmax,
        NULL as severityindexzerodebitmin,
        'N/A' as severityindexzerodebitrange,
        NULL as tailfactorunlimited,
        NULL as tailfactoryear_1,
        NULL as tailfactoryear_2,
        NULL as tailfactoryear_3,
        NULL as tailfactoryear_6,
        NULL as tailpremiumunlimited,
        NULL as tailpremiumyear_1,
        NULL as tailpremiumyear_2,
        NULL as tailpremiumyear_3,
        NULL as tailpremiumyear_6,
        NULL as thirdpartydocketind,
        NULL as thirdpartydocketsystem,
        NULL as wlmendmn_body, --R2
        NULL as wlmendmn_code, --R2
        NULL as wlmendmn_desc, --R2
        EXTRACT(YEAR FROM O.establishDate) as yearsestablished, --R2
        a.insureDuration as yearsinsured,
        NULL as conversionserver,
        NULL as sequencenumber
     from ISI_Policy PV  --previously ISI_PolicyView
        left join Policy p on p.policyNumber = PV.policykey
            left join Application a on a.applicationId = p.policyId
                left join (select * from ApplicationQuestion where questionId = 'AT') AQAT on AQAT.applicationId = a.applicationId
                left join RatingCrosstab RC on Rc.applicationId = a.applicationId
                    left join RatingLimitCrosstab rlc on rlc.quoteBookId = Rc.quoteBookId and rlc.Deductible = p.perClaimDeductible and rlc.Limit = p.perClaimLimit and rlc.LimitAgg is NULL
        left join Entity EN on en.entityId = p.firmId 
        left join Organization O on O.organizationId = p.firmId
        left join ISIPolicyEndorsementView PE on Pe.policyNumber = PV.policykey
    -- BELOW ITEMS NEED FIXING
    --  where PV.policykey not in ('0604074')  --endorsement effective date = 20050501 while policy effective date = 20060417
    order by POLICYINT, transactioneffectivedate, transactionseqno
;

/*-- Testing --*/
select * from ISI_LawFirm1View where conversionreference = 'VELEMO_2006_2007';
select * from ISI_LawFirm1View ;
select * from Policy where firmId in ('KIMLAV', 'VELEMO');
select * from ISI_PolicyView; where policyeffectivedate < '20060101';
select * from Application where firmId in ('KIMLAV') order by effectiveDate;
select * from RatingCrosstab where firmId in ('KIMLAV') order by effectiveDate;
select rlc.* from RatingCrosstab RC left join RatingLimitCrosstab rlc on rlc.quoteBookId = Rc.quoteBookId where Rc.firmId in ('KIMLAV') order by rlc.quoteBookId, rlc.quoteCoverageId;
select * from Quote;
select * from QuoteBook;
select * from QuoteCoverage;
select * from RatingLimitCrosstab order by quoteBookId, quoteCoverageId;
select 
    qc.*,
    q.*,
    a.* 
from Application a 
    left join Policy p on p.policyId = a.applicationId
    inner joinQuote q on q.applicationId = a.applicationId 
        left join QuoteCoverage qc on qc.quoteId = q.quoteId and qc.perClaimLimit = p.perClaimLimit and qc.aggregateLimit = p.aggregateLimit and qc.perClaimDeductible = p.perClaimDeductible
where a.firmId in ('KIMLAV') order by a.effectiveDate;
select * from ISIPolicyEndorsementView;
--------------------------------------------------------------------------------
/*
    ISI_Attorney1View
    i_attorney_1
    
    Dependancies:
    
    History
    12/04/2017  FZ  Created
    06/05/2018  LP Update view to select from ISI_ItemLawyerWithDeleteIndView 
*/
create or replace view ISI_Attorney1View as
    with A1 as (
        select
            iv.conversionreference as conversionreference,
            p.policyNumber as POLICYINT,
            iv.transactioneffectivedate as transactioneffectivedate,
            iv.transactionseqno as transactionseqno,
            iv.transactiontype as transactiontype,
            iv.sbuid as sbuid,
            0 as packageid,
            iv.riskid as riskid,  --should be same during concecutive policy period
            NULL as attorneyaddedthisterm,
            l.lawyerDesignationId as attorneydesignation,
            p1.email as attorneyemail,
            COALESCE(rlc.LawyerFactor, 1.00) as attorneyfactor1,  -- this is a problem for added lawyers
            p1.firstName||' '||p1.middleName||' '||p1.lastName as attorneyname,
            iv.LAWYERID as attorneynameid_ek,
            0 as attorneynameid_vn,
            --SUBSTR(TO_CHAR(TO_INT(ORA_HASH(iv.LAWYERID))),-3) as attorneynumber,  --unique number hashed using lawyerId
            iv.riskid as attorneynumber,
            0 as clecredits,
            DECODE(COALESCE(rlc.ContLegalEdCredit, 0), 0, 1.00, rlc.ContLegalEdCredit) as clecreditsfactor,
            NULL as currentleavestartdate,
            NULL as currentlyonleaveind,
    --        NULL as dateofbirth,  --01/16/2018
            NULL as endleavethistransind, 
            COALESCE(rlc.LawyerActivity, 1.00) * 160 as hoursworkedpermonth,
            TO_CHAR(COALESCE(L.inPracticeDate, lsb.barAdmitDate), 'YYYYMMDD') as inpracticedate,
            NULL as militaryleavetext,
            0 as militaryleavetotaldays,
            NULL as newattorneyprioracts,  --NEEDS WORK:::find WLMEND26
            NULL as otherlicenses,
            NULL as parttimecomment,
            COALESCE(rlc.LawyerActivity, 1.00) as parttimefactor,
            NULL as parttimefactoroverride,
            COALESCE(rlc.LawyerActivity, 1.00) as parttimefactorsys, --make same as parttimefactor
            NULL as parttimeoverridecomment,
            TO_CHAR(lsb.barAdmitDate, 'YYYY')||'-WI-'||lsb.stateBarNumber as primarylicense,
            'Less than '||(EXTRACT(YEAR from p.effectiveDate) - to_number(substr(iv.retrodate,1,4))) as retrodatecomment,
    --        'Less than '||(to_number(to_char(p.effectiveDate, 'YYYY')) - to_number(to_char(iv.retrodate, 'YYYY'))) as retrodatecomment,
            CASE
                WHEN rlc.LawyerRDI is NULL THEN
                    CASE
                        WHEN TRUNC(months_between(p.effectiveDate, TO_DATE(iv.retrodate,'YYYYMMDD'))/12) < 1 THEN 0.40
                        WHEN TRUNC(months_between(p.effectiveDate, TO_DATE(iv.retrodate,'YYYYMMDD'))/12) < 2 THEN 0.55
                        WHEN TRUNC(months_between(p.effectiveDate, TO_DATE(iv.retrodate,'YYYYMMDD'))/12) < 3 THEN 0.70
                        WHEN TRUNC(months_between(p.effectiveDate, TO_DATE(iv.retrodate,'YYYYMMDD'))/12) < 4 THEN 0.80
                        WHEN TRUNC(months_between(p.effectiveDate, TO_DATE(iv.retrodate,'YYYYMMDD'))/12) < 5 THEN 0.90
                        ELSE 1.00
                    END
                ELSE  rlc.LawyerRDI
            END as retrodatefactor,
            --COALESCE(rlc.LawyerRDI, 1.00) as retrodatefactor,
            iv.retrodate as retrodateoverride,
            NULL as startleavethistransind,
            NULL as tailattorneyfactor,
            NULL as taildeductiblefactor,
            NULL as tailfactor,
            NULL as tailfactorunlimited,
            NULL as tailfactoryear_1,
            NULL as tailfactoryear_2,
            NULL as tailfactoryear_3,
            NULL as tailfactoryear_6,
            NULL as tailpremiumbefattorneyfactor,
            NULL as tailpremiumfactoredup,
            NULL as tailpremiumstart,
            NULL as tailpremiumtotal,
            NULL as tailpremiumunlimited,
            NULL as tailpremiumwodeductible,
            NULL as tailpremiumxs10m,
            NULL as tailpremiumxs5m,
            NULL as tailpremiumyear_1,
            NULL as tailpremiumyear_2,
            NULL as tailpremiumyear_3,
            NULL as tailpremiumyear_6,
            NULL as tailschedadjustfactor,
            NULL as yearinpracticecommentor,
            CASE
                WHEN TO_INT(TO_CHAR(COALESCE(L.inPracticeDate, lsb.barAdmitDate), 'YYYY')) > TO_INT(TO_CHAR(p.effectiveDate, 'YYYY')) THEN 'Based on In Practice Date of 0 years'
                ELSE 'Based on In Practice Date of '||(TO_CHAR(TO_INT(TO_CHAR(p.effectiveDate, 'YYYY'))) - TO_INT(TO_CHAR(COALESCE(L.inPracticeDate, lsb.barAdmitDate), 'YYYY')))||' years' 
            END as yearinpracticecommentsys, 
            CASE
                WHEN rlc.LawyerYIP is NULL THEN
                    CASE
                        WHEN TRUNC(months_between(p.effectiveDate, COALESCE(L.inPracticeDate, lsb.barAdmitDate))/12) < 1 THEN 0.30
                        WHEN TRUNC(months_between(p.effectiveDate, COALESCE(L.inPracticeDate, lsb.barAdmitDate))/12) < 2 THEN 0.40
                        WHEN TRUNC(months_between(p.effectiveDate, COALESCE(L.inPracticeDate, lsb.barAdmitDate))/12) < 3 THEN 0.50
                        WHEN TRUNC(months_between(p.effectiveDate, COALESCE(L.inPracticeDate, lsb.barAdmitDate))/12) < 4 THEN 0.65
                        WHEN TRUNC(months_between(p.effectiveDate, COALESCE(L.inPracticeDate, lsb.barAdmitDate))/12) < 5 THEN 0.75
                        WHEN TRUNC(months_between(p.effectiveDate, COALESCE(L.inPracticeDate, lsb.barAdmitDate))/12) < 6 THEN 0.90
                        ELSE 1.00
                    END
                ELSE rlc.LawyerYIP
             END as yearinpracticefactor,   
            --COALESCE(rlc.LawyerYIP, 1.00) as yearinpracticefactor,
            NULL as yearsinpracticefactoror,
            COALESCE(rlc.LawyerYIP, 1.00) as yearsinpracticefactorsys,
            NULL as yearsinpracticeor,
            CASE
                WHEN TO_INT(TO_CHAR(p.effectiveDate, 'YYYY')) - TO_INT(TO_CHAR(COALESCE(L.inPracticeDate, lsb.barAdmitDate), 'YYYY')) > 0 THEN TO_INT(TO_CHAR(p.effectiveDate, 'YYYY')) - TO_INT(TO_CHAR(COALESCE(L.inPracticeDate, lsb.barAdmitDate), 'YYYY'))
                ELSE 0
            END as yearsinpracticesys  ---should really use date lawyer added to policy
    --    from ISI_InsuredsView iv
        ----->>>>>>> should have same number of i_attorneys1 then p_item   USE p_items as startin point
        -- from ISI_ItemLawyerView iv
        /* LP Update view to select from ISI_ItemLawyerWithDeleteIndView */
        FROM ISI_ItemLawyerWithDeleteIndView iv
            left join Policy p on p.policyNumber = iv.POLICYINT
                left join Application a on a.applicationId = p.policyId
                    inner join Quote q on q.applicationId = a.applicationId 
                        left join QuoteCoverage qc on qc.quoteId = q.quoteId and qc.perClaimLimit = p.perClaimLimit and qc.aggregateLimit = p.aggregateLimit and qc.perClaimDeductible = p.perClaimDeductible
                        left join RatingLawyerCrosstab rlc on rlc.quoteBookId = q.mainQuoteBookId and rlc.lawyerId = iv.LAWYERID
            left join Lawyer l on l.lawyerId = iv.LAWYERID
              /* The same alias being used */
                left join Person p1 on p1.personId = l.lawyerId   
                left join LawyerStateBar lsb on lsb.lawyerId = l.lawyerId         
        where iv.LAWYERID is not NULL
    )
    select
        LEAST(attorneyfactor1, yearinpracticefactor, retrodatefactor, parttimefactor) as attorneyfactor,
        A1.*
    from A1 A1

    order by POLICYINT, transactioneffectivedate, attorneynameid_ek
 ;
 
 /*-- Testing --*/
select count(*) fro``m ISI_Attorney1View;
select count(*) from ISI_QuoteLimitAttorney1View;
select * from RatingLawyerCrosstab where lawyerId in ('KIM_JULI'); 
select * from Lawyer;
select * from PremiumPerLawyerView where firmId in ('KIMLAV');
select * from Policy where firmId in ('KIMLAV');
select * from PolicyLawyerCoverageAllView;
select * from PolicyLawyerCoverage;
select * from ISI_InsuredsView;
select * from ISI_ItemFirmView where LAWYERID is not NULL;
select * from ISI_PolicyView;
select * from Application where firmId in ('KIMLAV') order by effectiveDate;
select * from RatingCrosstab where firmId in ('KIMLAV') order by effectiveDate;
select rlc.* from RatingCrosstab RC left join RatingLimitCrosstab rlc on rlc.quoteBookId = Rc.quoteBookId where Rc.firmId in ('KIMLAV') order by rlc.quoteBookId, rlc.quoteCoverageId;
select * from Quote;
select * from QuoteBook;
select * from QuoteCoverage;
select * from RatingLimitCrosstab order by quoteBookId, quoteCoverageId;

--------------------------------------------------------------------------------
/*
    ISI_CoverageView
    p_coverage
    
    Dependancies: ISI_ItemView
    
    NOTES:
    12/27/2017  May need to split this view into 3 views due to performance hit caused by PolicyCoverageLawyerAllView.
                PLus, tail coverage is not yet extracted.
    03/20/2018  annualpremiums and transactionpremium only on transactionseqno=1
    
    History
    12/17/2017  FZ  Created
    04/04/2018  FZ  split int respective coverages
*/
create or replace view ISI_CoverageLPLFirmView as 
    --LPL coverage firm
    select
        iv.conversionreference as conversionreference,
        iv.transactiontype as transactiontype,
        iv.transactioneffectivedate as transactioneffectivedate,
        iv.transactionseqno as transactionseqno,
        p.policyNumber as POLICYINT,
        iv.EVENTTYPEID as EVENTTYPEID,
        CASE
            WHEN iv.transactiontype = 'ERP' AND iv.extendreporttermlength is NULL THEN 'LPL__FIRM__WLMEU16_WI_19000101_20991231_4112'
            WHEN iv.transactiontype = 'ERP' AND iv.extendreporttermlength = 1 THEN 'LPL__FIRM__WLMEC16-1_WI_19000101_20991231_4112'
            WHEN iv.transactiontype = 'ERP' AND iv.extendreporttermlength = 2 THEN 'LPL__FIRM__WLMEC16-2_WI_19000101_20991231_4112'
            WHEN iv.transactiontype = 'ERP' AND iv.extendreporttermlength = 3 THEN 'LPL__FIRM__WLMEC16-3_WI_19000101_20991231_4112'
            WHEN iv.transactiontype = 'ERP' AND iv.extendreporttermlength = 6 THEN 'LPL__FIRM__WLMEC16-6_WI_19000101_20991231_4112'
            WHEN iv.transactiontype = 'END' AND iv.EVENTTYPEID = 'Cvg-DF' THEN 'LPL__FIRM__WLMEND20_WI_19000101_20180301_4112'  --DF
            WHEN iv.transactiontype = 'END' AND iv.EVENTTYPEID = 'Cvg-GC' THEN 'LPL__FIRM__WLMEND28_WI_19000101_20180301_4112'  --GC
            WHEN iv.transactiontype = 'END' AND iv.EVENTTYPEID = 'Cvg-SC' THEN 'LPL__FIRM__WLMEND01_WI_19000101_20991231_4112'  --SEC
            WHEN iv.transactiontype = 'END' AND iv.EVENTTYPEID = 'Cvg-EL' THEN 'LPL__FIRM__WLMEND03_WI_19000101_20991231_4112'  --exclude other entities
            WHEN iv.transactiontype = 'END' AND iv.EVENTTYPEID = 'Cvg-AT' THEN 'LPL__FIRM__WLMEND24_WI_19000101_20991231_4112'  --abstracter
            WHEN iv.transactiontype = 'END' AND iv.EVENTTYPEID = 'Cvg-EV' THEN 'LPL__FIRM__WLMEND25_WI_19000101_20991231_4112'  --exclude vicarious liability
            WHEN iv.transactiontype = 'END' AND iv.EVENTTYPEID = 'Cvg-MN' THEN 'LPL__FIRM__WLMENDMN_WI_19000101_20991231_4112'  --manuascript
            WHEN iv.transactiontype = 'END' AND iv.EVENTTYPEID = 'Cvg-AB' THEN 'LPL__FIRM__WLMEND30_WI_20180101_20991231_4112'  --additional benefits
            ELSE 'LPL__FIRM__PL_0_WI_19000101_20991231_4112'  --firm
        END AS pcmmappingkey,
        iv.sbuid as sbuid,
        0 as packageid,
        iv.riskid as riskid,
        0 as linenumber,
        CASE
            WHEN iv.transactiontype = 'ERP' AND iv.extendreporttermlength is NULL THEN 'WLMEU16'
            WHEN iv.transactiontype = 'ERP' AND iv.extendreporttermlength = 1 THEN 'WLMEC16-1'
            WHEN iv.transactiontype = 'ERP' AND iv.extendreporttermlength = 2 THEN 'WLMEC16-2'
            WHEN iv.transactiontype = 'ERP' AND iv.extendreporttermlength = 3 THEN 'WLMEC16-3'
            WHEN iv.transactiontype = 'ERP' AND iv.extendreporttermlength = 6 THEN 'WLMEC16-6'
            WHEN iv.transactiontype = 'END' AND iv.EVENTTYPEID = 'Cvg-DF' THEN 'WLMEND20' --DF
            WHEN iv.transactiontype = 'END' AND iv.EVENTTYPEID = 'Cvg-GC' THEN 'WLMEND28'  --GC
            WHEN iv.transactiontype = 'END' AND iv.EVENTTYPEID = 'Cvg-SC' THEN 'WLMEND01'  --SEC
            WHEN iv.transactiontype = 'END' AND iv.EVENTTYPEID = 'Cvg-EL' THEN 'WLMEND03'  --exclude other entities
            WHEN iv.transactiontype = 'END' AND iv.EVENTTYPEID = 'Cvg-AT' THEN 'WLMEND24'  --abstracter
            WHEN iv.transactiontype = 'END' AND iv.EVENTTYPEID = 'Cvg-EV' THEN 'WLMEND25'  --exclude vicarious liability
            WHEN iv.transactiontype = 'END' AND iv.EVENTTYPEID = 'Cvg-MN' THEN 'WLMENDMN'  --manuascript
            WHEN iv.transactiontype = 'END' AND iv.EVENTTYPEID = 'Cvg-AB' THEN 'WLMEND30'  --additional benefits
            ELSE 'PL_0' 
        END as coverageid,
        CAST(NULL as VARCHAR(4)) as basisofsettlement,
        CAST(NULL as VARCHAR(8)) as coinsurance,
        CAST(NULL as VARCHAR(20)) as rategroup,
        CAST(NULL as VARCHAR(20)) as drivingrecord,
        CAST(NULL as INT) as rateper,
        CAST(NULL as INT) as baseratefactor,
        CAST(NULL as INT) as overrideratefactor,
        p.perClaimLimit as limit1,
        p.aggregateLimit as limit2,
        'USD' as limitcurrencycd,
        p.perClaimDeductible as deductible1,
        0 as deductible2,
        'USD' as premiumcurrencycd,
        CAST(NULL as INT) as ratedbasepremium,
        CAST(NULL as INT) as ratedgrosspremium,
        CAST(NULL as INT) as ratednetpremium, 
        /*
        CASE
            WHEN iv.transactionseqno = 1 THEN COALESCE(pplv.premiumPerLawyer1M,0) + COALESCE(pplv.premiumPerLawyerXS5M,0) + COALESCE(pplv.premiumPerLawyerXS10M,0) 
            ELSE 0
        END as annualpremium,
        */
        0 as annualpremium,
        /*
        CASE
            WHEN iv.transactionseqno = 1 THEN COALESCE(pplv.premiumPerLawyer1M,0) + COALESCE(pplv.premiumPerLawyerXS5M,0) + COALESCE(pplv.premiumPerLawyerXS10M,0) 
            ELSE 0
        END as transactionpremium,
        */
        0 as transactionpremium,
        0 as nonpremiumsurchargeamount,
        0 as premiumsurchargeamount,
        0 as transactionsurcharge,
        0 as nonpremiumdiscountamount,
        0 as premiumdiscountamount,
        0 as transactiondiscount,
        0 as commissionamount,
        CAST(NULL as VARCHAR(4)) as numberof1,
        CAST(NULL as VARCHAR(4)) as valueof1,
        CAST(NULL as VARCHAR(4)) as typeof1,
        CAST(NULL as VARCHAR(4)) as numberof2,
        CAST(NULL as VARCHAR(4)) as valueof2,
        CAST(NULL as VARCHAR(4)) as typeof2,
        CASE 
            WHEN (TO_CHAR(y.expirationDate, 'YYYYMMDD') <> TO_CHAR(p.expirationDate, 'YYYYMMDD')) and (TO_CHAR(y.expirationDate, 'YYYYMMDD') <= iv.transactioneffectivedate) THEN 1
            ELSE 0
        END as deletedrow,
        CAST(NULL as VARCHAR(4)) as conversionserver,
        CAST(NULL as VARCHAR(4)) as sequencenumber,
        iv.veffectivedate as veffectivedate,
        iv.vexpirydate as vexpirydate,
        iv.claimsmadeind as claimsmadeind,
        COALESCE(iv.retrodate, TO_CHAR(p.effectiveDate, 'YYYYMMDD')) as retrodate,
        CAST(NULL as INT) as postedwweobjectid,
        CAST(NULL as VARCHAR(4)) as limittierinfo,
        -- only added to transactionseqno=1
        /*
        CASE
            WHEN iv.transactionseqno = 1 THEN COALESCE(pplv.premiumPerLawyer1M,0) + COALESCE(pplv.premiumPerLawyerXS5M,0) + COALESCE(pplv.premiumPerLawyerXS10M,0) 
            ELSE 0
        END as ratedftbasepremium,
        */
        0 as ratedftbasepremium,
        /*
        CASE
            WHEN iv.transactionseqno = 1 THEN COALESCE(pplv.premiumPerLawyer1M,0) + COALESCE(pplv.premiumPerLawyerXS5M,0) + COALESCE(pplv.premiumPerLawyerXS10M,0) 
            ELSE 0
        END as ratedftgrosspremium,
        */
        0 as ratedftgrosspremium,
        /*
        CASE
            WHEN iv.transactionseqno = 1 THEN COALESCE(pplv.premiumPerLawyer1M,0) + COALESCE(pplv.premiumPerLawyerXS5M,0) + COALESCE(pplv.premiumPerLawyerXS10M,0) 
            ELSE 0
        END as ratedftpremium,
        */
        0 as ratedftpremium,
        /*
        CASE
            WHEN iv.transactionseqno = 1 THEN COALESCE(pplv.premiumPerLawyer1M,0) + COALESCE(pplv.premiumPerLawyerXS5M,0) + COALESCE(pplv.premiumPerLawyerXS10M,0) 
            ELSE 0
        END as ratedannualpremium,
        */
        0 as ratedannualpremium,
        /*
        CASE
            WHEN iv.transactionseqno = 1 THEN COALESCE(pplv.premiumPerLawyer1M,0) + COALESCE(pplv.premiumPerLawyerXS5M,0) + COALESCE(pplv.premiumPerLawyerXS10M,0) 
            ELSE 0
        END as ratedtransactionpremium,
        */
        0 as ratedtransactionpremium,
        0 as ratedcommissionpercent,
        0 as ratedcommissionamount,
        --COALESCE(pplv.premiumPerLawyer1M,0) + COALESCE(pplv.premiumPerLawyerXS5M,0) + COALESCE(pplv.premiumPerLawyerXS10M,0) as ftpremium,
        0 as ftpremium,
        0 as offsetftpremium,
        1 as commissiontype,
        0 as commissionpercent,
        CAST(NULL as INT) as transactionpremiumfactor,  --04/19/2018 previously 1.00000
        0 as transactionpremiumdlyrate,
        CAST(NULL as INT) as overriderule,
        CAST(NULL as INT) as overrideftpremium,
        CAST(NULL as INT) as overrideannualpremium,
        CAST(NULL as INT) as overridetransactionpremium,
        CAST(NULL as INT) as overridecommissionpercent,
        CAST(NULL as INT) as overridecommissionamount,
        0 as taxamount,
        CAST(NULL as VARCHAR(4)) as gldiv,
        CAST(NULL as VARCHAR(4)) as gldept,
        CAST(NULL as VARCHAR(4)) as gllob,
        0 as siretention1,
        0 as siretention2,
        CAST(NULL as VARCHAR(4)) as mappingcode,
        CAST(NULL as VARCHAR(4)) as submappingcode,
        CAST(NULL as VARCHAR(4)) as submappingcode2,
        CAST(NULL as INT) as netrate,
        CAST(NULL as INT) as grossrate,
        CAST(NULL as VARCHAR(4)) as rateexposure,
        0 as adjbilltypeind,
        CAST(NULL as INT) as waivedtransactionpremium,
        CAST(NULL as VARCHAR(4)) as classcode,
        CAST(NULL as VARCHAR(4)) as iso_statecode,
        CAST(NULL as VARCHAR(4)) as iso_zipcode,
        CAST(NULL as VARCHAR(4)) as iso_ratingterritory,
        CAST(NULL as VARCHAR(4)) as iso_typeofpolicycode,
        CAST(NULL as VARCHAR(4)) as iso_aslobstatcode,
        CAST(NULL as VARCHAR(4)) as iso_sublinecode,
        CAST(NULL as VARCHAR(4)) as iso_classificationcode,
        CAST(NULL as VARCHAR(4)) as iso_coveragestatcode,
        CAST(NULL as VARCHAR(4)) as iso_ratingidentificationcode,
        CAST(NULL as VARCHAR(4)) as iso_constructioncode,
        CAST(NULL as VARCHAR(4)) as iso_fireprotectioncode,
        CAST(NULL as VARCHAR(4)) as iso_terrorismcoveragecode,
        CAST(NULL as VARCHAR(4)) as iso_windhaildedcode,
        CAST(NULL as VARCHAR(4)) as iso_bcegclass,
        CAST(NULL as VARCHAR(4)) as iso_ratingbasiscode,
        CAST(NULL as VARCHAR(4)) as iso_liabilityformcode,
        CAST(NULL as VARCHAR(4)) as iso_molddamagecode,
        CAST(NULL as VARCHAR(4)) as iso_liabilityexposureindcode,
        CAST(NULL as INT) as iso_exposurestatamount,
        CAST(NULL as VARCHAR(4)) as iso_ratingmodificationfactor,
        CAST(NULL as VARCHAR(4)) as iso_stateexceptionindcode,
        CAST(NULL as VARCHAR(4)) as iso_bussincomeexpensecode,
        CAST(NULL as VARCHAR(4)) as iso_liabcovindcode,
        CAST(NULL as VARCHAR(4)) as iso_pctowneroccupied,
        CAST(NULL as VARCHAR(4)) as iso_classcodedesc,
        TO_CHAR(p.effectiveDate, 'YYYYMMDD') as rateeffectivedate,
        'FL' as deductibletype,
        CAST(NULL as VARCHAR(4)) as iso_formcode,
        CAST(NULL as VARCHAR(4)) as iso_losscostmultiplier,
        CAST(NULL as VARCHAR(4)) as iso_losscostdate,
        CAST(NULL as VARCHAR(4)) as iso_yearofconstruction,
        TO_CHAR(TO_INT(ORA_HASH(iv.POLICYINT)))||TO_CHAR(TO_INT(ORA_HASH('PL_0'))) as ouid  --unique to coverage 
    from (select * from ISI_ItemFirmView where EVENTTYPEID not in ('Cvg-PA', 'Cvg-AA', 'Cvg-PE')) iv
        left join PolicyLawyerCoverageAllView y on y.policyNumber = iv.POLICYINT and y.lawyerId = iv.LAWYERID and (TO_CHAR(y.effectiveDate, 'YYYYMMDD') <= iv.transactioneffectivedate)
        left join Policy p on p.policyNumber = iv.POLICYINT
    order by POLICYINT, transactioneffectivedate, riskid, coverageid
;

/*-- NEEDED for PERFORMANCE - Claims --*/
drop table ISI_CoverageLPLFirm;
--insert into ISI_CoverageLPLLawyer (select * from ISI_CoverageLPLLawyerView);
create table ISI_CoverageLPLFirm as
    (select * from ISI_CoverageLPLFirmView);
select count(*) from ISI_CoverageLPLFirm;

-- view carry-forwards firm''s prior transaction record(s) (ie: 1, 2-1, 3-2-1)
    create or replace view ISI_CoverageLPLFirmAllView as
with cov1 as (
select 
    C1.* 
from ISI_CoverageLPLFirmView C1
)
    select 
        c2.transactiontype as new_transactiontype,
        c2.transactioneffectivedate as new_transactioneffectivedate,
        c2.transactionseqno as new_transactionseqno,    
        c1.*
    from (select * from cov1 where transactiontype not in ('RCA', 'CAN', 'ERP')) c1
        left join (select * from cov1 where transactiontype not in ('RCA', 'CAN', 'ERP')) c2 on c2.POLICYINT = c1.POLICYINT and c2.transactioneffectivedate = c1.transactioneffectivedate and c2.transactionseqno >= c1.transactionseqno 
    union all
    select 
        c1.transactiontype as new_transactiontype,
        c1.transactioneffectivedate as new_transactioneffectivedate,
        c1.transactionseqno as new_transactionseqno,    
        c1.*
    from (select * from cov1 where transactiontype in ('RCA', 'CAN', 'ERP')) c1
--order by POLICYINT, transactioneffectivedate, transactionseqno
;

select * from ISI_CoverageLPLFirmAllView;


select * from ISI_CoverageLPLLawyerView where conversionreference is NULL or transactiontype is NULL or transactioneffectivedate is null 
  or transactionseqno is null or POLICYINT is null or EVENTTYPEID is null or riskid is null
  or limit1 is null or limit2 is null or deductible1 is null or veffectivedate is null or vexpirydate is null
  or claimsmadeind is null or postedwweobjectid is null or rateeffectivedate is null;
--
/* 06/05/2018  LP Update view to select from ISI_ItemLawyerWithDeleteIndView */
/* 06/05/2018  LP Added support for carrying forward coverages */
create or replace view ISI_CoverageLPLLawyerView as 
    --LPL coverage lawyer
    SELECT DISTINCT
        iv.conversionreference as conversionreference,
        iv.transactiontype as transactiontype,
        iv.transactioneffectivedate as transactioneffectivedate,
        iv.transactionseqno as transactionseqno,
        p.policyNumber as POLICYINT,
        iv.EVENTTYPEID as EVENTTYPEID,
        /* LP Use Carry Forward Join */
        CASE
            WHEN iv.transactiontype = 'END' AND carry_forward_cov.EVENTTYPEID = 'Cvg-DF' THEN 'LPL__ATTORNEY__WLMEND20_WI_19000101_20180301_4112'  --DF
            WHEN iv.transactiontype = 'END' AND carry_forward_cov.EVENTTYPEID = 'Cvg-GC' THEN 'LPL__ATTORNEY__WLMEND28_WI_19000101_20180301_4112'  --GC
            --WHEN iv.transactiontype = 'END' AND carry_forward_cov.EVENTTYPEID = 'Cvg-SC' THEN 'LPL__ATTORNEY__WLMEND01_WI_19000101_20991231_4112'  --SEC
            WHEN iv.transactiontype = 'END' AND carry_forward_cov.EVENTTYPEID = 'Cvg-EL' THEN 'LPL__ATTORNEY__WLMEND03_WI_19000101_20991231_4112'  --exclude other entities
            WHEN iv.transactiontype = 'END' AND carry_forward_cov.EVENTTYPEID = 'Cvg-AT' THEN 'LPL__ATTORNEY__WLMEND24_WI_19000101_20991231_4112'  --abstracter
            WHEN iv.transactiontype = 'END' AND carry_forward_cov.EVENTTYPEID = 'Cvg-EV' THEN 'LPL__ATTORNEY__WLMEND25_WI_19000101_20991231_4112'  --exclude vicarious liability
            WHEN iv.transactiontype = 'END' AND carry_forward_cov.EVENTTYPEID = 'Cvg-MN' THEN 'LPL__ATTORNEY__WLMENDMN_WI_19000101_20991231_4112'  --manuascript
            WHEN iv.transactiontype = 'END' AND carry_forward_cov.EVENTTYPEID = 'Cvg-AB' THEN 'LPL__ATTORNEY__WLMEND30_WI_20180101_20991231_4112'  --additional benefits
            ELSE 'LPL__ATTORNEY__PL_0_WI_19000101_20991231_4112' --attorney
        END AS pcmmappingkey,
        iv.sbuid as sbuid,
        0 as packageid,
        iv.riskid as riskid,
        0 as linenumber,
        /* LP Use Carry Forward Join */
        CASE
            WHEN iv.transactiontype = 'END' AND carry_forward_cov.EVENTTYPEID = 'Cvg-DF' THEN 'WLMEND20'  --DF
            WHEN iv.transactiontype = 'END' AND carry_forward_cov.EVENTTYPEID = 'Cvg-GC' THEN 'WLMEND28'  --GC
            --WHEN iv.transactiontype = 'END' AND carry_forward_cov.EVENTTYPEID = 'Cvg-SC' THEN 'WLMEND01'  --SEC
            WHEN iv.transactiontype = 'END' AND carry_forward_cov.EVENTTYPEID = 'Cvg-EL' THEN 'WLMEND03'  --exclude other entities
            WHEN iv.transactiontype = 'END' AND carry_forward_cov.EVENTTYPEID = 'Cvg-AT' THEN 'WLMEND24'  --abstracter
            WHEN iv.transactiontype = 'END' AND carry_forward_cov.EVENTTYPEID = 'Cvg-EV' THEN 'WLMEND25'  --exclude vicarious liability
            WHEN iv.transactiontype = 'END' AND carry_forward_cov.EVENTTYPEID = 'Cvg-MN' THEN 'WLMENDMN'  --manuascript
            WHEN iv.transactiontype = 'END' AND carry_forward_cov.EVENTTYPEID = 'Cvg-AB' THEN 'WLMEND30'  --additional benefits
            ELSE 'PL_0' 
        END as coverageid,
        CAST(NULL as VARCHAR(4)) as basisofsettlement,
        CAST(NULL as VARCHAR(8)) as coinsurance,
        CAST(NULL as VARCHAR(20)) as rategroup,
        CAST(NULL as VARCHAR(20)) as drivingrecord,
        CAST(NULL as INT) as rateper,
        CAST(NULL as INT) as baseratefactor,
        CAST(NULL as INT) as overrideratefactor,
        CASE
            WHEN carry_forward_cov.EVENTTYPEID IN ('PolicyIssue','Cvg-AA','Cvg-DA') THEN p.perClaimLimit
            WHEN carry_forward_cov.EVENTTYPEID = 'Cvg-GC' THEN 5000
            ELSE 0 
        END as limit1,
        CASE
            WHEN carry_forward_cov.EVENTTYPEID IN ('PolicyIssue','Cvg-AA','Cvg-DA') THEN p.aggregateLimit
            ELSE 0 
        END as limit2,
        'USD' as limitcurrencycd,
        CASE
            WHEN carry_forward_cov.EVENTTYPEID IN ('PolicyIssue','Cvg-AA','Cvg-DA') THEN p.perClaimDeductible
            ELSE 0 
        END as deductible1,
        0 as deductible2,
        'USD' as premiumcurrencycd,
        CAST(NULL as INT) as ratedbasepremium,
        CAST(NULL as INT) as ratedgrosspremium,
        CAST(NULL as INT) as ratednetpremium, 
        CASE
            -- remove DF and prorate based on 
            WHEN iv.transactiontype in ('NEW', 'REN', 'RWN', 'RWR') AND ((COALESCE(pplv.premiumPerLawyer1M,0) + COALESCE(pplv.premiumPerLawyerXS5M,0) + COALESCE(pplv.premiumPerLawyerXS10M,0) - COALESCE(pplv.DefReimbCoverage,0) * 12) <> 0) THEN (COALESCE(pplv.premiumPerLawyer1M,0) + COALESCE(pplv.premiumPerLawyerXS5M,0) + COALESCE(pplv.premiumPerLawyerXS10M,0) - COALESCE(pplv.DefReimbCoverage,0) * 12/COALESCE(iv.POLICYTERM,12))
            WHEN iv.transactiontype = 'END' and iv.EVENTTYPEID = 'Cvg-DF' THEN COALESCE(pplv.DefReimbCoverage,0)
            --WHEN iv.transactiontype = 'END' and iv.EVENTTYPEID = 'Cvg-SC' THEN COALESCE(pplv.SECCoverage, 0)
            ELSE COALESCE(pa.premiumAdjust, 0)
        END as annualpremium,
        CASE
            WHEN iv.transactiontype in ('NEW', 'REN', 'RWN', 'RWR') THEN COALESCE(pplv.premiumPerLawyer1M,0) + COALESCE(pplv.premiumPerLawyerXS5M,0) + COALESCE(pplv.premiumPerLawyerXS10M,0) - COALESCE(pplv.DefReimbCoverage,0)
            WHEN iv.transactiontype = 'END' and iv.EVENTTYPEID = 'Cvg-DF' THEN COALESCE(pplv.DefReimbCoverage,0)
            --WHEN iv.transactiontype = 'END' and iv.EVENTTYPEID = 'Cvg-SC' THEN COALESCE(pplv.SECCoverage, 0)
            ELSE COALESCE(pa.premiumAdjust, 0)
        END as transactionpremium,
        0 as nonpremiumsurchargeamount,
        0 as premiumsurchargeamount,
        0 as transactionsurcharge,
        0 as nonpremiumdiscountamount,
        0 as premiumdiscountamount,
        0 as transactiondiscount,
        0 as commissionamount,
        CAST(NULL as VARCHAR(4)) as numberof1,
        CAST(NULL as VARCHAR(4)) as valueof1,
        CAST(NULL as VARCHAR(4)) as typeof1,
        CAST(NULL as VARCHAR(4)) as numberof2,
        CAST(NULL as VARCHAR(4)) as valueof2,
        CAST(NULL as VARCHAR(4)) as typeof2,
        iv.deletedrow,
        CAST(NULL as VARCHAR(4)) as conversionserver,
        CAST(NULL as VARCHAR(4)) as sequencenumber,
        iv.veffectivedate as veffectivedate,
        iv.vexpirydate as vexpirydate,
        iv.claimsmadeind as claimsmadeind,
        COALESCE(iv.retrodate, TO_CHAR(p.effectiveDate, 'YYYYMMDD')::NUMERIC) as retrodate,
        CAST(NULL as INT) as postedwweobjectid,
        CAST(NULL as VARCHAR(4)) as limittierinfo,
        -- only added to transactionseqno=1
        CASE
            WHEN iv.transactiontype in ('NEW', 'REN', 'RWN', 'RWR') THEN COALESCE(pplv.premiumPerLawyer1M,0) + COALESCE(pplv.premiumPerLawyerXS5M,0) + COALESCE(pplv.premiumPerLawyerXS10M,0) - COALESCE(pplv.DefReimbCoverage, 0) 
            WHEN iv.transactiontype = 'END' and iv.EVENTTYPEID = 'Cvg-DF' THEN COALESCE(pplv.DefReimbCoverage,0)
            --WHEN iv.transactiontype = 'END' and iv.EVENTTYPEID = 'Cvg-SC' THEN COALESCE(pplv.SECCoverage, 0)
            ELSE COALESCE(pa.premiumAdjust, 0)
        END as ratedftbasepremium,
        CASE
            WHEN iv.transactiontype in ('NEW', 'REN', 'RWN', 'RWR') THEN COALESCE(pplv.premiumPerLawyer1M,0) + COALESCE(pplv.premiumPerLawyerXS5M,0) + COALESCE(pplv.premiumPerLawyerXS10M,0) - COALESCE(pplv.DefReimbCoverage, 0) 
            WHEN iv.transactiontype = 'END' and iv.EVENTTYPEID = 'Cvg-DF' THEN COALESCE(pplv.DefReimbCoverage,0)
            --WHEN iv.transactiontype = 'END' and iv.EVENTTYPEID = 'Cvg-SC' THEN COALESCE(pplv.SECCoverage, 0)
            ELSE COALESCE(pa.premiumAdjust, 0)
        END as ratedftgrosspremium,
        CASE
            WHEN iv.transactiontype in ('NEW', 'REN', 'RWN', 'RWR') THEN COALESCE(pplv.premiumPerLawyer1M,0) + COALESCE(pplv.premiumPerLawyerXS5M,0) + COALESCE(pplv.premiumPerLawyerXS10M,0) - COALESCE(pplv.DefReimbCoverage, 0) 
            WHEN iv.transactiontype = 'END' and iv.EVENTTYPEID = 'Cvg-DF' THEN COALESCE(pplv.DefReimbCoverage,0)
            --WHEN iv.transactiontype = 'END' and iv.EVENTTYPEID = 'Cvg-SC' THEN COALESCE(pplv.SECCoverage, 0)
            ELSE COALESCE(pa.premiumAdjust, 0)
        END as ratedftpremium,
        CASE
            WHEN iv.transactiontype in ('NEW', 'REN', 'RWN', 'RWR') THEN COALESCE(pplv.premiumPerLawyer1M,0) + COALESCE(pplv.premiumPerLawyerXS5M,0) + COALESCE(pplv.premiumPerLawyerXS10M,0) - COALESCE(pplv.DefReimbCoverage, 0) 
            WHEN iv.transactiontype = 'END' and iv.EVENTTYPEID = 'Cvg-DF' THEN COALESCE(pplv.DefReimbCoverage,0)
            --WHEN iv.transactiontype = 'END' and iv.EVENTTYPEID = 'Cvg-SC' THEN COALESCE(pplv.SECCoverage, 0)
            ELSE COALESCE(pa.premiumAdjust, 0)
        END as ratedannualpremium,
        CASE
            WHEN iv.transactiontype in ('NEW', 'REN', 'RWN', 'RWR')  THEN COALESCE(pplv.premiumPerLawyer1M,0) + COALESCE(pplv.premiumPerLawyerXS5M,0) + COALESCE(pplv.premiumPerLawyerXS10M,0) 
            WHEN iv.transactiontype = 'END' and iv.EVENTTYPEID = 'Cvg-DF' THEN COALESCE(pplv.DefReimbCoverage,0)
            --WHEN iv.transactiontype = 'END' and iv.EVENTTYPEID = 'Cvg-SC' THEN COALESCE(pplv.SECCoverage, 0)
            ELSE COALESCE(pa.premiumAdjust, 0)
        END as ratedtransactionpremium,
        0 as ratedcommissionpercent,
        0 as ratedcommissionamount,
        CASE
            WHEN iv.transactiontype in ('NEW', 'REN', 'RWN', 'RWR') THEN COALESCE(pplv.premiumPerLawyer1M,0) + COALESCE(pplv.premiumPerLawyerXS5M,0) + COALESCE(pplv.premiumPerLawyerXS10M,0) - COALESCE(pplv.DefReimbCoverage, 0) 
            WHEN iv.transactiontype = 'END' and iv.EVENTTYPEID = 'Cvg-DF' THEN COALESCE(pplv.DefReimbCoverage,0)
            --WHEN iv.transactiontype = 'END' and iv.EVENTTYPEID = 'Cvg-SC' THEN COALESCE(pplv.SECCoverage, 0)
            ELSE COALESCE(pa.premiumAdjust, 0)
        END as ftpremium,
        0 as offsetftpremium,
        1 as commissiontype,
        0 as commissionpercent,
        1.0000000 as transactionpremiumfactor,
        0 as transactionpremiumdlyrate,
        CAST(NULL as INT) as overriderule,
        CAST(NULL as INT) as overrideftpremium,
        CAST(NULL as INT) as overrideannualpremium,
        CAST(NULL as INT) as overridetransactionpremium,
        CAST(NULL as INT) as overridecommissionpercent,
        CAST(NULL as INT) as overridecommissionamount,
        0 as taxamount,
        CAST(NULL as VARCHAR(4)) as gldiv,
        CAST(NULL as VARCHAR(4)) as gldept,
        CAST(NULL as VARCHAR(4)) as gllob,
        0 as siretention1,
        0 as siretention2,
        CAST(NULL as VARCHAR(4)) as mappingcode,
        CAST(NULL as VARCHAR(4)) as submappingcode,
        CAST(NULL as VARCHAR(4)) as submappingcode2,
        CAST(NULL as INT) as netrate,
        CAST(NULL as INT) as grossrate,
        CAST(NULL as VARCHAR(4)) as rateexposure,
        0 as adjbilltypeind,
        CAST(NULL as INT) as waivedtransactionpremium,
        CAST(NULL as VARCHAR(4)) as classcode,
        CAST(NULL as VARCHAR(4)) as iso_statecode,
        CAST(NULL as VARCHAR(4)) as iso_zipcode,
        CAST(NULL as VARCHAR(4)) as iso_ratingterritory,
        CAST(NULL as VARCHAR(4)) as iso_typeofpolicycode,
        CAST(NULL as VARCHAR(4)) as iso_aslobstatcode,
        CAST(NULL as VARCHAR(4)) as iso_sublinecode,
        CAST(NULL as VARCHAR(4)) as iso_classificationcode,
        CAST(NULL as VARCHAR(4)) as iso_coveragestatcode,
        CAST(NULL as VARCHAR(4)) as iso_ratingidentificationcode,
        CAST(NULL as VARCHAR(4)) as iso_constructioncode,
        CAST(NULL as VARCHAR(4)) as iso_fireprotectioncode,
        CAST(NULL as VARCHAR(4)) as iso_terrorismcoveragecode,
        CAST(NULL as VARCHAR(4)) as iso_windhaildedcode,
        CAST(NULL as VARCHAR(4)) as iso_bcegclass,
        CAST(NULL as VARCHAR(4)) as iso_ratingbasiscode,
        CAST(NULL as VARCHAR(4)) as iso_liabilityformcode,
        CAST(NULL as VARCHAR(4)) as iso_molddamagecode,
        CAST(NULL as VARCHAR(4)) as iso_liabilityexposureindcode,
        CAST(NULL as INT) as iso_exposurestatamount,
        CAST(NULL as VARCHAR(4)) as iso_ratingmodificationfactor,
        CAST(NULL as VARCHAR(4)) as iso_stateexceptionindcode,
        CAST(NULL as VARCHAR(4)) as iso_bussincomeexpensecode,
        CAST(NULL as VARCHAR(4)) as iso_liabcovindcode,
        CAST(NULL as VARCHAR(4)) as iso_pctowneroccupied,
        CAST(NULL as VARCHAR(4)) as iso_classcodedesc,
        TO_CHAR(p.effectiveDate, 'YYYYMMDD') as rateeffectivedate,
        'FL' as deductibletype,
        CAST(NULL as VARCHAR(4)) as iso_formcode,
        CAST(NULL as VARCHAR(4)) as iso_losscostmultiplier,
        CAST(NULL as VARCHAR(4)) as iso_losscostdate,
        CAST(NULL as VARCHAR(4)) as iso_yearofconstruction,
        TO_CHAR(TO_INT(ORA_HASH(iv.POLICYINT)))||TO_CHAR(TO_INT(ORA_HASH('PL_0'))) as ouid  --unique to coverage         
    from (select * from ISI_ItemLawyerView where transactiontype <> 'ERP') iv
        
        left join PolicyLawyerCoverageAllView y on y.policyNumber = iv.POLICYINT and y.lawyerId = iv.LAWYERID -- and (TO_CHAR(y.effectiveDate, 'YYYYMMDD') <= iv.transactioneffectivedate)
        left join Policy p on p.policyNumber = iv.POLICYINT
            left join PremiumPerLawyerView pplv on pplv.policyId = p.policyId and pplv.effectiveDate = p.effectiveDate and pplv.lawyerId = iv.LAWYERID and pplv.aggregateLimit = p.aggregateLimit and pplv."Limit" = p.perClaimLimit and pplv.Deductible = p.perClaimDeductible
        left join PolicyAccounting pa on pa.coverageId = iv.COVERAGEID and iv.transactiontype in ('END', 'CAN', 'RCA')
        /* LP Carry Forward Joins */
        LEFT JOIN (select * from ISI_ItemLawyerView where transactiontype <> 'ERP') carry_forward_cov
          ON iv.policyint = carry_forward_cov.policyint
              AND iv.lawyerid = carry_forward_cov.lawyerid
              AND (iv.transactioneffectivedate::BIGINT * 100) + iv.transactionseqno >= (carry_forward_cov.transactioneffectivedate::BIGINT * 100) + carry_forward_cov.transactionseqno
    --- FIX
    --where iv.EVENTTYPEID = 'Coverage' and iv.POLICYINT = '20090701' and iv.transactioneffectivedate = '20090701' and iv.transactionseqno = 1
    --WHERE iv.POLICYINT = '0607190' 
    order by POLICYINT, transactioneffectivedate, transactionseqno, riskid, coverageid
;
select count(*) from ISI_CoverageLPLLawyerView;
select * from ISI_CoverageLPLLawyerView;


/* 
-- MUST DO for PERFORMANCE
*/
drop table ISI_CoverageLPLLawyer;
--insert into ISI_CoverageLPLLawyer (select * from ISI_CoverageLPLLawyerView);
create table ISI_CoverageLPLLawyer as
    (select * from ISI_CoverageLPLLawyerView);
select count(*) from ISI_CoverageLPLLawyer;

-- Fix ISI_CoverageLplLawyer table
--Cvg-PA after initial REN/NEW
delete from ISI_CoverageLPLLawyer c2 where exists (
select c1.* from ISI_CoverageLPLLawyer c1
where c1.EVENTTYPEID = 'Cvg-PA'
  and c2.POLICYINT = c1.POLICYINT 
  and c2.riskid = c1.riskid
  and c2.transactioneffectivedate = c1.transactioneffectivedate 
  and c2.transactionseqno < c1.transactionseqno
);
--Cvg-AA after initial REN/NEW
delete from ISI_CoverageLPLLawyer c2 where exists (
select c1.* from ISI_CoverageLPLLawyer c1
where c1.EVENTTYPEID = 'Cvg-AA'
  and c2.POLICYINT = c1.POLICYINT 
  and c2.riskid = c1.riskid
  and c2.transactioneffectivedate = c1.transactioneffectivedate 
  and c2.transactionseqno < c1.transactionseqno
);
--HAUSMC bogus coverage
delete from ISI_CoverageLPLLawyer iv where iv.EVENTTYPEID = 'Coverage' and iv.POLICYINT = '0907019' and iv.transactioneffectivedate = '20090701' and iv.transactionseqno = 1;
--BOYKTH bogus Cvg-PE
delete from ISI_CoverageLPLLawyer iv where iv.POLICYINT = '1007027' and iv.transactioneffectivedate = '20100702' and iv.EVENTTYPEID = 'Cvg-PE';
--------------------------------------------------------------------------------

-- view carry-forwards firm's prior transaction record(s) (ie: 1, 2-1, 3-2-1)
create or replace view ISI_CoverageLPLLawyerAllView as
with cov1 as (
    select 
        C1.* 
    from ISI_CoverageLPLLawyer C1
    where C1.EVENTTYPEID not in ('Cvg-PE') --there is not a Cvg-PE (WLMEND26) ISI equivalent
)
    select 
        c2.transactiontype as new_transactiontype,
        c2.transactioneffectivedate as new_transactioneffectivedate,
        c2.transactionseqno as new_transactionseqno, 
        CASE
            WHEN c2.transactionseqno <> c1.transactionseqno THEN 0
            ELSE c1.transactionpremium
        END as new_transactionpremium,
        c1.*
    from (select * from cov1 where transactiontype not in ('RCA', 'CAN', 'ERP')) c1
        left join (select * from cov1 where transactiontype not in ('RCA', 'CAN', 'ERP')) c2 on c2.POLICYINT = c1.POLICYINT and c2.transactioneffectivedate = c1.transactioneffectivedate and c2.riskid = c1.riskid and c2.transactionseqno >= c1.transactionseqno
    union all
    select 
        c1.transactiontype as new_transactiontype,
        c1.transactioneffectivedate as new_transactioneffectivedate,
        c1.transactionseqno as new_transactionseqno, 
        c1.transactionseqno as new_transactionpremium,
        c1.*
    from (select * from cov1 where transactiontype in ('RCA', 'CAN', 'ERP')) c1 
    --order by c2.POLICYINT, c2.transactioneffectivedate, c2.transactionseqno, c2.riskid
;

select count(*) from ISI_CoverageLPLLawyerView;
select * from ISI_CoverageLPLLawyerView where POLICYINT = '1506090';
select * from ISI_CoverageLPLLawyerView order by POLICYINT, transactioneffectivedate, transactionseqno, riskid;
select * from PolicyLawyerCoverageAllView;
select * from Policy where policyNumber = '1506090';
select * from PremiumPerLawyerView where policyId = 'AP000720722';
select * from Endorsement;
select * from PolicyLawyerCoverage;



/*--- Table ---*/
drop table TailLawyer2;
--delete * from ISI_Policy;
create table TailLawyer2 as
    (select * from TailLawyerView2);
select * from TailLawyer2;   
select count(*) from TailLawyer2;  
 
--- 
create or replace view ISI_CoverageERPLawyerView as 
    --ERP coverage lawyer
    select
        iv.conversionreference as conversionreference,
        iv.transactiontype as transactiontype,
        iv.transactioneffectivedate as transactioneffectivedate,
        iv.transactionseqno as transactionseqno,
        p.policyNumber as POLICYINT,
        iv.EVENTTYPEID as EVENTTYPEID,
        iv.extendreporttermlength AS TAILLENGTH,
        CASE
            WHEN iv.transactiontype = 'ERP' AND iv.EVENTTYPEID = 'Cvg-EU' and iv.extendreporttermlength is NULL THEN 'LPL__ATTORNEY__WLMEU16_WI_19000101_20991231_4112'
            WHEN iv.transactiontype = 'ERP' AND iv.EVENTTYPEID = 'Cvg-EC' and tlv.tailTerm = '1' THEN 'LPL__ATTORNEY__WLMEC16-1_WI_19000101_20991231_4112'
            WHEN iv.transactiontype = 'ERP' AND iv.EVENTTYPEID = 'Cvg-EC' and tlv.tailTerm = '2' THEN 'LPL__ATTORNEY__WLMEC16-2_WI_19000101_20991231_4112'
            WHEN iv.transactiontype = 'ERP' AND iv.EVENTTYPEID = 'Cvg-EC' and tlv.tailTerm = '3' THEN 'LPL__ATTORNEY__WLMEC16-3_WI_19000101_20991231_4112'
            WHEN iv.transactiontype = 'ERP' AND iv.EVENTTYPEID = 'Cvg-EC' and tlv.tailTerm = '6' THEN 'LPL__ATTORNEY__WLMEC16-6_WI_19000101_20991231_4112'
            WHEN iv.transactiontype = 'ERP' AND iv.EVENTTYPEID = 'Cvg-ED' and iv.extendreporttermlength is NULL THEN 'LPL__ATTORNEY__WLMED16_WI_19000101_20991231_4112'
            WHEN iv.transactiontype = 'ERP' AND iv.EVENTTYPEID = 'Cvg-NU' and tlv.tailTerm is NULL THEN 'LPL__ATTORNEY__WLMNU17_WI_19000101_20991231_4112'
            WHEN iv.transactiontype = 'ERP' AND iv.EVENTTYPEID = 'Cvg-NP' and tlv.tailTerm = '1' THEN 'LPL__ATTORNEY__WLMNP17-1_WI_19000101_20991231_4112'
            WHEN iv.transactiontype = 'ERP' AND iv.EVENTTYPEID = 'Cvg-NP' and tlv.tailTerm = '2' THEN 'LPL__ATTORNEY__WLMNP17-2_WI_19000101_20991231_4112'
            WHEN iv.transactiontype = 'ERP' AND iv.EVENTTYPEID = 'Cvg-NP' and tlv.tailTerm = '3' THEN 'LPL__ATTORNEY__WLMNP17-3_WI_19000101_20991231_4112'
            WHEN iv.transactiontype = 'ERP' AND iv.EVENTTYPEID = 'Cvg-NP' and tlv.tailTerm = '6' THEN 'LPL__ATTORNEY__WLMNP17-6_WI_19000101_20991231_4112'
            WHEN iv.transactiontype = 'ERP' and iv.EVENTTYPEID = 'Cvg-ND' THEN 'LPL__ATTORNEY__WLMND17_WI_19000101_20991231_4112'
            WHEN iv.transactiontype = 'ERP' and iv.EVENTTYPEID = 'Cvg-CP' and tlv.tailTerm = '3' THEN 'LPL__ATTORNEY__WLMEND29-3_WI_19000101_20991231_4112'
            WHEN iv.transactiontype = 'ERP' and iv.EVENTTYPEID = 'Cvg-CP' and tlv.tailTerm = '6' THEN 'LPL__ATTORNEY__WLMEND29-6_WI_19000101_20991231_4112'
            --ELSE 'DONT KNOW TAIL ENDORSEMENT'
            ELSE 'LPL__ATTORNEY__WLMNU17_WI_19000101_20991231_4112'
        END AS pcmmappingkey,
        iv.sbuid as sbuid,
        0 as packageid,
        iv.riskid as riskid,
        0 as linenumber,
        CASE
            WHEN iv.transactiontype = 'ERP' AND iv.EVENTTYPEID = 'Cvg-EU' and iv.extendreporttermlength is NULL THEN 'WLMNU16'
            WHEN iv.transactiontype = 'ERP' AND iv.EVENTTYPEID = 'Cvg-EC' and tlv.tailTerm = '1' THEN 'WLMNP16-1'
            WHEN iv.transactiontype = 'ERP' AND iv.EVENTTYPEID = 'Cvg-EC' and tlv.tailTerm = '2' THEN 'WLMNP16-2'
            WHEN iv.transactiontype = 'ERP' AND iv.EVENTTYPEID = 'Cvg-EC' and tlv.tailTerm = '3' THEN 'WLMNP16-3'
            WHEN iv.transactiontype = 'ERP' AND iv.EVENTTYPEID = 'Cvg-EC' and tlv.tailTerm = '6' THEN 'WLMNP16-6'
            WHEN iv.transactiontype = 'ERP' AND iv.EVENTTYPEID = 'Cvg-ED' and iv.extendreporttermlength is NULL THEN 'WLMED16'
            WHEN iv.transactiontype = 'ERP' AND iv.EVENTTYPEID = 'Cvg-NU' and tlv.tailTerm is NULL THEN 'WLMNU17'
            WHEN iv.transactiontype = 'ERP' AND iv.EVENTTYPEID = 'Cvg-NP' and tlv.tailTerm = '1' THEN 'WLMNP17-1'
            WHEN iv.transactiontype = 'ERP' AND iv.EVENTTYPEID = 'Cvg-NP' and tlv.tailTerm = '2' THEN 'WLMNP17-2'
            WHEN iv.transactiontype = 'ERP' AND iv.EVENTTYPEID = 'Cvg-NP' and tlv.tailTerm = '3' THEN 'WLMNP17-3'
            WHEN iv.transactiontype = 'ERP' AND iv.EVENTTYPEID = 'Cvg-NP' and tlv.tailTerm = '6' THEN 'WLMNP17-6'
            WHEN iv.transactiontype = 'ERP' and iv.EVENTTYPEID = 'Cvg-ND' THEN 'WLMND17'
            WHEN iv.transactiontype = 'ERP' and iv.EVENTTYPEID = 'Cvg-CP' and tlv.tailTerm = '3' THEN 'WLMEND29-3'
            WHEN iv.transactiontype = 'ERP' and iv.EVENTTYPEID = 'Cvg-CP' and tlv.tailTerm = '6' THEN 'WLMEND29-6'
            --ELSE '?????????'
            ELSE 'WLMNU17'
        END as coverageid,
        NULL as basisofsettlement,
        NULL as coinsurance,
        NULL as rategroup,
        NULL as drivingrecord,
        NULL as rateper,
        NULL as baseratefactor,
        NULL as overrideratefactor,
        p.perClaimLimit as limit1,
        p.aggregateLimit as limit2,
        'USD' as limitcurrencycd,
        p.perClaimDeductible as deductible1,
        0 as deductible2,
        'USD' as premiumcurrencycd,
        NULL as ratedbasepremium,
        NULL as ratedgrosspremium,
        NULL as ratednetpremium,
        CASE
            -- remove DF and prorate based on 
            WHEN iv.transactiontype in ('ERP') THEN COALESCE(tlv.oldTailPremium, 0) ---currently always full term premium
            ELSE 0
        END as annualpremium,
        CASE
            WHEN iv.transactiontype in ('ERP') THEN COALESCE(tlv.oldTailPremium, 0) 
            ELSE 0
        END as transactionpremium,
        0 as nonpremiumsurchargeamount,
        0 as premiumsurchargeamount,
        0 as transactionsurcharge,
        0 as nonpremiumdiscountamount,
        0 as premiumdiscountamount,
        0 as transactiondiscount,
        0 as commissionamount,
        NULL as numberof1.
        NULL as valueof1.
        NULL as typeof1.
        NULL as numberof2,
        NULL as valueof2,
        NULL as typeof2,
        CASE 
            WHEN (TO_CHAR(y.expirationDate, 'YYYYMMDD') <> TO_CHAR(p.expirationDate, 'YYYYMMDD')) and (TO_CHAR(y.expirationDate, 'YYYYMMDD') <= iv.transactioneffectivedate) THEN 1
            ELSE 0
        END as deletedrow,
        ---iv.deletedrow as deletedrow,
        NULL as conversionserver,
        NULL as sequencenumber,
        iv.veffectivedate as veffectivedate,
        iv.vexpirydate as vexpirydate,
        iv.claimsmadeind as claimsmadeind,
        iv.retrodate as retrodate,
        iv.postedwweobjectid as postedwweobjectid,
        NULL as limittierinfo,
        -- only added to transactionseqno=1
        CASE
            WHEN iv.transactiontype in ('ERP') THEN COALESCE(tlv.oldTailPremium, 0) 
            ELSE 0
        END as ratedftbasepremium,
        CASE
            WHEN iv.transactiontype in ('ERP') THEN COALESCE(tlv.oldTailPremium, 0) 
            ELSE 0
        END as ratedftgrosspremium,
        CASE
            WHEN iv.transactiontype in ('ERP') THEN COALESCE(tlv.oldTailPremium, 0) 
            ELSE 0
        END as ratedftpremium,
        CASE
            WHEN iv.transactiontype in ('ERP') THEN COALESCE(tlv.oldTailPremium, 0) 
            ELSE 0
        END as ratedannualpremium,
        CASE
            WHEN iv.transactiontype in ('ERP') THEN COALESCE(tlv.oldTailPremium, 0) 
            ELSE 0
        END as ratedtransactionpremium,
        0 as ratedcommissionpercent,
        0 as ratedcommissionamount,
        CASE
            WHEN iv.transactiontype in ('ERP') THEN COALESCE(tlv.oldTailPremium, 0) 
            ELSE 0
        END as ftpremium,
        0 as offsetftpremium,
        1 as commissiontype,
        0 as commissionpercent,
        1.0000000 as transactionpremiumfactor,
        0 as transactionpremiumdlyrate,
        NULL as overriderule,
        NULL as overrideftpremium,
        NULL as overrideannualpremium,
        NULL as overridetransactionpremium,
        NULL as overridecommissionpercent,
        NULL as overridecommissionamount,
        0 as taxamount,
        NULL as gldiv,
        NULL as gldept,
        NULL as gllob,
        0 as siretention1,
        0 as siretention2,
        NULL as mappingcode,
        NULL as submappingcode,
        NULL as submappingcode2,
        NULL as netrate,
        NULL as grossrate,
        NULL as rateexposure,
        0 as adjbilltypeind,
        NULL as waivedtransactionpremium,
        NULL as classcode,
        NULL as iso_statecode,
        NULL as iso_zipcode,
        NULL as iso_ratingterritory,
        NULL as iso_typeofpolicycode,
        NULL as iso_aslobstatcode,
        NULL as iso_sublinecode,
        NULL as iso_classificationcode,
        NULL as iso_coveragestatcode,
        NULL as iso_ratingidentificationcode,
        NULL as iso_constructioncode,
        NULL as iso_fireprotectioncode,
        NULL as iso_terrorismcoveragecode,
        NULL as iso_windhaildedcode,
        NULL as iso_bcegclass,
        NULL as iso_ratingbasiscode,
        NULL as iso_liabilityformcode,
        NULL as iso_molddamagecode,
        NULL as iso_liabilityexposureindcode,
        NULL as iso_exposurestatamount,
        NULL as iso_ratingmodificationfactor,
        NULL as iso_stateexceptionindcode,
        NULL as iso_bussincomeexpensecode,
        NULL as iso_liabcovindcode,
        NULL as iso_pctowneroccupied,
        NULL as iso_classcodedesc,
        TO_CHAR(p.effectiveDate, 'YYYYMMDD') as rateeffectivedate,
        'FL' as deductibletype,
        NULL as iso_formcode,
        NULL as iso_losscostmultiplier,
        NULL as iso_losscostdate,
        NULL as iso_yearofconstruction,
        TO_CHAR(TO_INT(ORA_HASH(iv.POLICYINT)))||TO_CHAR(TO_INT(ORA_HASH('PL_0'))) as ouid  --unique to coverage 
    from (select * from ISI_ItemLawyerView where transactiontype = 'ERP') iv
        left join PolicyLawyerCoverageAllView y on y.policyNumber = iv.POLICYINT and y.lawyerId = iv.LAWYERID and (TO_CHAR(y.effectiveDate, 'YYYYMMDD') <= iv.transactioneffectivedate)
        left join Policy p on p.policyNumber = iv.POLICYINT
        left join TailLawyer2 TLV on tlv.coverageId = iv.COVERAGEID and tlv.lawyerId = iv.LAWYERID 
        /* per policy accounting --
        left join PolicyLawyerCoverage plc on plc.coverageId = iv.COVERAGEID and plc.lawyerId = iv.LAWYERID
            left join PolicyAccounting pa on pa.coverageId = plc.coverageId
        */
    order by POLICYINT, transactioneffectivedate, riskid
;

/*---Testing ---*/
select * from ISI_ItemView;
select * from ISI_InsuredsView;
select * from ISI_CoverageDRView;
select * from PremiumPerLawyerView where DefReimbCoverage is not NULL;
select e.*, p.* from Policy p right join (select policyId from Endorsement where substr(endorsementDesc,1,8) = 'WLMEND20') e on e.policyId = p.policyId;
--------------------------------------------------------------------------------
/*
    ISI_InlineSchedulesView
    p_inlineschedules
    
    Dependancies: ISI_ItemView
    
    History
    12/18/2017  FZ  Created
    01/20/2018  FZ  fixed duplicate AOP linenumber
*/
create or replace view ISI_InlineSchedulesFirmAOPView as 
--firm level area of practice
    select
        iv.POLICYINT as POLICYINT,
        iv.conversionreference as conversionreference,
        iv.transactiontype as transactiontype,
        iv.transactioneffectivedate as transactioneffectivedate,
        iv.transactionseqno as transactionseqno,
        iv.sbuid as sbuid,
        iv.packageid as packageid,
        iv.riskid as riskid,
        'areaofpractice' as inlinecategory,
        ---SUBSTR(TO_CHAR(TO_INT(ORA_HASH(ALA.lawAreaId||'-'||LA.lawAreaName))),-3) as linenumber,
        ALA.lawAreaSequence as linenumber,  --AOP sequence
        ALA.lawAreaId as description,
        NULL as itemvalue,
        CASE 
            WHEN (TO_CHAR(y.expirationDate, 'YYYYMMDD') <> TO_CHAR(p.expirationDate, 'YYYYMMDD')) and (TO_CHAR(y.expirationDate, 'YYYYMMDD') <= iv.transactioneffectivedate) THEN 1
            ELSE 0
        END as deletedrow,
        NULL as conversionserver,
        NULL as sequencenumber,
        iv.veffectivedate as veffectivedate,
        iv.vexpirydate as vexpirydate,
        NULL as postedwweobjectid 
    from (select * from ISI_ItemFirmView where riskid = 1) iv
        left join Policy p on p.policyNumber = iv.POLICYINT
            left join ApplicationLawAreaView ALA on ALA.applicationId = p.policyId
                left join LawArea LA on LA.lawAreaId = ALA.lawAreaId
        left join PolicyLawyerCoverageAllView y on y.policyNumber = iv.POLICYINT and y.lawyerId = iv.LAWYERID and (TO_CHAR(y.effectiveDate, 'YYYYMMDD') <= iv.transactioneffectivedate)
;

create or replace view ISI_InlineSchedulesFirmLimView as 
    -- firm level limit
    select 
        iv.POLICYINT as POLICYINT,
        iv.conversionreference as conversionreference,
        iv.transactiontype as transactiontype,
        iv.transactioneffectivedate as transactioneffectivedate,
        iv.transactionseqno as transactionseqno,
        iv.sbuid as sbuid,
        iv.packageid as packageid,
        iv.riskid as riskid,
        'quotelimitsfirm' as inlinecategory,
        1 as linenumber,
        NULL as description,
        NULL as itemvalue,
        0 as deletedrow,
        NULL as conversionserver,
        NULL as sequencenumber,
        iv.veffectivedate as veffectivedate,
        iv.vexpirydate as vexpirydate,
        NULL as postedwweobjectid  
    from (select * from ISI_ItemFirmView where riskid = 1) iv
        left join Policy p on p.policyNumber = iv.POLICYINT
;     


create or replace view ISI_InlineSchedulesLawyerView as
    --lawyer level
    select
        iv.POLICYINT as POLICYINT,
        iv.conversionreference as conversionreference,
        iv.transactiontype as transactiontype,
        iv.transactioneffectivedate as transactioneffectivedate,
        iv.transactionseqno as transactionseqno,
        iv.sbuid as sbuid,
        iv.packageid as packageid,
        iv.riskid as riskid,
        'quotelimitsattorney' as inlinecategory,
        --TO_CHAR(iv.riskid, 'FM000') as linenumber,  --SUBSTR(TO_CHAR(TO_INT(ORA_HASH(y.lawyerId))),-3) as linenumber,  --lawyer number
        iv.riskid as linenumber,  --lawyer number
        NULL as description,
        NULL as itemvalue,
        CASE 
            WHEN (TO_CHAR(y.expirationDate, 'YYYYMMDD') <> TO_CHAR(p.expirationDate, 'YYYYMMDD')) and (TO_CHAR(y.expirationDate, 'YYYYMMDD') <= iv.transactioneffectivedate) THEN 1
            ELSE 0
        END as deletedrow,
        NULL as conversionserver,
        NULL as sequencenumber,
        iv.veffectivedate as veffectivedate,
        iv.vexpirydate as vexpirydate,
        NULL as postedwweobjectid
    from (select * from ISI_ItemLawyerView where riskid <> '001') iv  
        left join Policy p on p.policyNumber = iv.POLICYINT
        left join PolicyLawyerCoverageAllView y on y.policyNumber = iv.POLICYINT and y.lawyerId = iv.LAWYERID and (TO_CHAR(y.effectiveDate, 'YYYYMMDD') <= iv.transactioneffectivedate)
order by POLICYINT, transactioneffectivedate, riskid
;

/*---Testing ---*/
select * from ISI_ItemView;
select * from ISI_InsuredsView;
select * from ISI_InlineSchedulesView;


--------------------------------------------------------------------------------
/*
    ISI_AreaOfPractice1View
    is_areaofpractice_1
    
    Dependancies: ISI_ItemView
    
    Notes:
    
    History:
    12/18/2017  FZ  Created
    01/20/2018  FZ  fixed linenumber AOP duplicates
    02/15/2018  FZ  default aop factor sould be 0.00 since ISI is adding aop factors
    
*/
create or replace view ISI_AreaOfPractice1View as 
    select
        iv.POLICYINT as POLICYINT,
        iv.conversionreference as conversionreference,
        iv.transactioneffectivedate as transactioneffectivedate,
        iv.transactionseqno as transactionseqno,
        iv.transactiontype as transactiontype,   
        iv.sbuid as sbuid,
        0 as packageid,
        --iv.riskid as riskid,
        iv.insuredid as riskid,  --source is InsuredsView 
        'areaofpractice' as inlinecategory,
        --SUBSTR(TO_CHAR(TO_INT(ORA_HASH(ALA.lawAreaId||'-'||LA.lawAreaName))),-3) as linenumber,
        ALA.lawAreaSequence as linenumber,
        ALA.lawAreaId as aopcode,
        CASE
            WHEN ALA.lawAreaId = 'BD' THEN COALESCE(Rc.BodilyInjuryDefense, 0.00)  --02/15/2018 all defaults change to 0.00 from 1.00
            WHEN ALA.lawAreaId = 'BP' THEN COALESCE(Rc.BodilyInjuryPlaintiff, 0.00)
            WHEN ALA.lawAreaId = 'CB' THEN COALESCE(Rc.CorpBusOrg, 0.00)
            WHEN ALA.lawAreaId = 'EP' THEN COALESCE(Rc.EstateProbateTrust, 0.00)
            WHEN ALA.lawAreaId = 'CP' THEN COALESCE(Rc.IntellProp, 0.00)
            WHEN ALA.lawAreaId = 'RE' THEN COALESCE(Rc.RealEstate, 0.00)
            WHEN ALA.lawAreaId = 'WD' THEN COALESCE(Rc.WorkersCompDefense, 0.00)
            WHEN ALA.lawAreaId = 'WP' THEN COALESCE(Rc.WorkersCompPlaintiff, 0.00)
            ELSE 0.00  --02/15/2018 changed to 0.00 from 1.00
        END as aopfactor,
        100 as aopfactorhigh,
        0 as aopfactorlow,
        NULL as aopotherexplain,  --NEED: determine where and if stored in LAW
        ALA.lawAreaPercent as aoppercent,
        NULL as aopweightedfactor,
        NULL as conditionalmandatoryfields,
        NULL as conversionserver,
        NULL as sequencenumber
--    from (select * from ISI_ItemView where riskid = 1) iv
    from (select * from ISI_InsuredsFirmView where insuredid = 1) iv  --to speed up creation
        left join Policy p on p.policyNumber = iv.POLICYINT
            left join ApplicationLawAreaView ALA on ALA.applicationId = p.policyId
            --    left join LawArea LA on LA.lawAreaId = ALA.lawAreaId
            left join RatingCrosstab RC on Rc.applicationId = p.policyId
order by POLICYINT, transactioneffectivedate, transactionseqno
;

/*--- Testing ---*/
select * from ApplicationLawArea where lawAreaId = 'ZZ' and lawAreaNote is not NULL;
select * from Question order by questionId;
select * from ApplicationQuestion where questionId = 'ZZ';
select * from Rating;
select * from LawArea order by lawAreaId;
--BERGL1_2008_2018|20110410
select * from ISI_AreaOfPractice1View where conversionreference = 'BERGL1_2008_2018' order by transactioneffectivedate; -- = '20110410';
select * from RatingCrosstab where firmId = 'BERGL1';
--------------------------------------------------------------------------------
/*
    ISI_QuoteLimits1View
    is_quotelimits_1
    
    Dependancies: ISI_QuoteLimitsAttorneyView, ISI_ItemView
    Notes:
    -   factors are multiply ready (ie: -0.05 = 1.05)
    
    History
    12/22/2017  FZ  Created
    12/26/2017  FZ  Completed version 1
*/
create or replace view ISI_QuoteLimits1View as
    select
        QLSV.POLICYINT as POLICYINT,
        QLSV.conversionreference as conversionreference,
        QLSV.transactioneffectivedate as transactioneffectivedate,
        QLSV.transactionseqno as transactionseqno,
        QLSV.transactiontype as transactiontype,  
        '4112' as sbuid,
        0 as packageid,
        1 as riskid,
        'quotelimitsfirm' as inlinecategory,
        1 as linenumber,
        COALESCE(rlc.LimitAgg, 1.00) as additionallimitfactor,
        NULL as attorneyfactor,
        QLSV.attorneyfactorsubtotal as attorneyfactorsubtotal,
        QLSV.attorneyfactorsubtotal - QLSV.clecreditsfactorsubtotal as attorneyfactortotal,
        QLSV.baserate as baserate,
        1 as clecreditsfactor,
        QLSV.clecreditsfactorsubtotal as clecreditsfactorsubtotal,
        QLSV.clecreditsfactorsubtotal - QLSV.continuityfactorsubtotal as clecreditsfactortotal,
        NULL as conditionalmandatoryfields,
        1 + COALESCE(Rc.YearsInsured, 0.00) as continuityfactor,
        QLSV.continuityfactorsubtotal as continuityfactorsubtotal,
        QLSV.continuityfactorsubtotal - QLSV.firmsizefactorsubtotal as continuityfactortotal,
        rlc.DeductibleFactor as deductiblefactor,
        QLSV.deductiblefactorsubtotal as deductiblefactorsubtotal,
        QLSV.deductiblefactorsubtotal - QLSV.baserate as deductiblefactortotal,
        QLSV.xs10premiumsubtotal as facpremiumxs10m,
        NULL as facpremiumxs10mreadonly,
        QLSV.xs5premiumsubtotal as facpremiumxs5m,
        NULL as facpremiumxs5mreadonly,
        NULL as facpremiumxs5msysind, --added v133
        1 as firmlinenumber,
        QLSV.attorneypremium as firmpremium,
        COALESCE(Rc.Firm, 1.00) as firmsizefactor,
        QLSV.firmsizefactorsubtotal as firmsizefactorsubtotal,
        QLSV.firmsizefactorsubtotal - QLSV.limitfactorsubtotal as firmsizefactortotal,
        p.perClaimDeductible as liabilitydeductible,
        LTRIM(TO_CHAR(p.perClaimDeductible, '999,999,999')) as liabilitydeductibletext,
        p.perClaimLimit||'_'||p.aggregateLimit as liabilitylimit,
        p.perClaimLimit as liabilitylimit1,
        p.aggregateLimit as liabilitylimit2,
        LTRIM(TO_CHAR(p.perClaimLimit, '999,999,999'))||'/'||LTRIM(TO_CHAR(p.aggregateLimit, '999,999,999')) as liabilitylimittext,
        NULL as liabilitypremiumtext,
        rlc.LimitFactor as limitfactor,
        QLSV.limitfactorsubtotal as limitfactorsubtotal,
        QLSV.limitfactorsubtotal - QLSV.deductiblefactorsubtotal as limitfactortotal,
        1 as policylimitind,
        --TO_CHAR(PRDV.ISIPolicyRetroDate, 'YYYYMMDD') as quotelimitretrodate,  --removed v133
        --TO_CHAR(PRDV.ISIPolicyRetroDate, 'YYYYMMDD') as quotelimitretrodatereadonly, --removed v133
        1 as quoteproposalind,
        ROUND(Rc.SchdAdjPctCalc,2) as scheduledadjustmentfactor,
        QLSV.attorneyfactorsubtotal - QLSV.scheduledadjustmentfactorsu_1 as scheduleadjustmentfactorsu_1,
        QLSV.scheduledadjustmentfactorsu_1 as scheduledadjustmentfactortotal,
        NULL as xs10premiumfactor,
        NULL as xs10premiumperattorney,
        QLSV.xs10premiumsubtotal as xs10premiumsubtotal,
        0 as xs10premiumtotal,
        1 as xs5factor,
        0 as xs5minimum,
        NULL as xs5premiumfactor,
        NULL as xs5premiumperattorney,
        QLSV.xs5premiumsubtotal as xs5premiumsubtotal,
        0 as xs5premiumtotal,
        NULL as conversionserver,
        NULL as sequencenumber
      from ISI_QuoteLimitSummary QLSV
        left join RatingCrosstab RC on Rc.applicationId = QLSV.policyId
        left join Policy p on p.policyId = QLSV.policyId
            left join QuoteCoverage qc on qc.quoteBookId = Rc.quoteBookId and qc.perClaimLimit = p.perClaimLimit and qc.aggregateLimit = p.aggregateLimit and qc.perClaimDeductible = p.perClaimDeductible
                left join RatingLimitCrosstab rlc on rlc.quoteBookId = Rc.quoteBookId and rlc.quoteCoverageId = qc.quoteCoverageId
        left join ISIPolicyRetroDateView PRDV on PRDV.policyNumber = QLSV.POLICYINT        
    order by QLSV.POLICYINT, QLSV.transactioneffectivedate, QLSV.transactionseqno    
;
/*--- Testing ---*/
select * from ISI_QuoteLimitSummaryView;
select * from ISI_ItemView;
select * from ISI_QuoteLimits1View;
select * from Policy where firmId in ('KIMLAV');
select * from RatingCrosstab where applicationId in ('AP000823716');
select * from Quote where applicationId in ('AP000823716');
select * from RatingLimitCrosstab where quoteBookId in ('AR000839542');
select * from RatingLimitCrosstab where LimitAgg is not NULL;
select * from QuoteCoverage where quoteBookId in ('AR000839542');
select * from PremiumPerLawyerView where firmId in ('KIMLAV');

--------------------------------------------------------------------------------
/*
    ISI_ QuoteLimitAttorneysView
    is_quotelimitsattorney_1
    
    Dependancies: ISI_ItemView
    Notes:
    -   factors are multiply ready (ie: -0.05 = 1.05)
    
    History
    12/19/2017  FZ  Created
    01/20/2018  FZ  riskid
*/
create or replace view ISI_QuoteLimitsAttorneyView as
    select
        iv.POLICYINT as POLICYINT,
        iv.LAWYERID as LAWYERID,
        iv.conversionreference as conversionreference,
        iv.transactioneffectivedate as transactioneffectivedate,
        iv.transactionseqno as transactionseqno,
        iv.transactiontype as transactiontype,  
        iv.sbuid as sbuid,
        0 as packageid,
        iv.riskid as riskid,
        'quotelimitsattorney' as inlinecategory,
        iv.riskid as linenumber,  --lawyer number
        COALESCE(pplv.LimitAgg, 1.00) as additionallimitfactor, ---QUESTION: assuming this is the aggregate limit factor??????
        COALESCE(pplv.LawyerFactor, 1.00) as attorneyfactor,
        NULL as attorneyfactorcomment,
        --#7 Lawyer Factor
        ROUND(ROUND(((((COALESCE(pplv.DeductibleFactor, 1.00) * COALESCE(pplv.Base, 0)) * (COALESCE(pplv.LimitFactor, 1.00) * COALESCE(pplv.LimitAgg, 1.00))) * COALESCE(pplv.Firm, 1.00)) * (1 + (COALESCE(pplv.YearsInsured,1.00)))) * (1 +  COALESCE(pplv.ContLegalEdCredit, 0)),0) * COALESCE(pplv.LawyerFactor,1.00),0) as attorneyfactorsubtotal,
        iv.riskid as attorneynumber,
        --#11 Total Lawyer Premium
        ROUND((((((((COALESCE(pplv.DeductibleFactor, 1.00) * COALESCE(pplv.Base, 0)) * (COALESCE(pplv.LimitFactor, 1.00) * COALESCE(pplv.LimitAgg, 1.00))) * COALESCE(pplv.Firm, 1.00)) * (1 + (COALESCE(pplv.YearsInsured, 1.00)))) * (1 +  COALESCE(pplv.ContLegalEdCredit, 0)) * COALESCE(pplv.LawyerFactor,1.00)) * COALESCE(pplv.SchdAdjPctCalc, 1.00)) + COALESCE(pplv.premiumPerLawyerXS5M, 0)),0) + COALESCE(pplv.premiumPerLawyerXS10M, 0) as attorneypremium,
        0 as attorneypremium1m,
        0 as attorneypremium5m,
        --#1 Base
        COALESCE(pplv.Base, 0) as baserate,
        1 +  COALESCE(pplv.ContLegalEdCredit, 0) as clecreditsfactor,
        NULL as clecreditsfactorcomment,
        --#6 CLE
        ROUND(((((COALESCE(pplv.DeductibleFactor, 1.00) * COALESCE(pplv.Base, 0)) * (COALESCE(pplv.LimitFactor, 1.00) * COALESCE(pplv.LimitAgg, 1.00))) * COALESCE(pplv.Firm, 1.00)) * (1 + (COALESCE(pplv.YearsInsured,1.00)))) * (1 +  COALESCE(pplv.ContLegalEdCredit, 0)),0) as clecreditsfactorsubtotal,
        1 + (COALESCE(pplv.YearsInsured,1.00)) as continuityfactor,
        NULL as continuityfactorcomment,
        --#5 Continuity
        ROUND((((COALESCE(pplv.DeductibleFactor, 1.00) * COALESCE(pplv.Base, 0)) * (COALESCE(pplv.LimitFactor, 1.00) * COALESCE(pplv.LimitAgg, 1.00))) * COALESCE(pplv.Firm, 1.00)) * (1 + (COALESCE(pplv.YearsInsured,1.00))),0) as continuityfactorsubtotal,
        COALESCE(pplv.DeductibleFactor, 1.00) as deductiblefactor,
        NULL as deductiblefactorcomment,
        --#2 Deductible
        ROUND(COALESCE(pplv.DeductibleFactor, 1.00) * COALESCE(pplv.Base, 0),0) as deductiblefactorsubtotal,
        --NULL as donotrateind,  --removed v133
        COALESCE(pplv.Firm, 1.00) as firmsizefactor,
        NULL as firmsizefactorcomment,
        --#4 Firmsize
        ROUND(((COALESCE(pplv.DeductibleFactor, 1.00) * COALESCE(pplv.Base, 0)) * (COALESCE(pplv.LimitFactor, 1.00)* COALESCE(pplv.LimitAgg, 1.00))) * COALESCE(pplv.Firm, 1.00),0)  as firmsizefactorsubtotal,
        p.perClaimDeductible as liabilitydeductibletext,
        p.perClaimLimit as liabilitylimit1,
        p.aggregateLimit as liabilitylimit2,
        LTRIM(TO_CHAR(p.perClaimLimit, '999,999,999'))||'/'||LTRIM(TO_CHAR(p.aggregateLimit, '999,999,999')) as liabilitylimittext,
        COALESCE(pplv.LimitFactor, 1.00) as limitfactor,
        NULL as limitfactorcomment,
        --#3 Limit
        ROUND((COALESCE(pplv.DeductibleFactor, 1.00) * COALESCE(pplv.Base, 0)) * (COALESCE(pplv.LimitFactor, 1.00) * COALESCE(pplv.LimitAgg, 1.00)),0) as limitfactorsubtotal,
        NULL as militaryleaveind,
        1 as policylimitind,
        --iv.retrodate as quotelimitretrodate,--removed v133
        ROUND(COALESCE(pplv.SchdAdjPctCalc, 1.00),2) as scheduledadjustmentfactor,
        NULL as scheduledadjustmentfactorco_1,      
        -- #8 Schedule Adjustment
        ROUND(ROUND((((((COALESCE(pplv.DeductibleFactor, 1.00) * COALESCE(pplv.Base, 0)) * (COALESCE(pplv.LimitFactor, 1.00) * COALESCE(pplv.LimitAgg, 1.00))) * COALESCE(pplv.Firm, 1.00)) * (1 + (COALESCE(pplv.YearsInsured,1.00)))) * (1 +  COALESCE(pplv.ContLegalEdCredit, 0)) * COALESCE(pplv.LawyerFactor,1.00)),0) * COALESCE(pplv.SchdAdjPctCalc, 1.00),0) as scheduledadjustmentfactorsu_1,
        COALESCE(pplv.premiumPerLawyerXS10M, 0) as xs10premium,
        NULL as xs10premiumcomment,
        NULL as xs10premiumperattorney,
        --#10 XS10M
        ROUND((((((((COALESCE(pplv.DeductibleFactor, 1.00) * COALESCE(pplv.Base, 0)) * (COALESCE(pplv.LimitFactor, 1.00) * COALESCE(pplv.LimitAgg, 1.00))) * COALESCE(pplv.Firm, 1.00)) * (1 + (COALESCE(pplv.YearsInsured,1.00)))) * (1 +  COALESCE(pplv.ContLegalEdCredit, 0)) * COALESCE(pplv.LawyerFactor,1.00)) * COALESCE(pplv.SchdAdjPctCalc, 1.00)) + COALESCE(pplv.premiumPerLawyerXS5M, 0)) + COALESCE(pplv.premiumPerLawyerXS10M, 0),0) as xs10premiumsubtotal,
        1 as xs5factor,
        0 as xs5minimum,
        COALESCE(pplv.premiumPerLawyerXS5M, 0) as xs5premium,
        NULL as xs5premiumcomment,
        COALESCE(pplv.premiumPerLawyerXS5M, 0) as xs5premiumperattorney,
        --#9 XS5M
        ROUND(((((((COALESCE(pplv.DeductibleFactor, 1.00) * COALESCE(pplv.Base, 0)) * (COALESCE(pplv.LimitFactor, 1.00) * COALESCE(pplv.LimitAgg, 1.00))) * COALESCE(pplv.Firm, 1.00)) * (1 + (COALESCE(pplv.YearsInsured,1.00)))) * (1 +  COALESCE(pplv.ContLegalEdCredit, 0)) * COALESCE(pplv.LawyerFactor,1.00)) * COALESCE(pplv.SchdAdjPctCalc, 1.00)) + COALESCE(pplv.premiumPerLawyerXS5M, 0),0) as xs5premiumsubtotal,
        NULL as conversionserver,
        NULL as sequencenumber
    from (select * from ISI_ItemLawyerView where riskid <> 1) iv
        left join Policy p on p.policyNumber = iv.POLICYINT
            --left join PremiumPerLawyerView pplv on pplv.policyId = p.policyId and pplv.lawyerId = iv.LAWYERID and pplv.aggregateLimit = p.aggregateLimit and pplv.Limit = p.perClaimLimit and pplv.Deductible = p.perClaimDeductible
            left join PremiumPerLawyerView pplv on pplv.policyId = p.policyId and pplv.effectiveDate = p.effectiveDate and pplv.lawyerId = iv.LAWYERID and pplv.aggregateLimit = p.aggregateLimit and pplv.Limit = p.perClaimLimit and pplv.Deductible = p.perClaimDeductible
 order by POLICYINT, transactioneffectivedate, transactionseqno   
;
/*---Testing ---*/
select * from ISI_ItemView;
select * from ISI_InsuredsView;
select * from PremiumPerLawyerView where firmId in ('KIMLAV');
select * from PremiumPerLawyerView where premiumPerLawyerXS5M <> 0 order by applicationId;--where ContLegalEdCredit <> 0;
select * from ISI_QuoteLimitsAttorneyView;

-- Summary view 
select * from ISI_QuoteLimitSummaryView;
create or replace view ISI_QuoteLimitSummaryView as
    select
        z.POLICYINT as POLICYINT,
        p.policyId as policyId,
        z.conversionreference as conversionreference,
        z.transactioneffectivedate as transactioneffectivedate,
        z.transactionseqno as transactionseqno,
        z.transactiontype as transactiontype,
        SUM(z.baserate) as baserate,
        SUM(z.attorneypremium) as attorneypremium,
        SUM(z.attorneyfactorsubtotal) as attorneyfactorsubtotal,
        SUM(z.clecreditsfactorsubtotal) as clecreditsfactorsubtotal,
        SUM(z.continuityfactorsubtotal) as continuityfactorsubtotal,
        SUM(z.deductiblefactorsubtotal) as deductiblefactorsubtotal,
        SUM(z.firmsizefactorsubtotal) as firmsizefactorsubtotal,
        SUM(z.limitfactorsubtotal) as limitfactorsubtotal,
        SUM(z.scheduledadjustmentfactorsu_1) as scheduledadjustmentfactorsu_1,
        SUM(z.xs10premiumsubtotal) as xs10premiumsubtotal,
        SUM(z.xs5premiumsubtotal) as xs5premiumsubtotal
    from ISI_QuoteLimitsAttorneyView Z
        left join Policy p on p.policyNumber = z.POLICYINT
    group by POLICYINT, policyId, conversionreference, transactioneffectivedate, transactionseqno, transactiontype
    order by POLICYINT, transactioneffectivedate, transactionseqno
;

/* MUST RUN for PERFORMANCE */
drop table ISI_QuoteLimitSummary;
create table ISI_QuoteLimitSummary as 
    select * from ISI_QuoteLimitSummaryView;