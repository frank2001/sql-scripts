/*  
--  CLAIMS
*/
--------------------------------------------------------------------------------
/*  UTILITIES
--  These are support views used to assemble claim extraction data from LAW.
*/

-- ISIClaimToPolicyView
-- Allows matching of claim to policy transaction based on claim's report date 
-- and policy transactioneffectivedate.  The goal is to get one match per claim.
--  Dependancies:   ISIConversionReferenceView
--  Used By:        ISI_ClaimItemView
--create or replace view "ISIClaimToPolicyView" as 
/*  OLD 05/04/2018
select
    I."instanceId" as "instanceId",
    ICRV."conversionreference" as "conversionreference",
    E."policyId" as "policyId",
    P."policyNumber" as "policyNumber",
    P."effectiveDate" as "effectiveDate",
    TO_CHAR(I."coverageDate", 'YYYYMMDD') as "coveragedate",
    TO_CHAR(MAX(E."effectiveDate"), 'YYYYMMDD') as "transactioneffectivedate"
from "Instance" I
    inner join (select DISTINCT "policyId", "effectiveDate", min("endorsementNumber") as "endorsementNumber" from "Endorsement" group by "policyId", "effectiveDate" order by "effectiveDate") E  on E."policyId" = I."policyId" and E."effectiveDate" <= I."coverageDate"
        left join "Policy" P on P."policyId" = E."policyId"
            left join "ISIConversionReferenceView" ICRV on ICRV."firmId" = P."firmId" and (P."effectiveDate" BETWEEN ICRV."startDate" and ICRV."endDate" and COALESCE(P."cancelEffectiveDate", P."expirationDate") BETWEEN ICRV."startDate" and ICRV."endDate")
--where I."instanceId" like '06-%'
group by I."instanceId", ICRV."conversionreference", E."policyId", P."policyNumber", P."effectiveDate", I."coverageDate"
order by I."instanceId"
;
*/
select * from "Coverage" where "coverageId" = 'CG000800933';

--- NEW (05/04/2018) made same as ISI_PolicyView to now account for multiple records for same transactioneffectivedate
create or replace view "ISIClaimToPolicyView" as 
with first_cut as (
select 
    I."instanceId" as "instanceId",
    PV."conversionreference" as "conversionreference",
    PV."POLICYID" as "policyId",
    PV."policykey" as "policyNumber",
    P."effectiveDate" as "effectiveDate",
    EV."eventTypeId" as "eventTypeId",
    TO_CHAR(I."coverageDate", 'YYYYMMDD') as "coveragedate",
    PV."transactioneffectivedate" as "transactioneffectivedate"
from "Instance" I
    left join "Coverage" CV on CV."coverageId" = I."coverageId"
        left join "Event" EV on EV."eventId" = CV."eventId"
    inner join (select "conversionreference", "transactioneffectivedate", "POLICYID", "policykey", "transactiontype", "transactionseqno" from "ISI_Policy" where "EVENTTYPEID" not in ('Cvg-PA', 'Cvg-DA', 'Cvg-AA', 'Cvg-PE', 'Cvg-NU', 'Cvg-ND', 'Cvg-NP', 'Cvg-CP')) PV on PV."POLICYID" = I."policyId" and PV."transactioneffectivedate" <= TO_CHAR(I."coverageDate", 'YYYYMMDD')   -- inner to account for excluded policies         
    --inner join (select  "conversionreference", "transactioneffectivedate", "POLICYID", "EVENTTYPEID", "policykey", "transactiontype", "transactionseqno" from "ISI_Policy" where "EVENTTYPEID" not in ('Cvg-PA', 'Cvg-DA', 'Cvg-AA', 'Cvg-PE')) PV on PV."POLICYID" = I."policyId" and PV."EVENTTYPEID" = EV."eventTypeId" and PV."transactioneffectivedate" <= TO_CHAR(I."coverageDate", 'YYYYMMDD')
    --inner join (select  "conversionreference", "transactioneffectivedate", "POLICYID", "EVENTTYPEID", "policykey", "transactiontype", "transactionseqno" from "ISI_Policy") PV on PV."POLICYID" = I."policyId" and PV."EVENTTYPEID" = EV."eventTypeId" and PV."transactioneffectivedate" <= TO_CHAR(I."coverageDate", 'YYYYMMDD')
        left join "Policy" P on P."policyId" = PV."POLICYID"
order by I."instanceId"
)
, second_cut as (
    select  
        "instanceId",
        "conversionreference",
        "policyId",
        "policyNumber",
        "effectiveDate",
        "eventTypeId",
        MAX("transactioneffectivedate") as "transactioneffectivedate",
        "coveragedate"
    from first_cut
    group by "instanceId", "conversionreference", "policyId", "policyNumber","eventTypeId", "effectiveDate", "coveragedate"
)
    , third_cut as (
    select 
            SC.*,
            P2."transactiontype",
            P2."transactionseqno",
            row_number() over (partition by SC."instanceId" order by P2."transactionseqno") as "tranSeq"
        from second_cut SC
            left join (select * from "ISI_Policy" where "EVENTTYPEID" not in ('Cvg-PA', 'Cvg-AA', 'Cvg-DA', 'Cvg-PE')) P2 on P2."policykey" = SC."policyNumber" and P2."transactioneffectivedate" = SC."transactioneffectivedate" 
            --left join (select * from "ISI_Policy") P2 on P2."policykey" = SC."policyNumber" and P2."EVENTTYPEID" = SC."eventTypeId" and P2."transactioneffectivedate" = SC."transactioneffectivedate" 
)
   , forth_cut as ( 
    select 
        "instanceId",
        "conversionreference",
        "policyId",
        "policyNumber",
        "effectiveDate",
        "eventTypeId",
        "transactioneffectivedate",
         MAX("transactionseqno") as "transactionseqno",
        "coveragedate"       
    from third_cut
    group by "instanceId", "conversionreference", "policyId", "policyNumber", "effectiveDate", "eventTypeId", "transactioneffectivedate", "coveragedate"
)
    select 
        FC.*,
        P3."transactiontype"
    from forth_cut FC
        left join "ISI_Policy" P3 on P3."policykey" = FC."policyNumber" and P3."transactioneffectivedate" = FC."transactioneffectivedate" and P3."transactionseqno" = FC."transactionseqno"
    order by FC."instanceId"      
;

select count(*) from "ISIClaimToPolicyView";
select * from "ISIClaimToPolicyView" where "eventTypeId" in ('Cvg-DF');
select * from "ISIClaimToPolicyView" where "instanceId" = '08-0001';
select * from "ISI_Policy" where "conversionreference" = 'TLUSHI_1995_2018' order by "transactioneffectivedate", "transactionseqno";
select * from "ISI_ClaimantCoverageDFView";
select * from "ISI_ClaimView" where "claimmappingkey" = '12-0021';
select * from "Instance" where "instanceId" = '12-0021';
select * from "Coverage" where "coverageId" = 'CG000434498';
select * from "Event" where "eventId" = 'ET000434503';



  



-- ISIClaimPayeeView
-- creates a distinct record for payee by claim
-- 04/10/2018   FZ  Stripped down to eliminate 
create or replace view "ISIClaimPayeeView" as 
    select distinct
        C."instanceId" as "instanceId",
        CASE
            WHEN C."organizationId" is not NULL THEN C."organizationId"
            WHEN C."personId" is not NULL THEN C."personId"
            ELSE 'UNKNOWN'
        END as "payeeId",
        CASE
            --WHEN E."entityName" is not NULL THEN LOWER(REPLACE(REPLACE(REPLACE(E."entityName", '.', ''), ',', ''), ' ', ''))
            WHEN E."entityName" is not NULL THEN E."entityName"
            ELSE C."contactSalutation"
        END as "payeeName",
        CASE
            WHEN C."organizationId" is not NULL THEN C."locationId"
            WHEN C."personId" is not NULL THEN C."locationId"
            ELSE 'UNKNOWN'
        END as "payeeLocation"
        /*
        C."locationId",
        L."addressLine1",
        L."addressLine2",
        L."addressLine3",
        L."city",
        L."state",
        L."zip"
        */
    from (select * from "Contact" where "contactTypeId" <> 'Firm') C
        left join "Entity" E on E."entityId" = COALESCE(C."organizationId", COALESCE(C."personId", 'UNKNOWN'))
    where C."instanceId" is not NULL
    order by C."instanceId"
 ;

select * from "ISIClaimPayeeView" where "instanceId" = '06-0049';
select count(*) from "ISIClaimPayeeView";

-- ISIClaimReserveTotalView
-- reserve running totals
create or replace view "ISIClaimReserveTotalView" as 
    select 
        CR."claimId",
        CR."claimReserveId",
        E."eventDate",
        E."eventTypeId",
        CR."estimateALAE",
        sum(CR."estimateALAE") over (partition by CR."claimId" order by CR."claimId", E."eventDate" range between unbounded preceding and current row) as "runningALAE",
        CR."estimateIndemnity",
        sum(CR."estimateIndemnity") over (partition by CR."claimId" order by CR."claimId", E."eventDate" range between unbounded preceding and current row) as "runningIndemnity"
    from "ClaimReserve" CR
        left join "Event" E on E."eventId" = CR."eventId"
    --where CR."claimId" like '15-0%' or CR."claimId" like '14-0%' or CR."claimId" like '13-0%'
    order by CR."claimId"
;
select * from "ISIClaimReserveTotalView";

-- ISILawErrorAggListView
-- creates an aggregate list of law error by instanceId with '~' (tilde) separator
create or replace view "ISILawErrorAggListView" as 
    select 
        ILE."instanceId",
        rtrim(rtrim(xmlagg(xmlelement(E, LE."lawErrorIdISI", '~').extract('//text()') order by ILE."instanceId").GetClobVal(),','), '~') as "lawErrorIdISIList",
        rtrim(rtrim(xmlagg(xmlelement(E, ILE."lawErrorId", '~').extract('//text()') order by ILE."instanceId").GetClobVal(),','), '~') as "lawErrorIdList",
        rtrim(rtrim(xmlagg(xmlelement(E, LE."lawErrorName", '~').extract('//text()') order by ILE."instanceId").GetClobVal(),','), '~') as "lawErrorNameList"
    from "InstanceLawError" ILE
        left join "LawError" LE on LE."lawErrorId" = ILE."lawErrorId"
    group by ILE."instanceId";

select * from "ISILawErrorAggListView";
    
    
-- ISILawAreaAggListView
-- creates an aggregate list of area of practice by instanceId with '~' (tilde) separator
create or replace view "ISILawAreaAggListView" as 
    select
        ILA."instanceId",
        rtrim(rtrim(xmlagg(xmlelement(E, LA."lawAreaId", '~').extract('//text()') order by ILA."instanceId").GetClobVal(),','), '~') as "lawAreaIdISIList",
        rtrim(rtrim(xmlagg(xmlelement(E, LA."lawAreaName", '~').extract('//text()') order by ILA."instanceId").GetClobVal(),','), '~') as "lawAreaNameISIList"
    from "InstanceLawArea" ILA
        left join "LawArea" LA on LA."lawAreaId" = ILA."lawAreaId"
    group by ILA."instanceId"
;

-- ISILitigationTempalteView
-- transposes the LAW Contact table to accommadate the ISI Litigation template
create or replace view "ISILitigationTemplateView" as  
select
    CV."instanceId",
    MIN(DECODE(CV."contactTypeId", 'DefenseAttorney', CV."personId")) as "DefenseAttorney1Id",
    MIN(DECODE(CV."contactTypeId", 'DefenseAttorney', CV."personEntityName")) as "DefenseAttorney1Name",
    MIN(DECODE(CV."contactTypeId", 'DefenseAttorney', CV."organizationId")) as "DefenseFirm1Id",
    MIN(DECODE(CV."contactTypeId", 'DefenseAttorney', CV."organizationEntityName")) as "DefenseFirm1Name",
    MAX(DECODE(CV."contactTypeId", 'DefenseAttorney', CV."personId")) as "DefenseAttorney2Id",
    MAX(DECODE(CV."contactTypeId", 'DefenseAttorney', CV."personEntityName")) as "DefenseAttorney2Name",
    MAX(DECODE(CV."contactTypeId", 'DefenseAttorney', CV."organizationId")) as "DefenseFirm2Id",
    MAX(DECODE(CV."contactTypeId", 'DefenseAttorney', CV."organizationEntityName")) as "DefenseFirm2Name",
    MAX(DECODE(CV."contactTypeId", 'ClaimantAttorney', CV."personId")) as "PlaintiffAttorneyId",
    MAX(DECODE(CV."contactTypeId", 'ClaimantAttorney', CV."personEntityName")) as "PlaintiffAttorneyName",
    MAX(DECODE(CV."contactTypeId", 'ClaimantAttorney', CV."organizationId")) as "PlaintiffFirmId",
    MAX(DECODE(CV."contactTypeId", 'ClaimantAttorney', CV."organizationEntityName")) as "PlaintiffFirmName",
    MAX(DECODE(CV."contactTypeId", 'Expert', CV."personId")) as "ExpertAttorneyId",
    MAX(DECODE(CV."contactTypeId", 'Expert', CV."personEntityName")) as "ExpertAttorneyName",
    MAX(DECODE(CV."contactTypeId", 'Expert', CV."organizationId")) as "ExpertFirmId",
    MAX(DECODE(CV."contactTypeId", 'Expert', CV."organizationEntityName")) as "ExpertFirmName",
    MAX(DECODE(CV."contactTypeId", 'Mediator', CV."personId")) as "MediatorAttorneyId",
    MAX(DECODE(CV."contactTypeId", 'Mediator', CV."personEntityName")) as "MediatorAttorneyName",
    MAX(DECODE(CV."contactTypeId", 'Mediator', CV."organizationId")) as "MediatorFirmId",
    MAX(DECODE(CV."contactTypeId", 'Mediator', CV."organizationEntityName")) as "MediatorFirmName",
    MAX(DECODE(CV."contactTypeId", 'ClaimantProSe', CV."personId")) as "ProSeId",
    MAX(DECODE(CV."contactTypeId", 'ClaimantProSe', CV."personEntityName")) as "ProSeName"
from "ContactView" CV
--where CV."instanceId" like '13-%' or CV."instanceId" like '14-%' or CV."instanceId" like '15-%'
group by CV."instanceId"
order by CV."instanceId"
;
select * from "ISILitigationTemplateView" where "ProSeId" is not NULL;
select * from "ISILitigationTemplateView" where "instanceId" = '14-0123';
select * from "ContactView" where "instanceId" = '14-0123';
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
/*
    ISI_ClaimView
    c_claim
    
    Dependancies: 
    
    NOTES:
     
    History
    01/18/2018  FZ  Created
    02/13/2018  FZ  Added litigationind logic
*/
create or replace view "ISI_ClaimView" as
    select
        C."claimId" as "claimmappingkey",
        0 as "claimvnumber",
        '4112' as "sbuid",
        C."claimId" as "claimkey",
        'LAW_PL_19000101_21121231_4112' as "claimtypekey",
        I."firmId" as "cliententitymappingkey",
        0 as "cliententitykeyvnumber",
        TO_CHAR(I."occurDate", 'YYYYMMDD') as "occurrencedate",
        '000000' as "occurrencetime",
        TO_CHAR(C."claimMadeDate", 'YYYYMMDD') as "reporteddate",  --IMPORTANT 
        '000000' as "reportedtime",
        CASE
            WHEN I."reOpenDate" is not NULL and COALESCE(I."closeDate",I."openDate") < I."reOpenDate" THEN 'R'
            WHEN (I."reOpenDate" is not NULL and I."closeDate" >= I."reOpenDate") or (I."closeDate" is not NULL) THEN 'C'
            ELSE 'O'
         END as "claimstatus",
        TO_CHAR(E."eventDate", 'YYYYMMDDHHMMSS') as "ventrydatetime",
         CASE
            WHEN E."createUserId" = 'Joyce' THEN 'jneumaier'
            WHEN E."createUserId" = 'Matt' THEN 'mbeier'
            WHEN E."createUserId" = 'Katja' THEN 'kkunzke'
            WHEN E."createUserId" = 'Brian' THEN 'banderson'
 --REPLACE once sanderson available           WHEN E."createUserId" = 'Sally' THEN 'sanderson'
            WHEN E."createUserId" = 'Sally' THEN 'banderson'
            ELSE '>>unknown<<'||COALESCE(I."adjuster", 'NULL') --QFC, NDZ, WHD
        END as "vuserid",
        CTP."conversionreference" as "polconversionref",
        CTP."transactiontype" as "poltranstype",
        CTP."transactioneffectivedate" as "poltranseffdate",
        CTP."transactionseqno" as "poltransseqno",
        NULL as "prevclaimno",
        'CST' as "losstimezone",
        'CST' as "reportedtimezone",
        CASE
            WHEN (I."reOpenDate" is not NULL and I."closeDate" >= I."reOpenDate") or (I."closeDate" is not NULL) THEN TO_CHAR(I."closeDate", 'YYYYMMDD')
            ELSE NULL
        END as "closedate",
        CASE
            WHEN I."reOpenDate" is not NULL and COALESCE(I."closeDate", I."coverageDate") < I."reOpenDate" THEN TO_CHAR(I."reOpenDate", 'YYYYMMDD')
            ELSE NULL
        END as "reopendate",
        1 as "chargeableind",
        NULL as "eventid",
        NULL as "occurrenceid",
        0 as "policecalledind",
        NULL as "policefileno",
        NULL as "lossdescription",  --NEED loss description
        NULL as "lossnotes",
        CASE
            WHEN I."reOpenDate" is not NULL and I."closeDate" < I."reOpenDate" THEN 10
            ELSE NULL
        END as "reopenreason",
        1 as "claimsmadeind",
        10 as "reportedmethod", --10-other, 2-fax, 3-phone, 4-email, 5-mail
        30 as "reportingtype", --10=claimant, 20-claimant attorney, 30-insured, 99-other
        NULL as "branch",
        NULL as "refid",  ---?????
        0 as "totallossind",
        NULL as "conversionserver",
        NULL as "sequencenumber",
        NULL as "reopenreasonother",
        TO_CHAR(I."closeDate", 'HHMMSS') as "closetime",
        CASE
            WHEN C."court" is not NULL THEN 1
            ELSE 0
        END as "litigationind", 
        0 as "subrogationchk",
        NULL as "subrogationtext",
        NULL as "packageid",
        NULL as "spoilreasonother",
        NULL as "spoilreason",
        REPLACE(C."claimId", '-', '') as "ouid",
        NULL as "reportedother",
        NULL as "reportingtypeother",
        NULL as "closereason"
    from "Claim" C
        left join "Instance" I on I."instanceId" = C."claimId"
            left join "Policy" P on P."policyId" = I."policyId"
            left join "Event" E on E."eventId" = I."eventId"
        inner join "ISIClaimToPolicyView" CTP on CTP."instanceId" = C."claimId"
 --- SCOPE 
    where P."effectiveDate" > '31-DEC-2005'  
      and I."coverageDate" > '31-DEC-2006'
    -- REQUIRED FIXES
      and C."claimId" not in ('08-0031', '11-0001', '17-0011')

order by C."claimId"
;

select count(*) from "ISI_ClaimView";
select * from "ISI_ClaimView"; where "claimmappingkey" = '08-0001';

/*-- Testing --*/
select count(*) from "Claim";
select * from "ISI_ClaimView";
select * from "ISI_ClaimView" where "polconversionref" is NULL;
select count("claimmappingkey") from "ISI_ClaimView" C;
select E.*, I.* from "Instance" I left join "Event" E on E."eventId" = I."eventId";
select * from "Claim"; -- where "fileStatus" is not NULL;
select * from "Claim" where "claimId" = '10-0123';
select * from "Instance" where "instanceId" = '10-0123';
select * from "Instance";
select * from "ISIConversionReferenceView" where "firmId" in ('YESCLA');
select * from "ISI_PolicyView" where "cliententitymappingkey" = 'NASHSP'; --0611008
select * from "ISI_PolicyView" where "policykey" in ('0511012', '1712025');
select * from "Instance" where "instanceId" in ('05-0071', '17-0003');  --reported=7/24/2006, YESCLA, 0511012
select * from "Policy" where "policyId" in ('AP11627352', 'AP001106847');
select * from "ISI_ClaimView" where "claimmappingkey" = '10-0123'; --05-0065
select * from "ISIClaimToPolicyView" where "instanceId" = '10-0123';
select * from "ISI_PolicyView" where "policykey" = '1004063' and "transactioneffectivedate" = '20100521';

-------------------------------------------------------------------------------------------------------------------
/*
    ISI_InvolvedPartyView
    c_involvedparty
    
    Dependancies: 
    
    NOTES:
        ASCII character exist: http://roubaixinteractive.com/PlayGround/Binary_Conversion/The_Characters.asp
        invno = 1 is Firm
        invno = 2 is Claimant
        invno = >2 is everything else
     
    History
    01/23/2018  FZ  Created
    02/12/2017  FZ  corrected NULL personId for contactentitykey
    02/27/2018  FZ  correct claimant's contactentitykey to be <claimId>||'_Claimant'
    03/02/2018  FZ  default all iscaller=NULL
*/

create or replace view "ISI_ContactClaimantView" as
    select
        C."claimId"||'_Claimant' as "entitykey",
        0 as "entitykeyvnumber",
        NULL as "sbuid",  --deprecated per documentation
        NULL as "firstname",
        NULL as "middlename",
        NULL as "lastname",
        NULL as "nameprefix",
        NULL as "namesuffix",
        NULL as "jobtitle",
        NULL as "fullname",
        I."claimant" as "companyname",
        1 as "preferredhtc",
        0 as "isindividual",
        'CP' as "contacttype",
        NULL as "birthdate",
        NULL as "gender",
        NULL as "maritalstatus",
        NULL as "comments",
        NULL as "conversionserver",
        NULL as "sequencenumber",
        NULL as "linkedentitykey",
        0 as "linkedentitykeyvnumber",
        NULL as "ssn",
        NULL as "hicn",
        NULL as "fein",
        NULL as "tin",
        'EN' as "preferredlanguage"
    from "Claim" C
        left join "Instance" I on I."instanceId" = C."claimId"
            left join "Policy" P on P."policyId" = I."policyId"
-- SCOPE
    where P."effectiveDate" > '31-DEC-2005'
      and I."coverageDate" > '31-DEC-2006'
     -- REQUIRED FIXES
      and C."claimId" not in ('08-0031', '11-0001', '17-0011')
    order by C."claimId"
;

create or replace view "ISI_InvolvedPartyView" as
    -- firm
    select
        C."claimId" as "claimmappingkey",
        0 as "claimvnumber",
        1 as "invno",
        I."firmId" as "contactentitykey", --firmId 
        0 as "contactentitykeyvnumber",
        NULL as "description",
        '4112' as "sbuid",
        'Firm' as "invdescription",
        NULL as "relationnum",
        NULL as "iscaller",
        NULL as "htctime",
        NULL as "repcomments",
        0 as "isclaimant",
        NULL as "adjustermappingkey",
        NULL as "adjustermappingvnumber",
        NULL as "adjusterfileno",
        NULL as "conversionserver",
        NULL as "sequencenumber",
        NULL as "claimantcontacttype",
        1 as "validcontact",
        NULL as "reportingstatus"
    from "Claim" C  
        left join "Instance" I on I."instanceId" = C."claimId"
            left join "Policy" P on P."policyId" = I."policyId"
-- SCOPE
    where P."effectiveDate" > '31-DEC-2005'
      and I."coverageDate" > '31-DEC-2006'
    -- REQUIRED FIXES
      and C."claimId" not in ('08-0031', '11-0001', '17-0011')
    
union all
    -- claimant
    select
        C."claimId" as "claimmappingkey",
        0 as "claimvnumber",
        2 as "invno",
        C."claimId"||'_Claimant' as "contactentitykey",
        0 as "contactentitykeyvnumber",
        I."claimant" as "description",
        '4112' as "sbuid",
        'Claimant' as "invdescription",
        NULL as "relationnum",
        NULL as "iscaller",
        NULL as "htctime",
        NULL as "repcomments",
        1 as "isclaimant",
        NULL as "adjustermappingkey",
        NULL as "adjustermappingvnumber",
        NULL as "adjusterfileno",
        NULL as "conversionserver",
        NULL as "sequencenumber",
        'T1' as "claimantcontacttype",
        1 as "validcontact",
        NULL as "reportingstatus"
    from "Claim" C
        left join "Instance" I on I."instanceId" = C."claimId"
            left join "Policy" P on P."policyId" = I."policyId"
-- SCOPE
    where P."effectiveDate" > '31-DEC-2005'
      and I."coverageDate" > '31-DEC-2006'
     -- REQUIRED FIXES
      and C."claimId" not in ('08-0031', '11-0001', '17-0011')
union all
    -- person 
    select
        C."claimId" as "claimmappingkey",
        0 as "claimvnumber",
        CV."contactSeq" + 2 as "invno",
        CV."personId" as "contactentitykey",
        /*
        CASE
            WHEN CV."personId" IS NOT NULL THEN CV."personId" --personId vs organizationId
            WHEN L."mainEntityId" is not NULL THEN L."mainEntityId" --fixes #91
            ELSE 'UNKNOWN'
        END  as "contactentitykey",
        */
        0 as "contactentitykeyvnumber",
        NULL as "description",
        '4112' as "sbuid",
        CV."contactTypeId" as "invdescription",
        NULL as "relationnum",
        NULL as "iscaller",
        NULL as "htctime",
        NULL as "repcomments",
        0 as "isclaimant",
        NULL as "adjustermappingkey",
        NULL as "adjustermappingvnumber",
        NULL as "adjusterfileno",
        NULL as "conversionserver",
        NULL as "sequencenumber",
        CASE
            WHEN CV."contactTypeId" in ('Claimant', 'ClaimantProSe') THEN 'T1'
            ELSE NULL
        END as "claimantcontacttype",
        1 as "validcontact",
        NULL as "reportingstatus"
    from "Claim" C
        inner join (select * from "ContactView" where "personId" is not NULL) CV on CV."instanceId" = C."claimId"  --inner join accounts for no contact records (ie: 14-0171 Test claim)
            left join "Instance" I on I."instanceId" = CV."instanceId"
            left join "Location" L on L."locationId" = CV."locationId"
                left join "Policy" P on P."policyId" = I."policyId"
-- SCOPE
    where P."effectiveDate" > '31-DEC-2005'
      and I."coverageDate" > '31-DEC-2006'
      -- REQUIRED FIXES
      and C."claimId" not in ('08-0031', '11-0001', '17-0011')
union all
    -- organization
    select
        C."claimId" as "claimmappingkey",
        0 as "claimvnumber",
        CV."contactSeq" + 42 as "invno",
        CV."organizationId" as "contactentitykey",
        0 as "contactentitykeyvnumber",
        NULL as "description",
        '4112' as "sbuid",
        CV."contactTypeId" as "invdescription",
        NULL as "relationnum",
        NULL as "iscaller",
        NULL as "htctime",
        NULL as "repcomments",
        0 as "isclaimant",
        NULL as "adjustermappingkey",
        NULL as "adjustermappingvnumber",
        NULL as "adjusterfileno",
        NULL as "conversionserver",
        NULL as "sequencenumber",
        CASE
            WHEN CV."contactTypeId" in ('Claimant', 'ClaimantProSe') THEN 'T1'
            ELSE NULL
        END as "claimantcontacttype",
        1 as "validcontact",
        NULL as "reportingstatus"
    from "Claim" C
        inner join (select * from "ContactView" where "organizationId" is not NULL) CV on CV."instanceId" = C."claimId"  --inner join accounts for no contact records (ie: 14-0171 Test claim)
            left join "Instance" I on I."instanceId" = CV."instanceId"
            left join "Location" L on L."locationId" = CV."locationId"
                left join "Policy" P on P."policyId" = I."policyId"
-- SCOPE
    where P."effectiveDate" > '31-DEC-2005'
      and I."coverageDate" > '31-DEC-2006'
     -- REQUIRED FIXES
      and C."claimId" not in ('08-0031', '11-0001', '17-0011')
order by "claimmappingkey", "invno"
;
/*--- Testing ---*/
select * from "ISI_InvolvedPartyView" where "claimmappingkey" in ('14-0171', '14-0123', '06-0076', '08-0001');
select * from "ContactView" where "instanceId" in ('14-0171', '14-0123', '06-0076');

--------------------------------------------------------------------------------
/*
    ISI_InvolvedRelationView
    c_involvedrelation
    
    Dependancies: 
    
    NOTES:
     
    History
    01/23/2018  FZ  Created
    03/02/2018  FZ  ClaimantAttorney = PAT
*/
create or replace view "ISI_InvolvedRelationView" as
    select
        CV."claimmappingkey" as "claimmappingkey",
        CV."claimvnumber" as "claimvnumber",
        CV."invno",
        CASE 
            ---select * from s_list_values_mapping where listname = 'ClaimPartyType' order by listname;
            --03/21/2018 notes insured=claim contact, other  letertoother=other
            WHEN CV."invdescription" in ('DefenseAttorney', 'OutOfStateDefense') and CV."invno" < 43 THEN 'DAT'  --defense lawyer
            WHEN CV."invdescription" in ('DefenseAttorney', 'OutOfStateDefense') and CV."invno" > 43 THEN 'DFM'  --defense firm
            WHEN CV."invdescription" in ('ClaimantAttorney') and CV."invno" < 43 THEN 'PAT' --plaintiff attorney
            WHEN CV."invdescription" in ('ClaimantAttorney') and CV."invno" > 43 THEN 'PFM' --plaintiff firm
            WHEN CV."invdescription" in ('Expert') THEN 'EX' --Expert
            WHEN CV."invdescription" in ('Mediator') THEN 'MED'  --mediator
            WHEN CV."invdescription" in ('Claimant', 'ClaimantProSe') THEN 'CL' --third party claimant
            WHEN CV."invdescription" in ('CoverageCounsel') THEN 'COVC'  --coverage counsel
            WHEN CV."invdescription" in ('AccountingConsultant', 'BankruptcyConsultant', 'Consultant', 'CriminalConsultant', 'FamilyConsultant', 'InsuranceConsultant', 'LitigationConsultant', 'PIDefConsultant', 'PIPlaintConsultant', 'ProbateConsultant') THEN 'CONS' --consultant
            WHEN CV."invdescription" in ('AON') THEN 'BRO' --broker
            WHEN CV."invdescription" in ('Firm', 'Tortfeasor') THEN 'FPO' --insured/first party involved 
            WHEN CV."invdescription" in ('InsuredAttorney') THEN 'IATT'  --insured attorney
            WHEN CV."invdescription" in ('DeductibleReimburse') THEN 'DPA' --deductible payor
            WHEN CV."invdescription" in ('GrievanceCounsel') THEN 'GRVC'  --grievance counsel
            WHEN CV."invdescription" in ('RepairCounsel') THEN 'RCOU' --repair counsel
            WHEN CV."invdescription" in ('BillingAddress') THEN 'PY' --payee
            WHEN CV."invdescription" in ('Insured') THEN '1' --claim contact
            ELSE 'OT'               
        END as "relationtype",      
        NULL as "conversionserver",
        NULL as "sequencenumber"
    from "ISI_InvolvedPartyView" CV
;

/*--- Testing ---*/
select * from "ISI_InvolvedRelationView" where "claimmappingkey" in ('14-0171', '14-0123', '06-0076');

--------------------------------------------------------------------------------
/*
    ISI_ClaimantDetailsView
    c_claimantdetails
    
    Dependancies: 
    
    NOTES:
     
    History
    01/24/2018  FZ  Created
    02/12/2018  FZ  Added lawsuitind logic
    03/02/2018  FZ  change clmttype default to 10
*/
create or replace view "ISI_ClaimantDetailsView" as
    select 
        IPV."claimmappingkey" as "claimmappingkey",
        IPV."claimvnumber" as "claimvnumber",
        IPV."invno" as "invno",
        '10' as "clmttype", --10=client, 20=non-client, 99-other   --not sure if this is law firm client or ISI client (ie: insured)
        0 as "involvedtype", --directly involved....not sure how to handle GC claims when claimant is insured
        NULL as "percentatfault",
        NULL as "losstransfer",
        NULL as "insuredbyid",
        NULL as "insuredbyvnumber",
        0 as "potentialrecovery",
        CASE
            WHEN C."court" is not NULL THEN 1
            ELSE 0
        END as "lawsuitind", 
        NULL as "conversionserver",
        NULL as "sequencenumber"
    from (select * from "ISI_InvolvedPartyView" where "invno" = 2) IPV
        left join "Claim" C on C."claimId" = IPV."claimmappingkey"
;

/*--- Testing ---*/
select * from "ISI_ClaimantDetailsView";
--------------------------------------------------------------------------------
/*
    ISI_ClaimantSubFileView
    c_claimantsubfile
    
    Dependancies:
    
    NOTES:
    
    History:
    02/21/2018  FZ  Created
*/
create or replace view "ISI_ClaimantSubFileView" as
    select
        CD."claimmappingkey" as "claimmappingkey",
        CD."claimvnumber" as "claimvnumber",
        CD."invno" as "invno",
        'LAW_PL_19000101_21121231_4112' as "claimtypekey",
        'LAW_PL_19000101_21121231_4112_IND' as "subfilekey",
        '4112' as "sbuid",
        NULL as "conversionserver",
        NULL as "sequencenumber"
    from "ISI_ClaimantDetailsView" CD
;
    
/*--- Testing ---*/

--------------------------------------------------------------------------------
/*
    ISI_ClaimItemView
    c_claimitem
    
    Dependancies: 
    
    NOTES:
     
    History
    01/24/2018  FZ  Created
    02/24/2018  FZ  fix duplicate recs from LPL & GC claims
*/
--05/05/2018 needed to revised the separation by coverage (LPL, GC, DF)
--02/24/2018 version

drop view "ISI_ClaimItemView";

create or replace view "ISI_ClaimItemLPLView" as
    -- liability, GC, DF and policy coverage 
    select unique
        C."reporteddate" as "CLAIMMADEDATE",
        C."claimmappingkey" as "claimmappingkey",
        0 as "claimvnumber",
        C."polconversionref" as "polconversionref",
        --PTC."conversionreference" as "polconversionref",
        --C2."polconversionref" as "polconversionref",
        C."poltranstype" as "poltranstype",
        --PTC."transactiontype" as "poltranstype",
        --C2."poltranstype" as "poltranstype",
        C."poltranseffdate" as "poltranseffdate",
        --PTC."transactioneffectivedate" as "poltranseffdate",
        --C2."poltranseffdate" as "poltranseffdate",
        C."poltransseqno" as "poltransseqno",
        --PTC."transactionseqno" as "poltransseqno",
        --C2."poltransseqno" as "poltransseqno",
        C."sbuid" as "sbuid",
        LPL."packageid" as"packageid",
        LPL."riskid" as "riskid",
        --LPL."linenumber" as "linenumber",
        0 as "linenumber",  --not sure what this refer 
        NULL as "conversionserver",
        NULL as "sequencenumber"
    from "ISI_ClaimView" C
         left join "ClaimCoverage" CC on CC."claimId" = C."claimmappingkey"
            inner join (select * from "Coverage" where "coverageTypeId" in ('Liability', 'Policy', 'Cvg-DF', 'Cvg-GC')) CV on CV."coverageId" = CC."coverageId"
                --left join "ISIClaimToPolicyView" PTC on PTC."instanceId" = C."claimId"
                    --left join "ISI_ItemFirmView" LPL on LPL."POLICYNUMBER" = PTC."policyNumber" and LPL."transactioneffectivedate" = PTC."transactioneffectivedate" and LPL."transactionseqno" = PTC."transactionseqno"
                    left join "ISI_ItemFirmView" LPL on LPL."conversionreference" = C."polconversionref" and LPL."transactioneffectivedate" = C."poltranseffdate" and LPL."transactionseqno" = C."poltransseqno" -- 05/27/2018
                    --05/05/2018 left join "ISI_ItemFirmView" LPL on LPL."POLICYNUMBER" = PTC."policyNumber" and LPL."transactioneffectivedate" = PTC."transactioneffectivedate" and LPL."riskid" = 1
        left join "Instance" I on I."instanceId" = C."claimmappingkey"
            left join "Policy" P on P."policyId" = I."policyId"
        --join "ISIClaimToPolicyView" CTP on CTP."instanceId" = C."claimId"                     
    --- SCOPE 
   where P."effectiveDate" > '31-DEC-2005'  
     and I."coverageDate" > '31-DEC-2006'
     -- REQUIRED FIXES
     and C."claimmappingkey" not in ('08-0031', '11-0001', '17-0011')
order by "claimmappingkey"
;
select * from "ISIClaimToPolicyView" where "instanceId" = '06-0033';
select * from "ISI_ClaimItemLPLView" where "claimmappingkey" = '12-0021';
select * from "ISI_ItemFirmView" where "transactioneffectivedate" = '20060701' and "transactionseqno" = 2 and "transactiontype" = 'END' and "conversionreference" = 'LAROGE_2006_2018';


/*--------  NOT REQUIRED  ---------*/
create or replace view "ISI_ClaimItemGCView" as
    -- grievance counsel coverage
    select
        C."reporteddate" as "CLAIMMADEDATE",
        C."claimmappingkey" as "claimmappingkey",
        0 as "claimvnumber",
        C."polconversionref" as "polconversionref",
        C."poltranstype" as "poltranstype",
        C."poltranseffdate" as "poltranseffdate",
        C."poltransseqno" as "poltransseqno",
        C."sbuid" as "sbuid",
        LPL."packageid" as"packageid",
        LPL."riskid" as "riskid",
        --LPL."linenumber" as "linenumber",
        0 as "linenumber",  --not sure what this refer 
        NULL as "conversionserver",
        NULL as "sequencenumber"
    from "ISI_ClaimView" C
        left join "ClaimCoverage" CC on CC."claimId" = C."claimmappingkey"
            inner join (select * from "Coverage" where "coverageTypeId" in ('Cvg-GC')) CV on CV."coverageId" = CC."coverageId"
                --left join "ISIClaimToPolicyView" PTC on PTC."instanceId" = C."claimId"
                    --left join "ISI_ItemFirmView" LPL on LPL."converPOLICYNUMBER" = PTC."policyNumber" and LPL."EVENTTYPEID" = 'Cvg-GC'
        left join "ISI_ItemFirmView" LPL on LPL."conversionreference" = C."polconversionref" and LPL."transactioneffectivedate" = C."poltranseffdate" and LPL."transactionseqno" = C."poltransseqno" -- 05/27/2018       
        left join "Instance" I on I."instanceId" = C."claimmappingkey"  
            left join "Policy" P on P."policyId" = I."policyId"
    --- SCOPE 
   where P."effectiveDate" > '31-DEC-2005'
     and I."coverageDate" > '31-DEC-2006'
     -- REQUIRED FIXES
     and C."claimmappingkey" not in ('08-0031', '11-0001', '17-0011')
order by "claimmappingkey"
;
select * from "ISI_ClaimItemGCView";

/*--------  NOT REQUIRED  ---------*/
create or replace view "ISI_ClaimItemDFView" as
    -- defendant reimbursement
    select
        C."claimMadeDate" as "CLAIMMADEDATE",
        C."claimId" as "claimmappingkey",
        0 as "claimvnumber",
        PTC."conversionreference" as "polconversionref",
        PTC."transactiontype" as "poltranstype",
        PTC."transactioneffectivedate" as "poltranseffdate",
        PTC."transactionseqno" as "poltransseqno",
        LPL."sbuid" as "sbuid",
        LPL."packageid" as"packageid",
        LPL."riskid" as "riskid",
        --LPL."linenumber" as "linenumber",
        0 as "linenumber",  --not sure what this refer 
        NULL as "conversionserver",
        NULL as "sequencenumber"
    from "Claim" C
         left join "ClaimCoverage" CC on CC."claimId" = C."claimId"
            inner join (select * from "Coverage" where "coverageTypeId" in ('Cvg-DF')) CV on CV."coverageId" = CC."coverageId"
                left join "ISIClaimToPolicyView" PTC on PTC."instanceId" = C."claimId"
                    left join "ISI_ItemFirmView" LPL on LPL."POLICYNUMBER" = PTC."policyNumber" and LPL."EVENTTYPEID" = 'Cvg-DF' 
         left join "Instance" I on I."instanceId" = C."claimId"
    --- SCOPE 
   where PTC."effectiveDate" > '31-DEC-2005'  
     and I."coverageDate" > '31-DEC-2006'
     -- REQUIRED FIXES
     and C."claimId" not in ('08-0031', '11-0001', '17-0011')
order by "claimmappingkey"  
;
select * from "ISI_ClaimItemDFView";
*/
-->>>>> ADD: Coverage Counsel ULAE
-->>>>> ADD: LawyerTail
-->>>>> ADD: Tail


 
/*--- Testing ---*/
select * from "ISI_PolicyItemView";
select * from "Claim";
select * from "Instance";
select * from "ISIClaimToPolicyLPLView";
select * from "ISI_ClaimView" where "claimkey" = '05-0093'; --05-0093
select * from "ISI_ClaimItemView" order by "claimmappingkey";
select * from "ISI_ClaimItemView" where "claimmappingkey" = '05-0093' order by "claimmappingkey";
    
--------------------------------------------------------------------------------
/*
    ISI_ClaimantCoverageView
    c_claimantcoverage
    
    Dependancies: 
    
    NOTES:
     
    History
    01/24/2018  FZ  Created
    02/13/2018  FZ  Added litigationind logic
*/
create or replace view "ISI_ClaimantCoverageView" as
    select
        C."claimId" as "claimmappingkey",
        0 as "claimvnumber",
        C2."polconversionref" as "polconversionref",
        LPL."new_transactiontype" as "poltranstype",
        --C2."poltranstype" as "poltranstype",
        LPL."transactioneffectivedate" as "poltranseffdate",
        --C2."poltranseffdate" as "poltranseffdate",
        LPL."new_transactionseqno" as "poltransseqno",
        --C2."poltransseqno" as "poltransseqno",
        LPL."packageid" as "packageid",
        LPL."riskid" as "riskid",
        LPL."linenumber" as "linenumber",
        LPL."coverageid" as "coverageid",
        '2' as "invno",
        'LAW_PL_19000101_21121231_4112_IND' as "subfilekey",
        'LAW_PL_19000101_21121231_4112' as "claimtypekey",
        LPL."sbuid" as "sbuid",
        'LPLI' as "kindofloss", --LPL=LPLI, perdiem=PERD, pre-suit assistance=PRES, subpoena assistance=SUBP
        'NA' as "classofloss",
        'NA' as "basisofloss",
        'NA' as "losscontrolevent",
        C."perClaimDeductible" as "deductible",
        1 as "deductibleind",  --Yes
        NULL as "deductibleinvoiced", --NOTE:
        NULL as "reservenumber",
        NULL as "reservestatus",
        NULL as "spoilreason",
        NULL as "spoilreasonother",
        CASE
            WHEN I."reOpenDate" is NOT NULL and I."reOpenDate" > COALESCE(I."closeDate", I."coverageDate") THEN 10 --30=other, 5=approved reopen
            ELSE NULL
        END as "reopenreason",
        NULL as "reopenreasonother",
        CASE
            WHEN C."court" is not NULL THEN 1
            ELSE 0
        END as "litigationind", 
        NULL as "siuind",
        NULL as "salvageind",
        NULL as "subrogationind",
        NULL as "conversionserver",
        NULL as "sequencenumber"
    from "ISI_ClaimView" C2  --Claim    
      left join "Claim" C on C."claimId" = C2."claimmappingkey"
        left join "ClaimCoverage" CC on CC."claimId" = C."claimId" --claimId
            inner join (select * from "Coverage" where "coverageTypeId" in ('Liability', 'Policy')) CV on CV."coverageId" = CC."coverageId"
            -- 05/27/2018 left join "ISIClaimToPolicyView" PTC on PTC."instanceId" = CC."claimId"
            --    left join "ISI_CoverageLPLFirmView" LPL on LPL."POLICYNUMBER" = PTC."policyNumber" and LPL."transactioneffectivedate" = PTC."transactioneffectivedate" and LPL."transactionseqno" = PTC."transactionseqno" and LPL."riskid" = 1 
        left join "ISI_CoverageLPLFirmAllView" LPL on LPL."conversionreference" = C2."polconversionref" and LPL."transactioneffectivedate" = C2."poltranseffdate" and LPL."new_transactionseqno" = C2."poltransseqno" and LPL."EVENTTYPEID" in ('PolicyIssue', 'Cvg-DA', 'Cvg-CP', 'Cvg-EC', 'Cvg-ED', 'Cvg-EU')
            left join "Instance" I on I."instanceId" = CC."claimId"
                left join "Policy" P on P."policyId" = I."policyId"
    --- SCOPE
    where P."effectiveDate" > '31-DEC-2005' 
      and I."coverageDate" > '31-DEC-2006'
       -- REQUIRED FIXES
      and C."claimId" not in ('08-0031', '11-0001', '17-0011')
    order by C."claimId"
;

select * from "ISI_CoverageLPLFirmAllView" where "conversionreference" = 'YAKEBA_1987_2018' and "transactioneffectivedate" = '20071101';
select * from "ISIClaimToPolicyView" where "conversionreference" = 'YAKEBA_1987_2018';



create or replace view "ISI_ClaimantCoverageGCView" as
    -- grievance counsel coverage
    select
        C."claimId" as "claimmappingkey",
        0 as "claimvnumber",
        PTC."conversionreference" as "polconversionref",
        LPL."new_transactiontype" as "poltranstype",
        LPL."transactioneffectivedate" as "poltranseffdate",
        LPL."new_transactionseqno" as "poltransseqno",
        LPL."packageid" as "packageid",
        LPL."riskid" as "riskid",
        LPL."linenumber" as "linenumber",
        LPL."coverageid" as "coverageid",
        '2' as "invno",
        'LAW_PL_19000101_21121231_4112_IND' as "subfilekey",
        'LAW_PL_19000101_21121231_4112' as "claimtypekey",
        LPL."sbuid" as "sbuid",
        'PRES' as "kindofloss", --LPL=LPLI, perdiem=PERD, pre-suit assistance=PRES, subpoena assistance=SUBP
        --'ClaimKindOfLoss_LA' as "kindofloss",
        'NA' as "classofloss",
        'NA' as "basisofloss",
        'NA' as "losscontrolevent",
        C."perClaimDeductible" as "deductible",
        1 as "deductibleind",  --Yes
        NULL as "deductibleinvoiced", --NOTE:
        NULL as "reservenumber",
        NULL as "reservestatus",
        NULL as "spoilreason",
        NULL as "spoilreasonother",
        CASE
            WHEN I."reOpenDate" is NOT NULL and I."reOpenDate" > COALESCE(I."closeDate", I."coverageDate") THEN 10 --30=other, 5=approved reopen
            ELSE NULL
        END as "reopenreason",
        NULL as "reopenreasonother",
        CASE
            WHEN C."court" is not NULL THEN 1
            ELSE 0
        END as "litigationind", 
        NULL as "siuind",
        NULL as "salvageind",
        NULL as "subrogationind",
        NULL as "conversionserver",
        NULL as "sequencenumber"
    --from "Claim" C
    from "ISI_ClaimView" C2  --Claim
      left join "Claim" C on C."claimId" = C2."claimmappingkey"
        left join "ClaimCoverage" CC on CC."claimId" = C."claimId"
            inner join (select * from "Coverage" where "coverageTypeId" in ('Cvg-GC')) CV on CV."coverageId" = CC."coverageId"
            left join "ISIClaimToPolicyView" PTC on PTC."instanceId" = CC."claimId"
                --left join "ISI_CoverageLPLFirmView" LPL on LPL."POLICYNUMBER" = PTC."policyNumber" and LPL."transactioneffectivedate" = PTC."transactioneffectivedate" and LPL."transactionseqno" = PTC."transactionseqno" ---and LPL."EVENTTYPEID" = 'Cvg-GC' 
            left join "ISI_CoverageLPLFirmAllView" LPL on LPL."conversionreference" = C2."polconversionref" and LPL."transactioneffectivedate" = C2."poltranseffdate" and LPL."new_transactionseqno" = C2."poltransseqno" and LPL."EVENTTYPEID" in ('Cvg-GC')
            left join "Instance" I on I."instanceId" = CC."claimId"
                left join "Policy" P on P."policyId" = I."policyId"
    --- SCOPE
    where P."effectiveDate" > '31-DEC-2005' 
      and I."coverageDate" > '31-DEC-2006'
       -- REQUIRED FIXES
      and C."claimId" not in ('08-0031', '11-0001', '17-0011')
    order by C."claimId"
;
select * from "ISI_ClaimantCoverageGCView" where "claimmappingkey" = '08-0052';
select * from "ISI_ClaimantCoverageGCView" where "claimmappingkey" = '17-0012';
select * from "ISIClaimToPolicyView" where "conversionreference" = 'DELANA_2008_2018' and "transactioneffectivedate" = '20110901';
select * from  "ISI_CoverageLPLFirmView" where "POLICYNUMBER" = '0607129';
select * from "ISI_CoverageLPLFirmAllView" where "conversionreference" = 'DELANA_2008_2018' and "transactioneffectivedate" = '20110901';
select * from "ISI_InvolvedPartyView" where "claimmappingkey" = '08-0001';
select * from "ISI_ClaimantCoverageView" where "claimmappingkey" = '12-0021';
select * from "ISI_ClaimantCoverageGCView" where "claimmappingkey" = '09-0119';
select * from "ISI_Policy" where "conversionreference" = 'DELANA_2008_2018' and "transactioneffectivedate" = '20110901';

create or replace view "ISI_ClaimantCoverageDFView" as
    -- defendant reimbursement coverage
    select
        C."claimmappingkey" as "claimmappingkey",
        0 as "claimvnumber",
        PTC."conversionreference" as "polconversionref",
        LPL."new_transactiontype" as "poltranstype",
        PTC."transactioneffectivedate" as "poltranseffdate",
        LPL."new_transactionseqno" as "poltransseqno",
        LPL."packageid" as "packageid",
        LPL."riskid" as "riskid",
        LPL."linenumber" as "linenumber",
        LPL."coverageid" as "coverageid",
        '2' as "invno",
        'LAW_PL_19000101_21121231_4112_IND' as "subfilekey",
        'LAW_PL_19000101_21121231_4112' as "claimtypekey",
        LPL."sbuid" as "sbuid",
        'PERD' as "kindofloss", --LPL=LPLI, perdiem=PERD, pre-suit assistance=PRES, subpoena assistance=SUBP
        --'ClaimKindOfLoss_LA' as "kindofloss",
        'NA' as "classofloss",
        'NA' as "basisofloss",
        'NA' as "losscontrolevent",
        CL."perClaimDeductible" as "deductible",
        1 as "deductibleind",  --Yes
        NULL as "deductibleinvoiced", --NOTE:
        NULL as "reservenumber",
        NULL as "reservestatus",
        NULL as "spoilreason",
        NULL as "spoilreasonother",
        CASE
            WHEN I."reOpenDate" is NOT NULL and I."reOpenDate" > COALESCE(I."closeDate", I."coverageDate") THEN 5 --30=other, 5=approved reopen
            ELSE NULL
        END as "reopenreason",
        NULL as "reopenreasonother",
        CASE
            WHEN CL."court" is not NULL THEN 1
            ELSE 0
        END as "litigationind", 
        NULL as "siuind",
        NULL as "salvageind",
        NULL as "subrogationind",
        NULL as "conversionserver",
        NULL as "sequencenumber"
    from "ISI_ClaimView" C
        left join "Claim" CL on CL."claimId" = C."claimmappingkey"
        left join "ClaimCoverage" CC on CC."claimId" = C."claimmappingkey"
            inner join (select * from "Coverage" where "coverageTypeId" in ('Cvg-DF')) CV on CV."coverageId" = CC."coverageId"
            left join "ISIClaimToPolicyView" PTC on PTC."instanceId" = CC."claimId"
                --left join "ISI_CoverageDRFirmView" LPL on LPL."POLICYNUMBER" = PTC."policyNumber" and LPL."transactioneffectivedate" = PTC."transactioneffectivedate" and LPL."transactionseqno" = PTC."transactionseqno" and LPL."riskid" = 1 
                left join "ISI_CoverageLPLFirmAllView" LPL on LPL."conversionreference" = C."polconversionref" and LPL."new_transactioneffectivedate" = C."poltranseffdate" and LPL."new_transactionseqno" = C."poltransseqno" and LPL."EVENTTYPEID" in ('Cvg-DF')
            left join "Instance" I on I."instanceId" = CC."claimId"
    --- SCOPE
    where PTC."effectiveDate" > '31-DEC-2005' 
      and I."coverageDate" > '31-DEC-2006'
       -- REQUIRED FIXES
      and C."claimmappingkey" not in ('08-0031', '11-0001', '17-0011')
    order by C."claimmappingkey"
;
select * from "ISI_CoverageLPLFirmView" where "conversionreference" = 'WALSKE_2003_2018' and "transactioneffectivedate" = '20120801';
select * from "ISI_CoverageLPLFirmAllView" where "conversionreference" = 'WALSKE_2003_2018' and "transactioneffectivedate" = '20120801';
select * from "ISIClaimToPolicyView" where "conversionreference" = 'WALSKE_2003_2018';

-- Coverage Counsel ULAE
create or replace view "ISI_ClaimantCoverageULAEView" as
    select
        C."claimId" as "claimmappingkey",
        0 as "claimvnumber",
        PTC."conversionreference" as "polconversionref",
        LPL."transactiontype" as "poltranstype",
        PTC."transactioneffectivedate" as "poltranseffdate",
        LPL."transactionseqno" as "poltransseqno",
        LPL."packageid" as "packageid",
        LPL."riskid" as "riskid",
        LPL."linenumber" as "linenumber",
        LPL."coverageid" as "coverageid",
        '2' as "invno",
        'LAW_PL_19000101_21121231_4112_COV' as "subfilekey",
        'LAW_PL_19000101_21121231_4112' as "claimtypekey",
        LPL."sbuid" as "sbuid",
        'LPLI' as "kindofloss", --LPL=LPLI, perdiem=PERD, pre-suit assistance=PRES, subpoena assistance=SUBP
        'NA' as "classofloss",
        'NA' as "basisofloss",
        'NA' as "losscontrolevent",
        0 as "deductible",
        0 as "deductibleind",  --No
        NULL as "deductibleinvoiced", --NOTE:
        NULL as "reservenumber",
        NULL as "reservestatus",
        NULL as "spoilreason",
        NULL as "spoilreasonother",
        CASE
            WHEN I."reOpenDate" is NOT NULL and I."reOpenDate" > I."closeDate" THEN 5 --30=other, 5=approved reopen
            ELSE NULL
        END as "reopenreason",
        NULL as "reopenreasonother",
        CASE
            WHEN C."court" is not NULL THEN 1
            ELSE 0
        END as "litigationind", 
        NULL as "siuind",
        NULL as "salvageind",
        NULL as "subrogationind",
        NULL as "conversionserver",
        NULL as "sequencenumber"
    from (select unique "instanceId" from "InstancePayment" where "paymentGeneralLegal" > 0) IP
        left join "Claim" C on C."claimId" = IP."instanceId"
            left join "ISIClaimToPolicyView" PTC on PTC."instanceId" = C."claimId"
                left join "ISI_CoverageLPLFirmView" LPL on LPL."POLICYNUMBER" = PTC."policyNumber" and LPL."transactioneffectivedate" = PTC."transactioneffectivedate" and LPL."transactionseqno" = PTC."transactionseqno" and LPL."riskid" = 1 
        left join "Instance" I on I."instanceId" = IP."instanceId"
    --- SCOPE
    where PTC."effectiveDate" > '31-DEC-2005' 
      and I."coverageDate" > '31-DEC-2006'
    -- REQUIRED FIXES
      and C."claimId" not in ('08-0031', '11-0001', '17-0011')  --11-0088
    order by C."claimId"
;

select * from "ISI_CoverageGCFirmView" where "conversionreference" = 'KNUTPO_2010_2019';-- and "transactioneffectivedate" = '20120915';
-->>>>> ADD: LawyerTail
-->>>>> ADD: Tail
   
    
/*--- Testing ---*/
select * from "ISI_ClaimantCoverageView";
select * from "ISI_ClaimantCoverageView" where "claimmappingkey" = '05-0093';
select * from "ClaimCoverage";
select * from "Claim";
select * from "Instance";

--------------------------------------------------------------------------------
/*
    ISI_ClaimExaminerView
    c_claimexaminer
    
    Dependancies: 
    
    NOTES:
     
    History
    01/25/2018  FZ  Created
*/
create or replace view "ISI_ClaimExaminerView" as
    select
        C."claimId" as "claimmappingkey",
        0 as "claimvnumber",
        CASE
            WHEN I."adjuster" = 'Matt' THEN 'mbeier'
            WHEN I."adjuster" = 'Katja' THEN 'kkunzke'
            WHEN I."adjuster" = 'Brian' THEN 'banderson'
            WHEN I."adjuster" = 'Sally' THEN 'sanderson'
            WHEN I."adjuster" = 'Sally' THEN 'banderson'
            ELSE '>>unknown<<'||COALESCE(I."adjuster", 'NULL') --QFC, NDZ, WHD
        END as "userentitymappingkey",
        '4112' as "sbuid",
        NULL as "conversionserver",
        NULL as "sequencenumber"
    from "Claim" C
        left join "Instance" I on I."instanceId" = C."claimId"
            left join "Policy" P on P."policyId" = I."policyId"
    --- SCOPE
    where P."effectiveDate" > '31-DEC-2005'
      and I."coverageDate" > '31-DEC-2006'
      -- REQUIRED FIXES
      and C."claimId" not in ('08-0031', '11-0001', '17-0011')  --11-0088
    order by C."claimId"
;    
    
/*--- Testing ---*/
select * from "ISI_ClaimaExaminerView";
select unique("adjuster") from "Instance";
--------------------------------------------------------------------------------
/*
    ISI_ClaimExaminerSubfileView
    c_claimexaminersubfile
    
    Dependencies:
        ISI_ClaimExaminerView
        
    History
    05/02/2018  FZ  Created
*/
create or replace view "ISI_ClaimExaminerSubfileView" as 
    select
        CE."claimmappingkey" as "claimmappingkey",
        CE."claimvnumber" as "claimvnumber",
        CE."userentitymappingkey" as "userentitymappingkey",
        CE."sbuid" as "sbuid",
        'LAW_PL_19000101_21121231_4112_IND' as "subfilekey",
        'LAW_PL_19000101_21121231_4112' as "claimtypekey",
        NULL as "conversionserver",
        NULL as "sequencenumber"
    from "ISI_ClaimExaminerView" CE
    ;
        

--------------------------------------------------------------------------------

/*
    ISI_ClaimTransactionView
    c_transaction
    
    Dependancies: 
    
    NOTES:
     
    History
    01/25/2018  FZ  Created
    02/26/2018  FZ  paymentkind - E=expense, L=indemnity
*/
create or replace view "ISI_ClaimTransactionView" as
    -- alae reserves transactions
    select        
        CR."claimId" as "claimmappingkey",
        0 as "claimvnumber",
        CR."claimReserveId"||'_ALAE' as "transmappingkey",
        CR."reserveSeq" as "transactionumber",
        ---GLT."glTransactionId" as "gltransnumber",
        regexp_replace(GLT."glTransactionId", '[^0-9]', '') as "gltransnumber",
        TO_CHAR(E."createDate", 'YYYYMMDD') as "ventrydate",
        TO_CHAR(E."eventDate", 'YYYYMMDD') as "accountingdate",
        CASE
            WHEN E."eventTypeId" like 'ClmRes%' THEN 3
            WHEN E."eventTypeId" = 'Reo' THEN 0  -- reopen
            WHEN E."eventTypeId" = 'Opn' THEN 0
            WHEN E."eventTypeId" = 'Cls' THEN 0
            WHEN E."eventTypeId" = 'Adj' THEN 0
            ELSE 9
        END as "action",
        CR."claimReserveId" as "reference",
        'USD' as "currencycd",
        NULL as "description",
        CR."estimateALAE" as "transamt",
        CASE
            WHEN E."createUserId" = 'Joyce' THEN 'jneumaier'
            WHEN E."createUserId" = 'Matt' THEN 'mbeier'
            WHEN E."createUserId" = 'Katja' THEN 'kkunzke'
            WHEN E."createUserId" = 'Brian' THEN 'banderson'
            WHEN E."createUserId" = 'Sally' THEN 'sanderson'
            ELSE '>>unknown<<'||COALESCE(E."createUserId", 'NULL') --QFC, NDZ, WHD
        END as "userentitymappingkey",
        '4112' as "sbuid",
        NULL as "chequeserieskey",
        NULL as "chequepayeename",
        NULL as "chequemailtoaddress",
        NULL as "mailingentitykey",
        NULL as "mailingentitykeyvnumber",
        NULL as "paymentmethod",
        NULL as "paymenttype",
        NULL as "paymentkind",
        NULL as "expensetype",
        NULL as "chequetype",
        NULL as "chequeno",
        NULL as "recoverytype",
        NULL as "depositno",
        'I' as "transtatus", --I=issued, P=pending,D=declined
        NULL as "conversionserver",
        NULL as "sequencenumber",
        NULL as "postedwweobjectid",
        GLE."fiscalYear"||TO_CHAR(GLE."fiscalPeriod", 'FM00') as "postedyearmth",
        NULL as "exchangerate",
        NULL as "payeeentitykey",
        NULL as "payeeentityvnumber",
        NULL as "recoverykind",
        'LAW_PL_19000101_21121231_4112' as "claimtypekey"
    from (select * from "ClaimReserveView" where "estimateALAE" <> 0) CR 
        left join "Event" E on E."eventId" = CR."eventId"
        left join "GLTransaction" GLT on GLT."transactionReference" = CR."claimReserveId" and GLT."glAccountId" = '5100' --GL account for ALAE reserve change
                left join "GLEntry" GLE on GLE."glEntryId" = GLT."glEntryId"
        left join "Instance" I on I."instanceId" = CR."claimId"
            left join "Policy" P on P."policyId" = I."policyId"
    --SCOPE
    where I."coverageDate" > '31-DEC-2006'
      and P."effectiveDate" > '31-DEC-2005'
       -- REQUIRED FIXES
      and CR."claimId" not in ('08-0031', '11-0001', '17-0011')  --11-0088
  union all
    -- indemnity reserve transactions
    select        
        CR."claimId" as "claimmappingkey",
        0 as "claimvnumber",
        CR."claimReserveId"||'_IND' as "transmappingkey",
        CR."reserveSeq" as "transactionumber",
        --GLT."glTransactionId" as "gltransnumber",
        regexp_replace(GLT."glTransactionId", '[^0-9]', '') as "gltransnumber",
        TO_CHAR(E."createDate", 'YYYYMMDD') as "ventrydate",
        TO_CHAR(E."eventDate", 'YYYYMMDD') as "accountingdate",
        CASE
            WHEN E."eventTypeId" like 'ClmRes%' THEN 3
            WHEN E."eventTypeId" = 'Opn' THEN 0
            WHEN E."eventTypeId" = 'Reo' THEN 0  -- reopen
            WHEN E."eventTypeId" = 'Cls' THEN 0
            WHEN E."eventTypeId" = 'Adj' THEN 0
            ELSE 9
        END as "action",
        CR."claimReserveId" as "reference",
        'USD' as "currencycd",
        NULL as "description",
        CR."estimateIndemnity" as "transamt",
        CASE
            WHEN E."createUserId" = 'Joyce' THEN 'jneumaier'
            WHEN E."createUserId" = 'Matt' THEN 'mbeier'
            WHEN E."createUserId" = 'Katja' THEN 'kkunzke'
            WHEN E."createUserId" = 'Brian' THEN 'banderson'
            WHEN E."createUserId" = 'Sally' THEN 'sanderson'
            ELSE '>>unknown<<'||COALESCE(E."createUserId", 'NULL') --QFC, NDZ, WHD
        END as "userentitymappingkey",
        '4112' as "sbuid",
        NULL as "chequeserieskey",
        NULL as "chequepayeename",
        NULL as "chequemailtoaddress",
        NULL as "mailingentitykey",
        NULL as "mailingentitykeyvnumber",
        NULL as "paymentmethod",
        NULL as "paymenttype",
        NULL as "paymentkind",
        NULL as "expensetype",
        NULL as "chequetype",
        NULL as "chequeno",
        NULL as "recoverytype",
        NULL as "depositno",
        'I' as "transtatus", --I=issued, P=pending,D=declined
        NULL as "conversionserver",
        NULL as "sequencenumber",
        NULL as "postedwweobjectid",
        GLE."fiscalYear"||TO_CHAR(GLE."fiscalPeriod", 'FM00') as "postedyearmth",
        NULL as "exchangerate",
        NULL as "payeeentitykey",
        NULL as "payeeentityvnumber",
        NULL as "recoverykind",
        'LAW_PL_19000101_21121231_4112' as "claimtypekey"
    from (select * from "ClaimReserveView" where "estimateIndemnity" <> 0) CR 
        left join "Event" E on E."eventId" = CR."eventId"
            left join "GLTransaction" GLT on GLT."transactionReference" = CR."claimReserveId" and GLT."glAccountId" = '5000' --GL account for Indemnity reserve change
                left join "GLEntry" GLE on GLE."glEntryId" = GLT."glEntryId"
        left join "Instance" I on I."instanceId" = CR."claimId"
            left join "Policy" P on P."policyId" = I."policyId"
    -- SCOPE
    where P."effectiveDate" > '31-DEC-2005'
      and I."coverageDate" > '31-DEC-2006'
       -- REQUIRED FIXES
      and CR."claimId" not in ('08-0031', '11-0001', '17-0011')  --11-0088'
 union all
    -- alae payment transactions
    select        
        CP."claimId" as "claimmappingkey",
        0 as "claimvnumber",
        CP."claimPaymentId"||'_ALAE' as "transmappingkey",
        CP."paymentSeq" as "transactionumber",
        --GLT."glTransactionId" as "gltransnumber",
        regexp_replace(GLT."glTransactionId", '[^0-9]', '') as "gltransnumber",
        TO_CHAR(E."createDate", 'YYYYMMDD') as "ventrydate",
        TO_CHAR(E."eventDate", 'YYYYMMDD') as "accountingdate",
        CASE
            WHEN E."eventTypeId" like 'ClmPay%' THEN 1
            ELSE 9
        END as "action",
        CP."claimPaymentId" as "reference",
        'USD' as "currencycd",
        NULL as "description",
        CP."paymentALAE" as "transamt",
        CASE
            WHEN E."createUserId" = 'Joyce' THEN 'jneumaier'
            WHEN E."createUserId" = 'Matt' THEN 'mbeier'
            WHEN E."createUserId" = 'Katja' THEN 'kkunzke'
            WHEN E."createUserId" = 'Brian' THEN 'banderson'
            WHEN E."createUserId" = 'Sally' THEN 'sanderson'
            ELSE '>>unknown<<'||COALESCE(E."createUserId", 'NULL') --QFC, NDZ, WHD
        END as "userentitymappingkey",
        '4112' as "sbuid",
        '4112000000000012637' as "chequeserieskey",
        D."payee" as "chequepayeename",
        CASE
            WHEN L."city" is not NULL THEN L."addressLine1"||','||L."addressLine2"||','||L."addressLine3"||','||L."city"||','||L."state"||','||L."zip" 
            ELSE 'UNKNOWN'
        END as "chequemailtoaddress",
         CASE
            WHEN CPV."payeeId" is not NULL THEN CPV."payeeId" 
            ELSE 'UNKNOWN'
        END as "mailingentitykey",
        0 as "mailingentitykeyvnumber",
        1 as "paymentmethod",
        1 as "paymenttype",
        'E' as "paymentkind",
        NULL as "expensetype",
        1 as "chequetype",
        D."checkNumber" as "chequeno",
        NULL as "recoverytype",
        NULL as "depositno",
        'I' as "transtatus", --I=issued, P=pending,D=declined
        NULL as "conversionserver",
        NULL as "sequencenumber",
        NULL as "postedwweobjectid",
        GLE."fiscalYear"||TO_CHAR(GLE."fiscalPeriod", 'FM00') as "postedyearmth",
        NULL as "exchangerate",
        COALESCE(CPV."payeeId", 'UNKNOWN') as "payeeentitykey",
        0 as "payeeentityvnumber",
        NULL as "recoverykind",
        'LAW_PL_19000101_21121231_4112' as "claimtypekey"
    from (select * from "ClaimPaymentView" where "paymentALAE" <> 0) CP 
        left join "Disbursement" D on D."disbursementId" = CP."disbursementId"
              left join (select "instanceId", MIN("payeeId") as "payeeId", MIN("payeeName") as "payeeName", MIN("payeeLocation") as "payeeLocation" from "ISIClaimPayeeView" group by "instanceId", "payeeId", "payeeName") CPV on CPV."instanceId" = CP."claimId" and LOWER(REPLACE(REPLACE(CPV."payeeName",'.', ''),',', '')) = LOWER(REPLACE(REPLACE(D."payee",'.', ''),',', ''))
                left join (select * from "LocationView" where "locationTypeId" <> 'Firm' and "locationSeq" = 1) L on L."mainEntityId" = CPV."payeeId"
        left join "Event" E on E."eventId" = CP."eventId"
        left join "GLTransaction" GLT on GLT."transactionReference" = CP."claimPaymentId" and GLT."glAccountId" in ('6010', '6015', '6017') --GL account for ALAE payment...6010=ALAE, 6015=GC, 6017=DF
            left join "GLEntry" GLE on GLE."glEntryId" = GLT."glEntryId"
        left join "Instance" I on I."instanceId" = CP."claimId"
            left join "Policy" P on P."policyId" = I."policyId"
    -- SCOPE
    where P."effectiveDate" > '31-DEC-2005'
      and I."coverageDate" > '31-DEC-2006'
      -- REQUIRED FIXES
      and CP."claimId" not in ('08-0031', '11-0001', '17-0011')  --11-0088%'
  union all
    -- indemnity payment transactions
    select        
        CP."claimId" as "claimmappingkey",
        0 as "claimvnumber",
        CP."claimPaymentId"||'_IND' as "transmappingkey",
        CP."paymentSeq" as "transactionumber",
        --GLT."glTransactionId" as "gltransnumber",
        regexp_replace(GLT."glTransactionId", '[^0-9]', '') as "gltransnumber",
        TO_CHAR(E."createDate", 'YYYYMMDD') as "ventrydate",
        TO_CHAR(E."eventDate", 'YYYYMMDD') as "accountingdate",
        CASE
            WHEN E."eventTypeId" like 'ClmPay%' THEN 1
            ELSE 9
        END as "action",
        CP."claimPaymentId" as "reference",
        'USD' as "currencycd",
        NULL as "description",
        CP."paymentIndemnity" as "transamt",
        CASE
            WHEN E."createUserId" = 'Joyce' THEN 'jneumaier'
            WHEN E."createUserId" = 'Matt' THEN 'mbeier'
            WHEN E."createUserId" = 'Katja' THEN 'kkunzke'
            WHEN E."createUserId" = 'Brian' THEN 'banderson'
            WHEN E."createUserId" = 'Sally' THEN 'sanderson'
            ELSE '>>unknown<<'||COALESCE(E."createUserId", 'NULL') --QFC, NDZ, WHD
        END as "userentitymappingkey",
        '4112' as "sbuid",
        '4112000000000012637' as "chequeserieskey",
        D."payee" as "chequepayeename",
        CASE
            WHEN L."city" is not NULL THEN L."addressLine1"||','||L."addressLine2"||','||L."addressLine3"||','||L."city"||','||L."state"||','||L."zip" 
            ELSE 'UNKNOWN'
        END as "chequemailtoaddress",
        CASE
            WHEN CPV."payeeId" is not NULL THEN CPV."payeeId" 
            ELSE 'UNKNOWN'
        END as "mailingentitykey",
        0 as "mailingentitykeyvnumber",
        1 as "paymentmethod",
        1 as "paymenttype",
        'L' as "paymentkind",
        NULL as "expensetype",
        1 as "chequetype",
        D."checkNumber" as "chequeno",
        NULL as "recoverytype",
        NULL as "depositno",
        'I' as "transtatus", --I=issued, P=pending,D=declined
        NULL as "conversionserver",
        NULL as "sequencenumber",
        NULL as "postedwweobjectid",
        GLE."fiscalYear"||TO_CHAR(GLE."fiscalPeriod", 'FM00') as "postedyearmth",
        NULL as "exchangerate",
        COALESCE(CPV."payeeId", 'UNKNOWN') as "payeeentitykey",
        0 as "payeeentityvnumber",
        NULL as "recoverykind",
        'LAW_PL_19000101_21121231_4112' as "claimtypekey"
    from (select * from "ClaimPaymentView" where "paymentIndemnity" <> 0) CP 
        left join "Disbursement" D on D."disbursementId" = CP."disbursementId"
            --left join "ISIClaimPayeeView" CPV on CPV."instanceId" = CP."claimId" and LOWER(REPLACE(REPLACE(CPV."payeeName",'.', ''),',', '')) = LOWER(REPLACE(REPLACE(D."payee",'.', ''),',', ''))
            left join (select "instanceId", MIN("payeeId") as "payeeId", MIN("payeeName") as "payeeName", MIN("payeeLocation") as "payeeLocation" from "ISIClaimPayeeView" group by "instanceId", "payeeId", "payeeName") CPV on CPV."instanceId" = CP."claimId" and LOWER(REPLACE(REPLACE(CPV."payeeName",'.', ''),',', '')) = LOWER(REPLACE(REPLACE(D."payee",'.', ''),',', ''))
                left join (select * from "LocationView" where "locationTypeId" <> 'Firm' and "locationSeq" = 1) L on L."mainEntityId" = CPV."payeeId"
        left join "Event" E on E."eventId" = CP."eventId"
        left join "GLTransaction" GLT on GLT."transactionReference" = CP."claimPaymentId" and GLT."glAccountId" in ('6030','6017') --GL account for indemnity payment...6030=indemnity, 6017-DR
            left join "GLEntry" GLE on GLE."glEntryId" = GLT."glEntryId"
        left join "Instance" I on I."instanceId" = CP."claimId"
            left join "Policy" P on P."policyId" = I."policyId"
    -- SCOPE
    where P."effectiveDate" > '31-DEC-2005'
      and I."coverageDate" > '31-DEC-2006'
       -- REQUIRED FIXES
      and CP."claimId" not in ('08-0031', '11-0001', '17-0011')  --11-0088
order by "claimmappingkey", "ventrydate", "transmappingkey"
;


-->>>>> ADD: deductible payment transactions
create or replace view "ISI_ClaimTransactionDedView" as 
    select        
        CP."claimId" as "claimmappingkey",
        0 as "claimvnumber",
        CP."claimPaymentId"||'_DPAY' as "transmappingkey",
        CP."paymentSeq" as "transactionumber",
        --GLT."glTransactionId" as "gltransnumber",
        regexp_replace(GLT."glTransactionId", '[^0-9]', '') as "gltransnumber",
        TO_CHAR(E."createDate", 'YYYYMMDD') as "ventrydate",
        TO_CHAR(E."eventDate", 'YYYYMMDD') as "accountingdate",
        2 as "action",
        CP."claimPaymentId" as "reference",
        'USD' as "currencycd",
        NULL as "description",
        CP."deductibleALAE" + CP."deductibleIndemnity" as "transamt",
        CASE
            WHEN E."createUserId" = 'Joyce' THEN 'jneumaier'
            WHEN E."createUserId" = 'Matt' THEN 'mbeier'
            WHEN E."createUserId" = 'Katja' THEN 'kkunzke'
            WHEN E."createUserId" = 'Brian' THEN 'banderson'
            WHEN E."createUserId" = 'Sally' THEN 'sanderson'
            ELSE '>>unknown<<'||COALESCE(E."createUserId", 'NULL') --QFC, NDZ, WHD
        END as "userentitymappingkey",
        '4112' as "sbuid",
        NULL as "chequeserieskey",
        NULL as "chequepayeename",
        NULL as "chequemailtoaddress",
        NULL as "mailingentitykey",
        NULL as "mailingentitykeyvnumber",
        1 as "paymentmethod",
        NULL as "paymenttype",
        NULL as "paymentkind",
        NULL as "expensetype",
        NULL as "chequetype",
        NULL as "chequeno",
        'DED' as "recoverytype",
        NULL as "depositno",
        'I' as "transtatus", --I=issued, P=pending,D=declined
        NULL as "conversionserver",
        NULL as "sequencenumber",
        NULL as "postedwweobjectid",
        GLE."fiscalYear"||TO_CHAR(GLE."fiscalPeriod", 'FM00') as "postedyearmth",
        NULL as "exchangerate",
        COALESCE(I."firmId", 'UNKNOWN') as "payeeentitykey",  ---DEFAULTING
        0 as "payeeentityvnumber",
        CASE
            WHEN CP."deductibleALAE" > 0 THEN 'E'
            ELSE 'L'
        END as "recoverykind",
        'LAW_PL_19000101_21121231_4112' as "claimtypekey"
    from (select * from "ClaimPaymentView" where (("deductibleIndemnity" <> 0 or "deductibleALAE" <> 0) and ("deductibleIndemnity" +  "deductibleALAE" <> 0) )) CP 
        --left join "Disbursement" D on D."disbursementId" = CP."disbursementId"
        --    left join "ISIClaimPayeeView" CPV on CPV."instanceId" = CP."claimId" and CPV."payeeName" = D."payee"
        left join "Event" E on E."eventId" = CP."eventId"
         left join "GLTransaction" GLT on GLT."transactionReference" = CP."claimPaymentId" and GLT."glAccountId" in ('6040', '6045') --GL account for deductible
            left join "GLEntry" GLE on GLE."glEntryId" = GLT."glEntryId"
        left join "Instance" I on I."instanceId" = CP."claimId"
            left join "Policy" P on P."policyId" = I."policyId"
    -- SCOPE
    where P."effectiveDate" > '31-DEC-2005'
      and I."coverageDate" > '31-DEC-2006'
      -- REQUIRED FIXES
      and CP."claimId" not in ('08-0031', '11-0001', '17-0011')  --11-0088
order by "claimmappingkey", "ventrydate", "transmappingkey"
;    
select * from "ISI_ClaimTransactionDedView" where "claimmappingkey" = '09-0036';   
select * from "GLTransaction" where "glTransactionId" in ('GT000203403', 'GT000203404');


-->>>>> ADD: claim coverage transactions - payment only
create or replace view "ISI_ClaimTransactionULAEView" as 
    select        
        IP."instanceId" as "claimmappingkey",
        0 as "claimvnumber",
        IP."instancePaymentId"||'_ULAE' as "transmappingkey",
        IP."paymentSeq" as "transactionumber",
        --GLT."glTransactionId" as "gltransnumber",
        regexp_replace(GLT."glTransactionId", '[^0-9]', '') as "gltransnumber",
        TO_CHAR(E."createDate", 'YYYYMMDD') as "ventrydate",
        TO_CHAR(E."eventDate", 'YYYYMMDD') as "accountingdate",
        1 as "action",
        IP."instancePaymentId" as "reference",
        'USD' as "currencycd",
        NULL as "description",
        IP."paymentGeneralLegal" as "transamt",
        CASE
            WHEN E."createUserId" = 'Joyce' THEN 'jneumaier'
            WHEN E."createUserId" = 'Matt' THEN 'mbeier'
            WHEN E."createUserId" = 'Katja' THEN 'kkunzke'
            WHEN E."createUserId" = 'Brian' THEN 'banderson'
            WHEN E."createUserId" = 'Sally' THEN 'sanderson'
            ELSE '>>unknown<<'||COALESCE(E."createUserId", 'NULL') --QFC, NDZ, WHD
        END as "userentitymappingkey",
        '4112' as "sbuid",
        '4112000000000012637' as "chequeserieskey",
        D."payee" as "chequepayeename",
        CASE
            WHEN L."city" is not NULL THEN L."addressLine1"||','||L."addressLine2"||','||L."addressLine3"||','||L."city"||','||L."state"||','||L."zip" 
            ELSE 'UNKNOWN'
        END as "chequemailtoaddress",
         CASE
            WHEN CPV."payeeId" is not NULL THEN CPV."payeeId" 
            ELSE 'UNKNOWN'
        END as "mailingentitykey",
        0 as "mailingentitykeyvnumber",
        1 as "paymentmethod",
        1 as "paymenttype",
        'E' as "paymentkind",
        NULL as "expensetype",
        1 as "chequetype",
        D."checkNumber" as "chequeno",
        NULL as "recoverytype",
        NULL as "depositno",
        'I' as "transtatus", --I=issued, P=pending,D=declined
        NULL as "conversionserver",
        NULL as "sequencenumber",
        NULL as "postedwweobjectid",
        GLE."fiscalYear"||TO_CHAR(GLE."fiscalPeriod", 'FM00') as "postedyearmth",
        NULL as "exchangerate",
        COALESCE(CPV."payeeId", 'UNKNOWN') as "payeeentitykey",
        0 as "payeeentityvnumber",
        NULL as "recoverykind",
        'LAW_PL_19000101_21121231_4112' as "claimtypekey"
    from (select * from "InstancePaymentView" where ("paymentGeneralLegal" <> 0)) IP 
        left join "Disbursement" D on D."disbursementId" = IP."disbursementId"
              left join "ISIClaimPayeeView" CPV on CPV."instanceId" = IP."instanceId" and LOWER(REPLACE(REPLACE(CPV."payeeName",'.', ''),',', '')) = LOWER(REPLACE(REPLACE(D."payee",'.', ''),',', ''))
                left join (select * from "LocationView" where "locationTypeId" <> 'Firm' and "locationSeq" = 1) L on L."mainEntityId" = CPV."payeeId"
        left join "Event" E on E."eventId" = IP."eventId"
        left join "GLTransaction" GLT on GLT."transactionReference" = IP."instancePaymentId" and GLT."glAccountId" in ('7811') --GL account for ULAE payment
            left join "GLEntry" GLE on GLE."glEntryId" = GLT."glEntryId"
        left join "Instance" I on I."instanceId" = IP."instanceId"
            left join "Policy" P on P."policyId" = I."policyId"
    -- SCOPE
    where P."effectiveDate" > '31-DEC-2005'
      and I."coverageDate" > '31-DEC-2006'
     -- REQUIRED FIXES
      and IP."instanceId" not in ('08-0031', '11-0001', '17-0011')  --11-0088
order by "claimmappingkey", "ventrydate", "transmappingkey"
;       
  


/*--- Testing ---*/
select count(*) from "ISI_ClaimTransactionView";
select * from "ISI_ClaimTransactionView" where "claimmappingkey" = '05-0097';
select count(*) from "ClaimReserve" where "estimateALAE" <> 0;
drop view "ISI_ClaimaTransactionView";
select * from "ContactView" where "instanceId" = '14-0052';  -- 14-0052, 0, CP000720405_ALAE
select * from "ISI_ClaimTransactionView" where "claimmappingkey" = '14-0052' order by "transmappingkey";
select * from "ClaimPayment" where "claimPaymentId" = 'CP000720405';
select * from "Event" where "eventId" = 'ET000690579';
update "Event" set "createUserId" = 'Joyce' where "createUserId" is NULL and "eventId" = 'ET000690579';

--------------------------------------------------------------------------------
/*
    ISI_ClaimTransactionCoverageView
    c_transactioncoverage
    
    Dependancies: 
    
    NOTES:
     
    History
    01/25/2018  FZ  Created
    04/11/2018  FZ  modified to confirm to v132 and added tail
*/
  
create or replace view "ISI_ClaimTranCov_ResALAEView"  as 
    -- alae reserve
    select
        CR."claimReserveId" as "CLAIMRESERVEID",
        CR."claimReserveId"||'_ALAE' as "transmappingkey",
        CR."claimId" as "claimmappingkey",
        0 as "claimvnumber",
        --LPL."conversionreference" as "polconversionref",
        CV."polconversionref" as "polconversionref",
        --LPL."transactiontype" as "poltranstype",
        CV."poltranstype" as "poltranstype",
        --LPL."transactioneffectivedate" as "poltranseffdate",
        CV."poltranseffdate" as "poltranseffdate",
        --LPL."transactionseqno" as "poltransseqno",
        CV."poltransseqno" as "poltransseqno",
        CV."sbuid" as "sbuid",
        CV."packageid"  as "packageid",
        CV."riskid" as "riskid",
        CV."linenumber" as "linenumber",
        CASE
            WHEN CV."poltranstype" = 'ERP' THEN 'WLMEU16' 
            ELSE 'PL_0'
        END as "coverageid",
        2 as "invno",
        'LAW_PL_19000101_21121231_4112_IND' as "subfilekey",
        'LAW_PL_19000101_21121231_4112' as "claimtypekey",
        'LPLI' as "kindofloss",
        CASE
            WHEN E."eventTypeId" like 'ClmRes%' THEN 3
            WHEN E."eventTypeId" = 'Reo' THEN 0  -- reopen
            WHEN E."eventTypeId" = 'Opn' THEN 0
            WHEN E."eventTypeId" = 'Cls' THEN 0
            WHEN E."eventTypeId" = 'Adj' THEN 0
            ELSE 9
        END as "transtype",
        NULL as "losspayment",
        NULL as "expensepayment",
        NULL as "salrecovery",
        NULL as "subrecovery",
        NULL as "lossreserve",
        CR."estimateALAE" as "expensereserve",
        NULL as "recoveryreserve",
        NULL as "conversionserver",
        NULL as "sequencenumber",
        0 as "iscurrent",
        NULL as "lossrecovered",
        NULL as "expenserecovered",
        'NA' as "classofloss",
        'NA' as "basisofloss",
        'NA' as "losscontrolevent"
    from (select * from "ClaimReserveView" where "estimateALAE" <> 0) CR 
        Left join "Event" E on E."eventId" = CR."eventId"
        inner join (select * from "GLTransaction" where "glAccountId" = '5100') GLT on GLT."transactionReference" = CR."claimReserveId"  --ALAE reserve
        left join "ISI_ClaimItemLPLView" CV on CV."claimmappingkey" = CR."claimId"
        
        --left join "ISIClaimToPolicyView" PTC on PTC."instanceId" = CR."claimId"
            --left join "ISI_CoverageLPLView" LPL on LPL."POLICYNUMBER" = PTC."policyNumber" and LPL."transactioneffectivedate" = PTC."transactioneffectivedate" and LPL."transactionseqno" = PTC."transactionseqno" and LPL."riskid" = 1 
        left join "Instance" I on I."instanceId" = CR."claimId"
            left join "Policy" P on P."policyId" = I."policyId"
    --SCOPE
    where P."effectiveDate" > '31-DEC-2005'
      and I."coverageDate" > '31-DEC-2006'
      -- REQUIRED FIXES
      and CR."claimId" not in ('08-0031', '11-0001', '17-0011')  --11-0088
order by "claimmappingkey", "transmappingkey" 
;

create or replace view "ISI_ClaimTranCov_ResIndView"  as
    -- indemnity reserve
    select
        CR."claimReserveId" as "CLAIMRESERVEID",
        CR."claimReserveId"||'_IND' as "transmappingkey",
        CV."claimmappingkey" as "claimmappingkey",
        0 as "claimvnumber",
        CV."polconversionref" as "polconversionref",
        CV."poltranstype" as "poltranstype",
        CV."poltranseffdate" as "poltranseffdate",
        CV."poltransseqno" as "poltransseqno",
        CV."sbuid" as "sbuid",
        CV."packageid" as "packageid",
        CV."riskid" as "riskid",
        CV."linenumber" as "linenumber",
        CASE
            WHEN CV."poltranstype" = 'ERP' THEN 'WLMEU16' 
            ELSE 'PL_0'
        END as "coverageid",
        2 as "invno",
        'LAW_PL_19000101_21121231_4112_IND' as "subfilekey",
        'LAW_PL_19000101_21121231_4112' as "claimtypekey",
        'LPLI' as "kindofloss",
        CASE
            WHEN E."eventTypeId" like 'ClmRes%' THEN 3
            WHEN E."eventTypeId" = 'Reo' THEN 0  -- reopen
            WHEN E."eventTypeId" = 'Opn' THEN 0
            WHEN E."eventTypeId" = 'Cls' THEN 0
            WHEN E."eventTypeId" = 'Adj' THEN 0
            ELSE 9
        END as "transtype",
        NULL as "losspayment",
        NULL as "expensepayment",
        NULL as "salrecovery",
        NULL as "subrecovery",
        CR."estimateIndemnity" as "lossreserve",
        NULL as "expensereserve",
        NULL as "recoveryreserve",
        NULL as "conversionserver",
        NULL as "sequencenumber",
        0 as "iscurrent",
        NULL as "lossrecovered",
        NULL as "expenserecovered",
        'NA' as "classofloss",
        'NA' as "basisofloss",
        'NA' as "losscontrolevent"
    from (select * from "ClaimReserveView" where "estimateIndemnity" <> 0) CR 
        left join "Event" E on E."eventId" = CR."eventId"
        inner join (select * from "GLTransaction" where "glAccountId" = '5000') GLT on GLT."transactionReference" = CR."claimReserveId"  --indemnity reserve
        left join "ISI_ClaimItemLPLView" CV on CV."claimmappingkey" = CR."claimId"
        --left join "ISIClaimToPolicyView" PTC on PTC."instanceId" = CR."claimId"
            --left join "ISI_CoverageLPLView" LPL on LPL."POLICYNUMBER" = PTC."policyNumber" and LPL."transactioneffectivedate" = PTC."transactioneffectivedate" and LPL."transactionseqno" = PTC."transactionseqno" and LPL."riskid" = 1 
        left join "Instance" I on I."instanceId" = CR."claimId"
            left join "Policy" P on P."policyId" = I."policyId"
    --SCOPE
    where P."effectiveDate" > '31-DEC-2005'
      and I."coverageDate" > '31-DEC-2006'
       -- REQUIRED FIXES
      and CR."claimId" not in ('08-0031', '11-0001', '17-0011')  --11-0088
order by "claimmappingkey", "transmappingkey"  
;

create or replace view "ISI_ClaimTranCov_PayALAEView"  as
    -- alae payment 
    select
        CP."claimPaymentId" as "CLAIMPAYMENTID",
        CP."claimPaymentId"||'_ALAE' as "transmappingkey",
        CV."claimmappingkey" as "claimmappingkey",
        0 as "claimvnumber",
        CV."polconversionref" as "polconversionref",
        CV."poltranstype" as "poltranstype",
        CV."poltranseffdate" as "poltranseffdate",
        CV."poltransseqno" as "poltransseqno",
        CV."sbuid" as "sbuid",
        CV."packageid" as "packageid",
        CV."riskid" as "riskid",
        CV."linenumber" as "linenumber",
        CASE
            WHEN CV."poltranstype" = 'ERP' and "glAccountId" = '6010' THEN 'WLMEU16' --LPL."coverageid"
            WHEN "glAccountId" = '6010' THEN 'PL_0'
            WHEN "glAccountId" = '6015' THEN 'WLMEND28'
            WHEN "glAccountId" = '6017' THEN 'WLMEND20'
            ELSE LPL."coverageid"
        END as "coverageid",
        2 as "invno",
        'LAW_PL_19000101_21121231_4112_IND' as "subfilekey",
        'LAW_PL_19000101_21121231_4112' as "claimtypekey",
        CASE
            WHEN CV."poltranstype" = 'ERP' and "glAccountId" = '6010' THEN 'LPLI' --LPL."coverageid"
            WHEN "glAccountId" = '6010' THEN 'LPLI'
            WHEN "glAccountId" = '6015' THEN 'PRES'
            WHEN "glAccountId" = '6017' THEN 'PERD'
            ELSE 'LPLI'
        END as "kindofloss",
        1 as "transtype",
        NULL as "losspayment",
        CP."paymentALAE" as "expensepayment",
        NULL as "salrecovery",
        NULL as "subrecovery",
        NULL as "lossreserve",
        NULL as "expensereserve",
        NULL as "recoveryreserve",
        NULL as "conversionserver",
        NULL as "sequencenumber",
        0 as "iscurrent",
        NULL as "lossrecovered",
        NULL as "expenserecovered",
        'NA' as "classofloss",
        'NA' as "basisofloss",
        'NA' as "losscontrolevent"
    from (select * from "ClaimPaymentView" where "paymentALAE" <> 0) CP 
        inner join (select * from "GLTransaction" where "glAccountId" in ('6010', '6015', '6017')) GLT on GLT."transactionReference" = CP."claimPaymentId"  --indemnity, GC, DF reserve
        left join "ISI_ClaimItemLPLView" CV on CV."claimmappingkey" = CP."claimId"
        --left join "ISIClaimToPolicyView" PTC on PTC."instanceId" = CP."claimId"
            left join "ISI_CoverageLPLFirm" LPL on LPL."conversionreference" = CV."polconversionref" and LPL."transactioneffectivedate" = CV."poltranseffdate" and LPL."transactionseqno" = CV."poltransseqno" and LPL."riskid" = 1
        left join "Instance" I on I."instanceId" = CP."claimId"
             left join "Policy" P on P."policyId" = I."policyId"
    --SCOPE
    where P."effectiveDate" > '31-DEC-2005'
      and I."coverageDate" > '31-DEC-2006'
      -- REQUIRED FIXES
      and CP."claimId" not in ('08-0031', '11-0001', '17-0011', '10-0101')  --11-0088
order by "claimmappingkey", "transmappingkey"  
;

 
create or replace view "ISI_ClaimTranCov_PayIndView"  as   
    -- indemnity payment
    select
        CP."claimPaymentId" as "CLAIMPAYMENTID",
        CP."claimPaymentId"||'_IND' as "transmappingkey",
        CV."claimmappingkey" as "claimmappingkey",
        0 as "claimvnumber",
        CV."polconversionref" as "polconversionref",
        CV."poltranstype" as "poltranstype",
        CV."poltranseffdate" as "poltranseffdate",
        CV."poltransseqno" as "poltransseqno",
        CV."sbuid" as "sbuid",
        CV."packageid" as "packageid",
        CV."riskid" as "riskid",
        CV."linenumber" as "linenumber",
        CASE
            WHEN LPL."transactiontype" = 'ERP' THEN 'WLMEU16'
            ELSE 'PL_0'  --LPL."coverageid" 
        END as "coverageid",
        2 as "invno",
        'LAW_PL_19000101_21121231_4112_IND' as "subfilekey",
        'LAW_PL_19000101_21121231_4112' as "claimtypekey",
        'LPLI' as "kindofloss",
        1 as "transtype",
        CP."paymentIndemnity" as "losspayment",
        NULL as "expensepayment",
        NULL as "salrecovery",
        NULL as "subrecovery",
        NULL as "lossreserve",
        NULL as "expensereserve",
        NULL as "recoveryreserve",
        NULL as "conversionserver",
        NULL as "sequencenumber",
        0 as "iscurrent",
        NULL as "lossrecovered",
        NULL as "expenserecovered",
        'NA' as "classofloss",
        'NA' as "basisofloss",
        'NA' as "losscontrolevent"
    from (select * from "ClaimPaymentView" where "paymentIndemnity" <> 0) CP 
        inner join (select * from "GLTransaction" where "glAccountId" = '6030') GLT on GLT."transactionReference" = CP."claimPaymentId"  --indemnity reserve
        left join "ISI_ClaimItemLPLView" CV on CV."claimmappingkey" = CP."claimId"
        --left join "ISIClaimToPolicyView" PTC on PTC."instanceId" = CP."claimId"
            left join "ISI_CoverageLPLFirm" LPL on LPL."conversionreference" = CV."polconversionref" and LPL."transactioneffectivedate" = CV."poltranseffdate" and LPL."transactionseqno" = CV."poltransseqno" and LPL."riskid" = 1 
        left join "Instance" I on I."instanceId" = CP."claimId"
            left join "Policy" P on P."policyId" = I."policyId"
    --SCOPE
    where P."effectiveDate" > '31-DEC-2005'
      and I."coverageDate" > '31-DEC-2006'
      -- REQUIRED FIXES
      and CP."claimId" not in ('08-0031', '11-0001', '17-0011', '10-0101')  --11-0088
order by "claimmappingkey", "transmappingkey"  
;
 
create or replace view "ISI_ClaimTranCov_PayDedView"  as
    -- deductible-ind/alae payments that trigger deductible due
    select
        CP."claimPaymentId" as "CLAIMDEDUCTIBLEID",
        CP."claimPaymentId"||'_DPAY' as "transmappingkey",
        CV."claimmappingkey" as "claimmappingkey",
        0 as "claimvnumber",
        CV."polconversionref" as "polconversionref",
        CV."poltranstype" as "poltranstype",
        CV."poltranseffdate" as "poltranseffdate",
        CV."poltransseqno" as "poltransseqno",
        CV."sbuid" as "sbuid",
        CV."packageid" as "packageid",
        CV."riskid" as "riskid",
        CV."linenumber" as "linenumber",
        'PL_0' as "coverageid",  --always for deductible
        2 as "invno",
        'LAW_PL_19000101_21121231_4112_IND' as "subfilekey",
        'LAW_PL_19000101_21121231_4112' as "claimtypekey",
        'LPLI' as "kindofloss",
        2 as "transtype",
        NULL as "losspayment",
        NULL as "expensepayment",
        NULL as "salrecovery",
        CP."deductibleALAE" + CP."deductibleIndemnity" as "subrecovery",  --deductible??
        NULL as "lossreserve",
        NULL as "expensereserve",
        NULL as "recoveryreserve",
        NULL as "conversionserver",
        NULL as "sequencenumber",
        0 as "iscurrent",
        CP."deductibleIndemnity" as "lossrecovered",
        CP."deductibleALAE" as "expenserecovered",
        'NA' as "classofloss",
        'NA' as "basisofloss",
        'NA' as "losscontrolevent"
    from (select * from "ClaimPaymentView" where "deductibleIndemnity" <> 0 or "deductibleALAE" <> 0) CP 
        inner join (select unique "glEntryId", "transactionReference" from "GLTransaction" where "glAccountId" in ('6040', '6045')) GLT on GLT."transactionReference" = CP."claimPaymentId"  --deductible recovery
        left join "ISI_ClaimItemLPLView" CV on CV."claimmappingkey" = CP."claimId"
            left join "ISI_CoverageLPLFirm" LPL on LPL."conversionreference" = CV."polconversionref" and LPL."transactioneffectivedate" = CV."poltranseffdate" and LPL."transactionseqno" = CV."poltransseqno" and LPL."riskid" = 1 
        --left join "ISIClaimToPolicyView" PTC on PTC."instanceId" = CP."claimId"
            --left join "ISI_CoverageLPLView" LPL on LPL."POLICYNUMBER" = PTC."policyNumber" and LPL."transactioneffectivedate" = PTC."transactioneffectivedate" and LPL."transactionseqno" = PTC."transactionseqno" and LPL."riskid" = 1 
        left join "Instance" I on I."instanceId" = CP."claimId"
            left join "Policy" P on P."policyId" = I."policyId"
    --SCOPE
    where P."effectiveDate" > '31-DEC-2005'
      and I."coverageDate" > '31-DEC-2006'
      -- REQUIRED FIXES
      and CP."claimId" not in ('08-0031', '11-0001', '17-0011', '10-0101')  --11-0088
order by "claimmappingkey", "transmappingkey"  
;


select * from "GLTransaction" where "transactionReference" = 'CP000218842';

-->>>>> ADD: coverage counsel
create or replace view "ISI_ClaimTranCov_PayULAEView"  as
    -- ulae payment 
    select
        IP."instancePaymentId" as "CLAIMPAYMENTID",
        IP."instancePaymentId"||'_ULAE' as "transmappingkey",
        IP."instanceId" as "claimmappingkey",
        0 as "claimvnumber",
        LPL."conversionreference" as "polconversionref",
        LPL."transactiontype" as "poltranstype",
        LPL."transactioneffectivedate" as "poltranseffdate",
        LPL."transactionseqno" as "poltransseqno",
        LPL."sbuid" as "sbuid",
        LPL."packageid" as "packageid",
        LPL."riskid" as "riskid",
        LPL."linenumber" as "linenumber",
        LPL."coverageid" as "coverageid",
        2 as "invno",
        'LAW_PL_19000101_21121231_4112_COV' as "subfilekey",
        'LAW_PL_19000101_21121231_4112' as "claimtypekey",
        'LPLI' as "kindofloss",
        1 as "transtype",
        NULL as "losspayment",
        IP."paymentGeneralLegal" as "expensepayment",
        NULL as "salrecovery",
        NULL as "subrecovery",
        NULL as "lossreserve",
        NULL as "expensereserve",
        NULL as "recoveryreserve",
        NULL as "conversionserver",
        NULL as "sequencenumber",
        0 as "iscurrent",
        NULL as "lossrecovered",
        NULL as "expenserecovered",
        'NA' as "classofloss",
        'NA' as "basisofloss",
        'NA' as "losscontrolevent"
    from (select * from "InstancePaymentView" where "paymentGeneralLegal" <> 0) IP 
        inner join (select * from "GLTransaction" where "glAccountId" in ('7811')) GLT on GLT."transactionReference" = IP."instancePaymentId"  --indemnity, GC, DF reserve
        left join "ISIClaimToPolicyView" PTC on PTC."instanceId" = IP."instanceId"
            left join "ISI_CoverageLPLFirm" LPL on LPL."POLICYNUMBER" = PTC."policyNumber" and LPL."transactioneffectivedate" = PTC."transactioneffectivedate" and LPL."transactionseqno" = PTC."transactionseqno" and LPL."riskid" = 1 
        left join "Instance" I on I."instanceId" = IP."instanceId"
    --SCOPE
    where PTC."effectiveDate" > '31-DEC-2005'
      and I."coverageDate" > '31-DEC-2006'
       -- REQUIRED FIXES
      and IP."instanceId" not in ('08-0031', '11-0001', '17-0011')  --11-0088
order by "claimmappingkey", "transmappingkey"  
;

/*--- Testing ---*/
select * from "ISI_CoverageLPLView";
select * from "ISI_ClaimTranCov_PayIndView";
select * from "ClaimPayment" where "paymentIndemnity" <> 0;
select * from "GLTransaction" where "transactionReference" = 'CP001011587';
select * from "GLAccount" order by "glAccountId"; 
select * from "ISI_ClaimTranCov_PayULAEView";
--------------------------------------------------------------------------------
/*
    ISI_ClaimTransactionCategoryView
    c_transactioncategory
    
    Dependancies: 
    
    NOTES:
     
    History
    02/14/2018  FZ  Created
    02/26/2018  FZ  Fixed categorytype issue E (expense) vs L (indemnity)
    
    
*/
create or replace view "ISI_ClaimTransCat_ResALAEView" as  
    -- reserves alae
    select
        CT_RA."transmappingkey" as "transmappingkey",
        CT_RA."claimmappingkey" as "claimmappingkey",
        CT_RA."claimvnumber" as "claimvnumber",
        CT_RA."sbuid" as "sbuid",
        CT_RA."claimtypekey" as "claimtypekey",
        'LAW_PL_19000101_21121231_4112_COVEX_2' as "categorykey",
        CASE
            WHEN E."eventTypeId" like 'ClmRes%' THEN 3
            WHEN E."eventTypeId" = 'Reo' THEN 0  -- reopen
            WHEN E."eventTypeId" = 'Opn' THEN 0
            WHEN E."eventTypeId" = 'Cls' THEN 0
            WHEN E."eventTypeId" = 'Adj' THEN 0
            ELSE 9
        END as "transtype",
        --0 as "transtype",  --reserve adjustment=0
        'E' as "categorytype",
        CRT."runningALAE" as "amount", --FIX: should be running total
        CT_RA."expensereserve" as"reservechange",
        NULL as "conversionserver",
        NULL as "sequencenumber"
    from "ISI_ClaimTranCov_ResALAEView" CT_RA
        left join "ClaimReserve" CR on CR."claimReserveId" = CT_RA."CLAIMRESERVEID"
            left join "Event" E on E."eventId" = CR."eventId"
        left join "ISIClaimReserveTotalView" CRT on CRT."claimReserveId" = CT_RA."CLAIMRESERVEID"
order by "claimmappingkey", CRT."eventDate", "transmappingkey" 
;
 
create or replace view "ISI_ClaimTransCat_ResIndView" as   
    -- reserve indemnity
    select
        CT_RI."transmappingkey" as "transmappingkey",
        CT_RI."claimmappingkey" as "claimmappingkey",
        CT_RI."claimvnumber" as "claimvnumber",
        CT_RI."sbuid" as "sbuid",
        CT_RI."claimtypekey" as "claimtypekey",
        'LAW_PL_19000101_21121231_4112_L_1' as "categorykey",
        CASE
            WHEN E."eventTypeId" like 'ClmRes%' THEN 3
            WHEN E."eventTypeId" = 'Reo' THEN 0  -- reopen
            WHEN E."eventTypeId" = 'Opn' THEN 0
            WHEN E."eventTypeId" = 'Cls' THEN 0
            WHEN E."eventTypeId" = 'Adj' THEN 0
            ELSE 9
        END as "transtype",  --reserve adjustment=0
        'L' as "categorytype",
        CRT."runningIndemnity" as "amount", --FIX: should be running total
        CT_RI."lossreserve" as "reservechange",
        NULL as "conversionserver",
        NULL as "sequencenumber"
    from "ISI_ClaimTranCov_ResIndView" CT_RI
        left join "ISIClaimReserveTotalView" CRT on CRT."claimReserveId" = CT_RI."CLAIMRESERVEID"
        left join "ClaimReserve" CR on CR."claimReserveId" = CT_RI."CLAIMRESERVEID"
            left join "Event" E on E."eventId" = CR."eventId"
order by "claimmappingkey", CRT."eventDate", "transmappingkey" 
;
    
create or replace view "ISI_ClaimTransCat_PayALAEView" as  
    -- payment alae
    select
        CT_PA."transmappingkey" as "transmappingkey",
        CT_PA."claimmappingkey" as "claimmappingkey",
        CT_PA."claimvnumber" as "claimvnumber",
        CT_PA."sbuid" as "sbuid",
        CT_PA."claimtypekey" as "claimtypekey",
        'LAW_PL_19000101_21121231_4112_COVEX_2' as "categorykey",
        1 as "transtype",
        'E' as "categorytype",
        CT_PA."expensepayment" as "amount",
        0 as "reservechange",
        NULL as "conversionserver",
        NULL as "sequencenumber"
    from "ISI_ClaimTranCov_PayALAEView" CT_PA
order by "claimmappingkey", "transmappingkey" 
;
    
create or replace view "ISI_ClaimTransCat_PayIndView" as      
    -- payment indemnity
    select
        CT_PI."transmappingkey" as "transmappingkey",
        CT_PI."claimmappingkey" as "claimmappingkey",
        CT_PI."claimvnumber" as "claimvnumber",
        CT_PI."sbuid" as "sbuid",
        CT_PI."claimtypekey" as "claimtypekey",
        'LAW_PL_19000101_21121231_4112_L_1' as "categorykey",
        1 as "transtype",
        'L' as "categorytype",
        CT_PI."losspayment" as "amount",
        0 as "reservechange",
        NULL as "conversionserver",
        NULL as "sequencenumber"
    from "ISI_ClaimTranCov_PayIndView" CT_PI
order by "claimmappingkey", "transmappingkey" 
;

--Deductible  
create or replace view "ISI_ClaimTransCat_PayDedView" as  
    -- deductible
    select
        CT_PD."transmappingkey" as "transmappingkey",
        CT_PD."claimmappingkey" as "claimmappingkey",
        CT_PD."claimvnumber" as "claimvnumber",
        CT_PD."sbuid" as "sbuid",
        CT_PD."claimtypekey" as "claimtypekey",
        'LAW_PL_19000101_21121231_4112_COVEX_2' as "categorykey",
        2 as "transtype",
        'R' as "categorytype",
        CT_PD."lossrecovered" + CT_PD."expenserecovered" as "amount",
        0 as "reservechange",
        NULL as "conversionserver",
        NULL as "sequencenumber"
    from "ISI_ClaimTranCov_PayDedView" CT_PD
order by "claimmappingkey", "transmappingkey" 
;    

--General Legal ULAE
create or replace view "ISI_ClaimTransCat_PayULAEView" as  
    -- payment ulae
    select
        CT_UA."transmappingkey" as "transmappingkey",
        CT_UA."claimmappingkey" as "claimmappingkey",
        CT_UA."claimvnumber" as "claimvnumber",
        CT_UA."sbuid" as "sbuid",
        CT_UA."claimtypekey" as "claimtypekey",
        'LAW_PL_19000101_21121231_4112_COCO_2' as "categorykey",
        1 as "transtype",
        'E' as "categorytype",
        CT_UA."expensepayment" as "amount",
        0 as "reservechange",
        NULL as "conversionserver",
        NULL as "sequencenumber"
    from "ISI_ClaimTranCov_PayULAEView" CT_UA
order by "claimmappingkey", "transmappingkey" 
;
 
 /*--- Testing ---*/
 select * from "ISI_ClaimTranCov_PayDedView";
 select * from "ClaimReserve" where "claimId" like '15-0024%' order by "claimId", "claimReserveId";
 select * from "ISI_ClaimTransCat_ResALAEView";
 select * from "ISI_ClaimTransCat_ResIndView";
 select * from "ISI_ClaimTransCat_PayALAEView";
 select * from "ISI_ClaimTransCat_PayIndView";
--------------------------------------------------------------------------------
/*
    ISI_ClaimLossDetailsView
    c_cl_clmlossdetails_1

    
    Dependancies: 
    
    NOTES:
     
    History
    02/12/2018  FZ  Created
    03/02/2018  FZ  fixed viewing aop, alleged error issues
    04/23/2018  FZ  fixed viewing aop, alleged error issues AGAIN!
*/
create or replace view "ISI_ClaimLossDetailsView" as  
    select
        C."claimId" as "claimmappingkey",
        0 as "claimvnumber",
        NULL as "additionalbenefitstype",
        --SUBSTR(LEA."lawErrorIdISIList", 1, 2) as "allegederror",
        NULL as "allegederror",
        NVL(SUBSTR(LEA."lawErrorIdISIList", 0, INSTR(LEA."lawErrorIdISIList", '~')-1), LEA."lawErrorIdISIList") as "allegederrorvalues",
        --SUBSTR(LAA."lawAreaIdISIList",1,2) as "aopactivity",
        SUBSTR(LAA."lawAreaIdISIList", 1, 2) as "areaofpractice",
        NULL as "associatedsublaw",
        LEA."lawErrorIdISIList" as "associatedsublawvalues",
        NULL as "claimremarkdate",
        NULL as "claimremarkind",
        NULL as "claimrepairind",
        NULL as "claimrepairindvo",
        'N' as "conflictofinterestind",
        NULL as "conflictofinteresttype",
        NULL as "cyberdatabreachind",
        NULL as "descofresolution",
        NULL as "dhccomplaintind",
        NULL as "ethicsissueind",
        NULL as "factmalpracticeallegations",
        NULL as "factualcoverageissue",
        NULL as "factualdamages",
        NULL as "factualevaluation",
        NULL as "factualplan",
        NULL as "factualprimarycase",
        NULL as "familyfailure",
        NULL as "feedispluteind",
        NULL as "groundsforror",
        NULL as "groundsforrorvalues",
        NULL as "insurermistakeind",
        NULL as "possibleexcessclaim",
        NULL as "priorsolfile",
        NULL as "reinsreportingrequiredind",
        NULL as "searcherrormadeby",
        NULL as "stateofaccident",
        NULL as "subpoenarequests",
        NULL as "subpeonarequestsvalues",
        NULL as "subpeonatoinsured",
        NULL as "successclaimrepairind",
        NULL as "successclaimrepairindvo",
        NULL as "taxrelatedissueind",
        NULL as "testatorcompetenceind",
        NULL as "timingofresolution",
        NULL as "titleinscompany",
        NULL as "titleinscompanyind",
        NULL as "titleinscompanyvalues",
        NULL as "tpbeneficiary2ind",
        NULL as "tpbeneficiaryind",
        NULL as "trustaccountissueind",
        NULL as "conversionserver",
        NULL as "sequencenumber"
    from "Claim" C
        left join "ISILawErrorAggListView" LEA on LEA."instanceId" = C."claimId"
        left join "ISILawAreaAggListView" LAA on LAA."instanceId" = C."claimId"
        left join "Instance" I on I."instanceId" = C."claimId"
            left join "Policy" P on P."policyId" = I."policyId"
    --SCOPE
    where P."effectiveDate" > '31-DEC-2005'
      and I."coverageDate" > '31-DEC-2006'
       -- REQUIRED FIXES
      and C."claimId" not in ('08-0031', '11-0001', '17-0011', '10-0101')  --11-0088
order by "claimmappingkey"
;

/*--- Testing ---*/
select * from "ISI_ClaimLossDetailsView";
select * from "LawError";
select * from "ISILawAreaAggListView";

--------------------------------------------------------------------------------
/*
    ISI_ClaimantWorkAreaView
    c_claimantworkarea
    
    Dependancies: 
    
    NOTES:  
     
    History
    02/13/2018  FZ  Created
*/
create or replace view "ISI_ClaimantWorkAreaView" as  
    select
        C."claimId" as "claimmappingkey",
        0 as "claimvnumber",
        LPL."conversionreference" as "polconversionref",
        LPL."transactiontype" as "poltranstype",
        LPL."transactioneffectivedate" as "poltranseffdate",
        LPL."transactionseqno" as "poltransseqno",
        LPL."packageid" as "packageid",
        LPL."riskid" as "riskid",
        LPL."linenumber" as "linenumber",
        CASE
            WHEN LPL."transactiontype" = 'ERP' THEN 'WLMEU16'
            ELSE 'PL_0'  --LPL."coverageid" 
        END as "coverageid",
        2 as "invno",
        'LAW_PL_19000101_21121231_4112_IND' as "subfilekey",
        'LAW_PL_19000101_21121231_4112' as "claimtypekey",
        '4112' as "sbuid",
        'LAW_PL_19000101_21121231_4112_LIT' as "workareakey",
        NULL as "conversionserver",
        NULL as "sequencenumber"
    from (select * from "Claim" where "court" is not NULL) C 
        left join "ISIClaimToPolicyView" PTC on PTC."instanceId" = C."claimId"
            left join "ISI_CoverageLPLFirm" LPL on LPL."POLICYNUMBER" = PTC."policyNumber" and LPL."transactioneffectivedate" = PTC."transactioneffectivedate" and LPL."transactionseqno" = PTC."transactionseqno" and LPL."riskid" = 1 
   --     left join (select *, row_number() over (partition by "instanceId" order by "instanceId", "contactTypeId") as "rn" from "ContactView" where "contactTypeId" = 'DefenseAttorney') C_DA on C_DA."instanceId" = C."claimId" and C_DA."rn" = 1
        left join "Instance" I on I."instanceId" = C."claimId"
    --SCOPE
    where PTC."effectiveDate" > '31-DEC-2005'
      and I."coverageDate" > '31-DEC-2006'
       -- REQUIRED FIXES
      and C."claimId" not in ('08-0031', '11-0001', '17-0011', '10-0101')  --11-0088
;

create or replace view "ISI_ClaimantWorkAreaULAEView" as 
    --ULAE general legal
    select
        C."claimId" as "claimmappingkey",
        0 as "claimvnumber",
        LPL."conversionreference" as "polconversionref",
        LPL."transactiontype" as "poltranstype",
        LPL."transactioneffectivedate" as "poltranseffdate",
        LPL."transactionseqno" as "poltransseqno",
        LPL."packageid" as "packageid",
        LPL."riskid" as "riskid",
        LPL."linenumber" as "linenumber",
        'PL_0' as "coverageid",
        2 as "invno",
        'LAW_PL_19000101_21121231_4112_COV' as "subfilekey",
        'LAW_PL_19000101_21121231_4112' as "claimtypekey",
        '4112' as "sbuid",
        'LAW_PL_19000101_21121231_4112_LIT' as "workareakey",
        NULL as "conversionserver",
        NULL as "sequencenumber"
    from (select unique "instanceId" from "InstancePayment" where "paymentGeneralLegal" > 0) IP
        inner join "Claim" C on C."claimId" = IP."instanceId"
            left join "ISIClaimToPolicyView" PTC on PTC."instanceId" = C."claimId"
                left join "ISI_CoverageLPLFirm" LPL on LPL."POLICYNUMBER" = PTC."policyNumber" and LPL."transactioneffectivedate" = PTC."transactioneffectivedate" and LPL."transactionseqno" = PTC."transactionseqno" and LPL."riskid" = 1
        left join "Instance" I on I."instanceId" = IP."instanceId"    
    --SCOPE
    where PTC."effectiveDate" > '31-DEC-2005'
      and I."coverageDate" > '31-DEC-2006'
      -- REQUIRED FIXES
      and IP."instanceId" not in ('08-0031', '11-0001', '17-0011', '11-0088', '10-0101')  --11-0088
    
order by "claimmappingkey"
;


/*--- Testing ---*/

--------------------------------------------------------------------------------
/*
    ISI_ClaimantLitigationView
    c_cl_clmtlitigation_1
    
    Dependancies: 
    
    NOTES:  NEED TO ADD OTHER TYPES (ie: other than defense attorney/firm
     
    History
    02/13/2018  FZ  Created
    02/23/2018  FZ  removed multiple records 
*/
create or replace view "ISI_ClaimantLitigationView" as
    select
        C."claimId" as "claimmappingkey",
        0 as "claimvnumber",
        LPL."conversionreference" as "polconversionref",
        LPL."transactioneffectivedate" as "poltranseffdate",
        LPL."transactionseqno" as "poltransseqno",
        LPL."transactiontype" as "poltranstype",   
        0 as "packageid",
        LPL."riskid" as "riskid",
        LPL."linenumber" as "linenumber",
        CASE
            WHEN LPL."transactiontype" = 'ERP' THEN 'WLMEU16'
            ELSE 'PL_0'  --LPL."coverageid" 
        END as "coverageid",
        2 as "invno",
        'LAW_PL_19000101_21121231_4112_IND' as "subfilekey",
        'LAW_PL_19000101_21121231_4112' as "claimtypekey",
        '4112' as "sbuid",
        'LAW_PL_19000101_21121231_4112_LIT' as "workareakey",
        LTV."ExpertAttorneyName" as "claimantexpert",
        LTV."ExpertAttorneyId" as "claimantexpertid_ek",
        0 as "claimantexpertid_vn",
        CASE 
            WHEN LTV."ProSeId" is NULL THEN NULL
            ELSE 1
        END as "claimantprose",
        LTV."DefenseAttorney1Name" as "defenseattorney",
        CASE
            WHEN LTV."DefenseAttorney1Id" <> LTV."DefenseAttorney2Id" THEN LTV."DefenseAttorney2Name"
            ELSE NULL
        END as "defenseattorney2",
        CASE
            WHEN LTV."DefenseAttorney1Id" <> LTV."DefenseAttorney2Id" THEN LTV."DefenseAttorney2Id"
            ELSE NULL
        END as "defenseattorney2id_ek",
        CASE
            WHEN LTV."DefenseAttorney1Id" <> LTV."DefenseAttorney2Id" THEN 0
            ELSE NULL
        END as "defenseattorney2id_vn",
        LTV."DefenseAttorney1Id" as "defenseattorneyid_ek",
        0 as "defenseattorneyid_vn",
        LTV."ExpertAttorneyName" as "defenseexpert",
        LTV."ExpertAttorneyId" as "defenseexpertid_ek",
        0 as "defenseexpertid_vn",
        LTV."DefenseFirm1Name" as "defensefirm",
        CASE
            WHEN LTV."DefenseAttorney1Id" <> LTV."DefenseAttorney2Id" THEN LTV."DefenseFirm2Name"
            ELSE NULL
        END as "defensefirm2",
        CASE
            WHEN LTV."DefenseAttorney1Id" <> LTV."DefenseAttorney2Id" THEN LTV."DefenseFirm2Id"
            ELSE NULL
        END as "defensefirm2id_ek",
        0 as "defensefirm2id_vn",
        LTV."DefenseFirm1Id" as "defensefirmid_ek",
        0 as "defensefirmid_vn",
        NULL as "indemexpdate",
        NULL as "judge",
        NULL as "judgeid_ek",
        NULL as "judgeid_vn",
        NULL as "longtermidemind",
        NULL as "mediationdate",
        LTV."MediatorAttorneyName" as "mediator",
        LTV."MediatorAttorneyId" as "mediatorid_ek",
        0 as "mediatorid_vn",
        C."pleadingCaption" as "pendingmotions",
        LTV."PlaintiffAttorneyName" as "plaintiffattorney",
        NULL as "plaintiffattorney2",
        NULL as "plaintiffattorney2id_ek",
        NULL as "plaintiffattorney2id_vn",
        LTV."PlaintiffAttorneyId" as "plaintiffattorneyid_ek",
        0 as "plaintiffattorneyid_vn",
        LTV."PlaintiffFirmName" as "plaintiffirm",
        NULL as "plaintiffirm2", 
        NULL as "plaintiffirm2id_ek", 
        NULL as "plaintiffirm2id_vn", 
        LTV."PlaintiffFirmId" as "plaintiffirmid_ek", 
        0 as "plaintiffirmid_vn", 
        NULL as "tollingagreeexpdate",
        NULL as "tollingagreeind",
        NULL as "trialdate",
        C."court" as "venue", 
        NULL as "conversionserver",
        NULL as "sequencenumber"
    from (select * from "Claim" where "court" is not NULL) C 
        left join "ISIClaimToPolicyView" PTC on PTC."instanceId" = C."claimId"
            left join "ISI_CoverageLPLFirm" LPL on LPL."POLICYNUMBER" = PTC."policyNumber" and LPL."transactioneffectivedate" = PTC."transactioneffectivedate" and LPL."transactionseqno" = PTC."transactionseqno" and LPL."riskid" = 1 
        left join "ISILitigationTemplateView" LTV on LTV."instanceId" = C."claimId"
        left join "Instance" I on I."instanceId" = C."claimId"
    --SCOPE
    where PTC."effectiveDate" > '31-DEC-2005'
      and I."coverageDate" > '31-DEC-2006'
      -- REQUIRED FIXES
      and C."claimId" not in ('08-0031', '11-0001', '17-0011', '11-0088')  --11-0088
order by "claimmappingkey"
;

/*--- Testing ---*/
select * from "ISI_ClaimantLitigationView";
select * from "ISIClaimToPolicyView";
select * from "ClaimView";