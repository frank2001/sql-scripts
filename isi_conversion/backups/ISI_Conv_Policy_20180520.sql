/*******************************************************************************
	ISI_Conv_Policy.sql
    
    ISI Conversion Scripts
	Scripts used to extract LAW data into ISI interim conversion tables
	
	Naming convention = 'ISI_'+ ISI tablename minus leading alpha underscore + 'View'
    
    Scope:
    Scope can be limited by restricting input (ie: firms, effective dates, etc..) 
    in ISI_PolicyView, ISI_BillingDetailView, ISI_InsuredsView
    
    10/01/2017  Firm = KIMLAV
	
    
    History:
	Created: FZ	09/29/2017
	Revised: 
*******************************************************************************/

/* UTILITIES */
------------------------------------------
CREATE OR REPLACE FORCE VIEW "LAWUA"."ISIFirmPolicyView" ("firmId", "firmEntityLocator", "policyId", "policyNumber", "policySuffix", "policyFirmId", "firmEntityName", "policyFirmName", "effectiveDate", "expirationDate", "cancelEffectiveDate", "policyFirmLocator", "ISITransaction", "relatedrevrcatranseffdate") AS 
  SELECT
 F1."firmId",
 E."entityLocator" as "firmEntityLocator",			-- V1 used Firm.firmLocator
 P."policyId", 
 P."policyNumber",
 P."policySuffix",
 P."firmId" AS "policyFirmId",
 E."entityName" AS "firmEntityName", 				-- V1 used Firm.firmName
-- E."entityName" AS "policyFirmName",
 P."firmName" as "policyFirmName",
 P."effectiveDate",
 P."expirationDate",
 P."cancelEffectiveDate",
 E."entityLocator" AS "policyFirmLocator",			-- V1 used Firm.firmLocator
 CASE
    WHEN P."newReissue" = 'R' THEN
        CASE
            WHEN P."effectiveDate" = lag(P."cancelEffectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber") THEN 'RWR' --rewrite after cancel of reissue
            ELSE 'REN'
        END
    WHEN P."newReissue" = 'N' THEN
        CASE
            WHEN P."effectiveDate" = lag(P."cancelEffectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber") THEN 'RWN' --rewrite after cancel of new business
            ELSE 'NEW'
        END
 END as "ISITransaction",
 CASE
    WHEN P."firmId" = lead(P."firmId") over (order by P."firmId", P."effectiveDate", P."policyNumber") and P."cancelEffectiveDate" is not NULL THEN lead(P."effectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber")   --used when ISITransaction = RWR or RWN
    ELSE NULL
 END as "relatedrevrcatranseffdate"   
FROM "Firm" F1
    left join "Policy" P on P."firmId" = F1."firmId"
    left join "Entity" E on E."entityId" = F1."firmId"
   
--where P."firmId" in ('GAUTLA')
;

------------------------------------------ 
create or replace view "ISILawyerRetroDateView" as 
    select 
        P."policyNumber",
        PLC."policyId",
        PLC."lawyerId",
        CASE
            WHEN PLC."retroactiveDate" is NOT NULL and PLC."retroactiveDate" > PLC."effectiveDate" THEN PLC."effectiveDate"   -- possible lawyer on multiple polices at same time, LAW PLC does not handle properly
            WHEN PLC."retroactiveDate" IS NOT NULL and PLC."retroactiveDate" <= PLC."effectiveDate" THEN PLC."retroactiveDate"  --01/25/2018 previously PLC.effectiveDate
            ELSE LEAST(LSB."barAdmitDate", L."inPracticeDate")
        END as "ISILawyerRetroDate"
    from "PolicyLawyerCoverageAllView" PLC   --01/14/2018 previously PolicyLawyerCoverage - issue with lawyers on multiple policies at same time causing multiple records
        left join "Policy" P on P."policyId" = PLC."policyId"
        left join "Lawyer" L on L."lawyerId" = PLC."lawyerId" 
            left join "LawyerStateBar" LSB on LSB."lawyerId" = L."lawyerId"   
    order by PLC."policyId", PLC."lawyerId"

;

--------------------------------------------------------------------------------
create or replace view "ISIPolicyRetroDateView" as 
select 
    X."policyId",
    X."policyNumber",
    MIN("ISILawyerRetroDate") as "ISIPolicyRetroDate"
from "ISILawyerRetroDateView" X
group by X."policyId", X."policyNumber"
;

--------------------------------------------------------------------------------
create or replace view "ISIFirmDatesView" as
select 
    P."firmId",
    MIN(P."effectiveDate") as "ISIFirmEarliestDate",
    MAX(P."expirationDate") as "ISIFirmLatestDate"
from "Policy" P
group by P."firmId"
order by P."firmId"
;

--------------------------------------------------------------------------------
-- ISIConversionReferenceView
-- Determines ISI conversion reference based on policy eff/exp dates
---https://stewashton.wordpress.com/2014/03/16/merging-contiguous-date-ranges/
--select * from "ISIFirmPolicyView" where "firmId" = 'KIMLAV';
create or replace view "ISIConversionReferenceView" as
with grp_starts as (
    select 
        "firmId",
        "effectiveDate",
        CASE
            WHEN "cancelEffectiveDate" is not NULL and "cancelEffectiveDate" <> "effectiveDate" THEN "cancelEffectiveDate"
            ELSE "expirationDate"
        END AS "expirationDate",
        case
            when "effectiveDate" = lag(COALESCE("cancelEffectiveDate", "expirationDate")) over (order by "firmId", "effectiveDate", COALESCE("cancelEffectiveDate", "expirationDate")) then 0
  --01/05/2018          when "effectiveDate" = lag("expirationDate") over (order by "firmId", "effectiveDate", "expirationDate") then 0
            else 1
        end as grp_start
    from "ISIFirmPolicyView"
)
, grps as (
    select
        "firmId",
        "effectiveDate",
        "expirationDate",
        sum(grp_start) over (order by "firmId", "effectiveDate", "expirationDate") grp
    from grp_starts
)
--create or replace view "ISIConversionReferenceView" as 
select
    "firmId",
    min("effectiveDate") as "startDate",
    max("expirationDate") as "endDate",
    "firmId"||'_'||to_char(min("effectiveDate"), 'YYYY')||'_'||to_char(max("expirationDate"),'YYYY') as "conversionreference"
from grps
group by "firmId", grp
order by "firmId", 1, 2
;

--------------------------------------------------------------------------------
-- PolicyLawyerCoverageAllView
-- This view is basically same as PolicyLawyerCoverage but each lawyer only has
-- one record per policy.  Normally deleted lawyers have two records 
create or replace view "PolicyLawyerCoverageAllView" as 
  with plc_start as (  
    select 
        P."firmId" as "firmId",
        P."policyNumber" as "policyNumber",
        PLC."policyId" as "policyId",
        PLC."lawyerId" as "lawyerId",
        row_number() over (partition by PLC."policyId", PLC."lawyerId" order by PLC."eventId") as "PLC_seq",
        PLC."coverageId",
        PLC."eventId",
        MIN(PLC."effectiveDate") over (partition by PLC."policyId", PLC."lawyerId") as "effectiveDate",
        CASE 
            WHEN SUBSTR(E."endorsementDesc", 1, 8) = 'WLMEND12' THEN E."effectiveDate"
            ELSE MIN(P."expirationDate") over (partition by PLC."policyId", PLC."lawyerId") 
        END as "expirationDate",
        
        PLC."retroactiveDate" as "retroactiveDate",
        CASE
            WHEN SUBSTR(E."endorsementDesc", 1, 8) = 'WLMEND29' THEN E."expirationDate" ---MAX(PLC."expirationDate") over (partition by PLC."policyId", PLC."lawyerId")
            ELSE MAX(PLC."excludeAfterDate") over (partition by PLC."policyId", PLC."lawyerId")
        END as "excludeAfterDate",
        MAX(PLC."excludeBeforeDate") over (partition by PLC."policyId", PLC."lawyerId") as "excludeBeforeDate"
    from "PolicyLawyerCoverage" PLC
        left join "Policy" P on P."policyId" = PLC."policyId"
        left join "Endorsement" E on E."endorsementId" = PLC."coverageId"
--    where PLC."policyId" = 'AP11696463'
    --group by PLC."policyId", PLC."lawyerId"
)
, plc_mid as (
    select 
        "firmId",
        "policyNumber",
        "policyId",
        "lawyerId",
        MIN("effectiveDate") as "effectiveDate",
        MIN("expirationDate") as "expirationDate",
        MAX("retroactiveDate") as "retroactiveDate",
        MAX("excludeAfterDate") as "excludeAfterDate",
        MIN("excludeBeforeDate") as "excludeBeforeDate"
      from plc_start 
      group by "firmId", "policyNumber", "policyId", "lawyerId"
)
    select
        "firmId",
        "policyNumber",
        "policyId",
        "lawyerId",
        "effectiveDate",
        "expirationDate",
        "retroactiveDate",
        "excludeAfterDate",
        "excludeBeforeDate",
        row_number() over (partition by "firmId", "policyNumber" order by "lawyerId") + 1 as "lawyerPolicySequence"
    from plc_mid
    
    order by "firmId", "policyNumber", "policyId", "lawyerId"
;
/* OLD VERSION PRE 01/12/2018
create or replace view "PolicyLawyerCoverageAllView" as 
    select
        P."firmId",
        P."policyNumber",
        PLC."policyId",
        PLC."lawyerId",
        row_number() over (partition by P."firmId", PLC."policyId" order by PLC."lawyerId") as "insuredid",
        PLC."coverageId",
        PLC."eventId",
        PLC."effectiveDate",
        PLC."expirationDate",
        PLC."retroactiveDate",
            PLC."cancelEffectiveDate",
        PLC."excludeBeforeDate",
        PLC."cancellationCoverageId",
        PLC."policyStatus",
        PLC2."coverageId" as "deleteCoverageId",
        PLC2."eventId" as "deleteEventId",
        PLC2."excludeAfterDate" as "excludeAfterDate"
    from "PolicyLawyerCoverage" PLC
        left join (select * from "PolicyLawyerCoverage" where "excludeAfterDate" is not NULL) PLC2 on PLC2."policyId" = PLC."policyId" and PLC2."lawyerId" = PLC."lawyerId"   
        left join "Policy" P on P."policyId" = PLC."policyId"
    where PLC."excludeAfterDate" is NULL
--        and P."policyNumber" = '0601090'
--      and P."firmId" in ('KIMLAV')
--      and P."policyNumber" = '1606112'
    order by P."firmId", P."policyNumber", PLC."effectiveDate", PLC."lawyerId"
;
*/
select * from "PolicyLawyerCoverageAllView" where "policyNumber" = '1606112' order by "firmId", "policyId", "effectiveDate", "lawyerId";
select * from "PolicyLawyerCoverageAllView" where "policyNumber" = '1004063' order by "firmId", "policyId", "effectiveDate", "lawyerId";

-- ISIPolicyEndorsementView
-- transposes the LAW Endorsement table for select endorsements to accommadate
-- listing endorsements by policy
create or replace view "ISIPolicyEndorsementView" as 
    select
        P."policyNumber" as "policyNumber",
    --    P."policyId" as "policyId",
    --    E."endorsementId" as "endorsementId",
        MAX(DECODE(SUBSTR(E."endorsementDesc",1,8), 'WLMEND02', 1)) as "WLMEND02",
        MAX(DECODE(SUBSTR(E."endorsementDesc",1,8), 'WLMEND03', 1)) as "WLMEND03",
        MAX(DECODE(SUBSTR(E."endorsementDesc",1,8), 'WLMEND24', 1)) as "WLMEND24",
        MAX(DECODE(SUBSTR(E."endorsementDesc",1,8), 'WLMEND25', 1)) as "WLMEND25",
        MAX(DECODE(SUBSTR(E."endorsementDesc",1,10), 'Manuscript', 1)) as "Manuscript"
    from "Endorsement" E 
        left join "Policy" P on P."policyId" = E."policyId"
    group by P."policyNumber" 
    order by P."policyNumber"
;

-- PolicyAccountingSumView
--
CREATE OR REPLACE FORCE VIEW "LAWUA"."PolicyAccountingSumView" ("firmId", "policyId", "paymentDate", "paymentAmount", "premiumAdjust", "transfer", "writeOff", "dueAmount", "premium", "financeCharge") AS 
  select 
        "firmId", 
        "policyId", 
        TO_DATE(TO_CHAR("paymentDate", 'DD-MON-YYYY')) as "paymentDate",
        sum("paymentAmount") as "paymentAmount", 
        sum("premiumAdjust") as "premiumAdjust",
        sum("transfer") as "transfer",
        sum("writeOff") as "writeOff",
        sum("dueAmount") as "dueAmount",
        sum("premium") as "premium", 
        sum("financeCharge") as "financeCharge" 
    from "PolicyAccountingView" 
    group by "firmId", "policyId", TO_DATE(TO_CHAR("paymentDate", 'DD-MON-YYYY'));
    
    
----
-- ISIEndorsementDetailView
--  created to provide endorsement detail since as of 04/23/2018 not necessary to combine
--  all endorsements on same effective date into 1 record since this causes a problem
--  
drop view "ISIEndorsementDetailView";
create or replace view "ISIEndorsementDetailView" as 
select
    P."firmId",
    P."policyId",
    P."policyNumber",
    P."effectiveDate",
    P."expirationDate",
    P."cancelEffectiveDate",
    COALESCE(PA."premium",0) as "coveragePremium",
    COALESCE(PA."premiumAdjust",0) as "coveragePremiumAdjust",
    COALESCE(PA."financeCharge",0) as "coverageFinanceCharge",
    COALESCE(PA."transfer",0) as "coverageTransfer",
    COALESCE(PA."writeOff",0) as "coverageWriteOff",
    COALESCE(PA."dueAmount",0) as "coverageDueAmount",
    COALESCE(PA."receiveAmount",0) as "coverageReceiveAmount",
    COALESCE(PA."returnAmount",0) as "coverageReturnAmount",
    E."eventTypeId",
    CASE
        WHEN EN."endorsementDesc" IS NULL THEN 'Policy Issue'
        ELSE EN."endorsementDesc"
    END as "endorsementDesc",
    C."coverageId",
    CASE
        WHEN EN."effectiveDate" is NULL THEN P."effectiveDate"
        ELSE EN."effectiveDate" 
    END as "endorsementEffectiveDate",
    TO_NUMBER(COALESCE(EN."endorsementNumber", '0')) + 1 as "endorsementNumber"   
from "Coverage" C
    left join "Event" E on E."eventId" = C."eventId"
    left join "Policy" P on P."policyId" = C."policyId"
    left join "Endorsement" EN on EN."endorsementId" = C."coverageId"
    left join (select "coverageId", sum("premium") as "premium", sum("premiumAdjust") as "premiumAdjust", sum("financeCharge") as "financeCharge", sum("transfer") as "transfer", sum("writeOff") as "writeOff", sum("dueAmount") as "dueAmount", sum("receiveAmount") as "receiveAmount", sum("returnAmount") as "returnAmount" from "PolicyAccounting" group by "coverageId") PA on PA."coverageId" = C."coverageId"
where C."coverageId" not in ('CG001148214') --04/27/2018--bogus fixes
  
--where P."policyId" in ( 'AP11865481', 'AP11936260')
order by P."policyId", TO_NUMBER(COALESCE(EN."endorsementNumber", '0'))
;


--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
/******************
**  POLICY
******************/
/*
    ISI_PolicyView
    p_policy

    History:
    03/07/2018  FZ  fix endorsements on policies with cancelEffectiveDate
    03/23/2018  FZ  added Firm tail information
    04/04/2018  FZ  added CAST for NULL values
    
*/
create or replace view "ISI_PolicyView" as
    select
        /* -- DEBUG AREA
        P."policyNumber" as "POLICYNUMBER",
        P."cancelEffectiveDate" as "CANCEL_DATE",
        */
        P."policyId" as "POLICYID",
        E."eventTypeId" as "EVENTTYPEID",
        ICRV."conversionreference",
        -- 01/05/2018 transaction redone to allow for cancellations
        CASE
            WHEN E."eventTypeId" in ('Cvg-CP', 'Cvg-EC', 'Cvg-ED', 'Cvg-EU', 'Cvg-NP', 'Cvg-ND', 'Cvg-NU') THEN 'ERP'  -- tail endorsement   
            WHEN E."eventTypeId" = 'Cvg-CN' THEN --cancellation
                CASE
                    WHEN P."cancelEffectiveDate" IS NOT NULL and P."cancelEffectiveDate" = P."effectiveDate" THEN  --flat cancelled policy --05/30/2018
                        CASE
                            WHEN P."cancelEffectiveDate" = lead(P."effectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") and P."firmId" = lead(P."firmId") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") THEN 'RCA'  --cancel for purposes of rewrite
                            --WHEN P."cancelEffectiveDate" = lead(P."effectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."transactionseqno") and P."firmId" = lead(P."firmId") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."transactionseqno") THEN 'RCA'  --cancel for purposes of rewrite
                            ELSE 'CAN'  --just cancelled flat without a reissue -- at least not effective the previous term's effective date
                        END
                    ELSE 'CAN'
                END 
            WHEN E."eventTypeId" = 'PolicyIssue' and P."effectiveDate" = lag(P."cancelEffectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber",E."endorsementNumber") and lag(P."cancelEffectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber",E."endorsementNumber") <> lag(P."effectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber",E."endorsementNumber")THEN 'REI' -- cancel/rewrite - no gap
            WHEN E."eventTypeId" = 'PolicyIssue' and P."newReissue" = 'R' and P."effectiveDate" = lag(P."cancelEffectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber",E."endorsementNumber") THEN 'RWR' -- flat cancel/rewrite  
            WHEN E."eventTypeId" = 'PolicyIssue' and P."newReissue" = 'N' and P."effectiveDate" = lag(P."cancelEffectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber",E."endorsementNumber") THEN 'RWN' -- flat cancel/rewrite
            --WHEN E."eventTypeId" = 'PolicyIssue' and P."newReissue" = 'R' and P."effectiveDate" = lag(P."cancelEffectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber",E."transactionseqno") THEN 'RWR' -- cancel/rewrite  
            --WHEN E."eventTypeId" = 'PolicyIssue' and P."newReissue" = 'N' and P."effectiveDate" = lag(P."cancelEffectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber",E."transactionseqno") THEN 'RWN' -- cancel/rewrite
            WHEN E."eventTypeId" = 'PolicyIssue' and P."newReissue" = 'N' THEN 'NEW'
            WHEN E."eventTypeId" = 'PolicyIssue' and P."newReissue" = 'R' and (FX."ISIFirmEarliestDate" = FPV."effectiveDate") THEN 'NEW'  --ADDED: 01/30/2018 to fix #4
            WHEN E."eventTypeId" = 'PolicyIssue' and P."newReissue" = 'R' THEN 'REN'
            ELSE 'END'
        END as "transactiontype",
        TO_CHAR(E."endorsementEffectiveDate", 'YYYYMMDD') as "transactioneffectivedate",
        --TO_CHAR(P."effectiveDate", 'YYYYMMDD') as "transactioneffectivedate",
        --0425 version....row_number() over (partition by P."firmId", E."endorsementEffectiveDate" order by P."policyId", E."endorsementEffectiveDate", E."endorsementNumber") as "transactionseqno",
        E."transactionseqno" as "transactionseqno",
        -- REVISE once s_predompolregion_mapping is updated with historical rate levels
        CASE
            WHEN P."effectiveDate" < '01-JAN-2010' THEN 'LPL_19000101_20991231_4112'  --'LPL_19000101_20100101_4112'
            WHEN P."effectiveDate" < '01-JAN-2016' THEN 'LPL_19000101_20991231_4112'  --'LPL_20100101_20160101_4112'
            WHEN P."effectiveDate" < '01-OCT-2017' THEN 'LPL_19000101_20991231_4112'  --'LPL_20160101_20171001_4112'
            WHEN P."effectiveDate" < '01-JAN-2018' THEN 'LPL_19000101_20991231_4112'  --'LPL_20171001_20180101_4112'
            ELSE 'LPL_19000101_20991231_4112'  --'LPL_20180101_99991231_4112'
        END as "pcmmappingkey", -- LPL_<rate effective date>_<rate expiration date>_4112
        4112 as "sbuid",
        TO_CHAR(FX."ISIFirmEarliestDate", 'YYYYMMDD') as "inceptiondate",
        TO_CHAR(FPV."effectiveDate", 'YYYYMMDD') as "policyeffectivedate",
        TO_CHAR(FPV."expirationDate", 'YYYYMMDD') as "policyexpirydate",
        
        CASE
            WHEN E."endorsementEffectiveDate" < P."effectiveDate" or E."endorsementEffectiveDate" > P."expirationDate" THEN TO_CHAR(P."effectiveDate", 'YYYYMMDD') -- fixes endorsements effective > policy expiration date (ie: 0703097)
            ELSE TO_CHAR(E."endorsementEffectiveDate", 'YYYYMMDD') 
        END as "veffectivedate",
        CASE
            WHEN E."endorsementEffectiveDate" < P."effectiveDate" or E."endorsementEffectiveDate" > P."expirationDate" THEN TO_CHAR(P."effectiveDate", 'YYYYMMDD')  -- fixes endorsements effective > policy expiration date (ie: 0703097)
            WHEN P."cancelEffectiveDate" is not NULL THEN TO_CHAR(E."endorsementEffectiveDate", 'YYYYMMDD')
            WHEN E."eventTypeId" = 'Cvg-CN' and P."cancelEffectiveDate" is NULL THEN TO_CHAR(E."endorsementEffectiveDate", 'YYYYMMDD')  --fixes c17 issue with LAW cancels w/out cancelEffectiveDate
            ELSE TO_CHAR(P."expirationDate", 'YYYYMMDD')
            ---ELSE TO_CHAR(LEAD(E."endorsementEffectiveDate", 1, P."expirationDate") over (partition by E."policyId" order by E."policyId", E."endorsementEffectiveDate"), 'YYYYMMDD') --gets next endorsement effective date
        END as "vexpirydate", 
        '000100' as "veffectivetime",
        '000100' as "vexpirytime",
        E."policyNumber" as "policykey",  --was FPV."policyNumber
        CAST(NULL as VARCHAR2(40)) as "previouspolicykey",
        E."coverageId" as "alternatepolicykey",
        CAST(NULL as VARCHAR2(40)) as "agentbrokerpolicykey",
        CAST(NULL as VARCHAR2(40)) as "binderkey",
        TO_CHAR(P."expirationDate", 'YYYYMMDD') as "policynextunderwritedate",
        P."firmId" as "cliententitymappingkey",  
        'A' as "renewaltype",
        MONTHS_BETWEEN(P."expirationDate", P."effectiveDate") as "termlengthind",
        'M' as "termlengthindunit",
        'D' as "billingtype",
        0 as "papind",  --should be 1 if payamentMethod=MP ???
        CASE
            WHEN P."paymentMethod" = 'QP' THEN '4 Pay 25 Down'
            WHEN P."paymentMethod" = 'MP' THEN '10 Pay'
            ELSE 'Annual'
        END as "paymentplan",
        19000101 as "planeffdate",
        20991231 as "planexpdate",
        'N' as "subscriptionind",
        CAST(NULL as NUMBER) as "facultativeind",
        'USD' as "premiumcurrencycd",
        'USD' as "limitcurrencycd",
        CAST(NULL as VARCHAR2(3)) as "sbuofficekey",
        'jmccarthy' as "underwriterentitymappingkey",
        0 as "priorperiodind",
        CASE
            WHEN E."eventTypeId" = 'Cvg-CN' THEN 0
            ELSE NULL
        END as "cancellationtype",
        CAST(NULL as NUMBER) as "canreturnpremium",
        CAST(NULL as NUMBER) as "canearnedpremium",
        CAST(NULL as NUMBER) as "reinstateexpirydate",
        CAST(NULL as VARCHAR2(40)) as "conversionserver",
        CAST(NULL as NUMBER) as "sequencenumber",
        -- only required if transactiontype=RCA
        CASE
            WHEN E."eventTypeId" = 'Cvg-CN' and P."cancelEffectiveDate" = P."effectiveDate" THEN  --flat cancelled policy  --05/30/2018
                CASE 
                    WHEN P."cancelEffectiveDate" = lead(P."effectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") and P."firmId" = lead(P."firmId") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") and lead("newReissue") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") = 'N' THEN 'RWN' 
                    WHEN P."cancelEffectiveDate" = lead(P."effectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") and P."firmId" = lead(P."firmId") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") and lead("newReissue") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") = 'R' THEN 'RWR'
                    ELSE NULL
                END
        END as "relatedrevrcatranstype",
        
        CASE
            WHEN E."eventTypeId" = 'Cvg-CN' and P."cancelEffectiveDate" = P."effectiveDate" and P."cancelEffectiveDate" = lead(P."effectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") and P."firmId" = lead(P."firmId") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") THEN TO_CHAR(LEAD(E."endorsementEffectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber"), 'YYYYMMDD') --flat cancelled policy
            --05/30/2018 WHEN E."eventTypeId" = 'Cvg-CN' and P."cancelEffectiveDate" is not NULL and P."cancelEffectiveDate" = lead(P."effectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") and P."firmId" = lead(P."firmId") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") THEN TO_CHAR(LEAD(E."endorsementEffectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber"), 'YYYYMMDD') --midterm cancel/rewrite
            ELSE NULL
        END as "relatedrevrcatranseffdate",
        /*
        CASE
            WHEN E."eventTypeId" = 'Cvg-CN' and P."cancelEffectiveDate" is not NULL and P."cancelEffectiveDate" = P."effectiveDate" and P."cancelEffectiveDate" = lead(P."effectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") and P."firmId" = lead(P."firmId") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") THEN row_number() over (partition by P."firmId", E."effectiveDate" order by P."policyId", E."endorsementNumber")+1
            WHEN E."eventTypeId" = 'Cvg-CN' and P."cancelEffectiveDate" is not NULL and P."cancelEffectiveDate" = lead(P."effectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") and P."firmId" = lead(P."firmId") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") THEN row_number() over (partition by P."firmId", E."effectiveDate" order by P."policyId", E."endorsementNumber")+1
            ELSE CAST(NULL as NUMBER) 
        END as "relatedrevrcatranseqno", 
        */
        CASE
            WHEN E."eventTypeId" = 'Cvg-CN' and P."cancelEffectiveDate" = P."effectiveDate" THEN  --flat cancelled policy --05/30/2018
                CASE 
                    --WHEN P."cancelEffectiveDate" = lead(P."effectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") and P."firmId" = lead(P."firmId") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") THEN row_number() over (partition by P."firmId", E."endorsementEffectiveDate" order by P."policyId", E."endorsementEffectiveDate", E."endorsementNumber") + 1 
                    WHEN P."cancelEffectiveDate" = lead(P."effectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") and P."firmId" = lead(P."firmId") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") THEN lead(E."transactionseqno") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") 
                    --WHEN P."cancelEffectiveDate" = lead(P."effectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") and P."firmId" = lead(P."firmId") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") and lead("newReissue") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") = 'R' THEN row_number() over (partition by P."firmId", E."endorsementEffectiveDate" order by P."policyId", E."endorsementEffectiveDate", E."endorsementNumber") + 1
                    ELSE NULL
                END
        END as "relatedrevrcatranseqno",
        1 as "claimsmadeind",
        TO_CHAR(X."ISIPolicyRetroDate", 'YYYYMMDD') as "retrodate",
        TO_CHAR(TLV."effectiveDate", 'YYYYMMDD') as "extendreporteffdate",  --firm tail effective date
        TO_CHAR(TLV."expirationDate", 'YYYYMMDD') as "extendreportexpdate",  --firm tail expiration date, unlimited 29991231      
        CASE
            WHEN E."eventTypeId" in ('Cvg-CP', 'Cvg-EC', 'Cvg-ED', 'Cvg-EU', 'Cvg-NP', 'Cvg-ND', 'Cvg-NU') THEN 702  -- ERP  tail endorsement   
            WHEN E."eventTypeId" = 'Cvg-CN' THEN --cancellation
                CASE
                    WHEN P."cancelEffectiveDate" IS NOT NULL and P."cancelEffectiveDate" = P."effectiveDate" THEN  --flat cancelled policy--05/30/2018
                        CASE
                            WHEN P."cancelEffectiveDate" = lead(P."effectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") and P."firmId" = lead(P."firmId") over (order by P."firmId", P."effectiveDate", P."policyNumber", E."endorsementNumber") THEN 999 --'RCA'  --cancel for purposes of rewrite
                            ELSE 699 --'CAN' just cancelled flat without a reissue -- at least not effective the previous term's effective date
                        END
                    ELSE 699 --CAN
                END  
            WHEN E."eventTypeId" = 'PolicyIssue' and P."effectiveDate" = lag(P."cancelEffectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber",E."endorsementNumber") and lag(P."cancelEffectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber",E."endorsementNumber") <> lag(P."effectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber",E."endorsementNumber") THEN 1501 --REI -- cancel/rewrite - no gap    
            WHEN E."eventTypeId" = 'PolicyIssue' and P."newReissue" = 'R' and P."effectiveDate" = lag(P."cancelEffectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber",E."endorsementNumber") THEN 1099 --'RWR'  cancel/rewrite  
            WHEN E."eventTypeId" = 'PolicyIssue' and P."newReissue" = 'N' and P."effectiveDate" = lag(P."cancelEffectiveDate") over (order by P."firmId", P."effectiveDate", P."policyNumber",E."endorsementNumber") THEN 1099 --'RWN'  cancel/rewrite
            WHEN E."eventTypeId" = 'PolicyIssue' and P."newReissue" = 'N' THEN 200 --'NEW' new business
            WHEN E."eventTypeId" = 'PolicyIssue' and P."newReissue" = 'R' and (FX."ISIFirmEarliestDate" = FPV."effectiveDate") THEN 200 --'NEW' new business --ADDED: 01/30/2018 to fix #4
            WHEN E."eventTypeId" = 'PolicyIssue' and P."newReissue" = 'R' THEN 500 --'REN' reissue
            ELSE 300 --'END' endorsement (non tail)
        END as "vreasoncode",
        TO_CHAR(EV."eventDate", 'YYYYMMDDHH24MISS') as "ventrydatetime",
        CASE
            WHEN EV."createUserId" in ('Jane', 'JaneH') THEN 'jharder'
            ELSE EV."createUserId" 
        END as "vusermappingkey",
        TO_CHAR(COALESCE(EV."dueDate",EV."eventDate"), 'YYYYMM') as "postedyearmth",  --This does not always match accounting's fiscalYear, fiscalPeriod due to multiple trans mapping to coverage spanning multiple periods (ie: BRUCLA, CG11878593)
        'D' as "policysourceind",
        CAST(NULL as VARCHAR2(40)) as "assumedcompanymappingkey",
        CAST(NULL as NUMBER) as "writingcompanymappingkey",
        CAST(NULL as VARCHAR2(40)) as "freetradezoneriskcode",
        'EN' as "languagecd",
        CAST(NULL as VARCHAR2(40)) as "overridereasoncd",
        CAST(NULL as VARCHAR2(40)) as "overridereasonnote",
        CAST(NULL as NUMBER) as "postedwweobjectid",
        CAST(NULL as NUMBER) as "postedwwevnumber",
        CAST(NULL as VARCHAR2(40)) as "transactionstatus",
        CAST(NULL as NUMBER) as "nextfollowupdate",
        0 as "finaladjustmentind",
        'LPL_Ratebook' as "rateeditionname",
        1 as "rateeditionversion",
        CAST(NULL as NUMBER) as "wipstatus", -- probably use event folder
        CAST(NULL as VARCHAR2(40)) as "quoterelatedtranstype",
        CAST(NULL as VARCHAR2(3)) as "relatedtranstype",
        CAST(NULL as VARCHAR2(8)) as "relatedtranseffdate",
        CAST(NULL as NUMBER) as "relatedtransseqno",
        CAST(NULL as VARCHAR2(40)) as "quotekey",
        CAST(NULL as VARCHAR2(8)) as "quotedate",
        CAST(NULL as NUMBER) as "quotedaystoaccept",
        CAST(NULL as VARCHAR2(8)) as "quoteexpirydate",
        CAST(NULL as VARCHAR2(40)) as "quotedefinedname",
        CAST(NULL as VARCHAR2(40)) as "quoteseriescode",
        CAST(NULL as VARCHAR2(1)) as "quotestatus",
        1 as "quotedocumentsprinted",
        CAST(NULL as VARCHAR2(40)) as "brkwritingcompanymappingkey",
        CAST(NULL as NUMBER) as "brokeragepolicycommission",
        CAST(NULL as VARCHAR2(1)) as "quickquoteind",
        CAST(NULL as VARCHAR2(1)) as "referralstatus",
        CAST(NULL as VARCHAR2(40)) as "documentcategory",
        CAST(NULL as VARCHAR2(40)) as "frontedcompanymappingkey",
        0 as "assumedcommissionamount",
        1 as "assumedcommissiontype",
        CAST(NULL as VARCHAR2(40)) as "policycompositiontype",
        'WI' as "predominantjurisdiction",
        'WI' as "predominantpolicyregion",
        0 as "totalwaivedpremium",
        --TO_CHAR(EV."eventDate", 'YYYYMMDD') as "accountingdate", --Event.eventDate
        TO_CHAR(COALESCE(EV."dueDate", EV."completeDate"), 'YYYYMMDD') as "accountingdate",  --make this match "postedyearmth"????
        CAST(NULL as VARCHAR2(3)) as "quotedeclinedby",
        CAST(NULL as VARCHAR2(40)) as "quotecompetitor",
        0 as "quotecompetitorpremium",
        CASE
            WHEN CT."personId" IS NOT NULL THEN CT."personId"
            ELSE CT."contactEntityId"
        END as "policycontactentitykey",
        0 as "policycontactentitykeyvnumber",
        CAST(NULL as VARCHAR2(40)) as "comments",  --helpful but not required
        CASE
            WHEN P."newReissue" = 'N' THEN 'N'
            WHEN P."newReissue" = 'R' THEN 'R'
        END as "renewalstatus",
        CASE
            WHEN P."cancelEffectiveDate" is NULL THEN 0
            ELSE 7
       END as "insurancestatus",
       P."perClaimLimit" as "liabilitylimit",
       CAST(NULL as VARCHAR2(40)) as "overridemailingaddrkey",
       0 as "auditreportingind",
       CAST(NULL as VARCHAR2(8)) as "convertedropolicytrneffdate",
       CAST(NULL as NUMBER) as "convertedtopolicytrnseqno",
       CAST(NULL as VARCHAR2(20)) as "quotecompletedreasoncode",
       CAST(NULL as VARCHAR2(40)) as "quotecompletedreasonnotes",
       CAST(NULL as VARCHAR2(40)) as "quotelastmodifieddatetime",
       CASE
            WHEN TLV."tailTerm" is not NULL and TLV."tailTerm" <> 'Unlimited' THEN TLV."tailTerm"
            ELSE CAST(NULL as VARCHAR2(20))
        END as "extendreporttermlength",  --v132  if U then NULL
        CASE
            WHEN TLV."tailTerm" is not NULL and TLV."tailTerm" = 'Unlimited' THEN 'U'
            WHEN TLV."tailTerm" is not NULL and TLV."tailTerm" <> 'Unlimited' THEN 'Y'
            ELSE CAST(NULL as VARCHAR2(1))
        END as "extendreportlengthunit",  --v132  Y=years, M=months, U=unlimited
        CAST(NULL as VARCHAR2(1)) as "useextendreportexpoverrideind"  --v132  default= NULL
from (select * from "ISIEndorsementDetail2View") E  --where "eventTypeId" not in ('Cvg-NC', 'Cvg-EL', 'Cvg-CC', 'Cvg-RC', 'Cvg-AT', 'Cvg-EV', 'Cvg-MN', 'Cvg-RL')) E  
    left join "ISIFirmPolicyView" FPV on FPV."policyId" = E."policyId"
        left join "Policy" P on P."policyId" = FPV."policyId"
            left join "Application" A on A."applicationId" = P."policyId"
            left join (select * from "Contact" where "contactTypeId" = 'Firm') CT on CT."organizationId" = P."firmId"  -- may produce duplicates when there are multiple contactTypeId=Firm
            left join "ISIConversionReferenceView" ICRV on ICRV."firmId" = P."firmId" and (P."effectiveDate" BETWEEN ICRV."startDate" and ICRV."endDate" and COALESCE(P."cancelEffectiveDate", P."expirationDate") BETWEEN ICRV."startDate" and ICRV."endDate")
        left join "Quote" Q2 on Q2."applicationId" = P."policyId"
        left join "ISIPolicyRetroDateView" X on X."policyId" = P."policyId"
        left join "ISIFirmDatesView" FX on FX."firmId" = P."firmId"
    left join "Coverage" C on C."coverageId" = E."coverageId"
            left join "Event" EV on EV."eventId" = C."eventId"  
    left join (select unique "coverageId", "effectiveDate", "expirationDate", "tailType", "tailTerm" from "TailLawyerAllView") TLV on TLV."coverageId" = E."coverageId"
where P."effectiveDate" > '31-DEC-2005'
-- BELOW ITEMS NEED FIXING
  and P."firmId" not in ('SKOGLA', 'CASEL1')
  and P."policyNumber" NOT in ('0607050', '0703097', '0807068', '0811071', '1003092', '1006065', '1007005', '1111122', '1112041', '1206065', '1308050', '1408002', '1505056', '1602014',  '1601090')
order by FPV."firmId", FPV."policyNumber", TO_CHAR(FPV."effectiveDate", 'YYYYMMDD'), E."transactionseqno"
;


/*--- Table created for performance---*/
drop table "ISI_Policy";
--delete * from "ISI_Policy";
create table "ISI_Policy" as
    (select * from "ISI_PolicyView");
select * from "ISI_Policy";   
select count(*) from "ISI_Policy";  


/*----- FIXES MUST BE RUN UNTIL FIXED -----*/
--select * from "ISI_Policy" where "conversionreference" = 'MARELT_2011_2018' ;
delete from "ISI_Policy" where "conversionreference" = 'MARELT_2011_2018' and "transactiontype" = 'REN' and "alternatepolicykey" = 'CG11483286';
--select * from "ISI_Policy" where "conversionreference" = 'PREVGO_1988_2018' and "transactioneffectivedate" = '20100601' ;
--update "ISI_Policy" set "relatedrevrcatranseqno" = 1 where "conversionreference" = 'PREVGO_1988_2018' and "transactiontype" = 'RCA' and "transactioneffectivedate" = '20100601' and "relatedrevrcatranseqno" = 6;
--select * from "ISI_Policy" where "conversionreference" = 'SANDKR_2010_2011' and "transactioneffectivedate" = '20101115' ;
--update "ISI_Policy" set "relatedrevrcatranseqno" = 1 where "conversionreference" = 'SANDKR_2010_2011' and "transactiontype" = 'RCA' and "transactioneffectivedate" = '20101115' and "relatedrevrcatranseqno" = 4;

/*---- POST CREATE 2nd PASS UPDATES ----*/
create or replace view "ISIERPUpdateView" as 
    select unique "policykey", "extendreporteffdate", "extendreportexpdate" from "ISI_Policy" where "transactiontype" = 'ERP' order by "policykey";
update "ISI_Policy" P1
set ("extendreporteffdate", "extendreportexpdate") = (select "extendreporteffdate", "extendreportexpdate" from "ISIERPUpdateView" ERP 
                                                        where P1."policykey" = ERP."policykey"
                                                          and P1."extendreporteffdate" is NULL
                                                          and P1."extendreportexpdate" is NULL)
;      


/*-- Testing --*/
select * from "ISI_PolicyView" order by "conversionreference", "policykey", "transactioneffectivedate", "transactionseqno";
select * from "ISI_PolicyView" where "conversionreference" = 'MARELT_2011_2018';
select * from "ISI_PolicyView" where "conversionreference" = 'PREVGO_1988_2018' and "transactiontype" = 'RCA' and "transactioneffectivedate" = '20100601' and "relatedrevrcatranseqno" = 6;
select * from "Endorsement" where "policyId" in ('AP000703551', 'AP000720161') order by "policyId", "endorsementNumber";
select DISTINCT "policyId", "effectiveDate", min("endorsementNumber") as "endorsementNumber"
from "Endorsement" E
    left join "ISIFirmPolicyView" FPV on FPV."policyId" = E."policyId"
        left join "Policy" P on P."policyId" = FPV."policyId"
            left join "ISIConversionReferenceView" ICRV on ICRV."firmId" = P."firmId" and (P."effectiveDate" BETWEEN ICRV."startDate" and ICRV."endDate" and P."expirationDate" BETWEEN ICRV."startDate" and ICRV."endDate")
--            left join "ISIConversionReferenceView" ICRV on ICRV."firmId" = P."firmId" and (P."effectiveDate" BETWEEN ICRV."startDate" and ICRV."endDate" and (P."expirationDate" BETWEEN ICRV."startDate" and ICRV."endDate" or P."cancelEffectiveDate" BETWEEN ICRV."startDate" and ICRV."endDate"))
where "policyId" in ('AP000703551', 'AP000720161') group by "policyId", "effectiveDate" order by "effectiveDate";
select * from "ISIConversionReferenceView" where "firmId" in ('KIMLAV', 'KIM_LA', 'LAVOLA', 'GAUTLA', 'ALEMAS') order by "firmId", "startDate" ;
select * from "ISIConversionReferenceView" where "firmId" in ('GAUTLA', 'ALEMAS') order by "firmId", "startDate" ;
-- tail testing
select * from "ISI_PolicyView" where "policykey" in ('1710130', '1608097', '1510133');
--------------------------------------------------------------------------------   
/*
    ISI_BillingDetailView
    p_billingdetail
    
    HISTORY:
    04/20/2018  FZ  changed policyexpirydate to use originl policy expiration date

*/

create or replace view "ISI_BillingDetailView" as 
    select
    -- DEBUG AREA
        P."policyNumber" as "POLICYNUMBER",
        PV."conversionreference" as "conversionreference",
        PV."sbuid" as "sbuid",
        CASE
            WHEN P."paymentMethod" = 'QP' THEN '4 Pay 25 Down'
            WHEN P."paymentMethod" = 'MP' THEN '10 Pay'
            ELSE 'Annual'
        END as "paymentplan",
        19000101 as "planeffdate",
        20991231 as "planexpdate",
        --TO_CHAR(P."effectiveDate", 'YYYYMMDD') as "policyeffectivedate",
        PV."policyeffectivedate" as "policyeffectivedate" ,--04/21/2018
        --TO_CHAR(COALESCE(P."cancelEffectiveDate", P."expirationDate"), 'YYYYMMDD') as "policyexpirydate",
        -------TO_CHAR(P."expirationDate", 'YYYYMMDD') as "policyexpirydate",  --04/20/2018 change to use original expiration date instead of 
        PV."policyexpirydate" as "policyexpirydate",  --04/21/2018
        2 as "payplantype",  --2=invoice
        NULL as "paymentmethodentitykey",
        P."financeCharge" as "fees",
        P."premium" + P."premiumAdjust" as "totalamount",  ---????? what about surplus, transfer and writeoff
        P."premium" + P."premiumAdjust" - P."financeCharge" as "grossamount",
        TO_CHAR(P."effectiveDate", 'DD') as "preferredbillday",
        CASE
            WHEN P."paymentMethod" = 'QP' THEN 4
            WHEN P."paymentMethod" = 'MP' THEN 10
            ELSE 1
        END as "maximumterms",
        CASE
            WHEN P."paymentMethod" = 'QP' THEN 3
            WHEN P."paymentMethod" = 'MP' THEN 1
            ELSE 5
        END as "unit",
        0 as "feesondownpaymentind",
        NULL as "downpaymenttype",  --??
        0 as "downpaymentvalue",
        'USD' as "downpaymentcurrency",
        NULL as "financetype",
        0 as "financevalue",
        'USD' as "financecurrency",
        NULL as "lagdays",
        NULL as "roundondownpaymentind",
        CASE
            WHEN P."paymentMethod" = 'QP' THEN 4
            WHEN P."paymentMethod" = 'MP' THEN 10
            ELSE 1
        END as "numpayments",
         -- REVISE: need to determine number of payments remaining
        CASE
            WHEN P."paymentMethod" = 'QP' THEN 0
            WHEN P."paymentMethod" = 'MP' THEN 0
            ELSE 0
        END as "numpaymentsleft",
        ((P."premium" + P."premiumAdjust" + P."financeCharge") - P."receiveAmount") as "outstandingamount",
        (P."premium" + P."premiumAdjust" + P."financeCharge") - (P."receiveAmount" - P."financeChargeReceipt") as "outstandinggrossamount",
        1 as "activeind",
        NULL as "authorizationdate",
        NULL as "downpaymentreceivedind",
        NULL as "invoicenumber",
        TO_CHAR(P."effectiveDate", 'YYYYMMDD') as "invoicedate",  --?????
        NULL as "invoicecontactentitykey",
        NULL as "invoiceaddresskey",
        NULL as "conversionsever",
        NULL as "sequencenumber",
        NULL as "postedwweobjectid",
        NULL as "invoicecontactentityvnumber",
        NULL as "premfinancecontactentitykey",
        NULL as "premfinancecontactentkeyvnum",
        NULL as "premfinanceamount",
        NULL as "premfinancecontractdate",
        0 as "overridefeesind",
        NULL as "nocissuedate",
        2 as "directbillmailto",
        NULL as "taxchargeref"
    from (select * from "Policy" where ("premium" + "premiumAdjust") <> 0) P
        inner join (select * from "ISI_Policy" where "EVENTTYPEID" = 'PolicyIssue') PV on PV."policykey" = P."policyNumber"
        
        --left join "ISIConversionReferenceView" ICRV on ICRV."firmId" = P."firmId" and (P."effectiveDate" BETWEEN ICRV."startDate" and ICRV."endDate" and COALESCE(P."cancelEffectiveDate", P."expirationDate") BETWEEN ICRV."startDate" and ICRV."endDate")
    where P."effectiveDate" > '31-DEC-2005'
    -- BELOW ITEMS NEED FIXING
    --  and P."firmId" not in ('SKOGLA', 'CASEL1')
    --  and P."policyNumber" NOT in ('0607050', '0703097', '0807068', '0811071', '1003092', '1006065', '1007005', '1111122', '1112041', '1206065', '1308050', '1408002', '1505056', '1602014',  '1601090')
;

/*-- Testing --*/
select * from "ISI_BillingDetailView";
select * from "ISI_Policy" where "policykey" = '1111122';
select * from "ISI_BillingDetailView" where "POLICYNUMBER" = '1111122';
select * from "ISI_Policy" where "policykey" = '0702005';
select * from "PolicyAccounting" where "policyId" in ('AP000823716', 'AP001054520', 'AP11057746');
select * from "PolicyAccounting" where "financeChargeDue" <> 0;
select * from "Policy" where "policyId" in ('AP000823716', 'AP001054520');
select * from "Policy" where "firmId" in ('KIMLAV') order by "effectiveDate";
select * from "ISI_BillingDetailView" where "conversionreference" = 'PREVGO_1988_2018' order by "conversionreference", "policyeffectivedate";--PREVGO_1988_2018	4112	20100601	20110601
--------------------------------------------------------------------------------
/*
    ISI_PaymentScheduleView
    p_paymentschedule
    
    Dependancies: ISI_BillingDetailView
    
    NOTES:  04/23/2018---ISI TO CREATE PAYMENTSCHEDULE INTERIM TABLE AS PART OF THE GL CONVERSION
    
    History:
    03/08/2018  FZ Created
*/
create or replace view "ISI_PaymentScheduleView" as
    --Quarterly and Monthly pay plan
    select
--        IV."eventTypeId" as "EVENTTYPEID",
        PV."policykey" as "POLICYNUMBER",
        PV."conversionreference" as "conversionreference",
        PV."transactiontype" as "transactiontype",
        PV."transactioneffectivedate" as "transactioneffectivedate",
        PV."transactionseqno" as "transactionseqno",
        PV."sbuid" as"sbuid",
        PV."policyeffectivedate" as "policyeffectivedate",
        PV."policyexpirydate" as "policyexpirydate",
        --PV."vexpirydate" as "policyexpirydate",
        TO_CHAR(IV."dueDate", 'YYYYMMDD') as "scheduledate",
        IV."premium" as "amount",
        COALESCE(IV."financeCharge", 0) + COALESCE(IV."lateCharge", 0) as "fees",
        IV."invoiceAmount" as "totalamount",
        IV."premium" as "grossamount",
        CASE
            WHEN IV."paidDate" is NULL THEN 0
            ELSE 1
        END as "fixedind",
        NULL as "transactionid",
        CASE 
            WHEN IV."paidDate" is NULL THEN 0
            ELSE 1
        END as "paidind",
        CASE
            WHEN IV."eventTypeId" = 'InitialPayInvoice' THEN 1
            ELSE 0
        END as "downpaymentind",  --0=initial payment, 1=regular payment
        NULL as "conversionserver",
        NULL as "sequencenumber",
        NULL as "postedwweobjectid",
        TO_CHAR(IV."eventDate", 'YYYYMMDD') as "processeddate",
        IV."premium" as "premium",
        NULL as "comm",
        COALESCE(IV."financeCharge", 0) + COALESCE(IV."lateCharge", 0) as  "tfs",
        TO_CHAR(IV."dueDate", 'YYYYMMDD') as "duedate",
        CASE
            WHEN P."paymentMethod" = 'MP' THEN 'FAKE_ACCOUNT'
            ELSE NULL
        END as "paymentmethodkey"
    from (select * from "InvoiceView" where "invoiceAmount" <> 0) IV
        inner join "Policy" P on P."policyId" = IV."policyId"
            inner join (select * from "ISI_Policy" where "transactionseqno" = 1 and "transactiontype" <> 'RCA') PV on PV."policykey" = P."policyNumber"
--order by P."policyNumber", IV."dueDate"
/*
    from (select * from "ISI_PolicyView" where "transactionseqno" = 1) PV
        left join "Policy" P on P."policyNumber" = PV."policykey"
            left join "InvoiceView" IV on IV."policyId" = P."policyId"
*/
;
--union all   
create or replace view "ISI_PaymentScheduleFPView" as 
    -- Full pay policies
    select
        PV."policykey" as "POLICYNUMBER",
        PV."conversionreference" as "conversionreference",
        PV."transactiontype" as "transactiontype",
        PV."transactioneffectivedate" as "transactioneffectivedate",
        PV."transactionseqno" as "transactionseqno",
        PV."sbuid" as"sbuid",
        PV."policyeffectivedate" as "policyeffectivedate",
        PV."policyexpirydate" as "policyexpirydate",  --04/21/2018 removed to use vexpirydate to account for cancel/rewrites
        PA."paymentDate" as "scheduledate",
        PA."paymentAmount" as "amount",
        COALESCE(PA."financeCharge", 0) as "fees",
        PA."paymentAmount" as "totalamount",
        PA."premium" as "grossamount",
        1 as "fixedind",
        NULL as "transactionid",
        CASE 
            WHEN PA."paymentDate" is NULL THEN 0
            ELSE 1
        END as "paidind",
        1 as "downpaymentind",  --0=initial payment, 1=regular payment
        NULL as "conversionserver",
        NULL as "sequencenumber",
        NULL as "postedwweobjectid",
        TO_CHAR(PA."paymentDate", 'YYYYMMDD') as "processeddate",
        PA."premium" as "premium",
        NULL as "comm",
        COALESCE(PA."financeCharge", 0) as  "tfs",
        TO_CHAR(PA."paymentDate", 'YYYYMMDD') as "duedate",
        CASE
            WHEN P."paymentMethod" = 'MP' THEN 'FAKE_ACCOUNT'
            ELSE NULL
        END as "paymentmethodkey"
    from (select * from "Policy" where "paymentMethod" = 'PD') P
        inner join "PolicyAccountingSumView" PA on PA."policyId" = P."policyId"
        inner join (select * from "ISI_Policy" where "transactionseqno" = 1) PV on PV."policykey" = P."policyNumber"
;
--------------------------------------------------------------------------------
/*
    ISI_InsuredsView
    ISI table = p_insureds
    
    Dependancies: ISI_PolicyView

    History
    12/01/2017  FZ  Created
    03/31/2018  FZ  NOTE:: lengthy compile time so split into 2 views
*/
create or replace view "ISI_InsuredsFirmView" as
--  firm level required for each transaction sequence
    select
        P."policykey" as "POLICYNUMBER",
        P."alternatepolicykey" as "COVERAGEID",
        P."policyeffectivedate" as "POLICYEFFECTIVEDATE",
        P."policyexpirydate" as "POLICYEXPIRYDATE",
        P."veffectivedate" as "VEFFECTIVEDATE",
        P."vexpirydate" as "VEXPIRYDATE",
        P."cliententitymappingkey" as "FIRMID",
        P."termlengthind" as "POLICYTERM",
        P."EVENTTYPEID" as "EVENTTYPEID",
        P."conversionreference" as "conversionreference",
        P."transactiontype" as "transactiontype",
        P."transactioneffectivedate" as "transactioneffectivedate",
        P."transactionseqno" as "transactionseqno",
        4112 as "sbuid",
        1 as "insuredid",
        2 as "insuredrole",
        P."cliententitymappingkey" as "contactentitykey",
        0 as "contactentityvnumber",
        E."entityId"||'_'||L."locationId" as "defaultaddresskey",
        NULL as "defaultcontactmethodkey",
        NULL as "conversionserver",
        NULL as "sequencenumber",
        NULL as "postedwweobjectid",
        P."policykey"||TO_NUMBER(ORA_HASH(P."cliententitymappingkey")) as "ouid"
    from "ISI_Policy" P    --previously ISI_PolicyView
        left join "Entity" E on E."entityId" = P."cliententitymappingkey"
            left join "Location" L on L."locationId" = E."mainLocationId"
    --where P."transactioneffectivedate" > '20051231' ---
;
select * from "ISI_Policy" where "EVENTTYPEID" = 'Cvg-AA';

create or replace view "ISI_InsuredsLawyerView" as 
--  lawyer (insured items) level
    select unique
        P."policykey" as "POLICYNUMBER",
        P."alternatepolicykey" as "COVERAGEID",
        P."policyeffectivedate" as "POLICYEFFECTIVEDATE",
        P."policyexpirydate" as "POLICYEXPIRYDATE",
        P."veffectivedate" as "VEFFECTIVEDATE",
        P."vexpirydate" as "VEXPIRYDATE", 
        P."cliententitymappingkey" as "FIRMID",
        P."termlengthind" as "POLICYTERM",
        P."EVENTTYPEID" as "EVENTTYPEID",
        P."conversionreference" as "conversionreference",
        P."transactiontype" as "transactiontype",
        P."transactioneffectivedate" as "transactioneffectivedate",
        P."transactionseqno" as "transactionseqno",
        4112 as "sbuid",
        PLC."lawyerPolicySequence" as "insuredid",
        7 as "insuredrole",
        PLC."lawyerId" as "contactentitykey",
        0 as "contactentityvnumber",
        NULL as "defaultaddresskey",
        NULL as "defaultcontactmethodkey",
        NULL as "conversionserver",
        NULL as "sequencenumber",
        NULL as "postedwweobjectid",
        P."policykey"||TO_NUMBER(ORA_HASH(PLC."lawyerId")) as "ouid"
    --from "ISI_Policy" P
    from (select * from "ISI_Policy" where "EVENTTYPEID" not in ('Cvg-SC', 'Cvg-EC', 'Cvg-ED', 'Cvg-EU', 'Cvg-CP', 'Cvg-ND', 'Cvg-NP', 'Cvg-NU', 'Cvg-DA', 'Cvg-AA', 'Cvg-PA')) P  
        left join "PolicyLawyerCoverageAllView" PLC on "policyId" = P."POLICYID"
        ---- date logic to exclude deleted lawyers since deleted lawyers appear as 2 records in LAW (add/delete)
            and (P."transactioneffectivedate" BETWEEN TO_CHAR(PLC."effectiveDate", 'YYYYMMDD') and TO_CHAR(PLC."expirationDate", 'YYYYMMDD')
            and P."transactioneffectivedate" <= TO_CHAR(COALESCE(PLC."excludeAfterDate", PLC."expirationDate"), 'YYYYMMDD'))  -- previously compare was "<"
    where PLC."effectiveDate" > '31-DEC-2005'

union all
--create or replace view "ISI_InsuredsLawyerERPView" as 
--  lawyer tail level (04/30/2018)
    select
        P."policykey" as "POLICYNUMBER",
        P."alternatepolicykey" as "COVERAGEID",
        P."policyeffectivedate" as "POLICYEFFECTIVEDATE",
        P."policyexpirydate" as "POLICYEXPIRYDATE",
        P."veffectivedate" as "VEFFECTIVEDATE",
        P."vexpirydate" as "VEXPIRYDATE", 
        P."cliententitymappingkey" as "FIRMID",
        P."termlengthind" as "POLICYTERM",
        P."EVENTTYPEID" as "EVENTTYPEID",
        P."conversionreference" as "conversionreference",
        P."transactiontype" as "transactiontype",
        P."transactioneffectivedate" as "transactioneffectivedate",
        P."transactionseqno" as "transactionseqno",
        4112 as "sbuid",
        PLC."lawyerPolicySequence" as "insuredid",
        7 as "insuredrole",
        PLC."lawyerId" as "contactentitykey",
        0 as "contactentityvnumber",
        NULL as "defaultaddresskey",
        NULL as "defaultcontactmethodkey",
        NULL as "conversionserver",
        NULL as "sequencenumber",
        NULL as "postedwweobjectid",
        P."policykey"||TO_NUMBER(ORA_HASH(PLC."lawyerId")) as "ouid"
    from (select * from "ISI_Policy" where "transactiontype" = 'ERP') P  --previously ISI_PolicyView
        inner join "TailLawyer2" TL on TL."coverageId" = P."alternatepolicykey"  
            left join "PolicyLawyerCoverageAllView" PLC on PLC."policyId" = TL."policyId" and PLC."lawyerId" = TL."lawyerId"
       
    /*
        left join "PolicyLawyerCoverageAllView" PLC on "policyId" = P."POLICYID"
        ---- date logic to exclude deleted lawyers since deleted lawyers appear as 2 records in LAW (add/delete)
            and (P."transactioneffectivedate" BETWEEN TO_CHAR(PLC."effectiveDate", 'YYYYMMDD') and TO_CHAR(PLC."expirationDate", 'YYYYMMDD')
            and P."transactioneffectivedate" <= TO_CHAR(COALESCE(PLC."excludeAfterDate", PLC."expirationDate"), 'YYYYMMDD'))  -- previously compare was "<"
    */
    where PLC."effectiveDate" > '31-DEC-2005'
--order by "POLICYNUMBER", "transactioneffectivedate", "insuredrole", "contactentitykey"
--;
union all
--create or replace view "ISI_InsuredsLawyerENDView" as 
--  lawyer endorsement level (04/30/2018)
    select
        P."policykey" as "POLICYNUMBER",
        P."alternatepolicykey" as "COVERAGEID",
        P."policyeffectivedate" as "POLICYEFFECTIVEDATE",
        P."policyexpirydate" as "POLICYEXPIRYDATE",
        P."veffectivedate" as "VEFFECTIVEDATE",
        P."vexpirydate" as "VEXPIRYDATE", 
        P."cliententitymappingkey" as "FIRMID",
        P."termlengthind" as "POLICYTERM",
        P."EVENTTYPEID" as "EVENTTYPEID",
        P."conversionreference" as "conversionreference",
        P."transactiontype" as "transactiontype",
        P."transactioneffectivedate" as "transactioneffectivedate",
        P."transactionseqno" as "transactionseqno",
        4112 as "sbuid",
        PLCV."lawyerPolicySequence" as "insuredid",
        7 as "insuredrole",
        PLCV."lawyerId" as "contactentitykey",
        0 as "contactentityvnumber",
        NULL as "defaultaddresskey",
        NULL as "defaultcontactmethodkey",
        NULL as "conversionserver",
        NULL as "sequencenumber",
        NULL as "postedwweobjectid",
        P."policykey"||TO_NUMBER(ORA_HASH(PLC."lawyerId")) as "ouid"
    from (select * from "ISI_Policy" where "EVENTTYPEID" in ('Cvg-DA', 'Cvg-AA', 'Cvg-PA'))P  --previously ISI_PolicyView
        --inner join "TailLawyer2" TL on TL."coverageId" = P."alternatepolicykey"  
        inner join "PolicyLawyerCoverage" PLC on PLC."coverageId" = P."alternatepolicykey" 
            left join "PolicyLawyerCoverageAllView" PLCV on PLCV."policyId" = PLC."policyId" and PLCV."lawyerId" = PLC."lawyerId"
    where PLC."effectiveDate" > '31-DEC-2005'
order by "POLICYNUMBER", "transactioneffectivedate", "insuredrole", "contactentitykey"
;


/*-- Testing --*/
select * from "ISI_InsuredsFirmView"; where "POLICYNUMBER" = '1003092';
select * from "ISI_InsuredsLawyerView"; where "transactiontype" is NULL;
select count(*) from "ISI_InsuredsLawyerView";
select * from "ISI_PolicyView" where "policykey" = '1003092';
select * from "ISI_PolicyView" where "policykey" = '0601090';
select * from "ISI_InsuredsLawyerView" where "POLICYNUMBER" = '0601090';
select * from "ISI_InsuredsLawyerView" where "contactentitykey" = 'SKOGLA'; 
select * from "ISI_PolicyView";
select * from "PolicyLawyerCoverageAllView" where "policyId" = 'AP11057746';
select * from "PolicyLawyerCoverageAllView" where "policyId" = 'AP11696463';
select * from "PolicyLawyerCoverage" where "policyId" = 'AP11057746';
select * from "PolicyLawyer" where "policyId" = 'AP11057746';

select * from "ISI_AddressView" where "entitykey" in ('KIMLAV', 'KIM_JULI');
select * from "ISI_ClientView" where "entitykey" = 'KIMLAV';
select * from "ISI_ContactView" where "entitykey" = 'KIM_JULI';
select * from "ISI_PolicyView"; --1606112  KIM_JULI
select * from "PolicyLawyerCoverage" where "policyId" in ('AP000823716', 'AP001054520') order by "policyId", "effectiveDate";
-- tail testing
select * from "ISI_PolicyView" where "policykey" in ('1710130', '1608097', '1510133');

--------------------------------------------------------------------------------    
/*
    ISI_ItemView
    ISI table = p_items
    
    Dependancies: ISI_InsuredsView
    
    History
    12/01/2017  FZ  Created
    03/31/2018  FZ  split into 2 views
    
*/    
select count(*) from "ISI_ItemFirmView";
create or replace view "ISI_ItemFirmView" as 
    -- firm record
    select
        IV."conversionreference" as "conversionreference",
        IV."transactiontype" as "transactiontype",
        IV."POLICYNUMBER" as "POLICYNUMBER", 
        IV."COVERAGEID" as "COVERAGEID",
        NULL as "LAWYERID",
        IV."POLICYTERM" as "POLICYTERM",
        IV."EVENTTYPEID" as "EVENTTYPEID",
        IV."transactioneffectivedate" as "transactioneffectivedate",
        IV."transactionseqno" as "transactionseqno",
        'LPL__FIRM__WI_19000101_20991231_4112' as "pcmitemmappingkey",
        NULL as "pcmfloatermappingkey",
        IV."sbuid" as "sbuid",
        0 as "packageid",
        IV."insuredid" as "riskid",  
        NULL as "locationnumber",
        1 as "riskgroup", -- firm
        NULL as "commissionamt",
        NULL as "commissioncalcamt",
        NULL as "commissiontype",    
        0 as "deletedrow",
        NULL as "conversionserver",
        NULL as "sequencenumber",
        IV."VEFFECTIVEDATE" as "veffectivedate",
        IV."VEXPIRYDATE" as "vexpirydate",
        NULL as "formcode",
        1 as "claimsmadeind",
        TO_CHAR(Z."ISIPolicyRetroDate", 'YYYYMMDD') as "retrodate",
        NULL as "postedwweobjectid",
        NULL as "overridereasoncd",
        NULL as "overridereasonnote",
        NULL as "billbyitemind",
        NULL as "taxamount",
        NULL as "parentriskid",
        NULL as "buildingnumber",
        NULL as "occupancynumber",
        'LPL_Ratebook' as "rateeditionname",
        2 as "rateeditionversion",
        NULL as "vehicleuse",
        NULL as "accumulationcode",
        IV."ouid" as "ouid",
        CASE 
            WHEN IV."transactiontype" = 'ERP' THEN TO_CHAR(TLA."effectiveDate", 'YYYYMMDD') 
            ELSE NULL
        END as "extendreporteffdate",
        CASE
            WHEN IV."transactiontype" = 'ERP' AND TO_CHAR(TLA."expirationDate", 'DD-MON-YY') = '31-DEC-99' THEN '99991231'
            WHEN IV."transactiontype" = 'ERP' AND TLA."tailTerm" is not NULL THEN TO_CHAR(TLA."expirationDate", 'YYYYMMDD')
            ELSE NULL
        END as "extendreportexpdate",
        CASE
            WHEN IV."transactiontype" = 'ERP' AND TLA."tailTerm" = 'Unlimited' THEN NULL
            WHEN IV."transactiontype" = 'ERP' AND TLA."tailTerm" in (1,2,3,4,5,6) THEN TLA."tailTerm"
            ELSE NULL
        END as "extendreporttermlength",
        CASE
            WHEN IV."transactiontype" = 'ERP' AND TLA."tailTerm" = 'Unlimited' THEN 'U'
            WHEN IV."transactiontype" = 'ERP' AND TLA."tailTerm" is not NULL THEN 'Y'
            ELSE NULL
        END as "extendreportlengthunit",
        IV."FIRMID" as "FIRMID"
    from "ISI_InsuredsFirmView" IV 
        left join "ISIPolicyRetroDateView" Z on Z."policyNumber" = IV."POLICYNUMBER"
    --    left join (select unique "coverageId", "effectiveDate", "expirationDate", "tailTerm", sum("oldTailPremium") as "tailPremiumTotal" from "TailLawyerAllView" where "tailType" = 'Firm' group by "policyNumber", "effectiveDate", "expirationDate", "tailTerm") TLA on TLA."policyNumber" = IV."POLICYNUMBER"
        left join (select unique "coverageId", "effectiveDate", "expirationDate", "tailTerm" from "TailLawyerAllView") TLA on TLA."coverageId" = IV."COVERAGEID"
    where IV."insuredrole" = 2
;
select * from "ISI_ItemFirmView"; where "conversionreference" = 'KLOSFL_1987_2007';
select * from "ISI_InsuredsFirmView" where "conversionreference" = 'KLOSFL_1987_2007';
select * from "TailLawyerAllView" where "policyNumber" = '0607050';

select count(*) from "ISI_ItemLawyerView";
create or replace view "ISI_ItemLawyerView" as 
-- lawyer record
    select
        IV."conversionreference" as "conversionreference",
        IV."transactiontype" as "transactiontype",
        IV."POLICYNUMBER" as "POLICYNUMBER",  
        IV."COVERAGEID" as "COVERAGEID",
        IV."contactentitykey" as "LAWYERID",
        IV."POLICYTERM" as "POLICYTERM",
        IV."EVENTTYPEID" as "EVENTTYPEID",
        IV."transactioneffectivedate" as "transactioneffectivedate",
        IV."transactionseqno" as "transactionseqno",
        'LPL__ATTORNEY__WI_19000101_20991231_4112' as "pcmitemmappingkey",
        NULL as "pcmfloatermappingkey",
        4112 as "sbuid",
        0 as "packageid",
        --SUBSTR(TO_CHAR(TO_NUMBER(ORA_HASH(Y."lawyerId"))),-3) as "riskid",  --unique number hashed using lawyerId
        IV."insuredid" as "riskid",
        NULL as "locationnumber",
        1 as "riskgroup", -- firm
        NULL as "commissionamt",
        NULL as "commissioncalcamt",
        NULL as "commissiontype", 
        CASE 
            WHEN (TO_CHAR(Y."expirationDate", 'YYYYMMDD') <> IV."POLICYEXPIRYDATE") and (TO_CHAR(Y."expirationDate", 'YYYYMMDD') <= IV."transactioneffectivedate") THEN 1
            ELSE 0
        END as "deletedrow",
        NULL as "conversionserver",
        NULL as "sequencenumber",
        IV."VEFFECTIVEDATE" as "veffectivedate", --01/14/2018 previously TO_CHAR(P."effectiveDate", 'YYYYMMDD') as "veffectivedate",
        IV."VEXPIRYDATE" as "vexpirydate",
        NULL as "formcode",
        1 as "claimsmadeind",
        CASE 
            WHEN X."ISILawyerRetroDate" is not NULL THEN TO_CHAR(X."ISILawyerRetroDate", 'YYYYMMDD') 
            ELSE IV."VEFFECTIVEDATE"
        END as "retrodate",
        NULL as "postedwweobjectid",
        NULL as "overridereasoncd",
        NULL as "overridereasonnote",
        NULL as "billbyitemind",
        NULL as "taxamount",
        1 as "parentriskid",  --fix #
        NULL as "buildingnumber",
        NULL as "occupancynumber",
        'LPL_Ratebook' as "rateeditionname",
        2 as "rateeditionversion",
        NULL as "vehicleuse",
        NULL as "accumulationcode",
        TO_CHAR(TO_NUMBER(ORA_HASH(IV."POLICYNUMBER")))||SUBSTR(TO_CHAR(TO_NUMBER(ORA_HASH(IV."insuredid"))),-3) as "ouid",  --
        CASE 
            WHEN IV."transactiontype" = 'ERP' THEN TO_CHAR(TLA."effectiveDate", 'YYYYMMDD') 
            ELSE NULL
        END as "extendreporteffdate",
        CASE
            WHEN IV."transactiontype" = 'ERP' AND TO_CHAR(TLA."expirationDate", 'DD-MON-YY') = '31-DEC-99' THEN '99991231'
            WHEN IV."transactiontype" = 'ERP' AND TLA."tailTerm" is not NULL THEN TO_CHAR(TLA."expirationDate", 'YYYYMMDD')
            ELSE NULL
        END as "extendreportexpdate",
        CASE
            WHEN IV."transactiontype" = 'ERP' AND TLA."tailTerm" = 'Unlimited' THEN NULL
            WHEN IV."transactiontype" = 'ERP' AND TLA."tailTerm" in (1,2,3,4,5,6) THEN TLA."tailTerm"
            ELSE NULL
        END as "extendreporttermlength",
        CASE
            WHEN IV."transactiontype" = 'ERP' AND TLA."tailTerm" = 'Unlimited' THEN 'U'
            WHEN IV."transactiontype" = 'ERP' AND TLA."tailTerm" is not NULL THEN 'Y'
            ELSE NULL
        END as "extendreportlengthunit",
        IV."FIRMID" as "FIRMID"      
    from (select * from "ISI_InsuredsLawyerView" where "COVERAGEID" <> 'CG001148214') IV 
        left join "PolicyLawyerCoverageAllView" Y on Y."policyNumber" = IV."POLICYNUMBER" and Y."lawyerId" = IV."contactentitykey" and (TO_CHAR(Y."effectiveDate", 'YYYYMMDD') <= IV."transactioneffectivedate")
        left join "ISILawyerRetroDateView" X on X."lawyerId" = IV."contactentitykey" and X."policyNumber" = IV."POLICYNUMBER" 
        --left join (select unique * from "TailLawyerAllView") TLA on TLA."coverageId" = IV."COVERAGEID" and TLA."lawyerId" = IV."contactentitykey"
        left join (select * from "TailLawyerView") TLA on TLA."coverageId" = IV."COVERAGEID" and TLA."lawyerId" = IV."contactentitykey"
    where IV."insuredrole" = 7
--order by "POLICYNUMBER", "transactioneffectivedate", "transactionseqno", "riskid"
;       
    
 
/*--- Testing  ---*/
select * from "PolicyLawyerCoverageAllView" where "policyNumber" = '1202024';
select * from "ISILawyerRetroDateView" where "lawyerId" = 'EY000200317' and "policyNumber" = '1202024';
select * from "ISI_ItemView";
select * from "ISI_ItemLawyerView";
select * from "ISI_ItemLawyerView" where "POLICYNUMBER" = '0709073';
select * from "ISI_ItemLawyerView" where "POLICYNUMBER" = '0601090' order by "FIRMID", "POLICYNUMBER", "transactioneffectivedate", "transactionseqno";
select "FIRMID", IV.* from "ISI_ItemLawyerView" IV order by IV."FIRMID", IV."POLICYNUMBER", IV."transactioneffectivedate", IV."transactionseqno";
select * from "ISILawyerRetroDateView"; 
select * from "ISILawyerRetroDateView" where "lawyerId" = 'LSB1031169';
select * from "ISIPolicyRetroDateView" order by "policyId";
select * from "ISI_InsuredsView" where "POLICYNUMBER" = '1202024';
select P."firmId", P."policyNumber",PLC.* from "PolicyLawyerCoverage" PLC left join "Policy" P on P."policyId" = PLC."policyId" where P."firmId" in ('KIMLAV') order by P."policyNumber", "lawyerId";
select * from "ISI_PolicyView";
select * from "PolicyLawyerCoverageAllView" where "lawyerId" = 'LSB1031169';
--------------------------------------------------------------------------------
/*
    ISI_LawFirm1View
    i_lawfirm_1
    
    Dependancies:
    
    History
    12/01/2017  FZ  Created
*/
create or replace view "ISI_LawFirm1View" as
    select
        PV."conversionreference" as "conversionreference",
        PV."transactioneffectivedate" as "transactioneffectivedate",
        PV."transactionseqno" as "transactionseqno",
        PV."transactiontype" as "transactiontype",
        P."policyNumber" as "POLICYNUMBER",
        PV."sbuid" as "sbuid",
        0 as "packageid",
        1 as "riskid",
        NULL as "abstracterentity",
        CASE
            WHEN AQAT."questionId" IS NOT NULL THEN 1
            ELSE 0
        END as "abstracterind",
        NULL as "abstracterinscompagentind",
        NULL as "abstracterownfirmentitylabel",
        NULL as "abstracterrevenue", 
        NULL as "affiliationind",
        COALESCE(RC."SchdAdjPctCalc",1.00) - COALESCE(RC."SeverityIndexZero",0) as "aopfactorschedadj",
        NULL as "attorneydisbarredind",
        NULL as "attorneydisbarredinfo",
        NULL as "attorneyerrorind",
        NULL as "attorneygrievanceind",
        0 as "calcpremiumxs5m",
        0 as "claimsratioactloss10_1",
        0 as "claimsratioactloss1_1",
        0 as "claimsratioactloss3_1",
        0 as "claimsratioactloss5_1",
        0 as "claimsratioactlossall_1",
        0 as "claimsratioactratio10_1",
        0 as "claimsratioactratio1_1",
        0 as "claimsratioactratio3_1",
        0 as "claimsratioactratio5_1",
        0 as "claimsratioactratioall_1",
        0 as "claimsratioincloss10_1",
        0 as "claimsratioincloss1_1",
        0 as "claimsratioincloss3_1",
        0 as "claimsratioincloss5_1",
        0 as "claimsratioinclossall_1",
        0 as "claimsratioincratio10_1",
        0 as "claimsratioincratio1_1",
        0 as "claimsratioincratio3_1",
        0 as "claimsratioincratio5_1",
        0 as "claimsratioincratioall_1",
        0 as "claimsratioprem10_1",
        0 as "claimsratioprem1_1",
        0 as "claimsratioprem3_1",
        0 as "claimsratioprem5_1",
        0 as "claimsratiopremall_1",
        COALESCE(RC."ClaimsSurcharge",0) * 100 as "claimsurcharge",
        NULL as "claimsurchargecreditmax",
        NULL as "claimsurchargecreditmin",
        'N/A' as "claimsurchargecreditrange",
        '150' as "claimsurchargedebitmax",
        '5' as "claimsurchargedebitmin",
        '5% to 150%' as "claimsurchargedebitrange",
        NULL as "conditionalmandatoryfields",
        NULL as "contactwhenabsent",
        NULL as "contingencyfeeind",
        1 + COALESCE(RC."YearsInsured", 0) as "continuityfactor",
        0 as "conversionpremiumrounding",
        COALESCE(PE."WLMEND02", NULL) as "covind_wlmend02",
        COALESCE(PE."WLMEND03", NULL) as "covind_wlmend03",
        NULL as "covind_wlmend22",
        NULL as "covind_wlmend23",
        COALESCE(PE."WLMEND24", NULL) as "covind_wlmend24",
        COALESCE(PE."WLMEND25", NULL) as "covind_wlmend25",
        NULL as "covind_wlmend27",
        COALESCE(PE."Manuscript", NULL) as "covind_wlmendmn",
        RLC."PremXS10mCalc" as "facpremiumxs10m",
        RLC."PremXS5mMinCalc" as "facpremiumxs5m",     
        NULL as "financialinterestclientind",
        A."firmName" as "firmname",
        A."totalLawyerCount" as "firmsize", --lawyerPrincipalCount
        COALESCE(RC."Firm", 1.00) as "firmsizefactor",  
        A."totalLawyerCount" as "firmsizeforrating",  
        --EN."entityURL" as "firmwebsite",
        NULL as "foreignclientcountries",
        NULL as "foreignclientind",
        0 as "insurepastworkind", --R2
        NULL as "licensingagreementind",
        NULL as "licensingagreementpct",
        NULL as "localaffiliateind",
        A."staffCount" as "nonattorneystaff", 
        0 as "officerclientind",
        0 as "officesharingind",
        0 as "oneclient10pctind",
        O."organizationLegalForm" as "organizationstructure",
        --start new release 2
        COALESCE(RC."OtherAdjustment",0) * 100 as "otherdebit", --OtherAdjustment
        NULL as "otherdebitcreditmax",
        NULL as "otherdebitcreditmin",
        NULL as "otherdebitcreditrange",
        NULL as "otherdebitdebitmax",
        NULL as "otherdebitdebitmin",
        NULL as "otherdebitdebitrange",
        NULL as "otherdebitexplain",
        0 as "outofstatefutureind",
        --end new release 2
        0 as "outofstateind",
        NULL as "outsoursepaymentind",
        NULL as "outsoursesearchind",
        NULL as "percentipcatcopyright",
        NULL as "percentipcatdomestic",
        NULL as "percentipcatforeign",
        NULL as "percentipcatinfringement",
        NULL as "percentipcatintproperty",
        NULL as "percentipcatother",
        NULL as "percentipcatotherexplain",
        NULL as "percentipcattrademark",
        NULL as "percentiptypebiotechnology",
        NULL as "percentiptypebusiness",
        NULL as "percentiptypechemical",
        NULL as "percentiptypecomputer",
        NULL as "percentiptypeelectrical",
        NULL as "percentiptypemechanical",
        NULL as "percentiptypeother",
        NULL as "percentiptypeotherexplain",
        NULL as "predecessorfirmsnotapplicab_1",
        0 as "prevlawfirmind",
        0 as "priordeclinedind",
        NULL as "priordeclinedinfo",
        NULL as "priortailind",
        NULL as "priortailinfo",
        0 as "reinsurancepremium1m",  --R2
        0 as "reinsurancepremium1mfactor", --R2
        0 as "reinsurancepremiumxs1", --R2
        NULL as "restrictprioractsind",
        NULL as "restrictprioractsinfo",
        RC."SchdAdjPctCalc" * 100 as "scheduledadjustmentstotal",
        COALESCE(RC."SeverityIndex", 0) * 100 as "severityindex",  --06/04/2018
        NULL as "severityindexcreditmax",
        NULL as "severityindexcreditmin",
        'N/A' as "severityindexcreditrange",
        '20' as "severityindexdebitmax",
        '1' as "severityindexdebitmin",
        '1% to 20%' as "severityindexdebitrange",
        COALESCE(RC."SeverityIndexZero", 0) * 100 as "severityindexzero",  --06/04/2018 *100
        NULL as "severityindexzerocreditmax",
        NULL as "severityindexzerocreditmin",
        'N/A' as "severityindexzerocreditrange",
        NULL as "severityindexzerodebitmax",
        NULL as "severityindexzerodebitmin",
        'N/A' as "severityindexzerodebitrange",
        NULL as "tailfactorunlimited",
        NULL as "tailfactoryear_1",
        NULL as "tailfactoryear_2",
        NULL as "tailfactoryear_3",
        NULL as "tailfactoryear_6",
        NULL as "tailpremiumunlimited",
        NULL as "tailpremiumyear_1",
        NULL as "tailpremiumyear_2",
        NULL as "tailpremiumyear_3",
        NULL as "tailpremiumyear_6",
        NULL as "thirdpartydocketind",
        NULL as "thirdpartydocketsystem",
        NULL as "wlmendmn_body", --R2
        NULL as "wlmendmn_code", --R2
        NULL as "wlmendmn_desc", --R2
        EXTRACT(YEAR FROM O."establishDate") as "yearsestablished", --R2
        A."insureDuration" as "yearsinsured",
        NULL as "conversionserver",
        NULL as "sequencenumber"
     from "ISI_Policy" PV  --previously ISI_PolicyView
        left join "Policy" P on P."policyNumber" = PV."policykey"
            left join "Application" A on A."applicationId" = P."policyId"
                left join (select * from "ApplicationQuestion" where "questionId" = 'AT') AQAT on AQAT."applicationId" = A."applicationId"
                left join "RatingCrosstab" RC on RC."applicationId" = A."applicationId"
                    left join "RatingLimitCrosstab" RLC on RLC."quoteBookId" = RC."quoteBookId" and RLC."Deductible" = P."perClaimDeductible" and RLC."Limit" = P."perClaimLimit" and RLC."LimitAgg" is NULL
        left join "Entity" EN on EN."entityId" = P."firmId" 
        left join "Organization" O on O."organizationId" = P."firmId"
        left join "ISIPolicyEndorsementView" PE on PE."policyNumber" = PV."policykey"
    -- BELOW ITEMS NEED FIXING
    --  where PV."policykey" not in ('0604074')  --endorsement effective date = 20050501 while policy effective date = 20060417
    order by "POLICYNUMBER", "transactioneffectivedate", "transactionseqno"
;

/*-- Testing --*/
select * from "ISI_LawFirm1View" where "conversionreference" = 'VELEMO_2006_2007';
select * from "ISI_LawFirm1View" ;
select * from "Policy" where "firmId" in ('KIMLAV', 'VELEMO');
select * from "ISI_PolicyView"; where "policyeffectivedate" < '20060101';
select * from "Application" where "firmId" in ('KIMLAV') order by "effectiveDate";
select * from "RatingCrosstab" where "firmId" in ('KIMLAV') order by "effectiveDate";
select RLC.* from "RatingCrosstab" RC left join "RatingLimitCrosstab" RLC on RLC."quoteBookId" = RC."quoteBookId" where RC."firmId" in ('KIMLAV') order by RLC."quoteBookId", RLC."quoteCoverageId";
select * from "Quote";
select * from "QuoteBook";
select * from "QuoteCoverage";
select * from "RatingLimitCrosstab" order by "quoteBookId", "quoteCoverageId";
select 
    QC.*,
    Q.*,
    A.* 
from "Application" A 
    left join "Policy" P on P."policyId" = A."applicationId"
    inner join"Quote" Q on Q."applicationId" = A."applicationId" 
        left join "QuoteCoverage" QC on QC."quoteId" = Q."quoteId" and QC."perClaimLimit" = P."perClaimLimit" and QC."aggregateLimit" = P."aggregateLimit" and QC."perClaimDeductible" = P."perClaimDeductible"
where A."firmId" in ('KIMLAV') order by A."effectiveDate";
select * from "ISIPolicyEndorsementView";
--------------------------------------------------------------------------------
/*
    ISI_Attorney1View
    i_attorney_1
    
    Dependancies:
    
    History
    12/04/2017  FZ  Created
*/
create or replace view "ISI_Attorney1View" as
    with A1 as (
        select
            IV."conversionreference" as "conversionreference",
            P."policyNumber" as "POLICYNUMBER",
            IV."transactioneffectivedate" as "transactioneffectivedate",
            IV."transactionseqno" as "transactionseqno",
            IV."transactiontype" as "transactiontype",
            IV."sbuid" as "sbuid",
            0 as "packageid",
            IV."riskid" as "riskid",  --should be same during concecutive policy period
            NULL as "attorneyaddedthisterm",
            L."lawyerDesignationId" as "attorneydesignation",
            P."email" as "attorneyemail",
            COALESCE(RLC."LawyerFactor", 1.00) as "attorneyfactor1",  -- this is a problem for added lawyers
            P."firstName"||' '||P."middleName"||' '||P."lastName" as "attorneyname",
            IV."LAWYERID" as "attorneynameid_ek",
            0 as "attorneynameid_vn",
            --SUBSTR(TO_CHAR(TO_NUMBER(ORA_HASH(IV."LAWYERID"))),-3) as "attorneynumber",  --unique number hashed using lawyerId
            IV."riskid" as "attorneynumber",
            0 as "clecredits",
            DECODE(COALESCE(RLC."ContLegalEdCredit", 0), 0, 1.00, RLC."ContLegalEdCredit") as "clecreditsfactor",
            NULL as "currentleavestartdate",
            NULL as "currentlyonleaveind",
    --        NULL as "dateofbirth",  --01/16/2018
            NULL as "endleavethistransind", 
            COALESCE(RLC."LawyerActivity", 1.00) * 160 as "hoursworkedpermonth",
            TO_CHAR(COALESCE(L."inPracticeDate", LSB."barAdmitDate"), 'YYYYMMDD') as "inpracticedate",
            NULL as "militaryleavetext",
            0 as "militaryleavetotaldays",
            NULL as "newattorneyprioracts",  --NEEDS WORK:::find WLMEND26
            NULL as "otherlicenses",
            NULL as "parttimecomment",
            COALESCE(RLC."LawyerActivity", 1.00) as "parttimefactor",
            NULL as "parttimefactoroverride",
            COALESCE(RLC."LawyerActivity", 1.00) as "parttimefactorsys", --make same as parttimefactor
            NULL as "parttimeoverridecomment",
            TO_CHAR(LSB."barAdmitDate", 'YYYY')||'-WI-'||LSB."stateBarNumber" as "primarylicense",
            'Less than '||(EXTRACT(YEAR from P."effectiveDate") - to_number(substr(IV."retrodate",1,4))) as "retrodatecomment",
    --        'Less than '||(to_number(to_char(P."effectiveDate", 'YYYY')) - to_number(to_char(IV."retrodate", 'YYYY'))) as "retrodatecomment",
            CASE
                WHEN RLC."LawyerRDI" is NULL THEN
                    CASE
                        WHEN TRUNC(MONTHS_BETWEEN(P."effectiveDate", TO_DATE(IV."retrodate",'YYYYMMDD'))/12) < 1 THEN 0.40
                        WHEN TRUNC(MONTHS_BETWEEN(P."effectiveDate", TO_DATE(IV."retrodate",'YYYYMMDD'))/12) < 2 THEN 0.55
                        WHEN TRUNC(MONTHS_BETWEEN(P."effectiveDate", TO_DATE(IV."retrodate",'YYYYMMDD'))/12) < 3 THEN 0.70
                        WHEN TRUNC(MONTHS_BETWEEN(P."effectiveDate", TO_DATE(IV."retrodate",'YYYYMMDD'))/12) < 4 THEN 0.80
                        WHEN TRUNC(MONTHS_BETWEEN(P."effectiveDate", TO_DATE(IV."retrodate",'YYYYMMDD'))/12) < 5 THEN 0.90
                        ELSE 1.00
                    END
                ELSE  RLC."LawyerRDI"
            END as "retrodatefactor",
            --COALESCE(RLC."LawyerRDI", 1.00) as "retrodatefactor",
            IV."retrodate" as "retrodateoverride",
            NULL as "startleavethistransind",
            NULL as "tailattorneyfactor",
            NULL as "taildeductiblefactor",
            NULL as "tailfactor",
            NULL as "tailfactorunlimited",
            NULL as "tailfactoryear_1",
            NULL as "tailfactoryear_2",
            NULL as "tailfactoryear_3",
            NULL as "tailfactoryear_6",
            NULL as "tailpremiumbefattorneyfactor",
            NULL as "tailpremiumfactoredup",
            NULL as "tailpremiumstart",
            NULL as "tailpremiumtotal",
            NULL as "tailpremiumunlimited",
            NULL as "tailpremiumwodeductible",
            NULL as "tailpremiumxs10m",
            NULL as "tailpremiumxs5m",
            NULL as "tailpremiumyear_1",
            NULL as "tailpremiumyear_2",
            NULL as "tailpremiumyear_3",
            NULL as "tailpremiumyear_6",
            NULL as "tailschedadjustfactor",
            NULL as "yearinpracticecommentor",
            CASE
                WHEN TO_NUMBER(TO_CHAR(COALESCE(L."inPracticeDate", LSB."barAdmitDate"), 'YYYY')) > TO_NUMBER(TO_CHAR(P."effectiveDate", 'YYYY')) THEN 'Based on In Practice Date of 0 years'
                ELSE 'Based on In Practice Date of '||(TO_CHAR(TO_NUMBER(TO_CHAR(P."effectiveDate", 'YYYY'))) - TO_NUMBER(TO_CHAR(COALESCE(L."inPracticeDate", LSB."barAdmitDate"), 'YYYY')))||' years' 
            END as "yearinpracticecommentsys", 
            CASE
                WHEN RLC."LawyerYIP" is NULL THEN
                    CASE
                        WHEN TRUNC(MONTHS_BETWEEN(P."effectiveDate", COALESCE(L."inPracticeDate", LSB."barAdmitDate"))/12) < 1 THEN 0.30
                        WHEN TRUNC(MONTHS_BETWEEN(P."effectiveDate", COALESCE(L."inPracticeDate", LSB."barAdmitDate"))/12) < 2 THEN 0.40
                        WHEN TRUNC(MONTHS_BETWEEN(P."effectiveDate", COALESCE(L."inPracticeDate", LSB."barAdmitDate"))/12) < 3 THEN 0.50
                        WHEN TRUNC(MONTHS_BETWEEN(P."effectiveDate", COALESCE(L."inPracticeDate", LSB."barAdmitDate"))/12) < 4 THEN 0.65
                        WHEN TRUNC(MONTHS_BETWEEN(P."effectiveDate", COALESCE(L."inPracticeDate", LSB."barAdmitDate"))/12) < 5 THEN 0.75
                        WHEN TRUNC(MONTHS_BETWEEN(P."effectiveDate", COALESCE(L."inPracticeDate", LSB."barAdmitDate"))/12) < 6 THEN 0.90
                        ELSE 1.00
                    END
                ELSE RLC."LawyerYIP"
             END as "yearinpracticefactor",   
            --COALESCE(RLC."LawyerYIP", 1.00) as "yearinpracticefactor",
            NULL as "yearsinpracticefactoror",
            COALESCE(RLC."LawyerYIP", 1.00) as "yearsinpracticefactorsys",
            NULL as "yearsinpracticeor",
            CASE
                WHEN TO_NUMBER(TO_CHAR(P."effectiveDate", 'YYYY')) - TO_NUMBER(TO_CHAR(COALESCE(L."inPracticeDate", LSB."barAdmitDate"), 'YYYY')) > 0 THEN TO_NUMBER(TO_CHAR(P."effectiveDate", 'YYYY')) - TO_NUMBER(TO_CHAR(COALESCE(L."inPracticeDate", LSB."barAdmitDate"), 'YYYY'))
                ELSE 0
            END as "yearsinpracticesys"  ---should really use date lawyer added to policy
    --    from "ISI_InsuredsView" IV
        ----->>>>>>> should have same number of i_attorneys1 then p_item   USE p_items as startin point
        from "ISI_ItemLawyerView" IV
            left join "Policy" P on P."policyNumber" = IV."POLICYNUMBER"
                left join "Application" A on A."applicationId" = P."policyId"
                    inner join "Quote" Q on Q."applicationId" = A."applicationId" 
                        left join "QuoteCoverage" QC on QC."quoteId" = Q."quoteId" and QC."perClaimLimit" = P."perClaimLimit" and QC."aggregateLimit" = P."aggregateLimit" and QC."perClaimDeductible" = P."perClaimDeductible"
                        left join "RatingLawyerCrosstab" RLC on RLC."quoteBookId" = Q."mainQuoteBookId" and RLC."lawyerId" = IV."LAWYERID"
            left join "Lawyer" L on L."lawyerId" = IV."LAWYERID"
                left join "Person" P on P."personId" = L."lawyerId"   
                left join "LawyerStateBar" LSB on LSB."lawyerId" = L."lawyerId"         
        where IV."LAWYERID" is not NULL
    )
    select
        LEAST("attorneyfactor1", "yearinpracticefactor", "retrodatefactor", "parttimefactor") as "attorneyfactor",
        A1.*
    from A1 A1

    order by "POLICYNUMBER", "transactioneffectivedate", "attorneynameid_ek"
 ;
 
 /*-- Testing --*/
select count(*) fro``m "ISI_Attorney1View";
select count(*) from "ISI_QuoteLimitAttorney1View";
select * from "RatingLawyerCrosstab" where "lawyerId" in ('KIM_JULI'); 
select * from "Lawyer";
select * from "PremiumPerLawyerView" where "firmId" in ('KIMLAV');
select * from "Policy" where "firmId" in ('KIMLAV');
select * from "PolicyLawyerCoverageAllView";
select * from "PolicyLawyerCoverage";
select * from "ISI_InsuredsView";
select * from "ISI_ItemFirmView" where "LAWYERID" is not NULL;
select * from "ISI_PolicyView";
select * from "Application" where "firmId" in ('KIMLAV') order by "effectiveDate";
select * from "RatingCrosstab" where "firmId" in ('KIMLAV') order by "effectiveDate";
select RLC.* from "RatingCrosstab" RC left join "RatingLimitCrosstab" RLC on RLC."quoteBookId" = RC."quoteBookId" where RC."firmId" in ('KIMLAV') order by RLC."quoteBookId", RLC."quoteCoverageId";
select * from "Quote";
select * from "QuoteBook";
select * from "QuoteCoverage";
select * from "RatingLimitCrosstab" order by "quoteBookId", "quoteCoverageId";

--------------------------------------------------------------------------------
/*
    ISI_CoverageView
    p_coverage
    
    Dependancies: ISI_ItemView
    
    NOTES:
    12/27/2017  May need to split this view into 3 views due to performance hit caused by PolicyCoverageLawyerAllView.
                PLus, tail coverage is not yet extracted.
    03/20/2018  annualpremiums and transactionpremium only on transactionseqno=1
    
    History
    12/17/2017  FZ  Created
    04/04/2018  FZ  split int respective coverages
*/
create or replace view "ISI_CoverageLPLFirmView" as 
    --LPL coverage firm
    select
        IV."conversionreference" as "conversionreference",
        IV."transactiontype" as "transactiontype",
        IV."transactioneffectivedate" as "transactioneffectivedate",
        IV."transactionseqno" as "transactionseqno",
        P."policyNumber" as "POLICYNUMBER",
        IV."EVENTTYPEID" as "EVENTTYPEID",
        CASE
            WHEN IV."transactiontype" = 'ERP' AND IV."extendreporttermlength" is NULL THEN 'LPL__FIRM__WLMEU16_WI_19000101_20991231_4112'
            WHEN IV."transactiontype" = 'ERP' AND IV."extendreporttermlength" = 1 THEN 'LPL__FIRM__WLMEC16-1_WI_19000101_20991231_4112'
            WHEN IV."transactiontype" = 'ERP' AND IV."extendreporttermlength" = 2 THEN 'LPL__FIRM__WLMEC16-2_WI_19000101_20991231_4112'
            WHEN IV."transactiontype" = 'ERP' AND IV."extendreporttermlength" = 3 THEN 'LPL__FIRM__WLMEC16-3_WI_19000101_20991231_4112'
            WHEN IV."transactiontype" = 'ERP' AND IV."extendreporttermlength" = 6 THEN 'LPL__FIRM__WLMEC16-6_WI_19000101_20991231_4112'
            WHEN IV."transactiontype" = 'END' AND IV."EVENTTYPEID" = 'Cvg-DF' THEN 'LPL__FIRM__WLMEND20_WI_19000101_20180301_4112'  --DF
            WHEN IV."transactiontype" = 'END' AND IV."EVENTTYPEID" = 'Cvg-GC' THEN 'LPL__FIRM__WLMEND28_WI_19000101_20180301_4112'  --GC
            WHEN IV."transactiontype" = 'END' AND IV."EVENTTYPEID" = 'Cvg-SC' THEN 'LPL__FIRM__WLMEND01_WI_19000101_20991231_4112'  --SEC
            WHEN IV."transactiontype" = 'END' AND IV."EVENTTYPEID" = 'Cvg-EL' THEN 'LPL__FIRM__WLMEND03_WI_19000101_20991231_4112'  --exclude other entities
            WHEN IV."transactiontype" = 'END' AND IV."EVENTTYPEID" = 'Cvg-AT' THEN 'LPL__FIRM__WLMEND24_WI_19000101_20991231_4112'  --abstracter
            WHEN IV."transactiontype" = 'END' AND IV."EVENTTYPEID" = 'Cvg-EV' THEN 'LPL__FIRM__WLMEND25_WI_19000101_20991231_4112'  --exclude vicarious liability
            WHEN IV."transactiontype" = 'END' AND IV."EVENTTYPEID" = 'Cvg-MN' THEN 'LPL__FIRM__WLMENDMN_WI_19000101_20991231_4112'  --manuascript
            WHEN IV."transactiontype" = 'END' AND IV."EVENTTYPEID" = 'Cvg-AB' THEN 'LPL__FIRM__WLMEND30_WI_20180101_20991231_4112'  --additional benefits
            ELSE 'LPL__FIRM__PL_0_WI_19000101_20991231_4112'  --firm
        END AS "pcmmappingkey",
        IV."sbuid" as "sbuid",
        0 as "packageid",
        IV."riskid" as "riskid",
        0 as "linenumber",
        CASE
            WHEN IV."transactiontype" = 'ERP' AND IV."extendreporttermlength" is NULL THEN 'WLMEU16'
            WHEN IV."transactiontype" = 'ERP' AND IV."extendreporttermlength" = 1 THEN 'WLMEC16-1'
            WHEN IV."transactiontype" = 'ERP' AND IV."extendreporttermlength" = 2 THEN 'WLMEC16-2'
            WHEN IV."transactiontype" = 'ERP' AND IV."extendreporttermlength" = 3 THEN 'WLMEC16-3'
            WHEN IV."transactiontype" = 'ERP' AND IV."extendreporttermlength" = 6 THEN 'WLMEC16-6'
            WHEN IV."transactiontype" = 'END' AND IV."EVENTTYPEID" = 'Cvg-DF' THEN 'WLMEND20' --DF
            WHEN IV."transactiontype" = 'END' AND IV."EVENTTYPEID" = 'Cvg-GC' THEN 'WLMEND28'  --GC
            WHEN IV."transactiontype" = 'END' AND IV."EVENTTYPEID" = 'Cvg-SC' THEN 'WLMEND01'  --SEC
            WHEN IV."transactiontype" = 'END' AND IV."EVENTTYPEID" = 'Cvg-EL' THEN 'WLMEND03'  --exclude other entities
            WHEN IV."transactiontype" = 'END' AND IV."EVENTTYPEID" = 'Cvg-AT' THEN 'WLMEND24'  --abstracter
            WHEN IV."transactiontype" = 'END' AND IV."EVENTTYPEID" = 'Cvg-EV' THEN 'WLMEND25'  --exclude vicarious liability
            WHEN IV."transactiontype" = 'END' AND IV."EVENTTYPEID" = 'Cvg-MN' THEN 'WLMENDMN'  --manuascript
            WHEN IV."transactiontype" = 'END' AND IV."EVENTTYPEID" = 'Cvg-AB' THEN 'WLMEND30'  --additional benefits
            ELSE 'PL_0' 
        END as "coverageid",
        CAST(NULL as VARCHAR2(4)) as "basisofsettlement",
        CAST(NULL as VARCHAR2(8)) as "coinsurance",
        CAST(NULL as VARCHAR2(20)) as "rategroup",
        CAST(NULL as VARCHAR2(20)) as "drivingrecord",
        CAST(NULL as NUMBER) as "rateper",
        CAST(NULL as NUMBER) as "baseratefactor",
        CAST(NULL as NUMBER) as "overrideratefactor",
        P."perClaimLimit" as "limit1",
        P."aggregateLimit" as "limit2",
        'USD' as "limitcurrencycd",
        P."perClaimDeductible" as "deductible1",
        0 as "deductible2",
        'USD' as "premiumcurrencycd",
        CAST(NULL as NUMBER) as "ratedbasepremium",
        CAST(NULL as NUMBER) as "ratedgrosspremium",
        CAST(NULL as NUMBER) as "ratednetpremium", 
        /*
        CASE
            WHEN IV."transactionseqno" = 1 THEN COALESCE(PPLV."premiumPerLawyer1M",0) + COALESCE(PPLV."premiumPerLawyerXS5M",0) + COALESCE(PPLV."premiumPerLawyerXS10M",0) 
            ELSE 0
        END as "annualpremium",
        */
        0 as "annualpremium",
        /*
        CASE
            WHEN IV."transactionseqno" = 1 THEN COALESCE(PPLV."premiumPerLawyer1M",0) + COALESCE(PPLV."premiumPerLawyerXS5M",0) + COALESCE(PPLV."premiumPerLawyerXS10M",0) 
            ELSE 0
        END as "transactionpremium",
        */
        0 as "transactionpremium",
        0 as "nonpremiumsurchargeamount",
        0 as "premiumsurchargeamount",
        0 as "transactionsurcharge",
        0 as "nonpremiumdiscountamount",
        0 as "premiumdiscountamount",
        0 as "transactiondiscount",
        0 as "commissionamount",
        CAST(NULL as VARCHAR2(4)) as "numberof1",
        CAST(NULL as VARCHAR2(4)) as "valueof1",
        CAST(NULL as VARCHAR2(4)) as "typeof1",
        CAST(NULL as VARCHAR2(4)) as "numberof2",
        CAST(NULL as VARCHAR2(4)) as "valueof2",
        CAST(NULL as VARCHAR2(4)) as "typeof2",
        CASE 
            WHEN (TO_CHAR(Y."expirationDate", 'YYYYMMDD') <> TO_CHAR(P."expirationDate", 'YYYYMMDD')) and (TO_CHAR(Y."expirationDate", 'YYYYMMDD') <= IV."transactioneffectivedate") THEN 1
            ELSE 0
        END as "deletedrow",
        CAST(NULL as VARCHAR2(4)) as "conversionserver",
        CAST(NULL as VARCHAR2(4)) as "sequencenumber",
        IV."veffectivedate" as "veffectivedate",
        IV."vexpirydate" as "vexpirydate",
        IV."claimsmadeind" as "claimsmadeind",
        COALESCE(IV."retrodate", TO_CHAR(P."effectiveDate", 'YYYYMMDD')) as "retrodate",
        CAST(NULL as NUMBER) as "postedwweobjectid",
        CAST(NULL as VARCHAR2(4)) as "limittierinfo",
        -- only added to transactionseqno=1
        /*
        CASE
            WHEN IV."transactionseqno" = 1 THEN COALESCE(PPLV."premiumPerLawyer1M",0) + COALESCE(PPLV."premiumPerLawyerXS5M",0) + COALESCE(PPLV."premiumPerLawyerXS10M",0) 
            ELSE 0
        END as "ratedftbasepremium",
        */
        0 as "ratedftbasepremium",
        /*
        CASE
            WHEN IV."transactionseqno" = 1 THEN COALESCE(PPLV."premiumPerLawyer1M",0) + COALESCE(PPLV."premiumPerLawyerXS5M",0) + COALESCE(PPLV."premiumPerLawyerXS10M",0) 
            ELSE 0
        END as "ratedftgrosspremium",
        */
        0 as "ratedftgrosspremium",
        /*
        CASE
            WHEN IV."transactionseqno" = 1 THEN COALESCE(PPLV."premiumPerLawyer1M",0) + COALESCE(PPLV."premiumPerLawyerXS5M",0) + COALESCE(PPLV."premiumPerLawyerXS10M",0) 
            ELSE 0
        END as "ratedftpremium",
        */
        0 as "ratedftpremium",
        /*
        CASE
            WHEN IV."transactionseqno" = 1 THEN COALESCE(PPLV."premiumPerLawyer1M",0) + COALESCE(PPLV."premiumPerLawyerXS5M",0) + COALESCE(PPLV."premiumPerLawyerXS10M",0) 
            ELSE 0
        END as "ratedannualpremium",
        */
        0 as "ratedannualpremium",
        /*
        CASE
            WHEN IV."transactionseqno" = 1 THEN COALESCE(PPLV."premiumPerLawyer1M",0) + COALESCE(PPLV."premiumPerLawyerXS5M",0) + COALESCE(PPLV."premiumPerLawyerXS10M",0) 
            ELSE 0
        END as "ratedtransactionpremium",
        */
        0 as "ratedtransactionpremium",
        0 as "ratedcommissionpercent",
        0 as "ratedcommissionamount",
        --COALESCE(PPLV."premiumPerLawyer1M",0) + COALESCE(PPLV."premiumPerLawyerXS5M",0) + COALESCE(PPLV."premiumPerLawyerXS10M",0) as "ftpremium",
        0 as "ftpremium",
        0 as "offsetftpremium",
        1 as "commissiontype",
        0 as "commissionpercent",
        CAST(NULL as NUMBER) as "transactionpremiumfactor",  --04/19/2018 previously 1.00000
        0 as "transactionpremiumdlyrate",
        CAST(NULL as NUMBER) as "overriderule",
        CAST(NULL as NUMBER) as "overrideftpremium",
        CAST(NULL as NUMBER) as "overrideannualpremium",
        CAST(NULL as NUMBER) as "overridetransactionpremium",
        CAST(NULL as NUMBER) as "overridecommissionpercent",
        CAST(NULL as NUMBER) as "overridecommissionamount",
        0 as "taxamount",
        CAST(NULL as VARCHAR2(4)) as "gldiv",
        CAST(NULL as VARCHAR2(4)) as "gldept",
        CAST(NULL as VARCHAR2(4)) as "gllob",
        0 as "siretention1",
        0 as "siretention2",
        CAST(NULL as VARCHAR2(4)) as "mappingcode",
        CAST(NULL as VARCHAR2(4)) as "submappingcode",
        CAST(NULL as VARCHAR2(4)) as "submappingcode2",
        CAST(NULL as NUMBER) as "netrate",
        CAST(NULL as NUMBER) as "grossrate",
        CAST(NULL as VARCHAR2(4)) as "rateexposure",
        0 as "adjbilltypeind",
        CAST(NULL as NUMBER) as "waivedtransactionpremium",
        CAST(NULL as VARCHAR2(4)) as "classcode",
        CAST(NULL as VARCHAR2(4)) as "iso_statecode",
        CAST(NULL as VARCHAR2(4)) as "iso_zipcode",
        CAST(NULL as VARCHAR2(4)) as "iso_ratingterritory",
        CAST(NULL as VARCHAR2(4)) as "iso_typeofpolicycode",
        CAST(NULL as VARCHAR2(4)) as "iso_aslobstatcode",
        CAST(NULL as VARCHAR2(4)) as "iso_sublinecode",
        CAST(NULL as VARCHAR2(4)) as "iso_classificationcode",
        CAST(NULL as VARCHAR2(4)) as "iso_coveragestatcode",
        CAST(NULL as VARCHAR2(4)) as "iso_ratingidentificationcode",
        CAST(NULL as VARCHAR2(4)) as "iso_constructioncode",
        CAST(NULL as VARCHAR2(4)) as "iso_fireprotectioncode",
        CAST(NULL as VARCHAR2(4)) as "iso_terrorismcoveragecode",
        CAST(NULL as VARCHAR2(4)) as "iso_windhaildedcode",
        CAST(NULL as VARCHAR2(4)) as "iso_bcegclass",
        CAST(NULL as VARCHAR2(4)) as "iso_ratingbasiscode",
        CAST(NULL as VARCHAR2(4)) as "iso_liabilityformcode",
        CAST(NULL as VARCHAR2(4)) as "iso_molddamagecode",
        CAST(NULL as VARCHAR2(4)) as "iso_liabilityexposureindcode",
        CAST(NULL as NUMBER) as "iso_exposurestatamount",
        CAST(NULL as VARCHAR2(4)) as "iso_ratingmodificationfactor",
        CAST(NULL as VARCHAR2(4)) as "iso_stateexceptionindcode",
        CAST(NULL as VARCHAR2(4)) as "iso_bussincomeexpensecode",
        CAST(NULL as VARCHAR2(4)) as "iso_liabcovindcode",
        CAST(NULL as VARCHAR2(4)) as "iso_pctowneroccupied",
        CAST(NULL as VARCHAR2(4)) as "iso_classcodedesc",
        TO_CHAR(P."effectiveDate", 'YYYYMMDD') as "rateeffectivedate",
        'FL' as "deductibletype",
        CAST(NULL as VARCHAR2(4)) as "iso_formcode",
        CAST(NULL as VARCHAR2(4)) as "iso_losscostmultiplier",
        CAST(NULL as VARCHAR2(4)) as "iso_losscostdate",
        CAST(NULL as VARCHAR2(4)) as "iso_yearofconstruction",
        TO_CHAR(TO_NUMBER(ORA_HASH(IV."POLICYNUMBER")))||TO_CHAR(TO_NUMBER(ORA_HASH('PL_0'))) as "ouid"  --unique to coverage 
    from (select * from "ISI_ItemFirmView" where "EVENTTYPEID" not in ('Cvg-PA', 'Cvg-AA', 'Cvg-PE')) IV
        left join "PolicyLawyerCoverageAllView" Y on Y."policyNumber" = IV."POLICYNUMBER" and Y."lawyerId" = IV."LAWYERID" and (TO_CHAR(Y."effectiveDate", 'YYYYMMDD') <= IV."transactioneffectivedate")
        left join "Policy" P on P."policyNumber" = IV."POLICYNUMBER"
    order by "POLICYNUMBER", "transactioneffectivedate", "riskid", "coverageid"
;

/*-- NEEDED for PERFORMANCE - Claims --*/
drop table "ISI_CoverageLPLFirm";
--insert into "ISI_CoverageLPLLawyer" (select * from "ISI_CoverageLPLLawyerView");
create table "ISI_CoverageLPLFirm" as
    (select * from "ISI_CoverageLPLFirmView");
select count(*) from "ISI_CoverageLPLFirm";

-- view carry-forwards firm's prior transaction record(s) (ie: 1, 2-1, 3-2-1)
    create or replace view "ISI_CoverageLPLFirmAllView" as
with cov1 as (
select 
    C1.* 
from "ISI_CoverageLPLFirmView" C1
)
    select 
        c2."transactiontype" as "new_transactiontype",
        c2."transactioneffectivedate" as "new_transactioneffectivedate",
        c2."transactionseqno" as "new_transactionseqno",    
        c1.*
    from (select * from cov1 where "transactiontype" not in ('RCA', 'CAN', 'ERP')) c1
        left join (select * from cov1 where "transactiontype" not in ('RCA', 'CAN', 'ERP')) c2 on c2."POLICYNUMBER" = c1."POLICYNUMBER" and c2."transactioneffectivedate" = c1."transactioneffectivedate" and c2."transactionseqno" >= c1."transactionseqno" 
    union all
    select 
        c1."transactiontype" as "new_transactiontype",
        c1."transactioneffectivedate" as "new_transactioneffectivedate",
        c1."transactionseqno" as "new_transactionseqno",    
        c1.*
    from (select * from cov1 where "transactiontype" in ('RCA', 'CAN', 'ERP')) c1
--order by "POLICYNUMBER", "transactioneffectivedate", "transactionseqno"
;

select * from "ISI_CoverageLPLFirmAllView";


select * from "ISI_CoverageLPLLawyerView" where "conversionreference" is NULL or "transactiontype" is NULL or "transactioneffectivedate" is null 
  or "transactionseqno" is null or "POLICYNUMBER" is null or "EVENTTYPEID" is null or "riskid" is null
  or "limit1" is null or "limit2" is null or "deductible1" is null or "veffectivedate" is null or "vexpirydate" is null
  or "claimsmadeind" is null or "postedwweobjectid" is null or "rateeffectivedate" is null;
--
create or replace view "ISI_CoverageLPLLawyerView" as 
    --LPL coverage lawyer
    select
        IV."conversionreference" as "conversionreference",
        IV."transactiontype" as "transactiontype",
        IV."transactioneffectivedate" as "transactioneffectivedate",
        IV."transactionseqno" as "transactionseqno",
        P."policyNumber" as "POLICYNUMBER",
        IV."EVENTTYPEID" as "EVENTTYPEID",
        CASE
            WHEN IV."transactiontype" = 'END' AND IV."EVENTTYPEID" = 'Cvg-DF' THEN 'LPL__ATTORNEY__WLMEND20_WI_19000101_20180301_4112'  --DF
            WHEN IV."transactiontype" = 'END' AND IV."EVENTTYPEID" = 'Cvg-GC' THEN 'LPL__ATTORNEY__WLMEND28_WI_19000101_20180301_4112'  --GC
            --WHEN IV."transactiontype" = 'END' AND IV."EVENTTYPEID" = 'Cvg-SC' THEN 'LPL__ATTORNEY__WLMEND01_WI_19000101_20991231_4112'  --SEC
            WHEN IV."transactiontype" = 'END' AND IV."EVENTTYPEID" = 'Cvg-EL' THEN 'LPL__ATTORNEY__WLMEND03_WI_19000101_20991231_4112'  --exclude other entities
            WHEN IV."transactiontype" = 'END' AND IV."EVENTTYPEID" = 'Cvg-AT' THEN 'LPL__ATTORNEY__WLMEND24_WI_19000101_20991231_4112'  --abstracter
            WHEN IV."transactiontype" = 'END' AND IV."EVENTTYPEID" = 'Cvg-EV' THEN 'LPL__ATTORNEY__WLMEND25_WI_19000101_20991231_4112'  --exclude vicarious liability
            WHEN IV."transactiontype" = 'END' AND IV."EVENTTYPEID" = 'Cvg-MN' THEN 'LPL__ATTORNEY__WLMENDMN_WI_19000101_20991231_4112'  --manuascript
            WHEN IV."transactiontype" = 'END' AND IV."EVENTTYPEID" = 'Cvg-AB' THEN 'LPL__ATTORNEY__WLMEND30_WI_20180101_20991231_4112'  --additional benefits
            ELSE 'LPL__ATTORNEY__PL_0_WI_19000101_20991231_4112' --attorney
        END AS "pcmmappingkey",
        IV."sbuid" as "sbuid",
        0 as "packageid",
        IV."riskid" as "riskid",
        0 as "linenumber",
        CASE
            WHEN IV."transactiontype" = 'END' AND IV."EVENTTYPEID" = 'Cvg-DF' THEN 'WLMEND20'  --DF
            WHEN IV."transactiontype" = 'END' AND IV."EVENTTYPEID" = 'Cvg-GC' THEN 'WLMEND28'  --GC
            --WHEN IV."transactiontype" = 'END' AND IV."EVENTTYPEID" = 'Cvg-SC' THEN 'WLMEND01'  --SEC
            WHEN IV."transactiontype" = 'END' AND IV."EVENTTYPEID" = 'Cvg-EL' THEN 'WLMEND03'  --exclude other entities
            WHEN IV."transactiontype" = 'END' AND IV."EVENTTYPEID" = 'Cvg-AT' THEN 'WLMEND24'  --abstracter
            WHEN IV."transactiontype" = 'END' AND IV."EVENTTYPEID" = 'Cvg-EV' THEN 'WLMEND25'  --exclude vicarious liability
            WHEN IV."transactiontype" = 'END' AND IV."EVENTTYPEID" = 'Cvg-MN' THEN 'WLMENDMN'  --manuascript
            WHEN IV."transactiontype" = 'END' AND IV."EVENTTYPEID" = 'Cvg-AB' THEN 'WLMEND30'  --additional benefits
            ELSE 'PL_0' 
        END as "coverageid",
        CAST(NULL as VARCHAR2(4)) as "basisofsettlement",
        CAST(NULL as VARCHAR2(8)) as "coinsurance",
        CAST(NULL as VARCHAR2(20)) as "rategroup",
        CAST(NULL as VARCHAR2(20)) as "drivingrecord",
        CAST(NULL as NUMBER) as "rateper",
        CAST(NULL as NUMBER) as "baseratefactor",
        CAST(NULL as NUMBER) as "overrideratefactor",
        P."perClaimLimit" as "limit1",
        P."aggregateLimit" as "limit2",
        'USD' as "limitcurrencycd",
        P."perClaimDeductible" as "deductible1",
        0 as "deductible2",
        'USD' as "premiumcurrencycd",
        CAST(NULL as NUMBER) as "ratedbasepremium",
        CAST(NULL as NUMBER) as "ratedgrosspremium",
        CAST(NULL as NUMBER) as "ratednetpremium", 
        CASE
            -- remove DF and prorate based on 
            WHEN IV."transactiontype" in ('NEW', 'REN', 'RWN', 'RWR') THEN (COALESCE(PPLV."premiumPerLawyer1M",0) + COALESCE(PPLV."premiumPerLawyerXS5M",0) + COALESCE(PPLV."premiumPerLawyerXS10M",0) - COALESCE(PPLV."DefReimbCoverage",0) * 12/COALESCE(IV."POLICYTERM",12))
            WHEN IV."transactiontype" = 'END' and IV."EVENTTYPEID" = 'Cvg-DF' THEN COALESCE(PPLV."DefReimbCoverage",0)
            --WHEN IV."transactiontype" = 'END' and IV."EVENTTYPEID" = 'Cvg-SC' THEN COALESCE(PPLV."SECCoverage", 0)
            ELSE COALESCE(PA."premiumAdjust", 0)
        END as "annualpremium",
        CASE
            WHEN IV."transactiontype" in ('NEW', 'REN', 'RWN', 'RWR') THEN COALESCE(PPLV."premiumPerLawyer1M",0) + COALESCE(PPLV."premiumPerLawyerXS5M",0) + COALESCE(PPLV."premiumPerLawyerXS10M",0) - COALESCE(PPLV."DefReimbCoverage",0)
            WHEN IV."transactiontype" = 'END' and IV."EVENTTYPEID" = 'Cvg-DF' THEN COALESCE(PPLV."DefReimbCoverage",0)
            --WHEN IV."transactiontype" = 'END' and IV."EVENTTYPEID" = 'Cvg-SC' THEN COALESCE(PPLV."SECCoverage", 0)
            ELSE COALESCE(PA."premiumAdjust", 0)
        END as "transactionpremium",
        0 as "nonpremiumsurchargeamount",
        0 as "premiumsurchargeamount",
        0 as "transactionsurcharge",
        0 as "nonpremiumdiscountamount",
        0 as "premiumdiscountamount",
        0 as "transactiondiscount",
        0 as "commissionamount",
        CAST(NULL as VARCHAR2(4)) as "numberof1",
        CAST(NULL as VARCHAR2(4)) as "valueof1",
        CAST(NULL as VARCHAR2(4)) as "typeof1",
        CAST(NULL as VARCHAR2(4)) as "numberof2",
        CAST(NULL as VARCHAR2(4)) as "valueof2",
        CAST(NULL as VARCHAR2(4)) as "typeof2",
        CASE 
            WHEN (TO_CHAR(Y."expirationDate", 'YYYYMMDD') <> TO_CHAR(P."expirationDate", 'YYYYMMDD')) and (TO_CHAR(Y."expirationDate", 'YYYYMMDD') <= IV."transactioneffectivedate") THEN 1
            ELSE 0
        END as "deletedrow",
        CAST(NULL as VARCHAR2(4)) as "conversionserver",
        CAST(NULL as VARCHAR2(4)) as "sequencenumber",
        IV."veffectivedate" as "veffectivedate",
        IV."vexpirydate" as "vexpirydate",
        IV."claimsmadeind" as "claimsmadeind",
        COALESCE(IV."retrodate", TO_CHAR(P."effectiveDate", 'YYYYMMDD')) as "retrodate",
        CAST(NULL as NUMBER) as "postedwweobjectid",
        CAST(NULL as VARCHAR2(4)) as "limittierinfo",
        -- only added to transactionseqno=1
        CASE
            WHEN IV."transactiontype" in ('NEW', 'REN', 'RWN', 'RWR') THEN COALESCE(PPLV."premiumPerLawyer1M",0) + COALESCE(PPLV."premiumPerLawyerXS5M",0) + COALESCE(PPLV."premiumPerLawyerXS10M",0) - COALESCE(PPLV."DefReimbCoverage", 0) 
            WHEN IV."transactiontype" = 'END' and IV."EVENTTYPEID" = 'Cvg-DF' THEN COALESCE(PPLV."DefReimbCoverage",0)
            --WHEN IV."transactiontype" = 'END' and IV."EVENTTYPEID" = 'Cvg-SC' THEN COALESCE(PPLV."SECCoverage", 0)
            ELSE COALESCE(PA."premiumAdjust", 0)
        END as "ratedftbasepremium",
        CASE
            WHEN IV."transactiontype" in ('NEW', 'REN', 'RWN', 'RWR') THEN COALESCE(PPLV."premiumPerLawyer1M",0) + COALESCE(PPLV."premiumPerLawyerXS5M",0) + COALESCE(PPLV."premiumPerLawyerXS10M",0) - COALESCE(PPLV."DefReimbCoverage", 0) 
            WHEN IV."transactiontype" = 'END' and IV."EVENTTYPEID" = 'Cvg-DF' THEN COALESCE(PPLV."DefReimbCoverage",0)
            --WHEN IV."transactiontype" = 'END' and IV."EVENTTYPEID" = 'Cvg-SC' THEN COALESCE(PPLV."SECCoverage", 0)
            ELSE COALESCE(PA."premiumAdjust", 0)
        END as "ratedftgrosspremium",
        CASE
            WHEN IV."transactiontype" in ('NEW', 'REN', 'RWN', 'RWR') THEN COALESCE(PPLV."premiumPerLawyer1M",0) + COALESCE(PPLV."premiumPerLawyerXS5M",0) + COALESCE(PPLV."premiumPerLawyerXS10M",0) - COALESCE(PPLV."DefReimbCoverage", 0) 
            WHEN IV."transactiontype" = 'END' and IV."EVENTTYPEID" = 'Cvg-DF' THEN COALESCE(PPLV."DefReimbCoverage",0)
            --WHEN IV."transactiontype" = 'END' and IV."EVENTTYPEID" = 'Cvg-SC' THEN COALESCE(PPLV."SECCoverage", 0)
            ELSE COALESCE(PA."premiumAdjust", 0)
        END as "ratedftpremium",
        CASE
            WHEN IV."transactiontype" in ('NEW', 'REN', 'RWN', 'RWR') THEN COALESCE(PPLV."premiumPerLawyer1M",0) + COALESCE(PPLV."premiumPerLawyerXS5M",0) + COALESCE(PPLV."premiumPerLawyerXS10M",0) - COALESCE(PPLV."DefReimbCoverage", 0) 
            WHEN IV."transactiontype" = 'END' and IV."EVENTTYPEID" = 'Cvg-DF' THEN COALESCE(PPLV."DefReimbCoverage",0)
            --WHEN IV."transactiontype" = 'END' and IV."EVENTTYPEID" = 'Cvg-SC' THEN COALESCE(PPLV."SECCoverage", 0)
            ELSE COALESCE(PA."premiumAdjust", 0)
        END as "ratedannualpremium",
        CASE
            WHEN IV."transactiontype" in ('NEW', 'REN', 'RWN', 'RWR')  THEN COALESCE(PPLV."premiumPerLawyer1M",0) + COALESCE(PPLV."premiumPerLawyerXS5M",0) + COALESCE(PPLV."premiumPerLawyerXS10M",0) 
            WHEN IV."transactiontype" = 'END' and IV."EVENTTYPEID" = 'Cvg-DF' THEN COALESCE(PPLV."DefReimbCoverage",0)
            --WHEN IV."transactiontype" = 'END' and IV."EVENTTYPEID" = 'Cvg-SC' THEN COALESCE(PPLV."SECCoverage", 0)
            ELSE COALESCE(PA."premiumAdjust", 0)
        END as "ratedtransactionpremium",
        0 as "ratedcommissionpercent",
        0 as "ratedcommissionamount",
        CASE
            WHEN IV."transactiontype" = 'END' and IV."EVENTTYPEID" = 'Cvg-DF' THEN COALESCE(PPLV."DefReimbCoverage",0)
            --WHEN IV."transactiontype" = 'END' and IV."EVENTTYPEID" = 'Cvg-SC' THEN COALESCE(PPLV."SECCoverage", 0)
            ELSE COALESCE(PA."premiumAdjust", 0)
        END as "ftpremium",
        0 as "offsetftpremium",
        1 as "commissiontype",
        0 as "commissionpercent",
        1.0000000 as "transactionpremiumfactor",
        0 as "transactionpremiumdlyrate",
        CAST(NULL as NUMBER) as "overriderule",
        CAST(NULL as NUMBER) as "overrideftpremium",
        CAST(NULL as NUMBER) as "overrideannualpremium",
        CAST(NULL as NUMBER) as "overridetransactionpremium",
        CAST(NULL as NUMBER) as "overridecommissionpercent",
        CAST(NULL as NUMBER) as "overridecommissionamount",
        0 as "taxamount",
        CAST(NULL as VARCHAR2(4)) as "gldiv",
        CAST(NULL as VARCHAR2(4)) as "gldept",
        CAST(NULL as VARCHAR2(4)) as "gllob",
        0 as "siretention1",
        0 as "siretention2",
        CAST(NULL as VARCHAR2(4)) as "mappingcode",
        CAST(NULL as VARCHAR2(4)) as "submappingcode",
        CAST(NULL as VARCHAR2(4)) as "submappingcode2",
        CAST(NULL as NUMBER) as "netrate",
        CAST(NULL as NUMBER) as "grossrate",
        CAST(NULL as VARCHAR2(4)) as "rateexposure",
        0 as "adjbilltypeind",
        CAST(NULL as NUMBER) as "waivedtransactionpremium",
        CAST(NULL as VARCHAR2(4)) as "classcode",
        CAST(NULL as VARCHAR2(4)) as "iso_statecode",
        CAST(NULL as VARCHAR2(4)) as "iso_zipcode",
        CAST(NULL as VARCHAR2(4)) as "iso_ratingterritory",
        CAST(NULL as VARCHAR2(4)) as "iso_typeofpolicycode",
        CAST(NULL as VARCHAR2(4)) as "iso_aslobstatcode",
        CAST(NULL as VARCHAR2(4)) as "iso_sublinecode",
        CAST(NULL as VARCHAR2(4)) as "iso_classificationcode",
        CAST(NULL as VARCHAR2(4)) as "iso_coveragestatcode",
        CAST(NULL as VARCHAR2(4)) as "iso_ratingidentificationcode",
        CAST(NULL as VARCHAR2(4)) as "iso_constructioncode",
        CAST(NULL as VARCHAR2(4)) as "iso_fireprotectioncode",
        CAST(NULL as VARCHAR2(4)) as "iso_terrorismcoveragecode",
        CAST(NULL as VARCHAR2(4)) as "iso_windhaildedcode",
        CAST(NULL as VARCHAR2(4)) as "iso_bcegclass",
        CAST(NULL as VARCHAR2(4)) as "iso_ratingbasiscode",
        CAST(NULL as VARCHAR2(4)) as "iso_liabilityformcode",
        CAST(NULL as VARCHAR2(4)) as "iso_molddamagecode",
        CAST(NULL as VARCHAR2(4)) as "iso_liabilityexposureindcode",
        CAST(NULL as NUMBER) as "iso_exposurestatamount",
        CAST(NULL as VARCHAR2(4)) as "iso_ratingmodificationfactor",
        CAST(NULL as VARCHAR2(4)) as "iso_stateexceptionindcode",
        CAST(NULL as VARCHAR2(4)) as "iso_bussincomeexpensecode",
        CAST(NULL as VARCHAR2(4)) as "iso_liabcovindcode",
        CAST(NULL as VARCHAR2(4)) as "iso_pctowneroccupied",
        CAST(NULL as VARCHAR2(4)) as "iso_classcodedesc",
        TO_CHAR(P."effectiveDate", 'YYYYMMDD') as "rateeffectivedate",
        'FL' as "deductibletype",
        CAST(NULL as VARCHAR2(4)) as "iso_formcode",
        CAST(NULL as VARCHAR2(4)) as "iso_losscostmultiplier",
        CAST(NULL as VARCHAR2(4)) as "iso_losscostdate",
        CAST(NULL as VARCHAR2(4)) as "iso_yearofconstruction",
        TO_CHAR(TO_NUMBER(ORA_HASH(IV."POLICYNUMBER")))||TO_CHAR(TO_NUMBER(ORA_HASH('PL_0'))) as "ouid"  --unique to coverage 
    from (select * from "ISI_ItemLawyerView" where "transactiontype" <> 'ERP') IV
        left join "PolicyLawyerCoverageAllView" Y on Y."policyNumber" = IV."POLICYNUMBER" and Y."lawyerId" = IV."LAWYERID" and (TO_CHAR(Y."effectiveDate", 'YYYYMMDD') <= IV."transactioneffectivedate")
        left join "Policy" P on P."policyNumber" = IV."POLICYNUMBER"
            left join "PremiumPerLawyerView" PPLV on PPLV."policyId" = P."policyId" and PPLV."effectiveDate" = P."effectiveDate" and PPLV."lawyerId" = IV."LAWYERID" and PPLV."aggregateLimit" = P."aggregateLimit" and PPLV."Limit" = P."perClaimLimit" and PPLV."Deductible" = P."perClaimDeductible"
        left join "PolicyAccounting" PA on PA."coverageId" = IV."COVERAGEID" and IV."transactiontype" in ('END', 'CAN', 'RCA')
    --- FIX
    where IV."EVENTTYPEID" = 'Coverage' and IV."POLICYNUMBER" = '20090701' and IV."transactioneffectivedate" = '20090701' and IV."transactionseqno" = 1
    order by "POLICYNUMBER", "transactioneffectivedate", "riskid", "coverageid"
;
select count(*) from "ISI_CoverageLPLLawyerView";
select * from "ISI_CoverageLPLLawyerView";


/* 
-- MUST DO for PERFORMANCE
*/
drop table "ISI_CoverageLPLLawyer";
--insert into "ISI_CoverageLPLLawyer" (select * from "ISI_CoverageLPLLawyerView");
create table "ISI_CoverageLPLLawyer" as
    (select * from "ISI_CoverageLPLLawyerView");
select count(*) from "ISI_CoverageLPLLawyer";

-- Fix ISI_CoverageLplLawyer table
--Cvg-PA after initial REN/NEW
delete from "ISI_CoverageLPLLawyer" c2 where exists (
select c1.* from "ISI_CoverageLPLLawyer" c1
where c1."EVENTTYPEID" = 'Cvg-PA'
  and c2."POLICYNUMBER" = c1."POLICYNUMBER" 
  and c2."riskid" = c1."riskid"
  and c2."transactioneffectivedate" = c1."transactioneffectivedate" 
  and c2."transactionseqno" < c1."transactionseqno"
);
--Cvg-AA after initial REN/NEW
delete from "ISI_CoverageLPLLawyer" c2 where exists (
select c1.* from "ISI_CoverageLPLLawyer" c1
where c1."EVENTTYPEID" = 'Cvg-AA'
  and c2."POLICYNUMBER" = c1."POLICYNUMBER" 
  and c2."riskid" = c1."riskid"
  and c2."transactioneffectivedate" = c1."transactioneffectivedate" 
  and c2."transactionseqno" < c1."transactionseqno"
);
--HAUSMC bogus coverage
delete from "ISI_CoverageLPLLawyer" IV where IV."EVENTTYPEID" = 'Coverage' and IV."POLICYNUMBER" = '0907019' and IV."transactioneffectivedate" = '20090701' and IV."transactionseqno" = 1;
--BOYKTH bogus Cvg-PE
delete from "ISI_CoverageLPLLawyer" IV where IV."POLICYNUMBER" = '1007027' and IV."transactioneffectivedate" = '20100702' and IV."EVENTTYPEID" = 'Cvg-PE';
--------------------------------------------------------------------------------

-- view carry-forwards firm's prior transaction record(s) (ie: 1, 2-1, 3-2-1)
create or replace view "ISI_CoverageLPLLawyerAllView" as
with cov1 as (
    select 
        C1.* 
    from "ISI_CoverageLPLLawyer" C1
    where C1."EVENTTYPEID" not in ('Cvg-PE') --there is not a Cvg-PE (WLMEND26) ISI equivalent
)
    select 
        c2."transactiontype" as "new_transactiontype",
        c2."transactioneffectivedate" as "new_transactioneffectivedate",
        c2."transactionseqno" as "new_transactionseqno", 
        CASE
            WHEN c2."transactionseqno" <> c1."transactionseqno" THEN 0
            ELSE c1."transactionpremium"
        END as "new_transactionpremium",
        c1.*
    from (select * from cov1 where "transactiontype" not in ('RCA', 'CAN', 'ERP')) c1
        left join (select * from cov1 where "transactiontype" not in ('RCA', 'CAN', 'ERP')) c2 on c2."POLICYNUMBER" = c1."POLICYNUMBER" and c2."transactioneffectivedate" = c1."transactioneffectivedate" and c2."riskid" = c1."riskid" and c2."transactionseqno" >= c1."transactionseqno"
    union all
    select 
        c1."transactiontype" as "new_transactiontype",
        c1."transactioneffectivedate" as "new_transactioneffectivedate",
        c1."transactionseqno" as "new_transactionseqno", 
        c1."transactionseqno" as "new_transactionpremium",
        c1.*
    from (select * from cov1 where "transactiontype" in ('RCA', 'CAN', 'ERP')) c1 
    --order by c2."POLICYNUMBER", c2."transactioneffectivedate", c2."transactionseqno", c2."riskid"
;

select count(*) from "ISI_CoverageLPLLawyerView";
select * from "ISI_CoverageLPLLawyerView" where "POLICYNUMBER" = '1506090';
select * from "ISI_CoverageLPLLawyerView" order by "POLICYNUMBER", "transactioneffectivedate", "transactionseqno", "riskid";
select * from "PolicyLawyerCoverageAllView";
select * from "Policy" where "policyNumber" = '1506090';
select * from "PremiumPerLawyerView" where "policyId" = 'AP000720722';
select * from "Endorsement";
select * from "PolicyLawyerCoverage";



/*--- Table ---*/
drop table "TailLawyer2";
--delete * from "ISI_Policy";
create table "TailLawyer2" as
    (select * from "TailLawyerView2");
select * from "TailLawyer2";   
select count(*) from "TailLawyer2";  
 
--- 
create or replace view "ISI_CoverageERPLawyerView" as 
    --ERP coverage lawyer
    select
        IV."conversionreference" as "conversionreference",
        IV."transactiontype" as "transactiontype",
        IV."transactioneffectivedate" as "transactioneffectivedate",
        IV."transactionseqno" as "transactionseqno",
        P."policyNumber" as "POLICYNUMBER",
        IV."EVENTTYPEID" as "EVENTTYPEID",
        IV."extendreporttermlength" AS "TAILLENGTH",
        CASE
            WHEN IV."transactiontype" = 'ERP' AND IV."EVENTTYPEID" = 'Cvg-EU' and IV."extendreporttermlength" is NULL THEN 'LPL__ATTORNEY__WLMEU16_WI_19000101_20991231_4112'
            WHEN IV."transactiontype" = 'ERP' AND IV."EVENTTYPEID" = 'Cvg-EC' and TLV."tailTerm" = '1' THEN 'LPL__ATTORNEY__WLMEC16-1_WI_19000101_20991231_4112'
            WHEN IV."transactiontype" = 'ERP' AND IV."EVENTTYPEID" = 'Cvg-EC' and TLV."tailTerm" = '2' THEN 'LPL__ATTORNEY__WLMEC16-2_WI_19000101_20991231_4112'
            WHEN IV."transactiontype" = 'ERP' AND IV."EVENTTYPEID" = 'Cvg-EC' and TLV."tailTerm" = '3' THEN 'LPL__ATTORNEY__WLMEC16-3_WI_19000101_20991231_4112'
            WHEN IV."transactiontype" = 'ERP' AND IV."EVENTTYPEID" = 'Cvg-EC' and TLV."tailTerm" = '6' THEN 'LPL__ATTORNEY__WLMEC16-6_WI_19000101_20991231_4112'
            WHEN IV."transactiontype" = 'ERP' AND IV."EVENTTYPEID" = 'Cvg-ED' and IV."extendreporttermlength" is NULL THEN 'LPL__ATTORNEY__WLMED16_WI_19000101_20991231_4112'
            WHEN IV."transactiontype" = 'ERP' AND IV."EVENTTYPEID" = 'Cvg-NU' and TLV."tailTerm" is NULL THEN 'LPL__ATTORNEY__WLMNU17_WI_19000101_20991231_4112'
            WHEN IV."transactiontype" = 'ERP' AND IV."EVENTTYPEID" = 'Cvg-NP' and TLV."tailTerm" = '1' THEN 'LPL__ATTORNEY__WLMNP17-1_WI_19000101_20991231_4112'
            WHEN IV."transactiontype" = 'ERP' AND IV."EVENTTYPEID" = 'Cvg-NP' and TLV."tailTerm" = '2' THEN 'LPL__ATTORNEY__WLMNP17-2_WI_19000101_20991231_4112'
            WHEN IV."transactiontype" = 'ERP' AND IV."EVENTTYPEID" = 'Cvg-NP' and TLV."tailTerm" = '3' THEN 'LPL__ATTORNEY__WLMNP17-3_WI_19000101_20991231_4112'
            WHEN IV."transactiontype" = 'ERP' AND IV."EVENTTYPEID" = 'Cvg-NP' and TLV."tailTerm" = '6' THEN 'LPL__ATTORNEY__WLMNP17-6_WI_19000101_20991231_4112'
            WHEN IV."transactiontype" = 'ERP' and IV."EVENTTYPEID" = 'Cvg-ND' THEN 'LPL__ATTORNEY__WLMND17_WI_19000101_20991231_4112'
            WHEN IV."transactiontype" = 'ERP' and IV."EVENTTYPEID" = 'Cvg-CP' and TLV."tailTerm" = '3' THEN 'LPL__ATTORNEY__WLMEND29-3_WI_19000101_20991231_4112'
            WHEN IV."transactiontype" = 'ERP' and IV."EVENTTYPEID" = 'Cvg-CP' and TLV."tailTerm" = '6' THEN 'LPL__ATTORNEY__WLMEND29-6_WI_19000101_20991231_4112'
            --ELSE 'DONT KNOW TAIL ENDORSEMENT'
            ELSE 'LPL__ATTORNEY__WLMNU17_WI_19000101_20991231_4112'
        END AS "pcmmappingkey",
        IV."sbuid" as "sbuid",
        0 as "packageid",
        IV."riskid" as "riskid",
        0 as "linenumber",
        CASE
            WHEN IV."transactiontype" = 'ERP' AND IV."EVENTTYPEID" = 'Cvg-EU' and IV."extendreporttermlength" is NULL THEN 'WLMNU16'
            WHEN IV."transactiontype" = 'ERP' AND IV."EVENTTYPEID" = 'Cvg-EC' and TLV."tailTerm" = '1' THEN 'WLMNP16-1'
            WHEN IV."transactiontype" = 'ERP' AND IV."EVENTTYPEID" = 'Cvg-EC' and TLV."tailTerm" = '2' THEN 'WLMNP16-2'
            WHEN IV."transactiontype" = 'ERP' AND IV."EVENTTYPEID" = 'Cvg-EC' and TLV."tailTerm" = '3' THEN 'WLMNP16-3'
            WHEN IV."transactiontype" = 'ERP' AND IV."EVENTTYPEID" = 'Cvg-EC' and TLV."tailTerm" = '6' THEN 'WLMNP16-6'
            WHEN IV."transactiontype" = 'ERP' AND IV."EVENTTYPEID" = 'Cvg-ED' and IV."extendreporttermlength" is NULL THEN 'WLMED16'
            WHEN IV."transactiontype" = 'ERP' AND IV."EVENTTYPEID" = 'Cvg-NU' and TLV."tailTerm" is NULL THEN 'WLMNU17'
            WHEN IV."transactiontype" = 'ERP' AND IV."EVENTTYPEID" = 'Cvg-NP' and TLV."tailTerm" = '1' THEN 'WLMNP17-1'
            WHEN IV."transactiontype" = 'ERP' AND IV."EVENTTYPEID" = 'Cvg-NP' and TLV."tailTerm" = '2' THEN 'WLMNP17-2'
            WHEN IV."transactiontype" = 'ERP' AND IV."EVENTTYPEID" = 'Cvg-NP' and TLV."tailTerm" = '3' THEN 'WLMNP17-3'
            WHEN IV."transactiontype" = 'ERP' AND IV."EVENTTYPEID" = 'Cvg-NP' and TLV."tailTerm" = '6' THEN 'WLMNP17-6'
            WHEN IV."transactiontype" = 'ERP' and IV."EVENTTYPEID" = 'Cvg-ND' THEN 'WLMND17'
            WHEN IV."transactiontype" = 'ERP' and IV."EVENTTYPEID" = 'Cvg-CP' and TLV."tailTerm" = '3' THEN 'WLMEND29-3'
            WHEN IV."transactiontype" = 'ERP' and IV."EVENTTYPEID" = 'Cvg-CP' and TLV."tailTerm" = '6' THEN 'WLMEND29-6'
            --ELSE '?????????'
            ELSE 'WLMNU17'
        END as "coverageid",
        NULL as "basisofsettlement",
        NULL as "coinsurance",
        NULL as "rategroup",
        NULL as "drivingrecord",
        NULL as "rateper",
        NULL as "baseratefactor",
        NULL as "overrideratefactor",
        P."perClaimLimit" as "limit1",
        P."aggregateLimit" as "limit2",
        'USD' as "limitcurrencycd",
        P."perClaimDeductible" as "deductible1",
        0 as "deductible2",
        'USD' as "premiumcurrencycd",
        NULL as "ratedbasepremium",
        NULL as "ratedgrosspremium",
        NULL as "ratednetpremium",
        CASE
            -- remove DF and prorate based on 
            WHEN IV."transactiontype" in ('ERP') THEN COALESCE(TLV."oldTailPremium", 0) ---currently always full term premium
            ELSE 0
        END as "annualpremium",
        CASE
            WHEN IV."transactiontype" in ('ERP') THEN COALESCE(TLV."oldTailPremium", 0) 
            ELSE 0
        END as "transactionpremium",
        0 as "nonpremiumsurchargeamount",
        0 as "premiumsurchargeamount",
        0 as "transactionsurcharge",
        0 as "nonpremiumdiscountamount",
        0 as "premiumdiscountamount",
        0 as "transactiondiscount",
        0 as "commissionamount",
        NULL as "numberof1",
        NULL as "valueof1",
        NULL as "typeof1",
        NULL as "numberof2",
        NULL as "valueof2",
        NULL as "typeof2",
        CASE 
            WHEN (TO_CHAR(Y."expirationDate", 'YYYYMMDD') <> TO_CHAR(P."expirationDate", 'YYYYMMDD')) and (TO_CHAR(Y."expirationDate", 'YYYYMMDD') <= IV."transactioneffectivedate") THEN 1
            ELSE 0
        END as "deletedrow",
        ---IV."deletedrow" as "deletedrow",
        NULL as "conversionserver",
        NULL as "sequencenumber",
        IV."veffectivedate" as "veffectivedate",
        IV."vexpirydate" as "vexpirydate",
        IV."claimsmadeind" as "claimsmadeind",
        IV."retrodate" as "retrodate",
        IV."postedwweobjectid" as "postedwweobjectid",
        NULL as "limittierinfo",
        -- only added to transactionseqno=1
        CASE
            WHEN IV."transactiontype" in ('ERP') THEN COALESCE(TLV."oldTailPremium", 0) 
            ELSE 0
        END as "ratedftbasepremium",
        CASE
            WHEN IV."transactiontype" in ('ERP') THEN COALESCE(TLV."oldTailPremium", 0) 
            ELSE 0
        END as "ratedftgrosspremium",
        CASE
            WHEN IV."transactiontype" in ('ERP') THEN COALESCE(TLV."oldTailPremium", 0) 
            ELSE 0
        END as "ratedftpremium",
        CASE
            WHEN IV."transactiontype" in ('ERP') THEN COALESCE(TLV."oldTailPremium", 0) 
            ELSE 0
        END as "ratedannualpremium",
        CASE
            WHEN IV."transactiontype" in ('ERP') THEN COALESCE(TLV."oldTailPremium", 0) 
            ELSE 0
        END as "ratedtransactionpremium",
        0 as "ratedcommissionpercent",
        0 as "ratedcommissionamount",
        CASE
            WHEN IV."transactiontype" in ('ERP') THEN COALESCE(TLV."oldTailPremium", 0) 
            ELSE 0
        END as "ftpremium",
        0 as "offsetftpremium",
        1 as "commissiontype",
        0 as "commissionpercent",
        1.0000000 as "transactionpremiumfactor",
        0 as "transactionpremiumdlyrate",
        NULL as "overriderule",
        NULL as "overrideftpremium",
        NULL as "overrideannualpremium",
        NULL as "overridetransactionpremium",
        NULL as "overridecommissionpercent",
        NULL as "overridecommissionamount",
        0 as "taxamount",
        NULL as "gldiv",
        NULL as "gldept",
        NULL as "gllob",
        0 as "siretention1",
        0 as "siretention2",
        NULL as "mappingcode",
        NULL as "submappingcode",
        NULL as "submappingcode2",
        NULL as "netrate",
        NULL as "grossrate",
        NULL as "rateexposure",
        0 as "adjbilltypeind",
        NULL as "waivedtransactionpremium",
        NULL as "classcode",
        NULL as "iso_statecode",
        NULL as "iso_zipcode",
        NULL as "iso_ratingterritory",
        NULL as "iso_typeofpolicycode",
        NULL as "iso_aslobstatcode",
        NULL as "iso_sublinecode",
        NULL as "iso_classificationcode",
        NULL as "iso_coveragestatcode",
        NULL as "iso_ratingidentificationcode",
        NULL as "iso_constructioncode",
        NULL as "iso_fireprotectioncode",
        NULL as "iso_terrorismcoveragecode",
        NULL as "iso_windhaildedcode",
        NULL as "iso_bcegclass",
        NULL as "iso_ratingbasiscode",
        NULL as "iso_liabilityformcode",
        NULL as "iso_molddamagecode",
        NULL as "iso_liabilityexposureindcode",
        NULL as "iso_exposurestatamount",
        NULL as "iso_ratingmodificationfactor",
        NULL as "iso_stateexceptionindcode",
        NULL as "iso_bussincomeexpensecode",
        NULL as "iso_liabcovindcode",
        NULL as "iso_pctowneroccupied",
        NULL as "iso_classcodedesc",
        TO_CHAR(P."effectiveDate", 'YYYYMMDD') as "rateeffectivedate",
        'FL' as "deductibletype",
        NULL as "iso_formcode",
        NULL as "iso_losscostmultiplier",
        NULL as "iso_losscostdate",
        NULL as "iso_yearofconstruction",
        TO_CHAR(TO_NUMBER(ORA_HASH(IV."POLICYNUMBER")))||TO_CHAR(TO_NUMBER(ORA_HASH('PL_0'))) as "ouid"  --unique to coverage 
    from (select * from "ISI_ItemLawyerView" where "transactiontype" = 'ERP') IV
        left join "PolicyLawyerCoverageAllView" Y on Y."policyNumber" = IV."POLICYNUMBER" and Y."lawyerId" = IV."LAWYERID" and (TO_CHAR(Y."effectiveDate", 'YYYYMMDD') <= IV."transactioneffectivedate")
        left join "Policy" P on P."policyNumber" = IV."POLICYNUMBER"
        left join "TailLawyer2" TLV on TLV."coverageId" = IV."COVERAGEID" and TLV."lawyerId" = IV."LAWYERID" 
        /* per policy accounting --
        left join "PolicyLawyerCoverage" PLC on PLC."coverageId" = IV."COVERAGEID" and PLC."lawyerId" = IV."LAWYERID"
            left join "PolicyAccounting" PA on PA."coverageId" = PLC."coverageId"
        */
    order by "POLICYNUMBER", "transactioneffectivedate", "riskid"
;

/*---Testing ---*/
select * from "ISI_ItemView";
select * from "ISI_InsuredsView";
select * from "ISI_CoverageDRView";
select * from "PremiumPerLawyerView" where "DefReimbCoverage" is not NULL;
select E.*, P.* from "Policy" P right join (select "policyId" from "Endorsement" where substr("endorsementDesc",1,8) = 'WLMEND20') E on E."policyId" = P."policyId";
--------------------------------------------------------------------------------
/*
    ISI_InlineSchedulesView
    p_inlineschedules
    
    Dependancies: ISI_ItemView
    
    History
    12/18/2017  FZ  Created
    01/20/2018  FZ  fixed duplicate AOP linenumber
*/
create or replace view "ISI_InlineSchedulesFirmAOPView" as 
--firm level area of practice
    select
        IV."POLICYNUMBER" as "POLICYNUMBER",
        IV."conversionreference" as "conversionreference",
        IV."transactiontype" as "transactiontype",
        IV."transactioneffectivedate" as "transactioneffectivedate",
        IV."transactionseqno" as "transactionseqno",
        IV."sbuid" as "sbuid",
        IV."packageid" as "packageid",
        IV."riskid" as "riskid",
        'areaofpractice' as "inlinecategory",
        ---SUBSTR(TO_CHAR(TO_NUMBER(ORA_HASH(ALA."lawAreaId"||'-'||LA."lawAreaName"))),-3) as "linenumber",
        ALA."lawAreaSequence" as "linenumber",  --AOP sequence
        ALA."lawAreaId" as "description",
        NULL as "itemvalue",
        CASE 
            WHEN (TO_CHAR(Y."expirationDate", 'YYYYMMDD') <> TO_CHAR(P."expirationDate", 'YYYYMMDD')) and (TO_CHAR(Y."expirationDate", 'YYYYMMDD') <= IV."transactioneffectivedate") THEN 1
            ELSE 0
        END as "deletedrow",
        NULL as "conversionserver",
        NULL as "sequencenumber",
        IV."veffectivedate" as "veffectivedate",
        IV."vexpirydate" as "vexpirydate",
        NULL as "postedwweobjectid" 
    from (select * from "ISI_ItemFirmView" where "riskid" = 1) IV
        left join "Policy" P on P."policyNumber" = IV."POLICYNUMBER"
            left join "ApplicationLawAreaView" ALA on ALA."applicationId" = P."policyId"
                left join "LawArea" LA on LA."lawAreaId" = ALA."lawAreaId"
        left join "PolicyLawyerCoverageAllView" Y on Y."policyNumber" = IV."POLICYNUMBER" and Y."lawyerId" = IV."LAWYERID" and (TO_CHAR(Y."effectiveDate", 'YYYYMMDD') <= IV."transactioneffectivedate")
;

create or replace view "ISI_InlineSchedulesFirmLimView" as 
    -- firm level limit
    select 
        IV."POLICYNUMBER" as "POLICYNUMBER",
        IV."conversionreference" as "conversionreference",
        IV."transactiontype" as "transactiontype",
        IV."transactioneffectivedate" as "transactioneffectivedate",
        IV."transactionseqno" as "transactionseqno",
        IV."sbuid" as "sbuid",
        IV."packageid" as "packageid",
        IV."riskid" as "riskid",
        'quotelimitsfirm' as "inlinecategory",
        1 as "linenumber",
        NULL as "description",
        NULL as "itemvalue",
        0 as "deletedrow",
        NULL as "conversionserver",
        NULL as "sequencenumber",
        IV."veffectivedate" as "veffectivedate",
        IV."vexpirydate" as "vexpirydate",
        NULL as "postedwweobjectid"  
    from (select * from "ISI_ItemFirmView" where "riskid" = 1) IV
        left join "Policy" P on P."policyNumber" = IV."POLICYNUMBER"
;     


create or replace view "ISI_InlineSchedulesLawyerView" as
    --lawyer level
    select
        IV."POLICYNUMBER" as "POLICYNUMBER",
        IV."conversionreference" as "conversionreference",
        IV."transactiontype" as "transactiontype",
        IV."transactioneffectivedate" as "transactioneffectivedate",
        IV."transactionseqno" as "transactionseqno",
        IV."sbuid" as "sbuid",
        IV."packageid" as "packageid",
        IV."riskid" as "riskid",
        'quotelimitsattorney' as "inlinecategory",
        --TO_CHAR(IV."riskid", 'FM000') as "linenumber",  --SUBSTR(TO_CHAR(TO_NUMBER(ORA_HASH(Y."lawyerId"))),-3) as "linenumber",  --lawyer number
        IV."riskid" as "linenumber",  --lawyer number
        NULL as "description",
        NULL as "itemvalue",
        CASE 
            WHEN (TO_CHAR(Y."expirationDate", 'YYYYMMDD') <> TO_CHAR(P."expirationDate", 'YYYYMMDD')) and (TO_CHAR(Y."expirationDate", 'YYYYMMDD') <= IV."transactioneffectivedate") THEN 1
            ELSE 0
        END as "deletedrow",
        NULL as "conversionserver",
        NULL as "sequencenumber",
        IV."veffectivedate" as "veffectivedate",
        IV."vexpirydate" as "vexpirydate",
        NULL as "postedwweobjectid"
    from (select * from "ISI_ItemLawyerView" where "riskid" <> '001') IV  
        left join "Policy" P on P."policyNumber" = IV."POLICYNUMBER"
        left join "PolicyLawyerCoverageAllView" Y on Y."policyNumber" = IV."POLICYNUMBER" and Y."lawyerId" = IV."LAWYERID" and (TO_CHAR(Y."effectiveDate", 'YYYYMMDD') <= IV."transactioneffectivedate")
order by "POLICYNUMBER", "transactioneffectivedate", "riskid"
;

/*---Testing ---*/
select * from "ISI_ItemView";
select * from "ISI_InsuredsView";
select * from "ISI_InlineSchedulesView";


--------------------------------------------------------------------------------
/*
    ISI_AreaOfPractice1View
    is_areaofpractice_1
    
    Dependancies: ISI_ItemView
    
    Notes:
    
    History:
    12/18/2017  FZ  Created
    01/20/2018  FZ  fixed linenumber AOP duplicates
    02/15/2018  FZ  default aop factor sould be 0.00 since ISI is adding aop factors
    
*/
create or replace view "ISI_AreaOfPractice1View" as 
    select
        IV."POLICYNUMBER" as "POLICYNUMBER",
        IV."conversionreference" as "conversionreference",
        IV."transactioneffectivedate" as "transactioneffectivedate",
        IV."transactionseqno" as "transactionseqno",
        IV."transactiontype" as "transactiontype",   
        IV."sbuid" as "sbuid",
        0 as "packageid",
        --IV."riskid" as "riskid",
        IV."insuredid" as "riskid",  --source is InsuredsView 
        'areaofpractice' as "inlinecategory",
        --SUBSTR(TO_CHAR(TO_NUMBER(ORA_HASH(ALA."lawAreaId"||'-'||LA."lawAreaName"))),-3) as "linenumber",
        ALA."lawAreaSequence" as "linenumber",
        ALA."lawAreaId" as "aopcode",
        CASE
            WHEN ALA."lawAreaId" = 'BD' THEN COALESCE(RC."BodilyInjuryDefense", 0.00)  --02/15/2018 all defaults change to 0.00 from 1.00
            WHEN ALA."lawAreaId" = 'BP' THEN COALESCE(RC."BodilyInjuryPlaintiff", 0.00)
            WHEN ALA."lawAreaId" = 'CB' THEN COALESCE(RC."CorpBusOrg", 0.00)
            WHEN ALA."lawAreaId" = 'EP' THEN COALESCE(RC."EstateProbateTrust", 0.00)
            WHEN ALA."lawAreaId" = 'CP' THEN COALESCE(RC."IntellProp", 0.00)
            WHEN ALA."lawAreaId" = 'RE' THEN COALESCE(RC."RealEstate", 0.00)
            WHEN ALA."lawAreaId" = 'WD' THEN COALESCE(RC."WorkersCompDefense", 0.00)
            WHEN ALA."lawAreaId" = 'WP' THEN COALESCE(RC."WorkersCompPlaintiff", 0.00)
            ELSE 0.00  --02/15/2018 changed to 0.00 from 1.00
        END as "aopfactor",
        100 as "aopfactorhigh",
        0 as "aopfactorlow",
        NULL as "aopotherexplain",  --NEED: determine where and if stored in LAW
        ALA."lawAreaPercent" as "aoppercent",
        NULL as "aopweightedfactor",
        NULL as "conditionalmandatoryfields",
        NULL as "conversionserver",
        NULL as "sequencenumber"
--    from (select * from "ISI_ItemView" where "riskid" = 1) IV
    from (select * from "ISI_InsuredsFirmView" where "insuredid" = 1) IV  --to speed up creation
        left join "Policy" P on P."policyNumber" = IV."POLICYNUMBER"
            left join "ApplicationLawAreaView" ALA on ALA."applicationId" = P."policyId"
            --    left join "LawArea" LA on LA."lawAreaId" = ALA."lawAreaId"
            left join "RatingCrosstab" RC on RC."applicationId" = P."policyId"
order by "POLICYNUMBER", "transactioneffectivedate", "transactionseqno"
;

/*--- Testing ---*/
select * from "ApplicationLawArea" where "lawAreaId" = 'ZZ' and "lawAreaNote" is not NULL;
select * from "Question" order by "questionId";
select * from "ApplicationQuestion" where "questionId" = 'ZZ';
select * from "Rating";
select * from "LawArea" order by "lawAreaId";
--BERGL1_2008_2018|20110410
select * from "ISI_AreaOfPractice1View" where "conversionreference" = 'BERGL1_2008_2018' order by "transactioneffectivedate"; -- = '20110410';
select * from "RatingCrosstab" where "firmId" = 'BERGL1';
--------------------------------------------------------------------------------
/*
    ISI_QuoteLimits1View
    is_quotelimits_1
    
    Dependancies: ISI_QuoteLimitsAttorneyView, ISI_ItemView
    Notes:
    -   factors are multiply ready (ie: -0.05 = 1.05)
    
    History
    12/22/2017  FZ  Created
    12/26/2017  FZ  Completed version 1
*/
create or replace view "ISI_QuoteLimits1View" as
    select
        QLSV."POLICYNUMBER" as "POLICYNUMBER",
        QLSV."conversionreference" as "conversionreference",
        QLSV."transactioneffectivedate" as "transactioneffectivedate",
        QLSV."transactionseqno" as "transactionseqno",
        QLSV."transactiontype" as "transactiontype",  
        '4112' as "sbuid",
        0 as "packageid",
        1 as "riskid",
        'quotelimitsfirm' as "inlinecategory",
        1 as "linenumber",
        COALESCE(RLC."LimitAgg", 1.00) as "additionallimitfactor",
        NULL as "attorneyfactor",
        QLSV."attorneyfactorsubtotal" as "attorneyfactorsubtotal",
        QLSV."attorneyfactorsubtotal" - QLSV."clecreditsfactorsubtotal" as "attorneyfactortotal",
        QLSV."baserate" as "baserate",
        1 as "clecreditsfactor",
        QLSV."clecreditsfactorsubtotal" as "clecreditsfactorsubtotal",
        QLSV."clecreditsfactorsubtotal" - QLSV."continuityfactorsubtotal" as "clecreditsfactortotal",
        NULL as "conditionalmandatoryfields",
        1 + COALESCE(RC."YearsInsured", 0.00) as "continuityfactor",
        QLSV."continuityfactorsubtotal" as "continuityfactorsubtotal",
        QLSV."continuityfactorsubtotal" - QLSV."firmsizefactorsubtotal" as "continuityfactortotal",
        RLC."DeductibleFactor" as "deductiblefactor",
        QLSV."deductiblefactorsubtotal" as "deductiblefactorsubtotal",
        QLSV."deductiblefactorsubtotal" - QLSV."baserate" as "deductiblefactortotal",
        QLSV."xs10premiumsubtotal" as "facpremiumxs10m",
        NULL as "facpremiumxs10mreadonly",
        QLSV."xs5premiumsubtotal" as "facpremiumxs5m",
        NULL as "facpremiumxs5mreadonly",
        NULL as "facpremiumxs5msysind", --added v133
        1 as "firmlinenumber",
        QLSV."attorneypremium" as "firmpremium",
        COALESCE(RC."Firm", 1.00) as "firmsizefactor",
        QLSV."firmsizefactorsubtotal" as "firmsizefactorsubtotal",
        QLSV."firmsizefactorsubtotal" - QLSV."limitfactorsubtotal" as "firmsizefactortotal",
        P."perClaimDeductible" as "liabilitydeductible",
        LTRIM(TO_CHAR(P."perClaimDeductible", '999,999,999')) as "liabilitydeductibletext",
        P."perClaimLimit"||'_'||P."aggregateLimit" as "liabilitylimit",
        P."perClaimLimit" as "liabilitylimit1",
        P."aggregateLimit" as "liabilitylimit2",
        LTRIM(TO_CHAR(P."perClaimLimit", '999,999,999'))||'/'||LTRIM(TO_CHAR(P."aggregateLimit", '999,999,999')) as "liabilitylimittext",
        NULL as "liabilitypremiumtext",
        RLC."LimitFactor" as "limitfactor",
        QLSV."limitfactorsubtotal" as "limitfactorsubtotal",
        QLSV."limitfactorsubtotal" - QLSV."deductiblefactorsubtotal" as "limitfactortotal",
        1 as "policylimitind",
        --TO_CHAR(PRDV."ISIPolicyRetroDate", 'YYYYMMDD') as "quotelimitretrodate",  --removed v133
        --TO_CHAR(PRDV."ISIPolicyRetroDate", 'YYYYMMDD') as "quotelimitretrodatereadonly", --removed v133
        1 as "quoteproposalind",
        ROUND(RC."SchdAdjPctCalc",2) as "scheduledadjustmentfactor",
        QLSV."attorneyfactorsubtotal" - QLSV."scheduledadjustmentfactorsu_1" as "scheduleadjustmentfactorsu_1",
        QLSV."scheduledadjustmentfactorsu_1" as "scheduledadjustmentfactortotal",
        NULL as "xs10premiumfactor",
        NULL as "xs10premiumperattorney",
        QLSV."xs10premiumsubtotal" as "xs10premiumsubtotal",
        0 as "xs10premiumtotal",
        1 as "xs5factor",
        0 as "xs5minimum",
        NULL as "xs5premiumfactor",
        NULL as "xs5premiumperattorney",
        QLSV."xs5premiumsubtotal" as "xs5premiumsubtotal",
        0 as "xs5premiumtotal",
        NULL as "conversionserver",
        NULL as "sequencenumber"
      from "ISI_QuoteLimitSummary" QLSV
        left join "RatingCrosstab" RC on RC."applicationId" = QLSV."policyId"
        left join "Policy" P on P."policyId" = QLSV."policyId"
            left join "QuoteCoverage" QC on QC."quoteBookId" = RC."quoteBookId" and QC."perClaimLimit" = P."perClaimLimit" and QC."aggregateLimit" = P."aggregateLimit" and QC."perClaimDeductible" = P."perClaimDeductible"
                left join "RatingLimitCrosstab" RLC on RLC."quoteBookId" = RC."quoteBookId" and RLC."quoteCoverageId" = QC."quoteCoverageId"
        left join "ISIPolicyRetroDateView" PRDV on PRDV."policyNumber" = QLSV."POLICYNUMBER"        
    order by QLSV."POLICYNUMBER", QLSV."transactioneffectivedate", QLSV."transactionseqno"    
;
/*--- Testing ---*/
select * from "ISI_QuoteLimitSummaryView";
select * from "ISI_ItemView";
select * from "ISI_QuoteLimits1View";
select * from "Policy" where "firmId" in ('KIMLAV');
select * from "RatingCrosstab" where "applicationId" in ('AP000823716');
select * from "Quote" where "applicationId" in ('AP000823716');
select * from "RatingLimitCrosstab" where "quoteBookId" in ('AR000839542');
select * from "RatingLimitCrosstab" where "LimitAgg" is not NULL;
select * from "QuoteCoverage" where "quoteBookId" in ('AR000839542');
select * from "PremiumPerLawyerView" where "firmId" in ('KIMLAV');

--------------------------------------------------------------------------------
/*
    ISI_ QuoteLimitAttorneysView
    is_quotelimitsattorney_1
    
    Dependancies: ISI_ItemView
    Notes:
    -   factors are multiply ready (ie: -0.05 = 1.05)
    
    History
    12/19/2017  FZ  Created
    01/20/2018  FZ  riskid
*/
create or replace view "ISI_QuoteLimitsAttorneyView" as
    select
        IV."POLICYNUMBER" as "POLICYNUMBER",
        IV."LAWYERID" as "LAWYERID",
        IV."conversionreference" as "conversionreference",
        IV."transactioneffectivedate" as "transactioneffectivedate",
        IV."transactionseqno" as "transactionseqno",
        IV."transactiontype" as "transactiontype",  
        IV."sbuid" as "sbuid",
        0 as "packageid",
        IV."riskid" as "riskid",
        'quotelimitsattorney' as "inlinecategory",
        IV."riskid" as "linenumber",  --lawyer number
        COALESCE(PPLV."LimitAgg", 1.00) as "additionallimitfactor", ---QUESTION: assuming this is the aggregate limit factor??????
        COALESCE(PPLV."LawyerFactor", 1.00) as "attorneyfactor",
        NULL as "attorneyfactorcomment",
        --#7 Lawyer Factor
        ROUND(ROUND(((((COALESCE(PPLV."DeductibleFactor", 1.00) * COALESCE(PPLV."Base", 0)) * (COALESCE(PPLV."LimitFactor", 1.00) * COALESCE(PPLV."LimitAgg", 1.00))) * COALESCE(PPLV."Firm", 1.00)) * (1 + (COALESCE(PPLV."YearsInsured",1.00)))) * (1 +  COALESCE(PPLV."ContLegalEdCredit", 0)),0) * COALESCE(PPLV."LawyerFactor",1.00),0) as "attorneyfactorsubtotal",
        IV."riskid" as "attorneynumber",
        --#11 Total Lawyer Premium
        ROUND((((((((COALESCE(PPLV."DeductibleFactor", 1.00) * COALESCE(PPLV."Base", 0)) * (COALESCE(PPLV."LimitFactor", 1.00) * COALESCE(PPLV."LimitAgg", 1.00))) * COALESCE(PPLV."Firm", 1.00)) * (1 + (COALESCE(PPLV."YearsInsured", 1.00)))) * (1 +  COALESCE(PPLV."ContLegalEdCredit", 0)) * COALESCE(PPLV."LawyerFactor",1.00)) * COALESCE(PPLV."SchdAdjPctCalc", 1.00)) + COALESCE(PPLV."premiumPerLawyerXS5M", 0)),0) + COALESCE(PPLV."premiumPerLawyerXS10M", 0) as "attorneypremium",
        0 as "attorneypremium1m",
        0 as "attorneypremium5m",
        --#1 Base
        COALESCE(PPLV."Base", 0) as "baserate",
        1 +  COALESCE(PPLV."ContLegalEdCredit", 0) as "clecreditsfactor",
        NULL as "clecreditsfactorcomment",
        --#6 CLE
        ROUND(((((COALESCE(PPLV."DeductibleFactor", 1.00) * COALESCE(PPLV."Base", 0)) * (COALESCE(PPLV."LimitFactor", 1.00) * COALESCE(PPLV."LimitAgg", 1.00))) * COALESCE(PPLV."Firm", 1.00)) * (1 + (COALESCE(PPLV."YearsInsured",1.00)))) * (1 +  COALESCE(PPLV."ContLegalEdCredit", 0)),0) as "clecreditsfactorsubtotal",
        1 + (COALESCE(PPLV."YearsInsured",1.00)) as "continuityfactor",
        NULL as "continuityfactorcomment",
        --#5 Continuity
        ROUND((((COALESCE(PPLV."DeductibleFactor", 1.00) * COALESCE(PPLV."Base", 0)) * (COALESCE(PPLV."LimitFactor", 1.00) * COALESCE(PPLV."LimitAgg", 1.00))) * COALESCE(PPLV."Firm", 1.00)) * (1 + (COALESCE(PPLV."YearsInsured",1.00))),0) as "continuityfactorsubtotal",
        COALESCE(PPLV."DeductibleFactor", 1.00) as "deductiblefactor",
        NULL as "deductiblefactorcomment",
        --#2 Deductible
        ROUND(COALESCE(PPLV."DeductibleFactor", 1.00) * COALESCE(PPLV."Base", 0),0) as "deductiblefactorsubtotal",
        --NULL as "donotrateind",  --removed v133
        COALESCE(PPLV."Firm", 1.00) as "firmsizefactor",
        NULL as "firmsizefactorcomment",
        --#4 Firmsize
        ROUND(((COALESCE(PPLV."DeductibleFactor", 1.00) * COALESCE(PPLV."Base", 0)) * (COALESCE(PPLV."LimitFactor", 1.00)* COALESCE(PPLV."LimitAgg", 1.00))) * COALESCE(PPLV."Firm", 1.00),0)  as "firmsizefactorsubtotal",
        P."perClaimDeductible" as "liabilitydeductibletext",
        P."perClaimLimit" as "liabilitylimit1",
        P."aggregateLimit" as "liabilitylimit2",
        LTRIM(TO_CHAR(P."perClaimLimit", '999,999,999'))||'/'||LTRIM(TO_CHAR(P."aggregateLimit", '999,999,999')) as "liabilitylimittext",
        COALESCE(PPLV."LimitFactor", 1.00) as "limitfactor",
        NULL as "limitfactorcomment",
        --#3 Limit
        ROUND((COALESCE(PPLV."DeductibleFactor", 1.00) * COALESCE(PPLV."Base", 0)) * (COALESCE(PPLV."LimitFactor", 1.00) * COALESCE(PPLV."LimitAgg", 1.00)),0) as "limitfactorsubtotal",
        NULL as "militaryleaveind",
        1 as "policylimitind",
        --IV."retrodate" as "quotelimitretrodate",--removed v133
        ROUND(COALESCE(PPLV."SchdAdjPctCalc", 1.00),2) as "scheduledadjustmentfactor",
        NULL as "scheduledadjustmentfactorco_1",      
        -- #8 Schedule Adjustment
        ROUND(ROUND((((((COALESCE(PPLV."DeductibleFactor", 1.00) * COALESCE(PPLV."Base", 0)) * (COALESCE(PPLV."LimitFactor", 1.00) * COALESCE(PPLV."LimitAgg", 1.00))) * COALESCE(PPLV."Firm", 1.00)) * (1 + (COALESCE(PPLV."YearsInsured",1.00)))) * (1 +  COALESCE(PPLV."ContLegalEdCredit", 0)) * COALESCE(PPLV."LawyerFactor",1.00)),0) * COALESCE(PPLV."SchdAdjPctCalc", 1.00),0) as "scheduledadjustmentfactorsu_1",
        COALESCE(PPLV."premiumPerLawyerXS10M", 0) as "xs10premium",
        NULL as "xs10premiumcomment",
        NULL as "xs10premiumperattorney",
        --#10 XS10M
        ROUND((((((((COALESCE(PPLV."DeductibleFactor", 1.00) * COALESCE(PPLV."Base", 0)) * (COALESCE(PPLV."LimitFactor", 1.00) * COALESCE(PPLV."LimitAgg", 1.00))) * COALESCE(PPLV."Firm", 1.00)) * (1 + (COALESCE(PPLV."YearsInsured",1.00)))) * (1 +  COALESCE(PPLV."ContLegalEdCredit", 0)) * COALESCE(PPLV."LawyerFactor",1.00)) * COALESCE(PPLV."SchdAdjPctCalc", 1.00)) + COALESCE(PPLV."premiumPerLawyerXS5M", 0)) + COALESCE(PPLV."premiumPerLawyerXS10M", 0),0) as "xs10premiumsubtotal",
        1 as "xs5factor",
        0 as "xs5minimum",
        COALESCE(PPLV."premiumPerLawyerXS5M", 0) as "xs5premium",
        NULL as "xs5premiumcomment",
        COALESCE(PPLV."premiumPerLawyerXS5M", 0) as "xs5premiumperattorney",
        --#9 XS5M
        ROUND(((((((COALESCE(PPLV."DeductibleFactor", 1.00) * COALESCE(PPLV."Base", 0)) * (COALESCE(PPLV."LimitFactor", 1.00) * COALESCE(PPLV."LimitAgg", 1.00))) * COALESCE(PPLV."Firm", 1.00)) * (1 + (COALESCE(PPLV."YearsInsured",1.00)))) * (1 +  COALESCE(PPLV."ContLegalEdCredit", 0)) * COALESCE(PPLV."LawyerFactor",1.00)) * COALESCE(PPLV."SchdAdjPctCalc", 1.00)) + COALESCE(PPLV."premiumPerLawyerXS5M", 0),0) as "xs5premiumsubtotal",
        NULL as "conversionserver",
        NULL as "sequencenumber"
    from (select * from "ISI_ItemLawyerView" where "riskid" <> 1) IV
        left join "Policy" P on P."policyNumber" = IV."POLICYNUMBER"
            --left join "PremiumPerLawyerView" PPLV on PPLV."policyId" = P."policyId" and PPLV."lawyerId" = IV."LAWYERID" and PPLV."aggregateLimit" = P."aggregateLimit" and PPLV."Limit" = P."perClaimLimit" and PPLV."Deductible" = P."perClaimDeductible"
            left join "PremiumPerLawyerView" PPLV on PPLV."policyId" = P."policyId" and PPLV."effectiveDate" = P."effectiveDate" and PPLV."lawyerId" = IV."LAWYERID" and PPLV."aggregateLimit" = P."aggregateLimit" and PPLV."Limit" = P."perClaimLimit" and PPLV."Deductible" = P."perClaimDeductible"
 order by "POLICYNUMBER", "transactioneffectivedate", "transactionseqno"   
;
/*---Testing ---*/
select * from "ISI_ItemView";
select * from "ISI_InsuredsView";
select * from "PremiumPerLawyerView" where "firmId" in ('KIMLAV');
select * from "PremiumPerLawyerView" where "premiumPerLawyerXS5M" <> 0 order by "applicationId";--where "ContLegalEdCredit" <> 0;
select * from "ISI_QuoteLimitsAttorneyView";

-- Summary view 
select * from "ISI_QuoteLimitSummaryView";
create or replace view "ISI_QuoteLimitSummaryView" as
    select
        Z."POLICYNUMBER" as "POLICYNUMBER",
        P."policyId" as "policyId",
        Z."conversionreference" as "conversionreference",
        Z."transactioneffectivedate" as "transactioneffectivedate",
        Z."transactionseqno" as "transactionseqno",
        Z."transactiontype" as "transactiontype",
        SUM(Z."baserate") as "baserate",
        SUM(Z."attorneypremium") as "attorneypremium",
        SUM(Z."attorneyfactorsubtotal") as "attorneyfactorsubtotal",
        SUM(Z."clecreditsfactorsubtotal") as "clecreditsfactorsubtotal",
        SUM(Z."continuityfactorsubtotal") as "continuityfactorsubtotal",
        SUM(Z."deductiblefactorsubtotal") as "deductiblefactorsubtotal",
        SUM(Z."firmsizefactorsubtotal") as "firmsizefactorsubtotal",
        SUM(Z."limitfactorsubtotal") as "limitfactorsubtotal",
        SUM(Z."scheduledadjustmentfactorsu_1") as "scheduledadjustmentfactorsu_1",
        SUM(Z."xs10premiumsubtotal") as "xs10premiumsubtotal",
        SUM(Z."xs5premiumsubtotal") as "xs5premiumsubtotal"
    from "ISI_QuoteLimitsAttorneyView" Z
        left join "Policy" P on P."policyNumber" = Z."POLICYNUMBER"
    group by "POLICYNUMBER", "policyId", "conversionreference", "transactioneffectivedate", "transactionseqno", "transactiontype"
    order by "POLICYNUMBER", "transactioneffectivedate", "transactionseqno"
;

/* MUST RUN for PERFORMANCE */
drop table "ISI_QuoteLimitSummary";
create table "ISI_QuoteLimitSummary" as 
    select * from "ISI_QuoteLimitSummaryView";