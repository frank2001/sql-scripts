    /*******************************************************************************
	ISI Conversion Scripts
	Scripts used to extract LAW data into ISI interim conversion tables
	
	Naming convention = 'ISI_'+ ISI tablename minus leading alpha underscore + 'View'
	
	Created: FZ	07/07/2017
	Revised: FZ 07/18/2017 - Added ISI_ContactRelationshipsView
	
*******************************************************************************/
******************
**  COMMON
******************

-- E_Contact
-- ISI_ContactView
--create or replace view "ISI_ContactView" as 
  select
    E."entityId" as "entitykey",
    0 as "entitykeynumber",
    CASE 
      WHEN O."organizationId" is NULL and P."lastName" is not NULL THEN P."firstName"
      ELSE NULL
    END as "firstname",
    CASE 
      WHEN O."organizationId" is NULL THEN P."middleName"
      ELSE NULL
    END as "middlename",
    CASE 
      WHEN O."organizationId" is NULL and P."firstName" is not NULL THEN P."lastName"
      ELSE NULL
    END as "lastname",
    CASE 
      --WHEN O."organizationId" is NULL and P."title" = 'Atty.' THEN 'Atty'
      WHEN O."organizationId" is NULL and (P."firstName" is not NULL or P."lastName" is not NULL) THEN REPLACE(P."title", '.', '')
      ELSE NULL
    END as "nameprefix",
    CASE 
      WHEN O."organizationId" is NULL THEN REPLACE(P."suffix", '.', '')
      ELSE NULL
    END as "namesuffix",
    CASE 
      WHEN O."organizationId" is NULL THEN P."jobTitle"
      ELSE NULL
    END as "jobtitle",
    CASE 
      WHEN O."organizationId" is NULL and (P."firstName" is not NULL and P."lastName" is not NULL) THEN E."entityName"
      ELSE NULL
    END as "fullname",  --maybe concatenate from Person
    CASE 
      WHEN O."organizationId" is not NULL then E."entityName"
      WHEN O."organizationId" is NULL and (P."firstName" is NULL or P."lastName" is NULL) THEN E."entityName"
      ELSE NULL
    END as "companyname",
    1 as "preferredhtc",
    CASE 
      WHEN O."organizationId" is NULL and (P."firstName" is not NULL and P."lastName" is not NULL) THEN 1  --there are person recs for companies????
      ELSE 0
    END as "isindividual",
    CASE
      WHEN (O."organizationId" is not NULL) or (O."organizationId" is NULL and (P."firstName" is NULL or P."lastName" is NULL)) THEN
        CASE 
          WHEN O."organizationLegalForm" = 'P' THEN 'PR'
          WHEN O."organizationLegalForm" = 'LLP' THEN 'LLP' 
          WHEN O."organizationLegalForm" = 'PSC' THEN 'PC'
          WHEN O."organizationLegalForm" = 'I' THEN 'SP'
          WHEN O."organizationLegalForm" = 'LLC' THEN 'LLC'
          WHEN O."organizationLegalForm" = 'GP' THEN 'GP'
          WHEN O."organizationLegalForm" is NULL and substr(E."entityName", -3, 3) = 'LLP' THEN 'LLP'
          WHEN O."organizationLegalForm" is NULL and substr(E."entityName", -3, 3) = 'LLC' THEN 'LLC'
          ELSE 'CP'
        END 
    END as "contacttype", 
    NULL as "birthdate",
    NULL as "gender",
    NULL as "maritalstatus",
    NULL as "comments",
    NULL as "conversionserver",
    NULL as "sequencenumber",
    NULL as "linkedentitykey,
    /* --09/19/2017 removed since WILMIC does use specific "legal name"
    CASE 
      WHEN O."organizationId" is not NULL THEN E."entityLocator"
      ELSE NULL
    END as "linkedentitykey", --entityLocator
    */
    0 as "linkedentitykeynumber",
    NULL as "ssn",
    NULL as "hcin",
    NULL as "fein",
    NULL as "tin"
  from "Entity" E
    left join "Organization" O on O."organizationId" = E."entityId"
    left join "Person" P on P."personId" = E."entityId"
;

select * from "ISI_ContactView";
/*

******************************************************************************
-- e_address
-- ISI_AddressVew
create or replace view "ISI_AddressView" as 
  select
    L."locationId"||'_'||C."contactId" as "addresskey",
    CASE
      WHEN L."mainEntityId" = 'LFUNKNOWN' THEN C."contactEntityId"  --accounts for LFUNKNOWN records in Contact
      ELSE L."mainEntityId" 
    END as "entitykey",
    0 as "entitykeyvnumber",
    CASE
      WHEN L."locationTypeId" = 'Firm' THEN 'P'
      ELSE 'CL' --locationTypeId = Alternate, Branch or Other
    END as "entitytype",  --default to C=Contact...P=Policy, CL=Claim
    'F' as "addresskind", -- default to F=Full
    CASE 
      WHEN C."contactTypeId" = 'Firm' THEN 1
      ELSE 0
    END as "mailingaddressind",
    1 as "addresstype",
    NULL as "incareof",
    NULL as "addressname",
    CASE
      WHEN substr("addressLine3", 1, 2) in ('PO', 'P.', 'P ', 'Po') THEN NULL
      ELSE "addressLine3"
    END as "buildingname",
    CASE
      WHEN substr("addressLine3", 1, 2) in ('PO', 'P.', 'P ', 'Po') THEN LTRIM(REGEXP_SUBSTR(L."addressLine3", '[^x]+$')) 
      ELSE NULL
    END as "pobox",
    NULL as "streetnumber",
    L."addressLine1" as "street",
    NULL as "streetdirection",
    CASE
        WHEN LOWER(SUBSTR(L."addressLine2", 1, 5)) = 'suite' THEN 'Suite'
        WHEN LOWER(SUBSTR(L."addressLine2", 1, 3)) = 'apt' THEN 'Apt'
        ELSE NULL
    END as "unitdesignator",
    CASE
        WHEN LOWER(SUBSTR(L."addressLine2", 1, 5)) = 'suite' OR LOWER(SUBSTR(L."addressLine2", 1, 3)) = 'apt' THEN REGEXP_REPLACE(L."addressLine2", '[^0-9]', '')
        ELSE NULL
    END as "unitnumber",
    CASE
        WHEN LOWER(SUBSTR(L."addressLine2", 1, 5)) = 'suite' OR LOWER(SUBSTR(L."addressLine2", 1, 3)) = 'apt' THEN NULL
        ELSE "addressLine2"
    END as "addressline2",
    NULL as "lot",
    NULL as "concession",
    NULL as "lotpart",
    NULL as "site",
    NULL as "compartment",
    NULL as "quadrant",
    NULL as "section",
    NULL as "township",
    NULL as "range",
    NULL as "meridian",
    NULL as "routetype",
    NULL as "routenumber",
    NULL as "stationtype",
    NULL as "stationnumber",
    NULL as "countycode",
    L."county" as "countyname",
    L."city" as "city",
    CASE
      WHEN L."country" is NULL THEN UPPER(L."state")
      ELSE NULL
    END as "regioncode",
    S."stateName" as "regionname",
    L."zip" as "zippostal",
    CASE
      WHEN L."country" is NULL THEN 'US'
      ELSE L."country"
    END as "countrycode",
    'United States' as "countryname",
    NULL as "customformattedaddress",
    CASE 
      WHEN E."modifyDate" is NOT NULL THEN TO_CHAR(E."modifyDate", 'YYYYMMDD')
      ELSE TO_CHAR(E."createDate", 'YYYYMMDD')
    END as "modifieddate",
    NULL as "wweobjectid",
    NULL as "conversionserver",
    NULL as "sequencenumber",
    NULL as "addrnumsuffix",
    NULL as "streettype",
    NULL as "traveldirection",
    NULL as "milemarker",
    NULL as "exitnumber",
    NULL as "streettype2",
    NULL as "traveldirection2",
    NULL as "interstreetname",
    NULL as "latitudedegree",
    NULL as "latitudeminute",
    NULL as "latitudesecond",
    NULL as "longitudedegree",
    NULL as "longitudeminute",
    NULL as "longitudesecond",
    NULL as "adjacentstreetind",
    NULL as "adjacentstreetnumber",
    NULL as "adjacentstreet",
    NULL as "adjacentstreetdirection"
  from "Location" L
    left join (select * from "Contact" where "contactTypeId" = 'Firm') C on C."locationId" = L."locationId"
    left join "State" S on S."stateId" = L."state"
    left join "Event" E on E."eventId" = L."eventId"
;
select * from "ISI_AddressView" order by "entitykey";
select count(*) from "ISI_AddressView";
select count(*) from "Location";

******************************************************************************
-- e_phonenumbers
-- ISI_PhoneNumbersView
--  Location
select * from "Location";
create or replace view "ISI_PhoneView" as 
  select
    L."locationId"||L."telephone"||'.1' as "contactmethodkey",
    L."mainEntityId" as "entitykey",
    0 as "entitykeyvnumber",
    1 as "primaryind",  --firm phone # is primary
    1 as "htctype",
    SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(L."telephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),1,3) as "areacode",
    SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(L."telephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),4,3)||'-'||
      SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(L."telephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),7,4) as "phonenumber",
    NULL as "extension",
    SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(L."telephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),1,3)||'-'||
      SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(L."telephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),4,3)||'-'||
      SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(L."telephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),7,4) as "formattedhtc",
    NULL as "conversionserver",
    NULL as "sequencenumber"
  from "Location" L
  where L."telephone" is not NULL
 union all
  select 
    L."locationId"||L."fax" as "contactmethodkey",
    L."mainEntityId" as "entitykey",
    0 as "entitykeyvnumber",
    0 as "primaryind",
    2 as "htctype",
    SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(L."fax", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),1,3) as "areacode",
    SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(L."fax", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),4,3)||'-'||
      SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(L."fax", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),7,4) as "phonenumber",
    NULL as "extension",
    SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(L."fax", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),1,3)||'-'||
      SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(L."fax", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),4,3)||'-'||
      SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(L."fax", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),7,4) as "formattedhtc",
    NULL as "conversionserver",
    NULL as "sequencenumber"
  from "Location" L
  where L."fax" is not NULL
 union all
  select
    P."personId"||P."homeTelephone"||'.5' as "contactmethodkey",
    P."personId" as "entitykey",
    0 as "entitykeyvnumber",
    0 as "primaryind",
    5 as "htctype",
    SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(P."homeTelephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),1,3) as "areacode",
    SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(P."homeTelephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),4,3)||'-'||
      SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(P."homeTelephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),7,4) as "phonenumber",
    NULL as "extension",
    SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(P."homeTelephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),1,3)||'-'||
      SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(P."homeTelephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),4,3)||'-'||
      SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(P."homeTelephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),7,4) as "formattedhtc",
    NULL as "conversionserver",
    NULL as "sequencenumber"
  from "Person" P 
  where P."homeTelephone" is not NULL
 union all
  select
    P."personId"||P."workTelephone" as "contactmethodkey",
    P."personId" as "entitykey",
    0 as "entitykeyvnumber",
    0 as "primaryind",
    1 as "htctype",
    SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(P."workTelephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),1,3) as "areacode",
    SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(P."workTelephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),4,3)||'-'||
      SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(P."workTelephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),7,4) as "phonenumber",
    NULL as "extension",
    SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(P."workTelephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),1,3)||'-'||
      SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(P."workTelephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),4,3)||'-'||
      SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(P."workTelephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),7,4) as "formattedhtc",
    NULL as "conversionserver",
    NULL as "sequencenumber"
  from "Person" P 
  where P."workTelephone" is not NULL
 union all
  select
    P."personId"||P."mobileTelephone" as "contactmethodkey",
    P."personId" as "entitykey",
    0 as "entitykeyvnumber",
    0 as "primaryind",
    8 as "htctype",
    SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(P."mobileTelephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),1,3) as "areacode",
    SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(P."mobileTelephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),4,3)||'-'||
      SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(P."mobileTelephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),7,4) as "phonenumber",
    NULL as "extension",
    SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(P."mobileTelephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),1,3)||'-'||
      SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(P."mobileTelephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),4,3)||'-'||
      SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(P."mobileTelephone", '.',NULL), '-',NULL),'(',NULL),')',NULL),' ',NULL),'_',0),7,4) as "formattedhtc",
    NULL as "conversionserver",
    NULL as "sequencenumber"
  from "Person" P 
  where P."mobileTelephone" is not NULL
;
select * from "ISI_PhoneView";
******************************************************************************
-- e_emailandwebsites
-- ISI_EmailWebsitesView
--  Entity, Location, Person
create or replace view "ISI_EmailWebsiteView" as 
  select
    E."eventId"||'.10' as "contactmethodkey",
    E."entityId" as "entitykey",
    0 as "entitykeyvnumber",
    0 as "primaryind",
    10 as "htctype", 
    CASE
      WHEN ((SUBSTR(E."entityURL",1,3) <> 'www') and (SUBSTR(E."entityURL",1,4) <> 'http')) THEN 'www.'||REPLACE(E."entityURL", ',', '.')
      ELSE REPLACE(E."entityURL", ',', '.')
    END as "emailandweb", 
    NULL as "conversionserver",
    NULL as "sequencenumber"
  from "Entity" E
  where E."entityURL" is not NULL
 union all
  select 
    P."personId"||'.3' as "contactmethodkey",
    P."personId" as "entitykey",
    0 as "entitykeyvnumber",
    0 as "primaryind",
    3 as "htctype", 
    REPLACE(P."email", ',', '.') as "emailandweb",  -- NEED: to remove non-email addresses - No email, unsubscribe, unlisted
    NULL as "conversionserver",
    NULL as "sequencenumber"
  from "Person" P
  where P."email" is not NULL
 union all
  select 
    P."personId"||'.7' as "contactmethodkey",
    P."personId" as "entitykey",
    0 as "entitykeyvnumber",
    0 as "primaryind",
    7 as "htctype", 
    REPLACE(P."homeEmail", ',', '.') as "emailandweb", 
    NULL as "conversionserver",
    NULL as "sequencenumber"
  from "Person" P
  where P."homeEmail" is not NULL
 union all
  select 
    L."eventId"||'.3' as "contactmethodkey",
    L."mainEntityId" as "entitykey",
    0 as "entitykeyvnumber",
    0 as "primaryind",
    3 as "htctype", 
    REPLACE(L."email", ',', '.') as "emailandweb",  -- NEED: to remove non-email addresses - No email, unsubscribe, unlisted
    NULL as "conversionserver",
    NULL as "sequencenumber"
  from "Location" L
  where L."email" is not NULL
;


******************************************************************************
-- e_contactaccounts
-- ISI_ContactAccountsView
--  EFT*
-------------------- DO THIS LATER SINCE SENSITIVE DATA -----------------------

--******************************************************************************
-- e_license
-- ISI_LicenseView
--  LawyerStateBar
select * from "LawyerStateBar";
create or replace view "ISI_LicenseView" as 
-- lawyer -> firm 
  select
    LSB."lawyerId" as "licensekey",
    LSB."lawyerId" as "entitykey",
    0 as "entitykeyvnumber",
    'BN' as "licensetype",
    'US' as "licensecountry",
    'WI' as "licensejurisdiction",
    '-' as "licenseclass",
    CASE
        WHEN LSB."barAdmitDate" is not NULL THEN TO_CHAR(LSB."barAdmitDate", 'YYYYMMDD')
        ELSE '20200101'
    END as "licensedate",  
    LSB."stateBarNumber" as "licensenumber",
    1 as "currentind",  -- controlled by state bar
    'A' as "licensestatus", -- controlled by state bar
    NULL as "licensestatus_reason",
    NULL as "licensestatus_startdate",
    NULL as "licensestatus_enddate",
    NULL as "specialtrainingind",
    NULL as "specialtrainingdate"
  from "LawyerStateBar" LSB
;
select * from "ISI_LicenseView";
--******************************************************************************
-- e_contactrelationships
-- ISI_ContactRelationshipView
--
create or replace view "ISI_ContactRelationshipView" as
  select
    L."lawyerId" as "contactentitykey",
    0 as "contactentitykeyvnumber",
    L."latestFirmId" as "relatedentitykey",
    0 as "relatedentitykeyvnumber",
    'Firm' as "relatedcontacttype",  --client, vendor, broker (aon)
    'Lawyer-Firm' as "relationshiptype",
    NULL as "description",
    TO_CHAR(EV."eventDate",'YYYYMMDD') as "startdate",
    NULL as "enddate", --default to NULL
    NULL as "conversionserver",
    NULL as "sequencenumber"
  from "Lawyer" L
    left join "Entity" E on E."entityId" = L."lawyerId"
      left join "Event" EV on EV."eventId" = E."eventId"
;
select * from "ISI_ContactRelationshipView";

--******************************************************************************
select * from "Contact";
select * from "Entity";
select * from "Person" where "homeTelephone" is not NULL;
select * from "Person" where "workTelephone" is not NULL;
select * from "Person" where "mobileTelephone" is not NULL;
select * from "Person" where "pager" is not NULL;
select * from "Person" where "homeEmail" is not NULL;
select * from "Person" where "email" is not NULL;
select * from "Location" where "email" is not NULL;
select * from "Lawyer";
select * from "Firm";
--******************************************************************************

--******************
--**  POLICY
--******************
-- p_policy
-- ISI_PolicyView
--  LawyerStateBar
--create or replace view "ISI_PolicyView" as 
  select
    P."policyId" as "conversionreference",
    'bogus' as "EOR"
  from "Policy" P
;
*/