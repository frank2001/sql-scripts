/*
-- ISI_LAW_extract_to_csv.sql
-- extracts csv files from LAW views that mimic ISI interim tables
--
-- assumptions:
--    > \\oewyn\import is mapped as drive Z:\ (using Samba)
--
--
-- created:	FZ	08/15/2017
-- updates:
--  11/01/2017  FZ  added deprecated columns since ISI not removing
--  11/02/2017  FZ  added p_policy csv creation
--
*/

--------------------------------------------------------------------------------
-- CSV and spooling setup
-- sets up csv file properly 

set sqlformat

set colsep '|'
set sqlprompt ''
set pagesize 0
set trimspool on
set headsep off
set pages 0
set feed off
set long 2000
set longchunksize 2000
--------------------------------------------------------------------------------

/*
-- COMMON
*/
-- e_contact

spool 'z:\e_contact.csv'
select "entitykey"||'|'||"entitykeyvnumber"||'|'||"sbuid"||'|'||"firstname"||'|'||"middlename"||'|'||"lastname"||'|'||"nameprefix"||'|'||"namesuffix"||'|'||"jobtitle"||'|'||"fullname"||'|'||"companyname"||'|'||"preferredhtc"||'|'||"isindividual"||'|'||"contacttype"||'|'||"birthdate"||'|'||"gender"||'|'||"maritalstatus"||'|'||"comments"||'|'||"conversionserver"||'|'||"sequencenumber"||'|'||"linkedentitykey"||'|'||"linkedentitykeyvnumber"||'|'||"ssn"||'|'||"hicn"||'|'||"fein"||'|'||"tin"||'|'||"preferredlanguage" from "ISI_ContactView";
--select * from "ISI_ContactView";
spool off

-- e_address
spool 'z:\e_address.csv'
select "addresskey"||'|'||"entitykey"||'|'||"entitykeyvnumber"||'|'||"entitytype"||'|'||"sbuid"||'|'||"addresskind"||'|'||"mailingaddressind"||'|'||"addresstype"||'|'||"incareof"||'|'||"addressname"||'|'||"buildingname"||'|'||"pobox"||'|'||"street"||'|'||"addressline2"||'|'||"unitdesignator"||'|'||"unitnumber"||'|'||"countyname"||'|'||"city"||'|'||"regioncode"||'|'||"regionname"||'|'||"zippostal"||'|'||"countrycode"||'|'||"countryname"||'|'||"modifieddate" from "ISI_AddressView";
spool off 

-- e_phonenumbers
spool 'z:\e_phonenumbers.csv'
select "contactmethodkey"||'|'||"entitykey"||'|'||"entitykeyvnumber"||'|'||"entitytype"||'|'||"sbuid"||'|'||"primaryind"||'|'||"htctype"||'|'||"areacode"||'|'||"phonenumber"||'|'||"extension"||'|'||"formattedhtc"||'|'||"conversionserver"||'|'||"sequencenumber" from "ISI_PhoneView";
spool off

-- e_emailandwebsites
spool 'z:\e_emailandwebsites.csv'
select "contactmethodkey"||'|'||"entitykey"||'|'||"entitykeyvnumber"||'|'||"entitytype"||'|'||"sbuid"||'|'||"primaryind"||'|'||"htctype"||'|'||"emailandweb"||'|'||"conversionserver"||'|'||"sequencenumber" from "ISI_EmailWebsiteView";
spool off

-- e_license
spool 'z:\e_license.csv'
select "licensekey"||'|'||"entitykey"||'|'||"entitykeyvnumber"||'|'||"licensetype"||'|'||"licensecountry"||'|'||"licensejurisdiction"||'|'||"licenseclass"||'|'||"licensenumber"||'|'||"licensedate"||'|'||"currentind"||'|'||"licensestatus"||'|'||"licensestatus_reason"||'|'||"licensestatus_startdate"||'|'||"licensestatus_enddate"||'|'||"specialtrainingind"||'|'||"specialtrainingdate" from "ISI_LicenseView";
spool off

-- e_contactaccounts
spool 'z:\e_contactaccounts.csv'
select "entitykey"||'|'||"entitykeyvnumber"||'|'||"entitytype"||'|'||"sbuid"||'|'||"primaryind"||'|'||"accounttype"||'|'||"institutionnumber"||'|'||"accountnumber"||'|'||"billingdaypreferred"||'|'||"withdrawaltypecd"||'|'||"withdrawalcurrencycd"||'|'||"accountholdername"||'|'||"transitnumber"||'|'||"accountexpirymonth"||'|'||"accountexpiryyear"||'|'||"banknumber"||'|'||"secondaccountholdername"||'|'||"conversionserver"||'|'||"sequencenumber"||'|'||"description"||'|'||"creditcardflag"||'|'||"accountusage"||'|'||"branchcontactentitykey"||'|'||"branchcontactentitykeyvnumber" from "ISI_ContactAccountsView";
spool off 

-- ct_lawyercontactdtls_1
spool 'z:\ct_lawyercontactdtls_1.csv'
select "entitykey122"||'|'||"vnumber122"||'|'||"sbuid"||'|'||"inpracticedate" from "ISI_LawyerContactDetailsView";
spool off


-- VENDOR
-- e_vendor
spool 'z:\e_vendor.csv'
select "mappingid"||'|'||"mappingvnumber"||'|'||"entitykey"||'|'||"entitykeyvnumber"||'|'||"previousvendorkey"||'|'||"businessnumber"||'|'||"status"||'|'||"inceptiondate"||'|'||"arap_glaccountmappingid"||'|'||"profilecreditlimit"||'|'||"profilecreditterms_days"||'|'||"sbuid"||'|'||"description"||'|'||"contactentitykey"||'|'||"contactentitykeyvnumber"||'|'||"defaultaddresskey"||'|'||"defaultcontactmethodkey"||'|'||"conversionserver"||'|'||"sequencenumber"||'|'||"vendortype"||'|'||"taxid"||'|'||"taxexemptind"||'|'||"corporationind"||'|'||"foreignentityind"||'|'||"screenkey"||'|'||"settlementmethod"||'|'||"deliverymethod"||'|'||"deliveryaddresskey"||'|'||"deliverymethodkey"||'|'||"separatechequeind" from "ISI_VendorView";
spool off


-- CLIENT
-- e_client
spool 'z:\e_client.csv'   
select "mappingid"||'|'||"mappingvnumber"||'|'||"entitykey"||'|'||"entitykeyvnumber"||'|'||"previousentitykey"||'|'||"clientstatus"||'|'||"inceptiondate"||'|'||"continuoussince"||'|'||"clientcustomformattedname"||'|'||"ar_glaccountmappingid"||'|'||"altkey"||'|'||"preferredhtc"||'|'||"vipind"||'|'||"employeeind"||'|'||"donotacceptchequesind"||'|'||"sbuid"||'|'||"comments"||'|'||"contactentitykey1"||'|'||"contactentitykeyvnumber1"||'|'||"contactentitykey2"||'|'||"contactentitykeyvnumber2"||'|'||"contactentitykey3"||'|'||"contactentitykeyvnumber3"||'|'||"contactentitykey4"||'|'||"contactentitykeyvnumber4"||'|'||"contactentitykey5"||'|'||"contactentitykeyvnumber5"||'|'||"defaultaddresskey"||'|'||"defaultcontactmethodkey"||'|'||"deliverymethod"||'|'||"deliveryaddresskey"||'|'||"deliverymethodkey"||'|'||"clienttype"||'|'||"settlementmethod"||'|'||"conversionserver"||'|'||"sequencenumber"||'|'||"screenkey" from "ISI_ClientView";
 --      "mappingid"||'|'||"mappingvnumber"||'|'||"entitykey"||'|'||"entitykeyvnumber"||'|'||"previousentitykey"||'|'||"clientstatus"||'|'||"inceptiondate"||'|'||"continuoussince"||'|'||"clientcustomformattedname"||'|'||"ar_glaccountmappingid"||'|'||"altkey"||'|'||"preferredhtc"||'|'||"vipind"||'|'||"employeeind"||'|'||"donotacceptchequesind"||'|'||"sbuid"||'|'||"comments"||'|'||"contactentitykey1"||'|'||"contactentitykeyvnumber1"||'|'||"contactentitykey2"||'|'||"contactentitykeyvnumber2"||'|'||"contactentitykey3"||'|'||"contactentitykeyvnumber3"||'|'||"contactentitykey4"||'|'||"contactentitykeyvnumber4"||'|'||"contactentitykey5"||'|'||"contactentitykeyvnumber5"||'|'||"defaultaddresskey"||'|'||"defaultcontactmethodkey"||'|'||"deliverymethod"||'|'||"deliveryaddresskey"||'|'||"deliverymethodkey"||'|'||"clienttype"||'|'||"settlementmethod"||'|'||"conversionserver"||'|'||"sequencenumber"||'|'||"screenkey" from "ISI_ClientView";
spool off

-- e_clientstaff
spool 'z:\e_clientstaff.csv'
select "cliententitykey"||'|'||"cliententitykeyvnumber"||'|'||"clientstaffkey"||'|'||"clientstaffkeyvnumber"||'|'||"sbuid"||'|'||"comments"||'|'||"contactentitykey"||'|'||"contactentitykeyvnumber"||'|'||"status" from "ISI_ClientStaffView";
spool off

-- cl_clientdetails_1
spool 'z:\cl_clientdetails_1.csv'
select "entitykey"||'|'||"entitykeyvnumber"||'|'||"sbuid"||'|'||"contactwhenabsent"||'|'||"nonattorneystaff"||'|'||"organizationstructure"||'|'||"yearsestablished" from "ISI_ClientDetails1View";
spool off




/*
-- POLICY
*/
-- p_policy
spool 'z:\p_policy.csv'
--select "conversionreference"||'|'||"transactiontype"||'|'||"transactioneffectivedate"||'|'||"transactionseqno"||'|'||"pcmmappingkey"||'|'||"sbuid"||'|'||"inceptiondate"||'|'||"policyeffectivedate"||'|'||"policyexpirydate"||'|'||"veffectivedate"||'|'||"vexpirationdate"||'|'||"veffectivetime"||'|'||"vexpirytime"||'|'||"policykey"||'|'||"previouspolicykey"||'|'||"alternatepolicykey"||'|'||"agentbrokerpolicykey"||'|'||"binderkey"||'|'||"policynextunderwritedate"||'|'||"cliententitymappingkey"||'|'||"renewaltype"||'|'||"termlengthind"||'|'||"termlengthindunit"||'|'||"billingtype"||'|'||"papind"||'|'||"paymentplan"||'|'||"planeffdate"||'|'||"planexpdate"||'|'||"subscriptionind"||'|'||"facultativeind"||'|'||"premiumcurrencycd"||'|'||"limitcurrencycd"||'|'||"sbuofficekey"||'|'||"underwriterentitymappingkey"||'|'||"priorperiodind"||'|'||"cancellationtype"||'|'||"canreturnpremium"||'|'||"canearnedpremium"||'|'||"reinstateexpirydate"||'|'||"conversionserver"||'|'||"sequencenumber"||'|'||"relatedrevrcatranstype"||'|'||"relatedrevrcatranseffdate"||'|'||"relatedrevrcatranseqno"||'|'||"claimsmadeind"||'|'||"retrodate"||'|'||"extendreporteffdate"||'|'||"extendreportexpdate"||'|'||"vreasoncode"||'|'||"ventrydatetime"||'|'||"vusermappingkey"||'|'||"postedyearmth"||'|'||"policysourceind"||'|'||"assumedcompanymappingkey"||'|'||"writingcompanymappingkey"||'|'||"freetradezoneriskcode"||'|'||"languagecd"||'|'||"overridereasoncd"||'|'||"overridereasonnote"||'|'||"postedwweobjectid"||'|'||"postedwwevnumber"||'|'||"transactionstatus"||'|'||"nextfollowupdate"||'|'||"finaladjustmentind"||'|'||"rateeditionname"||'|'||"rateeditionversion"||'|'||"wipstatus"||'|'||"quoterelatedtranstype"||'|'||"relatedtranstype"||'|'||"relatedtranseffdate"||'|'||"relatedtransseqno"||'|'||"quotekey"||'|'||"quotedate"||'|'||"quotedaystoaccept"||'|'||"quoteexpirydate"||'|'||"quotedefinedname"||'|'||"quoteseriescode"||'|'||"quotestatus"||'|'||"quotedocumentsprinted"||'|'||"brkwritingcompanymappingkey"||'|'||"brokeragepolicycommission"||'|'||"quickquoteind"||'|'||"referralstatus"||'|'||"documentcategory"||'|'||"frontcompanymappingkey"||'|'||"assumedcommissionamount"||'|'||"assumedcommissiontype"||'|'||"policycompositetype"||'|'||"predominantjusrisdiction"||'|'||"predominantpolicyreion"||'|'||"totalwaivedpremium"||'|'||"accountingdate"||'|'||"quotedeclinedby"||'|'||"quotecompetitor"||'|'||"quotecompetitorpremium"||'|'||"policycontactentitykey"||'|'||"policycontactentitykeyvnumber"||'|'||"comments"||'|'||"renewalstatus"||'|'||"insurancestatus"||'|'||"liabilitylimit"||'|'||"overridemailingaddrkey"||'|'||"auditreportingind"||'|'||"convertedropolicytrneffdate"||'|'||"convertedtopolicytrneqno"||'|'||"quotecompletedreasoncode"||'|'||"quotecompletedreasonnotes"||'|'||"quotelastmodifieddatetime"||'|'||"extendreporttermlength"||'|'||"extendreportlengthunit"||'|'||"useextendreportexpoverrideind" from "ISI_PolicyView";
select "conversionreference"||'|'||"transactiontype"||'|'||"transactioneffectivedate"||'|'||"transactionseqno"||'|'||"pcmmappingkey"||'|'||"sbuid"||'|'||"inceptiondate"||'|'||"policyeffectivedate"||'|'||"policyexpirydate"||'|'||"veffectivedate"||'|'||"vexpirydate"||'|'||"veffectivetime"||'|'||"vexpirytime"||'|'||"policykey"||'|'||"previouspolicykey"||'|'||"alternatepolicykey"||'|'||"agentbrokerpolicykey"||'|'||"binderkey"||'|'||"policynextunderwritedate"||'|'||"cliententitymappingkey"||'|'||"renewaltype"||'|'||"termlengthind"||'|'||"termlengthindunit"||'|'||"billingtype"||'|'||"papind"||'|'||"paymentplan"||'|'||"planeffdate"||'|'||"planexpdate"||'|'||"subscriptionind"||'|'||"facultativeind"||'|'||"premiumcurrencycd"||'|'||"limitcurrencycd"||'|'||"sbuofficekey"||'|'||"underwriterentitymappingkey"||'|'||"priorperiodind"||'|'||"cancellationtype"||'|'||"canreturnpremium"||'|'||"canearnedpremium"||'|'||"reinstateexpirydate"||'|'||"conversionserver"||'|'||"sequencenumber"||'|'||"relatedrevrcatranstype"||'|'||"relatedrevrcatranseffdate"||'|'||"relatedrevrcatranseqno"||'|'||"claimsmadeind"||'|'||"retrodate"||'|'||"extendreporteffdate"||'|'||"extendreportexpdate"||'|'||"vreasoncode"||'|'||"ventrydatetime"||'|'||"vusermappingkey"||'|'||"postedyearmth"||'|'||"policysourceind"||'|'||"assumedcompanymappingkey"||'|'||"writingcompanymappingkey"||'|'||"freetradezoneriskcode"||'|'||"languagecd"||'|'||"overridereasoncd"||'|'||"overridereasonnote"||'|'||"postedwweobjectid"||'|'||"postedwwevnumber"||'|'||"transactionstatus"||'|'||"nextfollowupdate"||'|'||"finaladjustmentind"||'|'||"rateeditionname"||'|'||"rateeditionversion"||'|'||"wipstatus"||'|'||"quoterelatedtranstype"||'|'||"relatedtranstype"||'|'||"relatedtranseffdate"||'|'||"relatedtransseqno"||'|'||"quotekey"||'|'||"quotedate"||'|'||"quotedaystoaccept"||'|'||"quoteexpirydate"||'|'||"quotedefinedname"||'|'||"quoteseriescode"||'|'||"quotestatus"||'|'||"quotedocumentsprinted"||'|'||"brkwritingcompanymappingkey"||'|'||"brokeragepolicycommission"||'|'||"quickquoteind"||'|'||"referralstatus"||'|'||"documentcategory"||'|'||"frontedcompanymappingkey"||'|'||"assumedcommissionamount"||'|'||"assumedcommissiontype"||'|'||"policycompositiontype"||'|'||"predominantjurisdiction"||'|'||"predominantpolicyregion"||'|'||"totalwaivedpremium"||'|'||"accountingdate"||'|'||"quotedeclinedby"||'|'||"quotecompetitor"||'|'||"quotecompetitorpremium"||'|'||"policycontactentitykey"||'|'||"policycontactentitykeyvnumber"||'|'||"comments"||'|'||"renewalstatus"||'|'||"insurancestatus"||'|'||"liabilitylimit"||'|'||"overridemailingaddrkey"||'|'||"auditreportingind"||'|'||"convertedropolicytrneffdate"||'|'||"convertedtopolicytrnseqno"||'|'||"quotecompletedreasoncode"||'|'||"quotecompletedreasonnotes"||'|'||"quotelastmodifieddatetime"||'|'||"extendreporttermlength"||'|'||"extendreportlengthunit"||'|'||"useextendreportexpoverrideind" from "ISI_Policy";
spool off
---  --- 03/19/2018 ON HOLD ---- UNTIL ISI PROVIDES MAPPINGS, SPECIAL CONSIDERATIONS
--spool 'z:\p_policy_quote.csv'
--select "conversionreference"||'|'||"transactiontype"||'|'||"transactioneffectivedate"||'|'||"transactionseqno"||'|'||"pcmmappingkey"||'|'||"sbuid"||'|'||"inceptiondate"||'|'||"policyeffectivedate"||'|'||"policyexpirydate"||'|'||"veffectivedate"||'|'||"vexpirationdate"||'|'||"veffectivetime"||'|'||"vexpirytime"||'|'||"policykey"||'|'||"previouspolicykey"||'|'||"alternatepolicykey"||'|'||"agentbrokerpolicykey"||'|'||"binderkey"||'|'||"policynextunderwritedate"||'|'||"cliententitymappingkey"||'|'||"renewaltype"||'|'||"termlengthind"||'|'||"termlengthindunit"||'|'||"billingtype"||'|'||"papind"||'|'||"paymentplan"||'|'||"planeffdate"||'|'||"planexpdate"||'|'||"subscriptionind"||'|'||"facultativeind"||'|'||"premiumcurrencycd"||'|'||"limitcurrencycd"||'|'||"sbuofficekey"||'|'||"underwriterentitymappingkey"||'|'||"priorperiodind"||'|'||"cancellationtype"||'|'||"canreturnpremium"||'|'||"canearnedpremium"||'|'||"reinstateexpirydate"||'|'||"conversionserver"||'|'||"sequencenumber"||'|'||"relatedrevrcatranstype"||'|'||"relatedrevrcatranseffdate"||'|'||"relatedrevrcatranseqno"||'|'||"claimsmadeind"||'|'||"retrodate"||'|'||"extendreporteffdate"||'|'||"extendreportexpdate"||'|'||"vreasoncode"||'|'||"ventrydatetime"||'|'||"vusermappingkey"||'|'||"postedyearmth"||'|'||"policysourceind"||'|'||"assumedcompanymappingkey"||'|'||"writingcompanymappingkey"||'|'||"freetradezoneriskcode"||'|'||"languagecd"||'|'||"overridereasoncd"||'|'||"overridereasonnote"||'|'||"postedwweobjectid"||'|'||"postedwwevnumber"||'|'||"transactionstatus"||'|'||"nextfollowupdate"||'|'||"finaladjustmentind"||'|'||"rateeditionname"||'|'||"rateeditionversion"||'|'||"wipstatus"||'|'||"quoterelatedtranstype"||'|'||"relatedtranstype"||'|'||"relatedtranseffdate"||'|'||"relatedtransseqno"||'|'||"quotekey"||'|'||"quotedate"||'|'||"quotedaystoaccept"||'|'||"quoteexpirydate"||'|'||"quotedefinedname"||'|'||"quoteseriescode"||'|'||"quotestatus"||'|'||"quotedocumentsprinted"||'|'||"brkwritingcompanymappingkey"||'|'||"brokeragepolicycommission"||'|'||"quickquoteind"||'|'||"referralstatus"||'|'||"documentcategory"||'|'||"frontcompanymappingkey"||'|'||"assumedcommissionamount"||'|'||"assumedcommissiontype"||'|'||"policycompositetype"||'|'||"predominantjusrisdiction"||'|'||"predominantpolicyreion"||'|'||"totalwaivedpremium"||'|'||"accountingdate"||'|'||"quotedeclinedby"||'|'||"quotecompetitor"||'|'||"quotecompetitorpremium"||'|'||"policycontactentitykey"||'|'||"policycontactentitykeyvnumber"||'|'||"comments"||'|'||"renewalstatus"||'|'||"insurancestatus"||'|'||"liabilitylimit"||'|'||"overridemailingaddrkey"||'|'||"auditreportingind"||'|'||"convertedropolicytrneffdate"||'|'||"convertedtopolicytrneqno"||'|'||"quotecompletedreasoncode"||'|'||"quotecompletedreasonnotes"||'|'||"quotelastmodifieddatetime" from "ISI_QuoteView";
--spool off

-- p_billingdetail
spool 'z:\p_billingdetail.csv'
select "conversionreference"||'|'||"sbuid"||'|'||"paymentplan"||'|'||"planeffdate"||'|'||"planexpdate"||'|'||"policyeffectivedate"||'|'||"policyexpirydate"||'|'||"payplantype"||'|'||"paymentmethodentitykey"||'|'||"fees"||'|'||"totalamount"||'|'||"grossamount"||'|'||"preferredbillday"||'|'||"maximumterms"||'|'||"unit"||'|'||"feesondownpaymentind"||'|'||"downpaymenttype"||'|'||"downpaymentvalue"||'|'||"downpaymentcurrency"||'|'||"financetype"||'|'||"financevalue"||'|'||"financecurrency"||'|'||"lagdays"||'|'||"roundondownpaymentind"||'|'||"numpayments"||'|'||"numpaymentsleft"||'|'||"outstandingamount"||'|'||"outstandinggrossamount"||'|'||"activeind"||'|'||"authorizationdate"||'|'||"downpaymentreceivedind"||'|'||"invoicenumber"||'|'||"invoicedate"||'|'||"invoicecontactentitykey"||'|'||"invoiceaddresskey"||'|'||"conversionsever"||'|'||"sequencenumber"||'|'||"postedwweobjectid"||'|'||"invoicecontactentityvnumber"||'|'||"premfinancecontactentitykey"||'|'||"premfinancecontactentkeyvnum"||'|'||"premfinanceamount"||'|'||"premfinancecontractdate"||'|'||"overridefeesind"||'|'||"nocissuedate"||'|'||"directbillmailto"||'|'||"taxchargeref" from "ISI_BillingDetailView";
spool off
   
-- p_insureds
spool 'z:\p_insuredsFIRM.csv'
select "conversionreference"||'|'||"transactiontype"||'|'||"transactioneffectivedate"||'|'||"transactionseqno"||'|'||"sbuid"||'|'||"insuredid"||'|'||"insuredrole"||'|'||"contactentitykey"||'|'||"contactentityvnumber"||'|'||"defaultaddresskey"||'|'||"defaultcontactmethodkey"||'|'||"conversionserver"||'|'||"sequencenumber"||'|'||"postedwweobjectid"||'|'||"ouid" from "ISI_InsuredsFirmView";
spool off
spool 'z:\p_insuredsLAWYER.csv'
select "conversionreference"||'|'||"transactiontype"||'|'||"transactioneffectivedate"||'|'||"transactionseqno"||'|'||"sbuid"||'|'||"insuredid"||'|'||"insuredrole"||'|'||"contactentitykey"||'|'||"contactentityvnumber"||'|'||"defaultaddresskey"||'|'||"defaultcontactmethodkey"||'|'||"conversionserver"||'|'||"sequencenumber"||'|'||"postedwweobjectid"||'|'||"ouid" from "ISI_InsuredsLawyerView";
spool off
spool 'z:\p_insuredsLAWYERERP.csv'
select "conversionreference"||'|'||"transactiontype"||'|'||"transactioneffectivedate"||'|'||"transactionseqno"||'|'||"sbuid"||'|'||"insuredid"||'|'||"insuredrole"||'|'||"contactentitykey"||'|'||"contactentityvnumber"||'|'||"defaultaddresskey"||'|'||"defaultcontactmethodkey"||'|'||"conversionserver"||'|'||"sequencenumber"||'|'||"postedwweobjectid"||'|'||"ouid" from "ISI_InsuredsLawyerERPView";
spool off

-- p_items
spool 'z:\p_itemsFIRM.csv'
select  "conversionreference"||'|'||"transactiontype"||'|'||"transactioneffectivedate"||'|'||"transactionseqno"||'|'||"pcmitemmappingkey"||'|'||"pcmfloatermappingkey"||'|'||"sbuid"||'|'||"packageid"||'|'||"riskid"||'|'||"locationnumber"||'|'||"riskgroup"||'|'||"commissionamt"||'|'||"commissioncalcamt"||'|'||"commissiontype"||'|'||"deletedrow"||'|'||"conversionserver"||'|'||"sequencenumber"||'|'||"veffectivedate"||'|'||"vexpirydate"||'|'||"formcode"||'|'||"claimsmadeind"||'|'||"retrodate"||'|'||"postedwweobjectid"||'|'||"overridereasoncd"||'|'||"overridereasonnote"||'|'||"billbyitemind"||'|'||"taxamount"||'|'||"parentriskid"||'|'||"buildingnumber"||'|'||"occupancynumber"||'|'||"rateeditionname"||'|'||"rateeditionversion"||'|'||"vehicleuse"||'|'||"accumulationcode"||'|'||"ouid"||'|'||"extendreporteffdate"||'|'||"extendreportexpdate"||'|'||"extendreporttermlength"||'|'||"extendreportlengthunit" from "ISI_ItemFirmView";
spool off
spool 'z:\p_itemsLAWYER.csv'
select  "conversionreference"||'|'||"transactiontype"||'|'||"transactioneffectivedate"||'|'||"transactionseqno"||'|'||"pcmitemmappingkey"||'|'||"pcmfloatermappingkey"||'|'||"sbuid"||'|'||"packageid"||'|'||"riskid"||'|'||"locationnumber"||'|'||"riskgroup"||'|'||"commissionamt"||'|'||"commissioncalcamt"||'|'||"commissiontype"||'|'||"deletedrow"||'|'||"conversionserver"||'|'||"sequencenumber"||'|'||"veffectivedate"||'|'||"vexpirydate"||'|'||"formcode"||'|'||"claimsmadeind"||'|'||"retrodate"||'|'||"postedwweobjectid"||'|'||"overridereasoncd"||'|'||"overridereasonnote"||'|'||"billbyitemind"||'|'||"taxamount"||'|'||"parentriskid"||'|'||"buildingnumber"||'|'||"occupancynumber"||'|'||"rateeditionname"||'|'||"rateeditionversion"||'|'||"vehicleuse"||'|'||"accumulationcode"||'|'||"ouid"||'|'||"extendreporteffdate"||'|'||"extendreportexpdate"||'|'||"extendreporttermlength"||'|'||"extendreportlengthunit" from "ISI_ItemLawyerView";
spool off

-- i_lawfirm_1
spool 'z:\i_lawfirm_1.csv'
select "conversionreference"||'|'||"transactioneffectivedate"||'|'||"transactionseqno"||'|'||"transactiontype"||'|'||"sbuid"||'|'||"packageid"||'|'||"riskid"||'|'||"abstracterentity"||'|'||"abstracterind"||'|'||"abstracterinscompagentind"||'|'||"abstracterownfirmentitylabel"||'|'||"abstracterrevenue"||'|'||"affiliationind"||'|'||"aopfactorschedadj"||'|'||"attorneydisbarredind"||'|'||"attorneydisbarredinfo"||'|'||"attorneyerrorind"||'|'||"attorneygrievanceind"||'|'||"calcpremiumxs5m"||'|'||"claimsratioactloss10_1"||'|'||"claimsratioactloss1_1"||'|'||"claimsratioactloss3_1"||'|'||"claimsratioactloss5_1"||'|'||"claimsratioactlossall_1"||'|'||"claimsratioactratio10_1"||'|'||"claimsratioactratio1_1"||'|'||"claimsratioactratio3_1"||'|'||"claimsratioactratio5_1"||'|'||"claimsratioactratioall_1"||'|'||"claimsratioincloss10_1"||'|'||"claimsratioincloss1_1"||'|'||"claimsratioincloss3_1"||'|'||"claimsratioincloss5_1"||'|'||"claimsratioinclossall_1"||'|'||"claimsratioincratio10_1"||'|'||"claimsratioincratio1_1"||'|'||"claimsratioincratio3_1"||'|'||"claimsratioincratio5_1"||'|'||"claimsratioincratioall_1"||'|'||"claimsratioprem10_1"||'|'||"claimsratioprem1_1"||'|'||"claimsratioprem3_1"||'|'||"claimsratioprem5_1"||'|'||"claimsratiopremall_1"||'|'||"claimsurcharge"||'|'||"claimsurchargecreditmax"||'|'||"claimsurchargecreditmin"||'|'||"claimsurchargecreditrange"||'|'||"claimsurchargedebitmax"||'|'||"claimsurchargedebitmin"||'|'||"claimsurchargedebitrange"||'|'||"conditionalmandatoryfields"||'|'||"contactwhenabsent"||'|'||"contingencyfeeind"||'|'||"continuityfactor"||'|'||"covind_wlmend02"||'|'||"covind_wlmend03"||'|'||"covind_wlmend22"||'|'||"covind_wlmend23"||'|'||"covind_wlmend24"||'|'||"covind_wlmend25"||'|'||"covind_wlmend27"||'|'|| "covind_wlmendmn"||'|'||"facpremiumxs10m"||'|'||"facpremiumxs5m"||'|'||"financialinterestclientind"||'|'||"firmname"||'|'||"firmsize"||'|'||"firmsizefactor"||'|'||"firmsizeforrating"||'|'||"foreignclientcountries"||'|'||"foreignclientind"||'|'||"insurepastworkind"||'|'||"licensingagreementind"||'|'||"licensingagreementpct"||'|'||"localaffiliateind"||'|'||"nonattorneystaff"||'|'||"officerclientind"||'|'||"officesharingind"||'|'||"oneclient10pctind"||'|'||"organizationstructure"||'|'||"otherdebit"||'|'||"otherdebitcreditmax"||'|'||"otherdebitcreditmin"||'|'||"otherdebitcreditrange"||'|'||"otherdebitdebitmax"||'|'||"otherdebitdebitmin"||'|'||"otherdebitdebitrange"||'|'||"otherdebitexplain"||'|'||"outofstateind"||'|'||"outsoursepaymentind"||'|'||"outsoursesearchind"||'|'||"percentipcatcopyright"||'|'||"percentipcatdomestic"||'|'||"percentipcatforeign"||'|'||"percentipcatinfringement"||'|'||"percentipcatintproperty"||'|'||"percentipcatother"||'|'||"percentipcatotherexplain"||'|'||"percentipcattrademark"||'|'||"percentiptypebiotechnology"||'|'||"percentiptypebusiness"||'|'||"percentiptypechemical"||'|'||"percentiptypecomputer"||'|'||"percentiptypeelectrical"||'|'||"percentiptypemechanical"||'|'||"percentiptypeother"||'|'||"percentiptypeotherexplain"||'|'||"predecessorfirmsnotapplicab_1"||'|'||"prevlawfirmind"||'|'||"priordeclinedind"||'|'||"priordeclinedinfo"||'|'||"priortailind"||'|'||"priortailinfo"||'|'||"reinsurancepremium1m"||'|'||"reinsurancepremium1mfactor"||'|'||"restrictprioractsind"||'|'||"restrictprioractsinfo"||'|'||"scheduledadjustmentstotal"||'|'||"severityindex"||'|'||"severityindexcreditmax"||'|'||"severityindexcreditmin"||'|'||"severityindexcreditrange"||'|'||"severityindexdebitmax"||'|'||"severityindexdebitmin"||'|'||"severityindexdebitrange"||'|'||"severityindexzero"||'|'||"severityindexzerocreditmax"||'|'||"severityindexzerocreditmin"||'|'||"severityindexzerocreditrange"||'|'||"severityindexzerodebitmax"||'|'||"severityindexzerodebitmin"||'|'||"severityindexzerodebitrange"||'|'||"tailfactorunlimited"||'|'||"tailfactoryear_1"||'|'||"tailfactoryear_2"||'|'||"tailfactoryear_3"||'|'||"tailfactoryear_6"||'|'||"tailpremiumunlimited"||'|'||"tailpremiumyear_1"||'|'||"tailpremiumyear_2"||'|'||"tailpremiumyear_3"||'|'||"tailpremiumyear_6"||'|'||"thirdpartydocketind"||'|'||"thirdpartydocketsystem"||'|'||"wlmendmn_body"||'|'||"wlmendmn_code"||'|'||"wlmendmn_desc"||'|'||"yearsestablished"||'|'||"yearsinsured"||'|'||"conversionserver"||'|'||"sequencenumber" from "ISI_LawFirm1View";
--R1..select "conversionreference"||'|'||"transactioneffectivedate"||'|'||"transactionseqno"||'|'||"transactiontype"||'|'||"sbuid"||'|'||"packageid"||'|'||"riskid"||'|'||"abstracterentity"||'|'||"abstracterinscompagentind"||'|'||"abstracterownfirmentitylabel"||'|'||"abstracterrevenue"||'|'||"affiliationind"||'|'||"aopfactorschedadj"||'|'||"attorneydisbarredind"||'|'||"attorneydisbarredinfo"||'|'||"attorneyerrorind"||'|'||"attorneygrievanceind"||'|'||"claimsratioactloss10_1"||'|'||"claimsratioactloss1_1"||'|'||"claimsratioactloss3_1"||'|'||"claimsratioactloss5_1"||'|'||"claimsratioactlossall_1"||'|'||"claimsratioactratio10_1"||'|'||"claimsratioactratio1_1"||'|'||"claimsratioactratio3_1"||'|'||"claimsratioactratio5_1"||'|'||"claimsratioactratioall_1"||'|'||"claimsratioincloss10_1"||'|'||"claimsratioincloss1_1"||'|'||"claimsratioincloss3_1"||'|'||"claimsratioincloss5_1"||'|'||"claimsratioinclossall_1"||'|'||"claimsratioincratio10_1"||'|'||"claimsratioincratio1_1"||'|'||"claimsratioincratio3_1"||'|'||"claimsratioincratio5_1"||'|'||"claimsratioincratioall_1"||'|'||"claimsratioprem10_1"||'|'||"claimsratioprem1_1"||'|'||"claimsratioprem3_1"||'|'||"claimsratioprem5_1"||'|'||"claimsratiopremall_1"||'|'||"claimsurcharge"||'|'||"claimsurchargecreditmax"||'|'||"claimsurchargecreditmin"||'|'||"claimsurchargecreditrange"||'|'||"claimsurchargedebitmax"||'|'||"claimsurchargedebitmin"||'|'||"claimsurchargedebitrange"||'|'||"conditionalmandatoryfields"||'|'||"contingencyfeeind"||'|'||"continuityfactor"||'|'||"covind_wlmend02"||'|'||"covind_wlmend03"||'|'||"covind_wlmend22"||'|'||"covind_wlmend23"||'|'||"covind_wlmend24"||'|'||"covind_wlmend25"||'|'||"covind_wlmend27"||'|'||"financialinterestclientind"||'|'||"firmname"||'|'||"firmsize"||'|'||"firmsizefactor"||'|'||"firmsizeforrating"||'|'||"firmwebsite"||'|'||"foreignclientcountries"||'|'||"foreignclientind"||'|'||"licensingagreementind"||'|'||"licensingagreementpct"||'|'||"localaffiliateind"||'|'||"nonattorneystaff"||'|'||"officerclientind"||'|'||"officesharingind"||'|'||"oneclient10pctind"||'|'||"outofstateind"||'|'||"outsoursepaymentind"||'|'||"outsoursesearchind"||'|'||"percentipcatcopyright"||'|'||"percentipcatdomestic"||'|'||"percentipcatforeign"||'|'||"percentipcatinfringement"||'|'||"percentipcatintproperty"||'|'||"percentipcatother"||'|'||"percentipcatotherexplain"||'|'||"percentipcattrademark"||'|'||"percentiptypebiotechnology"||'|'||"percentiptypebusiness"||'|'||"percentiptypechemical"||'|'||"percentiptypecomputer"||'|'||"percentiptypeelectrical"||'|'||"percentiptypemechanical"||'|'||"percentiptypeother"||'|'||"percentiptypeotherexplain"||'|'||"predecessorfirmsnotapplicab_1"||'|'||"prevlawfirmind"||'|'||"priordeclinedind"||'|'||"priordeclinedinfo"||'|'||"priortailind"||'|'||"priortailinfo"||'|'||"restrictprioractsind"||'|'||"restrictprioractsinfo"||'|'||"scheduledadjustmentstotal"||'|'||"severityindex"||'|'||"severityindexcreditmax"||'|'||"severityindexcreditmin"||'|'||"severityindexcreditrange"||'|'||"severityindexdebitmax"||'|'||"severityindexdebitmin"||'|'||"severityindexdebitrange"||'|'||"severityindexzero"||'|'||"severityindexzerocreditmax"||'|'||"severityindexzerocreditmin"||'|'||"severityindexzerocreditrange"||'|'||"severityindexzerodebitmax"||'|'||"severityindexzerodebitmin"||'|'||"severityindexzerodebitrange"||'|'||"tailfactorunlimited"||'|'||"tailfactoryear_1"||'|'||"tailfactoryear_2"||'|'||"tailfactoryear_3"||'|'||"tailfactoryear_6"||'|'||"tailpremiumunlimited"||'|'||"tailpremiumyear_1"||'|'||"tailpremiumyear_2"||'|'||"tailpremiumyear_3"||'|'||"tailpremiumyear_6"||'|'||"thirdpartydocketind"||'|'||"thirdpartydocketsystem"||'|'||"calcpremiumxs5m"||'|'||"facpremiumxs5m"||'|'||"facpremiumxs10m"||'|'||"yearsinsured"||'|'||"conversionpremiumrounding" from "ISI_LawFirm1View";
spool off

-- i_attorney_1
spool 'z:\i_attorney_1.csv'
select "conversionreference"||'|'||"transactioneffectivedate"||'|'||"transactionseqno"||'|'||"transactiontype"||'|'||"sbuid"||'|'||"packageid"||'|'||"riskid"||'|'||"attorneyaddedthisterm"||'|'||"attorneydesignation"||'|'||"attorneyemail"||'|'||"attorneyfactor"||'|'||"attorneyname"||'|'||"attorneynameid_ek"||'|'||"attorneynameid_vn"||'|'||"attorneynumber"||'|'||"clecredits"||'|'||"clecreditsfactor"||'|'||"currentleavestartdate"||'|'||"currentlyonleaveind"||'|'||"endleavethistransind"||'|'||"hoursworkedpermonth"||'|'||"inpracticedate"||'|'||"militaryleavetext"||'|'||"militaryleavetotaldays"||'|'||"newattorneyprioracts"||'|'||"otherlicenses"||'|'||"parttimecomment"||'|'||"parttimefactor"||'|'||"parttimefactoroverride"||'|'||"parttimefactorsys"||'|'||"parttimeoverridecomment"||'|'||"primarylicense"||'|'||"retrodatecomment"||'|'||"retrodatefactor"||'|'||"retrodateoverride"||'|'||"startleavethistransind"||'|'||"tailattorneyfactor"||'|'||"taildeductiblefactor"||'|'||"tailfactor"||'|'||"tailfactorunlimited"||'|'||"tailfactoryear_1"||'|'||"tailfactoryear_2"||'|'||"tailfactoryear_3"||'|'||"tailfactoryear_6"||'|'||"tailpremiumbefattorneyfactor"||'|'||"tailpremiumfactoredup"||'|'||"tailpremiumstart"||'|'||"tailpremiumtotal"||'|'||"tailpremiumunlimited"||'|'||"tailpremiumwodeductible"||'|'||"tailpremiumxs10m"||'|'||"tailpremiumxs5m"||'|'||"tailpremiumyear_1"||'|'||"tailpremiumyear_2"||'|'||"tailpremiumyear_3"||'|'||"tailpremiumyear_6"||'|'||"tailschedadjustfactor"||'|'||"yearinpracticecommentor"||'|'||"yearinpracticecommentsys"||'|'||"yearinpracticefactor"||'|'||"yearsinpracticefactoror"||'|'||"yearsinpracticefactorsys"||'|'||"yearsinpracticeor"||'|'||"yearsinpracticesys" from "ISI_Attorney1View";
spool off

-- p_coverage
spool 'z:\p_coverage_lpl_firm.csv'
--select "conversionreference"||'|'||"transactiontype"||'|'||"transactioneffectivedate"||'|'||"transactionseqno"||'|'||"pcmmappingkey"||'|'||"sbuid"||'|'||"packageid"||'|'||"riskid"||'|'||"linenumber"||'|'||"coverageid"||'|'||"basisofsettlement"||'|'||"coinsurance"||'|'||"rategroup"||'|'||"drivingrecord"||'|'||"rateper"||'|'||"baseratefactor"||'|'||"overrideratefactor"||'|'||"limit1"||'|'||"limit2"||'|'||"limitcurrencycd"||'|'||"deductible1"||'|'||"deductible2"||'|'||"premiumcurrencycd"||'|'||"ratedbasepremium"||'|'||"ratedgrosspremium"||'|'||"ratednetpremium"||'|'||"annualpremium"||'|'||"transactionpremium"||'|'||"nonpremiumsurchargeamount"||'|'||"premiumsurchargeamount"||'|'||"transactionsurcharge"||'|'||"nonpremiumdiscountamount"||'|'||"premiumdiscountamount"||'|'||"transactiondiscount"||'|'||"commissionamount"||'|'||"numberof1"||'|'||"valueof1"||'|'||"typeof1"||'|'||"numberof2"||'|'||"valueof2"||'|'||"typeof2"||'|'||"deletedrow"||'|'||"conversionserver"||'|'||"sequencenumber"||'|'||"veffectivedate"||'|'||"vexpirydate"||'|'||"claimsmadeind"||'|'||"retrodate"||'|'||"postedwweobjectid"||'|'||"limittierinfo"||'|'||"ratedftbasepremium"||'|'||"ratedftgrosspremium"||'|'||"ratedftpremium"||'|'||"ratedannualpremium"||'|'||"ratedtransactionpremium"||'|'||"ratedcommissionpercent"||'|'||"ratedcommissionamount"||'|'||"ftpremium"||'|'||"offsetftpremium"||'|'||"commissiontype"||'|'||"commissionpercent"||'|'||"transactionpremiumfactor"||'|'||"transactionpremiumdlyrate"||'|'||"overriderule"||'|'||"overrideftpremium"||'|'||"overrideannualpremium"||'|'||"overridetransactionpremium"||'|'||"overridecommissionpercent"||'|'||"overridecommissionamount"||'|'||"taxamount"||'|'||"gldiv"||'|'||"gldept"||'|'||"gllob"||'|'||"siretention1"||'|'||"siretention2"||'|'||"mappingcode"||'|'||"submappingcode"||'|'||"submappingcode2"||'|'||"netrate"||'|'||"grossrate"||'|'||"rateexposure"||'|'||"adjbilltypeind"||'|'||"waivedtransactionpremium"||'|'||"classcode"||'|'||"iso_statecode"||'|'||"iso_zipcode"||'|'||"iso_ratingterritory"||'|'||"iso_typeofpolicycode"||'|'||"iso_aslobstatcode"||'|'||"iso_sublinecode"||'|'||"iso_classificationcode"||'|'||"iso_coveragestatcode"||'|'||"iso_ratingidentificationcode"||'|'||"iso_constructioncode"||'|'||"iso_fireprotectioncode"||'|'||"iso_terrorismcoveragecode"||'|'||"iso_windhaildedcode"||'|'||"iso_bcegclass"||'|'||"iso_ratingbasiscode"||'|'||"iso_liabilityformcode"||'|'||"iso_molddamagecode"||'|'||"iso_liabilityexposureindcode"||'|'||"iso_exposurestatamount"||'|'||"iso_ratingmodificationfactor"||'|'||"iso_stateexceptionindcode"||'|'||"iso_bussincomeexpensecode"||'|'||"iso_liabcovindcode"||'|'||"iso_pctowneroccupied"||'|'||"iso_classcodedesc"||'|'||"rateeffectivedate"||'|'||"deductibletype"||'|'||"iso_formcode"||'|'||"iso_losscostmultiplier"||'|'||"iso_losscostdate"||'|'||"iso_yearofconstruction"||'|'||"ouid" from "ISI_CoverageLPLFirmView";
select "conversionreference"||'|'||"new_transactiontype"||'|'||"transactioneffectivedate"||'|'||"new_transactionseqno"||'|'||"pcmmappingkey"||'|'||"sbuid"||'|'||"packageid"||'|'||"riskid"||'|'||"linenumber"||'|'||"coverageid"||'|'||"basisofsettlement"||'|'||"coinsurance"||'|'||"rategroup"||'|'||"drivingrecord"||'|'||"rateper"||'|'||"baseratefactor"||'|'||"overrideratefactor"||'|'||"limit1"||'|'||"limit2"||'|'||"limitcurrencycd"||'|'||"deductible1"||'|'||"deductible2"||'|'||"premiumcurrencycd"||'|'||"ratedbasepremium"||'|'||"ratedgrosspremium"||'|'||"ratednetpremium"||'|'||"annualpremium"||'|'||"transactionpremium"||'|'||"nonpremiumsurchargeamount"||'|'||"premiumsurchargeamount"||'|'||"transactionsurcharge"||'|'||"nonpremiumdiscountamount"||'|'||"premiumdiscountamount"||'|'||"transactiondiscount"||'|'||"commissionamount"||'|'||"numberof1"||'|'||"valueof1"||'|'||"typeof1"||'|'||"numberof2"||'|'||"valueof2"||'|'||"typeof2"||'|'||"deletedrow"||'|'||"conversionserver"||'|'||"sequencenumber"||'|'||"veffectivedate"||'|'||"vexpirydate"||'|'||"claimsmadeind"||'|'||"retrodate"||'|'||"postedwweobjectid"||'|'||"limittierinfo"||'|'||"ratedftbasepremium"||'|'||"ratedftgrosspremium"||'|'||"ratedftpremium"||'|'||"ratedannualpremium"||'|'||"ratedtransactionpremium"||'|'||"ratedcommissionpercent"||'|'||"ratedcommissionamount"||'|'||"ftpremium"||'|'||"offsetftpremium"||'|'||"commissiontype"||'|'||"commissionpercent"||'|'||"transactionpremiumfactor"||'|'||"transactionpremiumdlyrate"||'|'||"overriderule"||'|'||"overrideftpremium"||'|'||"overrideannualpremium"||'|'||"overridetransactionpremium"||'|'||"overridecommissionpercent"||'|'||"overridecommissionamount"||'|'||"taxamount"||'|'||"gldiv"||'|'||"gldept"||'|'||"gllob"||'|'||"siretention1"||'|'||"siretention2"||'|'||"mappingcode"||'|'||"submappingcode"||'|'||"submappingcode2"||'|'||"netrate"||'|'||"grossrate"||'|'||"rateexposure"||'|'||"adjbilltypeind"||'|'||"waivedtransactionpremium"||'|'||"classcode"||'|'||"iso_statecode"||'|'||"iso_zipcode"||'|'||"iso_ratingterritory"||'|'||"iso_typeofpolicycode"||'|'||"iso_aslobstatcode"||'|'||"iso_sublinecode"||'|'||"iso_classificationcode"||'|'||"iso_coveragestatcode"||'|'||"iso_ratingidentificationcode"||'|'||"iso_constructioncode"||'|'||"iso_fireprotectioncode"||'|'||"iso_terrorismcoveragecode"||'|'||"iso_windhaildedcode"||'|'||"iso_bcegclass"||'|'||"iso_ratingbasiscode"||'|'||"iso_liabilityformcode"||'|'||"iso_molddamagecode"||'|'||"iso_liabilityexposureindcode"||'|'||"iso_exposurestatamount"||'|'||"iso_ratingmodificationfactor"||'|'||"iso_stateexceptionindcode"||'|'||"iso_bussincomeexpensecode"||'|'||"iso_liabcovindcode"||'|'||"iso_pctowneroccupied"||'|'||"iso_classcodedesc"||'|'||"rateeffectivedate"||'|'||"deductibletype"||'|'||"iso_formcode"||'|'||"iso_losscostmultiplier"||'|'||"iso_losscostdate"||'|'||"iso_yearofconstruction"||'|'||"ouid" from "ISI_CoverageLPLFirmAllView";
spool off
spool 'z:\p_coverage_lpl_lawyer.csv'
--select "conversionreference"||'|'||"transactiontype"||'|'||"transactioneffectivedate"||'|'||"transactionseqno"||'|'||"pcmmappingkey"||'|'||"sbuid"||'|'||"packageid"||'|'||"riskid"||'|'||"linenumber"||'|'||"coverageid"||'|'||"basisofsettlement"||'|'||"coinsurance"||'|'||"rategroup"||'|'||"drivingrecord"||'|'||"rateper"||'|'||"baseratefactor"||'|'||"overrideratefactor"||'|'||"limit1"||'|'||"limit2"||'|'||"limitcurrencycd"||'|'||"deductible1"||'|'||"deductible2"||'|'||"premiumcurrencycd"||'|'||"ratedbasepremium"||'|'||"ratedgrosspremium"||'|'||"ratednetpremium"||'|'||"annualpremium"||'|'||"transactionpremium"||'|'||"nonpremiumsurchargeamount"||'|'||"premiumsurchargeamount"||'|'||"transactionsurcharge"||'|'||"nonpremiumdiscountamount"||'|'||"premiumdiscountamount"||'|'||"transactiondiscount"||'|'||"commissionamount"||'|'||"numberof1"||'|'||"valueof1"||'|'||"typeof1"||'|'||"numberof2"||'|'||"valueof2"||'|'||"typeof2"||'|'||"deletedrow"||'|'||"conversionserver"||'|'||"sequencenumber"||'|'||"veffectivedate"||'|'||"vexpirydate"||'|'||"claimsmadeind"||'|'||"retrodate"||'|'||"postedwweobjectid"||'|'||"limittierinfo"||'|'||"ratedftbasepremium"||'|'||"ratedftgrosspremium"||'|'||"ratedftpremium"||'|'||"ratedannualpremium"||'|'||"ratedtransactionpremium"||'|'||"ratedcommissionpercent"||'|'||"ratedcommissionamount"||'|'||"ftpremium"||'|'||"offsetftpremium"||'|'||"commissiontype"||'|'||"commissionpercent"||'|'||"transactionpremiumfactor"||'|'||"transactionpremiumdlyrate"||'|'||"overriderule"||'|'||"overrideftpremium"||'|'||"overrideannualpremium"||'|'||"overridetransactionpremium"||'|'||"overridecommissionpercent"||'|'||"overridecommissionamount"||'|'||"taxamount"||'|'||"gldiv"||'|'||"gldept"||'|'||"gllob"||'|'||"siretention1"||'|'||"siretention2"||'|'||"mappingcode"||'|'||"submappingcode"||'|'||"submappingcode2"||'|'||"netrate"||'|'||"grossrate"||'|'||"rateexposure"||'|'||"adjbilltypeind"||'|'||"waivedtransactionpremium"||'|'||"classcode"||'|'||"iso_statecode"||'|'||"iso_zipcode"||'|'||"iso_ratingterritory"||'|'||"iso_typeofpolicycode"||'|'||"iso_aslobstatcode"||'|'||"iso_sublinecode"||'|'||"iso_classificationcode"||'|'||"iso_coveragestatcode"||'|'||"iso_ratingidentificationcode"||'|'||"iso_constructioncode"||'|'||"iso_fireprotectioncode"||'|'||"iso_terrorismcoveragecode"||'|'||"iso_windhaildedcode"||'|'||"iso_bcegclass"||'|'||"iso_ratingbasiscode"||'|'||"iso_liabilityformcode"||'|'||"iso_molddamagecode"||'|'||"iso_liabilityexposureindcode"||'|'||"iso_exposurestatamount"||'|'||"iso_ratingmodificationfactor"||'|'||"iso_stateexceptionindcode"||'|'||"iso_bussincomeexpensecode"||'|'||"iso_liabcovindcode"||'|'||"iso_pctowneroccupied"||'|'||"iso_classcodedesc"||'|'||"rateeffectivedate"||'|'||"deductibletype"||'|'||"iso_formcode"||'|'||"iso_losscostmultiplier"||'|'||"iso_losscostdate"||'|'||"iso_yearofconstruction"||'|'||"ouid" from "ISI_CoverageLPLLawyerView";
select "conversionreference"||'|'||"new_transactiontype"||'|'||"transactioneffectivedate"||'|'||"new_transactionseqno"||'|'||"pcmmappingkey"||'|'||"sbuid"||'|'||"packageid"||'|'||"riskid"||'|'||"linenumber"||'|'||"coverageid"||'|'||"basisofsettlement"||'|'||"coinsurance"||'|'||"rategroup"||'|'||"drivingrecord"||'|'||"rateper"||'|'||"baseratefactor"||'|'||"overrideratefactor"||'|'||"limit1"||'|'||"limit2"||'|'||"limitcurrencycd"||'|'||"deductible1"||'|'||"deductible2"||'|'||"premiumcurrencycd"||'|'||"ratedbasepremium"||'|'||"ratedgrosspremium"||'|'||"ratednetpremium"||'|'||"annualpremium"||'|'||"new_transactionpremium"||'|'||"nonpremiumsurchargeamount"||'|'||"premiumsurchargeamount"||'|'||"transactionsurcharge"||'|'||"nonpremiumdiscountamount"||'|'||"premiumdiscountamount"||'|'||"transactiondiscount"||'|'||"commissionamount"||'|'||"numberof1"||'|'||"valueof1"||'|'||"typeof1"||'|'||"numberof2"||'|'||"valueof2"||'|'||"typeof2"||'|'||"deletedrow"||'|'||"conversionserver"||'|'||"sequencenumber"||'|'||"veffectivedate"||'|'||"vexpirydate"||'|'||"claimsmadeind"||'|'||"retrodate"||'|'||"postedwweobjectid"||'|'||"limittierinfo"||'|'||"ratedftbasepremium"||'|'||"ratedftgrosspremium"||'|'||"ratedftpremium"||'|'||"ratedannualpremium"||'|'||"ratedtransactionpremium"||'|'||"ratedcommissionpercent"||'|'||"ratedcommissionamount"||'|'||"ftpremium"||'|'||"offsetftpremium"||'|'||"commissiontype"||'|'||"commissionpercent"||'|'||"transactionpremiumfactor"||'|'||"transactionpremiumdlyrate"||'|'||"overriderule"||'|'||"overrideftpremium"||'|'||"overrideannualpremium"||'|'||"overridetransactionpremium"||'|'||"overridecommissionpercent"||'|'||"overridecommissionamount"||'|'||"taxamount"||'|'||"gldiv"||'|'||"gldept"||'|'||"gllob"||'|'||"siretention1"||'|'||"siretention2"||'|'||"mappingcode"||'|'||"submappingcode"||'|'||"submappingcode2"||'|'||"netrate"||'|'||"grossrate"||'|'||"rateexposure"||'|'||"adjbilltypeind"||'|'||"waivedtransactionpremium"||'|'||"classcode"||'|'||"iso_statecode"||'|'||"iso_zipcode"||'|'||"iso_ratingterritory"||'|'||"iso_typeofpolicycode"||'|'||"iso_aslobstatcode"||'|'||"iso_sublinecode"||'|'||"iso_classificationcode"||'|'||"iso_coveragestatcode"||'|'||"iso_ratingidentificationcode"||'|'||"iso_constructioncode"||'|'||"iso_fireprotectioncode"||'|'||"iso_terrorismcoveragecode"||'|'||"iso_windhaildedcode"||'|'||"iso_bcegclass"||'|'||"iso_ratingbasiscode"||'|'||"iso_liabilityformcode"||'|'||"iso_molddamagecode"||'|'||"iso_liabilityexposureindcode"||'|'||"iso_exposurestatamount"||'|'||"iso_ratingmodificationfactor"||'|'||"iso_stateexceptionindcode"||'|'||"iso_bussincomeexpensecode"||'|'||"iso_liabcovindcode"||'|'||"iso_pctowneroccupied"||'|'||"iso_classcodedesc"||'|'||"rateeffectivedate"||'|'||"deductibletype"||'|'||"iso_formcode"||'|'||"iso_losscostmultiplier"||'|'||"iso_losscostdate"||'|'||"iso_yearofconstruction"||'|'||"ouid" from "ISI_CoverageLPLLawyerAllView";
spool off
spool 'z:\p_coverage_erp_lawyer.csv'
select "conversionreference"||'|'||"transactiontype"||'|'||"transactioneffectivedate"||'|'||"transactionseqno"||'|'||"pcmmappingkey"||'|'||"sbuid"||'|'||"packageid"||'|'||"riskid"||'|'||"linenumber"||'|'||"coverageid"||'|'||"basisofsettlement"||'|'||"coinsurance"||'|'||"rategroup"||'|'||"drivingrecord"||'|'||"rateper"||'|'||"baseratefactor"||'|'||"overrideratefactor"||'|'||"limit1"||'|'||"limit2"||'|'||"limitcurrencycd"||'|'||"deductible1"||'|'||"deductible2"||'|'||"premiumcurrencycd"||'|'||"ratedbasepremium"||'|'||"ratedgrosspremium"||'|'||"ratednetpremium"||'|'||"annualpremium"||'|'||"transactionpremium"||'|'||"nonpremiumsurchargeamount"||'|'||"premiumsurchargeamount"||'|'||"transactionsurcharge"||'|'||"nonpremiumdiscountamount"||'|'||"premiumdiscountamount"||'|'||"transactiondiscount"||'|'||"commissionamount"||'|'||"numberof1"||'|'||"valueof1"||'|'||"typeof1"||'|'||"numberof2"||'|'||"valueof2"||'|'||"typeof2"||'|'||"deletedrow"||'|'||"conversionserver"||'|'||"sequencenumber"||'|'||"veffectivedate"||'|'||"vexpirydate"||'|'||"claimsmadeind"||'|'||"retrodate"||'|'||"postedwweobjectid"||'|'||"limittierinfo"||'|'||"ratedftbasepremium"||'|'||"ratedftgrosspremium"||'|'||"ratedftpremium"||'|'||"ratedannualpremium"||'|'||"ratedtransactionpremium"||'|'||"ratedcommissionpercent"||'|'||"ratedcommissionamount"||'|'||"ftpremium"||'|'||"offsetftpremium"||'|'||"commissiontype"||'|'||"commissionpercent"||'|'||"transactionpremiumfactor"||'|'||"transactionpremiumdlyrate"||'|'||"overriderule"||'|'||"overrideftpremium"||'|'||"overrideannualpremium"||'|'||"overridetransactionpremium"||'|'||"overridecommissionpercent"||'|'||"overridecommissionamount"||'|'||"taxamount"||'|'||"gldiv"||'|'||"gldept"||'|'||"gllob"||'|'||"siretention1"||'|'||"siretention2"||'|'||"mappingcode"||'|'||"submappingcode"||'|'||"submappingcode2"||'|'||"netrate"||'|'||"grossrate"||'|'||"rateexposure"||'|'||"adjbilltypeind"||'|'||"waivedtransactionpremium"||'|'||"classcode"||'|'||"iso_statecode"||'|'||"iso_zipcode"||'|'||"iso_ratingterritory"||'|'||"iso_typeofpolicycode"||'|'||"iso_aslobstatcode"||'|'||"iso_sublinecode"||'|'||"iso_classificationcode"||'|'||"iso_coveragestatcode"||'|'||"iso_ratingidentificationcode"||'|'||"iso_constructioncode"||'|'||"iso_fireprotectioncode"||'|'||"iso_terrorismcoveragecode"||'|'||"iso_windhaildedcode"||'|'||"iso_bcegclass"||'|'||"iso_ratingbasiscode"||'|'||"iso_liabilityformcode"||'|'||"iso_molddamagecode"||'|'||"iso_liabilityexposureindcode"||'|'||"iso_exposurestatamount"||'|'||"iso_ratingmodificationfactor"||'|'||"iso_stateexceptionindcode"||'|'||"iso_bussincomeexpensecode"||'|'||"iso_liabcovindcode"||'|'||"iso_pctowneroccupied"||'|'||"iso_classcodedesc"||'|'||"rateeffectivedate"||'|'||"deductibletype"||'|'||"iso_formcode"||'|'||"iso_losscostmultiplier"||'|'||"iso_losscostdate"||'|'||"iso_yearofconstruction"||'|'||"ouid" from "ISI_CoverageERPLawyerView";
spool off

-- p_inlineschedules
spool 'z:\p_inlineschedulesFirmAOP.csv'
select "conversionreference"||'|'||"transactiontype"||'|'||"transactioneffectivedate"||'|'||"transactionseqno"||'|'||"sbuid"||'|'||"packageid"||'|'||"riskid"||'|'||"inlinecategory"||'|'||"linenumber"||'|'||"description"||'|'||"itemvalue"||'|'||"deletedrow"||'|'||"conversionserver"||'|'||"sequencenumber"||'|'||"veffectivedate"||'|'||"vexpirydate"||'|'||"postedwweobjectid" from "ISI_InlineSchedulesFirmAOPView";
spool off
spool 'z:\p_inlineschedulesFirmLim.csv'
select "conversionreference"||'|'||"transactiontype"||'|'||"transactioneffectivedate"||'|'||"transactionseqno"||'|'||"sbuid"||'|'||"packageid"||'|'||"riskid"||'|'||"inlinecategory"||'|'||"linenumber"||'|'||"description"||'|'||"itemvalue"||'|'||"deletedrow"||'|'||"conversionserver"||'|'||"sequencenumber"||'|'||"veffectivedate"||'|'||"vexpirydate"||'|'||"postedwweobjectid" from "ISI_InlineSchedulesFirmLimView";
spool off
spool 'z:\p_inlineschedulesLawyer.csv'
select "conversionreference"||'|'||"transactiontype"||'|'||"transactioneffectivedate"||'|'||"transactionseqno"||'|'||"sbuid"||'|'||"packageid"||'|'||"riskid"||'|'||"inlinecategory"||'|'||"linenumber"||'|'||"description"||'|'||"itemvalue"||'|'||"deletedrow"||'|'||"conversionserver"||'|'||"sequencenumber"||'|'||"veffectivedate"||'|'||"vexpirydate"||'|'||"postedwweobjectid" from "ISI_InlineSchedulesLawyerView";
spool off

-- is_areaofpractice_1
spool 'z:\is_areaofpractice_1.csv'
select "conversionreference"||'|'||"transactioneffectivedate"||'|'||"transactionseqno"||'|'||"transactiontype"||'|'||"sbuid"||'|'||"packageid"||'|'||"riskid"||'|'||"inlinecategory"||'|'||"linenumber"||'|'||"aopcode"||'|'||"aopfactor"||'|'||"aopfactorhigh"||'|'||"aopfactorlow"||'|'||"aopotherexplain"||'|'||"aoppercent"||'|'||"aopweightedfactor"||'|'||"conditionalmandatoryfields"||'|'||"conversionserver"||'|'||"sequencenumber" from "ISI_AreaOfPractice1View";
spool off

-- is_quotelimits_1
spool 'z:\is_quotelimits_1.csv'
select  "conversionreference"||'|'||"transactioneffectivedate"||'|'||"transactionseqno"||'|'||"transactiontype"||'|'||"sbuid"||'|'||"packageid"||'|'||"riskid"||'|'||"inlinecategory"||'|'||"linenumber"||'|'||"additionallimitfactor"||'|'||"attorneyfactor"||'|'||"attorneyfactorsubtotal"||'|'||"attorneyfactortotal"||'|'||"baserate"||'|'||"clecreditsfactor"||'|'||"clecreditsfactorsubtotal"||'|'||"clecreditsfactortotal"||'|'||"conditionalmandatoryfields"||'|'||"continuityfactor"||'|'||"continuityfactorsubtotal"||'|'||"continuityfactortotal"||'|'||"deductiblefactor"||'|'||"deductiblefactorsubtotal"||'|'||"deductiblefactortotal"||'|'||"facpremiumxs10m"||'|'||"facpremiumxs10mreadonly"||'|'||"facpremiumxs5m"||'|'||"facpremiumxs5mreadonly"||'|'||"facpremiumxs5msysind"||'|'||"firmlinenumber"||'|'||"firmpremium"||'|'||"firmsizefactor"||'|'||"firmsizefactorsubtotal"||'|'||"firmsizefactortotal"||'|'||"liabilitydeductible"||'|'||"liabilitydeductibletext"||'|'||"liabilitylimit"||'|'||"liabilitylimit1"||'|'||"liabilitylimit2"||'|'||"liabilitylimittext"||'|'||"liabilitypremiumtext"||'|'||"limitfactor"||'|'||"limitfactorsubtotal"||'|'||"limitfactortotal"||'|'||"policylimitind"||'|'||"quoteproposalind"||'|'||"scheduledadjustmentfactor"||'|'||"scheduleadjustmentfactorsu_1"||'|'||"scheduledadjustmentfactortotal"||'|'||"xs10premiumfactor"||'|'||"xs10premiumperattorney"||'|'||"xs10premiumsubtotal"||'|'||"xs10premiumtotal"||'|'||"xs5factor"||'|'||"xs5minimum"||'|'||"xs5premiumfactor"||'|'||"xs5premiumperattorney"||'|'||"xs5premiumsubtotal"||'|'||"xs5premiumtotal"||'|'||"conversionserver"||'|'||"sequencenumber" from "ISI_QuoteLimits1View";
spool off

-- is_quotelimitsattorney_1
spool 'z:\is_quotelimitsattorney_1.csv'
select  "conversionreference"||'|'||"transactioneffectivedate"||'|'||"transactionseqno"||'|'||"transactiontype"||'|'||"sbuid"||'|'||"packageid"||'|'||"riskid"||'|'||"inlinecategory"||'|'||"linenumber"||'|'||"additionallimitfactor"||'|'||"attorneyfactor"||'|'||"attorneyfactorcomment"||'|'||"attorneyfactorsubtotal"||'|'||"attorneynumber"||'|'||"attorneypremium"||'|'||"attorneypremium1m"||'|'||"attorneypremium5m"||'|'||"baserate"||'|'||"clecreditsfactor"||'|'||"clecreditsfactorcomment"||'|'||"clecreditsfactorsubtotal"||'|'||"continuityfactor"||'|'||"continuityfactorcomment"||'|'||"continuityfactorsubtotal"||'|'||"deductiblefactor"||'|'||"deductiblefactorcomment"||'|'||"deductiblefactorsubtotal"||'|'||"firmsizefactor"||'|'||"firmsizefactorcomment"||'|'||"firmsizefactorsubtotal"||'|'||"liabilitydeductibletext"||'|'||"liabilitylimit1"||'|'||"liabilitylimit2"||'|'||"liabilitylimittext"||'|'||"limitfactor"||'|'||"limitfactorcomment"||'|'||"limitfactorsubtotal"||'|'||"militaryleaveind"||'|'||"policylimitind"||'|'||"scheduledadjustmentfactor"||'|'||"scheduledadjustmentfactorco_1"||'|'||"scheduledadjustmentfactorsu_1"||'|'||"xs10premium"||'|'||"xs10premiumcomment"||'|'||"xs10premiumperattorney"||'|'||"xs10premiumsubtotal"||'|'||"xs5factor"||'|'||"xs5minimum"||'|'||"xs5premium"||'|'||"xs5premiumcomment"||'|'||"xs5premiumperattorney"||'|'||"xs5premiumsubtotal"||'|'||"conversionserver"||'|'||"sequencenumber" from "ISI_QuoteLimitsAttorneyView";
spool off


/*
-- CLAIMS
*/
-- c_claim
spool 'z:\c_claim.csv'
select "claimmappingkey"||'|'||"claimvnumber"||'|'||"sbuid"||'|'||"claimkey"||'|'||"claimtypekey"||'|'||"cliententitymappingkey"||'|'||"cliententitykeyvnumber"||'|'||"occurrencedate"||'|'||"occurrencetime"||'|'||"reporteddate"||'|'||"reportedtime"||'|'||"claimstatus"||'|'||"ventrydatetime"||'|'||"vuserid"||'|'||"polconversionref"||'|'||"poltranstype"||'|'||"poltranseffdate"||'|'||"poltransseqno"||'|'||"prevclaimno"||'|'||"losstimezone"||'|'||"reportedtimezone"||'|'||"closedate"||'|'||"reopendate"||'|'||"chargeableind"||'|'||"eventid"||'|'||"occurrenceid"||'|'||"policecalledind"||'|'||"policefileno"||'|'||"lossdescription"||'|'||"lossnotes"||'|'||"reopenreason"||'|'||"claimsmadeind"||'|'||"reportedmethod"||'|'||"reportingtype"||'|'||"branch"||'|'||"refid"||'|'||"totallossind"||'|'||"conversionserver"||'|'||"sequencenumber"||'|'||"reopenreasonother"||'|'||"closetime"||'|'||"litigationind"||'|'||"subrogationchk"||'|'||"subrogationtext"||'|'||"packageid"||'|'||"spoilreasonother"||'|'||"spoilreason"||'|'||"ouid"||'|'||"reportedother"||'|'||"reportingtypeother"||'|'||"closereason" from "ISI_ClaimView";
spool off

-- c_contactclaimant
spool 'z:\e_contactclaimant.csv'
select "entitykey"||'|'||"entitykeyvnumber"||'|'||"sbuid"||'|'||"firstname"||'|'||"middlename"||'|'||"lastname"||'|'||"nameprefix"||'|'||"namesuffix"||'|'||"jobtitle"||'|'||"fullname"||'|'||"companyname"||'|'||"preferredhtc"||'|'||"isindividual"||'|'||"contacttype"||'|'||"birthdate"||'|'||"gender"||'|'||"maritalstatus"||'|'||"comments"||'|'||"conversionserver"||'|'||"sequencenumber"||'|'||"linkedentitykey"||'|'||"linkedentitykeyvnumber"||'|'||"ssn"||'|'||"hicn"||'|'||"fein"||'|'||"tin"||'|'||"preferredlanguage" from "ISI_ContactClaimantView";
spool off

-- c_involvedparty
spool 'z:\c_involvedparty.csv'
select "claimmappingkey"||'|'||"claimvnumber"||'|'||"invno"||'|'||"contactentitykey"||'|'||"contactentitykeyvnumber"||'|'||"description"||'|'||"sbuid"||'|'||"invdescription"||'|'||"relationnum"||'|'||"iscaller"||'|'||"htctime"||'|'||"repcomments"||'|'||"isclaimant"||'|'||"adjustermappingkey"||'|'||"adjustermappingvnumber"||'|'||"adjusterfileno"||'|'||"conversionserver"||'|'||"sequencenumber"||'|'||"claimantcontacttype"||'|'||"validcontact"||'|'||"reportingstatus" from "ISI_InvolvedPartyView";
spool off

-- c_involvedrelation
spool 'z:\c_involvedrelation.csv'
select  "claimmappingkey"||'|'||"claimvnumber"||'|'||"invno"||'|'||"relationtype"||'|'||"conversionserver"||'|'||"sequencenumber" from "ISI_InvolvedRelationView";
spool off
    
-- c_claimantdetails
spool 'z:\c_claimantdetails.csv'
select "claimmappingkey"||'|'||"claimvnumber"||'|'||"invno"||'|'||"clmttype"||'|'||"involvedtype"||'|'||"percentatfault"||'|'||"losstransfer"||'|'||"insuredbyid"||'|'||"insuredbyvnumber"||'|'||"potentialrecovery"||'|'||"lawsuitind"||'|'||"conversionserver"||'|'||"sequencenumber" from "ISI_ClaimantDetailsView";
spool off

-- c_claimantsubfile
spool 'z:\c_claimantsubfile.csv'
select "claimmappingkey"||'|'||"claimvnumber"||'|'||"invno"||'|'||"claimtypekey"||'|'||"subfilekey"||'|'||"sbuid"||'|'||"conversionserver"||'|'||"sequencenumber" from "ISI_ClaimantSubFileView";
spool off

-- c_claimitem
--spool 'z:\c_claimitem.csv'
--select "claimmappingkey"||'|'||"claimvnumber"||'|'||"polconversionref"||'|'||"poltranstype"||'|'||"poltranseffdate"||'|'||"poltransseqno"||'|'||"sbuid"||'|'||"packageid"||'|'||"riskid"||'|'||"linenumber"||'|'||"conversionserver"||'|'||"sequencenumber" from "ISI_ClaimItemView";
--spool off

spool 'z:\c_claimitem_LPL.csv'
select "claimmappingkey"||'|'||"claimvnumber"||'|'||"polconversionref"||'|'||"poltranstype"||'|'||"poltranseffdate"||'|'||"poltransseqno"||'|'||"sbuid"||'|'||"packageid"||'|'||"riskid"||'|'||"linenumber"||'|'||"conversionserver"||'|'||"sequencenumber" from "ISI_ClaimItemLPLView";
spool off
--spool 'z:\c_claimitem_GC.csv'
--select "claimmappingkey"||'|'||"claimvnumber"||'|'||"polconversionref"||'|'||"poltranstype"||'|'||"poltranseffdate"||'|'||"poltransseqno"||'|'||"sbuid"||'|'||"packageid"||'|'||"riskid"||'|'||"linenumber"||'|'||"conversionserver"||'|'||"sequencenumber" from "ISI_ClaimItemGCView";
--spool off
--spool 'z:\c_claimitem_DF.csv'
--select "claimmappingkey"||'|'||"claimvnumber"||'|'||"polconversionref"||'|'||"poltranstype"||'|'||"poltranseffdate"||'|'||"poltransseqno"||'|'||"sbuid"||'|'||"packageid"||'|'||"riskid"||'|'||"linenumber"||'|'||"conversionserver"||'|'||"sequencenumber" from "ISI_ClaimItemDFView";
--spool off
--<< ADD ADDITIONAL c_claimitem EXTRACTS HERE >>
-->>>>> ADD: Coverage Counsel ULAE
-->>>>> ADD: LawyerTail
-->>>>> ADD: Tail


-- c_claimantcoverage
spool 'z:\c_claimantcoverage_LPL.csv'
select "claimmappingkey"||'|'||"claimvnumber"||'|'||"polconversionref"||'|'||"poltranstype"||'|'||"poltranseffdate"||'|'||"poltransseqno"||'|'||"packageid"||'|'||"riskid"||'|'||"linenumber"||'|'||"coverageid"||'|'||"invno"||'|'||"subfilekey"||'|'||"claimtypekey"||'|'||"sbuid"||'|'||"kindofloss"||'|'||"classofloss"||'|'||"basisofloss"||'|'||"losscontrolevent"||'|'||"deductible"||'|'||"deductibleind"||'|'||"deductibleinvoiced"||'|'||"reservenumber"||'|'||"reservestatus"||'|'||"spoilreason"||'|'||"spoilreasonother"||'|'||"reopenreason"||'|'||"reopenreasonother"||'|'||"litigationind"||'|'||"siuind"||'|'||"salvageind"||'|'||"subrogationind"||'|'||"conversionserver"||'|'||"sequencenumber" from "ISI_ClaimantCoverageView";
spool off       
spool 'z:\c_claimantcoverage_GC.csv'
select "claimmappingkey"||'|'||"claimvnumber"||'|'||"polconversionref"||'|'||"poltranstype"||'|'||"poltranseffdate"||'|'||"poltransseqno"||'|'||"packageid"||'|'||"riskid"||'|'||"linenumber"||'|'||"coverageid"||'|'||"invno"||'|'||"subfilekey"||'|'||"claimtypekey"||'|'||"sbuid"||'|'||"kindofloss"||'|'||"classofloss"||'|'||"basisofloss"||'|'||"losscontrolevent"||'|'||"deductible"||'|'||"deductibleind"||'|'||"deductibleinvoiced"||'|'||"reservenumber"||'|'||"reservestatus"||'|'||"spoilreason"||'|'||"spoilreasonother"||'|'||"reopenreason"||'|'||"reopenreasonother"||'|'||"litigationind"||'|'||"siuind"||'|'||"salvageind"||'|'||"subrogationind"||'|'||"conversionserver"||'|'||"sequencenumber" from "ISI_ClaimantCoverageGCView";
spool off 
spool 'z:\c_claimantcoverage_DF.csv'
select "claimmappingkey"||'|'||"claimvnumber"||'|'||"polconversionref"||'|'||"poltranstype"||'|'||"poltranseffdate"||'|'||"poltransseqno"||'|'||"packageid"||'|'||"riskid"||'|'||"linenumber"||'|'||"coverageid"||'|'||"invno"||'|'||"subfilekey"||'|'||"claimtypekey"||'|'||"sbuid"||'|'||"kindofloss"||'|'||"classofloss"||'|'||"basisofloss"||'|'||"losscontrolevent"||'|'||"deductible"||'|'||"deductibleind"||'|'||"deductibleinvoiced"||'|'||"reservenumber"||'|'||"reservestatus"||'|'||"spoilreason"||'|'||"spoilreasonother"||'|'||"reopenreason"||'|'||"reopenreasonother"||'|'||"litigationind"||'|'||"siuind"||'|'||"salvageind"||'|'||"subrogationind"||'|'||"conversionserver"||'|'||"sequencenumber" from "ISI_ClaimantCoverageDFView";
spool off 
spool 'z:\c_claimantcoverage_ULAE.csv'
select "claimmappingkey"||'|'||"claimvnumber"||'|'||"polconversionref"||'|'||"poltranstype"||'|'||"poltranseffdate"||'|'||"poltransseqno"||'|'||"packageid"||'|'||"riskid"||'|'||"linenumber"||'|'||"coverageid"||'|'||"invno"||'|'||"subfilekey"||'|'||"claimtypekey"||'|'||"sbuid"||'|'||"kindofloss"||'|'||"classofloss"||'|'||"basisofloss"||'|'||"losscontrolevent"||'|'||"deductible"||'|'||"deductibleind"||'|'||"deductibleinvoiced"||'|'||"reservenumber"||'|'||"reservestatus"||'|'||"spoilreason"||'|'||"spoilreasonother"||'|'||"reopenreason"||'|'||"reopenreasonother"||'|'||"litigationind"||'|'||"siuind"||'|'||"salvageind"||'|'||"subrogationind"||'|'||"conversionserver"||'|'||"sequencenumber" from "ISI_ClaimantCoverageULAEView";
spool off 
--<< ADD ADDITIONAL c_claimantcoverage EXTRACTS HERE >
-->>>>> ADD: LawyerTail
-->>>>> ADD: Tail


-- c_claimexaminer
spool 'z:\c_claimexaminer.csv'
select "claimmappingkey"||'|'||"claimvnumber"||'|'||"userentitymappingkey"||'|'||"sbuid"||'|'||"conversionserver"||'|'||"sequencenumber" from "ISI_ClaimExaminerView";
spool off

-- c_claimexaminersubfile
spool 'z:\c_claimexaminersubfile.csv'
select "claimmappingkey"||'|'||"claimvnumber"||'|'||"userentitymappingkey"||'|'||"sbuid"||'|'||"subfilekey"||'|'||"claimtypekey"||'|'||"conversionserver"||'|'||"sequencenumber" from "ISI_ClaimExaminerSubfileView";
spool off

-- c_transaction
spool 'z:\c_transaction.csv'
---interim missing "recoverykind"
select "claimmappingkey"||'|'||"claimvnumber"||'|'||"transmappingkey"||'|'||"transactionumber"||'|'||"gltransnumber"||'|'||"ventrydate"||'|'||"accountingdate"||'|'||"action"||'|'||"reference"||'|'||"currencycd"||'|'||"description"||'|'||"transamt"||'|'||"userentitymappingkey"||'|'||"sbuid"||'|'||"chequeserieskey"||'|'||"chequepayeename"||'|'||"chequemailtoaddress"||'|'||"mailingentitykey"||'|'||"mailingentitykeyvnumber"||'|'||"paymentmethod"||'|'||"paymenttype"||'|'||"paymentkind"||'|'||"expensetype"||'|'||"chequetype"||'|'||"chequeno"||'|'||"recoverytype"||'|'||"depositno"||'|'||"transtatus"||'|'||"conversionserver"||'|'||"sequencenumber"||'|'||"postedwweobjectid"||'|'||"postedyearmth"||'|'||"exchangerate"||'|'||"payeeentitykey"||'|'||"payeeentityvnumber"||'|'||"recoverykind"||'|'||"claimtypekey" from "ISI_ClaimTransactionView";
spool off
spool 'z:\c_transactionded.csv'
select "claimmappingkey"||'|'||"claimvnumber"||'|'||"transmappingkey"||'|'||"transactionumber"||'|'||"gltransnumber"||'|'||"ventrydate"||'|'||"accountingdate"||'|'||"action"||'|'||"reference"||'|'||"currencycd"||'|'||"description"||'|'||"transamt"||'|'||"userentitymappingkey"||'|'||"sbuid"||'|'||"chequeserieskey"||'|'||"chequepayeename"||'|'||"chequemailtoaddress"||'|'||"mailingentitykey"||'|'||"mailingentitykeyvnumber"||'|'||"paymentmethod"||'|'||"paymenttype"||'|'||"paymentkind"||'|'||"expensetype"||'|'||"chequetype"||'|'||"chequeno"||'|'||"recoverytype"||'|'||"depositno"||'|'||"transtatus"||'|'||"conversionserver"||'|'||"sequencenumber"||'|'||"postedwweobjectid"||'|'||"postedyearmth"||'|'||"exchangerate"||'|'||"payeeentitykey"||'|'||"payeeentityvnumber"||'|'||"recoverykind"||'|'||"claimtypekey" from "ISI_ClaimTransactionDedView";
spool off
spool 'z:\c_transactionULAE.csv'
select "claimmappingkey"||'|'||"claimvnumber"||'|'||"transmappingkey"||'|'||"transactionumber"||'|'||"gltransnumber"||'|'||"ventrydate"||'|'||"accountingdate"||'|'||"action"||'|'||"reference"||'|'||"currencycd"||'|'||"description"||'|'||"transamt"||'|'||"userentitymappingkey"||'|'||"sbuid"||'|'||"chequeserieskey"||'|'||"chequepayeename"||'|'||"chequemailtoaddress"||'|'||"mailingentitykey"||'|'||"mailingentitykeyvnumber"||'|'||"paymentmethod"||'|'||"paymenttype"||'|'||"paymentkind"||'|'||"expensetype"||'|'||"chequetype"||'|'||"chequeno"||'|'||"recoverytype"||'|'||"depositno"||'|'||"transtatus"||'|'||"conversionserver"||'|'||"sequencenumber"||'|'||"postedwweobjectid"||'|'||"postedyearmth"||'|'||"exchangerate"||'|'||"payeeentitykey"||'|'||"payeeentityvnumber"||'|'||"recoverykind"||'|'||"claimtypekey" from "ISI_ClaimTransactionULAEView";
spool off

-- c_transactioncoverage
spool 'z:\c_transcov_res_alae.csv'
select "transmappingkey"||'|'||"claimmappingkey"||'|'||"claimvnumber"||'|'||"polconversionref"||'|'||"poltranstype"||'|'||"poltranseffdate"||'|'||"poltransseqno"||'|'||"sbuid"||'|'||"packageid"||'|'||"riskid"||'|'||"linenumber"||'|'||"coverageid"||'|'||"invno"||'|'||"subfilekey"||'|'||"claimtypekey"||'|'||"kindofloss"||'|'||"transtype"||'|'||"losspayment"||'|'||"expensepayment"||'|'||"salrecovery"||'|'||"subrecovery"||'|'||"lossreserve"||'|'||"expensereserve"||'|'||"recoveryreserve"||'|'||"conversionserver"||'|'||"sequencenumber"||'|'||"iscurrent"||'|'||"lossrecovered"||'|'||"expenserecovered"||'|'||"classofloss"||'|'||"basisofloss"||'|'||"losscontrolevent" from "ISI_ClaimTranCov_ResALAEView";
spool off
spool 'z:\c_transcov_res_ind.csv'
select "transmappingkey"||'|'||"claimmappingkey"||'|'||"claimvnumber"||'|'||"polconversionref"||'|'||"poltranstype"||'|'||"poltranseffdate"||'|'||"poltransseqno"||'|'||"sbuid"||'|'||"packageid"||'|'||"riskid"||'|'||"linenumber"||'|'||"coverageid"||'|'||"invno"||'|'||"subfilekey"||'|'||"claimtypekey"||'|'||"kindofloss"||'|'||"transtype"||'|'||"losspayment"||'|'||"expensepayment"||'|'||"salrecovery"||'|'||"subrecovery"||'|'||"lossreserve"||'|'||"expensereserve"||'|'||"recoveryreserve"||'|'||"conversionserver"||'|'||"sequencenumber"||'|'||"iscurrent"||'|'||"lossrecovered"||'|'||"expenserecovered"||'|'||"classofloss"||'|'||"basisofloss"||'|'||"losscontrolevent" from "ISI_ClaimTranCov_ResIndView";
spool off
spool 'z:\c_transcov_pay_alae.csv'
select "transmappingkey"||'|'||"claimmappingkey"||'|'||"claimvnumber"||'|'||"polconversionref"||'|'||"poltranstype"||'|'||"poltranseffdate"||'|'||"poltransseqno"||'|'||"sbuid"||'|'||"packageid"||'|'||"riskid"||'|'||"linenumber"||'|'||"coverageid"||'|'||"invno"||'|'||"subfilekey"||'|'||"claimtypekey"||'|'||"kindofloss"||'|'||"transtype"||'|'||"losspayment"||'|'||"expensepayment"||'|'||"salrecovery"||'|'||"subrecovery"||'|'||"lossreserve"||'|'||"expensereserve"||'|'||"recoveryreserve"||'|'||"conversionserver"||'|'||"sequencenumber"||'|'||"iscurrent"||'|'||"lossrecovered"||'|'||"expenserecovered"||'|'||"classofloss"||'|'||"basisofloss"||'|'||"losscontrolevent" from "ISI_ClaimTranCov_PayALAEView";
spool off
spool 'z:\c_transcov_pay_ind.csv'
select "transmappingkey"||'|'||"claimmappingkey"||'|'||"claimvnumber"||'|'||"polconversionref"||'|'||"poltranstype"||'|'||"poltranseffdate"||'|'||"poltransseqno"||'|'||"sbuid"||'|'||"packageid"||'|'||"riskid"||'|'||"linenumber"||'|'||"coverageid"||'|'||"invno"||'|'||"subfilekey"||'|'||"claimtypekey"||'|'||"kindofloss"||'|'||"transtype"||'|'||"losspayment"||'|'||"expensepayment"||'|'||"salrecovery"||'|'||"subrecovery"||'|'||"lossreserve"||'|'||"expensereserve"||'|'||"recoveryreserve"||'|'||"conversionserver"||'|'||"sequencenumber"||'|'||"iscurrent"||'|'||"lossrecovered"||'|'||"expenserecovered"||'|'||"classofloss"||'|'||"basisofloss"||'|'||"losscontrolevent" from "ISI_ClaimTranCov_PayIndView";
spool off
spool 'z:\c_transcov_pay_ded.csv'
select "transmappingkey"||'|'||"claimmappingkey"||'|'||"claimvnumber"||'|'||"polconversionref"||'|'||"poltranstype"||'|'||"poltranseffdate"||'|'||"poltransseqno"||'|'||"sbuid"||'|'||"packageid"||'|'||"riskid"||'|'||"linenumber"||'|'||"coverageid"||'|'||"invno"||'|'||"subfilekey"||'|'||"claimtypekey"||'|'||"kindofloss"||'|'||"transtype"||'|'||"losspayment"||'|'||"expensepayment"||'|'||"salrecovery"||'|'||"subrecovery"||'|'||"lossreserve"||'|'||"expensereserve"||'|'||"recoveryreserve"||'|'||"conversionserver"||'|'||"sequencenumber"||'|'||"iscurrent"||'|'||"lossrecovered"||'|'||"expenserecovered"||'|'||"classofloss"||'|'||"basisofloss"||'|'||"losscontrolevent" from "ISI_ClaimTranCov_PayDedView";
spool off
spool 'z:\c_transcov_pay_ulae.csv'
select "transmappingkey"||'|'||"claimmappingkey"||'|'||"claimvnumber"||'|'||"polconversionref"||'|'||"poltranstype"||'|'||"poltranseffdate"||'|'||"poltransseqno"||'|'||"sbuid"||'|'||"packageid"||'|'||"riskid"||'|'||"linenumber"||'|'||"coverageid"||'|'||"invno"||'|'||"subfilekey"||'|'||"claimtypekey"||'|'||"kindofloss"||'|'||"transtype"||'|'||"losspayment"||'|'||"expensepayment"||'|'||"salrecovery"||'|'||"subrecovery"||'|'||"lossreserve"||'|'||"expensereserve"||'|'||"recoveryreserve"||'|'||"conversionserver"||'|'||"sequencenumber"||'|'||"iscurrent"||'|'||"lossrecovered"||'|'||"expenserecovered"||'|'||"classofloss"||'|'||"basisofloss"||'|'||"losscontrolevent" from "ISI_ClaimTranCov_PayULAEView";
spool off
        
-- c_transactioncategory
spool 'z:\c_transcat_res_alae.csv'
select "transmappingkey"||'|'||"claimmappingkey"||'|'||"claimvnumber"||'|'||"sbuid"||'|'||"claimtypekey"||'|'||"categorykey"||'|'||"transtype"||'|'||"categorytype"||'|'||"amount"||'|'||"reservechange"||'|'||"conversionserver"||'|'||"sequencenumber" from "ISI_ClaimTransCat_ResALAEView";
spool off
spool 'z:\c_transcat_res_ind.csv'
select "transmappingkey"||'|'||"claimmappingkey"||'|'||"claimvnumber"||'|'||"sbuid"||'|'||"claimtypekey"||'|'||"categorykey"||'|'||"transtype"||'|'||"categorytype"||'|'||"amount"||'|'||"reservechange"||'|'||"conversionserver"||'|'||"sequencenumber" from "ISI_ClaimTransCat_ResIndView";
spool off
spool 'z:\c_transcat_pay_alae.csv'
select "transmappingkey"||'|'||"claimmappingkey"||'|'||"claimvnumber"||'|'||"sbuid"||'|'||"claimtypekey"||'|'||"categorykey"||'|'||"transtype"||'|'||"categorytype"||'|'||"amount"||'|'||"reservechange"||'|'||"conversionserver"||'|'||"sequencenumber" from "ISI_ClaimTransCat_PayALAEView";
spool off
spool 'z:\c_transcat_pay_ind.csv'
select "transmappingkey"||'|'||"claimmappingkey"||'|'||"claimvnumber"||'|'||"sbuid"||'|'||"claimtypekey"||'|'||"categorykey"||'|'||"transtype"||'|'||"categorytype"||'|'||"amount"||'|'||"reservechange"||'|'||"conversionserver"||'|'||"sequencenumber" from "ISI_ClaimTransCat_PayIndView";
spool off
spool 'z:\c_transcat_ded.csv'
select "transmappingkey"||'|'||"claimmappingkey"||'|'||"claimvnumber"||'|'||"sbuid"||'|'||"claimtypekey"||'|'||"categorykey"||'|'||"transtype"||'|'||"categorytype"||'|'||"amount"||'|'||"reservechange"||'|'||"conversionserver"||'|'||"sequencenumber" from "ISI_ClaimTransCat_PayDedView";
spool off
spool 'z:\c_transcat_ULAE.csv'
select "transmappingkey"||'|'||"claimmappingkey"||'|'||"claimvnumber"||'|'||"sbuid"||'|'||"claimtypekey"||'|'||"categorykey"||'|'||"transtype"||'|'||"categorytype"||'|'||"amount"||'|'||"reservechange"||'|'||"conversionserver"||'|'||"sequencenumber" from "ISI_ClaimTransCat_PayULAEView;
spool off

-- c_cl_clmlossdetail_1
--utl_file.fremove('z:\', 'c_cl_clmlossdetail_1.csv');
spool 'z:\c_cl_clmlossdetail_1.csv'
select "claimmappingkey" ||'|'||"claimvnumber" ||'|'||"additionalbenefitstype"||'|'||"allegederror" ||'|'||"allegederrorvalues"||'|'||"areaofpractice" ||'|'||"associatedsublaw" ||'|'||"associatedsublawvalues" ||'|'||"claimrepairind"||'|'||"claimremarkdate"||'|'||"claimremarkind"||'|'||"claimrepairindvo" ||'|'||"conflictofinterestind" ||'|'||"conflictofinteresttype" ||'|'||"cyberdatabreachind" ||'|'||"descofresolution" ||'|'||"dhccomplaintind" ||'|'||"ethicsissueind" ||'|'||"factmalpracticeallegations" ||'|'||"factualcoverageissue" ||'|'||"factualdamages" ||'|'||"factualevaluation" ||'|'||"factualplan" ||'|'||"factualprimarycase" ||'|'||"familyfailure" ||'|'||"feedispluteind" ||'|'||"groundsforror" ||'|'||"groundsforrorvalues" ||'|'||"insurermistakeind" ||'|'||"possibleexcessclaim" ||'|'||"priorsolfile" ||'|'||"reinsreportingrequiredind" ||'|'||"searcherrormadeby" ||'|'||"stateofaccident" ||'|'||"subpoenarequests" ||'|'||"subpeonarequestsvalues" ||'|'||"subpeonatoinsured" ||'|'||"successclaimrepairind" ||'|'||"successclaimrepairindvo" ||'|'||"taxrelatedissueind" ||'|'||"testatorcompetenceind" ||'|'||"timingofresolution" ||'|'||"titleinscompany" ||'|'||"titleinscompanyind" ||'|'||"titleinscompanyvalues" ||'|'||"tpbeneficiary2ind" ||'|'||"tpbeneficiaryind" ||'|'||"trustaccountissueind" ||'|'||"conversionserver" ||'|'||"sequencenumber" from "ISI_ClaimLossDetailsView";
spool off

-- c_claimantworkarea
spool 'z:\c_claimantworkarea.csv'
select "claimmappingkey"||'|'||"claimvnumber"||'|'||"polconversionref"||'|'||"poltranstype"||'|'||"poltranseffdate"||'|'||"poltransseqno"||'|'||"packageid"||'|'||"riskid"||'|'||"linenumber"||'|'||"coverageid"||'|'||"invno"||'|'||"subfilekey"||'|'||"claimtypekey"||'|'||"sbuid"||'|'||"workareakey"||'|'||"conversionserver"||'|'||"sequencenumber" from "ISI_ClaimantWorkAreaView";
spool off
spool 'z:\c_claimantworkareaULAE.csv'
select "claimmappingkey"||'|'||"claimvnumber"||'|'||"polconversionref"||'|'||"poltranstype"||'|'||"poltranseffdate"||'|'||"poltransseqno"||'|'||"packageid"||'|'||"riskid"||'|'||"linenumber"||'|'||"coverageid"||'|'||"invno"||'|'||"subfilekey"||'|'||"claimtypekey"||'|'||"sbuid"||'|'||"workareakey"||'|'||"conversionserver"||'|'||"sequencenumber" from "ISI_ClaimantWorkAreaULAEView";
spool off

--c_clclmtlitigation_1
spool 'z:\c_cl_clmtlitigation_1.csv'
select "claimmappingkey"||'|'||"claimvnumber"||'|'||"polconversionref"||'|'||"poltranseffdate"||'|'||"poltransseqno"||'|'||"poltranstype"||'|'||"packageid"||'|'||"riskid"||'|'||"linenumber"||'|'||"coverageid"||'|'||"invno"||'|'||"subfilekey"||'|'||"claimtypekey"||'|'||"sbuid"||'|'||"workareakey"||'|'||"claimantexpert"||'|'||"claimantexpertid_ek"||'|'||"claimantexpertid_vn"||'|'||"claimantprose"||'|'||"defenseattorney"||'|'||"defenseattorney2"||'|'||"defenseattorney2id_ek"||'|'||"defenseattorney2id_vn"||'|'||"defenseattorneyid_ek"||'|'||"defenseattorneyid_vn"||'|'||"defenseexpert"||'|'||"defenseexpertid_ek"||'|'||"defenseexpertid_vn"||'|'||"defensefirm"||'|'||"defensefirm2"||'|'||"defensefirm2id_ek"||'|'||"defensefirm2id_vn"||'|'||"defensefirmid_ek"||'|'||"defensefirmid_vn"||'|'||"indemexpdate"||'|'||"judge"||'|'||"judgeid_ek"||'|'||"judgeid_vn"||'|'||"longtermidemind"||'|'||"mediationdate"||'|'||"mediator"||'|'||"mediatorid_ek"||'|'||"mediatorid_vn"||'|'||"pendingmotions"||'|'||"plaintiffattorney"||'|'||"plaintiffattorney2"||'|'||"plaintiffattorney2id_ek"||'|'||"plaintiffattorney2id_vn"||'|'||"plaintiffattorneyid_ek"||'|'||"plaintiffattorneyid_vn"||'|'||"plaintiffirm"||'|'||"plaintiffirm2"||'|'||"plaintiffirm2id_ek"||'|'||"plaintiffirm2id_vn"||'|'||"plaintiffirmid_ek"||'|'||"plaintiffirmid_vn"||'|'||"tollingagreeexpdate"||'|'||"tollingagreeind"||'|'||"trialdate"||'|'||"venue"||'|'|| "conversionserver"||'|'||"sequencenumber" from "ISI_ClaimantLitigationView";
spool off


/*
-- INCIDENT
*/
-- c_claim
spool 'z:\c_INCIDENT.csv'
select "claimmappingkey"||'|'||"claimvnumber"||'|'||"sbuid"||'|'||"claimkey"||'|'||"claimtypekey"||'|'||"cliententitymappingkey"||'|'||"cliententitykeyvnumber"||'|'||"occurrencedate"||'|'||"occurrencetime"||'|'||"reporteddate"||'|'||"reportedtime"||'|'||"claimstatus"||'|'||"ventrydatetime"||'|'||"vuserid"||'|'||"polconversionref"||'|'||"poltranstype"||'|'||"poltranseffdate"||'|'||"poltransseqno"||'|'||"prevclaimno"||'|'||"losstimezone"||'|'||"reportedtimezone"||'|'||"closedate"||'|'||"reopendate"||'|'||"chargeableind"||'|'||"eventid"||'|'||"occurrenceid"||'|'||"policecalledind"||'|'||"policefileno"||'|'||"lossdescription"||'|'||"lossnotes"||'|'||"reopenreason"||'|'||"claimsmadeind"||'|'||"reportedmethod"||'|'||"reportingtype"||'|'||"branch"||'|'||"refid"||'|'||"totallossind"||'|'||"conversionserver"||'|'||"sequencenumber"||'|'||"reopenreasonother"||'|'||"closetime"||'|'||"litigationind"||'|'||"subrogationchk"||'|'||"subrogationtext"||'|'||"packageid"||'|'||"spoilreasonother"||'|'||"spoilreason"||'|'||"ouid"||'|'||"reportedother"||'|'||"reportingtypeother"||'|'||"closereason" from "ISI_IncidentView";
spool off

-- contactclaimant (incident)
spool 'z:\e_contactclaimantINCIDENT.csv'
select "entitykey"||'|'||"entitykeyvnumber"||'|'||"sbuid"||'|'||"firstname"||'|'||"middlename"||'|'||"lastname"||'|'||"nameprefix"||'|'||"namesuffix"||'|'||"jobtitle"||'|'||"fullname"||'|'||"companyname"||'|'||"preferredhtc"||'|'||"isindividual"||'|'||"contacttype"||'|'||"birthdate"||'|'||"gender"||'|'||"maritalstatus"||'|'||"comments"||'|'||"conversionserver"||'|'||"sequencenumber"||'|'||"linkedentitykey"||'|'||"linkedentitykeyvnumber"||'|'||"ssn"||'|'||"hicn"||'|'||"fein"||'|'||"tin"||'|'||"preferredlanguage" from "ISI_ContactClaimantINCView";
spool off

-- c_involvedparty
spool 'z:\c_involvedpartyINCIDENT.csv'
select "claimmappingkey"||'|'||"claimvnumber"||'|'||"invno"||'|'||"contactentitykey"||'|'||"contactentitykeyvnumber"||'|'||"description"||'|'||"sbuid"||'|'||"invdescription"||'|'||"relationnum"||'|'||"iscaller"||'|'||"htctime"||'|'||"repcomments"||'|'||"isclaimant"||'|'||"adjustermappingkey"||'|'||"adjustermappingvnumber"||'|'||"adjusterfileno"||'|'||"conversionserver"||'|'||"sequencenumber"||'|'||"claimantcontacttype"||'|'||"validcontact"||'|'||"reportingstatus" from "ISI_InvolvedPartyINCView";
spool off

-- c_involvedrelation
spool 'z:\c_involvedrelationINCIDENT.csv'
select  "claimmappingkey"||'|'||"claimvnumber"||'|'||"invno"||'|'||"relationtype"||'|'||"conversionserver"||'|'||"sequencenumber" from "ISI_InvolvedRelationINCView";
spool off

-- c_claimantdetails
spool 'z:\c_claimantdetailsINCIDENT.csv'
select "claimmappingkey"||'|'||"claimvnumber"||'|'||"invno"||'|'||"clmttype"||'|'||"involvedtype"||'|'||"percentatfault"||'|'||"losstransfer"||'|'||"insuredbyid"||'|'||"insuredbyvnumber"||'|'||"potentialrecovery"||'|'||"lawsuitind"||'|'||"conversionserver"||'|'||"sequencenumber" from "ISI_ClaimantDetailsINCView";
spool off

-- c_claimantsubfile
spool 'z:\c_claimantsubfileINCIDENT.csv'
select "claimmappingkey"||'|'||"claimvnumber"||'|'||"invno"||'|'||"claimtypekey"||'|'||"subfilekey"||'|'||"sbuid"||'|'||"conversionserver"||'|'||"sequencenumber" from "ISI_ClaimantSubFileINCView";
spool off

-- c_claimexaminer
spool 'z:\c_claimexaminerINCIDENT.csv'
select "claimmappingkey"||'|'||"claimvnumber"||'|'||"userentitymappingkey"||'|'||"sbuid"||'|'||"conversionserver"||'|'||"sequencenumber" from "ISI_ClaimExaminerINCView";
spool off

-- c_claimantexaminersubfile
spool 'z:\c_claimexaminersubfileINCIDENT.csv'
select "claimmappingkey"||'|'||"claimvnumber"||'|'||"userentitymappingkey"||'|'||"sbuid"||'|'||"subfilekey"||'|'||"claimtypekey"||'|'||"conversionserver"||'|'||"sequencenumber" from "ISI_ClaimExaminerSubfileINCView";
spool off

--c_cl_clmlossdetails_1
spool 'z:\c_cl_claimlossdetail_1INCIDENT.csv'
select "claimmappingkey" ||'|'||"claimvnumber" ||'|'||"additionalbenefitstype"||'|'||"allegederror" ||'|'||"allegederrorvalues"||'|'||"areaofpractice" ||'|'||"associatedsublaw" ||'|'||"associatedsublawvalues" ||'|'||"claimrepairind"||'|'||"claimremarkdate"||'|'||"claimremarkind"||'|'||"claimrepairindvo" ||'|'||"conflictofinterestind" ||'|'||"conflictofinteresttype" ||'|'||"cyberdatabreachind" ||'|'||"descofresolution" ||'|'||"dhccomplaintind" ||'|'||"ethicsissueind" ||'|'||"factmalpracticeallegations" ||'|'||"factualcoverageissue" ||'|'||"factualdamages" ||'|'||"factualevaluation" ||'|'||"factualplan" ||'|'||"factualprimarycase" ||'|'||"familyfailure" ||'|'||"feedispluteind" ||'|'||"groundsforror" ||'|'||"groundsforrorvalues" ||'|'||"insurermistakeind" ||'|'||"possibleexcessclaim" ||'|'||"priorsolfile" ||'|'||"reinsreportingrequiredind" ||'|'||"searcherrormadeby" ||'|'||"stateofaccident" ||'|'||"subpoenarequests" ||'|'||"subpeonarequestsvalues" ||'|'||"subpeonatoinsured" ||'|'||"successclaimrepairind" ||'|'||"successclaimrepairindvo" ||'|'||"taxrelatedissueind" ||'|'||"testatorcompetenceind" ||'|'||"timingofresolution" ||'|'||"titleinscompany" ||'|'||"titleinscompanyind" ||'|'||"titleinscompanyvalues" ||'|'||"tpbeneficiary2ind" ||'|'||"tpbeneficiaryind" ||'|'||"trustaccountissueind" ||'|'||"conversionserver" ||'|'||"sequencenumber" from "ISI_ClaimLossDetailsINCView";
spool off


/*
-- ACTIVITY
*/
-- a_activity
spool 'z:\a_activity.csv'
select "activitykey"||'|'||"vnumber"||'|'||"ventrydate"||'|'||"vmodifieddate"||'|'||"vmodifiedtime"||'|'||"vusermappingkey"||'|'||"activitytypekey"||'|'||"status"||'|'||"subject"||'|'||"sbuid"||'|'||"detaildescription"||'|'||"receiveddate"||'|'||"resolveddate"||'|'||"resolvedbymappingkey"||'|'||"assignedtokey"||'|'||"priority"||'|'||"percentcomplete"||'|'||"activequeuekey"||'|'||"activeownerkey"||'|'||"projectedstartdate"||'|'||"projectedstarttime"||'|'||"projectedenddate"||'|'||"projectedendtime"||'|'||"duedate"||'|'||"taglist"||'|'||"activitycommtype"||'|'||"vstatus"||'|'||"reminderdate"||'|'||"remindertime"||'|'||"reminderkey"||'|'||"notetypeid"||'|'||"privateind"||'|'||"conversionserver"||'|'||"sequencenumber" from "ISI_ActivityView";
spool off

-- l_linkdocument
spool 'z:\l_linkdocument.csv'
select "activitykey"||'|'||"activityvnumber"||'|'||"linkedentitykey"||'|'||"linkedentityvnumber"||'|'||"linktype"||'|'||"sbuid"||'|'||"polconversionref"||'|'||"poltransactiontype"||'|'||"poltransactioneffectivedate"||'|'||"poltransactionseqno"||'|'||"conversionserver"||'|'||"sequencenumber" from "ISI_ActivityLinkView";
spool off

-- l_activitydocuments
spool 'z:\l_activitydocuments.csv'
select "activitykey"||'|'||"sbuid"||'|'||"fullpath"||'|'||"filename"||'|'||"categoryid"||'|'||"vusermappingkey"||'|'||"description"||'|'||"ventrydate"||'|'||"ventrytime"||'|'||"conversionserver"||'|'||"sequencenumber"||'|'||"activityvnumber" from "ISI_ActivityDocumentsView";
spool off



/*
-- INCIDENT
*/